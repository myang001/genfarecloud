The repo to be used for all Cloud related projects

Will add an overall build project at a later date
Run  mvn install in the GenfareParent for the build on the eFare project to work.
Will update the distribution management as soon as the nexus repository is setup


Developer Setup
---------------

Prerequisites:

* VPN Access to the Development VPC
* [VirtualBox](https://www.virtualbox.org/) installed 
* [Vagrant](http://www.vagrantup.com/) installed

Once you are VPN'ed in, open a console and change to this directory. From this 
directory run the command `vagrant up`. This will do several things:

1. Download a basic VM image (first time only, can take a while)
2. Instantiate a new VM in virtual box
3. Configure the VM with the puppet manifest found in `manifests/site.pp`
    * Installs MySQL, JDK, JBoss EAP, Maven (configured for our nexus), and the AWS 
      CLI (with the read only `vagrant` account)
4. Mounts this directory in the VM as `/vagrant`
5. Maps ports 8080 and 9990 from host to VM
6. Sets up SSH for easy access

The first time this runs it will take a while to download the base image. After 
the initial run it will take ~15 minutes to create a new vm.

You should now be able to open a browser and navigate to 
`http://cdta-local.gfcp.io:8080` and see the JBoss welcome page. Once you deploy 
applications they can also be accessed at this URL. `http://cdta-local.gfcp.io:9990`
brings you to the JBoss Web Console.

Use `vagrant ssh` to access the VM. You are now logged on as the `vagrant` 
user. To get started, execute the following:

1. `cd /vagrant`
2. `mvn clean install -pl eFare -am`

This will apply "clean install" goals to the eFare sub-module (-pl submodule),
and all its dependencies (-am). It will also skip all integration tests by
default, and deploy to JBoss during install by default.

If you would like to run integration tests, use:

`mvn clean install [-pl eFare -am] -DskipITs=false`

If you would like to skip JBoss deployment, use:

`mvn clean install [-pl eFare -am] -DskipDeployment=true`

To deploy your SNAPSHOT to the S3 repository and skip JBoss deployment:

`mvn clean install deploy [-pl eFare -am] -DskipDeployment`

To clean your database manually with flyway, execute:

`mvn flyway:clean -pl model`

To Build and Deploy a Project and Run one feature file's test scenarios use:

`mvn clean install -pl :eFare -Dcucumber.options="--tags shopping cart "`

Compile and run only integration tests, ***application must already be deployed

`'mvn clean test-compile failsafe:integration-test`

To view log files:
`tail -f -n 100 /opt/jboss/standalone/log/gfcp.log`

Back on your host, you should now be able to access `http://cdta-local.gfcp.io:8080/eFare/`

When you are done with the VM, back on your host console either `vagrant 
suspend`, `vagrant halt`, or if you want to just delete the VM `vagrant 
destroy`. Either way, when you want to use the VM again execute `vagrant up`.

To reprovision the machchine, execute `vagrant provision` if the system is
already running. You can also start the system and provision at the same time
with `vagrant up --provision`
