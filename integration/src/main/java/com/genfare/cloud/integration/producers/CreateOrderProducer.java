/**
 *
 */
package com.genfare.cloud.integration.producers;

import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.core.JmsTemplate;

import com.genfare.cloud.integration.annotation.Producer;
import com.genfare.cloud.model.converters.OrdersToOrderManagementAPIType;
import com.genfare.cloud.model.entity.Orders;
import com.genfare.cloud.model.om.ObjectFactory;
import com.genfare.cloud.model.om.OrderManagementAPIType;

/**
 * @author mpeter
 */
@Producer(destination = "order_publish")
public class CreateOrderProducer {

    private static final Logger LOGGER = LoggerFactory.getLogger(CreateOrderProducer.class);

    private JmsTemplate jmsTemplate;

    public void sendOrderNotification(String schemaName, Orders ord) {
        OrdersToOrderManagementAPIType converter = new OrdersToOrderManagementAPIType();
        OrderManagementAPIType ordApi = converter.convert(ord);
        ordApi.setSchemaName(schemaName);

        JAXBContext jaxbContext = null;
        try {
            jaxbContext = JAXBContext.newInstance(OrderManagementAPIType.class);
            ObjectFactory objectFactory = new ObjectFactory();
            JAXBElement<OrderManagementAPIType> ordJaxb = objectFactory.createOrderManagementAPI(ordApi);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            final StringWriter stringWriter = new StringWriter();
            jaxbMarshaller.marshal(ordJaxb, stringWriter);

            LOGGER.info("Sending XML: " + stringWriter.toString());

            jmsTemplate.convertAndSend(stringWriter.toString());

        } catch (JAXBException e) {
            LOGGER.error("Error sending Create Order Notification: JAXBException:", e);
        }
    }
}
