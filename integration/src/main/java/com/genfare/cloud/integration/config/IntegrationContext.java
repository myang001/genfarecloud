package com.genfare.cloud.integration.config;

import org.skyscreamer.nevado.jms.NevadoConnectionFactory;
import org.skyscreamer.nevado.jms.connector.amazonaws.AmazonAwsSQSConnectorFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.genfare.cloud.model.tenant.TenantConfiguration;
import com.genfare.cloud.model.tenant.impl.ResourceTenantConfigurationRepository;

/**
 * @author mpeter
 */
@Configuration
@ComponentScan(basePackages = { "com.genfare.cloud.integration.receivers", "com.genfare.cloud.integration.producers",
    "com.genfare.cloud.integration.converters" })
public class IntegrationContext {

    private TenantConfiguration tenantConfiguration;

    public IntegrationContext() {
        ResourceTenantConfigurationRepository cut;
        cut = new ResourceTenantConfigurationRepository();
        tenantConfiguration = cut.getByDomainName("localhost");
        // TODO: Genericize this and try to make producer/consumers tenant aware
    }

    @Bean
    public NevadoConnectionFactory connectionFactory() {
        AmazonAwsSQSConnectorFactory sqscf = new AmazonAwsSQSConnectorFactory();
        NevadoConnectionFactory cf = new NevadoConnectionFactory(sqscf);
        cf.setAwsAccessKey(tenantConfiguration.getAmazonConfiguration().getAwsAccessKey());
        cf.setAwsSecretKey(tenantConfiguration.getAmazonConfiguration().getAwsSecretKey());
        return cf;
    }
}
