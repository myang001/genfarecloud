package com.genfare.cloud.integration.registry;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

import javax.annotation.PostConstruct;

import org.skyscreamer.nevado.jms.NevadoConnectionFactory;
import org.skyscreamer.nevado.jms.destination.NevadoTopic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.ConstructorArgumentValues;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.type.filter.AnnotationTypeFilter;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import com.genfare.cloud.integration.annotation.Producer;

@Service
public class ProducerRegistry {

    @Autowired
    NevadoConnectionFactory connectionFactory;

    @Autowired
    SpringBeanRegistry beanRegistry;

    @PostConstruct
    private void register() throws ClassNotFoundException, NoSuchFieldException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException,
        NoSuchMethodException, InstantiationException {
        ClassPathScanningCandidateComponentProvider scanner = new ClassPathScanningCandidateComponentProvider(true);

        scanner.addIncludeFilter(new AnnotationTypeFilter(Producer.class));

        // TODO: Dynamic, full class-path, or list based scan instead of
        // specific directory
        for (BeanDefinition bd : scanner.findCandidateComponents("com.genfare.cloud")) {
            Class<?> clazz = Class.forName(bd.getBeanClassName());
            Producer p = clazz.getAnnotation(Producer.class);

            if (p == null) {
                continue;
            }

            String destination = System.getProperty("aws.Environment") + "-" + p.destination();
            ConstructorArgumentValues args = new ConstructorArgumentValues();
            args.addIndexedArgumentValue(0, connectionFactory);

            JmsTemplate jmst = beanRegistry.registerBean(destination + "-template", JmsTemplate.class, args, null);
            jmst.setDefaultDestination(generateTopic(destination));

            Object o = beanRegistry.registerBean(destination + "-producer", clazz, null, null);

            Field f = clazz.getDeclaredField("jmsTemplate");
            f.setAccessible(true);
            f.set(o, jmst);

        }
    }

    private NevadoTopic generateTopic(String destination) {
        ConstructorArgumentValues args = new ConstructorArgumentValues();
        args.addGenericArgumentValue(destination);
        NevadoTopic topic = beanRegistry.registerBean(destination, NevadoTopic.class, args, null);
        return topic;
    }

}
