package com.genfare.cloud.integration.registry;

import org.springframework.beans.MutablePropertyValues;
import org.springframework.beans.factory.config.ConstructorArgumentValues;

public interface SpringBeanRegistry {
    <T> T registerBean(String name, Class<?> clazz, ConstructorArgumentValues constructorArgumentValues,
        MutablePropertyValues propertyValues);
}
