package com.genfare.cloud.integration.registry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.MutablePropertyValues;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.beans.factory.config.ConstructorArgumentValues;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.GenericBeanDefinition;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

@Service
public class SpringBeanRegistryImpl implements SpringBeanRegistry {

    private static final Logger LOGGER = LoggerFactory.getLogger(SpringBeanRegistryImpl.class);

    @Autowired
    private ApplicationContext appContext;

    private void createBean(String name, Class<?> clazz, ConstructorArgumentValues constructorArgumentValues, MutablePropertyValues propertyValues) {
        AutowireCapableBeanFactory factory = appContext.getAutowireCapableBeanFactory();
        BeanDefinitionRegistry registry = (BeanDefinitionRegistry) factory;
        GenericBeanDefinition beanDefinition = new GenericBeanDefinition();
        beanDefinition.setBeanClass(clazz);
        beanDefinition.setConstructorArgumentValues(constructorArgumentValues);
        beanDefinition.setPropertyValues(propertyValues);
        beanDefinition.setAutowireCandidate(true);
        registry.registerBeanDefinition(name, beanDefinition);
        factory.autowireBeanProperties(this, AutowireCapableBeanFactory.AUTOWIRE_BY_TYPE, false);
    }

    @SuppressWarnings("unchecked")
    private <T> T getBean(String name) {
        // TODO: Try to make this typesafe so we do not need the unchecked
        // suppression
        return (T) appContext.getBean(name);
    }

    @Override
    public <T> T registerBean(String name, Class<?> clazz, ConstructorArgumentValues constructorArgumentValues, MutablePropertyValues propertyValues) {
        createBean(name, clazz, constructorArgumentValues, propertyValues);
        LOGGER.info("Registering Bean " + name + " of class " + clazz.getName());
        return getBean(name);
    }
}
