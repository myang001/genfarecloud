package com.genfare.cloud.integration.producers;

import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.skyscreamer.nevado.jms.NevadoConnectionFactory;
import org.skyscreamer.nevado.jms.destination.NevadoQueue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;

import com.genfare.cloud.integration.annotation.Producer;
import com.genfare.cloud.model.converters.SubscriptionsToSubscriptonAPIType;
import com.genfare.cloud.model.entity.Subscriptions;
import com.genfare.cloud.model.om.ObjectFactory;
import com.genfare.cloud.model.om.SubscriptionAPIType;

@Producer(destination = "subscription_publish")
public class FullfillSubscriptionProducer {

    private static final Logger LOGGER = LoggerFactory.getLogger(FullfillSubscriptionProducer.class);

    private JmsTemplate jmsTemplate;

    @Autowired
    NevadoConnectionFactory connectionFactory;

    public void sendSubscriptionNotification(String schemaName, Subscriptions subscription) {
        SubscriptionsToSubscriptonAPIType converter = new SubscriptionsToSubscriptonAPIType();
        SubscriptionAPIType subApi = converter.convert(subscription);
        subApi.setSchemaName(schemaName);

        JAXBContext jaxbContext = null;
        try {
            jaxbContext = JAXBContext.newInstance(SubscriptionAPIType.class);
            ObjectFactory objectFactory = new ObjectFactory();
            JAXBElement<SubscriptionAPIType> subJaxb = objectFactory.createSubscriptionAPI(subApi);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            final StringWriter stringWriter = new StringWriter();
            jaxbMarshaller.marshal(subJaxb, stringWriter);

            LOGGER.info("Sending XML: " + stringWriter.toString());
            // Because the producer would not autowire into the job, this is done manually
            jmsTemplate = new JmsTemplate();
            NevadoQueue queue = new NevadoQueue(System.getProperty("aws.Environment") + "-subscription_publish_core");

            jmsTemplate.setDefaultDestination(queue);
            jmsTemplate.setConnectionFactory(connectionFactory);
            jmsTemplate.convertAndSend(stringWriter.toString());

        } catch (JAXBException e) {
            LOGGER.error("Error sending Subscription Notification: JAXBException:", e);
        }
    }

}
