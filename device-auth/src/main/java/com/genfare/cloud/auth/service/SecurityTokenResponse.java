package com.genfare.cloud.auth.service;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class SecurityTokenResponse {
    private String bucketName;
    private final Map<String, URL> urlMap = new HashMap<String, URL>();
    private String accessKeyId;
    private String secretAccessKey;
    private String sessionToken;
    private long expiration;
    private String message;
    private boolean success;
    private final Map<String, String> map = new HashMap<String, String>();

    public String getAccessKeyId() {
        return accessKeyId;
    }

    public void setAccessKeyId(String accessKeyId) {
        this.accessKeyId = accessKeyId;
    }

    public String getSecretAccessKey() {
        return secretAccessKey;
    }

    public void setSecretAccessKey(String secretAccessKey) {
        this.secretAccessKey = secretAccessKey;
    }

    public String getSessionToken() {
        return sessionToken;
    }

    public void setSessionToken(String sessionToken) {
        this.sessionToken = sessionToken;
    }

    public long getExpiration() {
        return expiration;
    }

    public void setExpiration(long expiration) {
        this.expiration = expiration;
    }

    public Map<String, URL> getUrlMap() {
        return urlMap;
    }

    public void setURL(String key, URL url) {
        urlMap.put(key, url);
    }

    public String getBucketName() {
        return bucketName;
    }

    public void setBucketName(String bucketName) {
        this.bucketName = bucketName;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public void map(String key, String value) {
        map.put(key, value);
    }

    public Map<String, String> getMap() {
        return map;
    }
}
