package com.genfare.cloud.auth.service;

import java.util.EnumSet;
import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.genfare.cloud.model.entity.Equipmenttypes;
import com.genfare.cloud.model.types.EquipmenttypesType;

public class DeviceAuthenticator {
    private static final Logger LOGGER = LoggerFactory.getLogger(DeviceAuthenticator.class);
    private static final Set<String> set = new HashSet<String>();

    static {
        for (EquipmenttypesType type : EnumSet.allOf(EquipmenttypesType.class)) {
            Equipmenttypes types = type.getType();
            String str = types.getDescription().toLowerCase();
            set.add(str);
            LOGGER.info("add: " + str);
        }
    }

    public static boolean isAuthorized(String deviceID) {
        boolean b = set.contains(deviceID.toLowerCase());
        LOGGER.info("DeviceAuthenticatorisAuthorized: " + deviceID + " " + b);
        return b;
    }
}
