package com.genfare.cloud.auth.service;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicSessionCredentials;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.amazonaws.services.securitytoken.AWSSecurityTokenServiceClient;
import com.amazonaws.services.securitytoken.model.Credentials;
import com.amazonaws.services.securitytoken.model.GetSessionTokenRequest;
import com.amazonaws.services.securitytoken.model.GetSessionTokenResult;

public class SecurityTokenService {
    private static String bucketName = "genfare-device-configuration";

    // public static void main(String[] argv) throws Exception {
    // SecurityTokenResponse resp = new SecurityTokenResponse();
    // String env = "dev";
    // String tenant = "cdta";
    // String type = "apos";
    // SecurityTokenService.getToken(resp, env, tenant, type);
    // System.out.println("resp=" + resp);
    // }

    public static void getToken(SecurityTokenResponse resp, String env, String tenant, String type, Set<String> locationSet) throws Exception {
        String ID = new String(env + "/" + tenant + "/" + type + "/").toLowerCase();
        ProfileCredentialsProvider profileCredentialsProvider = new ProfileCredentialsProvider();
        AWSSecurityTokenServiceClient stsClient = new AWSSecurityTokenServiceClient(profileCredentialsProvider);
        GetSessionTokenRequest getSessionTokenRequest = new GetSessionTokenRequest();
        GetSessionTokenResult sessionTokenResult = stsClient.getSessionToken(getSessionTokenRequest);
        Credentials sessionCredentials = sessionTokenResult.getCredentials();

        String accessKeyId = sessionCredentials.getAccessKeyId();
        String secretAccessKey = sessionCredentials.getSecretAccessKey();
        String sessionToken = sessionCredentials.getSessionToken();
        long expiration = sessionCredentials.getExpiration().getTime();

        resp.setBucketName(bucketName);
        resp.setAccessKeyId(accessKeyId);
        resp.setExpiration(expiration);
        resp.setSecretAccessKey(secretAccessKey);
        resp.setSessionToken(sessionToken);

        AWSCredentials basicSessionCredentials = new BasicSessionCredentials(
            accessKeyId,
            secretAccessKey,
            sessionToken);

        AmazonS3Client s3client = new AmazonS3Client(basicSessionCredentials);

        ObjectListing objectList = s3client.listObjects(bucketName);
        List<S3ObjectSummary> summaryList = objectList.getObjectSummaries();

        for (S3ObjectSummary s : summaryList) {
            String summaryKey = s.getKey();
            if (!contains(summaryKey, ID, locationSet))
                continue;
            S3Object o = s3client.getObject(bucketName, summaryKey);
            String key = o.getKey();
            URL url = s3client.getUrl(bucketName, key);
            resp.setURL(key, url);
        }
    }

    private static boolean contains(String summaryKey, String ID, Set<String> locationSet) {
        String summaryKeyLowerCase = summaryKey.toLowerCase();
        List<String> locationList = new ArrayList<String>(locationSet);
        for (String location : locationList) {
            String locationLowerCase = location.toLowerCase();
            if (summaryKeyLowerCase.endsWith(locationLowerCase + "/"))
                return false;
            String id = new String(ID + location).toLowerCase();
            if (summaryKeyLowerCase.startsWith(id))
                return true;
        }
        return false;
    }

    public static void getToken(SecurityTokenResponse resp) throws Exception {
        ProfileCredentialsProvider profileCredentialsProvider = new ProfileCredentialsProvider();
        AWSSecurityTokenServiceClient stsClient = new AWSSecurityTokenServiceClient(profileCredentialsProvider);
        GetSessionTokenRequest getSessionTokenRequest = new GetSessionTokenRequest();
        GetSessionTokenResult sessionTokenResult = stsClient.getSessionToken(getSessionTokenRequest);
        Credentials sessionCredentials = sessionTokenResult.getCredentials();

        String accessKeyId = sessionCredentials.getAccessKeyId();
        String secretAccessKey = sessionCredentials.getSecretAccessKey();
        String sessionToken = sessionCredentials.getSessionToken();
        long expiration = sessionCredentials.getExpiration().getTime();

        resp.setBucketName(bucketName);
        resp.setAccessKeyId(accessKeyId);
        resp.setExpiration(expiration);
        resp.setSecretAccessKey(secretAccessKey);
        resp.setSessionToken(sessionToken);

        AWSCredentials basicSessionCredentials = new BasicSessionCredentials(
            accessKeyId,
            secretAccessKey,
            sessionToken);

        AmazonS3Client s3client = new AmazonS3Client(basicSessionCredentials);

        ObjectListing objectList = s3client.listObjects(bucketName);
        List<S3ObjectSummary> summaryList = objectList.getObjectSummaries();

        for (S3ObjectSummary s : summaryList) {
            String summaryKey = s.getKey();
            S3Object o = s3client.getObject(bucketName, summaryKey);
            String key = o.getKey();
            URL url = s3client.getUrl(bucketName, key);
            resp.setURL(key, url);
        }
    }
}
