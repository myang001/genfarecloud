package com.genfare.cloud.auth.service;

//public class SecurityTokenController {
import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.genfare.cloud.model.dto.JSONWrapper;
import com.genfare.cloud.model.entity.Equipment;
import com.genfare.cloud.model.entity.Equipmenttypes;
import com.genfare.cloud.model.entity.Locations;
import com.genfare.cloud.model.entity.repository.EquipmentRepository;

@Controller
@RequestMapping(value = "/token")
public class SecurityTokenController {
    private static final Logger LOGGER = LoggerFactory.getLogger(SecurityTokenController.class);
    private String env = "dev";

    @Autowired
    EquipmentRepository equipmentRepository;

    @RequestMapping(value = "/get", method = RequestMethod.GET)
    @ResponseBody
    public JSONWrapper<SecurityTokenResponse> getToken(@RequestParam("deviceID") String deviceID, @RequestParam("tenant") String tenant, @RequestParam("type") String type) {

        SecurityTokenResponse resp = new SecurityTokenResponse();

        if (!DeviceAuthenticator.isAuthorized(type)) {
            resp.setSuccess(false);
            resp.setMessage("deviceID: " + deviceID + " is not authorized");
        } else {
            try {
                Set<String> locationSet = getLocations(deviceID, type);
                SecurityTokenService.getToken(resp, env, tenant, type, locationSet);
                resp.setSuccess(true);
                resp.setMessage("OK");
            } catch (Exception e) {
                resp.setSuccess(false);
                resp.setMessage("" + e);
            }
        }
        return new JSONWrapper<SecurityTokenResponse>(resp, "success");
    }

    private Set<String> getLocations(String deviceID, String type) {
        Set<String> set = new HashSet<String>();
        for (Equipment equipment : equipmentRepository.findAll()) {
            if (!deviceID.equalsIgnoreCase(equipment.getDescription()))
                continue;
            Equipmenttypes types = equipment.getEquipmenttypes();
            if (!type.equalsIgnoreCase(types.getDescription()))
                continue;
            Locations location = equipment.getLocations();
            set.add(location.getDescription());
        }
        return set;
    }

    @RequestMapping(value = "/key", method = RequestMethod.GET)
    @ResponseBody
    public JSONWrapper<SecurityTokenResponse> getKey() {

        SecurityTokenResponse resp = new SecurityTokenResponse();

        try {
            SecurityTokenService.getToken(resp);
            resp.setSuccess(true);
            resp.setMessage("OK");
        } catch (Exception e) {
            resp.setSuccess(false);
            resp.setMessage("" + e);
        }
        return new JSONWrapper<SecurityTokenResponse>(resp, "success");
    }
}