package com.genfare.cloud.csr.controller;

import java.security.Principal;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/")
public class RootController {

    @RequestMapping(value = { "", "/login" }, method = RequestMethod.GET)
    public String get(Principal principal) {
        return (principal == null) ? "basic/login" : "root/main";
    }
}
