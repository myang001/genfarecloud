package com.genfare.cloud.csr.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import com.genfare.cloud.config.mvc.AbstractBaseWebMvcConfigurerAdapter;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = { "com.genfare.cloud.resources", "com.genfare.cloud.csr", "com.genfare.cloud.model" })
@EnableTransactionManagement
public class WebMvcConfig extends AbstractBaseWebMvcConfigurerAdapter {

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/login").setViewName("root/login");
    }

}
