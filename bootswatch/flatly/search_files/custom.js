
jQuery(function($) {
	$('.well-height').responsiveEqualHeightGrid();
	$('.panel-height').responsiveEqualHeightGrid();
	$('.alert-signin').bind('close', function () {
		$('.panel-height').responsiveEqualHeightGrid();
	});
});

$('#tabAll').click(function(){
	  $('#tabAll').addClass('active');
	  $('.tab-pane').each(function(i,t){
	    $('#myTabs li').removeClass('active');
	    $('#myTabs li').removeClass('in');
	  	$(this).addClass('active');
	  	$(this).addClass('in');
	  });
	});

$('[data-role="filter"]').click(function() {
	var type = $(this).text();
    $('[data-filterable]:not([data-filterable="' + type + '"])').hide();
    $('[data-filterable*="' + type + '"]').show();
       
    $(".tab-pane").each(function() {
    	var countActiveProduct = $( this ).find( "[data-filterable]:not([style='display: none;'])" ).length;
    	var tabId = $( this ).attr('id');
    	if(countActiveProduct == 0){
    		//Thats mean that the tap is empty 
    		$(".nav-tabs").find("[href$='" + tabId + "']").hide();
    		/*This code is for disable the tab instead of remove it.
    		$(".nav-tabs").find("[href$='" + tabId + "']").parent().addClass( "disabled" );
    		$(".nav-tabs").find("[href$='" + tabId + "']").parent().click(function(event){
    	        if ($(this).hasClass('disabled')) { return false; }
    	        return true;
    	    });
    	    */
    	}else{
    		$(".nav-tabs").find("[href$='" + tabId + "']").show();
    		/*This code is for disable the tab instead of remove it.
    		$(".nav-tabs").find("[href$='" + tabId + "']").parent().removeClass( "disabled" );
    		*/
    	}
    });
   
});


