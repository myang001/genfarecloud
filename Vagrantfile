# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.require_version ">= 1.6.3"

%x(vagrant plugin install vagrant-vbguest) unless Vagrant.has_plugin?('vagrant-vbguest')
%x(vagrant plugin install vagrant-aws) unless Vagrant.has_plugin?('vagrant-aws')
%x(vagrant plugin install vagrant-cachier) unless Vagrant.has_plugin?('vagrant-cachier')
%x(vagrant plugin install vagrant-rsync) unless Vagrant.has_plugin?('vagrant-rsync')


# Vagrantfile API/syntax version. Don't touch unless you know what you're doing!
VAGRANTFILE_API_VERSION = "2"

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
  config.vm.hostname = "gfcp-local.gfcp.io"
  config.vm.box = "centos-6.5"
  config.vm.box_url = "http://puppet-vagrant-boxes.puppetlabs.com/centos-65-x64-virtualbox-puppet.box"


  if Vagrant.has_plugin?("vagrant-cachier")
    config.cache.scope = :box
  end

  config.vm.network "private_network", ip: "172.16.1.2"

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine.
  config.vm.network :forwarded_port, guest: 6666, host: 6666
  config.vm.network :forwarded_port, guest: 8080, host: 8080
  config.vm.network :forwarded_port, guest: 8787, host: 8787
  config.vm.network :forwarded_port, guest: 9990, host: 9990
  config.vm.network :forwarded_port, guest: 9999, host: 9999
  config.vm.network :forwarded_port, guest: 3306, host: 3306

  # If true, then any SSH connections made will enable agent forwarding.
  config.ssh.forward_agent = true

  config.vm.synced_folder "vagrant/cache/download", "/tmp/download-cache", :create => true
  config.vm.synced_folder File.expand_path("~/.m2/repository"), "/tmp/maven-cache", :create => true

  config.vm.provider :virtualbox do |vb|
     # Don't boot with headless mode
     #vb.gui = true

     #Get our host OS
     host = RbConfig::CONFIG['host_os']
     #Attempt to give all CPU Cores and 1/4 of System Memory
     if host =~ /darwin/
       #Mac Users
       cpus = `sysctl -n hw.ncpu`.to_i
       # Convert B to MB, then take 1/4
       mem = `sysctl -n hw.memsize`.to_i / 1024 / 1024 / 4
     elsif host =~ /linux/
       #Linux Users
       cpus = `nproc`.to_i
       # Convert kB to MB, then take 1/4
       mem = `grep 'MemTotal' /proc/meminfo | sed -e 's/MemTotal://' -e 's/ kB//'`.to_i / 1024 / 4
     else
       #Windows Users
       cpus = `echo %NUMBER_OF_PROCESSORS%`.to_i
       # Convert B to MB, then take 1/4
       mem = `wmic computersystem get TotalPhysicalMemory|findstr /R "^[0-9]"`.to_i / 1024 / 1024 / 4
     end
     vb.customize ["modifyvm", :id, "--memory", mem]
     vb.customize ["modifyvm", :id, "--cpus", cpus]
     vb.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
   end

   config.vm.provider :aws do |aws, override|
       override.vm.box = "dummy"
       aws.name = "gfcp-jboss-local-#{ENV['USER']}"
       aws.access_key_id = ENV['AWS_ACCESS_KEY_ID']
       aws.secret_access_key = ENV['AWS_SECRET_ACCESS_KEY']
       aws.keypair_name = ENV['USER']
       # CentOS 6 from https://aws.amazon.com/marketplace/pp/B00A6KUVBW
       # For some images you need to manually comment out the sudoers "Defaults requiretty" line with visudo
       aws.ami = "ami-eb6b0182"
       aws.instance_type = "m1.medium"
       aws.region = "us-east-1"
       aws.security_groups = ["gfcp-jboss-local"]
       aws.tags = {"vagrant" => true, "user" => ENV['USER'], "Environment" => "local", "Type" => "localnode"}
       override.ssh.username = "root"
       override.ssh.private_key_path = File.expand_path("~/.ssh/id_rsa")
   end

  config.vm.provision :puppet do |puppet|
    puppet.manifests_path = "vagrant/manifests"
    puppet.manifest_file  = "site.pp"
    puppet.module_path   = "vagrant/modules"
    puppet.hiera_config_path = "vagrant/hiera.yml"
    puppet.working_directory = "/vagrant/vagrant"
    puppet.options = ""
  end

  config.vm.synced_folder "deployments", "/tmp/deployments", :create => true, :mount_options => ["dmode=777","fmode=777"]

end
