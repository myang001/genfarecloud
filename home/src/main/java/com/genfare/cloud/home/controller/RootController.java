package com.genfare.cloud.home.controller;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.genfare.cloud.model.entity.repository.ModulesRepository;
import com.genfare.cloud.model.service.GroupPermissionsService;

@Controller
@RequestMapping("/")
public class RootController {

    @Autowired
    private ModulesRepository modulesRepository;

    @Autowired
    private GroupPermissionsService groupPermissionsService;

    @RequestMapping(value = { "", "/login" }, method = RequestMethod.GET)
    public String get(ModelMap model, Principal principal) {
        if (principal == null) {
            return "basic/login";
        }
        model.addAttribute("groupPermissionsService", groupPermissionsService);
        model.addAttribute("modules", modulesRepository.findAll());
        return "root/main";
    }
}
