package com.genfare.cloud.home.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.servlet.configuration.EnableWebMvcSecurity;

import com.genfare.cloud.config.security.AbstractBaseWebSecurityConfigurerAdapter;

@Configuration
@EnableWebMvcSecurity
public class SecurityConfig extends AbstractBaseWebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // @formatter:off
        super.configure(http);
        http
            .authorizeRequests()
            .anyRequest().permitAll()
            .and().csrf()
            .and().formLogin().defaultSuccessUrl("/");
        enableSSO(http);
        // @formatter:on
    }
}
