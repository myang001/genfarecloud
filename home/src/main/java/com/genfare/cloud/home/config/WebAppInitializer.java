package com.genfare.cloud.home.config;

import org.springframework.core.annotation.Order;

import com.genfare.cloud.config.mvc.AbstractBaseWebAppInitializer;

@Order(2)
public class WebAppInitializer extends AbstractBaseWebAppInitializer {

    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class<?>[] { WebMvcConfig.class };
    }

    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class<?>[] { WebMvcConfig.class };
    }

}
