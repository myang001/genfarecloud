package com.genfare.cloud.usermanagement.system.pagemodels;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class LoginPage {

    @FindBy(id = "username")
    public WebElement usernameInput;

    @FindBy(id = "password")
    public WebElement passwordInput;

    @FindBy(name = "submit")
    public WebElement submitButton;

    @FindBy(id = "logout")
    public WebElement logoutLink;

    @Autowired
    WebDriver driver;

    public LoginPage goToPage() {
        driver.get("http://localhost:8080/usermanagement");
        return this;
    }

    public LoginPage pageIsLoaded() {
        PageFactory.initElements(driver, this);
        return this;
    }

    public LoginPage login(String username, String password) {
        usernameInput.sendKeys(username);
        passwordInput.sendKeys(password);
        submitButton.click();
        return this;
    }

    public LoginPage isLoggedIn(boolean expected) {
        boolean actual;
        try {
            driver.findElement(By.id("logout"));
            actual = true;
        } catch (NoSuchElementException e) {
            actual = false;
        }
        Assert.assertEquals(expected, actual);
        return this;
    }

}
