package com.genfare.cloud.usermanagement.system.steps;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;

import com.genfare.cloud.usermanagement.system.pagemodels.LoginPage;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

@WebAppConfiguration
@ContextConfiguration("classpath:/cucumber.xml")
public class LoginSteps {

    private static final Logger LOGGER = LoggerFactory.getLogger(LoginSteps.class);

    @Autowired
    LoginPage loginPage;

    @Given("^I navigate to the application$")
    public void given_I_navigate_to_the_application() {
        LOGGER.info("Entering: I navigate to the application");
        loginPage.goToPage().pageIsLoaded();
    }

    @When("^I try to login with valid credentials$")
    public void when_I_try_to_login_valid() {
        LOGGER.info("Entering: I try to login with valid credentials");
        loginPage.login("user_admin@someemail.com", "Password1!");
    }

    @When("^I try to login with invalid credentials$")
    public void when_I_try_to_login_invalid() {
        LOGGER.info("Entering: I try to login with invalid credentials");
        loginPage.login("user", "password");
    }

    @Then("^I should see that I logged in successfully$")
    public void then_I_login_successfully() {
        LOGGER.info("Entering: I should see that I logged in successfully");
        loginPage.isLoggedIn(true);
    }

    @Then("^I should see that I logged in unsuccessfully$")
    public void then_I_login_unsuccessfully() {
        LOGGER.info("Entering: I should see that I logged in unsuccessfully");
        loginPage.isLoggedIn(false);
    }
}
