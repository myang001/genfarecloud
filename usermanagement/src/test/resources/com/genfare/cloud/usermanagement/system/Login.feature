#encoding: utf-8
@login @all
Feature: Users can login successfully
 
Scenario: Login Success 
	Given I navigate to the application
	When I try to login with valid credentials
	Then I should see that I logged in successfully

Scenario: Login Failure
	Given I navigate to the application
	When I try to login with invalid credentials
	Then I should see that I logged in unsuccessfully