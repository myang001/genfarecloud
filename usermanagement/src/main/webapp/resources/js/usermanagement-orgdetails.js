$(function() {
	$('#org-details-tabs a').click(function (e) {
		  e.preventDefault();
		  $(this).tab('show');
	});

	function transform(data) {
		var result = [];

		for(var i=0,l=data.length;i<l;++i) {
			
			var phone = data[i].phone;
			result.push([
		             data[i].employeeId,
		             data[i].nameFirst + ' ' + data[i].nameLast,
		             '', //TODO: Get Job Title
		             data[i].email,
		             '(' + phone.slice(0,3) + ') ' + phone.slice(3,6) + '-' + phone.slice(6),
		             data[i].status,
		             '', //TODO: Get Available Actions
		             '<a href="' + extract_url("/user?uid=" + data[i].id) + '">View</a>'
		    ]);
		}
			
		return result;
	};
	
	
	$('#search-results').DataTable({
		bFilter : false,
		sDom: '<"top"i>rt<"bottom"lp><"clear">',
		aoColumnDefs: [{
			bSortable: false,
			aTargets: [6,7]
		}],
		data: transform(userdata)
	});
	
	GroupsModulesComponents.init();
});