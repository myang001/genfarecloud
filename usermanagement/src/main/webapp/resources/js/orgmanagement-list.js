$(function() {
	SearchManager.init({
			bFilter : false,
			sDom: '<"top"i>rt<"bottom"lp><"clear">',
			aoColumnDefs: [{
				bSortable: false,
				aTargets: [6,7]
			}],
		},
		extract_url('/rest/organizations/search'),
		function(data) {
			var result = [];
			var dataLen = data.value.length;

			if(data.status === 'success' && dataLen > 0) {
				for(var i=0,l=dataLen;i<l;++i) {
					
					result.push([
					    data.value[i].name,
					    data.value[i].id,
					    data.value[i].mainContact,
					    data.value[i].phone,
					    data.value[i].creditLimit,
					    data.value[i].status,
					    '', //TODO: Get Available Actions
					    '<a href="' + extract_url("/organizations/details?oid=" + data.value[i].id) + '">View</a>'
					]);
				}
			}
					
			return result;
		}
	);
});