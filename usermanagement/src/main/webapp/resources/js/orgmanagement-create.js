$(function() {
	var ps = $('#pageslider').pageslider();

	function indicate(n) {
		$('#indicator-panel > .indicator-selected').removeClass('indicator-selected');
		$('#indicator-panel > .indicator:eq(' + n + ')').addClass('indicator-selected');
	}

	$('#fc-backbutton').click(function(e) {
		e.preventDefault();
		indicate(ps.pageslider('prev'));
		$('#fc-nextbutton').text('Next');
	});

	$('#fc-nextbutton').click(function(e) {
		e.preventDefault();
		if (!validate.validate()) {
			return false;
		}
		var l = ps.pageslider('length');
		var n = ps.pageslider('current');

		if (n === l - 2) {
			$(this).text('Activate Organization');
			$('#name-verify').text($('#name-input').val());
			$('#type-verify').text($('#type-input').val());
			$('#credit-verify').text($('#credit-input').val());

			if ($('input[name=parent]:checked').val() === 'Yes') {
				$('#parent-verify-wrap').show();
				$('#parent-verify').text($('#parent-input').val());
			} else {
				$('#parent-verify-wrap').hide();
				$('#parent-verify').text('');
			}
		} else {
			$('#orgForm').submit();
		}

		indicate(ps.pageslider('next'));
	});

	
	validate.register(validate_name);
	validate.register(validate_credit);
	validate.register(validate_parent);
	$('#name-input').blur(validate_name);
	$('#credit-input').blur(validate_credit);
	$('#parent-input').blur(validate_parent);
});