
var ModulesComponents = {
	init : function() {
		$('#modulesTabs a').click(function (e) {
		    e.preventDefault();
		    $(this).tab('show');
		 });
		
		$(".privilege-box").click(function() {
			var component = $(this).parent().parent();
			var list = component.find('.privilege-box');
			var priv_all = component.parent().find('.permissions-all');
			
			for(var i=0,l=list.length;i<l;++i) {
				if(!$(list[i]).prop('checked')) {
					priv_all.prop('checked',false);
					return;
				}
			}
			
			priv_all.prop('checked',true);
		});
		
		$('.permissions-all').click(function() {
			var $this = $(this);
			var checked = $this.prop('checked');
			var checks = $this.parent().parent().next().find('.privilege-box');

			for(var i=0,l=checks.length;i<l;++i) {
				$(checks[i]).prop('checked',checked);
			}
		});
	},
	getSelectedPermissions : function() {
		var out = {};
		$("input:checked.privilege-box").each(function() {
			var $this = $(this);
			var module = $this.data('module');
			var component = $this.data('component');
			
			if(out[module] === undefined) {
				out[module] = {};
			}
			
			if(out[module][component] === undefined) {
				out[module][component] = [];
			}
			out[module][component].push($this.attr('name'));
		});
		
		return out;
	},
	
	submitJSON : function() {
		$.ajax({
			url: window.location.origin + '/' + window.location.pathname.split('/')[1] + "/rest/groups/update",
			method: "POST",
			data: {
				_csrf: CSRF_TOKEN,
				group: $('#group-name').text(),
				privileges: JSON.stringify(ModulesComponents.getSelectedPermissions())
			},
			dataType: "json",
			success: function(response) {
				url = window.location.origin + '/' + window.location.pathname.split('/')[1] + "/groups";
				window.location = url;
			},
			error: function(response) {
				$('#save-errors').addClass("form-control-error");
				$('#save-errors').html("There was an error updating the group");
			}
		});
	}	
};


var GroupsModulesComponents = {
	init : function() {
		$('#groupTabs a').click(function (e) {
		    var $this = $(this);
		    
		    if($this.find('span.glyphicon').hasClass('glyphicon-ok-sign')) {
		    	$('#add-group > .add-group-button').hide();
		    	$('#add-group > .remove-group-button').show();
		    } else {
		    	$('#add-group > .add-group-button').show();
		    	$('#add-group > .remove-group-button').hide();
		    }
		    
		    $this.tab('show');
		    
		    e.preventDefault();
		});
		
		var details = $('.group-permissions').show();
		var descirptions = $('.group-details').hide();

		$('#details-toggle').click(function() {
			var $this = $(this);
			$this.addClass('bold');
			$this.next().removeClass('bold');
			details.show();
			descirptions.hide();				
		});

		$('#description-toggle').click(function() {
			var $this = $(this);
			$this.addClass('bold');
			$this.prev().removeClass('bold');
			details.hide();
			descirptions.show();
		});

		$('#add-group').click(function(e) {
			var $this = $(this);
			
			$this.children().toggle();
			$('#groupTabs > li.active').find('span.glyphicon')
				.toggleClass('glyphicon-ok-sign')
				.toggleClass('assignment-modified');
		});

		$('#reset-changes').click(function() {
			var modified_elem = $('.assignment-modified');
			for(var i=0,l=modified_elem.length;i<l;++i) {
				$(modified_elem[i])
					.toggleClass('glyphicon-ok-sign')
					.toggleClass('assignment-modified');
			}
			$('#groupTabs > .active > a').click();
		});

		$('.moduletabs a').click(function (e) {
		    e.preventDefault();
		    $(this).tab('show');
		 });
		
		$('#groupTabs a').first().click();
		$('#assignment-status').hide();
	},
	getModified : function () {
		var modified = {};
		var modified_elem = $('.assignment-modified');
		for(var i=0,l=modified_elem.length;i<l;++i) {
			var elem = $(modified_elem[i]);
			modified[elem.next().text()] = (elem.hasClass('glyphicon-ok-sign') ? 'added' : 'removed');
		}

		return modified;
	},

	getAssigned : function() {
		var assigned = [];
		var assigned_elem = $('.assignment-toggle.glyphicon-ok-sign');

		for(var i=0,l=assigned_elem.length;i<l;++i) {
			assigned.push($(assigned_elem[i]).next().text());
		}
		
		return assigned;
	},
	submitJSON : function() {

		var modified = GroupsModulesComponents.getModified();
		if(Object.keys(modified).length === 0) {
			return;
		}
		
		$.ajax({
			url: window.location.origin + '/' + window.location.pathname.split('/')[1] + "/rest/user/groups",
			method: "POST",
			data: {
				user: $('#user-email').text(),
				_csrf: CSRF_TOKEN,
				modified: JSON.stringify(modified)
			},
			dataType: "json",
			success: function(d) {
				$('.assignment-modified').removeClass('assignment-modified');
				$('#assignment-status')
					.addClass('label-success')
					.removeClass('label-danger')
					.text('Group Assignments Updated')
					.delay(250).fadeIn('normal', function() {
						$(this).delay(2500).fadeOut();
					});
			},
			error: function(d) {
				$('#assignment-status')
					.removeClass('label-success')
					.addClass('label-danger')
					.text('Failed to Apply Changes')
					.delay(250).fadeIn('normal', function() {
						$(this).delay(2500).fadeOut();
					});
			}
		});
	}
};

