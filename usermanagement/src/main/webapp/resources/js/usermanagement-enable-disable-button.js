var EnableDisableButton = {
	setStatus : function(status, action, buttonStyle, buttonId, listId) {
		$(buttonId).html(status + ' <span class="caret">');
		$(buttonId).addClass(buttonStyle);
		$(listId).empty();
		$(listId).append('<li><a id="status-action" href="#">' + action + '</a></li>');
	}
};