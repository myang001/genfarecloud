var validate = Validate();

function hide_show_pass_fail(didPass, passNode, failNode) {
	if (didPass == false) {
		failNode.show();
		passNode.hide();
	} else {
		failNode.hide();
		passNode.show();
	}

	return didPass;
}


function search_org_name(value) {
	var result = false;
	$.ajax({
		url : extract_url('/rest/organizations/exists'),
		method : 'GET',
		async : false,
		data : {
			name : value
		},
		dataType : 'json',
		success : function(d) {
			result = d.value;
		},
		error : function(d) {
			result = false;
		}
	});

	return result;
}

function validate_name() {
	var val = $('#name-input').val();
	return hide_show_pass_fail(val === '' ? false : search_org_name(val),$('#name-pass'), $('#name-fail'));
}


function validate_credit() {
	return hide_show_pass_fail($.isNumeric($('#credit-input').val()),$('#credit-pass'), $('#credit-fail'));
}


function validate_parent() {
	if ($('input[name=parent]:checked').val() === 'Yes') {
		var val = $('#parent-input').val();
		return hide_show_pass_fail(val === '' ? false : !search_org_name(val), $('#parent-pass'), $('#parent-fail'));
	}
	$('#parent-pass').hide();
	$('#parent-fail').hide();
	return true;
}

function verify_contact_name(value) {
	var result = false;
	if(value === '') {
		return true;
	}
	
	$.ajax({
		url : extract_url('/rest/organizations/people/exists'),
		method : 'GET',
		async : false,
		data : {
			name : value,
			orgid : $('#orgid').val()
		},
		dataType : 'json',
		success : function(d) {
			result = d.value;
		},
		error : function(d) {
			result = false;
		}
	});

	return result;
}

function validate_contact() {
	return hide_show_pass_fail(verify_contact_name($('#contact-input').val()),$('#contact-pass'), $('#contact-fail'));
}

$(function() {
	
	var pi = $('#parent-info');	
	
	$('#parent-input').autocomplete({
		minLength: 3,
		source: function(req,resp) {
			$.ajax({
				url: extract_url('/rest/organizations'),
				method: 'GET',
				dataType: 'json',
				data: {
					name : req.term
				},
				success: function(data) {
					resp(data.value);
				}
			});
		}
	});
	
	if($('input[name=parent]:checked').val() === 'No') {
		pi.hide();
	}
	
	$('#parent-yes').click(function() {
		pi.slideDown();
	});
	$('#parent-no').click(function() {
		pi.hide('slide', {
			direction : 'up'
		}, 400);
	});
	
	var ci = $('#contact-input');
	ci.blur(validate_contact);
	ci.autocomplete({
		minLength: 3,
		source: function(req,resp) {
			$.ajax({
				url: extract_url('/rest/organizations/people'),
				method: 'GET',
				dataType: 'json',
				data: {
					name : req.term,
					orgid : $('#orgid').val()
				},
				success: function(data) {
					resp(data.value);
				}
			});
		}
	});
	
});