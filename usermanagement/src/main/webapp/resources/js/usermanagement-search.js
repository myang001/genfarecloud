
var SearchManager = {
		
	init: function(dataTableConfig,searchUrl,transform) {
		var keypress_timeout = undefined;
		var xhr = undefined;
		
		$('#search-results').DataTable(dataTableConfig);
		
		$('#search').keydown(function(e) {
			
			clearTimeout(keypress_timeout);
				
			var code = e.keyCode || e.which;
			if(code == 13) {
				search();
				return;
			}
			
			if($(this).val().length >= 2) {
				keypress_timeout = setTimeout(search,500);
			}
		});
		
		var loading = $('#loading').hide();
		$('#search-button').click(search);
		
		function search() {
			loading.show();
				
			//Previous search hasn't finished and we are issuing a new search, abort previous
			if(xhr && xhr.readyState != 4) {
				xhr.abort();
			}
				
			var searchResults = $('#search-results').DataTable();
			var resultsbox = $('.results-box');
				
			xhr = $.ajax({
				url: searchUrl,
				type: 'GET',
				dataType: 'json',
				data: {
					q: $('#search').val(),
					s: $('#status-filter').val() || ''
				},
				success: function(d) {
						
					if(typeof transform === 'function') {
						d = transform(d);
					}
					
					resultsbox.show();
					searchResults.clear();
					searchResults.rows.add(d).draw();
					loading.hide();
				},
				error: function() {
					searchResults.clear();
					loading.hide();
				}
			});
		}
	}
};