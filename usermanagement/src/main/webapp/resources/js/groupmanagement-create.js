$(document).ready(function() {
	$('#rootwizard').bootstrapWizard({
		'tabClass' : 'nav nav-tabs',
		'onNext' : function(tab, navigation, index) {

		},
		'onTabShow' : function(tab, navigation, index) {
			var $total = navigation.find('li').length;
			var $current = index + 1;
			// If it's the last tab then hide the last button
			// and show the finish instead
			var node = $('#rootwizard').find('.next');
			if ($current >= $total) {
				node.hide()
		        .next().removeClass('disabled').show()
		        .next().removeClass('disabled').show();
				
				/* Filling last step fields */
				$('#lastStepTitle').html($('#groupName').val());
				$('#lastStepDescription').html($('#description').val());
				GroupCreate.summaryTable();
			} else {
				node.show()
				.next().hide()
				.next().show();
			}
		}
	});
	
	ModulesComponents.init();
});

var GroupCreate = {
	summaryTable : function() {
		var data = ModulesComponents.getSelectedPermissions();
		
		var tableHtml = "";

		$.each(data, function(module, components){
		    tableHtml += '<tr class="emphasis-row"><td>'+module+'</td></tr>';
		    $.each(components, function(component, privileges){
		        tableHtml += '<tr><td>'+component+'</td></tr>';
		        tableHtml += '<tr><td><ul class="list-inline">';
		        $.each(privileges, function(idx, privilege){
		           tableHtml += '<li><span class="glyphicon glyphicon-ok"></span>'+privilege+'</li>'; 
		        });        
		        tableHtml += '</ul></td></tr>';
		    });
		});

		$('#permission-summary').empty();
		$('#permission-summary').append(tableHtml);
	},
	
	submitJSON : function(name, description) {
		var assignments = {};
		assignments.name = name;
		assignments.description = description;
		assignments.privileges = ModulesComponents.getSelectedPermissions();
		
		$.ajax({
			url: window.location.origin + '/' + window.location.pathname.split('/')[1] + "/rest/groups/create",
			method: "POST",
			data: {
				_csrf: CSRF_TOKEN,
				group: JSON.stringify(assignments)
			},
			dataType: "json",
			success: function(response) {
				url = window.location.origin + '/' + window.location.pathname.split('/')[1] + "/groups";
				window.location = url;
			},
			error: function(response) {
				$('#save-errors').addClass("form-control-error");
				$('#save-errors').html("There was an error saving the group");
			}
		});
	}	
};

