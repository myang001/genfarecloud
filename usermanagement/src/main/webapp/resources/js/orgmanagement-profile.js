$(function() {
	validate.register(validate_credit);
	validate.register(validate_parent);
	validate.register(validate_contact);
	$('#credit-input').blur(validate_credit);
	$('#parent-input').blur(validate_parent);
	$('#contact-input').blur(validate_contact);
	
	$('#fc-nextbutton').click(function(e) {
		e.preventDefault();
		
		if(validate.validate()) {
			$('#orgForm').submit();
		}
		return false;
	});
});