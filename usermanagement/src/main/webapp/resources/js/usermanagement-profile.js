$(document).ready(function() {	
	$('#jobTitle').autocomplete({
		minLength: 3,
		source: function(req,resp) {
			$.ajax({
				url: extract_url('/rest/user/job'),
				method: 'GET',
				dataType: 'json',
				data: {
					name : req.term
				},
				success: function(data) {
					resp(data.value);
				}
			});
		}
	});
	
});

