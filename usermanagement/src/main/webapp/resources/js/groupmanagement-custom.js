$(function() {
	function init(){
		$('#usersForGroup').DataTable({
			sDom: '<"top"i>rt<"bottom"lp><"clear">',
			aoColumnDefs: [{
				bSortable: false,
				aTargets: [5]
			}],
		});
	}
	window.onload = init;
		
	$('.plus-minus-trigger').click(function(e) {
			var personId = $(this).attr('id').slice(3);
			var row = $('#row' + personId);
			var button = $('#button' + personId);
			var input = $('#input' + personId);
			row.toggleClass('strikethru');
			button.toggleClass('glyphicon-minus-sign');
			button.toggleClass('glyphicon-plus-sign');
			if(input.attr('name')===('default'+personId)){
				input.attr('name', 'delete'+personId);
			}
			else {
				input.attr('name', 'default'+personId);
			}
		});

});
