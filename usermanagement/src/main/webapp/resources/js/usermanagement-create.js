
$(document).ready(function() {
	$('#rootwizard').bootstrapWizard({
		'tabClass' : 'nav nav-pills',
		'onTabShow' : function(tab, navigation, index) {
			var $total = navigation.find('li').length;
			var $current = index + 1;
			// If it's the last tab then hide the last button
			// and show the finish instead
			var node = $('#rootwizard').find('.next');
			if ($current >= $total) {
				node.hide()
		        .next().removeClass('disabled').show()
		        .next().removeClass('disabled').show();
				
				/* Filling last step fields */
				$('#verifyAgency').html($('#agency').val());
				$('#verifyFirstName').html($('#firstName').val());
				$('#verifyLastName').html($('#lastName').val());
				$('#verifyEmail').html($('#email').val());
				$('#verifyJobTitle').html($('#jobTitle').val());
				$('#verifyEmployeeId').html($('#employeeId').val());
				$('#verifyPhoneNumber').html($('#phoneNumber').val());
				$('#verifyMobilePhoneNumber').html($('#mobilePhoneNumber').val());			
				
			} else {
				node.show()
				.next().hide()
				.next().show();
			}
		}
	});
	
	$('#jobTitle').autocomplete({
		minLength: 3,
		source: function(req,resp) {
			$.ajax({
				url: extract_url('/rest/user/job'),
				method: 'GET',
				dataType: 'json',
				data: {
					name : req.term
				},
				success: function(data) {
					resp(data.value);
				}
			});
		}
	});
	
});

function submitForm(){
    var modified=GroupsModulesComponents.getAssigned();
    $('#groupsList').val(modified);
    document.getElementById('createUserForm').submit();
}