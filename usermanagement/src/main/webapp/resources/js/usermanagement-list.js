$(function() {
	SearchManager.init({
			bFilter : false,
			sDom: '<"top"i>rt<"bottom"lp><"clear">',
			aoColumnDefs: [{
				bSortable: false,
				aTargets: [6,7]
			}],
		},
		extract_url('/rest/user/search'),
		function(data) {
			var result = [];
			var dataLen = data.value.length;

			
			if(data.status === 'success' && dataLen > 0) {
				for(var i=0,l=dataLen;i<l;++i) {
					
					var phone = data.value[i].phone;
					result.push([
					    data.value[i].employeeId,
					    data.value[i].nameFirst + ' ' + data.value[i].nameLast,
					    '', //TODO: Get Job Title
					    data.value[i].email,
					    '(' + phone.slice(0,3) + ') ' + phone.slice(3,6) + '-' + phone.slice(6),
					    data.value[i].status,
					    '', //TODO: Get Available Actions
					    '<a href="' + extract_url("/user?uid=" + data.value[i].id) + '">View</a>'
					]);
				}
			}
					
			return result;
		}
	);
});