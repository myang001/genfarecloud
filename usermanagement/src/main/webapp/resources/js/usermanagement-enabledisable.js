var EnableDisableUser = {
		toggle : function () {
			 $.ajax({
			 	url: window.location.origin + '/' + window.location.pathname.split('/')[1] + "/rest/user/enable_disable",
			 	method: "POST",
			 	data: {
			 		_csrf: CSRF_TOKEN,
			 		uid: userId
			 	},
			 	dataType: "json",
			 	success: function(response) {
			 		console.log("User status successfully changed");
			 		location.reload();
			 	},
			 	error: function(response) {
			 		console.log("User status failed to update")
			 	}
			 });
		}
}