package com.genfare.cloud.usermanagement.config;

import org.springframework.core.annotation.Order;

import com.genfare.cloud.config.mvc.AbstractBaseWebAppInitializer;

@Order(2)
public class WebAppInitializer extends AbstractBaseWebAppInitializer {

    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class<?>[] { WebMvcConfig.class, SecurityConfig.class };
    }

    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class<?>[] { WebMvcConfig.class };
    }

}
