package com.genfare.cloud.usermanagement.constraints;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

import com.genfare.cloud.model.entity.repository.OrganizationsRepository;
import com.genfare.cloud.usermanagement.annotation.OrgAvailable;

public class OrgAvailableValidator
    implements ConstraintValidator<OrgAvailable, String> {

    @Autowired
    OrganizationsRepository orgRepo;

    @Override
    public void initialize(OrgAvailable constraintAnnotation) {

    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {

        if (value == null || value.isEmpty()) {
            return true;
        }

        return ((orgRepo.findByName(value)) == null);
    }

}
