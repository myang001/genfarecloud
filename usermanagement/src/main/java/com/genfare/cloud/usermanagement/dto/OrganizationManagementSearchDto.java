package com.genfare.cloud.usermanagement.dto;

import java.math.BigDecimal;

import com.genfare.cloud.model.entity.Organizationlinks;
import com.genfare.cloud.model.entity.Organizations;
import com.genfare.cloud.model.entity.People;

public class OrganizationManagementSearchDto {

    private String name;
    private String id;
    private String mainContact;
    private String phone;
    private String creditLimit;
    private String status;

    public OrganizationManagementSearchDto(Organizations org) {

        this.name = org.getName();
        this.id = org.getOrganizationId().toString();

        this.mainContact = "";
        People mainContact = org.getMainContact();
        if (mainContact != null) {
            this.mainContact = String.format("%s %s", mainContact.getNameFirst(), mainContact.getNameLast());
        }

        Iterable<Organizationlinks> links = org.getOrganizationlinkses();
        this.phone = "";
        for (Organizationlinks ol : links) {
            if (ol.getLinktypes().getLinkTypeId().equals(2)) {
                this.phone = ol.getLinkValue();
                break;
            }
        }

        this.creditLimit = org.getCreditLimit().toString();
        this.status = org.getStatus().getDescription();
    }

    public OrganizationManagementSearchDto(String name, Integer id, People mainContact, String phone, BigDecimal creditLimit, String status) {
        this.name = name;
        this.id = id.toString();

        if (mainContact != null) {
            this.mainContact = String.format("%s %s", mainContact.getNameFirst(), mainContact.getNameLast());
        }

        this.phone = phone;
        this.creditLimit = creditLimit.toString();
        this.status = status;
    }

    public OrganizationManagementSearchDto(String name, Integer id, String mainContactFirstName, String mainContactLastName, String phone, BigDecimal creditLimit, String status) {
        this.name = name;
        this.id = id.toString();
        this.mainContact = mainContactFirstName + " " + mainContactLastName;
        this.phone = phone;
        this.creditLimit = creditLimit.toString();
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMainContact() {
        return mainContact;
    }

    public void setMainContact(String mainContact) {
        this.mainContact = mainContact;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCreditLimit() {
        return creditLimit;
    }

    public void setCreditLimit(String creditLimit) {
        this.creditLimit = creditLimit;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
            + ((creditLimit == null) ? 0 : creditLimit.hashCode());
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result
            + ((mainContact == null) ? 0 : mainContact.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((phone == null) ? 0 : phone.hashCode());
        result = prime * result + ((status == null) ? 0 : status.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        OrganizationManagementSearchDto other = (OrganizationManagementSearchDto) obj;
        if (creditLimit == null) {
            if (other.creditLimit != null)
                return false;
        } else if (!creditLimit.equals(other.creditLimit))
            return false;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        if (mainContact == null) {
            if (other.mainContact != null)
                return false;
        } else if (!mainContact.equals(other.mainContact))
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        if (phone == null) {
            if (other.phone != null)
                return false;
        } else if (!phone.equals(other.phone))
            return false;
        if (status == null) {
            if (other.status != null)
                return false;
        } else if (!status.equals(other.status))
            return false;
        return true;
    }
}
