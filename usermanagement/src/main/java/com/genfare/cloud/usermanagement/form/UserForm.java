package com.genfare.cloud.usermanagement.form;

import java.util.LinkedHashSet;
import java.util.Set;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import com.genfare.cloud.model.contact.Address;
import com.genfare.cloud.model.contact.Email;
import com.genfare.cloud.model.contact.Phone;
import com.genfare.cloud.model.entity.Groups;
import com.genfare.cloud.model.security.User;
import com.genfare.cloud.model.security.User.UserBuilder;

public class UserForm {

    private Integer userId;

    @NotBlank
    private String agency;

    @Length(max = 4, message = "{usermanagement.userform.validation.prefix.length}")
    private String prefix;

    @NotBlank(message = "{usermanagement.userform.validation.firstName.blank}")
    @Length(max = 256, message = "{usermanagement.userform.validation.firstName.length}")
    private String firstName;

    @Pattern(regexp = "^[A-Za-z]*$", message = "{usermanagement.userform.validation.middleInitial.pattern}")
    @Length(max = 1, message = "{usermanagement.userform.validation.middleInitial.length}")
    private String middleInitial;

    @NotBlank(message = "{usermanagement.userform.validation.lastName.blank}")
    @Length(max = 256, message = "{usermanagement.userform.validation.lastName.length}")
    private String lastName;

    @Valid
    // @EmailAvailable(message = "{register.email.exists}")
    private Email email;

    private String jobTitle;
    private String employeeID;

    @Valid
    @NotNull
    private Phone phoneNumber;

    @Valid
    @NotNull
    private Phone mobileNumber;

    private Set<Groups> groups;

    private String groupsList;

    public UserForm() {
        this.email = new Email();
        this.phoneNumber = new Phone();
        this.mobileNumber = new Phone();
        this.groups = new LinkedHashSet<Groups>();
    }

    public UserForm(User user) {
        this.userId = user.getId();
        this.prefix = user.getPrefix();
        this.firstName = user.getFirstName();

        if (user.getMiddleName() != null && !user.getMiddleName().isEmpty()) {
            this.middleInitial = user.getMiddleName().substring(0, 1);
        }

        this.lastName = user.getLastName();
        this.jobTitle = user.getJobTitle();
        this.employeeID = user.getEmployeeId();

        this.email = user.getEmail();
        this.phoneNumber = user.getPhone();

        // TODO: Set these fields correctly
        this.agency = "A agency";
    }

    public User updateUser(User user) {

        UserBuilder builder = null;

        if (user != null) {
            builder = new UserBuilder(
                user,
                this.email.getAddress(),
                this.firstName,
                this.lastName,
                this.email,
                this.phoneNumber,
                user.getAddress()
                );
        } else {
            Integer nullId = null;
            builder = new UserBuilder(
                nullId,
                this.email.getAddress(),
                this.firstName,
                this.lastName,
                this.email,
                this.phoneNumber,
                new Address()
                );
        }

        builder
            .prefix(this.prefix)
            .middleName(this.middleInitial)
            .employeeId(this.employeeID)
            .jobTitle(this.jobTitle);

        return builder.build();
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getAgency() {
        return agency;
    }

    public void setAgency(String agency) {
        this.agency = agency;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleInitial() {
        return middleInitial;
    }

    public void setMiddleInitial(String middleInitial) {
        this.middleInitial = middleInitial;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Email getEmail() {
        return email;
    }

    public void setEmail(Email email) {
        this.email = email;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getEmployeeID() {
        return employeeID;
    }

    public void setEmployeeID(String employeeID) {
        this.employeeID = employeeID;
    }

    public Phone getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(Phone phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Phone getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(Phone mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public Set<Groups> getGroups() {
        return groups;
    }

    public void setGroups(Set<Groups> groups) {
        this.groups = groups;
    }

    public String getGroupsList() {
        return groupsList;
    }

    public void setGroupsList(String groupsList) {
        this.groupsList = groupsList;
    }

}
