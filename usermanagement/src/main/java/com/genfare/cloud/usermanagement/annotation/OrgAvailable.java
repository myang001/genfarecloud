package com.genfare.cloud.usermanagement.annotation;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.genfare.cloud.usermanagement.constraints.OrgAvailableValidator;

@Target({ FIELD, ANNOTATION_TYPE })
@Retention(RUNTIME)
@Constraint(validatedBy = OrgAvailableValidator.class)
public @interface OrgAvailable {

    String message() default "Org exists";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
