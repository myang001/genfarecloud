package com.genfare.cloud.usermanagement.controller;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.genfare.cloud.model.converters.PeopleToUserConverter;
import com.genfare.cloud.model.dto.JSONWrapper;
import com.genfare.cloud.model.entity.Groups;
import com.genfare.cloud.model.entity.Organizations;
import com.genfare.cloud.model.entity.People;
import com.genfare.cloud.model.entity.repository.GroupsRepository;
import com.genfare.cloud.model.entity.repository.JobTitleRepository;
import com.genfare.cloud.model.entity.repository.OrganizationsRepository;
import com.genfare.cloud.model.entity.repository.PeopleRepository;
import com.genfare.cloud.model.security.User;
import com.genfare.cloud.model.security.User.UserBuilder;
import com.genfare.cloud.model.service.GroupPermissionsService;
import com.genfare.cloud.model.service.GroupsManagementService;
import com.genfare.cloud.model.service.OrganizationManagementService;
import com.genfare.cloud.model.service.PrivilegesManagementService;
import com.genfare.cloud.model.service.UserManagementService;
import com.genfare.cloud.model.types.GroupStatusType;
import com.genfare.cloud.model.types.PeopleStatusType;
import com.genfare.cloud.usermanagement.dto.OrganizationManagementSearchDto;
import com.genfare.cloud.usermanagement.dto.UserManagementSearchDto;

@Controller
@RequestMapping(value = "/rest")
public class RestController {
    private static final Logger LOGGER = LoggerFactory.getLogger(RestController.class);

    @PersistenceContext
    private EntityManager em;

    @Autowired
    private UserManagementService userManager;

    @Autowired
    OrganizationManagementService organizationmanager;

    @Autowired
    private GroupPermissionsService groupPermService;

    @Autowired
    private GroupsRepository groupsRepository;

    @Autowired
    private GroupsManagementService groupsManager;

    @Autowired
    private OrganizationsRepository orgRepo;

    @Autowired
    PrivilegesManagementService privilegesManager;

    @Autowired
    private PeopleRepository peopleRepo;

    @Autowired
    private JobTitleRepository jobTitleRepository;

    @Autowired
    private SessionRegistry sessionRegistry;

    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/user/search", method = RequestMethod.GET)
    @ResponseBody
    public JSONWrapper<List<UserManagementSearchDto>> search(@RequestParam("q") String q,
        @RequestParam("s") String s) {

        String query =
            "SELECT DISTINCT NEW com.genfare.cloud.usermanagement.dto.UserManagementSearchDto(p.personId,p.personId,p.nameFirst,p.nameLast,em.linkValue,ph.linkValue,p.status.description) "
                + "FROM People p inner join p.peoplelinkses em inner join p.peoplelinkses ph "
                + "WHERE em.linktypes.description LIKE '%email%' AND ph.linktypes.description LIKE '%phone%' ";
        StringBuilder sb = new StringBuilder(query);
        String[] criteria = { q.replaceAll("-\\(\\)", "") };
        if (q.contains(" ")) {
            criteria = q.split(" ");
        }

        for (int i = 0, l = criteria.length; i < l; ++i) {
            if (criteria[i].contains(".") || criteria[i].contains("@")) {
                sb.append(String.format("AND em.linkValue LIKE '%%%s%%' ", criteria[i]));
            } else if (criteria[i].matches("^[0-9]+$")) {
                sb.append(String.format("AND (ph.linkValue LIKE '%%%s%%' OR p.personId LIKE '%%%s%%') ", criteria[i],
                    criteria[i]));
            } else if (criteria[i].matches("^[A-Za-z]+$")) {
                sb.append(String.format("AND (p.nameFirst LIKE '%%%s%%' OR p.nameLast LIKE '%%%s%%') ", criteria[i],
                    criteria[i]));
            }
        }

        if (!s.equalsIgnoreCase("All")) {
            sb.append(String.format("AND p.status.description = '%s'", s));
        }

        List<UserManagementSearchDto> results =
            em.createQuery(sb.toString()).getResultList();
        return new JSONWrapper<List<UserManagementSearchDto>>(results, ((results.isEmpty()) ? "no hits" : "success"));
    }

    @RequestMapping(value = "/user/groups", method = RequestMethod.POST)
    @ResponseBody
    public JSONWrapper<String> updateGroup(@RequestParam("user") String user,
        @RequestParam("modified") String modified) {

        PeopleToUserConverter converter = new PeopleToUserConverter();
        User u = converter.convert(peopleRepo.findPeopleByExactLink(user));

        if (u != null) {
            JSONObject json = new JSONObject(modified);
            Iterator<?> keys = json.keys();

            while (keys.hasNext()) {
                String group = (String) keys.next();
                String action = json.getString(group);

                if (action.equals("added")) {
                    groupPermService.addUserToGroup(u, group);
                } else if (action.equals("removed")) {
                    groupPermService.removeUserFromGroup(u, group);
                }
            }
            userManager.reloadAuthorities(u, sessionRegistry.getAllPrincipals());
            return new JSONWrapper<String>("User Updated", "success");
        }

        return new JSONWrapper<String>("Invalid User", "failure");
    }

    @RequestMapping(value = "/user/job", method = RequestMethod.GET)
    @ResponseBody
    public JSONWrapper<List<String>> findJobTitle(@RequestParam("name") String description) {
        return new JSONWrapper<List<String>>(jobTitleRepository.findJobTitleLikeName(String.format("%%%s%%",
            description)),
            "success");
    }

    @RequestMapping(value = "/organizations/exists", method = RequestMethod.GET)
    @ResponseBody
    public JSONWrapper<Boolean> findOrg(@RequestParam("name") String name) {
        return new JSONWrapper<Boolean>(orgRepo.findByName(name) == null, "success");
    }

    @RequestMapping(value = "/organizations", method = RequestMethod.GET)
    @ResponseBody
    public JSONWrapper<List<String>> findOrgs(@RequestParam("name") String name) {
        return new JSONWrapper<List<String>>(orgRepo.findOrgsLikeName(String.format("%%%s%%", name)), "success");
    }

    @RequestMapping(value = "/organizations/people", method = RequestMethod.GET)
    @ResponseBody
    public JSONWrapper<List<String>> findPeopleInOrg(@RequestParam("name") String name,
        @RequestParam("orgid") String orgid) {
        String[] names;

        if (name.contains(" ")) {
            names = name.split(" ");
            if (names.length != 2) {
                return new JSONWrapper<List<String>>(null, "bad request");
            }
        } else {
            names = new String[] { name, name };
        }

        List<People> peoples =
            peopleRepo.searchPeopleByFirstOrLastNameInOrg(String.format("%%%s%%", names[0]),
                String.format("%%%s%%", names[1]), Integer.parseInt(orgid));
        List<String> result = new ArrayList<String>();
        for (People p : peoples) {
            result.add(String.format("%s %s", p.getNameFirst(), p.getNameLast()));
        }

        return new JSONWrapper<List<String>>(result, "success");
    }

    @RequestMapping(value = "/organizations/people/exists", method = RequestMethod.GET)
    @ResponseBody
    public JSONWrapper<Boolean> existPeopleInOrg(@RequestParam("name") String name,
        @RequestParam("orgid") String orgid) {
        String[] names = name.split(" ");
        if (names.length != 2) {
            return new JSONWrapper<Boolean>(false, "bad request");
        }

        return new JSONWrapper<Boolean>(peopleRepo.getPeopleByFirstAndLastNameInOrg(names[0], names[1],
            Integer.parseInt(orgid)) != null,
            "success");
    }

    @RequestMapping(value = "/groups/status/toggle", method = RequestMethod.POST)
    @ResponseBody
    public JSONWrapper<String> toggleGroupStatus(@RequestParam("groupId") Integer groupId) {
        Groups group = groupsRepository.findOne(groupId);

        if (group != null) {
            groupsManager.toggleGroupStatus(group);
            return new JSONWrapper<String>("Updated Group Status", "success");
        }

        return new JSONWrapper<String>("Invalid group", "failure");
    }

    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/organizations/search", method = RequestMethod.GET)
    @ResponseBody
    public JSONWrapper<List<OrganizationManagementSearchDto>> searchOrg(@RequestParam("q") String q,
        @RequestParam("s") String s) {
        String query =
            "SELECT DISTINCT NEW com.genfare.cloud.usermanagement.dto.OrganizationManagementSearchDto(o) FROM Organizations o ";
        StringBuilder sb = new StringBuilder(query);

        String[] criteria = { q.replaceAll("-\\(\\)", "") };
        if (q.contains(" ")) {
            criteria = q.split(" ");
        }

        String term = "WHERE";
        for (int i = 0, l = criteria.length; i < l; ++i) {
            if (criteria[i].matches("^[0-9]+$")) {
                sb.append(String.format("%s (o.organizationId = '%s' OR o.name LIKE '%%%s%%') ", term, criteria[i],
                    criteria[i]));
            } else {
                sb.append(String.format("%s o.name LIKE '%%%s%%' ", term, criteria[i]));
            }
            term = "AND";
        }

        if (!s.equalsIgnoreCase("All")) {
            sb.append(String.format("%s o.status.description = '%s' ", term, s));
        }

        List<OrganizationManagementSearchDto> results =
            em.createQuery(sb.toString()).getResultList();
        return new JSONWrapper<List<OrganizationManagementSearchDto>>(results, ((results.isEmpty()) ? "no hits"
            : "success"));
    }

    @RequestMapping(value = "/user/enable_disable", method = RequestMethod.POST)
    @ResponseBody
    public JSONWrapper<String> enableDisableUser(@RequestParam("uid") Integer userId) {
        User userById = userManager.getUser(userId);
        if (userById != null) {
            PeopleStatusType status = (userById.getStatus().equals(PeopleStatusType.ACTIVE)) ? PeopleStatusType.DISABLED : PeopleStatusType.ACTIVE;
            UserBuilder builder = new UserBuilder(userById).status(status);
            userManager.saveUser(builder.build());
            return new JSONWrapper<String>("Updated Status", "success");
        }

        return new JSONWrapper<String>("Invalid User", "failure");
    }

    @RequestMapping(value = "/groups/create", method = RequestMethod.POST)
    @ResponseBody
    public JSONWrapper<String> createGroup(@RequestParam("group") String groupString, Principal principal) {
        User user = userManager.getUser(principal);
        Organizations organization = organizationmanager.getOrganizationForUser(user);

        Groups group = new Groups();
        group.setOrganizations(organization);

        JSONObject json = new JSONObject(groupString);
        group.setName(json.getString("name"));
        group.setDescription(json.getString("description"));
        group.setRole(groupsManager.generateRoleForGroup(json.getString("name")));
        group.setStatus(GroupStatusType.ACTIVE.getStatus());

        group = groupsRepository.save(group);
        if (group == null) {
            return new JSONWrapper<String>("Error saving group", "error");
        }

        privilegesManager.savePrivilegesForGroup(group, json.getJSONObject("privileges"));

        return new JSONWrapper<String>("Group created", "success");
    }

    @RequestMapping(value = "/groups/update", method = RequestMethod.POST)
    @ResponseBody
    public JSONWrapper<String> updateGroupPrivileges(@RequestParam("group") String groupName,
        @RequestParam("privileges") String privileges) {
        Groups group = groupsRepository.findGroupByName(groupName);
        if (group == null) {
            return new JSONWrapper<String>("Error updating group", "error");
        }

        JSONObject privilegesJSON = new JSONObject(privileges);
        privilegesManager.clearPriviligesForGroup(group);
        privilegesManager.savePrivilegesForGroup(group, privilegesJSON);

        userManager.reloadGroupAuthorities(group, sessionRegistry.getAllPrincipals());

        return new JSONWrapper<String>("Group created", "success");
    }

}
