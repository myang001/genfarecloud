package com.genfare.cloud.usermanagement.controller;

import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.thymeleaf.context.WebContext;

import com.genfare.cloud.model.entity.Groups;
import com.genfare.cloud.model.entity.Modules;
import com.genfare.cloud.model.entity.Organizations;
import com.genfare.cloud.model.entity.repository.GroupsRepository;
import com.genfare.cloud.model.entity.repository.JobTitleRepository;
import com.genfare.cloud.model.entity.repository.ModulesRepository;
import com.genfare.cloud.model.entity.repository.OrganizationsRepository;
import com.genfare.cloud.model.security.User;
import com.genfare.cloud.model.service.EmailService;
import com.genfare.cloud.model.service.GroupPermissionsService;
import com.genfare.cloud.model.service.OrganizationManagementService;
import com.genfare.cloud.model.service.UserManagementService;
import com.genfare.cloud.usermanagement.form.UserForm;

@Controller
@RequestMapping("/user")
public class UserController {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(UserController.class);

    @Autowired
    private UserManagementService usermanager;

    @Autowired
    private GroupsRepository groupsRepository;

    @Autowired
    GroupPermissionsService groupPermService;

    @Autowired
    ModulesRepository modulesRepo;

    @Autowired
    private EmailService emailservice;

    @Autowired
    private OrganizationsRepository organizationsRepository;

    @Autowired
    private OrganizationManagementService organizationmanager;

    @Autowired
    private JobTitleRepository jobTitleRepository;

    @RequestMapping(method = RequestMethod.GET)
    public String get(@RequestParam("uid") String userId, ModelMap model) {
        User u = usermanager.getUser(Integer.parseInt(userId));
        Organizations org = organizationmanager.getOrganizationForUser(u);
        model.addAttribute("user", u);
        model.addAttribute("usergroups", u.getGroups());
        model.addAttribute("groups", groupsRepository.findGroupsByOrganization(org.getOrganizationId()));
        model.addAttribute("modules", groupPermService.sortModules(((List<Modules>) modulesRepo.findAll())));
        model.addAttribute("groupPermService", groupPermService);
        return (u == null) ? "redirect:/" : "user/details";
    }

    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public String createUserUserForm(UserForm userForm, ModelMap model, Principal principal,
        @RequestParam(value = "orgId", required = false) Integer organizationId) {

        String organizationName;
        if (organizationId == null) {
            User user = usermanager.getUser(principal);
            organizationId = organizationmanager.getOrganizationForUser(user).getOrganizationId();
            organizationName = organizationmanager.getOrganizationNameForUser(user);
        }
        else {
            organizationName = organizationsRepository.findOne(organizationId).getName();
        }

        userForm.setAgency(organizationName);
        model.addAttribute("groups", groupsRepository.findGroupsByOrganization(organizationId));
        model.addAttribute("modules", groupPermService.sortModules(((List<Modules>) modulesRepo.findAll())));
        model.addAttribute("groupPermService", groupPermService);
        return "user/create";
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public String saveUser(@Valid UserForm userForm, BindingResult result, Model model, Locale locale, HttpServletRequest request, HttpServletResponse response) {
        if (result.hasErrors()) {
            LOGGER.debug("Errors Registering User");
            return "user/create";
        }

        User user = userForm.updateUser(null);
        assignGroupsFromString(user, userForm.getGroupsList());

        user = usermanager.saveUser(user);
        emailservice.setContext(new WebContext(request, response, request.getServletContext()));
        emailservice.generateSetPasswordVerificationToken(user);
        model.addAttribute("createUser", userForm);
        return "user/success";
    }

    private void assignGroupsFromString(User u, String modified) {
        if (u != null) {
            String[] groups = modified.split(",");
            Set<Groups> ugroups = u.getGroups();
            ugroups.clear();

            for (String group : groups) {
                ugroups.add(groupsRepository.findGroupByName(group));
            }
        }
    }

    @RequestMapping(value = "/profile", method = RequestMethod.GET)
    public String getprofile(@RequestParam("uid") Integer userId, ModelMap model) {
        User u = usermanager.getUser(userId);

        if (u == null) {
            return "redirect:/";
        }

        model.addAttribute("userForm", new UserForm(u));
        return "user/profile";
    }

    @RequestMapping(value = "/profile", method = RequestMethod.POST)
    public ModelAndView saveProfile(@Valid UserForm form, BindingResult results) {

        if (results.hasErrors()) {
            return new ModelAndView("user/profile", results.getModel());
        }

        Map<String, Object> map = new HashMap<String, Object>();

        Integer userId = form.getUserId();
        map.put("uid", userId);

        User u = usermanager.getUser(userId);
        usermanager.saveUser(form.updateUser(u));
        return new ModelAndView(new RedirectView("/usermanagement/user/profile"), map);
    }

    @RequestMapping(value = "/copy", method = RequestMethod.GET)
    public String copyUser(@RequestParam("uid") String userId, UserForm userForm, ModelMap model, Principal principal) {
        User userById = usermanager.getUser(Integer.parseInt(userId));
        userForm.setGroups(userById.getGroups());
        Organizations organization = organizationmanager.getOrganizationForUser(userById);
        return createUserUserForm(userForm, model, principal, organization.getOrganizationId());
    }

}
