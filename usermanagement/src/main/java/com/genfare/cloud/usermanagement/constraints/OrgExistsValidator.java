package com.genfare.cloud.usermanagement.constraints;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

import com.genfare.cloud.model.entity.repository.OrganizationsRepository;
import com.genfare.cloud.usermanagement.annotation.OrgExists;

public class OrgExistsValidator
    implements ConstraintValidator<OrgExists, String> {

    @Autowired
    OrganizationsRepository orgRepo;

    @Override
    public void initialize(OrgExists constraintAnnotation) {

    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {

        if (value == null || value.isEmpty()) {
            return true;
        }

        return ((orgRepo.findByName(value)) != null);
    }

}
