package com.genfare.cloud.usermanagement.form;

import java.math.BigDecimal;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

import org.hibernate.validator.constraints.NotBlank;

import com.genfare.cloud.model.entity.OrganizationStatus;
import com.genfare.cloud.model.entity.Organizations;
import com.genfare.cloud.model.entity.People;
import com.genfare.cloud.model.types.OrganizationStatusType;
import com.genfare.cloud.model.types.OrganizationtypesType;
import com.genfare.cloud.model.types.PaymenttypesType;
import com.genfare.cloud.usermanagement.annotation.OrgAvailable;
import com.genfare.cloud.usermanagement.annotation.OrgExists;
import com.genfare.cloud.usermanagement.annotation.PersonExistsInOrg;
import com.genfare.cloud.usermanagement.form.OrganizationForm.Update;

@PersonExistsInOrg(id = "id", person = "mainContact", groups = { Update.class }, message = "{{usermanagement.org.form.validation.contact.not.exists}}")
public class OrganizationForm {

    @NotNull(groups = { Update.class }, message = "{{usermanagement.org.form.validation.id.null}}")
    @Null(groups = { Create.class }, message = "{{usermanagement.org.form.validation.id.not.null}}")
    private Integer id;

    @NotBlank(groups = { Create.class, Update.class }, message = "{{usermanagement.org.form.validation.name.blank}}")
    @OrgAvailable(groups = { Create.class }, message = "{{usermanagement.org.form.validation.name.exists}}")
    private String name;

    @NotBlank(groups = { Create.class, Update.class }, message = "{{usermanagement.org.form.validation.type.blank}}")
    private String type;

    @NotNull(groups = { Create.class, Update.class }, message = "{{usermanagement.org.form.validation.credit.blank}}")
    private BigDecimal creditLimit;

    private String mainContact;

    @NotBlank(groups = { Create.class, Update.class }, message = "{{usermanagement.org.form.validation.parent.blank}}")
    private String parent;

    @OrgExists(groups = { Create.class, Update.class }, message = "{{usermanagement.org.form.validation.parent.not.exist}}")
    private String parentName;

    public OrganizationForm() {
        this.parent = "No";
    }

    public OrganizationForm(Organizations o) {
        this.id = o.getOrganizationId();
        this.name = o.getName();
        this.creditLimit = o.getCreditLimit();

        Organizations po = o.getOrganizations();
        if (po == null) {
            this.parent = "No";
        } else {
            this.parent = "Yes";
            this.parentName = po.getName();
        }

        People p = o.getMainContact();
        if (p != null) {
            this.mainContact = String.format("%s %s", p.getNameFirst(), p.getNameLast());
        }
    }

    public Organizations updateOrganization(Organizations o) {

        o.setOrganizationId(this.id);
        o.setName(this.name);
        o.setOrganizationtypes(OrganizationtypesType.findType(this.type));
        o.setCreditLimit(this.creditLimit);
        o.setPaymenttypes(PaymenttypesType.INVOICED.getType());

        OrganizationStatus os = o.getStatus();
        if (os == null) {
            o.setStatus(OrganizationStatusType.ACTIVE.getStatus());
        }

        if (o.getBalance() == null) {
            o.setBalance(this.creditLimit);
        }

        return o;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public BigDecimal getCreditLimit() {
        return creditLimit;
    }

    public void setCreditLimit(BigDecimal creditLimit) {
        this.creditLimit = creditLimit;
    }

    public String getMainContact() {
        return mainContact;
    }

    public void setMainContact(String mainContact) {
        this.mainContact = mainContact;
    }

    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public interface Create {
    };

    public interface Update {
    };
}
