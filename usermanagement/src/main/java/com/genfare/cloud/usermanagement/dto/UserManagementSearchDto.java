package com.genfare.cloud.usermanagement.dto;

public class UserManagementSearchDto {

    private Integer id;
    private String employeeId;
    private String nameFirst;
    private String nameLast;
    private String email;
    private String phone;
    private String status;

    public UserManagementSearchDto(Integer id, Integer employeeId, String nameFirst,
        String nameLast, String email, String phone, String status) {
        this.id = id;
        this.employeeId = employeeId.toString(); // TEMPORARY FIX UNTIL EMPLOYEE ID IS AVAILABLE IN DATAMODEL
        this.nameFirst = nameFirst;
        this.nameLast = nameLast;
        this.email = email;
        this.phone = phone;
        this.status = status;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getNameFirst() {
        return nameFirst;
    }

    public void setNameFirst(String nameFirst) {
        this.nameFirst = nameFirst;
    }

    public String getNameLast() {
        return nameLast;
    }

    public void setNameLast(String nameLast) {
        this.nameLast = nameLast;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
