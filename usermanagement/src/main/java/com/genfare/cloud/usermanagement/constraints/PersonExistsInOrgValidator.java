package com.genfare.cloud.usermanagement.constraints;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.genfare.cloud.model.entity.repository.PeopleRepository;
import com.genfare.cloud.usermanagement.annotation.PersonExistsInOrg;

public class PersonExistsInOrgValidator implements ConstraintValidator<PersonExistsInOrg, Object> {

    @Autowired
    PeopleRepository peopleRepo;

    private String idFieldName;

    private String personFieldName;

    @Override
    public void initialize(PersonExistsInOrg constraintAnnotation) {
        idFieldName = constraintAnnotation.id();
        personFieldName = constraintAnnotation.person();
    }

    @Override
    public boolean isValid(final Object value, ConstraintValidatorContext context) {
        try {
            final String id = (String) BeanUtils.getProperty(value, idFieldName);
            final String name = BeanUtils.getProperty(value, personFieldName);

            if (name == null || name.isEmpty()) {
                return true;
            }

            String[] names = name.split(" ");
            if (names.length < 2) {
                return false;
            }

            if (id == null) {
                return false;
            }

            return peopleRepo.getPeopleByFirstAndLastNameInOrg(names[0], names[1], Integer.parseInt(id)) != null;

        } catch (final Exception ignore) {
            // ignore
        }
        return false;
    }

}
