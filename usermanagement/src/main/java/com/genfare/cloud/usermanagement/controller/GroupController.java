package com.genfare.cloud.usermanagement.controller;

import java.security.Principal;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.genfare.cloud.model.entity.Groups;
import com.genfare.cloud.model.entity.GroupsofpeopleId;
import com.genfare.cloud.model.entity.Modules;
import com.genfare.cloud.model.entity.Organizations;
import com.genfare.cloud.model.entity.repository.GroupsRepository;
import com.genfare.cloud.model.entity.repository.GroupsofpeopleRepository;
import com.genfare.cloud.model.entity.repository.ModulesRepository;
import com.genfare.cloud.model.entity.repository.PeopleRepository;
import com.genfare.cloud.model.security.User;
import com.genfare.cloud.model.service.GroupPermissionsService;
import com.genfare.cloud.model.service.GroupsManagementService;
import com.genfare.cloud.model.service.OrganizationManagementService;
import com.genfare.cloud.model.service.UserManagementService;

@Controller
@RequestMapping("/groups")
public class GroupController {

    @Autowired
    UserManagementService userManager;
    @Autowired
    PeopleRepository peopleRepository;
    @Autowired
    GroupsofpeopleRepository groupsofpeopleRepo;
    @Autowired
    GroupsRepository groupsRepository;
    @Autowired
    SessionRegistry sessionRegistry;

    private static final Logger LOGGER = LoggerFactory.getLogger(GroupController.class);

    @Autowired
    OrganizationManagementService organizationmanager;

    @Autowired
    UserManagementService usermanager;

    @Autowired
    GroupsManagementService groupsmanager;

    @Autowired
    ModulesRepository modulesRepo;

    @Autowired
    GroupPermissionsService groupPermService;

    @Autowired
    GroupsManagementService groupsManager;

    @RequestMapping(method = RequestMethod.GET)
    public String index(Principal principal, ModelMap model, Pageable pageable) {
        User user = usermanager.getUser(principal);
        Organizations signedInUserOrg = organizationmanager.getOrganizationForUser(user);
        Page<Groups> page = groupsRepository.findEnabledByOrganizationIdOrderByNameAsc(signedInUserOrg.getOrganizationId(), pageable);

        model.addAttribute("page", page);
        return "groups/main";
    }

    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public String groupUsers(Principal principal, @RequestParam(value = "groupId", required = true) String groupId, ModelMap model) {
        List<User> users = userManager.getUsersByGroup(Integer.parseInt(groupId));
        Groups group = groupsRepository.findOne(Integer.parseInt(groupId));
        Iterable<Groups> groups = groupsRepository.findAll();
        LOGGER.info("Number of people found: " + users.size());
        model.addAttribute("people", users);
        model.addAttribute("groupId", groupId);
        model.addAttribute("currentGroup", group);
        model.addAttribute("groups", groups);
        model.addAttribute("usersSize", users.size());
        return "groups/users";
    }

    @RequestMapping(value = "/users", method = RequestMethod.POST)
    public String saveUsers(Principal principal, @RequestParam(value = "groupId", required = true) String groupId, HttpServletRequest request, ModelMap model) {
        Integer personIdToDelete = null;
        Integer i = 0;
        Integer size = Integer.parseInt(request.getParameter("usersSize"));
        while (i < size) {
            if (request.getParameter("delete" + i.toString()) != null) {
                personIdToDelete = Integer.parseInt(request.getParameter("delete" + i.toString()));
                GroupsofpeopleId id = new GroupsofpeopleId(Integer.parseInt(groupId), personIdToDelete);
                groupsofpeopleRepo.delete(id);
                usermanager.reloadAuthorities(usermanager.getUser(personIdToDelete), sessionRegistry.getAllPrincipals());
            }
            i++;
        }
        return "redirect:/groups/users?groupId=" + groupId;
    }

    @RequestMapping(value = "/details", method = RequestMethod.GET)
    public String getDetails(@RequestParam(value = "groupId", required = true) Integer groupId, Principal principal, ModelMap model) {
        Groups group = groupsRepository.findOne(groupId);

        model.addAttribute("group", group);
        model.addAttribute("modules", groupPermService.sortModules(((List<Modules>) modulesRepo.findAll())));
        model.addAttribute("groupPermService", groupPermService);
        model.addAttribute("groupsManager", groupsManager);
        return "groups/details";
    }

    @RequestMapping(value = "/details", method = RequestMethod.POST)
    public String saveBasic(@RequestParam(value = "groupId", required = true) Integer groupId, HttpServletRequest request) {
        Groups group = groupsRepository.findOne(groupId);
        group.setName(request.getParameter("namefield"));
        group.setDescription(request.getParameter("description"));

        if (groupsRepository.save(group) == null) {
            LOGGER.error("Error updating basic group info");
            return "groups/editBasic";
        }
        return "redirect:/groups/details?groupId=" + groupId;
    }

    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    public String editBasic(@RequestParam(value = "groupId", required = true) Integer groupId, Principal principal, ModelMap model) {
        Groups group = groupsRepository.findOne(groupId);

        model.addAttribute("group", group);
        return "groups/editBasic";
    }

    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public String createGroup(Principal principal, ModelMap model) {
        model.addAttribute("group", null);
        model.addAttribute("modules", groupPermService.sortModules(((List<Modules>) modulesRepo.findAll())));
        return "groups/create";
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public String saveGroup(Principal principal, ModelMap model) {
        return "redirect:/groups";
    }
}
