package com.genfare.cloud.usermanagement.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.servlet.configuration.EnableWebMvcSecurity;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.session.SessionRegistryImpl;

import com.genfare.cloud.config.security.AbstractBaseWebSecurityConfigurerAdapter;

@Configuration
@EnableWebMvcSecurity
public class SecurityConfig extends AbstractBaseWebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // @formatter:off
        super.configure(http);
        http
            .authorizeRequests()
            .antMatchers("/groups/**", "/user/**", "/organizations/**").hasRole("USER")
            .anyRequest().permitAll()
            .and().csrf()
            .and().formLogin().defaultSuccessUrl("/");
        enableSSO(http);
        // -1 = unlimited logins
        http.sessionManagement().maximumSessions(-1).sessionRegistry(sessionRegistry());
        // @formatter:on
    }

    @Bean
    public SessionRegistry sessionRegistry() {
        return new SessionRegistryImpl();
    }
}
