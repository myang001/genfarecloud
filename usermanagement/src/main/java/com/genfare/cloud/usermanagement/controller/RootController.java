package com.genfare.cloud.usermanagement.controller;

import java.security.Principal;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.genfare.cloud.model.types.PeopleStatusType;

@Controller
@RequestMapping("/")
public class RootController {

    @RequestMapping(value = { "", "/login" }, method = RequestMethod.GET)
    public String get(Principal principal, ModelMap model) {
        model.addAttribute("statuses", PeopleStatusType.getStatuses());
        return (principal == null) ? "basic/login" : "root/main";
    }
}
