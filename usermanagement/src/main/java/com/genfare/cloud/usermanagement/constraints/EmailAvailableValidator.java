package com.genfare.cloud.usermanagement.constraints;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

import com.genfare.cloud.model.contact.Email;
import com.genfare.cloud.model.service.EmailService;
import com.genfare.cloud.usermanagement.annotation.EmailAvailable;

public class EmailAvailableValidator
    implements ConstraintValidator<EmailAvailable, Email> {

    @Autowired
    EmailService emailService;

    @Override
    public void initialize(EmailAvailable constraintAnnotation) {

    }

    @Override
    public boolean isValid(Email value, ConstraintValidatorContext context) {

        if (value == null || value.getAddress() == null || value.getAddress().isEmpty()) {
            return true;
        }

        Email e = emailService.findEmail(value.getAddress());
        return e == null;
    }

}
