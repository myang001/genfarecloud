package com.genfare.cloud.usermanagement.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;
import com.genfare.cloud.model.entity.Modules;
import com.genfare.cloud.model.entity.OrganizationStatus;
import com.genfare.cloud.model.entity.Organizations;
import com.genfare.cloud.model.entity.repository.GroupsRepository;
import com.genfare.cloud.model.entity.repository.ModulesRepository;
import com.genfare.cloud.model.entity.repository.OrganizationsRepository;
import com.genfare.cloud.model.entity.repository.PeopleRepository;
import com.genfare.cloud.model.service.GroupPermissionsService;
import com.genfare.cloud.model.types.OrganizationStatusType;
import com.genfare.cloud.model.types.OrganizationtypesType;
import com.genfare.cloud.usermanagement.dto.UserManagementSearchDto;
import com.genfare.cloud.usermanagement.form.OrganizationForm;

@Controller
@RequestMapping("/organizations")
public class OrgController {

    @PersistenceContext
    EntityManager em;

    @Autowired
    OrganizationsRepository orgRepo;

    @Autowired
    GroupPermissionsService groupsService;

    @Autowired
    GroupsRepository groupRepo;

    @Autowired
    PeopleRepository peopleRepo;

    @Autowired
    ModulesRepository modulesRepository;

    @RequestMapping(method = RequestMethod.GET)
    public String getList(ModelMap model) {
        model.addAttribute("statuses", OrganizationStatusType.getStatuses());
        return "orgs/list";
    }

    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public String createOrg(@ModelAttribute("orgForm") OrganizationForm orgForm, ModelMap model) {
        model.addAttribute("orgForm", orgForm);
        model.addAttribute("types", OrganizationtypesType.getTypes());
        return "orgs/create";
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public String saveUser(@ModelAttribute("orgForm") @Validated({ OrganizationForm.Create.class }) OrganizationForm orgForm, BindingResult results, Model model,
        RedirectAttributes attrs) {
        if (results.hasErrors()) {
            model.addAttribute("orgForm", orgForm);
            return "orgs/create";
        }

        Organizations o = orgForm.updateOrganization(new Organizations());
        if (orgForm.getParent().equals("Yes")) {
            Organizations po = orgRepo.findByName(orgForm.getParentName());
            o.setOrganizations(po);
        }
        orgRepo.save(o);
        groupsService.createDefaultGroupForOrg(o);

        attrs.addFlashAttribute("org", o);

        return "redirect:/organizations/create/success";
    }

    @RequestMapping(value = "/create/success", method = RequestMethod.GET)
    public String confirmCreate(@ModelAttribute("org") Organizations o, ModelMap model) {
        if (o == null || o.getName() == null || o.getName().isEmpty()) {
            return "redirect:/organizations";
        }
        return "orgs/success";
    }

    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/details", method = RequestMethod.GET)
    public String getOrgDetails(@RequestParam("oid") Integer orgId, ModelMap model) {
        Organizations o = orgRepo.findOne(orgId);

        if (orgId == null) {
            return "redirect:/organizations";
        }

        // This could maybe be in a repo? would require some juggling of the DTO objects at the very least I think
        String userQuery =
            "SELECT DISTINCT NEW com.genfare.cloud.usermanagement.dto.UserManagementSearchDto(p.personId,p.personId,p.nameFirst,p.nameLast,em.linkValue,ph.linkValue,p.status.description) "
                + "FROM People p INNER JOIN p.peoplelinkses em INNER JOIN p.peoplelinkses ph INNER JOIN p.groups g "
                + "WHERE em.linktypes.description LIKE '%email%' AND ph.linktypes.description LIKE '%phone%' AND g.organizations.organizationId = " + orgId;
        model.addAttribute("org", o);
        model.addAttribute("users", (List<UserManagementSearchDto>) em.createQuery(userQuery).getResultList());
        model.addAttribute("groups", groupRepo.findGroupsByOrganization(orgId));
        model.addAttribute("modules", groupsService.sortModules(((List<Modules>) modulesRepository.findAll())));
        model.addAttribute("groupPermService", groupsService);
        model.addAttribute("statuses", OrganizationStatusType.getStatuses());
        return "orgs/details";
    }

    @RequestMapping(value = "/changeStatus", method = RequestMethod.GET)
    public String changeStatus(@RequestParam("oid") Integer orgId, @RequestParam("status") String status, ModelMap model) {
        Organizations o = orgRepo.findOne(orgId);
        OrganizationStatus newStatus = OrganizationStatusType.findStatus(status);
        if (newStatus != null) {
            o.setStatus(newStatus);
            orgRepo.save(o);
        }
        return "redirect:/organizations/details?oid=" + orgId;
    }

    @RequestMapping(value = "/profile", method = RequestMethod.GET)
    public String getOrgProfile(@RequestParam("oid") Integer orgId, ModelMap model) {
        Organizations o = orgRepo.findOne(orgId);

        if (orgId == null) {
            return "redirect:/organizations";
        }

        model.addAttribute("orgForm", new OrganizationForm(o));
        model.addAttribute("org", o);
        model.addAttribute("types", OrganizationtypesType.getTypes());
        return "orgs/profile";
    }

    @RequestMapping(value = "/profile", method = RequestMethod.POST)
    public ModelAndView setOrgDetails(@ModelAttribute("orgForm") @Validated({ OrganizationForm.Update.class }) OrganizationForm orgForm, BindingResult results, Model model) {
        Organizations o = orgRepo.findOne(orgForm.getId());

        if (results.hasErrors()) {
            model.addAttribute("orgForm", orgForm);
            model.addAttribute("org", o);
            model.addAttribute("types", OrganizationtypesType.getTypes());

            return new ModelAndView("orgs/profile", model.asMap());
        }

        o = orgForm.updateOrganization(o);
        if (orgForm.getParent().equals("Yes")) {
            Organizations po = orgRepo.findByName(orgForm.getParentName());
            o.setOrganizations(po);
        }

        String mc = orgForm.getMainContact();
        if (mc != null && !mc.isEmpty()) {
            String[] names = mc.split(" ");

            if (names.length == 2) {
                o.setMainContact(peopleRepo.getPeopleByFirstAndLastNameInOrg(names[0], names[1], o.getOrganizationId()));
            }
        } else {
            o.setMainContact(null);
        }

        orgRepo.save(o);

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("oid", o.getOrganizationId());
        return new ModelAndView(new RedirectView("/usermanagement/organizations/details"), map);
    }
}