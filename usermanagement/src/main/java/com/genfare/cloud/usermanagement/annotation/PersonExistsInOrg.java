package com.genfare.cloud.usermanagement.annotation;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.genfare.cloud.usermanagement.constraints.PersonExistsInOrgValidator;

@Target({ TYPE, ANNOTATION_TYPE })
@Retention(RUNTIME)
@Constraint(validatedBy = PersonExistsInOrgValidator.class)
public @interface PersonExistsInOrg {

    String message() default "Person does not exist";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    String id();

    String person();

    @Target({ TYPE, ANNOTATION_TYPE })
    @Retention(RUNTIME)
    @Documented
    @interface List {
        PersonExistsInOrg[] value();
    }
}