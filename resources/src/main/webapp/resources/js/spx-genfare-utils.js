
/*
 * Extracts the base url and page context and appends path variable
 * 
 * Ex.
 * 
 * Site on -> http://localhost:1234/example
 * extract_url('/rest/service'); --> returns http://localhost:1234/example/rest/service 
 */
function extract_url(path) {		
	return window.location.origin + '/' + window.location.pathname.split('/')[1] + path;
}


/*
 * Validation Registry
 */
function Validate() {
	
	var registry = {};
	
	return {
		register: function(method) {
			if(!registry[method.name]) {
				registry[method.name] = method;
			}
			return method.name;
		},
		validate: function() {
			var b = true;
			for(var v in registry) {
				b &= registry[v]();
			}
			return b;
		},
		unregister: function(method) {
			if(registry[method.name]) {
				delete registry[method.name];
			}
		}
	};
}



$(function() {
	$('.calendar').datepicker({
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+0"
    });
	
});