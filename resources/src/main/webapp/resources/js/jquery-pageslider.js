

;(function($) {

	var default_settings = {
		effects: {
			next: {
				action: 'slide',
				options: {direction: 'right'},
				speed: 500
			},
			prev: {
				action: 'slide',
				options: {direction: 'left'},
				speed: 500
			}
		},
		page_target: 'div'
	};

	var methods = {
		init: function(options) {
			return this.each(function() {
				var options = $.extend({},default_settings,options);
				var $this = $(this)
				var $children = $this.children(options.page_target);

				$this.addClass('pageslider');
				$this.data('pageslider-options',options);	

				$children.each(function() {
					$(this).addClass('pageslider-page').hide();
				});

				$children.first().addClass('pageslider-active').show();


			});
		},
		destroy: function() {
			return this.each(function() {
				var $this = $(this);
				$this.removeClass('pageslider').children().removeClass('pageslider-page').show();
				$this.removeData('pageslider-options');
			});
		},
		next: function() {
			var $this = $(this);
			var options = $this.data('pageslider-options');
			var $children = $this.children('.pageslider-page');
			var index = $this.children('.pageslider-active').index();

			if(index < $children.length - 1) {
				$($children[index]).hide().removeClass('pageslider-active').next().addClass('pageslider-active').show(options.effects.next.action,options.effects.next.options,options.effects.next.speed);
				index = index + 1;
			}
			return index;
		},
		prev: function() {
			var $this = $(this);
			var options = $this.data('pageslider-options');
			var $children = $this.children('.pageslider-page');
			var index = $this.children('.pageslider-active').index();

			if(index > 0) {
				$($children[index]).hide().removeClass('pageslider-active').prev().addClass('pageslider-active').show(options.effects.prev.action,options.effects.prev.duration,options.effects.prev.speed);
				index = index - 1;
			}
			return index;

		},
		select: function(page) {
			var $this = $(this);
			var options = $this.data('pageslider-options');
			var $children = $this.children('.pageslider-page');

			if(page < 0 || page >= $children.length) {
				return $this.children('.pageslider-active').index();
			}

			$($children).hide().removeClass('pageslider-active');
			$($children[page]).addClass('pageslider-active').show(options.effects.next.action,options.effects.next.options,options.effects.next.speed);
			index = page;

			return index;
		},
		length: function() {
			return $(this).children('.pageslider-page').length;
		},
		current: function() {
			return $(this).children('.pageslider-active').index();
		}
	};

	$.fn.pageslider = function(method) {
		if(methods[method]) {
			return methods[method].apply(this,Array.prototype.slice.call(arguments,1));
		} else if(typeof method === 'object' || !method) {
			return methods.init.apply(this,arguments);
		} else {
			$.error('Method ' + method + ' does not exist');
		}
	};
})(jQuery);
