GenfareCloud IDE Configuration Files For Eclipse
================================================
The following instructions cover how to configure eclipse to use files and
conventions to comply with GenfareCloud coding standards.

Java Formatter
--------------
(Ctrl+Shift+F by default)

1. In Eclipse preferences go to Java -> Code Style -> Formatter
2. Click Import
3. Select the Java formatter file from build-tools/src/main/resources/build-tools/eclipse/genfare-formatter.xml

Java Auto-format on Save
------------------------
By default automatic formatting and cleanup is disabled. To enable it, do the following:

1. Go to Window > Preferences > Java > Editor > Save Actions.
2. Select Perform the selected actions on save.
3. Select Format Source code. Make sure Format all lines is selected.
4. Make sure Organize imports is selected.
5. Select Additional actions.
6. Click Ok, edit some code, save it and watch Eclipse format it automatically.

Java Clean up
-------------
(not bound by default, use  of Ctrl+Shift+O recommended)

1. In Eclipse preferences for to Java -> Code Style -> Clean-up
2. Click Import
3. Select the file build-tools/src/main/resources/build-tools/eclipse/genfare-cleanup.xml

*Map the Clean-up Rules to Ctrl+Shift+O*

1. You may have to unbind Ctrl+Shift+O from "Organise imports" action first
2. In Eclipse preferences go to General -> Keys
3. Type "clean" into filter input ("type filter text")
4. Select Clean-up
5. Setup Binding: focus the input and type Ctrl+Shift+O as individual keys (or any other preferred shortcut)

JavaScript Formatter
--------------------
(Ctrl+Shift+F by default)

1. In Eclipse preferences go to JavaScript -> Code Style -> Formatter
2. Click Import
3. Select the file build-tools/src/main/resources/build-tools/eclipse/genfare-js-formatter.xml

JavaScript Clean Up 
-------------------
(not bound by default, use  of Ctrl+Shift+O recommended - see above)

1. In Eclipse preferences for to JavaScript -> Code Style -> Clean-up
2. Click Import
3. Select the file build-tools/src/main/resources/build-tools/eclipse/genfare-js-cleanup.xml

General Text Editor Formatting
------------------------------
1. In Eclipse Preferences go to General -> Editors -> Text Editors
2. Setup
    * Insert spaces for tabs: YES
    * Show print margin: YES
    * Print margin column: 128

XML formatting
--------------
1. In Eclipse Preferences go to XML -> XML Files -> Editor
2. Setup
    * Line width: 128
    * Split multiple attributes each on new line: NO
    * Align final bracked in multi-line element tags: NO
    * Preserve whitespace in tags with PCDATA content: NO
    * Clear all blank lines: NO
    * Format comments: NO
    * Insert whitespace before closing empty eng-tags: YES
    * Indent using spaces
    * Indentation size: 2

HTML formatting
---------------
1. In Eclipse Preferences go to -> Web -> HTML Files -> Editor
2. Setup
    * Line width: 128
    * Split multiple attributes each on new line: NO
    * Align final bracked in multi-line element tags: NO
    * Clear all blank lines: NO
    * Indent using spaces
    * Indentation size: 4
    * Tag names: Lowercase
    * Attribute names: Lowercase

CSS Formatting
--------------
1. In Eclipse Preferences go to Web -> CSS Files -> Editor
2. Setup
    * Line width: 128
    * Insert line break between properties: YES
    * Disable wrapping in style attribute of Html: YES
    * Indent using spaces
    * Indentation size: 4
    * Capitalization style: all Lowercase
