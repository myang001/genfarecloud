package com.genfare.cloud.oms.batch.config;

import java.util.Date;

import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.SimpleTrigger;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.quartz.JobDetailBean;
import org.springframework.scheduling.quartz.SimpleTriggerBean;
import com.genfare.cloud.integration.producers.FullfillSubscriptionProducer;
import com.genfare.cloud.oms.batch.SubscriptionBatchJob;

@Configuration
@EnableBatchProcessing
public class StandaloneInfrastructureConfiguration {

    @Autowired
    private ApplicationContext applicationContext;

    @Bean
    public JobDetailBean jobDetail() {
        JobDetailBean jobDetail = new JobDetailBean();
        jobDetail.setName("Subscription Batch Job");
        jobDetail.setJobClass(SubscriptionBatchJob.class);
        return jobDetail;
    }

    @Bean
    public SimpleTriggerBean trigger() {
        SimpleTriggerBean simpleTrigger = new SimpleTriggerBean();
        simpleTrigger.setStartTime(new Date(System.currentTimeMillis() + 1000));
        simpleTrigger.setRepeatCount(SimpleTrigger.REPEAT_INDEFINITELY);
        simpleTrigger.setRepeatInterval(86400000);// once a day, in tenant config, but not pulling it out yet
        simpleTrigger.setName("FirstTrigger");
        return simpleTrigger;
    }

    @Bean
    public SchedulerFactory schedulerFactory() throws SchedulerException {
        SchedulerFactory factory = new StdSchedulerFactory();
        return factory;
    }

    @Bean
    public Scheduler scheduler() throws SchedulerException {
        Scheduler scheduler = schedulerFactory().getScheduler();
        AutowiringSpringBeanJobFactory jobFactory = new AutowiringSpringBeanJobFactory();
        jobFactory.setApplicationContext(applicationContext);
        scheduler.setJobFactory(jobFactory);
        scheduler.start();
        scheduler.scheduleJob(jobDetail(), trigger());
        return scheduler;
    }

    // Would not autowire
    @Bean
    public FullfillSubscriptionProducer producer() {
        FullfillSubscriptionProducer producer = new FullfillSubscriptionProducer();
        return producer;
    }
}
