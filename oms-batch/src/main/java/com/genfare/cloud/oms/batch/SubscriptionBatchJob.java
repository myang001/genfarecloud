package com.genfare.cloud.oms.batch;

import java.util.Date;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.genfare.cloud.integration.producers.FullfillSubscriptionProducer;
import com.genfare.cloud.model.entity.Subscriptions;
import com.genfare.cloud.model.entity.Walletcontents;
import com.genfare.cloud.model.entity.repository.SubscriptionsRepository;
import com.genfare.cloud.model.entity.repository.WalletcontentsRepository;
import com.genfare.cloud.model.tenant.TenantConfigurationRepository;
import com.genfare.cloud.model.types.SubscriptionstatusType;

@Component
public class SubscriptionBatchJob implements Job {

    private static final Logger LOGGER = LoggerFactory.getLogger(SubscriptionBatchJob.class);
    @Autowired
    SubscriptionsRepository subscriptionsRepo;
    @Autowired
    WalletcontentsRepository wcRepo;
    @Autowired
    FullfillSubscriptionProducer producer;
    @Autowired
    private TenantConfigurationRepository tenantConfig;

    @Override
    public void execute(JobExecutionContext context)
        throws JobExecutionException {
        Iterable<Subscriptions> subscriptions = subscriptionsRepo.findAll();
        for (Subscriptions s : subscriptions) {
            Long subId = s.getSubscriptionId();
            Date expires = s.getDateReplenishExpires();
            String status = subscriptionsRepo.findStatusForSubscription(subId).getDescription();
            Date today = new Date();
            if ((status.equals("New") || status.equals("Active")) && (expires != null) && (expires.after(today) || expires.equals(today))) {
                Walletcontents wc = s.getWalletcontents();
                int result = subscriptionsRepo.findThresholdForSubscription(subId).compareTo(wcRepo.findValueRemainingForWalletcontent(wc.getWalletContentsId()));
                if ((result == 1) || (result == 0)) {
                    if (status.equals("New")) {
                        s.setSubscriptionstatus(SubscriptionstatusType.ACTIVE.getStatus());
                        subscriptionsRepo.save(s);
                    }
                    sendSubscriptionToQueue(s);
                }
            }
        }
    }

    private void sendSubscriptionToQueue(Subscriptions s) {
        LOGGER.info("Sending suscription with id: " + s.getSubscriptionId() + " to queue to be fullfilled");
        producer.sendSubscriptionNotification(tenantConfig.getDefaultTenant().getSchemaName(), s);
    }
}