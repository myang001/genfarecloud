package com.genfare.cloud.config.sso.handlers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import com.genfare.cloud.model.security.jwt.JwtToken;
import com.genfare.cloud.model.tenant.TenantConfiguration;
import com.genfare.cloud.model.tenant.impl.SsoConfigurationType;

public class JwtAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

    private TenantConfiguration tenantConfig;

    public void setTenantConfiguration(TenantConfiguration tenantConfig) {
        this.tenantConfig = tenantConfig;
    }

    private Cookie buildJwtToken(SsoConfigurationType sso, String server, String username, String password) {
        String inner = Base64.encodeBase64URLSafeString((username + ":" + password).getBytes());
        String sub = Base64.encodeBase64URLSafeString((username + "(" + inner + ")").getBytes());

        JwtToken token = new JwtToken(server, sso.getQueryHash().getPath(), sso.getQueryHash().getMethod(), new Long(sso.getExpiry())).sub(sub);

        Cookie _jwtauth = new Cookie(sso.getCookieName(), token.token(sso.getSignature()));
        _jwtauth.setMaxAge(sso.getExpiry());
        _jwtauth.setPath("/");
        return _jwtauth;
    }

    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication)
        throws IOException, ServletException {

        SsoConfigurationType sso = tenantConfig.getSsoConfiguration();

        if (sso.isEnabled()) {
            response.addCookie(buildJwtToken(sso, request.getServerName(), request.getParameter("username"), request.getParameter("password")));
        }

        response.sendRedirect(request.getContextPath());
    }
}