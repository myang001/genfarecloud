package com.genfare.cloud.config.sso.handlers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.SimpleUrlLogoutSuccessHandler;

import com.genfare.cloud.model.tenant.TenantConfiguration;

public class JwtLogoutHandler extends SimpleUrlLogoutSuccessHandler {

    private TenantConfiguration tenantConfig;

    public void setTenantConfiguration(TenantConfiguration tenantConfig) {
        this.tenantConfig = tenantConfig;
    }

    @Override
    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication)
        throws IOException, ServletException {

        Cookie[] cookies = request.getCookies();

        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals(tenantConfig.getSsoConfiguration().getCookieName())) {
                    cookie.setValue(null);
                    cookie.setMaxAge(0);
                    cookie.setPath("/");
                    response.addCookie(cookie);
                }
            }
        }
        super.onLogoutSuccess(request, response, authentication);
    }

}
