package com.genfare.cloud.config.mvc;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import nz.net.ultraq.thymeleaf.LayoutDialect;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableArgumentResolver;
import org.springframework.format.FormatterRegistry;
import org.springframework.validation.Validator;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.mvc.method.annotation.ServletWebArgumentResolverAdapter;
import org.thymeleaf.extras.springsecurity3.dialect.SpringSecurityDialect;
import org.thymeleaf.messageresolver.AbstractMessageResolver;
import org.thymeleaf.spring3.SpringTemplateEngine;
import org.thymeleaf.spring3.messageresolver.SpringMessageResolver;
import org.thymeleaf.spring3.view.ThymeleafViewResolver;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.thymeleaf.templateresolver.ServletContextTemplateResolver;
import org.thymeleaf.templateresolver.TemplateResolver;

import com.genfare.cloud.model.converters.DateToStringConverter;
import com.genfare.cloud.model.converters.StringToDateConverter;
import com.genfare.cloud.model.entity.repository.ModulesRepository;
import com.genfare.cloud.model.interceptor.AdminModelHandlerInterceptor;
import com.genfare.cloud.model.service.UserManagementService;

/**
 * Abstract Base configuration class for MVC projects
 * 
 * <p>
 * The abstract class sets up a basic MVC style configuration, complete with the configuration for the Thymeleaf Engine. <br>
 * <br>
 * When adding a new web project, extend this class instead of the {@code WebMvcConfigurerAdaptor} and then {@code implement} or
 * {@code override} the necessary methods with application specific values
 * </p>
 * 
 * @author jyore
 */
@Configuration
public abstract class AbstractBaseWebMvcConfigurerAdapter extends WebMvcConfigurerAdapter {

    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractBaseWebMvcConfigurerAdapter.class);

    // TODO: Determine tenant to replace 'test' in URL
    private static final String URL_VIEWS = "https://s3.amazonaws.com/genfare-tenant/test/resources/views/";

    private static final String LOCALE = "/resources/locale/messages";
    private static final int MESSAGE_SOURCE_CACHE_SECONDS = 15;
    private static final String RESOURCES_HANDLER = "/resources/";
    private static final String RESOURCES_LOCATION = RESOURCES_HANDLER + "**";
    private static final String VIEWS = "/resources/views/";
    private static final String EMAILS = "mail/";

    @Autowired
    UserManagementService userManagementService;
    @Autowired
    ModulesRepository modulesRepository;

    @Override
    public void configureDefaultServletHandling(final DefaultServletHandlerConfigurer configurer) {
        configurer.enable();
    }

    @Override
    public void addResourceHandlers(final ResourceHandlerRegistry registry) {
        registry.addResourceHandler(RESOURCES_HANDLER).addResourceLocations(RESOURCES_LOCATION);
    }

    @Override
    public Validator getValidator() {
        LocalValidatorFactoryBean validator = new LocalValidatorFactoryBean();
        validator.setValidationMessageSource(messageSource());
        return validator;
    }

    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
        PageableArgumentResolver resolver = new PageableArgumentResolver();
        resolver.setFallbackPageable(new PageRequest(1, 5));
        argumentResolvers.add(new ServletWebArgumentResolverAdapter(resolver));
    }

    @Bean(name = "messageSource")
    public MessageSource messageSource() {
        ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
        messageSource.setBasenames(LOCALE);
        messageSource.setCacheSeconds(MESSAGE_SOURCE_CACHE_SECONDS);
        return messageSource;
    }

    @Override
    public void addFormatters(FormatterRegistry registry) {
        super.addFormatters(registry);
        registry.addConverter(new DateToStringConverter());
        registry.addConverter(new StringToDateConverter());
    }

    protected Set<TemplateResolver> templateResolvers() {
        Set<TemplateResolver> resolvers = new HashSet<TemplateResolver>();
        resolvers.add(new TemplateResolverBuilder(EMAILS, ".html", 1).resolverClass(ClassLoaderTemplateResolver.class).build());
        resolvers.add(new TemplateResolverBuilder(VIEWS, ".html", 2).cacheable(false).build());
        resolvers.add(new TemplateResolverBuilder(URL_VIEWS, ".html", 3).cacheable(true).build());// TODO: switch order for
                                                                                                  // urlTemplateResolver and
                                                                                                  // webTemplateResolver when s3
        // integrated with maven build process, this needs to be fixed for non-development environments
        return resolvers;
    }

    protected Set<AbstractMessageResolver> messageResolvers() {
        Set<AbstractMessageResolver> resolvers = new HashSet<AbstractMessageResolver>();

        SpringMessageResolver spring = new SpringMessageResolver();
        spring.setMessageSource(messageSource());

        resolvers.add(spring);
        return resolvers;
    }

    @Bean
    public SpringTemplateEngine templateEngine() {
        SpringTemplateEngine templateEngine = new SpringTemplateEngine();
        templateEngine.setTemplateResolvers(templateResolvers());
        templateEngine.setMessageResolvers(messageResolvers());
        templateEngine.addDialect(new LayoutDialect());
        templateEngine.addDialect(new SpringSecurityDialect());

        return templateEngine;
    }

    @Bean
    public ThymeleafViewResolver viewResolver() {
        ThymeleafViewResolver thymeleafViewResolver = new ThymeleafViewResolver();
        thymeleafViewResolver.setTemplateEngine(templateEngine());
        thymeleafViewResolver.setCharacterEncoding("UTF-8");
        return thymeleafViewResolver;
    }

    public class TemplateResolverBuilder {
        private String prefix;
        private String suffix;
        private int order;
        private String templateMode = "HTML5";
        private String characterEncoding = "UTF-8";
        private boolean cacheable = true;
        private Class<? extends TemplateResolver> resolverClass = ServletContextTemplateResolver.class;

        public TemplateResolverBuilder(String prefix, String suffix, int order) {
            this.prefix = prefix;
            this.suffix = suffix;
            this.order = order;
        }

        public TemplateResolverBuilder templateMode(String templateMode) {
            this.templateMode = templateMode;
            return this;
        }

        public TemplateResolverBuilder resolverClass(Class<? extends TemplateResolver> resolverClass) {
            this.resolverClass = resolverClass;
            return this;
        }

        public TemplateResolverBuilder characterEncoding(String characterEncoding) {
            this.characterEncoding = characterEncoding;
            return this;
        }

        public TemplateResolverBuilder cacheable(boolean cacheable) {
            this.cacheable = cacheable;
            return this;
        }

        public TemplateResolver build() {
            TemplateResolver templateResolver;
            try {
                templateResolver = resolverClass.newInstance();
            } catch (InstantiationException | IllegalAccessException e) {
                LOGGER.error("Invalid class type for TemplateResolver", e);
                return null;
            }

            templateResolver.setPrefix(prefix);
            templateResolver.setSuffix(suffix);
            templateResolver.setTemplateMode(templateMode);
            templateResolver.setOrder(order);
            templateResolver.setCacheable(cacheable);
            templateResolver.setCharacterEncoding(characterEncoding);
            return templateResolver;
        }

    }

    public void addInterceptors(InterceptorRegistry registry) {
        AdminModelHandlerInterceptor interceptor = new AdminModelHandlerInterceptor();
        interceptor.setUserManagementService(userManagementService);
        interceptor.setModulesRepository(modulesRepository);
        registry.addInterceptor(interceptor);
    }
}
