package com.genfare.cloud.config.security;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.logout.LogoutFilter;
import org.springframework.social.security.SocialUserDetailsService;

import com.genfare.cloud.config.sso.handlers.JwtAuthenticationHandler;
import com.genfare.cloud.config.sso.handlers.JwtAuthenticationSuccessHandler;
import com.genfare.cloud.config.sso.handlers.JwtLogoutHandler;
import com.genfare.cloud.model.exception.EmailNotVerifiedException;
import com.genfare.cloud.model.service.LoginFailureHandler;
import com.genfare.cloud.model.service.SimpleSocialUserDetailsService;
import com.genfare.cloud.model.service.UserDetailsServiceImpl;
import com.genfare.cloud.model.tenant.TenantConfiguration;

@Configuration
public abstract class AbstractBaseWebSecurityConfigurerAdapter extends WebSecurityConfigurerAdapter {

    @Autowired
    protected TenantConfiguration tenantConfiguration;

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/resources/**");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // @formatter:off
        http
            .formLogin()
            .loginPage("/login")
            .usernameParameter("username")
            .passwordParameter("password")
            .failureUrl("/login?error")
            .failureHandler(authenticationFailureHandler())
            .permitAll()
            .and().logout()
            .deleteCookies("JSESSIONID")
            .logoutUrl("/logout")
            .logoutSuccessUrl("/");
        // @formatter:on
    }

    protected void enableSSO(HttpSecurity http) throws Exception {

        JwtAuthenticationHandler authHandler = new JwtAuthenticationHandler();
        JwtAuthenticationSuccessHandler successHandler = new JwtAuthenticationSuccessHandler();
        JwtLogoutHandler logoutHandler = new JwtLogoutHandler();

        authHandler.setTenantConfiguration(tenantConfiguration);
        successHandler.setTenantConfiguration(tenantConfiguration);
        logoutHandler.setTenantConfiguration(tenantConfiguration);

        http.addFilterBefore(authHandler, LogoutFilter.class);
        http.formLogin().successHandler(successHandler);
        http.logout().logoutSuccessHandler(logoutHandler);
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth)
        throws Exception {
        auth.userDetailsService(userDetailsService()).passwordEncoder(passwordEncoder());
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder(10);
    }

    @Bean
    public SocialUserDetailsService socialUserDetailsService() {
        return new SimpleSocialUserDetailsService(userDetailsService());
    }

    @Override
    @Bean
    public UserDetailsService userDetailsService() {
        return new UserDetailsServiceImpl();
    }

    @Bean
    public LoginFailureHandler authenticationFailureHandler() {
        LoginFailureHandler handler = new LoginFailureHandler();
        Map<String, String> map = new HashMap<String, String>();
        map.put(BadCredentialsException.class.getName(), "/login?error=Incorrect%20email%20or%20password");
        map.put(EmailNotVerifiedException.class.getName(), "/login?error=Account%20not%20verified");
        map.put(DisabledException.class.getName(), "/login?error=Account%20disable");
        handler.setExceptionMappings(map);
        handler.setDefaultFailureUrl("/login?error");

        return handler;
    }
}
