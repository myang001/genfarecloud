package com.genfare.cloud.config.sso.handlers;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.StringUtils;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

import com.genfare.cloud.model.security.jwt.JwtClaims;
import com.genfare.cloud.model.security.jwt.JwtToken;
import com.genfare.cloud.model.tenant.TenantConfiguration;
import com.genfare.cloud.model.tenant.impl.SsoConfigurationType;

@Component
public class JwtAuthenticationHandler extends GenericFilterBean {

    private TenantConfiguration tenantConfig;

    public void setTenantConfiguration(TenantConfiguration tenantConfig) {
        this.tenantConfig = tenantConfig;
    }

    private Authentication getAuth(SsoConfigurationType sso, HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth == null) {
            Cookie[] cookies = request.getCookies();

            if (cookies != null) {
                for (Cookie cookie : cookies) {
                    if (cookie.getName().equals(sso.getCookieName())) {
                        String _jwtauth = cookie.getValue();
                        if (JwtToken.verify(_jwtauth, sso.getSignature()) && JwtToken.verifyQueryHash(_jwtauth, sso.getQueryHash().getPath(), sso.getQueryHash().getMethod())) {
                            JwtClaims claims = JwtToken.parseClaims(_jwtauth);
                            String sub = claims.getSub();
                            String outer = StringUtils.newStringUtf8(Base64.decodeBase64(sub));
                            String inner = outer.substring(outer.indexOf("(") + 1, outer.indexOf(")"));
                            String[] details = StringUtils.newStringUtf8(Base64.decodeBase64(inner)).split(":");

                            if (details.length != 2) {
                                break;
                            }
                            return new UsernamePasswordAuthenticationToken(details[0], details[1]);
                        } else {
                            cookie.setValue(null);
                            cookie.setMaxAge(0);
                            cookie.setPath("/");
                            response.addCookie(cookie);
                        }
                    }
                }
            }
        }
        return auth;
    }

    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
        SsoConfigurationType sso = tenantConfig.getSsoConfiguration();
        if (sso.isEnabled()) {
            SecurityContextHolder.getContext().setAuthentication(getAuth(sso, (HttpServletRequest) req, (HttpServletResponse) res));
        }
        chain.doFilter(req, res);
    }

}