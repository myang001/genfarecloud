package com.genfare.cloud.config.mvc;

import java.util.EnumSet;

import javax.servlet.DispatcherType;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import org.springframework.orm.jpa.support.OpenEntityManagerInViewFilter;
import org.springframework.security.web.session.HttpSessionEventPublisher;
import org.springframework.web.context.request.RequestContextListener;
import org.springframework.web.filter.DelegatingFilterProxy;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

public abstract class AbstractBaseWebAppInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

    @Override
    protected String[] getServletMappings() {
        return new String[] { "/*" };
    }

    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
        super.onStartup(servletContext);
        servletContext.addListener(RequestContextListener.class);
        servletContext.addListener(HttpSessionEventPublisher.class);

        EnumSet<DispatcherType> dispatcherTypes = EnumSet.of(DispatcherType.REQUEST, DispatcherType.FORWARD);

        javax.servlet.FilterRegistration.Dynamic auditRequest =
            servletContext.addFilter("domainRequestFilter", DelegatingFilterProxy.class);
        auditRequest.addMappingForUrlPatterns(dispatcherTypes, false, "/*");

        javax.servlet.FilterRegistration.Dynamic openEntityManagerInViewFilter =
            servletContext.addFilter("openEntityManagerInViewFilter", OpenEntityManagerInViewFilter.class);
        openEntityManagerInViewFilter.addMappingForUrlPatterns(dispatcherTypes, false, "/*");

        javax.servlet.FilterRegistration.Dynamic sessionAuditRequest =
            servletContext.addFilter("sessionFilter", DelegatingFilterProxy.class);
        sessionAuditRequest.addMappingForUrlPatterns(dispatcherTypes, false, "/*");

    }
}
