CREATE OR REPLACE VIEW GenRidershipSummaryDaily AS
SELECT wt.Description AS MediaType, tt.Description AS FareProduct, sl.Description AS ServiceLevel, a.CountUse, d.DateTransit
FROM activitysummary a
INNER JOIN wallettypes wt ON a.WalletTypeID = wt.WalletTypeID
INNER JOIN tickettypes tt ON tt.TicketTypeID = a.TicketTypeID
INNER JOIN servicelevels sl ON a.ServiceLevelID = sl.ServiceLevelID
INNER JOIN dates d ON d.DateID = a.DateID;

-- Daily 
CREATE OR REPLACE VIEW GenRidershipSummaryDaily AS
SELECT wt.Description AS MediaType, tt.Description AS FareProduct, sl.Description AS ServiceLevel, a.CountUse, d.DateTransit
FROM activitysummary a
INNER JOIN wallettypes wt ON a.WalletTypeID = wt.WalletTypeID
INNER JOIN tickettypes tt ON tt.TicketTypeID = a.TicketTypeID
INNER JOIN servicelevels sl ON a.ServiceLevelID = sl.ServiceLevelID
INNER JOIN dates d ON d.DateID = a.DateID;


-- Monthly
CREATE OR REPLACE VIEW GenRidershipSummaryMonthly AS
SELECT wt.Description AS MediaType, tt.Description AS FareProduct, sl.Description AS ServiceLevel, a.CountUse, a.month, a.year
FROM activitymonthly a
INNER JOIN wallettypes wt ON a.WalletTypeID = wt.WalletTypeID
INNER JOIN tickettypes tt ON tt.TicketTypeID = a.TicketTypeID
INNER JOIN servicelevels sl ON a.ServiceLevelID = sl.ServiceLevelID;

-- Yearly
CREATE OR REPLACE VIEW GenRidershipSummaryYearly AS
SELECT wt.Description AS MediaType, tt.Description AS FareProduct, sl.Description AS ServiceLevel, a.CountUse, a.year
FROM activityyearly a
INNER JOIN wallettypes wt ON a.WalletTypeID = wt.WalletTypeID
INNER JOIN tickettypes tt ON tt.TicketTypeID = a.TicketTypeID
INNER JOIN servicelevels sl ON a.ServiceLevelID = sl.ServiceLevelID;
