---------------------------Create Groups for the Org-----------------------------------------------------------------------
INSERT INTO groups (OrganizationID,Role,Name,Description,Status,TouchedBy,CreatedBy) VALUES
	('1','ROLE_GFI_DEFAULT','GFI Default','GFI Default group includes all users within GFI','1','root@localhost','root@localhost');
INSERT INTO groups (OrganizationID,Role,Name,Description,Status,TouchedBy,CreatedBy) VALUES
	('1','ROLE_GFI_ADMIN','GFI Admin','GFI Administrator','1','root@localhost','root@localhost');
INSERT INTO groups (OrganizationID,Role,Name,Description,Status,TouchedBy,CreatedBy) VALUES
	('1','ROLE_GFI_STAFF','GFI Staff','Staff Member in GFI','1','root@localhost','root@localhost');
INSERT INTO groups (OrganizationID,Role,Name,Description,Status,TouchedBy,CreatedBy) VALUES
	('1','ROLE_GFI_USER','GFI User','Network user in GFI','1','root@localhost','root@localhost');
--UPDATE groups SET Role='ROLE_GFI_SUPER_USER',Name='GFI Super User',Description='GFI Super User' WHERE groupid=2;
--Until permissions are sorted out, leave ROLE_SUPER_USER alone
---------------------------------------------------------------------------------------------------------------------------


---------------------------Assign Group Priveleges-------------------------------------------------------------------------
INSERT INTO groupprivileges (GroupID,PrivilegeID,TouchedBy,CreatedBy) VALUES
        ((select groupid from groups where name='GFI Default'),1,'root@localhost','root@localhost');
INSERT INTO groupprivileges (GroupID,PrivilegeID,TouchedBy,CreatedBy) VALUES
        ((select groupid from groups where name='GFI Admin'),1,'root@localhost','root@localhost');
INSERT INTO groupprivileges (GroupID,PrivilegeID,TouchedBy,CreatedBy) VALUES
        ((select groupid from groups where name='GFI Admin'),2,'root@localhost','root@localhost');
INSERT INTO groupprivileges (GroupID,PrivilegeID,TouchedBy,CreatedBy) VALUES
        ((select groupid from groups where name='GFI Admin'),3,'root@localhost','root@localhost');
INSERT INTO groupprivileges (GroupID,PrivilegeID,TouchedBy,CreatedBy) VALUES
        ((select groupid from groups where name='GFI Admin'),4,'root@localhost','root@localhost');
INSERT INTO groupprivileges (GroupID,PrivilegeID,TouchedBy,CreatedBy) VALUES
        ((select groupid from groups where name='GFI Admin'),5,'root@localhost','root@localhost');
INSERT INTO groupprivileges (GroupID,PrivilegeID,TouchedBy,CreatedBy) VALUES
        ((select groupid from groups where name='GFI Admin'),6,'root@localhost','root@localhost');
INSERT INTO groupprivileges (GroupID,PrivilegeID,TouchedBy,CreatedBy) VALUES
        ((select groupid from groups where name='GFI Admin'),7,'root@localhost','root@localhost');
INSERT INTO groupprivileges (GroupID,PrivilegeID,TouchedBy,CreatedBy) VALUES
        ((select groupid from groups where name='GFI Admin'),8,'root@localhost','root@localhost');
INSERT INTO groupprivileges (GroupID,PrivilegeID,TouchedBy,CreatedBy) VALUES
        ((select groupid from groups where name='GFI Admin'),9,'root@localhost','root@localhost');
INSERT INTO groupprivileges (GroupID,PrivilegeID,TouchedBy,CreatedBy) VALUES
        ((select groupid from groups where name='GFI Admin'),10,'root@localhost','root@localhost');
INSERT INTO groupprivileges (GroupID,PrivilegeID,TouchedBy,CreatedBy) VALUES
        ((select groupid from groups where name='GFI Admin'),11,'root@localhost','root@localhost');
INSERT INTO groupprivileges (GroupID,PrivilegeID,TouchedBy,CreatedBy) VALUES
        ((select groupid from groups where name='GFI Admin'),12,'root@localhost','root@localhost');
INSERT INTO groupprivileges (GroupID,PrivilegeID,TouchedBy,CreatedBy) VALUES
        ((select groupid from groups where name='GFI Staff'),1,'root@localhost','root@localhost');
INSERT INTO groupprivileges (GroupID,PrivilegeID,TouchedBy,CreatedBy) VALUES
        ((select groupid from groups where name='GFI Staff'),2,'root@localhost','root@localhost');
INSERT INTO groupprivileges (GroupID,PrivilegeID,TouchedBy,CreatedBy) VALUES
        ((select groupid from groups where name='GFI Staff'),3,'root@localhost','root@localhost');
INSERT INTO groupprivileges (GroupID,PrivilegeID,TouchedBy,CreatedBy) VALUES
        ((select groupid from groups where name='GFI Staff'),4,'root@localhost','root@localhost');
INSERT INTO groupprivileges (GroupID,PrivilegeID,TouchedBy,CreatedBy) VALUES
        ((select groupid from groups where name='GFI Staff'),5,'root@localhost','root@localhost');
INSERT INTO groupprivileges (GroupID,PrivilegeID,TouchedBy,CreatedBy) VALUES
        ((select groupid from groups where name='GFI User'),1,'root@localhost','root@localhost');
INSERT INTO groupprivileges (GroupID,PrivilegeID,TouchedBy,CreatedBy) VALUES
        ((select groupid from groups where name='GFI User'),2,'root@localhost','root@localhost');
--INSERT INTO groupprivileges (GroupID,PrivilegeID,TouchedBy,CreatedBy) VALUES
--        ((select groupid from groups where name='GFI Super User'),1,'root@localhost','root@localhost');
--INSERT INTO groupprivileges (GroupID,PrivilegeID,TouchedBy,CreatedBy) VALUES
--        ((select groupid from groups where name='GFI Super User'),2,'root@localhost','root@localhost');
--INSERT INTO groupprivileges (GroupID,PrivilegeID,TouchedBy,CreatedBy) VALUES
--        ((select groupid from groups where name='GFI Super User'),3,'root@localhost','root@localhost');
--INSERT INTO groupprivileges (GroupID,PrivilegeID,TouchedBy,CreatedBy) VALUES
--        ((select groupid from groups where name='GFI Super User'),4,'root@localhost','root@localhost');
--INSERT INTO groupprivileges (GroupID,PrivilegeID,TouchedBy,CreatedBy) VALUES
--        ((select groupid from groups where name='GFI Super User'),5,'root@localhost','root@localhost');
--INSERT INTO groupprivileges (GroupID,PrivilegeID,TouchedBy,CreatedBy) VALUES
--        ((select groupid from groups where name='GFI Super User'),6,'root@localhost','root@localhost');
--INSERT INTO groupprivileges (GroupID,PrivilegeID,TouchedBy,CreatedBy) VALUES
--        ((select groupid from groups where name='GFI Super User'),7,'root@localhost','root@localhost');
--INSERT INTO groupprivileges (GroupID,PrivilegeID,TouchedBy,CreatedBy) VALUES
--        ((select groupid from groups where name='GFI Super User'),8,'root@localhost','root@localhost');
--INSERT INTO groupprivileges (GroupID,PrivilegeID,TouchedBy,CreatedBy) VALUES
--        ((select groupid from groups where name='GFI Super User'),9,'root@localhost','root@localhost');
---------------------------------------------------------------------------------------------------------------------------


---------------------------Create Admin to add to groups-------------------------------------------------------------------
INSERT INTO people (PersonID,Prefix,NameLast,NameMiddle,NameFirst,Suffix,DateOfBirth,Gender,Status,LanguageID,TouchedBy,CreatedBy) VALUES
        ((select max(p.personid) + 1 from people p),'','Admin','','GFI','','1990-01-01','M','1','1','root@localhost','root@localhost');
INSERT INTO peopleaddresses(PersonID,AddressTypeID,AddressLine1,AddressLine2,City,State,PostalCode,Country,TouchedBy,CreatedBy) VALUES
        ((select max(p.personid) from people p),1,'800 Arthur Ave.','','Elk Grove Village','IL','60007','US','root@localhost','root@localhost');
INSERT INTO peoplelinks (PersonID,LinkTypeID,LinkValue,IsPreferred,TouchedBy,CreatedBy) VALUES
        ((select max(p.personid) from people p),6,'admin@gfi.com','F','root@localhost','root@localhost');
INSERT INTO peoplelinks (PersonID,LinkTypeID,LinkValue,IsPreferred,TouchedBy,CreatedBy) VALUES
        ((select max(p.personid) from people p),2,'4086203827','T','root@localhost','root@localhost');
INSERT INTO logins (LoginID,PersonID,UserLogin,TouchedBy,CreatedBy) VALUES 
        ((select max(l.loginid)+1 from logins l),(select max(p.personid) from people p),'admin@gfi.com','root@localhost','root@localhost');
INSERT INTO authentication (PersonID,AuthenticationTypeID,Response,TouchedBy,CreatedBy) VALUES 
        ((select max(p.personid) from people p),1,'$2a$10$zlA213tFDRqOH0ffTtmnoe6Lp2qSdOUZxVzXbUSpygDjrvU4pxDSK','root@localhost','root@localhost');
---------------------------------------------------------------------------------------------------------------------------


---------------------------Add Admin To Groups-----------------------------------------------------------------------------
INSERT INTO groupsofpeople (GroupID,PersonID,TouchedBy,CreatedBy) VALUES
        ((select groupid from groups where name='GFI Default'),(select max(p.personid) from people p),'root@localhost','root@localhost');
INSERT INTO groupsofpeople (GroupID,PersonID,TouchedBy,CreatedBy) VALUES
        ((select groupid from groups where name='GFI Admin'),(select max(p.personid) from people p),'root@localhost','root@localhost');
INSERT INTO groupsofpeople (GroupID,PersonID,TouchedBy,CreatedBy) VALUES
        ((select groupid from groups where name='GFI Staff'),(select max(p.personid) from people p),'root@localhost','root@localhost');
INSERT INTO groupsofpeople (GroupID,PersonID,TouchedBy,CreatedBy) VALUES
        ((select groupid from groups where name='GFI User'),(select max(p.personid) from people p),'root@localhost','root@localhost');
--INSERT INTO groupsofpeople (GroupID,PersonID,TouchedBy,CreatedBy) VALUES
--        ((select groupid from groups where name='GFI Super User'),(select max(p.personid) from people p),'root@localhost','root@localhost');
UPDATE organizations SET maincontact=(select max(p.personid) from people p) WHERE organizationid='1';
---------------------------------------------------------------------------------------------------------------------------


---------------------------Create Normal User and add to groups------------------------------------------------------------
INSERT INTO people (PersonID,Prefix,NameLast,NameMiddle,NameFirst,Suffix,DateOfBirth,Gender,Status,LanguageID,TouchedBy,CreatedBy) VALUES
        ((select max(p.personid) + 1 from people p),'','User','','GFI','','1990-01-01','M','1','1','root@localhost','root@localhost');
INSERT INTO peopleaddresses(PersonID,AddressTypeID,AddressLine1,AddressLine2,City,State,PostalCode,Country,TouchedBy,CreatedBy) VALUES
        ((select max(p.personid) from people p),1,'800 Arthur Ave.','','Elk Grove Village','IL','60007','US','root@localhost','root@localhost');
INSERT INTO peoplelinks (PersonID,LinkTypeID,LinkValue,IsPreferred,TouchedBy,CreatedBy) VALUES
        ((select max(p.personid) from people p),6,'user01@gfi.com','F','root@localhost','root@localhost');
INSERT INTO peoplelinks (PersonID,LinkTypeID,LinkValue,IsPreferred,TouchedBy,CreatedBy) VALUES
        ((select max(p.personid) from people p),2,'5086203827','T','root@localhost','root@localhost');
INSERT INTO logins (LoginID,PersonID,UserLogin,TouchedBy,CreatedBy) VALUES 
        ((select max(l.loginid)+1 from logins l),(select max(p.personid) from people p),'user01@gfi.com','root@localhost','root@localhost');
INSERT INTO authentication (PersonID,AuthenticationTypeID,Response,TouchedBy,CreatedBy) VALUES 
        ((select max(p.personid) from people p),1,'$2a$10$zlA213tFDRqOH0ffTtmnoe6Lp2qSdOUZxVzXbUSpygDjrvU4pxDSK','root@localhost','root@localhost');
INSERT INTO groupsofpeople (GroupID,PersonID,TouchedBy,CreatedBy) VALUES
        ((select groupid from groups where name='GFI Default'),(select max(p.personid) from people p),'root@localhost','root@localhost');
INSERT INTO groupsofpeople (GroupID,PersonID,TouchedBy,CreatedBy) VALUES
        ((select groupid from groups where name='GFI User'),(select max(p.personid) from people p),'root@localhost','root@localhost');
---------------------------------------------------------------------------------------------------------------------------


---------------------------Create Staff Member and add to groups------------------------------------------------------------
INSERT INTO people (PersonID,Prefix,NameLast,NameMiddle,NameFirst,Suffix,DateOfBirth,Gender,Status,LanguageID,TouchedBy,CreatedBy) VALUES
        ((select max(p.personid) + 1 from people p),'','Staff','','GFI','','1990-01-01','M','1','1','root@localhost','root@localhost');
INSERT INTO peopleaddresses(PersonID,AddressTypeID,AddressLine1,AddressLine2,City,State,PostalCode,Country,TouchedBy,CreatedBy) VALUES
        ((select max(p.personid) from people p),1,'800 Arthur Ave.','','Elk Grove Village','IL','60007','US','root@localhost','root@localhost');
INSERT INTO peoplelinks (PersonID,LinkTypeID,LinkValue,IsPreferred,TouchedBy,CreatedBy) VALUES
        ((select max(p.personid) from people p),6,'user02@gfi.com','F','root@localhost','root@localhost');
INSERT INTO peoplelinks (PersonID,LinkTypeID,LinkValue,IsPreferred,TouchedBy,CreatedBy) VALUES
        ((select max(p.personid) from people p),2,'6086203827','T','root@localhost','root@localhost');
INSERT INTO logins (LoginID,PersonID,UserLogin,TouchedBy,CreatedBy) VALUES 
        ((select max(l.loginid)+1 from logins l),(select max(p.personid) from people p),'user02@gfi.com','root@localhost','root@localhost');
INSERT INTO authentication (PersonID,AuthenticationTypeID,Response,TouchedBy,CreatedBy) VALUES 
        ((select max(p.personid) from people p),1,'$2a$10$zlA213tFDRqOH0ffTtmnoe6Lp2qSdOUZxVzXbUSpygDjrvU4pxDSK','root@localhost','root@localhost');
INSERT INTO groupsofpeople (GroupID,PersonID,TouchedBy,CreatedBy) VALUES
        ((select groupid from groups where name='GFI Default'),(select max(p.personid) from people p),'root@localhost','root@localhost');
INSERT INTO groupsofpeople (GroupID,PersonID,TouchedBy,CreatedBy) VALUES
        ((select groupid from groups where name='GFI User'),(select max(p.personid) from people p),'root@localhost','root@localhost');
INSERT INTO groupsofpeople (GroupID,PersonID,TouchedBy,CreatedBy) VALUES
        ((select groupid from groups where name='GFI Staff'),(select max(p.personid) from people p),'root@localhost','root@localhost');
---------------------------------------------------------------------------------------------------------------------------


---------------------------Create Super User and add to groups------------------------------------------------------------
INSERT INTO people (PersonID,Prefix,NameLast,NameMiddle,NameFirst,Suffix,DateOfBirth,Gender,Status,LanguageID,TouchedBy,CreatedBy) VALUES
        ((select max(p.personid) + 1 from people p),'','SuperUser','','GFI','','1990-01-01','M','1','1','root@localhost','root@localhost');
INSERT INTO peopleaddresses(PersonID,AddressTypeID,AddressLine1,AddressLine2,City,State,PostalCode,Country,TouchedBy,CreatedBy) VALUES
        ((select max(p.personid) from people p),1,'800 Arthur Ave.','','Elk Grove Village','IL','60007','US','root@localhost','root@localhost');
INSERT INTO peoplelinks (PersonID,LinkTypeID,LinkValue,IsPreferred,TouchedBy,CreatedBy) VALUES
        ((select max(p.personid) from people p),6,'user03@gfi.com','F','root@localhost','root@localhost');
INSERT INTO peoplelinks (PersonID,LinkTypeID,LinkValue,IsPreferred,TouchedBy,CreatedBy) VALUES
        ((select max(p.personid) from people p),2,'7086203827','T','root@localhost','root@localhost');
INSERT INTO logins (LoginID,PersonID,UserLogin,TouchedBy,CreatedBy) VALUES 
        ((select max(l.loginid)+1 from logins l),(select max(p.personid) from people p),'user03@gfi.com','root@localhost','root@localhost');
INSERT INTO authentication (PersonID,AuthenticationTypeID,Response,TouchedBy,CreatedBy) VALUES 
        ((select max(p.personid) from people p),1,'$2a$10$zlA213tFDRqOH0ffTtmnoe6Lp2qSdOUZxVzXbUSpygDjrvU4pxDSK','root@localhost','root@localhost');
INSERT INTO groupsofpeople (GroupID,PersonID,TouchedBy,CreatedBy) VALUES
        ((select groupid from groups where name='GFI Default'),(select max(p.personid) from people p),'root@localhost','root@localhost');
INSERT INTO groupsofpeople (GroupID,PersonID,TouchedBy,CreatedBy) VALUES
        ((select groupid from groups where name='GFI User'),(select max(p.personid) from people p),'root@localhost','root@localhost');
--INSERT INTO groupsofpeople (GroupID,PersonID,TouchedBy,CreatedBy) VALUES
--        ((select groupid from groups where name='GFI Super User'),(select max(p.personid) from people p),'root@localhost','root@localhost');
---------------------------------------------------------------------------------------------------------------------------
