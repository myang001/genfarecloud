-- 
-- TABLE: GroupStatus 
--

CREATE TABLE GroupStatus(
    Status         TINYINT(3) UNSIGNED NOT NULL,
    Name           VARCHAR(50)    NOT NULL,
    TouchedBy      VARCHAR(50)    NOT NULL,
    TouchedAt      DATETIME       DEFAULT NOW() NOT NULL,
    CreatedBy      VARCHAR(50)    NOT NULL,
    CreatedAt      DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (Status)
)ENGINE=INNODB
;

INSERT INTO GroupStatus (Status,Name,TouchedBy,CreatedBy) VALUES (1,'Active','hliu','hliu');
INSERT INTO GroupStatus (Status,Name,TouchedBy,CreatedBy) VALUES (2,'Disabled','hliu','hliu');

ALTER TABLE Groups
	ADD CONSTRAINT RefGroupStatuses
		FOREIGN KEY (Status)
		REFERENCES GroupStatus(Status)
;
