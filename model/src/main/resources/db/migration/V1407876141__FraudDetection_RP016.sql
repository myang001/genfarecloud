-- Fraud Detection Report - VIEW
CREATE OR REPLACE VIEW frauddetection AS
SELECT walletIdentifier, TicketDescription, MediaType, Discount, AmtCharged, AmtRemaining, ActivityType,
 ViolationType, ValueActivity, EquipmentDesc, ActivityDate, Location 
 FROM temp_rp016;
 