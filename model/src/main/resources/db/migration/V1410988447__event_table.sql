-- 
-- Renaming the EventDetail and Event table for clarity
--

DROP TABLE IF EXISTS EventDetails, Events;



-- 
-- TABLE: EventDetails 
--

CREATE TABLE events(  
    ActivityID     INT            NOT NULL,
    Sequence       INT            NOT NULL,
    EventTypeID    SMALLINT       NOT NULL,
    EquipmentID    INT            NOT NULL,
    TSEvent        TIMESTAMP      NOT NULL,
    State          CHAR(10),
    TouchedBy      VARCHAR(50),
    TouchedAt      DATETIME       DEFAULT NOW() NOT NULL,
    PRIMARY KEY (ActivityID, Sequence)
)ENGINE=INNODB
;

-- 
-- TABLE: EventTypes 
--

CREATE TABLE eventTypes(
    EventTypeID        SMALLINT       AUTO_INCREMENT,
    Description    VARCHAR(50),
    TouchedBy      VARCHAR(50),
    TouchedAt      DATETIME       DEFAULT NOW() NOT NULL,
    PRIMARY KEY (EventTypeID)
)ENGINE=INNODB
;




CREATE INDEX Ref78117 ON Events(EventTypeID)
;
-- 
-- INDEX: Ref43118 
--

CREATE INDEX Ref43118 ON Events(EquipmentID)
;
-- 
-- INDEX: Ref81149 
--

CREATE INDEX Ref81149 ON Events(ActivityID)
;
-- 
-- INDEX: Ref2830 
--

-- 
-- TABLE: Events 
--

ALTER TABLE events ADD CONSTRAINT RefEvents117 
    FOREIGN KEY (EventTypeID)
    REFERENCES EventTypes(EventTypeID)
;

ALTER TABLE events ADD CONSTRAINT RefEquipment118 
    FOREIGN KEY (EquipmentID)
    REFERENCES Equipment(EquipmentID)
;

ALTER TABLE events ADD CONSTRAINT RefActivities149 
    FOREIGN KEY (ActivityID)
    REFERENCES Activities(ActivityID)
;
