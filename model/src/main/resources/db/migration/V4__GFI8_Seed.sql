INSERT INTO OrganizationTypes (OrganizationTypeID,Description,IsRetail,TouchedBy,TouchedAt) VALUES (1,'School','F','root@localhost','2014-03-21 13:35:01');
INSERT INTO OrganizationTypes (OrganizationTypeID,Description,IsRetail,TouchedBy,TouchedAt) VALUES (2,'Vendor','T','root@localhost','2014-03-21 13:40:40');
INSERT INTO OrganizationTypes (OrganizationTypeID,Description,IsRetail,TouchedBy,TouchedAt) VALUES (3,'Sponsoring Employer','F','root@localhost','2014-03-21 13:40:40');
INSERT INTO ORGANIZATIONTYPES (DESCRIPTION, TOUCHEDBY) VALUES ('Non Profit', CURRENT_USER());

INSERT INTO OrderStatuses (Status,Description,TouchedBy,TouchedAt) VALUES (1,'New Order','root@localhost','2014-03-21 14:54:34');
INSERT INTO OrderStatuses (Status,Description,TouchedBy,TouchedAt) VALUES (2,'Open Order','root@localhost','2014-03-21 14:54:47');
INSERT INTO OrderStatuses (Status,Description,TouchedBy,TouchedAt) VALUES (3,'In Shipping','root@localhost','2014-03-21 14:55:31');
INSERT INTO OrderStatuses (Status,Description,TouchedBy,TouchedAt) VALUES (4,'Closed Order','root@localhost','2014-03-21 14:55:46');

INSERT INTO AddressTypes (AddressTypeID,Description,TouchedBy,TouchedAt) VALUES (1,'Billing Address','root@localhost','2014-03-21 15:03:16');
INSERT INTO AddressTypes (AddressTypeID,Description,TouchedBy,TouchedAt) VALUES (2,'Shipping Address','root@localhost','2014-03-21 15:03:25');
INSERT INTO AddressTypes (AddressTypeID,Description,TouchedBy,TouchedAt) VALUES (3,'Home Address','root@localhost','2014-03-21 15:03:33');
INSERT INTO AddressTypes (AddressTypeID,Description,TouchedBy,TouchedAt) VALUES (4,'Work Address','root@localhost','2014-03-21 15:03:43');
INSERT INTO AddressTypes (AddressTypeID,Description,TouchedBy,TouchedAt) VALUES (5,'School Address','root@localhost','2014-03-21 15:03:53');

INSERT INTO LinkTypes (LinkTypeID,Description,TouchedBy,TouchedAt) VALUES (1,'Home Phone','root@localhost','2014-03-21 15:07:12');
INSERT INTO LinkTypes (LinkTypeID,Description,TouchedBy,TouchedAt) VALUES (2,'Work Phone','root@localhost','2014-03-21 15:07:19');
INSERT INTO LinkTypes (LinkTypeID,Description,TouchedBy,TouchedAt) VALUES (3,'Mobile Phone','root@localhost','2014-03-21 15:07:25');
INSERT INTO LinkTypes (LinkTypeID,Description,TouchedBy,TouchedAt) VALUES (4,'Work eMail','root@localhost','2014-03-21 15:07:44');
INSERT INTO LinkTypes (LinkTypeID,Description,TouchedBy,TouchedAt) VALUES (5,'Personal eMail','root@localhost','2014-03-21 15:07:58');

INSERT INTO PaymentTypes (PaymentTypeID,IsCredit,Description,TouchedBy,TouchedAt) VALUES (1,'F','Invoiced','root@localhost','2014-03-21 15:12:48');
INSERT INTO PaymentTypes (PaymentTypeID,IsCredit,Description,TouchedBy,TouchedAt) VALUES (2,'F','Cash','root@localhost','2014-03-21 15:12:56');
INSERT INTO PaymentTypes (PaymentTypeID,IsCredit,Description,TouchedBy,TouchedAt) VALUES (3,'F','Check','root@localhost','2014-03-21 15:13:01');
INSERT INTO PaymentTypes (PaymentTypeID,IsCredit,Description,TouchedBy,TouchedAt) VALUES (4,'T','American Express','root@localhost','2014-03-21 15:13:20');
INSERT INTO PaymentTypes (PaymentTypeID,IsCredit,Description,TouchedBy,TouchedAt) VALUES (5,'T','Visa','root@localhost','2014-03-21 15:13:34');
INSERT INTO PaymentTypes (PaymentTypeID,IsCredit,Description,TouchedBy,TouchedAt) VALUES (6,'T','Master Card','root@localhost','2014-03-21 15:13:45');

INSERT INTO disabilities (DisabilityID,Description,TouchedBy,TouchedAt) VALUES (1,'Visually Impaired','root@localhost','2014-03-25 14:58:54.');
INSERT INTO disabilities (DisabilityID,Description,TouchedBy,TouchedAt) VALUES (2,'Hearing Impaired','root@localhost','2014-03-25 14:59:17.');
INSERT INTO disabilities (DisabilityID,Description,TouchedBy,TouchedAt) VALUES (3,'Visual and Hearing Impaired','root@localhost','2014-03-25 14:59:37.');

INSERT INTO languages (LanguageID,Description,TouchedBy,TouchedAt) VALUES (1,'English','mmcgraw','2013-09-20 08:57:00.');
INSERT INTO languages (LanguageID,Description,TouchedBy,TouchedAt) VALUES (2,'French','mmcgraw','2013-09-20 08:57:00.');
INSERT INTO languages (LanguageID,Description,TouchedBy,TouchedAt) VALUES (3,'German','mmcgraw','2013-09-20 08:57:00.');
INSERT INTO languages (LanguageID,Description,TouchedBy,TouchedAt) VALUES (4,'Tagalong (Filipino)','mmcgraw','2013-09-20 08:57:00.');
INSERT INTO languages (LanguageID,Description,TouchedBy,TouchedAt) VALUES (5,'Ukrainian','mmcgraw','2013-09-20 08:57:00.');
INSERT INTO languages (LanguageID,Description,TouchedBy,TouchedAt) VALUES (6,'Spanish','mmcgraw','2013-09-20 08:57:00.');
INSERT INTO languages (LanguageID,Description,TouchedBy,TouchedAt) VALUES (7,'Cantonese','mmcgraw','2013-09-20 08:57:00.');
INSERT INTO languages (LanguageID,Description,TouchedBy,TouchedAt) VALUES (8,'Mandarin','mmcgraw','2013-09-20 08:57:00.');
INSERT INTO languages (LanguageID,Description,TouchedBy,TouchedAt) VALUES (9,'Polish','mmcgraw','2013-09-20 08:57:00.');
INSERT INTO languages (LanguageID,Description,TouchedBy,TouchedAt) VALUES (10,'Portugese','mmcgraw','2013-09-20 08:57:00.');
INSERT INTO languages (LanguageID,Description,TouchedBy,TouchedAt) VALUES (11,'Italian','mmcgraw','2013-09-20 08:57:00.');
INSERT INTO languages (LanguageID,Description,TouchedBy,TouchedAt) VALUES (12,'Punjabi','mmcgraw','2013-09-20 08:57:00.');
INSERT INTO languages (LanguageID,Description,TouchedBy,TouchedAt) VALUES (13,'Vietnamese','mmcgraw','2013-09-20 08:57:00.');
INSERT INTO languages (LanguageID,Description,TouchedBy,TouchedAt) VALUES (14,'Ojibway','mmcgraw','2013-09-20 08:57:00.');
INSERT INTO languages (LanguageID,Description,TouchedBy,TouchedAt) VALUES (15,'Hindi','mmcgraw','2013-09-20 08:57:00.');
INSERT INTO languages (LanguageID,Description,TouchedBy,TouchedAt) VALUES (16,'Russian','mmcgraw','2013-09-20 08:57:00.');
INSERT INTO languages (LanguageID,Description,TouchedBy,TouchedAt) VALUES (17,'Cree','mmcgraw','2013-09-20 08:57:00.');
INSERT INTO languages (LanguageID,Description,TouchedBy,TouchedAt) VALUES (18,'Dutch','mmcgraw','2013-09-20 08:57:00.');

INSERT INTO identifiertypes (IdentifierID,Description,TouchedBy,TouchedAt) VALUES (1,'Electronic ID','root@localhost','2014-03-27 16:17:51.');
INSERT INTO identifiertypes (IdentifierID,Description,TouchedBy,TouchedAt) VALUES (2,'Printed ID','root@localhost','2014-03-27 16:18:05.');
INSERT INTO identifiertypes (IdentifierID,Description,TouchedBy,TouchedAt) VALUES (3,'Track2 Magnetic Stripe','root@localhost','2014-03-27 16:18:34.');
INSERT INTO identifiertypes (IdentifierID,Description,TouchedBy,TouchedAt) VALUES (4,'Mobile Phone Number','root@localhost','2014-03-27 16:19:01.');
INSERT INTO identifiertypes (IdentifierID,Description,TouchedBy,TouchedAt) VALUES (5,'Bar Code','root@localhost','2014-03-27 16:19:36.');
INSERT INTO identifiertypes (IdentifierID,Description,TouchedBy,TouchedAt) VALUES (6,'QR Code','root@localhost','2014-03-27 16:19:42.');

INSERT INTO media (MediaID,Description,AvailableSlots,TouchedBy,TouchedAt) VALUES (1,'Magnetic Card',1,'root@localhost','2014-03-27 16:21:10.');
INSERT INTO media (MediaID,Description,AvailableSlots,TouchedBy,TouchedAt) VALUES (2,'Paper',1,'root@localhost','2014-03-27 16:21:28.');
INSERT INTO media (MediaID,Description,AvailableSlots,TouchedBy,TouchedAt) VALUES (3,'DesFire Smart Card',1,'root@localhost','2014-03-27 16:21:45.');
INSERT INTO media (MediaID,Description,AvailableSlots,TouchedBy,TouchedAt) VALUES (4,'Limited Use Contactless Card',1,'root@localhost','2014-03-27 16:22:10.');
INSERT INTO media (MediaID,Description,AvailableSlots,TouchedBy,TouchedAt) VALUES (5,'Mobile Phone',1,'root@localhost','2014-03-27 16:23:07.');
INSERT INTO media (MediaID,Description,AvailableSlots,TouchedBy,TouchedAt) VALUES (6,'Adhesive Smart Media',1,'root@localhost','2014-03-27 16:31:10.');
INSERT INTO media (MediaID,Description,AvailableSlots,TouchedBy,TouchedAt) VALUES (7,'Third Party Issued',1,'root@localhost','2014-03-27 16:33:49.');
INSERT INTO media (MediaID,Description,AvailableSlots,TouchedBy,TouchedAt) VALUES (8,'Multi Function Smart Card',1,'root@localhost','2014-03-27 16:34:06.');
INSERT INTO media (MediaID,Description,AvailableSlots,TouchedBy,TouchedAt) VALUES (9,'Near Field Communication Device',1,'root@localhost','2014-03-27 16:34:30.');

INSERT INTO purposes (PurposeID,Description,TouchedBy,TouchedAt) VALUES (1,'Normal Use','root@localhost','2014-03-27 16:24:22.');
INSERT INTO purposes (PurposeID,Description,TouchedBy,TouchedAt) VALUES (2,'Driver ID','root@localhost','2014-03-27 16:24:33.');
INSERT INTO purposes (PurposeID,Description,TouchedBy,TouchedAt) VALUES (3,'Maintenance ID','root@localhost','2014-03-27 16:24:40.');
INSERT INTO purposes (PurposeID,Description,TouchedBy,TouchedAt) VALUES (4,'Test Purposes','root@localhost','2014-03-27 16:25:15.');
INSERT INTO purposes (PurposeID,Description,TouchedBy,TouchedAt) VALUES (5,'POS Clerk ID','root@localhost','2014-03-27 16:25:35.');

INSERT INTO discounttypes (DiscountTypeID,Description,Criteria,TermLimit,TouchedBy,TouchedAt) VALUES (1,'Senior',null,null,'root@localhost','2014-03-27 16:38:11.');
INSERT INTO discounttypes (DiscountTypeID,Description,Criteria,TermLimit,TouchedBy,TouchedAt) VALUES (2,'Youth',null,null,'root@localhost','2014-03-27 16:38:17.');
INSERT INTO discounttypes (DiscountTypeID,Description,Criteria,TermLimit,TouchedBy,TouchedAt) VALUES (3,'Eomployee',null,null,'root@localhost','2014-03-27 16:38:31.');
INSERT INTO discounttypes (DiscountTypeID,Description,Criteria,TermLimit,TouchedBy,TouchedAt) VALUES (4,'Eomployer Sponsored',null,null,'root@localhost','2014-03-27 16:38:42.');
INSERT INTO discounttypes (DiscountTypeID,Description,Criteria,TermLimit,TouchedBy,TouchedAt) VALUES (5,'Disabled',null,null,'root@localhost','2014-03-27 16:38:53.');

INSERT INTO tickettypes (TicketTypeID,Description,TouchedBy,TouchedAt) VALUES (1,'Stored Fixed Value','root@localhost','2014-03-27 16:57:03.');
INSERT INTO tickettypes (TicketTypeID,Description,TouchedBy,TouchedAt) VALUES (2,'Stored Ride','root@localhost','2014-03-27 16:57:10.');
INSERT INTO tickettypes (TicketTypeID,Description,TouchedBy,TouchedAt) VALUES (3,'Floating Period','root@localhost','2014-03-27 16:57:37.');
INSERT INTO tickettypes (TicketTypeID,Description,TouchedBy,TouchedAt) VALUES (4,'Fixed Period','root@localhost','2014-03-27 16:57:44.');
INSERT INTO tickettypes (TicketTypeID,Description,TouchedBy,TouchedAt) VALUES (5,'Stepped Value','root@localhost','2014-03-27 16:59:25.');
INSERT INTO tickettypes (TicketTypeID,Description,TouchedBy,TouchedAt) VALUES (6,'Farest Fare','root@localhost','2014-03-27 17:07:55.');

INSERT INTO taxrates (TaxRateID,Description,Rate1,Rate2,Rate3,Rate4,Rate5,TouchedBy,TouchedAt) VALUES (1,'Not Taxable',0.00,0.00,0.00,0.00,0.00,'root@localhost','2014-04-04 11:07:05.');
INSERT INTO taxrates (TaxRateID,Description,Rate1,Rate2,Rate3,Rate4,Rate5,TouchedBy,TouchedAt) VALUES (2,'Taxable',0.00,0.00,0.00,0.00,0.00,'root@localhost','2014-04-04 11:07:19.');

INSERT INTO offerings (OfferingID,Description,UnitPrice,UnitCost,IsSubscribable,IsBulk,TaxRateID,QuantityMax,TouchedBy,TouchedAt) VALUES (1,'New Card',3.00,0.00,'F','T',1,500,'root@localhost','2014-04-04 11:12:17.');
INSERT INTO offerings (OfferingID,Description,UnitPrice,UnitCost,IsSubscribable,IsBulk,TaxRateID,QuantityMax,TouchedBy,TouchedAt) VALUES (2,'New Ticket',0.00,0.00,'F','T',1,500,'root@localhost','2014-04-04 11:12:50.');
INSERT INTO offerings (OfferingID,Description,UnitPrice,UnitCost,IsSubscribable,IsBulk,TaxRateID,QuantityMax,TouchedBy,TouchedAt) VALUES (3,'Refill Ticket',0.00,0.00,'T','F',1,500,'root@localhost','2014-04-04 11:13:16.');
INSERT INTO offerings (OfferingID,Description,UnitPrice,UnitCost,IsSubscribable,IsBulk,TaxRateID,QuantityMax,TouchedBy,TouchedAt) VALUES (4,'Card Holder',3.59,2.99,'F','F',2,500,'root@localhost','2014-04-04 11:14:26.');
INSERT INTO offerings (OfferingID,Description,UnitPrice,UnitCost,IsSubscribable,IsBulk,TaxRateID,QuantityMax,TouchedBy,TouchedAt) VALUES (5,'Transit T-Shirt',19.95,12.38,'F','F',2,10,'root@localhost','2014-04-04 11:15:26.');
INSERT INTO offerings (OfferingID,Description,UnitPrice,UnitCost,IsSubscribable,IsBulk,TaxRateID,QuantityMax,TouchedBy,TouchedAt) VALUES (6,'Transit Mug',12.95,8.38,'F','F',2,10,'root@localhost','2014-04-04 11:15:55.');

INSERT INTO servicelevels (ServiceLevelID,Description,TouchedBy,TouchedAt) VALUES (1,'Local','root@localhost','2014-03-27 16:48:21.');
INSERT INTO servicelevels (ServiceLevelID,Description,TouchedBy,TouchedAt) VALUES (2,'BRT/Bus Plus','root@localhost','2014-03-27 16:49:07.');
INSERT INTO servicelevels (ServiceLevelID,Description,TouchedBy,TouchedAt) VALUES (3,'Regional','root@localhost','2014-03-27 16:49:23.');
INSERT INTO servicelevels (ServiceLevelID,Description,TouchedBy,TouchedAt) VALUES (4,'STAR Paratransit Service','root@localhost','2014-03-31 16:47:04.');

INSERT INTO validdays (ValidDayID,Description,IsDate,ValidDay,TouchedBy,TouchedAt) VALUES (1,'Week Days Only','F','2,3,4,5,6','root@localhost','2014-03-31 12:15:40.');

INSERT INTO zones (ZoneID,Description,TouchedBy,TouchedAt) VALUES (1,'NX Zone 1','root@localhost','2014-03-28 11:29:43.');
INSERT INTO zones (ZoneID,Description,TouchedBy,TouchedAt) VALUES (2,'NX Zone 2','root@localhost','2014-03-28 11:29:52.');
INSERT INTO zones (ZoneID,Description,TouchedBy,TouchedAt) VALUES (3,'NX Zone 3','root@localhost','2014-03-28 11:30:00.');

INSERT INTO periods (PeriodID,Description,PeriodsPerYear,RenewalMonths,RenewalDay,TouchedBy,TouchedAt) VALUES (1,'Calendar Monthly Period',12,'1,2,3,4,5,6,7,8,9,10,11,12',20,'root@localhost','2014-03-28 12:10:43.');
INSERT INTO periods (PeriodID,Description,PeriodsPerYear,RenewalMonths,RenewalDay,TouchedBy,TouchedAt) VALUES (2,'Summer July - August',1,null,null,'root@localhost','2014-04-01 16:35:38.');

INSERT INTO authenticationTypes(AuthenticationTypeID, Description, Prompt, TouchedBy, TouchedAt) VALUES (1, 'Password', 'password','unassigned','2014-04-14 16:35:38. ');
INSERT INTO authenticationTypes(AuthenticationTypeID, Description, Prompt, TouchedBy, TouchedAt) VALUES (2, 'Security Question', 'security question','unassigned','2014-04-14 16:35:38. ');
INSERT INTO authenticationTypes(AuthenticationTypeID, Description, Prompt, TouchedBy, TouchedAt) VALUES (3, 'Email Verification Token', 'email verification token','unassigned','2014-04-14 16:35:38. ');