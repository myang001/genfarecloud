SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

UPDATE equipment
SET Status=1;

ALTER TABLE equipment 
CHANGE COLUMN Status Status TINYINT(3) UNSIGNED NOT NULL ,
ADD INDEX RefEquipmentStatuses394 (Status ASC);

ALTER TABLE equipment 
ADD CONSTRAINT RefEquipmentStatuses394
  FOREIGN KEY (Status)
  REFERENCES equipmentstatuses (Status);
 

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;


-- For RP033: Equipment Status report
CREATE OR REPLACE VIEW EquipmentStatus AS
SELECT e.Description AS EquipmentDescr, e.SerialNumber, e.Version, es.Description AS EquipmentStatus, l.Description AS LocationDescr,  e.TouchedAt AS LastCommunication, l.Latitude, l.Longtitude
FROM equipment e
INNER JOIN locations l ON e.LocationID = l.LocationID
INNER JOIN equipmentstatuses es ON e.status = es.status;
