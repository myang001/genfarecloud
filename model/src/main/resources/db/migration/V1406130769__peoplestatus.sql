DROP TABLE IF EXISTS PeopleStatus;

CREATE TABLE PeopleStatus (
    Status       TINYINT UNSIGNED NOT NULL,
    Description  VARCHAR(50),
    TouchedBy    VARCHAR(50) NOT NULL,
    TouchedAt    DATETIME DEFAULT NOW() NOT NULL,
    CreatedBy    VARCHAR(50) NOT NULL,
    CreatedAt    DATETIME DEFAULT NOW() NOT NULL,
    PRIMARY KEY (Status)
)ENGINE=INNODB
;

INSERT INTO PeopleStatus (Status,Description,TouchedBy,TouchedAt,CreatedBy,CreatedAt) VALUES(1,'Active','admin','2014-07-21 21:45:36','admin','2014-07-21 21:45:36');
INSERT INTO PeopleStatus (Status,Description,TouchedBy,TouchedAt,CreatedBy,CreatedAt) VALUES(2,'Disabled','admin','2014-07-21 21:45:36','admin','2014-07-21 21:45:36');
INSERT INTO PeopleStatus (Status,Description,TouchedBy,TouchedAt,CreatedBy,CreatedAt) VALUES(3,'Pending','admin','2014-07-21 21:45:36','admin','2014-07-21 21:45:36');
INSERT INTO PeopleStatus (Status,Description,TouchedBy,TouchedAt,CreatedBy,CreatedAt) VALUES(4,'Locked','admin','2014-07-21 21:45:36','admin','2014-07-21 21:45:36');
INSERT INTO PeopleStatus (Status,Description,TouchedBy,TouchedAt,CreatedBy,CreatedAt) VALUES(5,'Expired','admin','2014-07-21 21:45:36','admin','2014-07-21 21:45:36');

UPDATE People SET Status = 1;

ALTER TABLE People 
	ADD CONSTRAINT RefPeople380
		FOREIGN KEY (Status)
		REFERENCES PeopleStatus(Status)
;


