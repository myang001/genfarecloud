SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';


CREATE TABLE IF NOT EXISTS OrderItemTypes (
  OrderItemTypeID TINYINT(3) UNSIGNED NOT NULL AUTO_INCREMENT,
  Description VARCHAR(50) NULL DEFAULT NULL,
  TouchedBy VARCHAR(50) NOT NULL,
  TouchedAt DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  CreatedBy VARCHAR(50) NOT NULL,
  CreatedAt DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (OrderItemTypeID))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

INSERT INTO OrderItemTypes (Description, TouchedBy, CreatedBy) VALUES ('TICKET', 'admin', 'admin');
INSERT INTO OrderItemTypes (Description, TouchedBy, CreatedBy) VALUES ('WALLET', 'admin', 'admin');
INSERT INTO OrderItemTypes (Description, TouchedBy, CreatedBy) VALUES ('WALLETTYPE', 'admin', 'admin');

ALTER TABLE OrderItems
  ADD COLUMN OrderItemTypeID TINYINT(3) UNSIGNED NOT NULL,  
  ADD INDEX RefOrderItemTypes403 (OrderItemTypeID ASC),
  ADD CONSTRAINT RefOrderItemTypes403
    FOREIGN KEY (OrderItemTypeID)
    REFERENCES OrderItemTypes (OrderItemTypeID);  

UPDATE OrderItems SET OrderItemTypeID = 1 WHERE TicketID IS NOT NULL;
UPDATE OrderItems SET OrderItemTypeID = 2 WHERE TicketID IS NULL AND WalletID IS NOT NULL;
UPDATE OrderItems SET OrderItemTypeID = 3 WHERE TicketID IS NULL AND WalletID IS NULL 
  AND WalletTypeID IS NOT NULL;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
