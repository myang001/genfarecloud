DROP TABLE IF EXISTS temp_rp001;
DROP TABLE IF EXISTS temp_rp002;
DROP TABLE IF EXISTS temp_rp003;
DROP TABLE IF EXISTS temp_rp004;
DROP TABLE IF EXISTS temp_rp005;
DROP TABLE IF EXISTS temp_rp006;
DROP TABLE IF EXISTS temp_rp007;
DROP TABLE IF EXISTS temp_rp008;
DROP TABLE IF EXISTS temp_rp009;
DROP TABLE IF EXISTS temp_rp010;
DROP TABLE IF EXISTS temp_rp011;
DROP TABLE IF EXISTS temp_rp012;
DROP TABLE IF EXISTS temp_rp013;
DROP TABLE IF EXISTS temp_rp014;
DROP TABLE IF EXISTS temp_rp015;
DROP TABLE IF EXISTS temp_rp016;
DROP TABLE IF EXISTS temp_rp017;
DROP TABLE IF EXISTS temp_rp018;
DROP TABLE IF EXISTS temp_rp019;
DROP TABLE IF EXISTS temp_rp020;
DROP TABLE IF EXISTS temp_rp021;
DROP TABLE IF EXISTS temp_rp022;
DROP TABLE IF EXISTS temp_rp023;
DROP TABLE IF EXISTS temp_rp024;
DROP TABLE IF EXISTS temp_rp025;
DROP TABLE IF EXISTS temp_rp026;
DROP TABLE IF EXISTS temp_rp027;
DROP TABLE IF EXISTS temp_rp028;
DROP TABLE IF EXISTS temp_rp029;
DROP TABLE IF EXISTS temp_rp030;
DROP TABLE IF EXISTS temp_rp031;
DROP TABLE IF EXISTS temp_rp032;
DROP TABLE IF EXISTS temp_rp033;
DROP TABLE IF EXISTS temp_rp034;
DROP TABLE IF EXISTS temp_rp035;
DROP TABLE IF EXISTS temp_rp036;
DROP TABLE IF EXISTS temp_rp037;
DROP TABLE IF EXISTS temp_rp038;
DROP TABLE IF EXISTS temp_rp039;

-- General Sales Summary - RP001 
CREATE TABLE temp_rp001a (EquipmentName VARCHAR(50), TicketType VARCHAR(50), Discount VARCHAR(50), Quantity INT(11), UnitPrice DECIMAL (5,2), ShippingAmt DECIMAL (5,2), TaxAmt DECIMAL (5,2), SalesAmt DECIMAL (5,2), DateActivity DateTime);

INSERT INTO temp_rp001a VALUES("POS", "Monthly Pass", "STAR", 1, 85.00, null, 1.25, 85.00, "2014-08-05 04:14:07");
INSERT INTO temp_rp001a VALUES("POS", "Daily Pass", "STAR", 50, 4.00, null, 1.25, 200.00, "2014-08-05 04:14:07");
INSERT INTO temp_rp001a VALUES("Mobile", "Monthly Pass", "STAR", 10, 85.00, null, 1.25, 850.00, "2014-08-05 04:14:07");
INSERT INTO temp_rp001a VALUES("Mobile", "Daily Pass", "STAR", 50, 4.00, null, 1.25, 200.00, "2014-08-05 04:14:07");

-- View for General Sales Summary  - RP001a 
CREATE OR REPLACE VIEW GeneralSalesSummary AS
SELECT EquipmentName, TicketType, Discount, Quantity, UnitPrice, ShippingAmt, TaxAmt, SalesAmt, DateActivity FROM temp_rp001a;


-- Product Sales Summary - RP002 
CREATE TABLE temp_rp002 (WalletType VARCHAR(50), TicketType VARCHAR(50), Discount VARCHAR(50), Quantity INT(11), UnitPrice DECIMAL (5,2), ShippingAmt DECIMAL (5,2), TaxAmt DECIMAL (5,2), SalesAmt DECIMAL (5,2), DateActivity DateTime);

INSERT INTO temp_rp002 VALUES("Magnetic", "Full Fare", "STAR", 1, 85.00, null, 1.25, 85.00, "2014-08-05 04:14:07");
INSERT INTO temp_rp002 VALUES("Magnetic", "30 Day", "STAR", 50, 4.00, null, 1.25, 200.00, "2014-08-05 04:14:07");
INSERT INTO temp_rp002 VALUES("Swipe", "Full Fare", "STAR", 10, 85.00, null, 1.25, 850.00, "2014-08-05 04:14:07");
INSERT INTO temp_rp002 VALUES("Swipe", "30 Day", "STAR", 50, 4.00, null, 1.25, 200.00, "2014-08-05 04:14:07");

-- View for Product Sales Summary  - RP002  
CREATE OR REPLACE VIEW ProductSalesSummary AS
SELECT WalletType, TicketType, Discount, Quantity, UnitPrice, ShippingAmt, TaxAmt, SalesAmt, DateActivity FROM temp_rp002;


-- Detailed Sales Summary - RP003 
CREATE TABLE temp_rp003 (MediaType VARCHAR(50), FareProduct VARCHAR(50), ServiceLevel VARCHAR(50), TimesUsed INT(11), DateActivity DateTime);

INSERT INTO temp_rp003 VALUES("Mobile", "Full", "Express", 250, "2014-08-05 04:14:07");
INSERT INTO temp_rp003 VALUES("Mobile", "Half Fare", "Express", 100, "2014-08-05 04:14:07");
INSERT INTO temp_rp003 VALUES("Mobile", "STAR", "Express", 50, "2014-08-05 05:14:07");
INSERT INTO temp_rp003 VALUES("Smart Card", "Full", "Express", 500, "2014-08-05 06:14:07");
INSERT INTO temp_rp003 VALUES("Smart Card", "Half Fare", "Express", 200, "2014-08-05 08:14:07");
INSERT INTO temp_rp003 VALUES("Smart Card", "STAR", "Express", 100, "2014-08-05 09:14:07");

-- View for Detailed Sales Summary  - RP003  
CREATE OR REPLACE VIEW DetailedSalesSummary AS
SELECT MediaType, FareProduct, ServiceLevel, TimesUsed, DateActivity FROM temp_rp003;


-- General Revenue Summary - RP004 
CREATE TABLE temp_rp004 (EquipmentName VARCHAR(50), PaymentType VARCHAR(50), Quantity INT(11), ShippingAmt DECIMAL (5,2), TaxAmt DECIMAL (5,2), SalesAmt DECIMAL (5,2), DateActivity DATETIME);

INSERT INTO temp_rp004 VALUES("POS", "Visa", 1, 0.00, 1.25, 85.00, "2014-08-05 04:14:07");
INSERT INTO temp_rp004 VALUES("POS", "Mastercard", 50, 0.00, 1.25, 200.00, "2014-08-05 04:14:07");
INSERT INTO temp_rp004 VALUES("Mobile", "Visa", 51, 0.00, 2.50, 285.00, "2014-08-05 04:14:07");
INSERT INTO temp_rp004 VALUES("Mobile", "Paypal", 12, 0.00, 2.50, 285.00, "2014-08-05 04:14:07");

-- View for General Revenue Summary  - RP004  
CREATE OR REPLACE VIEW GeneralRevenueSummary AS
SELECT EquipmentName, PaymentType, Quantity, ShippingAmt, TaxAmt, SalesAmt, DateActivity FROM temp_rp004;


-- Detailed Boarding Summary - RP005 
CREATE TABLE temp_rp005 (RetailChain VARCHAR(50), Location VARCHAR(50), Quantity INT(11), ShippingAmt DECIMAL (5,2), TaxAmt DECIMAL (5,2), SalesAmt DECIMAL (5,2), DateActivity DATETIME);

INSERT INTO temp_rp005 VALUES("CVS", "29th St", 1, 0.00, 1.25, 85.00, "2014-08-05 04:14:07");
INSERT INTO temp_rp005 VALUES("CVS", "100th St", 50, 0.00, 1.25, 200.00, "2014-08-05 04:14:07");
INSERT INTO temp_rp005 VALUES("Walgreens", "30th St", 10, 0.00, 1.25, 850.00, "2014-08-05 04:14:07");
INSERT INTO temp_rp005 VALUES("Walgreens", "60th St", 50, 0.00, 1.25, 200.00, "2014-08-05 04:14:07");

-- View for RPOS Terminal Invoicing  - RP005  
CREATE OR REPLACE VIEW RPOSTerminalInvoicing AS
SELECT RetailChain, Location, Quantity, ShippingAmt, TaxAmt, SalesAmt FROM temp_rp005;


-- Daily 
CREATE OR REPLACE VIEW GenRidershipSummaryDaily AS
SELECT wt.Description AS MediaType, tt.Description AS FareProduct, sl.Description AS ServiceLevel, a.CountUse, d.DateTransit
FROM activitysummary a
INNER JOIN wallettypes wt ON a.WalletTypeID = wt.WalletTypeID
INNER JOIN tickettypes tt ON tt.TicketTypeID = a. TicketTypeID
INNER JOIN servicelevels sl ON a.ServiceLevelID = sl.ServiceLevelID
INNER JOIN dates d ON d.DateID = a.DateID;


-- Monthly
CREATE OR REPLACE VIEW GenRidershipSummaryMonthly AS
SELECT wt.Description AS MediaType, tt.Description AS FareProduct, sl.Description AS ServiceLevel, a.CountUse, d.DateTransit
FROM activitymonthly a
INNER JOIN wallettypes wt ON a.WalletTypeID = wt.WalletTypeID
INNER JOIN tickettypes tt ON tt.TicketTypeID = a.TicketTypeID
INNER JOIN servicelevels sl ON a.ServiceLevelID = sl.ServiceLevelID
INNER JOIN dates d ON d.Month = a.Month;

-- Yearly
CREATE OR REPLACE VIEW GenRidershipSummaryYearly AS
SELECT wt.Description AS MediaType, tt.Description AS FareProduct, sl.Description AS ServiceLevel, a.CountUse, d.DateTransit
FROM activityyearly a
INNER JOIN wallettypes wt ON a.WalletTypeID = wt.WalletTypeID
INNER JOIN tickettypes tt ON tt.TicketTypeID = a.TicketTypeID
INNER JOIN servicelevels sl ON a.ServiceLevelID = sl.ServiceLevelID
INNER JOIN dates d ON d.Year = a.Year;


-- Detailed Ridership Summary - RP007 
CREATE TABLE temp_rp007 (MediaType VARCHAR(50), FareProduct VARCHAR(50), ServiceLevel VARCHAR(50), RouteNumber VARCHAR(10), Day VARCHAR(50), TimesUsed INT(11), DateActivity DateTime);

INSERT INTO temp_rp007 VALUES("Mobile", "Full", "Express", "14", "Weekday", 100, "2014-08-05 03:14:07");
INSERT INTO temp_rp007 VALUES("Mobile", "Half Fare", "Express", "14", "Weekday", 10, "2014-08-05 03:14:07");
INSERT INTO temp_rp007 VALUES("Mobile", "STAR", "Express", "14", "Weekday", 5, "2014-08-05 03:14:07");
INSERT INTO temp_rp007 VALUES("Smart Card", "Full", "Express", "14", "Weekend", 5, "2014-08-05 03:14:07");
INSERT INTO temp_rp007 VALUES("Smart Card", "Half Fare", "Express", "14", "Weekend", 10, "2014-08-05 03:14:07");
INSERT INTO temp_rp007 VALUES("Smart Card", "STAR", "Express", "14", "Weekend", 5, "2014-08-05 03:14:07");

INSERT INTO temp_rp007 VALUES("Mobile", "Full", "Express", "15", "Weekday", 100, "2014-08-05 03:14:07");
INSERT INTO temp_rp007 VALUES("Mobile", "Half Fare", "Express", "15", "Weekday", 10, "2014-08-05 03:14:07");
INSERT INTO temp_rp007 VALUES("Mobile", "STAR", "Express", "15", "Weekday", 5, "2014-08-05 03:14:07");
INSERT INTO temp_rp007 VALUES("Smart Card", "Full", "Express", "15", "Weekend	", 5, "2014-08-05 03:14:07");
INSERT INTO temp_rp007 VALUES("Smart Card", "Half Fare", "Express", "15", "Weekend", 10, "2014-08-05 03:14:07");
INSERT INTO temp_rp007 VALUES("Smart Card", "STAR", "Express", "15", "Weekend", 5, "2014-08-05 03:14:07");

-- View for Detailed Ridership Summary  - temp_rp007  
CREATE OR REPLACE VIEW DetailedRidershipSummary AS
SELECT MediaType, FareProduct, ServiceLevel, RouteNumber, Day, TimesUsed, DateActivity FROM temp_rp007;


-- General Boarding Summary - RP009 
CREATE TABLE temp_rp008 (MediaType VARCHAR(50), FareProduct VARCHAR(50), ServiceLevel VARCHAR(50), TimesUsed INT(11), DateActivity DateTime);

INSERT INTO temp_rp008 VALUES("Mobile", "Full", "Express", 250, "2014-08-05 04:14:07");
INSERT INTO temp_rp008 VALUES("Mobile", "Half Fare", "Express", 100, "2014-08-05 04:14:07");
INSERT INTO temp_rp008 VALUES("Mobile", "STAR", "Express", 50, "2014-08-05 05:14:07");
INSERT INTO temp_rp008 VALUES("Smart Card", "Full", "Express", 500, "2014-08-05 06:14:07");
INSERT INTO temp_rp008 VALUES("Smart Card", "Half Fare", "Express", 200, "2014-08-05 08:14:07");
INSERT INTO temp_rp008 VALUES("Smart Card", "STAR", "Express", 100, "2014-08-05 09:14:07");

-- View for General Boarding Summary  - RP009  
CREATE OR REPLACE VIEW GeneralBoardingSummary AS
SELECT MediaType, FareProduct, ServiceLevel, TimesUsed, DateActivity FROM temp_rp008;


-- Detailed Boarding Summary - RP009 
CREATE TABLE temp_rp009 (MediaType VARCHAR(50), FareProduct VARCHAR(50), ServiceLevel VARCHAR(50), RouteNumber VARCHAR(10), Day VARCHAR(50), TimesUsed INT(11), DateActivity DateTime);

INSERT INTO temp_rp009 VALUES("Mobile", "Full", "Express", "14", "Weekday", 100, "2014-08-05 04:14:07");
INSERT INTO temp_rp009 VALUES("Mobile", "Half Fare", "Express", "14", "Weekday", 10, "2014-08-05 04:14:07");
INSERT INTO temp_rp009 VALUES("Mobile", "STAR", "Express", "14", "Weekday", 5, "2014-08-05 05:14:07");
INSERT INTO temp_rp009 VALUES("Smart Card", "Full", "Express", "14", "Weekend", 5, "2014-08-05 06:14:07");
INSERT INTO temp_rp009 VALUES("Smart Card", "Half Fare", "Express", "14", "Weekend", 10, "2014-08-05 08:14:07");
INSERT INTO temp_rp009 VALUES("Smart Card", "STAR", "Express", "14", "Weekend", 5, "2014-08-05 09:14:07");

INSERT INTO temp_rp009 VALUES("Mobile", "Full", "Express", "15", "Weekday", 100, "2014-08-06 03:14:07");
INSERT INTO temp_rp009 VALUES("Mobile", "Half Fare", "Express", "15", "Weekday", 10, "2014-08-06 03:14:07");
INSERT INTO temp_rp009 VALUES("Mobile", "STAR", "Express", "15", "Weekday", 5, "2014-08-06 03:14:07");
INSERT INTO temp_rp009 VALUES("Smart Card", "Full", "Express", "15", "Weekend	", 5, "2014-08-06 03:14:07");
INSERT INTO temp_rp009 VALUES("Smart Card", "Half Fare", "Express", "15", "Weekend", 10, "2014-08-06 03:14:07");
INSERT INTO temp_rp009 VALUES("Smart Card", "STAR", "Express", "15", "Weekend", 5, "2014-08-06 03:14:07");

-- View for Detailed Boarding Summary  - RP009  
CREATE OR REPLACE VIEW DetailedBoardingSummary AS
SELECT MediaType, FareProduct, ServiceLevel, RouteNumber, Day, TimesUsed, DateActivity FROM temp_rp009;


-- Detailed Transacation History Summary - RP010 
CREATE TABLE temp_rp010 (WalletId BIGINT(20), TicketDesc VARCHAR(256), Discount VARCHAR(256), AmtCharged DECIMAL (5,2), AmtRemaining DECIMAL (5,2), ActivityType VARCHAR(256), 
ValueActivity DECIMAL (5,2), RouteNumber VARCHAR(10), DateActivity DateTime, EquipmentNumber INT, Latitude VARCHAR(10), Longtitude VARCHAR(10), Run INT(11), DriverNumber VARCHAR(10), WalletType VARCHAR(25));

INSERT INTO temp_rp010 VALUES(1000002520, "Monthly Pass",  "Standard", null, null, "Tap", 17.50, "14", "2014-08-05 03:14:07", 315, "+42.644298", "-73.762894", 14, 113562, "Smart Card");
INSERT INTO temp_rp010 VALUES(1000002520, "Monthly Pass",  "Standard", null, null, "Transfer", null, "32", "2014-08-05 03:14:07", 21, "+42.644298", "-73.762894", 8, 113579, "Smart Card");
INSERT INTO temp_rp010 VALUES(1000002520, "Value",  "Standard", 4.00, 53.50, "Tap", 4.50, "8", "2014-08-05 03:14:07", 454, "+42.644298", "-73.762894", 666, 113625, "Smart Card");
INSERT INTO temp_rp010 VALUES(1000002520, "Fre Ride",  "Standard", 0.00, null, "Tap", 4.50, "8", "2014-08-05 03:14:07", 454, "+42.644298", "-73.762894", 666, 113625, "Smart Card");
INSERT INTO temp_rp010 VALUES(1000002520, "10-Ride Pass",  "Standard", 0.00, null, "Tap", 4.50, "8", "2014-08-05 03:14:07", 454, "+42.644298", "-73.762894", 666, 113625, "Smart Card");
INSERT INTO temp_rp010 VALUES(1000002520, "10-Ride Pass",  "Standard", 0.00, null, "Passback", 4.50, "8", "2014-08-05 03:14:07", 454, "+42.644298", "-73.762894", 666, 113625, "Smart Card");

-- View for General Usage History Summary  - temp_rp011  
CREATE OR REPLACE VIEW DetailedTransHistorySummary AS
SELECT WalletId, TicketDesc, Discount, AmtCharged, AmtRemaining, ActivityType, ValueActivity, RouteNumber, DateActivity, EquipmentNumber, Latitude, Longtitude, Run, DriverNumber, WalletType FROM temp_rp010;


-- General Usage History Summary - RP011 
CREATE TABLE temp_rp011 (WalletId BIGINT(20), TicketDesc VARCHAR(256), MediaType VARCHAR(256), AmtCharged DECIMAL (9,2), TimesUsed BIGINT(20), GenUsageDate DateTime);
INSERT INTO temp_rp011 VALUES(1000002520, "Monthly Pass",  "Smart Card", 85.00, 125, "2014-08-05 03:14:07");
INSERT INTO temp_rp011 VALUES(1000002520, "Stored Value",  "Smart Card", null, 5, "2014-08-05 03:14:07");
INSERT INTO temp_rp011 VALUES(1000002520, "Daily Pass",  "Smart Card", 4.00, 1297, "2014-08-05 03:14:07");

-- View for General Usage History Summary  - temp_rp011  
CREATE OR REPLACE VIEW GeneralUsageHistorySummary AS
SELECT WalletId, TicketDesc, MediaType, AmtCharged, TimesUsed, GenUsageDate FROM temp_rp011;


-- Fare Product Usage Summary - RP012 
CREATE TABLE temp_rp012 (TicketDesc VARCHAR(256), MediaType VARCHAR(256), TimesUsed BIGINT(20), FareProductDate DateTime);
INSERT INTO temp_rp012 VALUES("Monthly Pass",  "Smart Card", 15478, "2014-08-05 03:14:07");
INSERT INTO temp_rp012 VALUES("Stored Value",  "Smart Card", 42342, "2014-08-05 03:14:07");
INSERT INTO temp_rp012 VALUES("Daily Pass",  "Smart Card", 432, "2014-08-05 03:14:07");
INSERT INTO temp_rp012 VALUES("Daily Pass",  "Paper Ticket", 4879, "2014-08-05 03:14:07");

-- View for Fare Product Usage Summary - temp_rp012  
CREATE OR REPLACE VIEW FareProductUsageSummary AS
SELECT TicketDesc, MediaType, TimesUsed FROM temp_rp012;


-- Detailed Event Summary - RP013 
CREATE TABLE temp_rp013 (EquipmentName VARCHAR(50), EventDesc VARCHAR(50), EventDateTime DATETIME, State VARCHAR(50), EventDescLong VARCHAR(256));

INSERT INTO temp_rp013 VALUES("Farebox 123", "Voltage Reading", "2014-08-05 04:14:07", "High", "Voltage Reading: Was OK, now reading High");
INSERT INTO temp_rp013 VALUES("RPOS 432", "Temperature", "2014-08-06 04:14:07", "Low", "Temperature: Was OK, now reading Low");
INSERT INTO temp_rp013 VALUES("TVM 984", "Door Open", "2014-08-06 04:14:07", null, "Door Open:  Door should be closed");
INSERT INTO temp_rp013 VALUES("Farebox 555", "Misread Card", "2014-08-09 04:14:07", null, "Misread Card: Unable to read card");

-- View for Detailed Boarding Summary  - RP013  
CREATE OR REPLACE VIEW DetailedEventSummary AS
SELECT EquipmentName, EventDesc, EventDateTime, State, EventDescLong FROM temp_rp013;

-- Table for Detailed Last Proved - RP014
Create table temp_rp014 (EquipmentDesc  Varchar(50), SerialNumber  Varchar(50), ProbeType Varchar(50), Status Varchar(50), LocationDesc Varchar(50), date_time DATETIME, equipmentstatus Varchar(50));
insert into temp_rp014 values("Farebox 13", "001-302-123032" , "Wifi" , "Failed" , "Albany Office" , "2014-05-15 03:14:07" , "Maintenance");
insert into temp_rp014 values("Bus 11", "ABDX124" , "IR Probe" , "OK" , "Garage 12" , "2014-05-16 03:14:07" , "In Service");

-- View for devicelastprobed  - RP014
CREATE OR REPLACE VIEW devicelastprobed AS
SELECT EquipmentDesc, SerialNumber, ProbeType, Status, LocationDesc, date_time, equipmentstatus from temp_rp014;


-- Table for Origin/Destination - RP015
CREATE TABLE  temp_rp015 (walletIdentifier BIGINT(20), TicketDescription VARCHAR(50), Discount VARCHAR(50), AmtCharged DECIMAL (5,2), AmtRemaining DECIMAL (5,2), ActivityType VARCHAR(50), PendingType VARCHAR(50),
 ValueActivity DECIMAL (5,2), EquipmentDesc VARCHAR(50), ActivityDate DATETIME, Location VARCHAR(50));

INSERT INTO temp_rp015 VALUES(12345678, "Monthly Pass" , "STAR" , null, null, "Tap", null, 2.00, "Bus 126", "2014-08-01", "+42.644298 -73.762894");
INSERT INTO temp_rp015 VALUES(987654321, "Monthly Pass" , "STAR" , null, null, "Tap", null, 2.00, "Bus 126", "2014-08-15", "+42.644298 -73.762894");
INSERT INTO temp_rp015 VALUES(9947382910, "Fixed Value" , "Student" , 10.00, 12.50, "Load", "Reload", 10.00, "RPoS 432", "2014-08-16", "+42.644298 -73.762894");

-- View for Origin/Destination - RP015
CREATE OR REPLACE VIEW origindestination AS
SELECT walletIdentifier , TicketDescription , Discount , AmtCharged , AmtRemaining , ActivityType , PendingType , ValueActivity , EquipmentDesc , ActivityDate, Location FROM temp_rp015;



-- Table for Fraud Detection - RP016
CREATE TABLE temp_rp016 (walletIdentifier BIGINT(20), TicketDescription VARCHAR(50), MediaType VARCHAR(50), Discount VARCHAR(50), AmtCharged VARCHAR(50), AmtRemaining VARCHAR(50), ActivityType VARCHAR(50),
 ViolationType VARCHAR(50), ValueActivity VARCHAR(50), EquipmentDesc VARCHAR(50), ActivityDate DATETIME, Location VARCHAR(50));
INSERT INTO temp_rp016 VALUES(115, "Local" , "Smart Card", "Full Fare" , "1.00", "199.99", "Tap", "Multi-Use", "1.00", "Bus 126", "2014-08-01", "+42.644298 -73.762894");
INSERT INTO temp_rp016 VALUES(115, "Regional" , "Mobile", "Half Fare" , null, "1.75", "Tap", "Not Issued", "1.75", "Bus 126", "2014-08-15", "+42.644298 -73.762894");
INSERT INTO temp_rp016 VALUES(115, "Parking" , "Mobile", "Student" , "10.00", "12.50", "Load", "Excessive Use", "10.00", "RPoS 432", "2014-08-16", "+42.644298 -73.762894");

-- View for Fraud Detection - RP016
CREATE OR REPLACE VIEW frauddetection AS
SELECT walletIdentifier, TicketDescription, MediaType, Discount, AmtCharged, AmtRemaining, ActivityType,
 ViolationType, ValueActivity, EquipmentDesc, ActivityDate, Location FROM temp_rp016;

-- View for Fare Tables (all 3) - RP017, RP018, RP109
CREATE OR REPLACE VIEW FareTables AS
SELECT t.TicketID, t.Description, t.Price, t.Value, t.ValueUse AS ValueCharged, vd.Description AS ValidDaysDescr, sl.Description AS ServiceLevelDescr, t.DateEffective, t.DateExpires, tp.TimeStart, tp.TimeEnd
FROM tickets t
INNER JOIN tickettypes tt ON  tt.tickettypeid = t.tickettypeid
LEFT OUTER JOIN validdays vd ON vd.ValidDayID = t.ValidDayID
LEFT OUTER JOIN  servicelevels sl ON sl.ServiceLevelID = t.ServiceLevelID
LEFT OUTER JOIN tickettimes tts ON tts.TicketID = t.TicketID
LEFT OUTER JOIN timeperiods tp ON tp.TimePeriodID = tts.TimePeriodID;

-- Table for Configuration Files - RP020
-- View for Configuration Files - RP020

-- Table for Fraud Detection - RP021
-- View for Fraud Detection - RP021


-- View for Fraud Detection - RP022
CREATE OR REPLACE VIEW vCDSAuthUser AS
SELECT p.NameLast AS LastName, p.NameFirst AS FirstName, p.Status AS PersonStatus, o.Name AS OrgName, g.Description AS GroupSecLvlDescr, l.UserLogin AS LoginId, l.TouchedAt AS DateLastAccessed
FROM People p
INNER JOIN GroupsofPeople gop ON p.personid = gop.personid
INNER JOIN Groups g ON g.groupid = gop.groupid
INNER JOIN Logins l ON p.personid = l.personid
INNER JOIN Organizations o ON g.OrganizationID = o.OrganizationID;


-- Table for Web Portal Users - RP023
CREATE TABLE temp_rp023 (LastName VARCHAR(50), FirstName VARCHAR(50), Status VARCHAR(50), Organization VARCHAR(50), Description VARCHAR(50), ContactInfo VARCHAR(50), DateLastAccessed DateTime);
INSERT INTO temp_rp023 VALUES("Smith", "John" , "Active", "CDTA" , "Finance", "jsmith@abc.com", "2014-08-05 03:14:07");
INSERT INTO temp_rp023 VALUES("Trip", "Bruce" , "Active", "CDTA" , "Driver", "btrip@abc.com", "2014-08-05 03:14:07");
INSERT INTO temp_rp023 VALUES("Zeno", "Zidane" , "Active", "CDTA" , "Maintenance", "ZZenos@abc.com", "2014-08-05 03:14:07");

-- View for Web Portal Users - RP023
CREATE OR REPLACE VIEW WebPortalUsers AS
SELECT LastName, FirstName, Status, Organization, Description, ContactInfo, DateLastAccessed FROM temp_rp023;

-- Table for Admin POS Users - RP024
CREATE TABLE temp_rp024 (LastName VARCHAR(50), FirstName VARCHAR(50), Status VARCHAR(50), Organization VARCHAR(50), Description VARCHAR(50), GroupDescription VARCHAR(50), LoginId VARCHAR(50), DateLastAccessed DateTime);
INSERT INTO temp_rp024 VALUES("Smith", "John" , "Active", "CDTA" , "Finance", "Admin", "jsmith@abc.com", "2014-08-05 03:14:07");
INSERT INTO temp_rp024 VALUES("Trip", "Bruce" , "Active", "CDTA" , "Driver", "Admin", "btrip@abc.com", "2014-08-05 03:14:07");
INSERT INTO temp_rp024 VALUES("Zeno", "Zidane" , "Active", "CDTA" , "Maintenance", "Admin", "ZZenos@abc.com", "2014-08-05 03:14:07");

-- View for Admin POS Users - RP024
CREATE OR REPLACE VIEW AdminPOSUsers AS
SELECT LastName, FirstName, Status, Organization, Description, GroupDescription, LoginId, DateLastAccessed FROM temp_rp024;

-- Table for Admin POS Users - RP025
CREATE TABLE temp_rp025 (LastName VARCHAR(50), FirstName VARCHAR(50), Status VARCHAR(50), Organization VARCHAR(50), Description VARCHAR(50), GroupDescription VARCHAR(50), LoginId VARCHAR(50), DateLastAccessed DateTime);
INSERT INTO temp_rp025 VALUES("Smith", "John" , "Active", "CDTA" , "Store Manager", "Admin", "jsmith@abc.com", "2014-08-05 03:14:07");
INSERT INTO temp_rp025 VALUES("Trip", "Bruce" , "Active", "CDTA" , "Store Manager", "Admin", "btrip@abc.com", "2014-08-05 03:14:07");
INSERT INTO temp_rp025 VALUES("Zeno", "Zidane" , "Active", "CDTA" , "Store Manager", "Admin", "ZZenos@abc.com", "2014-08-05 03:14:07");

-- Table for Admin POS Users - RP025
CREATE OR REPLACE VIEW RetailPOSUsers AS
SELECT LastName, FirstName, Status, Organization, Description, GroupDescription, LoginId, DateLastAccessed FROM temp_rp025;


-- Table for Current Action List - RP026
CREATE TABLE temp_rp026 (WalletId BIGINT(20), Nickname VARCHAR(50), DateExpires DateTime, Status VARCHAR(50), PendingActivity VARCHAR(256), ValueActivity DECIMAL (5,2), PendingStatus VARCHAR(50), DateAdded DateTime, DateEffective DateTime, SourceOfChange VARCHAR(100));
INSERT INTO temp_rp026 VALUES(12345678, "Ted's Wallet" , "2014-08-05 03:14:07", "Active" , "Bad List - Wallet", null, "In Progress", "2014-08-05 03:14:07", "2014-08-05 03:14:07", "Univ of Albany");

-- View for Current Action List - RP026
CREATE OR REPLACE VIEW CurrentActionList AS
SELECT WalletId, Nickname, DateExpires, Status, PendingActivity, ValueActivity, PendingStatus, DateAdded, DateEffective, SourceOfChange FROM temp_rp026;


-- RP026 Original SQL
-- SELECT pa.WalletID AS WalletId, w.Nickname AS NickName, pa.DateExpires AS DateExpires, pa.PendingStatus AS PendingStatus, at.description AS PendingActivity, 
-- pa.ValueActivity AS ValueActivity, pa.DateAdded AS DateAdded, pa.DateEffective AS DateEffective, pa.source AS SourceOfChange
-- FROM activitytypes at, pendingactivity pa, Wallets w
-- WHERE pa.WalletID = w.WalletID
-- AND pa.ActivityTypeID = at.ActivityTypeID
-- AND at.ispending = 'T'
-- AND pa.DateExpires >=  $P{begdate} 
-- AND pa.DateExpires <=  $P{enddate}


-- Table for Deactivated & Suspended Cards - RP027  
CREATE TABLE temp_rp027 (WalletID BIGINT(20), Nickname VARCHAR(50), DateExpires DateTime, Status VARCHAR(50));
INSERT INTO temp_rp027 VALUES(12345678,  "Ted's Wallet", "2014-08-05 03:14:07", "Inactive");
INSERT INTO temp_rp027 VALUES(987654321, "Jane's Wallet" , "2014-08-05 03:14:07", "Damaged");
INSERT INTO temp_rp027 VALUES(5647382910, "Julie's Wallet", "2014-08-15 03:14:07", "Stolen");

-- View for Deactivated & Suspended Cards - RP027  
CREATE OR REPLACE VIEW DeactivatedSuspendedCards AS
SELECT WalletID, Nickname, DateExpires, Status FROM temp_rp027;


-- Table for Current Benefits Administrators - RP029
CREATE TABLE temp_rp028 (LastName VARCHAR(50), FirstName VARCHAR(50), OrgName VARCHAR(50), DiscountType VARCHAR(50),  DateExpires DateTime, PostalCode VARCHAR(10), PaymentMethod VARCHAR(50),  EmailAddress VARCHAR(50));
INSERT INTO temp_rp028 VALUES("Smith", "John" , "Albany University", "Full", "2014-08-05 03:14:07", "60606-1234", "Visa",  "jsmith@email.com");
INSERT INTO temp_rp028 VALUES("Trip", "Bruce" , "Albany University", "Half Fare", "2014-08-05 03:14:07", "60600-1234", "Paypal", "btrip@email.com");
INSERT INTO temp_rp028 VALUES("Zeno", "Zidane" , "Albany University", "Student", "2014-08-05 03:14:07", "60123-1234", "Mastercard", "zzenos@email.com");

-- View for Current Benefits Administrators - RP029
CREATE OR REPLACE VIEW CurrentCustomers AS
SELECT LastName, FirstName, OrgName, DiscountType, DateExpires, PostalCode, PaymentMethod, EmailAddress FROM temp_rp028;


-- Table for Current Benefits Administrators - RP029
CREATE TABLE temp_rp029 (LastName VARCHAR(50), FirstName VARCHAR(50), OrgName VARCHAR(50), ContactInfo VARCHAR(50), PaymentMethod VARCHAR(50), ContractDate DateTime, PostalCode VARCHAR(10));
INSERT INTO temp_rp029 VALUES("Smith", "John" , "Albany", "jsmith@email.com" , "Visa", "2014-08-05 03:14:07", "60606-1234");
INSERT INTO temp_rp029 VALUES("Trip", "Bruce" , "General Electric", "btrip@email.com", "Paypal", "2014-08-05 03:14:07", "60600-1234");
INSERT INTO temp_rp029 VALUES("Zeno", "Zidane" , "AT&T", "zzenos@email.com" , "Mastercard", "2014-08-05 03:14:07", "60123-1234");

-- View for Current Benefits Administrators - RP029
CREATE OR REPLACE VIEW CurrBenefitsAdmins AS
SELECT LastName, FirstName, OrgName, ContactInfo, PaymentMethod, ContractDate, PostalCode FROM temp_rp029;

-- Table for Pending Direct Autoload - RP030
CREATE TABLE temp_rp030 (WalletId BIGINT(20), Nickname VARCHAR(50), DateExpires DateTime, Product VARCHAR(100), PendingActivity VARCHAR(256), ValueActivity DECIMAL (5,2), PendingStatus VARCHAR(50), DateAdded DateTime, DateEffective DateTime);
INSERT INTO temp_rp030 VALUES(12345678, "Jim's Wallet" , "2014-08-05 03:14:07", "Value" , "Load", 25.00, "Pending", "2014-08-05 03:14:07", "2014-08-05 03:14:07");

-- View for Pending Direct Autoload - RP030
CREATE OR REPLACE VIEW PendingDirectAutoload AS
SELECT WalletId, Nickname, DateExpires, Product, PendingActivity, ValueActivity, PendingStatus, DateAdded, DateEffective FROM temp_rp030;


-- RP0310 Original SQL
-- SELECT pa.WalletID AS WalletId, w.Nickname AS NickName, pa.DateExpires AS DateExpires, pa.PendingStatus AS PendingStatus, at.description AS PendingActivity, 
-- pa.ValueActivity AS ValueActivity, pa.DateAdded AS DateAdded, pa.DateEffective AS DateEffective, pa.source AS SourceOfChange
-- FROM activitytypes at, pendingactivity pa, Wallets w
-- WHERE pa.WalletID = w.WalletID
-- AND pa.ActivityTypeID = at.ActivityTypeID
-- AND pa.DateExpires >=  $P{begdate} 
-- AND pa.DateExpires <=  $P{enddate}

-- Table for Active Subscription Autoloads - RP031
CREATE TABLE temp_rp031 (WalletId BIGINT(20), Nickname VARCHAR(50), DateExpires DateTime, productDesc VARCHAR(256), Activity VARCHAR(50), ValueActivity DECIMAL (5,2), PendingStatus VARCHAR(50), DateAdded DateTime, ReplenishThreshold VARCHAR(50), TicketDesc VARCHAR(100), DiscountType VARCHAR(100));
INSERT INTO temp_rp031 VALUES(12345678, "Jim's Wallet" , "2014-08-05 03:14:07", "Value" , "Load", 25.00, "Active", "2014-08-05 03:14:07", "$20.00", "Local", "Full Fare");
INSERT INTO temp_rp031 VALUES(987654321, "Nancy's" , "2014-08-05 03:14:07", "Value" , "Load", 50.00, "Active", "2014-08-05 03:14:07", "3 Days Before", "Regional", "Half Fare");
INSERT INTO temp_rp031 VALUES(5647382910, "Lil John" , "2014-08-05 03:14:07", "Value" , "Load", 20.00, "Active", "2014-08-05 03:14:07", "2 Rides Remaining", "Parking", "Student");

-- View for Active Subscription Autoloads - RP031
CREATE OR REPLACE VIEW ActiveSubscrAutoload AS
SELECT WalletId, Nickname, DateExpires, productDesc, Activity, ValueActivity, PendingStatus, DateAdded, ReplenishThreshold, TicketDesc, DiscountType FROM temp_rp031;

-- RP031 Original SQL
-- SELECT wc.WalletID AS WalletId,  w.Nickname AS Nickname, wc.DateExpires AS DateExpires, tt.Description AS ProductDescription, s.ReplenishValue AS ValueActivity, 
-- ss.Description AS PendingStatus, s.CreatedAt AS DateAdded, s.ReplenishThreshold AS ReplenishThreshold, t.Description AS TicketDescription, dt.Description AS DiscountType
-- FROM subscriptions s, subscriptionstatus ss,  tickets t, wallets w, walletcontents wc, discounttypes dt, tickettypes tt
-- WHERE s.walletcontentsid = wc.WalletContentsID
-- AND wc.WalletID = w.WalletID
-- AND wc.TicketID = t.TicketID
-- AND s.SubscriptionStatusID = ss.SubscriptionStatusID
-- AND wc.DiscountTypeID = dt.DiscountTypeID
-- AND t.TicketTypeID = tt.TicketTypeID



-- Table for Failed Autoload Payment Transaction - RP032  ***WILL NOT BE DONE BECAUSE WE DO NOT AUTOLOAD BEFORE GETTING PAID

-- View for Equipment Status - RP033
CREATE OR REPLACE VIEW EquipmentStatus AS
SELECT e.Description AS EquipmentDescr, e.SerialNumber, e.Version, es.Description AS EquipmentStatus, l.Description AS LocationDescr,  e.TouchedAt AS LastCommunication, l.Latitude, l.Longtitude
FROM equipment e
INNER JOIN locations l ON e.LocationID = l.LocationID
INNER JOIN equipmentstatuses es ON e.status = es.status;

-- Table for Current Software Configuration - RP034
--  Will not be done, same report as RP033, and will not be necessary to create, per Albany
-- View for Current Software Configuration - RP034


-- Table for Equipment Location - RP035
CREATE TABLE temp_rp035 (EquipmentDescr VARCHAR(50), SerialNumber VARCHAR(50), Version VARCHAR(50), EquipmentStatus VARCHAR(50), LocationDescr VARCHAR(50), DateTimeUpdated DateTime);
INSERT INTO temp_rp035 VALUES("Farebox 234", "ABDX123" , "1.25", "OK" , "Bus 123", "2014-08-05 03:14:07");
INSERT INTO temp_rp035 VALUES("Farebox 57", "001-302-123032" , "0.98", "Out of Service" , "Bus 234", "2014-08-11 03:14:07");
INSERT INTO temp_rp035 VALUES("Farebox 61", "ABDX124" , "1.25", "OK" , "Bus 456", "2014-08-15 03:14:07");

-- View for Equipment Location - RP035
CREATE OR REPLACE VIEW EquipmentLocation AS
SELECT EquipmentDescr, SerialNumber, Version, EquipmentStatus, LocationDescr, DateTimeUpdated FROM temp_rp035;


-- Table for Current Smart Card Status - RP036
CREATE TABLE temp_rp036 (WalletId BIGINT(20), OrgName VARCHAR(50), Status VARCHAR(50), Product VARCHAR(50), Value DECIMAL(9,2), DateLastAction DATETIME, LastTransDate DATETIME);
INSERT INTO temp_rp036 VALUES(12345678, "Albany CDTA" , "Active", "Monthly Pass", 1.00, "2014-08-01 03:14:07", "2014-08-01 03:14:07");
INSERT INTO temp_rp036 VALUES(343334553, "Albany University" , "Active", "Monthly Pass", 1.75, "2014-08-01 03:14:07", "2014-08-15 03:14:07");
INSERT INTO temp_rp036 VALUES(3949485835, "Albany Highschool" , "Inactive", "Daily Pass", 10.00, "2014-08-01 03:14:07", "2014-08-16 03:14:07");

-- View for Current Smart Card Status - RP036
CREATE OR REPLACE VIEW CurrentSmartCardStatus AS
SELECT WalletId, OrgName, Status, Product, Value, DateLastAction, LastTransDate FROM temp_rp036;



-- Table for Smart Card inventory Review - RP037   ***  Awaiting feedback from Albany for what will go into report - nothing heard back as of 8.27.2014
-- View for Smart Card inventory Review - RP037

-- Table for Stored Value Liability - RP038
CREATE TABLE temp_rp038 (TicketDesc VARCHAR(50), ValueRemaining DECIMAL(9,2), DiscountType VARCHAR(50), Status VARCHAR(50));
INSERT INTO temp_rp038 VALUES("3 Day Pass", 12500 , "Full Fare", "Inactive");
INSERT INTO temp_rp038 VALUES("Daily Pass", 12500 , "Half Fare", "Inactive");
INSERT INTO temp_rp038 VALUES("Period Pass", 16250 , "Student", "Inactive");
INSERT INTO temp_rp038 VALUES("Stepped Value", 45492.35 , "Full Fare", "Inactive");
INSERT INTO temp_rp038 VALUES("Stored Value", 45492.35 , "Half Fare", "Inactive");

-- View for Stored Value Liability - RP038
CREATE OR REPLACE VIEW StoredValueLiability AS
SELECT TicketDesc, ValueRemaining, DiscountType, Status FROM temp_rp038;

-- Table for Escheat Liability - RP039
CREATE TABLE temp_rp039 (TicketDesc VARCHAR(50), ValueRemaining DECIMAL(9,2), DiscountType VARCHAR(50), Status VARCHAR(50));
INSERT INTO temp_rp039 VALUES("3 Day Pass", 12500 , "Full Fare", "Inactive");
INSERT INTO temp_rp039 VALUES("Daily Pass", 12500 , "Half Fare", "Inactive");
INSERT INTO temp_rp039 VALUES("Period Pass", 16250 , "Student", "Inactive");
INSERT INTO temp_rp039 VALUES("Stepped Value", 45492.35 , "Full Fare", "Inactive");
INSERT INTO temp_rp039 VALUES("Stored Value", 45492.35 , "Half Fare", "Inactive");

-- View for Escheat Liability - RP039
CREATE OR REPLACE VIEW Escheat AS
SELECT TicketDesc, ValueRemaining, DiscountType, Status FROM temp_rp039;

-- Table for Escheat Liability - RP040
CREATE TABLE temp_rp040 (LastName VARCHAR(50), FirstName VARCHAR(50), Organization VARCHAR(50), Amounts DECIMAL(5,2), PaymentType VARCHAR(50), LastFour VARCHAR(4),  TransDate DateTime);
INSERT INTO temp_rp040 VALUES("Smith", "John" , "Fleet Middle School", 25.00 , "Visa", "4453", "2014-08-05 03:14:07");
INSERT INTO temp_rp040 VALUES("Trip", "Bruce" , "CDTA", 85.00, "Mastercard", "6611", "2014-08-05 03:14:07");
INSERT INTO temp_rp040 VALUES("Zeno", "Zidane" , "Customer", 44.50 , "Discover", "2582", "2014-08-05 03:14:07");

-- View for Escheat Liability - RP040
CREATE OR REPLACE VIEW BankcardReports AS
SELECT LastName, FirstName, Organization, Amounts, PaymentType, LastFour, TransDate FROM temp_rp040;

