ALTER TABLE `CrossReferenceFile` 
ADD COLUMN `TouchedBy`       VARCHAR(50),
ADD COLUMN `TouchedAt`       DATETIME DEFAULT NOW() NOT NULL;
 
UPDATE CrossReferenceFile
SET
CreatedBy = 'localhost',
CreatedAt = TouchedAt
WHERE (length(CreatedBy) = 0) or (length(CreatedAt)=0);