ALTER TABLE groups ADD `Type` varchar(50) NOT NULL;
ALTER TABLE privileges ADD `Type` varchar(50) NOT NULL;

INSERT INTO `groups`
(`GroupID`,
`OrganizationID`,
`TouchedBy`,
`Type`)
VALUES
(1,
10,
'root@localhost',
'ROLE_USER');

INSERT INTO `privileges`
(`PrivilegeID`,
`Description`,
`TouchedBy`,
`Type`)
VALUES
(1,
'Access Permission',
'root@localhost',
'PERM_ACCESS');

INSERT INTO `groupprivileges` 
(`GroupID`,
`TouchedBy`,
`PrivilegeID`)
VALUES
(1,
'root@localhost',
1);

INSERT INTO `groupsofpeople` 
(`GroupID`, 
`PersonID`,
`TouchedBy`) 
SELECT 1, p.PersonID, 'root@localhost' from people p;



