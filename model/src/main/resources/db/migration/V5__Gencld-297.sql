SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

ALTER TABLE `OrderItems` 
DROP FOREIGN KEY `RefWalletContents83`,
DROP FOREIGN KEY `RefTickets82`;

ALTER TABLE `Discounts` 
ADD COLUMN `DateExpires` DATE NULL DEFAULT NULL AFTER `Credentials`;

CREATE TABLE IF NOT EXISTS `Documents` (
  `DocumentID` INT(11) NULL DEFAULT NULL AUTO_INCREMENT,
  `PageKey` VARCHAR(512) NOT NULL,
  `PageText` TEXT NOT NULL,
  `LanguageID` TINYINT(3) UNSIGNED NOT NULL,
  `TouchedBy` VARCHAR(50) NOT NULL,
  `TouchedAt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`DocumentID`),
  INDEX `RefLanguages243` (`LanguageID` ASC),
  CONSTRAINT `RefLanguages243`
    FOREIGN KEY (`LanguageID`)
    REFERENCES `Languages` (`LanguageID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

CREATE TABLE IF NOT EXISTS `ExpirationTypes` (
  `ExpirationTypeID` TINYINT(3) UNSIGNED NULL DEFAULT NULL AUTO_INCREMENT,
  `Description` VARCHAR(50) NOT NULL,
  `TouchedBy` VARCHAR(50) NOT NULL,
  `TouchedAt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ExpirationTypeID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

ALTER TABLE `Languages` 
ADD COLUMN `Locale` CHAR(10) NULL DEFAULT NULL AFTER `Description`,
ADD COLUMN `FareBoxSeq` TINYINT(4) NOT NULL AFTER `Locale`;

ALTER TABLE `Offerings` 
ADD COLUMN `ParentOfferingID` TINYINT(3) UNSIGNED NULL DEFAULT NULL AFTER `OfferingID`,
ADD COLUMN `MediaID` TINYINT(3) UNSIGNED NULL DEFAULT NULL AFTER `UnitCost`,
ADD COLUMN `TicketID` INT(11) NULL DEFAULT NULL AFTER `MediaID`,
ADD COLUMN `DateEffective` DATE NULL DEFAULT NULL AFTER `QuantityMax`,
ADD COLUMN `DateExpires` DATE NULL DEFAULT NULL AFTER `DateEffective`,
ADD COLUMN `Status` TINYINT(4) NULL DEFAULT NULL AFTER `DateExpires`,
ADD INDEX `RefTickets244` (`TicketID` ASC),
ADD INDEX `RefOfferings245` (`ParentOfferingID` ASC),
ADD INDEX `RefMedia250` (`MediaID` ASC);

ALTER TABLE `OrderItems` 
DROP COLUMN `Slot`,
DROP COLUMN `TicketID`,
CHANGE COLUMN `WalletID` `WalletID` BIGINT(20) NULL DEFAULT NULL AFTER `OfferingID`,
ADD COLUMN `ParentItemNumber` TINYINT(3) UNSIGNED NULL DEFAULT NULL AFTER `OrderItemNumber`,
ADD INDEX `RefOrderItems246` (`OrderID` ASC, `ParentItemNumber` ASC),
ADD INDEX `RefWallets247` (`WalletID` ASC),
DROP INDEX `RefWalletContents83` ,
DROP INDEX `Ref1983` ,
DROP INDEX `Ref1082` ;

ALTER TABLE `Orders` 
ADD COLUMN `OrderData` LONGTEXT NULL DEFAULT NULL AFTER `OrganizationID`;

ALTER TABLE `Organizations` 
ADD COLUMN `CodeThirdParty` SMALLINT(6) NULL DEFAULT NULL AFTER `HasOwnCards`;

ALTER TABLE `Periods` 
CHANGE COLUMN `PeriodsPerYear` `PeriodsPerYear` TINYINT(3) UNSIGNED NULL DEFAULT NULL ,
ADD COLUMN `IsFloating` CHAR(1) NULL DEFAULT 'T' AFTER `Description`,
ADD COLUMN `ExpirationTypeID` TINYINT(3) UNSIGNED NOT NULL AFTER `RenewalDay`,
ADD INDEX `RefExpirationTypes248` (`ExpirationTypeID` ASC);

ALTER TABLE `Tickets` 
ADD COLUMN `DescriptionLong` TEXT NULL DEFAULT NULL AFTER `Description`,
ADD COLUMN `TermsConditions` TEXT NULL DEFAULT NULL AFTER `DescriptionLong`;

ALTER TABLE `Offerings` 
ADD CONSTRAINT `RefTickets244`
  FOREIGN KEY (`TicketID`)
  REFERENCES `Tickets` (`TicketID`),
ADD CONSTRAINT `RefOfferings245`
  FOREIGN KEY (`ParentOfferingID`)
  REFERENCES `Offerings` (`OfferingID`),
ADD CONSTRAINT `RefMedia250`
  FOREIGN KEY (`MediaID`)
  REFERENCES `Media` (`MediaID`);

ALTER TABLE `OrderItems` 
ADD CONSTRAINT `RefOrderItems246`
  FOREIGN KEY (`OrderID` , `ParentItemNumber`)
  REFERENCES `OrderItems` (`OrderID` , `OrderItemNumber`),
ADD CONSTRAINT `RefWallets247`
  FOREIGN KEY (`WalletID`)
  REFERENCES `Wallets` (`WalletID`);

ALTER TABLE `Periods` 
ADD CONSTRAINT `RefExpirationTypes248`
  FOREIGN KEY (`ExpirationTypeID`)
  REFERENCES `ExpirationTypes` (`ExpirationTypeID`);

/*  Data changes to go along with the table changes */

DELETE FROM TICKETTYPES
WHERE TICKETTYPEID = 4;

UPDATE TICKETTYPES
SET TICKETTYPEID = TICKETTYPEID - 1
WHERE TICKETTYPEID > 4;

UPDATE TICKETTYPES
SET DESCRIPTION = 'Period Pass'
WHERE TICKETTYPEID = 3;

INSERT EXPIRATIONTYPES (
	ExpirationTypeID
	,Description
	,TouchedBy
	,TouchedAt
)
Values (
	1
	,'Midnight'
	,Current_user()
	,Now()
);
INSERT EXPIRATIONTYPES (
	ExpirationTypeID
	,Description
	,TouchedBy
	,TouchedAt
)
Values (
	2
	,'End of Transit Day'
	,Current_user()
	,Now()
);
INSERT EXPIRATIONTYPES (
	ExpirationTypeID
	,Description
	,TouchedBy
	,TouchedAt
)
Values (
	3
	,'24 Hours'
	,Current_user()
	,Now()
);
INSERT EXPIRATIONTYPES (
	ExpirationTypeID
	,Description
	,TouchedBy
	,TouchedAt
)
Values (
	4
	,'Less than 24 Hours'
	,Current_user()
	,Now()
);

UPDATE PERIODS
SET PeriodID = 3
where PeriodID = 2;

UPDATE PERIODS
SET PeriodID = 2
where PeriodID = 1;

insert periods (
periodid
,description
,expirationTypeID
,touchedby
,touchedat
)
Values (
1
,'Floating Period'
,2
,current_user()
,now()
);

UPDATE PERIODS
SET IsFloating = 'T'
WHERE PeriodID = 1;

UPDATE PERIODS
SET IsFloating = 'F'
WHERE PeriodID > 1;

UPDATE TICKETS
SET ModifierID = 1
WHERE TicketTypeID = 3;

UPDATE TICKETS
SET TicketTypeID = 3
,ModifierID = ModifierID + 1
WHERE TicketTypeID = 4;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
