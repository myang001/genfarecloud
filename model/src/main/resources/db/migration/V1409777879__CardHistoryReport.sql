
CREATE OR REPLACE VIEW CardHistoryReport AS
	SELECT
		wh.DateActivity AS DateTime,
		at.description AS Event,
		wi.Identifier AS CardNumber,
		wh.ValueActivity AS ValuePlusMinus,
		pt.Description AS PaymentType,
		tt.Description AS Product,
		wh.AmountRemaining AS AmountRemaining,
		e.Description AS EquipmentDescription,
		r.RouteNumber AS Route,
		p.PersonId AS PersonId
	FROM
		WalletHistory wh
	INNER JOIN ActivityTypes at
		ON at.ActivityTypeID = wh.ActivityTypeID
	INNER JOIN TicketTypes tt
		ON tt.TicketTypeID = wh.TicketTypeID
	INNER JOIN Equipment e
		ON e.EquipmentID = wh.EquipmentID
	INNER JOIN PaymentTypes pt
		ON pt.PaymentTypeID = wh.PaymentTypeID
	INNER JOIN WalletIdentifiers wi
		ON wh.WalletId = wi.WalletId
	INNER JOIN Routes r
		ON r.RouteID = wh.RouteID
	INNER JOIN Wallets w
		ON w.WalletID = wh.WalletID
	INNER JOIN People p
		ON p.PersonID = w.PersonID
	WHERE
		wi.IdentifierID <> 1
	ORDER BY wh.DateActivity DESC
;

