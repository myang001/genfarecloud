ALTER TABLE subscriptions MODIFY ReplenishPrice int NOT NULL;

ALTER TABLE subscriptions MODIFY ReplenishValue int NOT NULL;

ALTER TABLE subscriptions MODIFY ReplenishThreshold int NOT NULL;
