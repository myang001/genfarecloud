CREATE TABLE OrganizationStatus (
    Status       TINYINT UNSIGNED NOT NULL,
    Description  VARCHAR(50),
    TouchedBy    VARCHAR(50) NOT NULL,
    TouchedAt    DATETIME DEFAULT NOW() NOT NULL,
    CreatedBy    VARCHAR(50) NOT NULL,
    CreatedAt    DATETIME DEFAULT NOW() NOT NULL,
    PRIMARY KEY (Status)
)ENGINE=INNODB
;

INSERT INTO OrganizationStatus (Status,Description,TouchedBy,CreatedBy) VALUES(1,'Active','admin','admin');
INSERT INTO OrganizationStatus (Status,Description,TouchedBy,CreatedBy) VALUES(2,'Disabled','admin','admin');

UPDATE Organizations SET Status = 1;

ALTER TABLE Organizations
	ADD CONSTRAINT RefOrganizationStatus
		FOREIGN KEY (Status)
		REFERENCES OrganizationStatus(Status)
;
