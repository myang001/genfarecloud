--
-- ER/Studio Data Architect 9.5 SQL Code Generation
-- Project :      GFI8-2.dm1
--
-- Date Created : Thursday, March 13, 2014 15:46:59
-- Target DBMS : MySQL 5.x
--

-- 
-- TABLE: Activities 
--

CREATE TABLE Activities(
    ActivityID      INT            AUTO_INCREMENT,
    OperatorID      INT,
    EquipmentID     INT            NOT NULL,
    DateActivity    DATETIME,
    Description     VARCHAR(50),
    TouchedBy       VARCHAR(50),
    TouchedAt       DATETIME       DEFAULT NOW() NOT NULL,
    PRIMARY KEY (ActivityID)
)ENGINE=INNODB
;



-- 
-- TABLE: ActivityAttributes 
--

CREATE TABLE ActivityAttributes(
    ActivityID        INT            NOT NULL,
    AttributeID       TINYINT UNSIGNED  NOT NULL,
    AttributeValue    VARCHAR(50),
    TouchedBy         VARCHAR(50),
    TouchedAt         DATETIME       DEFAULT NOW() NOT NULL,
    PRIMARY KEY (ActivityID, AttributeID)
)ENGINE=INNODB
;



-- 
-- TABLE: ActivityDetails 
--

CREATE TABLE ActivityDetails(
    ActivityID         INT              NOT NULL,
    Sequence           INT              NOT NULL,
    CardIdentifier     VARCHAR(50),
    Slot               TINYINT          NOT NULL,
    TicketID           INT              NOT NULL,
    DiscountTypeID     TINYINT UNSIGNED,
    AmountCharge       DECIMAL(5, 2),
    AmountRemaining    DECIMAL(5, 2),
    AmountTendered     DECIMAL(5, 2),
    AmountReturned     DECIMAL(5, 2),
    ActivityTypeID     TINYINT UNSIGNED  NOT NULL,
    PendingID          BIGINT,
    PendingStatus      TINYINT,
    Longtitude         CHAR(10),
    Latitude           CHAR(10),
    TSActivity         DATETIME,
    TouchedBy          VARCHAR(50)      NOT NULL,
    TouchedAt          DATETIME         DEFAULT NOW() NOT NULL,
    PRIMARY KEY (ActivityID, Sequence)
)ENGINE=INNODB
;



-- 
-- TABLE: ActivitySummary 
--

CREATE TABLE ActivitySummary(
    AcitivitySummaryID    BIGINT            AUTO_INCREMENT,
    ActivityID            INT               NOT NULL,
    DateID                INT               NOT NULL,
    ServiceLevelID        TINYINT UNSIGNED,
    TicketTypeID          TINYINT UNSIGNED,
    DiscountTypeID        TINYINT UNSIGNED,
    MediaID               TINYINT UNSIGNED,
    OrganizationTypeID    TINYINT UNSIGNED,
    LocationTypeID        TINYINT UNSIGNED,
    ActivityTypeID        TINYINT UNSIGNED  NOT NULL,
    CountAcitivity        INT,
    CountUse              INT,
    CountMin              INT,
    CountMax              INT,
    AmountMin             DECIMAL(10, 2),
    AmountMax             DECIMAL(10, 2),
    AmountChargedTotal    DECIMAL(10, 2),
    TouchedBy             VARCHAR(50)       NOT NULL,
    TouchedAt             DATETIME          DEFAULT NOW() NOT NULL,
    PRIMARY KEY (AcitivitySummaryID)
)ENGINE=INNODB
;



-- 
-- TABLE: ActivityTypes 
--

CREATE TABLE ActivityTypes(
    ActivityTypeID    TINYINT UNSIGNED  AUTO_INCREMENT,
    IsRevenue         CHAR(1)        DEFAULT 'N' NOT NULL,
    IsRidership       CHAR(1)        DEFAULT 'N' NOT NULL,
    Description       VARCHAR(50),
    TouchedBy         VARCHAR(50),
    TouchedAt         DATETIME       DEFAULT NOW() NOT NULL,
    CHECK (IsRevenue in ('Y', 'N')),
    CHECK (IsRidership in ('N', 'Y')),
    PRIMARY KEY (ActivityTypeID)
)ENGINE=INNODB
;



-- 
-- TABLE: AddressTypes 
--

CREATE TABLE AddressTypes(
    AddressTypeID    TINYINT UNSIGNED  AUTO_INCREMENT,
    Description      VARCHAR(50)    NOT NULL,
    TouchedBy        VARCHAR(50)    NOT NULL,
    TouchedAt        DATETIME       DEFAULT NOW() NOT NULL,
    PRIMARY KEY (AddressTypeID)
)ENGINE=INNODB
;



-- 
-- TABLE: Authentication 
--

CREATE TABLE Authentication(
    PersonID                INT             NOT NULL,
    AuthenticationTypeID    TINYINT UNSIGNED  NOT NULL,
    Response                VARCHAR(256),
    TouchedBy               VARCHAR(50)     NOT NULL,
    TouchedAt               DATETIME        DEFAULT NOW() NOT NULL,
    PRIMARY KEY (PersonID, AuthenticationTypeID)
)ENGINE=INNODB
;



-- 
-- TABLE: AuthenticationTypes 
--

CREATE TABLE AuthenticationTypes(
    AuthenticationTypeID    TINYINT UNSIGNED  AUTO_INCREMENT,
    Description             VARCHAR(50)     NOT NULL,
    Prompt                  VARCHAR(256),
    TouchedBy               VARCHAR(50)     NOT NULL,
    TouchedAt               DATETIME        DEFAULT NOW() NOT NULL,
    PRIMARY KEY (AuthenticationTypeID)
)ENGINE=INNODB
;



-- 
-- TABLE: Caps 
--

CREATE TABLE Caps(
    CapID          TINYINT UNSIGNED  NOT NULL,
    Description    VARCHAR(50)    NOT NULL,
    TouchedBy      VARCHAR(50)    NOT NULL,
    TouchedAt      DATETIME       DEFAULT NOW() NOT NULL,
    PRIMARY KEY (CapID)
)ENGINE=INNODB
;



-- 
-- TABLE: CapSteps 
--

CREATE TABLE CapSteps(
    CapID             TINYINT UNSIGNED  NOT NULL,
    Step              TINYINT UNSIGNED  NOT NULL,
    AmountCap         DECIMAL(5, 2),
    PeriodLookBack    TINYINT UNSIGNED,
    TouchedBy         VARCHAR(50)      NOT NULL,
    TouchedAt         DATETIME         DEFAULT NOW() NOT NULL,
    PRIMARY KEY (CapID, Step)
)ENGINE=INNODB
;



-- 
-- TABLE: CardActivity 
--

CREATE TABLE CardActivity(
    CardActivityID    BIGINT           AUTO_INCREMENT,
    WalletID          BIGINT           NOT NULL,
    Slot              TINYINT UNSIGNED  NOT NULL,
    ActivityTypeID    TINYINT UNSIGNED  NOT NULL,
    PendingID         BIGINT           NOT NULL,
    ValueActivity     DECIMAL(5, 2)    DEFAULT 0.00 NOT NULL,
    LoadSequence      TINYINT UNSIGNED  DEFAULT 1 NOT NULL,
    PendingStatus     TINYINT UNSIGNED,
    DateActivity      DATETIME,
    PRIMARY KEY (CardActivityID)
)ENGINE=INNODB
;



-- 
-- TABLE: CardUsage 
--

CREATE TABLE CardUsage(
    CardUsageID        BIGINT           AUTO_INCREMENT,
    WalletID           BIGINT           NOT NULL,
    Slot               TINYINT UNSIGNED  NOT NULL,
    DateActivity       DATETIME,
    ActivityTypeID     TINYINT UNSIGNED  NOT NULL,
    AmountCharge       DECIMAL(5, 2),
    AmountRemaining    DECIMAL(5, 2),
    PRIMARY KEY (CardUsageID)
)ENGINE=INNODB
;



-- 
-- TABLE: Dates 
--

CREATE TABLE Dates(
    DateID          INT         AUTO_INCREMENT,
    DateActivity    DATETIME,
    DateTransit     DATE,
    Year            SMALLINT    NOT NULL,
    Month           TINYINT UNSIGNED,
    Quarter         TINYINT UNSIGNED,
    CHECK (Quarter in (1,2,3,4)),
    PRIMARY KEY (DateID)
)ENGINE=INNODB
;



-- 
-- TABLE: Disabilities 
--

CREATE TABLE Disabilities(
    DisabilityID    CHAR(10)       NOT NULL,
    Description     VARCHAR(50)    NOT NULL,
    TouchedBy       VARCHAR(50)    NOT NULL,
    TouchedAt       DATETIME       DEFAULT NOW() NOT NULL,
    PRIMARY KEY (DisabilityID)
)ENGINE=INNODB
;



-- 
-- TABLE: Discounts 
--

CREATE TABLE Discounts(
    PersonID          INT            NOT NULL,
    DiscountTypeID    TINYINT UNSIGNED  NOT NULL,
    Credentials       VARCHAR(50),
    TouchedBy         VARCHAR(50)    NOT NULL,
    TouchedAt         DATETIME       DEFAULT NOW() NOT NULL,
    PRIMARY KEY (PersonID, DiscountTypeID)
)ENGINE=INNODB
;



-- 
-- TABLE: DiscountTypes 
--

CREATE TABLE DiscountTypes(
    DiscountTypeID    TINYINT UNSIGNED  AUTO_INCREMENT,
    Description       VARCHAR(50)     NOT NULL,
    Criteria          VARCHAR(256),
    TermLimit         INT,
    TouchedBy         VARCHAR(50)     NOT NULL,
    TouchedAt         DATETIME        DEFAULT NOW() NOT NULL,
    PRIMARY KEY (DiscountTypeID)
)ENGINE=INNODB
;



-- 
-- TABLE: Equipment 
--

CREATE TABLE Equipment(
    EquipmentID        INT            AUTO_INCREMENT,
    ContainerID        INT,
    LocationID         SMALLINT       NOT NULL,
    ManufacturerID     TINYINT UNSIGNED  NOT NULL,
    EquipmentTypeID    SMALLINT       NOT NULL,
    Description        VARCHAR(50),
    SerialNumber       VARCHAR(50),
    Version            CHAR(10),
    Status             TINYINT UNSIGNED,
    TouchedBy          VARCHAR(50),
    TouchedAt          DATETIME       DEFAULT NOW() NOT NULL,
    PRIMARY KEY (EquipmentID)
)ENGINE=INNODB
;



-- 
-- TABLE: EquipmentAttributes 
--

CREATE TABLE EquipmentAttributes(
    AttributeID        TINYINT UNSIGNED  NOT NULL,
    EquipmentTypeID    SMALLINT       NOT NULL,
    Description        VARCHAR(50),
    TouchedBy          VARCHAR(50),
    TouchedAt          DATETIME       DEFAULT NOW() NOT NULL,
    PRIMARY KEY (AttributeID)
)ENGINE=INNODB
;



-- 
-- TABLE: EquipmentTypes 
--

CREATE TABLE EquipmentTypes(
    EquipmentTypeID    SMALLINT       AUTO_INCREMENT,
    Description        VARCHAR(50),
    TouchedBy          VARCHAR(50),
    TouchedAt          DATETIME       DEFAULT NOW() NOT NULL,
    PRIMARY KEY (EquipmentTypeID)
)ENGINE=INNODB
;



-- 
-- TABLE: EventDetails 
--

CREATE TABLE EventDetails(
    ActivityID     INT            NOT NULL,
    Sequence       INT            NOT NULL,
    EventID        SMALLINT       NOT NULL,
    EquipmentID    INT            NOT NULL,
    TSEvent        TIMESTAMP      NOT NULL,
    State          CHAR(10),
    TouchedBy      VARCHAR(50),
    TouchedAt      DATETIME       DEFAULT NOW() NOT NULL,
    PRIMARY KEY (ActivityID, Sequence)
)ENGINE=INNODB
;



-- 
-- TABLE: Events 
--

CREATE TABLE Events(
    EventID        SMALLINT       AUTO_INCREMENT,
    Description    VARCHAR(50),
    TouchedBy      VARCHAR(50),
    TouchedAt      DATETIME       DEFAULT NOW() NOT NULL,
    PRIMARY KEY (EventID)
)ENGINE=INNODB
;



-- 
-- TABLE: GroupPrivileges 
--

CREATE TABLE GroupPrivileges(
    GroupID        INT            NOT NULL,
    PrivilegeID    SMALLINT       NOT NULL,
    TouchedBy      VARCHAR(50)    NOT NULL,
    TouchedAt      DATETIME       DEFAULT NOW() NOT NULL,
    PRIMARY KEY (GroupID, PrivilegeID)
)ENGINE=INNODB
;



-- 
-- TABLE: Groups 
--

CREATE TABLE Groups(
    GroupID           INT            AUTO_INCREMENT,
    OrganizationID    INT,
    TouchedBy         VARCHAR(50)    NOT NULL,
    TouchedAt         DATETIME       DEFAULT NOW() NOT NULL,
    PRIMARY KEY (GroupID)
)ENGINE=INNODB
;



-- 
-- TABLE: GroupsOfPeople 
--

CREATE TABLE GroupsOfPeople(
    GroupID      INT            NOT NULL,
    PersonID     INT            NOT NULL,
    TouchedBy    VARCHAR(50)    NOT NULL,
    TouchedAt    DATETIME       DEFAULT NOW() NOT NULL,
    PRIMARY KEY (GroupID, PersonID)
)ENGINE=INNODB
;



-- 
-- TABLE: IdentifierTypes 
--

CREATE TABLE IdentifierTypes(
    IdentifierID    TINYINT UNSIGNED  AUTO_INCREMENT,
    Description     VARCHAR(50)    NOT NULL,
    TouchedBy       VARCHAR(50)    NOT NULL,
    TouchedAt       DATETIME       DEFAULT NOW() NOT NULL,
    PRIMARY KEY (IdentifierID)
)ENGINE=INNODB
;



-- 
-- TABLE: InventoryTransactions 
--

CREATE TABLE InventoryTransactions(
    InventoryTransactionID    INT            AUTO_INCREMENT,
    LocationFromID            SMALLINT       NOT NULL,
    LocationToID              SMALLINT,
    ShippingListID            INT,
    LotNumber                 TINYINT UNSIGNED,
    InventoryTransTypeID      TINYINT UNSIGNED  NOT NULL,
    NumberStarting            VARCHAR(50),
    NumberEnding              VARCHAR(50),
    Quantity                  INT,
    TouchedBy                 VARCHAR(50)    NOT NULL,
    TouchedAt                 DATETIME       DEFAULT NOW() NOT NULL,
    PRIMARY KEY (InventoryTransactionID)
)ENGINE=INNODB
;



-- 
-- TABLE: InventoryTransTypes 
--

CREATE TABLE InventoryTransTypes(
    InventoryTransTypeID    TINYINT UNSIGNED  AUTO_INCREMENT,
    Description             VARCHAR(50)    NOT NULL,
    TouchedBy               VARCHAR(50)    NOT NULL,
    TouchedAt               DATETIME       DEFAULT NOW() NOT NULL,
    PRIMARY KEY (InventoryTransTypeID)
)ENGINE=INNODB
;



-- 
-- TABLE: Languages 
--

CREATE TABLE Languages(
    LanguageID     CHAR(10)       NOT NULL,
    Description    VARCHAR(50)    NOT NULL,
    TouchedBy      VARCHAR(50)    NOT NULL,
    TouchedAt      DATETIME       DEFAULT NOW() NOT NULL,
    PRIMARY KEY (LanguageID)
)ENGINE=INNODB
;



-- 
-- TABLE: LinkTypes 
--

CREATE TABLE LinkTypes(
    LinkTypeID     TINYINT UNSIGNED  AUTO_INCREMENT,
    Description    VARCHAR(50)    NOT NULL,
    TouchedBy      VARCHAR(50)    NOT NULL,
    TouchedAt      DATETIME       DEFAULT NOW() NOT NULL,
    PRIMARY KEY (LinkTypeID)
)ENGINE=INNODB
;



-- 
-- TABLE: Locations 
--

CREATE TABLE Locations(
    LocationID        SMALLINT       NOT NULL,
    Description       VARCHAR(50),
    LocationTypeID    TINYINT UNSIGNED  NOT NULL,
    Longtitude        CHAR(10),
    Latitude          CHAR(10),
    TouchedBy         VARCHAR(50),
    TouchedAt         DATETIME       DEFAULT NOW() NOT NULL,
    PRIMARY KEY (LocationID)
)ENGINE=INNODB
;



-- 
-- TABLE: LocationTypes 
--

CREATE TABLE LocationTypes(
    LocationTypeID    TINYINT UNSIGNED  AUTO_INCREMENT,
    Description       VARCHAR(50)    NOT NULL,
    TouchedBy         VARCHAR(50)    NOT NULL,
    TouchedAt         DATETIME       DEFAULT NOW() NOT NULL,
    PRIMARY KEY (LocationTypeID)
)ENGINE=INNODB
;



-- 
-- TABLE: Logins 
--

CREATE TABLE Logins(
    LoginID      INT        	 NOT NULL,
    PersonID     INT             NOT NULL,
    UserLogin    VARCHAR(256)    NOT NULL,
    TouchedBy    VARCHAR(50)     NOT NULL,
    TouchedAt    DATETIME        DEFAULT NOW() NOT NULL,
    PRIMARY KEY (LoginID)
)ENGINE=INNODB
;



-- 
-- TABLE: Lots 
--

CREATE TABLE Lots(
    ShippingListID    INT            NOT NULL,
    LotNumber         TINYINT UNSIGNED  NOT NULL,
    Quantity          INT,
    LocationID        SMALLINT       NOT NULL,
    NumberStarting    VARCHAR(50),
    NumberEnding      VARCHAR(50),
    TouchedBy         VARCHAR(50)    NOT NULL,
    TouchedAt         DATETIME       DEFAULT NOW() NOT NULL,
    PRIMARY KEY (ShippingListID, LotNumber)
)ENGINE=INNODB
;



-- 
-- TABLE: Manufacturers 
--

CREATE TABLE Manufacturers(
    ManufacturerID    TINYINT UNSIGNED  AUTO_INCREMENT,
    Description       VARCHAR(50),
    TouchedBy         VARCHAR(50),
    TouchedAt         DATETIME       DEFAULT NOW() NOT NULL,
    PRIMARY KEY (ManufacturerID)
)ENGINE=INNODB
;



-- 
-- TABLE: Media 
--

CREATE TABLE Media(
    MediaID           TINYINT UNSIGNED  AUTO_INCREMENT,
    Description       VARCHAR(50)    NOT NULL,
    AvailableSlots    TINYINT UNSIGNED  DEFAULT 1 NOT NULL,
    TouchedBy         VARCHAR(50)    NOT NULL,
    TouchedAt         DATETIME       DEFAULT NOW() NOT NULL,
    PRIMARY KEY (MediaID)
)ENGINE=INNODB
;



-- 
-- TABLE: Offerings 
--

CREATE TABLE Offerings(
    OfferingID        TINYINT UNSIGNED  AUTO_INCREMENT,
    Description       VARCHAR(50),
    UnitPrice         DECIMAL(5, 2)    DEFAULT 0.00 NOT NULL,
    UnitCost          DECIMAL(5, 2)    DEFAULT 0.00,
    IsSubscribable    CHAR(1)          DEFAULT 'N' NOT NULL,
    IsBulk            CHAR(1)          DEFAULT 'N' NOT NULL,
    TaxRateID         SMALLINT         NOT NULL,
    QuantityMax       SMALLINT         DEFAULT 1 NOT NULL,
    TouchedBy         VARCHAR(50)      NOT NULL,
    TouchedAt         DATETIME         DEFAULT NOW() NOT NULL,
    CHECK (IsSubscribable in ('Y','N')),
    CHECK (IsBulk in ('Y','N')),
    PRIMARY KEY (OfferingID)
)ENGINE=INNODB
;



-- 
-- TABLE: Operators 
--

CREATE TABLE Operators(
    OperatorID        INT            NOT NULL,
    EmployeeID        VARCHAR(16),
    PIN               VARCHAR(16),
    WalletID          BIGINT,
    OperatorTypeID    TINYINT UNSIGNED  NOT NULL,
    TouchedBy         VARCHAR(50),
    TouchedAt         DATETIME       DEFAULT NOW() NOT NULL,
    PRIMARY KEY (OperatorID)
)ENGINE=INNODB
;



-- 
-- TABLE: OperatorTypes 
--

CREATE TABLE OperatorTypes(
    OperatorTypeID    TINYINT UNSIGNED  AUTO_INCREMENT,
    Description       VARCHAR(50),
    TouchedBy         VARCHAR(50),
    TouchedAt         DATETIME       DEFAULT NOW() NOT NULL,
    PRIMARY KEY (OperatorTypeID)
)ENGINE=INNODB
;



-- 
-- TABLE: OrderItems 
--

CREATE TABLE OrderItems(
    OrderID            INT              NOT NULL,
    OrderItemNumber    TINYINT UNSIGNED  NOT NULL,
    OfferingID         TINYINT UNSIGNED  NOT NULL,
    TicketID           INT,
    DiscountTypeID     TINYINT UNSIGNED,
    WalletID           BIGINT,
    Slot               TINYINT UNSIGNED,
    Quantity           SMALLINT         DEFAULT 1 NOT NULL,
    UnitPrice          DECIMAL(5, 2)    DEFAULT 0.00 NOT NULL,
    Amount             DECIMAL(7, 2)    DEFAULT 0.00 NOT NULL,
    AmountTax          DECIMAL(5, 2),
    Status             TINYINT UNSIGNED  NOT NULL,
    TouchedBy          VARCHAR(50)      NOT NULL,
    TouchedAt          DATETIME         DEFAULT NOW() NOT NULL,
    CHECK (Quantity > 0),
    CHECK (UnitPrice >=0),
    PRIMARY KEY (OrderID, OrderItemNumber)
)ENGINE=INNODB
;



-- 
-- TABLE: Orders 
--

CREATE TABLE Orders(
    OrderID           INT              AUTO_INCREMENT,
    DateOrdered       DATETIME,
    AmountOrder       DECIMAL(7, 2),
    Notes             VARCHAR(4000),
    PersonID          INT,
    OrganizationID    INT,
    Status            TINYINT UNSIGNED  NOT NULL,
    TouchedBy         VARCHAR(50)      NOT NULL,
    TouchedAt         DATETIME         DEFAULT NOW() NOT NULL,
    PRIMARY KEY (OrderID)
)ENGINE=INNODB
;



-- 
-- TABLE: OrderStatuses 
--

CREATE TABLE OrderStatuses(
    Status         TINYINT UNSIGNED  NOT NULL,
    Description    VARCHAR(50),
    TouchedBy      VARCHAR(50)    NOT NULL,
    TouchedAt      DATETIME       DEFAULT NOW() NOT NULL,
    PRIMARY KEY (Status)
)ENGINE=INNODB
;



-- 
-- TABLE: OrganizationAddresses 
--

CREATE TABLE OrganizationAddresses(
    OrganizationID    INT             NOT NULL,
    AddressTypeID     TINYINT UNSIGNED  NOT NULL,
    AddressLine1      VARCHAR(256),
    AddressLine2      VARCHAR(256),
    City              VARCHAR(256),
    State             VARCHAR(256),
    PostalCode        CHAR(10),
    TouchedBy         VARCHAR(50)     NOT NULL,
    TouchedAt         DATETIME        DEFAULT NOW() NOT NULL,
    PRIMARY KEY (OrganizationID, AddressTypeID)
)ENGINE=INNODB
;



-- 
-- TABLE: OrganizationCards 
--

CREATE TABLE OrganizationCards(
    OrganizationID    INT             NOT NULL,
    CardNumber        TINYINT UNSIGNED  NOT NULL,
    PaymentTypeID     TINYINT UNSIGNED  NOT NULL,
    LastFour          CHAR(4)         NOT NULL,
    DateExpires       CHAR(4)         NOT NULL,
    Token             VARCHAR(256),
    TouchedBy         VARCHAR(50)     NOT NULL,
    TouchedAt         DATETIME        DEFAULT NOW() NOT NULL,
    PRIMARY KEY (OrganizationID, CardNumber)
)ENGINE=INNODB
;



-- 
-- TABLE: OrganizationLinks 
--

CREATE TABLE OrganizationLinks(
    OrganizationID    INT             NOT NULL,
    LinkTypeID        TINYINT UNSIGNED  NOT NULL,
    LinkValue         VARCHAR(256),
    IsPreferred       CHAR(1)         DEFAULT 'N',
    TouchedBy         VARCHAR(50)     NOT NULL,
    TouchedAt         DATETIME        DEFAULT NOW() NOT NULL,
    CHECK (IsPreferred in ('Y', 'N')),
    PRIMARY KEY (OrganizationID, LinkTypeID)
)ENGINE=INNODB
;



-- 
-- TABLE: Organizations 
--

CREATE TABLE Organizations(
    OrganizationID        INT              AUTO_INCREMENT,
    Name                  VARCHAR(256),
    OrganizationTypeID    TINYINT UNSIGNED  NOT NULL,
    BillingMethod         TINYINT UNSIGNED,
    PaymentMethod         TINYINT UNSIGNED,
    CreditLimit           DECIMAL(7, 2),
    Balance               DECIMAL(7, 2),
    HasOwnCards           CHAR(1)          DEFAULT 'N',
    Status                TINYINT UNSIGNED,
    TouchedBy             VARCHAR(50)      NOT NULL,
    TouchedAt             DATETIME         DEFAULT NOW() NOT NULL,
    CHECK (HasOwnCards in ('Y', 'N')),
    PRIMARY KEY (OrganizationID)
)ENGINE=INNODB
;



-- 
-- TABLE: OrganizationTypes 
--

CREATE TABLE OrganizationTypes(
    OrganizationTypeID    TINYINT UNSIGNED  AUTO_INCREMENT,
    Description           VARCHAR(50)    NOT NULL,
    IsRetail              CHAR(1)        DEFAULT 'N' NOT NULL,
    TouchedBy             VARCHAR(50)    NOT NULL,
    TouchedAt             DATETIME       DEFAULT NOW() NOT NULL,
    CHECK (IsRetail in ('N', 'Y')),
    PRIMARY KEY (OrganizationTypeID)
)ENGINE=INNODB
;



-- 
-- TABLE: Payments 
--

CREATE TABLE Payments(
    OrderID                     INT              NOT NULL,
    PaymentNumber               TINYINT UNSIGNED  NOT NULL,
    PaymentTypeID               TINYINT UNSIGNED  NOT NULL,
    Amount                      DECIMAL(7, 2)    DEFAULT 0 NOT NULL,
    ReferenceNumber             VARCHAR(50),
    DatePayment                 DATE,
    GatewayReturnCode           INT,
    GatewayReturnText           VARCHAR(256),
    GatewayTransactionNumber    BIGINT,
    GatewayTimeStamp            DATETIME,
    TouchedBy                   VARCHAR(50)      NOT NULL,
    TouchedAt                   DATETIME         DEFAULT NOW() NOT NULL,
    PRIMARY KEY (OrderID, PaymentNumber)
)ENGINE=INNODB
;



-- 
-- TABLE: PaymentTypes 
--

CREATE TABLE PaymentTypes(
    PaymentTypeID    TINYINT UNSIGNED  AUTO_INCREMENT,
    IsCredit         CHAR(1)        DEFAULT 'Y',
    Description      VARCHAR(50),
    TouchedBy        VARCHAR(50)    NOT NULL,
    TouchedAt        DATETIME       DEFAULT NOW() NOT NULL,
    CHECK (IsCredit in ('Y','N')),
    PRIMARY KEY (PaymentTypeID)
)ENGINE=INNODB
;



-- 
-- TABLE: PendingActivity 
--

CREATE TABLE PendingActivity(
    PendingID          BIGINT           AUTO_INCREMENT,
    OrderID            INT,
    OrderItemNumber    TINYINT UNSIGNED,
    WalletID           BIGINT,
    Slot               TINYINT UNSIGNED,
    PendingTypeID      TINYINT UNSIGNED  NOT NULL,
    TicketID           INT,
    DiscountTypeID     TINYINT UNSIGNED,
    ValueActivity      DECIMAL(5, 2)    DEFAULT 0.00 NOT NULL,
    LoadSequence       TINYINT UNSIGNED  DEFAULT 1 NOT NULL,
    PendingStatus      TINYINT UNSIGNED,
    DateAdded          DATETIME,
    DateEffective      DATETIME,
    DateApplied        DATETIME,
    DateExpires        DATETIME,
    TouchedBy          VARCHAR(50)      NOT NULL,
    TouchedAt          DATETIME         DEFAULT NOW() NOT NULL,
    PRIMARY KEY (PendingID)
)ENGINE=INNODB
;



-- 
-- TABLE: PendingTypes 
--

CREATE TABLE PendingTypes(
    PendingTypeID    TINYINT UNSIGNED  AUTO_INCREMENT,
    Description      VARCHAR(50),
    TouchedBy        VARCHAR(50),
    TouchedAt        DATETIME       DEFAULT NOW() NOT NULL,
    PRIMARY KEY (PendingTypeID)
)ENGINE=INNODB
;



-- 
-- TABLE: People 
--

CREATE TABLE People(
    PersonID        INT             AUTO_INCREMENT,
    ParentID        INT,
    Prefix          VARCHAR(10),
    NameLast        VARCHAR(256),
    NameMiddle      VARCHAR(256),
    NameFirst       VARCHAR(256),
    Suffix          VARCHAR(10),
    DateOfBirth     DATE,
    Gender          CHAR(1),
    Status          TINYINT UNSIGNED,
    LanguageID      CHAR(10),
    DisabilityID    CHAR(10),
    TouchedBy       VARCHAR(50)     NOT NULL,
    TouchedAt       DATETIME        DEFAULT NOW() NOT NULL,
    PRIMARY KEY (PersonID)
)ENGINE=INNODB
;



-- 
-- TABLE: PeopleAddresses 
--

CREATE TABLE PeopleAddresses(
    PersonID         INT             NOT NULL,
    AddressTypeID    TINYINT UNSIGNED  NOT NULL,
    AddressLine1     VARCHAR(256),
    AddressLine2     VARCHAR(256),
    City             VARCHAR(256),
    State            VARCHAR(256),
    PostalCode       CHAR(10),
    TouchedBy        VARCHAR(50)     NOT NULL,
    TouchedAt        DATETIME        DEFAULT NOW() NOT NULL,
    PRIMARY KEY (PersonID, AddressTypeID)
)ENGINE=INNODB
;



-- 
-- TABLE: PeopleCards 
--

CREATE TABLE PeopleCards(
    PersonID         INT             NOT NULL,
    CardNumber       TINYINT UNSIGNED  NOT NULL,
    PaymentTypeID    TINYINT UNSIGNED  NOT NULL,
    LastFour         CHAR(4)         NOT NULL,
    DateExpires      CHAR(4)         NOT NULL,
    Token            VARCHAR(256),
    TouchedBy        VARCHAR(50)     NOT NULL,
    TouchedAt        DATETIME        DEFAULT NOW() NOT NULL,
    PRIMARY KEY (PersonID, CardNumber)
)ENGINE=INNODB
;



-- 
-- TABLE: PeopleLinks 
--

CREATE TABLE PeopleLinks(
    PersonID       INT             NOT NULL,
    LinkTypeID     TINYINT UNSIGNED  NOT NULL,
    LinkValue      VARCHAR(256),
    IsPreferred    CHAR(1)         DEFAULT 'N',
    TouchedBy      VARCHAR(50)     NOT NULL,
    TouchedAt      DATETIME        DEFAULT NOW() NOT NULL,
    CONSTRAINT _1 CHECK (IsPreferred in ('Y', 'N')),
    PRIMARY KEY (PersonID, LinkTypeID)
)ENGINE=INNODB
;



-- 
-- TABLE: Periods 
--

CREATE TABLE Periods(
    TicketTypeID      TINYINT UNSIGNED  AUTO_INCREMENT,
    Description       VARCHAR(50)    NOT NULL,
    PeriodsPerYear    TINYINT UNSIGNED  NOT NULL,
    RenewalMonths     VARCHAR(30),
    RenewalDay        TINYINT UNSIGNED,
    TouchedBy         VARCHAR(50)    NOT NULL,
    TouchedAt         DATETIME       DEFAULT NOW() NOT NULL,
    PRIMARY KEY (TicketTypeID)
)ENGINE=INNODB
;



-- 
-- TABLE: Privileges 
--

CREATE TABLE Privileges(
    PrivilegeID    SMALLINT       AUTO_INCREMENT,
    Description    VARCHAR(50)    NOT NULL,
    TouchedBy      VARCHAR(50)    NOT NULL,
    TouchedAt      DATETIME       DEFAULT NOW() NOT NULL,
    PRIMARY KEY (PrivilegeID)
)ENGINE=INNODB
;



-- 
-- TABLE: ProcessedActivity 
--

CREATE TABLE ProcessedActivity(
    PendingID          BIGINT           AUTO_INCREMENT,
    OrderID            INT,
    OrderItemNumber    TINYINT UNSIGNED,
    WalletID           BIGINT,
    Slot               TINYINT UNSIGNED,
    PendingTypeID      TINYINT UNSIGNED  NOT NULL,
    TicketID           INT,
    DiscountTypeID     TINYINT UNSIGNED,
    ValueActivity      DECIMAL(5, 2)    DEFAULT 0.00 NOT NULL,
    LoadSequence       TINYINT          DEFAULT 1 NOT NULL,
    PendingStatus      TINYINT,
    DateAdded          DATETIME,
    DateEffective      DATETIME,
    DateApplied        DATETIME,
    DateExpires        DATETIME,
    TouchedBy          VARCHAR(50)      NOT NULL,
    TouchedAt          DATETIME         DEFAULT NOW() NOT NULL,
    PRIMARY KEY (PendingID)
)ENGINE=INNODB
;



-- 
-- TABLE: Purposes 
--

CREATE TABLE Purposes(
    PurposeID      TINYINT UNSIGNED  AUTO_INCREMENT,
    Description    VARCHAR(50)    NOT NULL,
    TouchedBy      VARCHAR(50)    NOT NULL,
    TouchedAt      DATETIME       DEFAULT NOW() NOT NULL,
    PRIMARY KEY (PurposeID)
)ENGINE=INNODB
;



-- 
-- TABLE: Routes 
--

CREATE TABLE Routes(
    RouteID        SMALLINT       AUTO_INCREMENT,
    RouteNumber    VARCHAR(10),
    Description    VARCHAR(50),
    TouchedBy      VARCHAR(50),
    TouchedAt      DATETIME       DEFAULT NOW() NOT NULL,
    PRIMARY KEY (RouteID)
)ENGINE=INNODB
;



-- 
-- TABLE: Sales 
--

CREATE TABLE Sales(
    SalesID            BIGINT           AUTO_INCREMENT,
    EquipmentID        INT              NOT NULL,
    OfferingID         TINYINT UNSIGNED  NOT NULL,
    OrderID            INT,
    OrderItemNumber    TINYINT UNSIGNED,
    DateID             INT              NOT NULL,
    Quantity           SMALLINT         DEFAULT 1 NOT NULL,
    UnitPrice          DECIMAL(5, 2)    DEFAULT 0.00 NOT NULL,
    Amount             DECIMAL(7, 2)    DEFAULT 0.00 NOT NULL,
    AmountShipping     DECIMAL(5, 2),
    AmountTax          DECIMAL(5, 2),
    CONSTRAINT _2 CHECK (Quantity > 0),
    CONSTRAINT _3 CHECK (UnitPrice >=0),
    PRIMARY KEY (SalesID)
)ENGINE=INNODB
;



-- 
-- TABLE: ServiceLevels 
--

CREATE TABLE ServiceLevels(
    ServiceLevelID    TINYINT UNSIGNED  NOT NULL,
    Description       VARCHAR(50)    NOT NULL,
    TouchedBy         VARCHAR(50)    NOT NULL,
    TouchedAt         DATETIME       DEFAULT NOW() NOT NULL,
    PRIMARY KEY (ServiceLevelID)
)ENGINE=INNODB
;



-- 
-- TABLE: Shipments 
--

CREATE TABLE Shipments(
    OrderID                  INT            NOT NULL,
    ShipmentNumber           TINYINT UNSIGNED  NOT NULL,
    OrderItemNumber          TINYINT UNSIGNED  NOT NULL,
    Quantity                 SMALLINT       DEFAULT 1 NOT NULL,
    ShipperID                TINYINT UNSIGNED  NOT NULL,
    ShipperTrackingNumber    VARCHAR(64),
    TouchedBy                VARCHAR(50)    NOT NULL,
    TouchedAt                DATETIME       DEFAULT NOW() NOT NULL,
    CONSTRAINT _4 CHECK (Quantity > 0),
    PRIMARY KEY (OrderID, ShipmentNumber)
)ENGINE=INNODB
;



-- 
-- TABLE: Shippers 
--

CREATE TABLE Shippers(
    ShipperID       TINYINT UNSIGNED  NOT NULL,
    RateSchedule    CHAR(10),
    Description     VARCHAR(50),
    TouchedBy       VARCHAR(50)    NOT NULL,
    TouchedAt       DATETIME       DEFAULT NOW() NOT NULL,
    PRIMARY KEY (ShipperID)
)ENGINE=INNODB
;



-- 
-- TABLE: ShippingLists 
--

CREATE TABLE ShippingLists(
    ShippingListID    INT            AUTO_INCREMENT,
    MediaID           TINYINT UNSIGNED,
    Supplier          VARCHAR(50),
    DateShipped       DATE,
    DateReceived      DATE,
    NumberPackages    TINYINT UNSIGNED,
    Quantity          INT,
    TouchedBy         VARCHAR(50)    NOT NULL,
    TouchedAt         DATETIME       DEFAULT NOW() NOT NULL,
    PRIMARY KEY (ShippingListID)
)ENGINE=INNODB
;



-- 
-- TABLE: Stops 
--

CREATE TABLE Stops(
    RouteID        SMALLINT         NOT NULL,
    StopNumber     SMALLINT         NOT NULL,
    LocationID     SMALLINT,
    Miles          DECIMAL(5, 2)    NOT NULL,
    IsAdded        CHAR(1)          DEFAULT 'Y',
    Description    VARCHAR(50),
    TouchedBy      VARCHAR(50),
    TouchedAt      DATETIME         DEFAULT NOW() NOT NULL,
    CHECK (IsAdded in ('Y','N')),
    PRIMARY KEY (RouteID, StopNumber)
)ENGINE=INNODB
;



-- 
-- TABLE: Subscriptions 
--

CREATE TABLE Subscriptions(
    PersonID                INT              NOT NULL,
    SubscriptionID          TINYINT UNSIGNED  NOT NULL,
    TicketID                INT              NOT NULL,
    DiscountTypeID          TINYINT UNSIGNED,
    PaymentTypeID           TINYINT UNSIGNED  NOT NULL,
    ReplenishPrice          DECIMAL(5, 2),
    ReplenishValue          DECIMAL(5, 2),
    ReplenishThreshold      DECIMAL(5, 2),
    DateReplenishExpires    DATE,
    CCToken                 CHAR(50),
    DateCCExpires           DATE             NOT NULL,
    TouchedBy               VARCHAR(50)      NOT NULL,
    TouchedAt               DATETIME         DEFAULT NOW() NOT NULL,
    PRIMARY KEY (PersonID, SubscriptionID)
)ENGINE=INNODB
;



-- 
-- TABLE: TaxRates 
--

CREATE TABLE TaxRates(
    TaxRateID      SMALLINT         AUTO_INCREMENT,
    Description    VARCHAR(50),
    Rate1          DECIMAL(5, 2)    DEFAULT 0.00 NOT NULL,
    Rate2          DECIMAL(5, 2)    DEFAULT 0.00 NOT NULL,
    Rate3          DECIMAL(5, 2)    DEFAULT 0.00 NOT NULL,
    Rate4          DECIMAL(5, 2)    DEFAULT 0.00 NOT NULL,
    Rate5          DECIMAL(5, 2)    DEFAULT 0.00 NOT NULL,
    TouchedBy      VARCHAR(50)      NOT NULL,
    TouchedAt      DATETIME         DEFAULT NOW() NOT NULL,
    PRIMARY KEY (TaxRateID)
)ENGINE=INNODB
;



-- 
-- TABLE: Thresholds 
--

CREATE TABLE Thresholds(
    ThresholdID    TINYINT UNSIGNED  AUTO_INCREMENT,
    Description    VARCHAR(50)    NOT NULL,
    TouchedBy      VARCHAR(50)    NOT NULL,
    TouchedAt      DATETIME       DEFAULT NOW() NOT NULL,
    PRIMARY KEY (ThresholdID)
)ENGINE=INNODB
;



-- 
-- TABLE: ThresholdSteps 
--

CREATE TABLE ThresholdSteps(
    ThresholdID    TINYINT UNSIGNED  NOT NULL,
    Step           TINYINT UNSIGNED  NOT NULL,
    StepStart      SMALLINT         NOT NULL,
    StepEnd        SMALLINT         NOT NULL,
    Value          DECIMAL(5, 2)    NOT NULL,
    TouchedBy      VARCHAR(50)      NOT NULL,
    TouchedAt      DATETIME         DEFAULT NOW() NOT NULL,
    PRIMARY KEY (ThresholdID, Step)
)ENGINE=INNODB
;



-- 
-- TABLE: TicketDiscounts 
--

CREATE TABLE TicketDiscounts(
    TicketID          INT             NOT NULL,
    DiscountTypeID    TINYINT UNSIGNED  NOT NULL,
    OrganizationID    INT,
    DeviceKey         CHAR(10),
    Display           VARCHAR(30),
    Description       VARCHAR(256),
    TouchedBy         VARCHAR(50)     NOT NULL,
    TouchedAt         DATETIME        DEFAULT NOW() NOT NULL,
    PRIMARY KEY (TicketID, DiscountTypeID)
)ENGINE=INNODB
;



-- 
-- TABLE: Tickets 
--

CREATE TABLE Tickets(
    TicketID          INT              AUTO_INCREMENT,
    Description       VARCHAR(256),
    Price             DECIMAL(5, 2)    NOT NULL,
    Days              TINYINT UNSIGNED  DEFAULT 0,
    Uses              TINYINT UNSIGNED  DEFAULT 0,
    MonetaryValue     DECIMAL(5, 2)    DEFAULT 0.00,
    ValueUse          DECIMAL(5, 2)    DEFAULT 0.00,
    TicketTypeID      TINYINT UNSIGNED  NOT NULL,
    ModifierID        TINYINT UNSIGNED,
    ValidDayID        TINYINT UNSIGNED  NOT NULL,
    ServiceLevelID    TINYINT UNSIGNED  NOT NULL,
    ZoneID            SMALLINT,
    DateEffective     DATE,
    DateExpires       DATE,
    Status            TINYINT UNSIGNED,
    TouchedBy         VARCHAR(50)      NOT NULL,
    TouchedAt         DATETIME         DEFAULT NOW() NOT NULL,
    PRIMARY KEY (TicketID)
)ENGINE=INNODB
;



-- 
-- TABLE: TicketTimes 
--

CREATE TABLE TicketTimes(
    TicketID        INT              NOT NULL,
    TimePeriodID    TINYINT UNSIGNED  NOT NULL,
    ValueUseDiff    DECIMAL(5, 2)    DEFAULT 0.00,
    TouchedBy       VARCHAR(50)      NOT NULL,
    TouchedAt       DATETIME         DEFAULT NOW() NOT NULL,
    PRIMARY KEY (TicketID, TimePeriodID)
)ENGINE=INNODB
;



-- 
-- TABLE: TicketTypes 
--

CREATE TABLE TicketTypes(
    TicketTypeID    TINYINT UNSIGNED  AUTO_INCREMENT,
    Description     VARCHAR(50)    NOT NULL,
    TouchedBy       VARCHAR(50)    NOT NULL,
    TouchedAt       DATETIME       DEFAULT NOW() NOT NULL,
    PRIMARY KEY (TicketTypeID)
)ENGINE=INNODB
;



-- 
-- TABLE: TimePeriods 
--

CREATE TABLE TimePeriods(
    TimePeriodID    TINYINT UNSIGNED  NOT NULL,
    Description     VARCHAR(256),
    TimeStart       TIME,
    TimeEnd         TIME,
    TouchedBy       VARCHAR(50)     NOT NULL,
    TouchedAt       DATETIME        DEFAULT NOW() NOT NULL,
    PRIMARY KEY (TimePeriodID)
)ENGINE=INNODB
;



-- 
-- TABLE: ValidDays 
--

CREATE TABLE ValidDays(
    ValidDayID     TINYINT UNSIGNED  AUTO_INCREMENT,
    Description    VARCHAR(50)    NOT NULL,
    IsDate         CHAR(1)        DEFAULT 'N',
    ValidDay       CHAR(10)       NOT NULL,
    TouchedBy      VARCHAR(50)    NOT NULL,
    TouchedAt      DATETIME       DEFAULT NOW() NOT NULL,
    CHECK (IsDate in ('N', 'Y')),
    PRIMARY KEY (ValidDayID)
)ENGINE=INNODB
;



-- 
-- TABLE: WalletContents 
--

CREATE TABLE WalletContents(
    WalletID          BIGINT           NOT NULL,
    Slot              TINYINT UNSIGNED  NOT NULL,
    TicketID          INT              NOT NULL,
    DiscountTypeID    TINYINT UNSIGNED,
    Priority          TINYINT UNSIGNED  DEFAULT 1 NOT NULL,
    IsBad             CHAR(1)          DEFAULT 'N' NOT NULL,
    DateStart         DATE,
    DateExpires       DATETIME,
    ValueOriginal     DECIMAL(5, 2),
    ValueRemaining    DECIMAL(5, 2),
    ValueUse          DECIMAL(5, 2),
    SlotStatus        TINYINT UNSIGNED  DEFAULT 1 NOT NULL,
    TouchedBy         VARCHAR(50)      NOT NULL,
    TouchedAt         DATETIME         DEFAULT NOW() NOT NULL,
    CHECK (IsBad in ('Y', 'N')),
    CHECK (SlotStatus in (1,2,3)),
    PRIMARY KEY (WalletID, Slot)
)ENGINE=INNODB
;



-- 
-- TABLE: WalletIdentifiers 
--

CREATE TABLE WalletIdentifiers(
    WalletID        BIGINT         NOT NULL,
    IdentifierID    TINYINT UNSIGNED  NOT NULL,
    Identifier      VARCHAR(50)    NOT NULL,
    TouchedBy       VARCHAR(50)    NOT NULL,
    TouchedAt       DATETIME       DEFAULT NOW() NOT NULL,
    PRIMARY KEY (WalletID, IdentifierID)
)ENGINE=INNODB
;



-- 
-- TABLE: Wallets 
--

CREATE TABLE Wallets(
    WalletID          BIGINT         AUTO_INCREMENT,
    PersonID          INT,
    DateExpires       DATETIME,
    IsBad             CHAR(1)        DEFAULT 'N',
    IsRegistered      CHAR(1)        DEFAULT 'N',
    IsThirdParty      CHAR(1)        DEFAULT 'N',
    LoadSequence      TINYINT UNSIGNED  DEFAULT 0 NOT NULL,
    OrganizationID    INT,
    PurposeID         TINYINT UNSIGNED  NOT NULL,
    Status            TINYINT UNSIGNED,
    MediaID           TINYINT UNSIGNED  NOT NULL,
    TouchedBy         VARCHAR(50)    NOT NULL,
    TouchedAt         DATETIME       DEFAULT NOW() NOT NULL,
    CHECK (IsBad in ('Y', 'N')),
    CHECK (IsRegistered in ('Y', 'N')),
    CHECK (IsThirdParty in ('Y', 'N')),
    PRIMARY KEY (WalletID)
)ENGINE=INNODB
;



-- 
-- TABLE: Zones 
--

CREATE TABLE Zones(
    ZoneID         SMALLINT       NOT NULL,
    Description    VARCHAR(50)    NOT NULL,
    TouchedBy      VARCHAR(50)    NOT NULL,
    TouchedAt      DATETIME       DEFAULT NOW() NOT NULL,
    PRIMARY KEY (ZoneID)
)ENGINE=INNODB
;



-- 
-- INDEX: Ref43131 
--

CREATE INDEX Ref43131 ON Activities(EquipmentID)
;
-- 
-- INDEX: Ref74132 
--

CREATE INDEX Ref74132 ON Activities(OperatorID)
;
-- 
-- INDEX: Ref81153 
--

CREATE INDEX Ref81153 ON ActivityAttributes(ActivityID)
;
-- 
-- INDEX: Ref75154 
--

CREATE INDEX Ref75154 ON ActivityAttributes(AttributeID)
;
-- 
-- INDEX: Ref4249 
--

CREATE INDEX Ref4249 ON ActivityDetails(ActivityTypeID)
;
-- 
-- INDEX: Ref3465 
--

CREATE INDEX Ref3465 ON ActivityDetails(PendingID)
;
-- 
-- INDEX: Ref1066 
--

CREATE INDEX Ref1066 ON ActivityDetails(TicketID)
;
-- 
-- INDEX: Ref12127 
--

CREATE INDEX Ref12127 ON ActivityDetails(DiscountTypeID)
;
-- 
-- INDEX: Ref81134 
--

CREATE INDEX Ref81134 ON ActivityDetails(ActivityID)
;
-- 
-- INDEX: Ref1570 
--

CREATE INDEX Ref1570 ON ActivitySummary(OrganizationTypeID)
;
-- 
-- INDEX: Ref1272 
--

CREATE INDEX Ref1272 ON ActivitySummary(DiscountTypeID)
;
-- 
-- INDEX: Ref1874 
--

CREATE INDEX Ref1874 ON ActivitySummary(MediaID)
;
-- 
-- INDEX: Ref2675 
--

CREATE INDEX Ref2675 ON ActivitySummary(LocationTypeID)
;
-- 
-- INDEX: Ref5776 
--

CREATE INDEX Ref5776 ON ActivitySummary(DateID)
;
-- 
-- INDEX: Ref4296 
--

CREATE INDEX Ref4296 ON ActivitySummary(ActivityTypeID)
;
-- 
-- INDEX: Ref70115 
--

CREATE INDEX Ref70115 ON ActivitySummary(ServiceLevelID)
;
-- 
-- INDEX: Ref77123 
--

CREATE INDEX Ref77123 ON ActivitySummary(TicketTypeID)
;
-- 
-- INDEX: Ref81137 
--

CREATE INDEX Ref81137 ON ActivitySummary(ActivityID)
;
-- 
-- INDEX: Ref346 
--

CREATE INDEX Ref346 ON Authentication(PersonID)
;
-- 
-- INDEX: Ref5567 
--

CREATE INDEX Ref5567 ON Authentication(AuthenticationTypeID)
;
-- 
-- INDEX: Ref71104 
--

CREATE INDEX Ref71104 ON CapSteps(CapID)
;
-- 
-- INDEX: Ref1993 
--

CREATE INDEX Ref1993 ON CardActivity(Slot, WalletID)
;
-- 
-- INDEX: Ref4298 
--

CREATE INDEX Ref4298 ON CardActivity(ActivityTypeID)
;
-- 
-- INDEX: Ref3494 
--

CREATE INDEX Ref3494 ON CardActivity(PendingID)
;
-- 
-- INDEX: Ref1995 
--

CREATE INDEX Ref1995 ON CardUsage(Slot, WalletID)
;
-- 
-- INDEX: Ref4297 
--

CREATE INDEX Ref4297 ON CardUsage(ActivityTypeID)
;
-- 
-- INDEX: Ref1212 
--

CREATE INDEX Ref1212 ON Discounts(DiscountTypeID)
;
-- 
-- INDEX: Ref313 
--

CREATE INDEX Ref313 ON Discounts(PersonID)
;
-- 
-- INDEX: Ref2551 
--

CREATE INDEX Ref2551 ON Equipment(LocationID)
;
-- 
-- INDEX: Ref4452 
--

CREATE INDEX Ref4452 ON Equipment(EquipmentTypeID)
;
-- 
-- INDEX: Ref5058 
--

CREATE INDEX Ref5058 ON Equipment(ManufacturerID)
;
-- 
-- INDEX: Ref4377 
--

CREATE INDEX Ref4377 ON Equipment(ContainerID)
;
-- 
-- INDEX: Ref44152 
--

CREATE INDEX Ref44152 ON EquipmentAttributes(EquipmentTypeID)
;
-- 
-- INDEX: Ref78117 
--

CREATE INDEX Ref78117 ON EventDetails(EventID)
;
-- 
-- INDEX: Ref43118 
--

CREATE INDEX Ref43118 ON EventDetails(EquipmentID)
;
-- 
-- INDEX: Ref81149 
--

CREATE INDEX Ref81149 ON EventDetails(ActivityID)
;
-- 
-- INDEX: Ref2830 
--

CREATE INDEX Ref2830 ON InventoryTransactions(InventoryTransTypeID)
;
-- 
-- INDEX: Ref2534 
--

CREATE INDEX Ref2534 ON InventoryTransactions(LocationFromID)
;
-- 
-- INDEX: Ref25139 
--

CREATE INDEX Ref25139 ON InventoryTransactions(LocationToID)
;
-- 
-- INDEX: Ref2627 
--

CREATE INDEX Ref2627 ON Locations(LocationTypeID)
;
-- 
-- INDEX: Ref17111 
--

CREATE INDEX Ref17111 ON Operators(WalletID)
;
-- 
-- INDEX: Ref83140 
--

CREATE INDEX Ref83140 ON Operators(OperatorTypeID)
;
-- 
-- INDEX: Ref3141 
--

CREATE INDEX Ref3141 ON Operators(OperatorID)
;
-- 
-- INDEX: Ref2937 
--

CREATE INDEX Ref2937 ON OrderItems(OrderID)
;
-- 
-- INDEX: Ref5881 
--

CREATE INDEX Ref5881 ON OrderItems(OfferingID)
;
-- 
-- INDEX: Ref1082 
--

CREATE INDEX Ref1082 ON OrderItems(TicketID)
;
-- 
-- INDEX: Ref6184 
--

CREATE INDEX Ref6184 ON OrderItems(Status)
;
-- 
-- INDEX: Ref12128 
--

CREATE INDEX Ref12128 ON OrderItems(DiscountTypeID)
;
-- 
-- INDEX: Ref1983 
--

CREATE INDEX Ref1983 ON OrderItems(Slot, WalletID)
;
-- 
-- INDEX: Ref378 
--

CREATE INDEX Ref378 ON Orders(PersonID)
;
-- 
-- INDEX: Ref279 
--

CREATE INDEX Ref279 ON Orders(OrganizationID)
;
-- 
-- INDEX: Ref6185 
--

CREATE INDEX Ref6185 ON Orders(Status)
;
-- 
-- INDEX: Ref23 
--

CREATE INDEX Ref23 ON OrganizationAddresses(OrganizationID)
;
-- 
-- INDEX: Ref97 
--

CREATE INDEX Ref97 ON OrganizationAddresses(AddressTypeID)
;
-- 
-- INDEX: Ref24 
--

CREATE INDEX Ref24 ON OrganizationLinks(OrganizationID)
;
-- 
-- INDEX: Ref810 
--

CREATE INDEX Ref810 ON OrganizationLinks(LinkTypeID)
;
-- 
-- INDEX: Ref1516 
--

CREATE INDEX Ref1516 ON Organizations(OrganizationTypeID)
;
-- 
-- INDEX: Ref2938 
--

CREATE INDEX Ref2938 ON Payments(OrderID)
;
-- 
-- INDEX: Ref5980 
--

CREATE INDEX Ref5980 ON Payments(PaymentTypeID)
;
-- 
-- INDEX: Ref3043 
--

CREATE INDEX Ref3043 ON PendingActivity(OrderItemNumber, OrderID)
;
-- 
-- INDEX: Ref1088 
--

CREATE INDEX Ref1088 ON PendingActivity(TicketID)
;
-- 
-- INDEX: Ref12129 
--

CREATE INDEX Ref12129 ON PendingActivity(DiscountTypeID)
;
-- 
-- INDEX: Ref1986 
--

CREATE INDEX Ref1986 ON PendingActivity(Slot, WalletID)
;
-- 
-- INDEX: Ref6287 
--

CREATE INDEX Ref6287 ON PendingActivity(PendingTypeID)
;
-- 
-- INDEX: Ref319 
--

CREATE INDEX Ref319 ON People(ParentID)
;
-- 
-- INDEX: Ref35 
--

CREATE INDEX Ref35 ON PeopleAddresses(PersonID)
;
-- 
-- INDEX: Ref98 
--

CREATE INDEX Ref98 ON PeopleAddresses(AddressTypeID)
;
-- 
-- INDEX: Ref36 
--

CREATE INDEX Ref36 ON PeopleLinks(PersonID)
;
-- 
-- INDEX: Ref89 
--

CREATE INDEX Ref89 ON PeopleLinks(LinkTypeID)
;
-- 
-- INDEX: Ref58119 
--

CREATE INDEX Ref58119 ON Sales(OfferingID)
;
-- 
-- INDEX: Ref30120 
--

CREATE INDEX Ref30120 ON Sales(OrderItemNumber, OrderID)
;
-- 
-- INDEX: Ref43122 
--

CREATE INDEX Ref43122 ON Sales(EquipmentID)
;
-- 
-- INDEX: Ref2936 
--

CREATE INDEX Ref2936 ON Shipments(OrderID)
;
-- 
-- INDEX: Ref30121 
--

CREATE INDEX Ref30121 ON Shipments(OrderItemNumber, OrderID)
;
-- 
-- INDEX: Ref84142 
--

CREATE INDEX Ref84142 ON Shipments(ShipperID)
;
-- 
-- INDEX: Ref1891 
--

CREATE INDEX Ref1891 ON ShippingLists(MediaID)
;
-- 
-- INDEX: Ref3143 
--

CREATE INDEX Ref3143 ON Subscriptions(PersonID)
;
-- 
-- INDEX: Ref10145 
--

CREATE INDEX Ref10145 ON Subscriptions(TicketID)
;
-- 
-- INDEX: Ref12146 
--

CREATE INDEX Ref12146 ON Subscriptions(DiscountTypeID)
;
-- 
-- INDEX: Ref59147 
--

CREATE INDEX Ref59147 ON Subscriptions(PaymentTypeID)
;
-- 
-- INDEX: Ref1014 
--

CREATE INDEX Ref1014 ON TicketDiscounts(TicketID)
;
-- 
-- INDEX: Ref1262 
--

CREATE INDEX Ref1262 ON TicketDiscounts(DiscountTypeID)
;
-- 
-- INDEX: Ref6390 
--

CREATE INDEX Ref6390 ON Tickets(ValidDayID)
;
-- 
-- INDEX: Ref70103 
--

CREATE INDEX Ref70103 ON Tickets(ServiceLevelID)
;
-- 
-- INDEX: Ref77116 
--

CREATE INDEX Ref77116 ON Tickets(TicketTypeID)
;
-- 
-- INDEX: Ref10101 
--

CREATE INDEX Ref10101 ON TicketTimes(TicketID)
;
-- 
-- INDEX: Ref68102 
--

CREATE INDEX Ref68102 ON TicketTimes(TimePeriodID)
;
-- 
-- INDEX: Ref10125 
--

CREATE INDEX Ref10125 ON WalletContents(TicketID)
;
-- 
-- INDEX: Ref1720 
--

CREATE INDEX Ref1720 ON WalletContents(WalletID)
;
-- 
-- INDEX: Ref12126 
--

CREATE INDEX Ref12126 ON WalletContents(DiscountTypeID)
;
-- 
-- INDEX: Ref1760 
--

CREATE INDEX Ref1760 ON WalletIdentifiers(WalletID)
;
-- 
-- INDEX: Ref5261 
--

CREATE INDEX Ref5261 ON WalletIdentifiers(IdentifierID)
;
-- 
-- INDEX: Ref1821 
--

CREATE INDEX Ref1821 ON Wallets(MediaID)
;
-- 
-- INDEX: Ref325 
--

CREATE INDEX Ref325 ON Wallets(PersonID)
;
-- 
-- INDEX: Ref2130 
--

CREATE INDEX Ref2130 ON Wallets(OrganizationID)
;
-- 
-- TABLE: Activities 
--

ALTER TABLE Activities ADD CONSTRAINT RefEquipment131 
    FOREIGN KEY (EquipmentID)
    REFERENCES Equipment(EquipmentID)
;

ALTER TABLE Activities ADD CONSTRAINT RefOperators132 
    FOREIGN KEY (OperatorID)
    REFERENCES Operators(OperatorID)
;


-- 
-- TABLE: ActivityAttributes 
--

ALTER TABLE ActivityAttributes ADD CONSTRAINT RefActivities153 
    FOREIGN KEY (ActivityID)
    REFERENCES Activities(ActivityID)
;

ALTER TABLE ActivityAttributes ADD CONSTRAINT RefEquipmentAttributes154 
    FOREIGN KEY (AttributeID)
    REFERENCES EquipmentAttributes(AttributeID)
;


-- 
-- TABLE: ActivityDetails 
--

ALTER TABLE ActivityDetails ADD CONSTRAINT RefActivityTypes49 
    FOREIGN KEY (ActivityTypeID)
    REFERENCES ActivityTypes(ActivityTypeID)
;

ALTER TABLE ActivityDetails ADD CONSTRAINT RefDiscountTypes127 
    FOREIGN KEY (DiscountTypeID)
    REFERENCES DiscountTypes(DiscountTypeID)
;

ALTER TABLE ActivityDetails ADD CONSTRAINT RefActivities134 
    FOREIGN KEY (ActivityID)
    REFERENCES Activities(ActivityID)
;

ALTER TABLE ActivityDetails ADD CONSTRAINT RefPendingActivity65 
    FOREIGN KEY (PendingID)
    REFERENCES PendingActivity(PendingID)
;

ALTER TABLE ActivityDetails ADD CONSTRAINT RefTickets66 
    FOREIGN KEY (TicketID)
    REFERENCES Tickets(TicketID)
;


-- 
-- TABLE: ActivitySummary 
--

ALTER TABLE ActivitySummary ADD CONSTRAINT RefOrganizationTypes70 
    FOREIGN KEY (OrganizationTypeID)
    REFERENCES OrganizationTypes(OrganizationTypeID)
;

ALTER TABLE ActivitySummary ADD CONSTRAINT RefDiscountTypes72 
    FOREIGN KEY (DiscountTypeID)
    REFERENCES DiscountTypes(DiscountTypeID)
;

ALTER TABLE ActivitySummary ADD CONSTRAINT RefMedia74 
    FOREIGN KEY (MediaID)
    REFERENCES Media(MediaID)
;

ALTER TABLE ActivitySummary ADD CONSTRAINT RefLocationTypes75 
    FOREIGN KEY (LocationTypeID)
    REFERENCES LocationTypes(LocationTypeID)
;

ALTER TABLE ActivitySummary ADD CONSTRAINT RefDates76 
    FOREIGN KEY (DateID)
    REFERENCES Dates(DateID)
;

ALTER TABLE ActivitySummary ADD CONSTRAINT RefActivityTypes96 
    FOREIGN KEY (ActivityTypeID)
    REFERENCES ActivityTypes(ActivityTypeID)
;

ALTER TABLE ActivitySummary ADD CONSTRAINT RefServiceLevels115 
    FOREIGN KEY (ServiceLevelID)
    REFERENCES ServiceLevels(ServiceLevelID)
;

ALTER TABLE ActivitySummary ADD CONSTRAINT RefTicketTypes123 
    FOREIGN KEY (TicketTypeID)
    REFERENCES TicketTypes(TicketTypeID)
;

ALTER TABLE ActivitySummary ADD CONSTRAINT RefActivities137 
    FOREIGN KEY (ActivityID)
    REFERENCES Activities(ActivityID)
;


-- 
-- TABLE: Authentication 
--

ALTER TABLE Authentication ADD CONSTRAINT RefPeople46 
    FOREIGN KEY (PersonID)
    REFERENCES People(PersonID)
;

ALTER TABLE Authentication ADD CONSTRAINT RefAuthenticationTypes67 
    FOREIGN KEY (AuthenticationTypeID)
    REFERENCES AuthenticationTypes(AuthenticationTypeID)
;


-- 
-- TABLE: CapSteps 
--

ALTER TABLE CapSteps ADD CONSTRAINT RefCaps104 
    FOREIGN KEY (CapID)
    REFERENCES Caps(CapID)
;


-- 
-- TABLE: CardActivity 
--

ALTER TABLE CardActivity ADD CONSTRAINT RefWalletContents93 
    FOREIGN KEY (WalletID, Slot)
    REFERENCES WalletContents(WalletID, Slot)
;

ALTER TABLE CardActivity ADD CONSTRAINT RefActivityTypes98 
    FOREIGN KEY (ActivityTypeID)
    REFERENCES ActivityTypes(ActivityTypeID)
;

ALTER TABLE CardActivity ADD CONSTRAINT RefPendingActivity94 
    FOREIGN KEY (PendingID)
    REFERENCES PendingActivity(PendingID)
;


-- 
-- TABLE: CardUsage 
--

ALTER TABLE CardUsage ADD CONSTRAINT RefWalletContents95 
    FOREIGN KEY (WalletID, Slot)
    REFERENCES WalletContents(WalletID, Slot)
;

ALTER TABLE CardUsage ADD CONSTRAINT RefActivityTypes97 
    FOREIGN KEY (ActivityTypeID)
    REFERENCES ActivityTypes(ActivityTypeID)
;


-- 
-- TABLE: Discounts 
--

ALTER TABLE Discounts ADD CONSTRAINT RefDiscountTypes12 
    FOREIGN KEY (DiscountTypeID)
    REFERENCES DiscountTypes(DiscountTypeID)
;

ALTER TABLE Discounts ADD CONSTRAINT RefPeople13 
    FOREIGN KEY (PersonID)
    REFERENCES People(PersonID)
;


-- 
-- TABLE: Equipment 
--

ALTER TABLE Equipment ADD CONSTRAINT RefLocations51 
    FOREIGN KEY (LocationID)
    REFERENCES Locations(LocationID)
;

ALTER TABLE Equipment ADD CONSTRAINT RefEquipmentTypes52 
    FOREIGN KEY (EquipmentTypeID)
    REFERENCES EquipmentTypes(EquipmentTypeID)
;

ALTER TABLE Equipment ADD CONSTRAINT RefManufacturers58 
    FOREIGN KEY (ManufacturerID)
    REFERENCES Manufacturers(ManufacturerID)
;

ALTER TABLE Equipment ADD CONSTRAINT RefEquipment77 
    FOREIGN KEY (ContainerID)
    REFERENCES Equipment(EquipmentID)
;


-- 
-- TABLE: EquipmentAttributes 
--

ALTER TABLE EquipmentAttributes ADD CONSTRAINT RefEquipmentTypes152 
    FOREIGN KEY (EquipmentTypeID)
    REFERENCES EquipmentTypes(EquipmentTypeID)
;


-- 
-- TABLE: EventDetails 
--

ALTER TABLE EventDetails ADD CONSTRAINT RefEvents117 
    FOREIGN KEY (EventID)
    REFERENCES Events(EventID)
;

ALTER TABLE EventDetails ADD CONSTRAINT RefEquipment118 
    FOREIGN KEY (EquipmentID)
    REFERENCES Equipment(EquipmentID)
;

ALTER TABLE EventDetails ADD CONSTRAINT RefActivities149 
    FOREIGN KEY (ActivityID)
    REFERENCES Activities(ActivityID)
;


-- 
-- TABLE: GroupPrivileges 
--

ALTER TABLE GroupPrivileges ADD CONSTRAINT RefPrivileges193 
    FOREIGN KEY (PrivilegeID)
    REFERENCES Privileges(PrivilegeID)
;

ALTER TABLE GroupPrivileges ADD CONSTRAINT RefGroups194 
    FOREIGN KEY (GroupID)
    REFERENCES Groups(GroupID)
;


-- 
-- TABLE: Groups 
--

ALTER TABLE Groups ADD CONSTRAINT RefOrganizations204 
    FOREIGN KEY (OrganizationID)
    REFERENCES Organizations(OrganizationID)
;


-- 
-- TABLE: GroupsOfPeople 
--

ALTER TABLE GroupsOfPeople ADD CONSTRAINT RefPeople188 
    FOREIGN KEY (PersonID)
    REFERENCES People(PersonID)
;

ALTER TABLE GroupsOfPeople ADD CONSTRAINT RefGroups189 
    FOREIGN KEY (GroupID)
    REFERENCES Groups(GroupID)
;


-- 
-- TABLE: InventoryTransactions 
--

ALTER TABLE InventoryTransactions ADD CONSTRAINT RefInventoryTransTypes30 
    FOREIGN KEY (InventoryTransTypeID)
    REFERENCES InventoryTransTypes(InventoryTransTypeID)
;

ALTER TABLE InventoryTransactions ADD CONSTRAINT RefLocations34 
    FOREIGN KEY (LocationFromID)
    REFERENCES Locations(LocationID)
;

ALTER TABLE InventoryTransactions ADD CONSTRAINT RefLocations139 
    FOREIGN KEY (LocationToID)
    REFERENCES Locations(LocationID)
;

ALTER TABLE InventoryTransactions ADD CONSTRAINT RefLots155 
    FOREIGN KEY (ShippingListID, LotNumber)
    REFERENCES Lots(ShippingListID, LotNumber)
;


-- 
-- TABLE: Locations 
--

ALTER TABLE Locations ADD CONSTRAINT RefLocationTypes27 
    FOREIGN KEY (LocationTypeID)
    REFERENCES LocationTypes(LocationTypeID)
;


-- 
-- TABLE: Logins 
--

ALTER TABLE Logins ADD CONSTRAINT RefPeople205 
    FOREIGN KEY (PersonID)
    REFERENCES People(PersonID)
;


-- 
-- TABLE: Lots 
--

ALTER TABLE Lots ADD CONSTRAINT RefLocations156 
    FOREIGN KEY (LocationID)
    REFERENCES Locations(LocationID)
;

ALTER TABLE Lots ADD CONSTRAINT RefShippingLists157 
    FOREIGN KEY (ShippingListID)
    REFERENCES ShippingLists(ShippingListID)
;


-- 
-- TABLE: Offerings 
--

ALTER TABLE Offerings ADD CONSTRAINT RefTaxRates165 
    FOREIGN KEY (TaxRateID)
    REFERENCES TaxRates(TaxRateID)
;


-- 
-- TABLE: Operators 
--

ALTER TABLE Operators ADD CONSTRAINT RefWallets111 
    FOREIGN KEY (WalletID)
    REFERENCES Wallets(WalletID)
;

ALTER TABLE Operators ADD CONSTRAINT RefOperatorTypes140 
    FOREIGN KEY (OperatorTypeID)
    REFERENCES OperatorTypes(OperatorTypeID)
;

ALTER TABLE Operators ADD CONSTRAINT RefPeople141 
    FOREIGN KEY (OperatorID)
    REFERENCES People(PersonID)
;


-- 
-- TABLE: OrderItems 
--

ALTER TABLE OrderItems ADD CONSTRAINT RefOrders37 
    FOREIGN KEY (OrderID)
    REFERENCES Orders(OrderID)
;

ALTER TABLE OrderItems ADD CONSTRAINT RefOfferings81 
    FOREIGN KEY (OfferingID)
    REFERENCES Offerings(OfferingID)
;

ALTER TABLE OrderItems ADD CONSTRAINT RefTickets82 
    FOREIGN KEY (TicketID)
    REFERENCES Tickets(TicketID)
;

ALTER TABLE OrderItems ADD CONSTRAINT RefWalletContents83 
    FOREIGN KEY (WalletID, Slot)
    REFERENCES WalletContents(WalletID, Slot)
;

ALTER TABLE OrderItems ADD CONSTRAINT RefOrderStatuses84 
    FOREIGN KEY (Status)
    REFERENCES OrderStatuses(Status)
;

ALTER TABLE OrderItems ADD CONSTRAINT RefDiscountTypes128 
    FOREIGN KEY (DiscountTypeID)
    REFERENCES DiscountTypes(DiscountTypeID)
;


-- 
-- TABLE: Orders 
--

ALTER TABLE Orders ADD CONSTRAINT RefPeople78 
    FOREIGN KEY (PersonID)
    REFERENCES People(PersonID)
;

ALTER TABLE Orders ADD CONSTRAINT RefOrganizations79 
    FOREIGN KEY (OrganizationID)
    REFERENCES Organizations(OrganizationID)
;

ALTER TABLE Orders ADD CONSTRAINT RefOrderStatuses85 
    FOREIGN KEY (Status)
    REFERENCES OrderStatuses(Status)
;


-- 
-- TABLE: OrganizationAddresses 
--

ALTER TABLE OrganizationAddresses ADD CONSTRAINT RefOrganizations3 
    FOREIGN KEY (OrganizationID)
    REFERENCES Organizations(OrganizationID)
;

ALTER TABLE OrganizationAddresses ADD CONSTRAINT RefAddressTypes7 
    FOREIGN KEY (AddressTypeID)
    REFERENCES AddressTypes(AddressTypeID)
;


-- 
-- TABLE: OrganizationCards 
--

ALTER TABLE OrganizationCards ADD CONSTRAINT RefPaymentTypes177 
    FOREIGN KEY (PaymentTypeID)
    REFERENCES PaymentTypes(PaymentTypeID)
;

ALTER TABLE OrganizationCards ADD CONSTRAINT RefOrganizations178 
    FOREIGN KEY (OrganizationID)
    REFERENCES Organizations(OrganizationID)
;


-- 
-- TABLE: OrganizationLinks 
--

ALTER TABLE OrganizationLinks ADD CONSTRAINT RefOrganizations4 
    FOREIGN KEY (OrganizationID)
    REFERENCES Organizations(OrganizationID)
;

ALTER TABLE OrganizationLinks ADD CONSTRAINT RefLinkTypes10 
    FOREIGN KEY (LinkTypeID)
    REFERENCES LinkTypes(LinkTypeID)
;


-- 
-- TABLE: Organizations 
--

ALTER TABLE Organizations ADD CONSTRAINT RefOrganizationTypes16 
    FOREIGN KEY (OrganizationTypeID)
    REFERENCES OrganizationTypes(OrganizationTypeID)
;


-- 
-- TABLE: Payments 
--

ALTER TABLE Payments ADD CONSTRAINT RefOrders38 
    FOREIGN KEY (OrderID)
    REFERENCES Orders(OrderID)
;

ALTER TABLE Payments ADD CONSTRAINT RefPaymentTypes80 
    FOREIGN KEY (PaymentTypeID)
    REFERENCES PaymentTypes(PaymentTypeID)
;


-- 
-- TABLE: PendingActivity 
--

ALTER TABLE PendingActivity ADD CONSTRAINT RefOrderItems43 
    FOREIGN KEY (OrderID, OrderItemNumber)
    REFERENCES OrderItems(OrderID, OrderItemNumber)
;

ALTER TABLE PendingActivity ADD CONSTRAINT RefWalletContents86 
    FOREIGN KEY (WalletID, Slot)
    REFERENCES WalletContents(WalletID, Slot)
;

ALTER TABLE PendingActivity ADD CONSTRAINT RefPendingTypes87 
    FOREIGN KEY (PendingTypeID)
    REFERENCES PendingTypes(PendingTypeID)
;

ALTER TABLE PendingActivity ADD CONSTRAINT RefTickets88 
    FOREIGN KEY (TicketID)
    REFERENCES Tickets(TicketID)
;

ALTER TABLE PendingActivity ADD CONSTRAINT RefDiscountTypes129 
    FOREIGN KEY (DiscountTypeID)
    REFERENCES DiscountTypes(DiscountTypeID)
;


-- 
-- TABLE: People 
--

ALTER TABLE People ADD CONSTRAINT RefPeople19 
    FOREIGN KEY (ParentID)
    REFERENCES People(PersonID)
;

ALTER TABLE People ADD CONSTRAINT RefLanguages206 
    FOREIGN KEY (LanguageID)
    REFERENCES Languages(LanguageID)
;

ALTER TABLE People ADD CONSTRAINT RefDisabilities207 
    FOREIGN KEY (DisabilityID)
    REFERENCES Disabilities(DisabilityID)
;


-- 
-- TABLE: PeopleAddresses 
--

ALTER TABLE PeopleAddresses ADD CONSTRAINT RefPeople5 
    FOREIGN KEY (PersonID)
    REFERENCES People(PersonID)
;

ALTER TABLE PeopleAddresses ADD CONSTRAINT RefAddressTypes8 
    FOREIGN KEY (AddressTypeID)
    REFERENCES AddressTypes(AddressTypeID)
;


-- 
-- TABLE: PeopleCards 
--

ALTER TABLE PeopleCards ADD CONSTRAINT RefPaymentTypes179 
    FOREIGN KEY (PaymentTypeID)
    REFERENCES PaymentTypes(PaymentTypeID)
;

ALTER TABLE PeopleCards ADD CONSTRAINT RefPeople180 
    FOREIGN KEY (PersonID)
    REFERENCES People(PersonID)
;


-- 
-- TABLE: PeopleLinks 
--

ALTER TABLE PeopleLinks ADD CONSTRAINT RefPeople6 
    FOREIGN KEY (PersonID)
    REFERENCES People(PersonID)
;

ALTER TABLE PeopleLinks ADD CONSTRAINT RefLinkTypes9 
    FOREIGN KEY (LinkTypeID)
    REFERENCES LinkTypes(LinkTypeID)
;


-- 
-- TABLE: ProcessedActivity 
--

ALTER TABLE ProcessedActivity ADD CONSTRAINT RefDiscountTypes208 
    FOREIGN KEY (DiscountTypeID)
    REFERENCES DiscountTypes(DiscountTypeID)
;

ALTER TABLE ProcessedActivity ADD CONSTRAINT RefOrderItems209 
    FOREIGN KEY (OrderID, OrderItemNumber)
    REFERENCES OrderItems(OrderID, OrderItemNumber)
;

ALTER TABLE ProcessedActivity ADD CONSTRAINT RefTickets210 
    FOREIGN KEY (TicketID)
    REFERENCES Tickets(TicketID)
;

ALTER TABLE ProcessedActivity ADD CONSTRAINT RefPendingTypes211 
    FOREIGN KEY (PendingTypeID)
    REFERENCES PendingTypes(PendingTypeID)
;

ALTER TABLE ProcessedActivity ADD CONSTRAINT RefWalletContents212 
    FOREIGN KEY (WalletID, Slot)
    REFERENCES WalletContents(WalletID, Slot)
;


-- 
-- TABLE: Sales 
--

ALTER TABLE Sales ADD CONSTRAINT RefDates190 
    FOREIGN KEY (DateID)
    REFERENCES Dates(DateID)
;

ALTER TABLE Sales ADD CONSTRAINT RefOfferings119 
    FOREIGN KEY (OfferingID)
    REFERENCES Offerings(OfferingID)
;

ALTER TABLE Sales ADD CONSTRAINT RefOrderItems120 
    FOREIGN KEY (OrderID, OrderItemNumber)
    REFERENCES OrderItems(OrderID, OrderItemNumber)
;

ALTER TABLE Sales ADD CONSTRAINT RefEquipment122 
    FOREIGN KEY (EquipmentID)
    REFERENCES Equipment(EquipmentID)
;


-- 
-- TABLE: Shipments 
--

ALTER TABLE Shipments ADD CONSTRAINT RefOrders36 
    FOREIGN KEY (OrderID)
    REFERENCES Orders(OrderID)
;

ALTER TABLE Shipments ADD CONSTRAINT RefOrderItems121 
    FOREIGN KEY (OrderID, OrderItemNumber)
    REFERENCES OrderItems(OrderID, OrderItemNumber)
;

ALTER TABLE Shipments ADD CONSTRAINT RefShippers142 
    FOREIGN KEY (ShipperID)
    REFERENCES Shippers(ShipperID)
;


-- 
-- TABLE: ShippingLists 
--

ALTER TABLE ShippingLists ADD CONSTRAINT RefMedia91 
    FOREIGN KEY (MediaID)
    REFERENCES Media(MediaID)
;


-- 
-- TABLE: Stops 
--

ALTER TABLE Stops ADD CONSTRAINT RefLocations166 
    FOREIGN KEY (LocationID)
    REFERENCES Locations(LocationID)
;

ALTER TABLE Stops ADD CONSTRAINT RefRoutes167 
    FOREIGN KEY (RouteID)
    REFERENCES Routes(RouteID)
;


-- 
-- TABLE: Subscriptions 
--

ALTER TABLE Subscriptions ADD CONSTRAINT RefPeople143 
    FOREIGN KEY (PersonID)
    REFERENCES People(PersonID)
;

ALTER TABLE Subscriptions ADD CONSTRAINT RefTickets145 
    FOREIGN KEY (TicketID)
    REFERENCES Tickets(TicketID)
;

ALTER TABLE Subscriptions ADD CONSTRAINT RefDiscountTypes146 
    FOREIGN KEY (DiscountTypeID)
    REFERENCES DiscountTypes(DiscountTypeID)
;

ALTER TABLE Subscriptions ADD CONSTRAINT RefPaymentTypes147 
    FOREIGN KEY (PaymentTypeID)
    REFERENCES PaymentTypes(PaymentTypeID)
;


-- 
-- TABLE: ThresholdSteps 
--

ALTER TABLE ThresholdSteps ADD CONSTRAINT RefThresholds182 
    FOREIGN KEY (ThresholdID)
    REFERENCES Thresholds(ThresholdID)
;


-- 
-- TABLE: TicketDiscounts 
--

ALTER TABLE TicketDiscounts ADD CONSTRAINT RefTickets14 
    FOREIGN KEY (TicketID)
    REFERENCES Tickets(TicketID)
;

ALTER TABLE TicketDiscounts ADD CONSTRAINT RefDiscountTypes62 
    FOREIGN KEY (DiscountTypeID)
    REFERENCES DiscountTypes(DiscountTypeID)
;

ALTER TABLE TicketDiscounts ADD CONSTRAINT RefOrganizations160 
    FOREIGN KEY (OrganizationID)
    REFERENCES Organizations(OrganizationID)
;


-- 
-- TABLE: Tickets 
--

ALTER TABLE Tickets ADD CONSTRAINT RefValidDays90 
    FOREIGN KEY (ValidDayID)
    REFERENCES ValidDays(ValidDayID)
;

ALTER TABLE Tickets ADD CONSTRAINT RefServiceLevels103 
    FOREIGN KEY (ServiceLevelID)
    REFERENCES ServiceLevels(ServiceLevelID)
;

ALTER TABLE Tickets ADD CONSTRAINT RefTicketTypes116 
    FOREIGN KEY (TicketTypeID)
    REFERENCES TicketTypes(TicketTypeID)
;

ALTER TABLE Tickets ADD CONSTRAINT RefZones191 
    FOREIGN KEY (ZoneID)
    REFERENCES Zones(ZoneID)
;


-- 
-- TABLE: TicketTimes 
--

ALTER TABLE TicketTimes ADD CONSTRAINT RefTickets101 
    FOREIGN KEY (TicketID)
    REFERENCES Tickets(TicketID)
;

ALTER TABLE TicketTimes ADD CONSTRAINT RefTimePeriods102 
    FOREIGN KEY (TimePeriodID)
    REFERENCES TimePeriods(TimePeriodID)
;


-- 
-- TABLE: WalletContents 
--

ALTER TABLE WalletContents ADD CONSTRAINT RefWallets20 
    FOREIGN KEY (WalletID)
    REFERENCES Wallets(WalletID)
;

ALTER TABLE WalletContents ADD CONSTRAINT RefDiscountTypes126 
    FOREIGN KEY (DiscountTypeID)
    REFERENCES DiscountTypes(DiscountTypeID)
;

ALTER TABLE WalletContents ADD CONSTRAINT RefTickets125 
    FOREIGN KEY (TicketID)
    REFERENCES Tickets(TicketID)
;


-- 
-- TABLE: WalletIdentifiers 
--

ALTER TABLE WalletIdentifiers ADD CONSTRAINT RefWallets60 
    FOREIGN KEY (WalletID)
    REFERENCES Wallets(WalletID)
;

ALTER TABLE WalletIdentifiers ADD CONSTRAINT RefIdentifierTypes61 
    FOREIGN KEY (IdentifierID)
    REFERENCES IdentifierTypes(IdentifierID)
;


-- 
-- TABLE: Wallets 
--

ALTER TABLE Wallets ADD CONSTRAINT RefPeople25 
    FOREIGN KEY (PersonID)
    REFERENCES People(PersonID)
;

ALTER TABLE Wallets ADD CONSTRAINT RefMedia21 
    FOREIGN KEY (MediaID)
    REFERENCES Media(MediaID)
;

ALTER TABLE Wallets ADD CONSTRAINT RefOrganizations130 
    FOREIGN KEY (OrganizationID)
    REFERENCES Organizations(OrganizationID)
;

ALTER TABLE Wallets ADD CONSTRAINT RefPurposes183 
    FOREIGN KEY (PurposeID)
    REFERENCES Purposes(PurposeID)
;


