-- Deactivated & Suspended Cards - RP027  
CREATE TABLE temp_rp027 (WalletID BIGINT(20), Nickname VARCHAR(50), DateExpires DateTime, Status VARCHAR(50));

-- View for Deactivated & Suspended Cards - RP027  
CREATE OR REPLACE VIEW DeactivatedSuspendedCards AS
SELECT WalletID, Nickname, DateExpires, Status FROM temp_rp027;