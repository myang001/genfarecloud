
INSERT INTO Modules (ModuleID,Name,Description,TouchedBy,CreatedBy) VALUES(2,'Admin','Administration Module','admin','admin');
INSERT INTO Modules (ModuleID,Name,Description,TouchedBy,CreatedBy) VALUES(3,'Customer Service','Customer Service Module','admin','admin');

-- User Management
INSERT INTO Components (ComponentID,ModuleID,Name,Description,TouchedBy,CreatedBy) VALUES(2,2,'User Management','Allows for the management of agency users','admin','admin');
INSERT INTO Privileges (PrivilegeID,ComponentID,Permission,Name,Description,TouchedBy,CreatedBy) VALUES(2,2,'PERM_ADMIN_USERMGMT_VIEW_USER','View Users','View User Details','admin','admin');
INSERT INTO Privileges (PrivilegeID,ComponentID,Permission,Name,Description,TouchedBy,CreatedBy) VALUES(3,2,'PERM_ADMIN_USERMGMT_CREATE_USER','Create Users','Create Users','admin','admin');
INSERT INTO Privileges (PrivilegeID,ComponentID,Permission,Name,Description,TouchedBy,CreatedBy) VALUES(4,2,'PERM_ADMIN_USERMGMT_UPDATE_USER','Update Users','Update User Details','admin','admin');
INSERT INTO Privileges (PrivilegeID,ComponentID,Permission,Name,Description,TouchedBy,CreatedBy) VALUES(5,2,'PERM_ADMIN_USERMGMT_ENABLE_USER','Enable/Disable Users','Enabe/Disable a User','admin','admin');
INSERT INTO Privileges (PrivilegeID,ComponentID,Permission,Name,Description,TouchedBy,CreatedBy) VALUES(6,2,'PERM_ADMIN_USERMGMT_ASSIGN_GROUP','Assign User Groups','Assign Users to groups','admin','admin');

-- Group Management
INSERT INTO Components (ComponentID,ModuleID,Name,Description,TouchedBy,CreatedBy) VALUES(3,2,'Group Management','Allows for the management of security groups','admin','admin');
INSERT INTO Privileges (PrivilegeID,ComponentID,Permission,Name,Description,TouchedBy,CreatedBy) VALUES(7,3,'PERM_ADMIN_GROUPMGMT_CREATE_GROUP','Create a Group','Create a group','admin','admin');
INSERT INTO Privileges (PrivilegeID,ComponentID,Permission,Name,Description,TouchedBy,CreatedBy) VALUES(8,3,'PERM_ADMIN_GROUPMGMT_UPDATE_GROUP','Update a Group','Update group details','admin','admin');
INSERT INTO Privileges (PrivilegeID,ComponentID,Permission,Name,Description,TouchedBy,CreatedBy) VALUES(9,3,'PERM_ADMIN_GROUPMGMT_ENABLE_GROUP','Enable/Disable Groups','Enable/Disable a group','admin','admin');

-- Organization Management
INSERT INTO Components (ComponentID,ModuleID,Name,Description,TouchedBy,CreatedBy) VALUES(4,2,'Organization Management','Allows for the management of organizations','admin','admin');
INSERT INTO Privileges (PrivilegeID,ComponentID,Permission,Name,Description,TouchedBy,CreatedBy) VALUES(10,4,'PERM_ADMIN_ORGMGMT_VIEW_ORG','View an Organization','View Organization details','admin','admin');
INSERT INTO Privileges (PrivilegeID,ComponentID,Permission,Name,Description,TouchedBy,CreatedBy) VALUES(11,4,'PERM_ADMIN_ORGMGMT_CRATE_ORG','Create an Organization','Create an Organization','admin','admin');
INSERT INTO Privileges (PrivilegeID,ComponentID,Permission,Name,Description,TouchedBy,CreatedBy) VALUES(12,4,'PERM_ADMIN_ORGMGMT_UPDATE_ORG','Update an Organization','Update Organization details','admin','admin');