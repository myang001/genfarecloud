CREATE TABLE equipmentstatuses (
  Status tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  Description varchar(50) DEFAULT NULL,
  TouchedBy varchar(50) NOT NULL DEFAULT 'root@localhost',
  TouchedAt datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  CreatedBy varchar(50) NOT NULL DEFAULT 'root@localhost',
  CreatedAt datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (Status)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE OR REPLACE VIEW EquipmentStatus AS
SELECT e.Description AS EquipmentDescr, e.SerialNumber, e.Version, e.Status AS EquipmentStatus, l.Description AS LocationDescr,  e.TouchedAt AS LastCommunication, l.Latitude, l.Longtitude
FROM equipment e
INNER JOIN locations l ON e.LocationID = l.LocationID
INNER JOIN equipmentstatuses es ON e.status = es.status
