
ALTER TABLE people ADD COLUMN EmployeeID VARCHAR(256) NULL DEFAULT NULL AFTER DisabilityID;

ALTER TABLE linktypes ADD COLUMN Category VARCHAR(50) NOT NULL AFTER Description;

UPDATE linktypes
SET
Category = 'PHONE'
WHERE DESCRIPTION like '%Phone%';

UPDATE linktypes
SET
Category = 'EMAIL'
WHERE DESCRIPTION like '%email%';