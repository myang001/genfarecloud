

CREATE OR REPLACE VIEW OrderHistoryReport AS
	SELECT
		o.OrderID,
		o.DateOrdered,
		o.AmountOrder,
		s.Description as Status,
		o.PersonID,
		oi.OrderItemNumber,
		oi.ParentItemNumber,
		oi.Quantity,
		oi.UnitPrice,
		oi.Amount,
		wwt.Description AS WalletDescription,
		wt.Description AS WalletTypeDescription,
		of.Description AS OfferingDescription,
		t.Description AS TicketDescription
	FROM
		OrderItems oi
	INNER JOIN Orders o 
		ON oi.OrderID = o.OrderID
	INNER JOIN Orderstatuses s 
		ON s.Status = o.Status
	LEFT OUTER JOIN WalletTypes wt
		ON wt.WalletTypeID = oi.WalletTypeID
	LEFT OUTER JOIN WalletTypes wwt
		ON wwt.WalletTypeID = (SELECT WalletTypeId FROM Wallets w WHERE w.WalletID = oi.WalletID)
	LEFT OUTER JOIN Offerings of
		ON of.OfferingID = oi.OfferingID
	LEFT OUTER JOIN Tickets t
		ON t.TicketID = oi.TicketID
	ORDER BY o.DateOrdered DESC, oi.OrderItemNumber ASC
;
