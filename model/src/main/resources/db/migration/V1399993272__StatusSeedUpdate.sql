
ALTER TABLE orders
	DROP FOREIGN KEY RefOrderStatuses85
;

ALTER TABLE Orders
	DROP FOREIGN KEY RefOrderStatuses86
;

ALTER TABLE orderitems
	DROP FOREIGN KEY RefOrderStatuses84
;

DELETE FROM OrderStatuses;
INSERT INTO OrderStatuses (Status,Description,TouchedBy,TouchedAt) VALUES (1,'Unsubmitted','root@localhost','2014-03-21 14:54:34');
INSERT INTO OrderStatuses (Status,Description,TouchedBy,TouchedAt) VALUES (2,'Submitted','root@localhost','2014-03-21 14:54:47');
INSERT INTO OrderStatuses (Status,Description,TouchedBy,TouchedAt) VALUES (3,'Fufillment','root@localhost','2014-03-21 14:55:31');
INSERT INTO OrderStatuses (Status,Description,TouchedBy,TouchedAt) VALUES (4,'Rejected','root@localhost','2014-03-21 14:55:31');
INSERT INTO OrderStatuses (Status,Description,TouchedBy,TouchedAt) VALUES (5,'Abandoned','root@localhost','2014-03-21 14:55:46');
INSERT INTO OrderStatuses (Status,Description,TouchedBy,TouchedAt) VALUES (6,'Complete','root@localhost','2014-03-21 14:55:46');
INSERT INTO OrderStatuses (Status,Description,TouchedBy,TouchedAt) VALUES (7,'Cancelled','root@localhost','2014-03-21 14:55:46');
INSERT INTO OrderStatuses (Status,Description,TouchedBy,TouchedAt) VALUES (8,'In Progress','root@localhost','2014-03-21 14:55:46');
INSERT INTO OrderStatuses (Status,Description,TouchedBy,TouchedAt) VALUES (9,'Unpaid','root@localhost','2014-03-21 14:55:46');
INSERT INTO OrderStatuses (Status,Description,TouchedBy,TouchedAt) VALUES (10,'Invoiced','root@localhost','2014-03-21 14:55:46');
INSERT INTO OrderStatuses (Status,Description,TouchedBy,TouchedAt) VALUES (11,'Paid','root@localhost','2014-03-21 14:55:46');

ALTER TABLE orders
	ADD CONSTRAINT RefOrderStatuses85
		FOREIGN KEY (Status)
		REFERENCES orderstatuses(Status)
;

ALTER TABLE Orders
	ADD CONSTRAINT RefOrderStatuses86
    	FOREIGN KEY (PaymentStatus)
    	REFERENCES OrderStatuses(Status)
;

ALTER TABLE orderitems
	ADD CONSTRAINT RefOrderStatuses84
		FOREIGN KEY (Status)
		REFERENCES orderstatuses(Status)
;