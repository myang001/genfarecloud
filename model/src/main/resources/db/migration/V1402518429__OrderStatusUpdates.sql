


ALTER TABLE orders
	DROP FOREIGN KEY RefOrderStatuses85
;

ALTER TABLE Orders
	DROP FOREIGN KEY RefOrderStatuses86
;

ALTER TABLE orderitems
	DROP FOREIGN KEY RefOrderStatuses84
;


ALTER TABLE OrderStatuses
	ADD COLUMN `IsPayment` CHAR(1) NOT NULL DEFAULT 'F' AFTER `IsFulfillment`
;


DELETE FROM OrderStatuses;
INSERT INTO OrderStatuses (Status,Description,IsItem,IsFulfillment,isPayment,TouchedBy,CreatedBy) VALUES (1,'Unsubmitted','T','F','F','jyore','jyore');
INSERT INTO OrderStatuses (Status,Description,IsItem,IsFulfillment,isPayment,TouchedBy,CreatedBy) VALUES (2,'Submitted','T','F','F','jyore','jyore');
INSERT INTO OrderStatuses (Status,Description,IsItem,IsFulfillment,isPayment,TouchedBy,CreatedBy) VALUES (3,'Fufillment','T','F','F','jyore','jyore');
INSERT INTO OrderStatuses (Status,Description,IsItem,IsFulfillment,isPayment,TouchedBy,CreatedBy) VALUES (4,'Rejected','T','F','F','jyore','jyore');
INSERT INTO OrderStatuses (Status,Description,IsItem,IsFulfillment,isPayment,TouchedBy,CreatedBy) VALUES (5,'Abandoned','T','F','F','jyore','jyore');
INSERT INTO OrderStatuses (Status,Description,IsItem,IsFulfillment,isPayment,TouchedBy,CreatedBy) VALUES (6,'Complete','T','F','F','jyore','jyore');
INSERT INTO OrderStatuses (Status,Description,IsItem,IsFulfillment,isPayment,TouchedBy,CreatedBy) VALUES (7,'Cancelled','T','F','F','jyore','jyore');
INSERT INTO OrderStatuses (Status,Description,IsItem,IsFulfillment,isPayment,TouchedBy,CreatedBy) VALUES (8,'In Progress','F','F','F','jyore','jyore');
INSERT INTO OrderStatuses (Status,Description,IsItem,IsFulfillment,isPayment,TouchedBy,CreatedBy) VALUES (9,'Unpaid','F','F','T','jyore','jyore');
INSERT INTO OrderStatuses (Status,Description,IsItem,IsFulfillment,isPayment,TouchedBy,CreatedBy) VALUES (10,'Invoiced','F','F','T','jyore','jyore');
INSERT INTO OrderStatuses (Status,Description,IsItem,IsFulfillment,isPayment,TouchedBy,CreatedBy) VALUES (11,'Paid','F','F','T','jyore','jyore');

ALTER TABLE orders
	ADD CONSTRAINT RefOrderStatuses85
		FOREIGN KEY (Status)
		REFERENCES orderstatuses(Status)
;

ALTER TABLE Orders
	ADD CONSTRAINT RefOrderStatuses86
    	FOREIGN KEY (PaymentStatus)
    	REFERENCES OrderStatuses(Status)
;

ALTER TABLE orderitems
	ADD CONSTRAINT RefOrderStatuses84
		FOREIGN KEY (Status)
		REFERENCES orderstatuses(Status)
;