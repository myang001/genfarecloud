ALTER TABLE organizations
DROP FOREIGN KEY RefOrganizationTypes16
;
ALTER TABLE activitysummary
DROP FOREIGN KEY RefOrganizationTypes70
;
DELETE FROM OrganizationTypes;
INSERT INTO OrganizationTypes (OrganizationTypeID,Description,IsRetail,TouchedBy,TouchedAt) VALUES (1,'School','F','root@localhost','2014-03-21 13:35:01');
INSERT INTO OrganizationTypes (OrganizationTypeID,Description,IsRetail,TouchedBy,TouchedAt) VALUES (2,'Vendor','T','root@localhost','2014-03-21 13:40:40');
INSERT INTO OrganizationTypes (OrganizationTypeID,Description,IsRetail,TouchedBy,TouchedAt) VALUES (3,'Sponsoring Employer','F','root@localhost','2014-03-21 13:40:40');
INSERT INTO ORGANIZATIONTYPES (OrganizationTypeID,Description,IsRetail,TOUCHEDBY) VALUES (4,'Non Profit', 'F', CURRENT_USER());
ALTER TABLE organizations
ADD CONSTRAINT RefOrganizationTypes16
FOREIGN KEY (OrganizationTypeID)
REFERENCES organizationtypes(OrganizationTypeID)
;
ALTER TABLE activitysummary
ADD CONSTRAINT RefOrganizationTypes70
FOREIGN KEY (OrganizationTypeID)
REFERENCES organizationtypes(OrganizationTypeID)
;
-------------------------------------------------------------------------------------------------------
ALTER TABLE orders
DROP FOREIGN KEY RefOrderStatuses85
;
ALTER TABLE orderitems
DROP FOREIGN KEY RefOrderStatuses84
;
DELETE FROM OrderStatuses;
INSERT INTO OrderStatuses (Status,Description,TouchedBy,TouchedAt) VALUES (1,'New Order','root@localhost','2014-03-21 14:54:34');
INSERT INTO OrderStatuses (Status,Description,TouchedBy,TouchedAt) VALUES (2,'Open Order','root@localhost','2014-03-21 14:54:47');
INSERT INTO OrderStatuses (Status,Description,TouchedBy,TouchedAt) VALUES (3,'In Shipping','root@localhost','2014-03-21 14:55:31');
INSERT INTO OrderStatuses (Status,Description,TouchedBy,TouchedAt) VALUES (4,'Closed Order','root@localhost','2014-03-21 14:55:46');
ALTER TABLE orders
ADD CONSTRAINT RefOrderStatuses85
FOREIGN KEY (Status)
REFERENCES orderstatuses(Status)
;
ALTER TABLE orderitems
ADD CONSTRAINT RefOrderStatuses84
FOREIGN KEY (Status)
REFERENCES orderstatuses(Status)
;
-------------------------------------------------------------------------------------------------------
ALTER TABLE organizationaddresses
DROP FOREIGN KEY RefAddressTypes7
;
ALTER TABLE peopleaddresses
DROP FOREIGN KEY RefAddressTypes8
;
DELETE FROM ADDRESSTYPES;
INSERT INTO AddressTypes (AddressTypeID,Description,TouchedBy,TouchedAt) VALUES (1,'Billing Address','root@localhost','2014-03-21 15:03:16');
INSERT INTO AddressTypes (AddressTypeID,Description,TouchedBy,TouchedAt) VALUES (2,'Shipping Address','root@localhost','2014-03-21 15:03:25');
INSERT INTO AddressTypes (AddressTypeID,Description,TouchedBy,TouchedAt) VALUES (3,'Home Address','root@localhost','2014-03-21 15:03:33');
INSERT INTO AddressTypes (AddressTypeID,Description,TouchedBy,TouchedAt) VALUES (4,'Work Address','root@localhost','2014-03-21 15:03:43');
INSERT INTO AddressTypes (AddressTypeID,Description,TouchedBy,TouchedAt) VALUES (5,'School Address','root@localhost','2014-03-21 15:03:53');
ALTER TABLE organizationaddresses
ADD CONSTRAINT RefAddressTypes7
FOREIGN KEY (AddressTypeID)
REFERENCES addresstypes(AddressTypeID)
;
ALTER TABLE peopleaddresses
ADD CONSTRAINT RefAddressTypes8
FOREIGN KEY (AddressTypeID)
REFERENCES addresstypes(AddressTypeID)
;
-------------------------------------------------------------------------------------------------------
ALTER TABLE organizationlinks
DROP FOREIGN KEY RefLinkTypes10
;
ALTER TABLE peoplelinks
DROP FOREIGN KEY RefLinkTypes9
;
DELETE FROM LINKTYPES;
INSERT INTO LinkTypes (LinkTypeID,Description,TouchedBy,TouchedAt) VALUES (1,'Home Phone','root@localhost','2014-03-21 15:07:12');
INSERT INTO LinkTypes (LinkTypeID,Description,TouchedBy,TouchedAt) VALUES (2,'Work Phone','root@localhost','2014-03-21 15:07:19');
INSERT INTO LinkTypes (LinkTypeID,Description,TouchedBy,TouchedAt) VALUES (3,'Mobile Phone','root@localhost','2014-03-21 15:07:25');
INSERT INTO LinkTypes (LinkTypeID,Description,TouchedBy,TouchedAt) VALUES (4,'Work eMail','root@localhost','2014-03-21 15:07:44');
INSERT INTO LinkTypes (LinkTypeID,Description,TouchedBy,TouchedAt) VALUES (5,'Personal eMail','root@localhost','2014-03-21 15:07:58');
ALTER TABLE organizationlinks
ADD CONSTRAINT RefLinkTypes10
FOREIGN KEY (LinkTypeID)
REFERENCES linktypes(LinkTypeID)
;
ALTER TABLE peoplelinks
ADD CONSTRAINT RefLinkTypes9
FOREIGN KEY (LinkTypeID)
REFERENCES linktypes(LinkTypeID)
;
-------------------------------------------------------------------------------------------------------
ALTER TABLE organizationcards
DROP FOREIGN KEY RefPaymentTypes177
;
ALTER TABLE organizations
DROP FOREIGN KEY RefPaymentTypes225
;
ALTER TABLE payments
DROP FOREIGN KEY RefPaymentTypes80
;
ALTER TABLE peoplecards
DROP FOREIGN KEY RefPaymentTypes179
;
ALTER TABLE sales
DROP FOREIGN KEY RefPaymentTypes226
;
ALTER TABLE subscriptions
DROP FOREIGN KEY RefPaymentTypes147
;
DELETE FROM PAYMENTTYPES;
INSERT INTO PaymentTypes (PaymentTypeID,IsCredit,Description,TouchedBy,TouchedAt) VALUES (1,'F','Invoiced','root@localhost','2014-03-21 15:12:48');
INSERT INTO PaymentTypes (PaymentTypeID,IsCredit,Description,TouchedBy,TouchedAt) VALUES (2,'F','Cash','root@localhost','2014-03-21 15:12:56');
INSERT INTO PaymentTypes (PaymentTypeID,IsCredit,Description,TouchedBy,TouchedAt) VALUES (3,'F','Check','root@localhost','2014-03-21 15:13:01');
INSERT INTO PaymentTypes (PaymentTypeID,IsCredit,Description,TouchedBy,TouchedAt) VALUES (4,'T','American Express','root@localhost','2014-03-21 15:13:20');
INSERT INTO PaymentTypes (PaymentTypeID,IsCredit,Description,TouchedBy,TouchedAt) VALUES (5,'T','Visa','root@localhost','2014-03-21 15:13:34');
INSERT INTO PaymentTypes (PaymentTypeID,IsCredit,Description,TouchedBy,TouchedAt) VALUES (6,'T','Master Card','root@localhost','2014-03-21 15:13:45');
ALTER TABLE organizationcards
ADD CONSTRAINT RefPaymentTypes177
FOREIGN KEY (PaymentTypeID)
REFERENCES paymenttypes(PaymentTypeID)
;
ALTER TABLE organizations
ADD CONSTRAINT RefPaymentTypes225
FOREIGN KEY (PaymentTypeID)
REFERENCES paymenttypes(PaymentTypeID)
;
ALTER TABLE payments
ADD CONSTRAINT RefPaymentTypes80
FOREIGN KEY (PaymentTypeID)
REFERENCES paymenttypes(PaymentTypeID)
;
ALTER TABLE peoplecards
ADD CONSTRAINT RefPaymentTypes179
FOREIGN KEY (PaymentTypeID)
REFERENCES paymenttypes(PaymentTypeID)
;
ALTER TABLE sales
ADD CONSTRAINT RefPaymentTypes226
FOREIGN KEY (PaymentTypeID)
REFERENCES paymenttypes(PaymentTypeID)
;
ALTER TABLE subscriptions
ADD CONSTRAINT RefPaymentTypes147
FOREIGN KEY (PaymentTypeID)
REFERENCES paymenttypes(PaymentTypeID)
;
-------------------------------------------------------------------------------------------------------
ALTER TABLE people
DROP FOREIGN KEY RefDisabilities207
;
DELETE FROM DISABILITIES;
INSERT INTO disabilities (DisabilityID,Description,TouchedBy,TouchedAt) VALUES (1,'Visually Impaired','root@localhost','2014-03-25 14:58:54.');
INSERT INTO disabilities (DisabilityID,Description,TouchedBy,TouchedAt) VALUES (2,'Hearing Impaired','root@localhost','2014-03-25 14:59:17.');
INSERT INTO disabilities (DisabilityID,Description,TouchedBy,TouchedAt) VALUES (3,'Visual and Hearing Impaired','root@localhost','2014-03-25 14:59:37.');
ALTER TABLE people
ADD CONSTRAINT RefDisabilities207
FOREIGN KEY (DisabilityID)
REFERENCES disabilities(DisabilityID)
;
-------------------------------------------------------------------------------------------------------
ALTER TABLE documents
DROP FOREIGN KEY RefLanguages243
;
ALTER TABLE people
DROP FOREIGN KEY RefLanguages206
;
DELETE FROM LANGUAGES;
INSERT INTO languages (LanguageID,Description,Locale,FareBoxSeq,TouchedBy,TouchedAt) VALUES (1,'English','en_US',1,'mmcgraw','2013-09-20 08:57:00.');
INSERT INTO languages (LanguageID,Description,Locale,FareBoxSeq,TouchedBy,TouchedAt) VALUES (2,'French','fr_Ca',3,'mmcgraw','2013-09-20 08:57:00.');
INSERT INTO languages (LanguageID,Description,Locale,FareBoxSeq,TouchedBy,TouchedAt) VALUES (3,'German','de_DE',6,'mmcgraw','2013-09-20 08:57:00.');
INSERT INTO languages (LanguageID,Description,Locale,FareBoxSeq,TouchedBy,TouchedAt) VALUES (4,'Tagalong (Filipino)',null,7,'mmcgraw','2013-09-20 08:57:00.');
INSERT INTO languages (LanguageID,Description,Locale,FareBoxSeq,TouchedBy,TouchedAt) VALUES (5,'Ukrainian',null,5,'mmcgraw','2013-09-20 08:57:00.');
INSERT INTO languages (LanguageID,Description,Locale,FareBoxSeq,TouchedBy,TouchedAt) VALUES (6,'Spanish','es_ES',2,'mmcgraw','2013-09-20 08:57:00.');
INSERT INTO languages (LanguageID,Description,Locale,FareBoxSeq,TouchedBy,TouchedAt) VALUES (7,'Cantonese',null,4,'mmcgraw','2013-09-20 08:57:00.');
INSERT INTO languages (LanguageID,Description,Locale,FareBoxSeq,TouchedBy,TouchedAt) VALUES (8,'Mandarin',null,8,'mmcgraw','2013-09-20 08:57:00.');
INSERT INTO languages (LanguageID,Description,Locale,FareBoxSeq,TouchedBy,TouchedAt) VALUES (9,'Polish',null,9,'mmcgraw','2013-09-20 08:57:00.');
INSERT INTO languages (LanguageID,Description,Locale,FareBoxSeq,TouchedBy,TouchedAt) VALUES (10,'Portugese',null,10,'mmcgraw','2013-09-20 08:57:00.');
INSERT INTO languages (LanguageID,Description,Locale,FareBoxSeq,TouchedBy,TouchedAt) VALUES (11,'Italian',null,11,'mmcgraw','2013-09-20 08:57:00.');
INSERT INTO languages (LanguageID,Description,Locale,FareBoxSeq,TouchedBy,TouchedAt) VALUES (12,'Punjabi',null,12,'mmcgraw','2013-09-20 08:57:00.');
INSERT INTO languages (LanguageID,Description,Locale,FareBoxSeq,TouchedBy,TouchedAt) VALUES (13,'Vietnamese',null,13,'mmcgraw','2013-09-20 08:57:00.');
INSERT INTO languages (LanguageID,Description,Locale,FareBoxSeq,TouchedBy,TouchedAt) VALUES (14,'Ojibway',null,14,'mmcgraw','2013-09-20 08:57:00.');
INSERT INTO languages (LanguageID,Description,Locale,FareBoxSeq,TouchedBy,TouchedAt) VALUES (15,'Hindi',null,15,'mmcgraw','2013-09-20 08:57:00.');
INSERT INTO languages (LanguageID,Description,Locale,FareBoxSeq,TouchedBy,TouchedAt) VALUES (16,'Russian',null,16,'mmcgraw','2013-09-20 08:57:00.');
INSERT INTO languages (LanguageID,Description,Locale,FareBoxSeq,TouchedBy,TouchedAt) VALUES (17,'Cree',null,17,'mmcgraw','2013-09-20 08:57:00.');
INSERT INTO languages (LanguageID,Description,Locale,FareBoxSeq,TouchedBy,TouchedAt) VALUES (18,'Dutch',null,18,'mmcgraw','2013-09-20 08:57:00.');
ALTER TABLE documents
ADD CONSTRAINT RefLanguages243
FOREIGN KEY (LanguageID)
REFERENCES languages(LanguageID)
;
ALTER TABLE people
ADD CONSTRAINT RefLanguages206
FOREIGN KEY (LanguageID)
REFERENCES languages(LanguageID)
;
-------------------------------------------------------------------------------------------------------
ALTER TABLE walletidentifiers
DROP FOREIGN KEY RefIdentifierTypes61
;
DELETE FROM IDENTIFIERTYPES;
INSERT INTO identifiertypes (IdentifierID,Description,TouchedBy,TouchedAt) VALUES (1,'Electronic ID','root@localhost','2014-03-27 16:17:51.');
INSERT INTO identifiertypes (IdentifierID,Description,TouchedBy,TouchedAt) VALUES (2,'Printed ID','root@localhost','2014-03-27 16:18:05.');
INSERT INTO identifiertypes (IdentifierID,Description,TouchedBy,TouchedAt) VALUES (3,'Track2 Magnetic Stripe','root@localhost','2014-03-27 16:18:34.');
INSERT INTO identifiertypes (IdentifierID,Description,TouchedBy,TouchedAt) VALUES (4,'Mobile Phone Number','root@localhost','2014-03-27 16:19:01.');
INSERT INTO identifiertypes (IdentifierID,Description,TouchedBy,TouchedAt) VALUES (5,'Bar Code','root@localhost','2014-03-27 16:19:36.');
INSERT INTO identifiertypes (IdentifierID,Description,TouchedBy,TouchedAt) VALUES (6,'QR Code','root@localhost','2014-03-27 16:19:42.');
ALTER TABLE walletidentifiers
ADD CONSTRAINT RefIdentifierTypes61
FOREIGN KEY (IdentifierID)
REFERENCES identifiertypes(IdentifierID)
;
-------------------------------------------------------------------------------------------------------
ALTER TABLE activitysummary
DROP FOREIGN KEY RefMedia74
;
ALTER TABLE offerings
DROP FOREIGN KEY RefMedia250
;
ALTER TABLE shippinglists
DROP FOREIGN KEY RefMedia91
;
ALTER TABLE wallets
DROP FOREIGN KEY RefMedia21
;
DELETE FROM MEDIA;
INSERT INTO media (MediaID,Description,AvailableSlots,TouchedBy,TouchedAt) VALUES (1,'Magnetic Card',1,'root@localhost','2014-03-27 16:21:10.');
INSERT INTO media (MediaID,Description,AvailableSlots,TouchedBy,TouchedAt) VALUES (2,'Paper',1,'root@localhost','2014-03-27 16:21:28.');
INSERT INTO media (MediaID,Description,AvailableSlots,TouchedBy,TouchedAt) VALUES (3,'DesFire Smart Card',1,'root@localhost','2014-03-27 16:21:45.');
INSERT INTO media (MediaID,Description,AvailableSlots,TouchedBy,TouchedAt) VALUES (4,'Limited Use Contactless Card',1,'root@localhost','2014-03-27 16:22:10.');
INSERT INTO media (MediaID,Description,AvailableSlots,TouchedBy,TouchedAt) VALUES (5,'Mobile Phone',1,'root@localhost','2014-03-27 16:23:07.');
INSERT INTO media (MediaID,Description,AvailableSlots,TouchedBy,TouchedAt) VALUES (6,'Adhesive Smart Media',1,'root@localhost','2014-03-27 16:31:10.');
INSERT INTO media (MediaID,Description,AvailableSlots,TouchedBy,TouchedAt) VALUES (7,'Third Party Issued',1,'root@localhost','2014-03-27 16:33:49.');
INSERT INTO media (MediaID,Description,AvailableSlots,TouchedBy,TouchedAt) VALUES (8,'Multi Function Smart Card',1,'root@localhost','2014-03-27 16:34:06.');
INSERT INTO media (MediaID,Description,AvailableSlots,TouchedBy,TouchedAt) VALUES (9,'Near Field Communication Device',1,'root@localhost','2014-03-27 16:34:30.');
ALTER TABLE activitysummary
ADD CONSTRAINT RefMedia74
FOREIGN KEY (MediaID)
REFERENCES media(MediaID)
;
ALTER TABLE offerings
ADD CONSTRAINT RefMedia250
FOREIGN KEY (MediaID)
REFERENCES media(MediaID)
;
ALTER TABLE shippinglists
ADD CONSTRAINT RefMedia91
FOREIGN KEY (MediaID)
REFERENCES media(MediaID)
;
ALTER TABLE wallets
ADD CONSTRAINT RefMedia21
FOREIGN KEY (MediaID)
REFERENCES media(MediaID)
;
-------------------------------------------------------------------------------------------------------
ALTER TABLE wallets
DROP FOREIGN KEY RefPurposes183
;
DELETE FROM PURPOSES;
INSERT INTO purposes (PurposeID,Description,TouchedBy,TouchedAt) VALUES (1,'Normal Use','root@localhost','2014-03-27 16:24:22.');
INSERT INTO purposes (PurposeID,Description,TouchedBy,TouchedAt) VALUES (2,'Driver ID','root@localhost','2014-03-27 16:24:33.');
INSERT INTO purposes (PurposeID,Description,TouchedBy,TouchedAt) VALUES (3,'Maintenance ID','root@localhost','2014-03-27 16:24:40.');
INSERT INTO purposes (PurposeID,Description,TouchedBy,TouchedAt) VALUES (4,'Test Purposes','root@localhost','2014-03-27 16:25:15.');
INSERT INTO purposes (PurposeID,Description,TouchedBy,TouchedAt) VALUES (5,'POS Clerk ID','root@localhost','2014-03-27 16:25:35.');
ALTER TABLE wallets
ADD CONSTRAINT RefPurposes183
FOREIGN KEY (PurposeID)
REFERENCES purposes(PurposeID)
;
-------------------------------------------------------------------------------------------------------
ALTER TABLE activitydetails
DROP FOREIGN KEY RefDiscountTypes127
;
ALTER TABLE activitysummary
DROP FOREIGN KEY RefDiscountTypes72
;
ALTER TABLE discounts
DROP FOREIGN KEY RefDiscountTypes12
;
ALTER TABLE orderitems
DROP FOREIGN KEY RefDiscountTypes128
;
ALTER TABLE pendingactivity
DROP FOREIGN KEY RefDiscountTypes129
;
ALTER TABLE processedactivity
DROP FOREIGN KEY RefDiscountTypes208
;
ALTER TABLE sales
DROP FOREIGN KEY RefDiscountTypes228
;
ALTER TABLE subscriptions
DROP FOREIGN KEY RefDiscountTypes146
;
ALTER TABLE ticketdiscounts
DROP FOREIGN KEY RefDiscountTypes62
;
ALTER TABLE walletcontents
DROP FOREIGN KEY RefDiscountTypes126
;
DELETE FROM DISCOUNTTYPES;
INSERT INTO discounttypes (DiscountTypeID,Description,Criteria,TermLimit,TouchedBy,TouchedAt) VALUES (1,'Senior',null,null,'root@localhost','2014-03-27 16:38:11.');
INSERT INTO discounttypes (DiscountTypeID,Description,Criteria,TermLimit,TouchedBy,TouchedAt) VALUES (2,'Youth',null,null,'root@localhost','2014-03-27 16:38:17.');
INSERT INTO discounttypes (DiscountTypeID,Description,Criteria,TermLimit,TouchedBy,TouchedAt) VALUES (3,'Employee',null,null,'root@localhost','2014-03-27 16:38:31.');
INSERT INTO discounttypes (DiscountTypeID,Description,Criteria,TermLimit,TouchedBy,TouchedAt) VALUES (4,'Employer Sponsored',null,null,'root@localhost','2014-03-27 16:38:42.');
INSERT INTO discounttypes (DiscountTypeID,Description,Criteria,TermLimit,TouchedBy,TouchedAt) VALUES (5,'Disabled',null,null,'root@localhost','2014-03-27 16:38:53.');
ALTER TABLE activitydetails
ADD CONSTRAINT RefDiscountTypes127
FOREIGN KEY (DiscountTypeID)
REFERENCES discounttypes(DiscountTypeID)
;
ALTER TABLE activitysummary
ADD CONSTRAINT RefDiscountTypes72
FOREIGN KEY (DiscountTypeID)
REFERENCES discounttypes(DiscountTypeID)
;
ALTER TABLE discounts
ADD CONSTRAINT RefDiscountTypes12
FOREIGN KEY (DiscountTypeID)
REFERENCES discounttypes(DiscountTypeID)
;
ALTER TABLE orderitems
ADD CONSTRAINT RefDiscountTypes128
FOREIGN KEY (DiscountTypeID)
REFERENCES discounttypes(DiscountTypeID)
;
ALTER TABLE pendingactivity
ADD CONSTRAINT RefDiscountTypes129
FOREIGN KEY (DiscountTypeID)
REFERENCES discounttypes(DiscountTypeID)
;
ALTER TABLE processedactivity
ADD CONSTRAINT RefDiscountTypes208
FOREIGN KEY (DiscountTypeID)
REFERENCES discounttypes(DiscountTypeID)
;
ALTER TABLE sales
ADD CONSTRAINT RefDiscountTypes228
FOREIGN KEY (DiscountTypeID)
REFERENCES discounttypes(DiscountTypeID)
;
ALTER TABLE subscriptions
ADD CONSTRAINT RefDiscountTypes146
FOREIGN KEY (DiscountTypeID)
REFERENCES discounttypes(DiscountTypeID)
;
ALTER TABLE ticketdiscounts
ADD CONSTRAINT RefDiscountTypes62
FOREIGN KEY (DiscountTypeID)
REFERENCES discounttypes(DiscountTypeID)
;
ALTER TABLE walletcontents
ADD CONSTRAINT RefDiscountTypes126
FOREIGN KEY (DiscountTypeID)
REFERENCES discounttypes(DiscountTypeID)
;
-------------------------------------------------------------------------------------------------------
ALTER TABLE activitysummary
DROP FOREIGN KEY RefTicketTypes123
;
ALTER TABLE sales
DROP FOREIGN KEY RefTicketTypes227
;
ALTER TABLE tickets
DROP FOREIGN KEY RefTicketTypes116
;
DELETE FROM TICKETTYPES;
INSERT INTO tickettypes (TicketTypeID,Description,TouchedBy,TouchedAt) VALUES (1,'Stored Fixed Value','root@localhost','2014-03-27 16:57:03.');
INSERT INTO tickettypes (TicketTypeID,Description,TouchedBy,TouchedAt) VALUES (2,'Stored Ride','root@localhost','2014-03-27 16:57:10.');
INSERT INTO tickettypes (TicketTypeID,Description,TouchedBy,TouchedAt) VALUES (3,'Period Pass','root@localhost','2014-03-27 16:57:37.');
INSERT INTO tickettypes (TicketTypeID,Description,TouchedBy,TouchedAt) VALUES (4,'Stepped Value','root@localhost','2014-03-27 16:59:25.');
INSERT INTO tickettypes (TicketTypeID,Description,TouchedBy,TouchedAt) VALUES (5,'Farest Fare','root@localhost','2014-03-27 17:07:55.');
ALTER TABLE activitysummary
ADD CONSTRAINT RefTicketTypes123
FOREIGN KEY (TicketTypeID)
REFERENCES tickettypes(TicketTypeID)
;
ALTER TABLE sales
ADD CONSTRAINT RefTicketTypes227
FOREIGN KEY (TicketTypeID)
REFERENCES tickettypes(TicketTypeID)
;
ALTER TABLE tickets
ADD CONSTRAINT RefTicketTypes116
FOREIGN KEY (TicketTypeID)
REFERENCES tickettypes(TicketTypeID)
;
-------------------------------------------------------------------------------------------------------
ALTER TABLE offerings
DROP FOREIGN KEY RefOfferings245
;
ALTER TABLE orderitems
DROP FOREIGN KEY RefOfferings81
;
ALTER TABLE sales
DROP FOREIGN KEY RefOfferings119
;
DELETE FROM OFFERINGS;
INSERT INTO offerings (OfferingID,Description,UnitPrice,UnitCost,IsSubscribable,IsBulk,TaxRateID,QuantityMax,TouchedBy,TouchedAt) VALUES (1,'New Card',3.00,0.00,'F','T',1,500,'root@localhost','2014-04-04 11:12:17.');
INSERT INTO offerings (OfferingID,Description,UnitPrice,UnitCost,IsSubscribable,IsBulk,TaxRateID,QuantityMax,TouchedBy,TouchedAt) VALUES (2,'New Ticket',0.00,0.00,'F','T',1,500,'root@localhost','2014-04-04 11:12:50.');
INSERT INTO offerings (OfferingID,Description,UnitPrice,UnitCost,IsSubscribable,IsBulk,TaxRateID,QuantityMax,TouchedBy,TouchedAt) VALUES (3,'Refill Ticket',0.00,0.00,'T','F',1,500,'root@localhost','2014-04-04 11:13:16.');
INSERT INTO offerings (OfferingID,Description,UnitPrice,UnitCost,IsSubscribable,IsBulk,TaxRateID,QuantityMax,TouchedBy,TouchedAt) VALUES (4,'Card Holder',3.59,2.99,'F','F',2,500,'root@localhost','2014-04-04 11:14:26.');
INSERT INTO offerings (OfferingID,Description,UnitPrice,UnitCost,IsSubscribable,IsBulk,TaxRateID,QuantityMax,TouchedBy,TouchedAt) VALUES (5,'Transit T-Shirt',19.95,12.38,'F','F',2,10,'root@localhost','2014-04-04 11:15:26.');
INSERT INTO offerings (OfferingID,Description,UnitPrice,UnitCost,IsSubscribable,IsBulk,TaxRateID,QuantityMax,TouchedBy,TouchedAt) VALUES (6,'Transit Mug',12.95,8.38,'F','F',2,10,'root@localhost','2014-04-04 11:15:55.');
ALTER TABLE offerings
ADD CONSTRAINT RefOfferings245
FOREIGN KEY (ParentOfferingID)
REFERENCES offerings(OfferingID)
;
ALTER TABLE orderitems
ADD CONSTRAINT RefOfferings81
FOREIGN KEY (OfferingID)
REFERENCES offerings(OfferingID)
;
ALTER TABLE sales
ADD CONSTRAINT RefOfferings119
FOREIGN KEY (OfferingID)
REFERENCES offerings(OfferingID)
;
-------------------------------------------------------------------------------------------------------
ALTER TABLE offerings
DROP FOREIGN KEY RefTaxRates165
;
DELETE FROM TAXRATES;
INSERT INTO taxrates (TaxRateID,Description,Rate1,Rate2,Rate3,Rate4,Rate5,TouchedBy,TouchedAt) VALUES (1,'Not Taxable',0.00,0.00,0.00,0.00,0.00,'root@localhost','2014-04-04 11:07:05.');
INSERT INTO taxrates (TaxRateID,Description,Rate1,Rate2,Rate3,Rate4,Rate5,TouchedBy,TouchedAt) VALUES (2,'Taxable',0.00,0.00,0.00,0.00,0.00,'root@localhost','2014-04-04 11:07:19.');
ALTER TABLE offerings
ADD CONSTRAINT RefTaxRates165
FOREIGN KEY (TaxRateID)
REFERENCES taxrates(TaxRateID)
;
-------------------------------------------------------------------------------------------------------
ALTER TABLE activitysummary
DROP FOREIGN KEY RefServiceLevels115
;
ALTER TABLE tickets
DROP FOREIGN KEY RefServiceLevels103
;
DELETE FROM SERVICELEVELS;
INSERT INTO servicelevels (ServiceLevelID,Description,TouchedBy,TouchedAt) VALUES (1,'Local','root@localhost','2014-03-27 16:48:21.');
INSERT INTO servicelevels (ServiceLevelID,Description,TouchedBy,TouchedAt) VALUES (2,'BRT/Bus Plus','root@localhost','2014-03-27 16:49:07.');
INSERT INTO servicelevels (ServiceLevelID,Description,TouchedBy,TouchedAt) VALUES (3,'Regional','root@localhost','2014-03-27 16:49:23.');
INSERT INTO servicelevels (ServiceLevelID,Description,TouchedBy,TouchedAt) VALUES (4,'STAR Paratransit Service','root@localhost','2014-03-31 16:47:04.');
ALTER TABLE activitysummary
ADD CONSTRAINT RefServiceLevels115
FOREIGN KEY (ServiceLevelID)
REFERENCES servicelevels(ServiceLevelID)
;
ALTER TABLE tickets
ADD CONSTRAINT RefServiceLevels103
FOREIGN KEY (ServiceLevelID)
REFERENCES servicelevels(ServiceLevelID)
;
-------------------------------------------------------------------------------------------------------
ALTER TABLE tickets
DROP FOREIGN KEY RefValidDays90
;
DELETE FROM VALIDDAYS;
INSERT INTO validdays (ValidDayID,Description,IsDate,ValidDay,TouchedBy,TouchedAt) VALUES (1,'Week Days Only','F','2,3,4,5,6','root@localhost','2014-03-31 12:15:40.');
ALTER TABLE tickets
ADD CONSTRAINT RefValidDays90
FOREIGN KEY (ValidDayID)
REFERENCES validdays(ValidDayID)
;
-------------------------------------------------------------------------------------------------------
ALTER TABLE tickets
DROP FOREIGN KEY RefZones191
;
DELETE FROM ZONES;
INSERT INTO zones (ZoneID,Description,TouchedBy,TouchedAt) VALUES (1,'NX Zone 1','root@localhost','2014-03-28 11:29:43.');
INSERT INTO zones (ZoneID,Description,TouchedBy,TouchedAt) VALUES (2,'NX Zone 2','root@localhost','2014-03-28 11:29:52.');
INSERT INTO zones (ZoneID,Description,TouchedBy,TouchedAt) VALUES (3,'NX Zone 3','root@localhost','2014-03-28 11:30:00.');
ALTER TABLE tickets
ADD CONSTRAINT RefZones191
FOREIGN KEY (ZoneID)
REFERENCES zones(ZoneID)
;
-------------------------------------------------------------------------------------------------------
ALTER TABLE periods
DROP FOREIGN KEY RefExpirationTypes248
;
DELETE FROM EXPIRATIONTYPES;
INSERT INTO expirationtypes (ExpirationTypeID,Description,TouchedBy,TouchedAt) VALUES (1,'Midnight','root@localhost','2014-04-22 16:10:51.');
INSERT INTO expirationtypes (ExpirationTypeID,Description,TouchedBy,TouchedAt) VALUES (2,'End of Transit Day','root@localhost','2014-04-22 16:10:57.');
INSERT INTO expirationtypes (ExpirationTypeID,Description,TouchedBy,TouchedAt) VALUES (3,'24 Hours','root@localhost','2014-04-22 16:11:01.');
INSERT INTO expirationtypes (ExpirationTypeID,Description,TouchedBy,TouchedAt) VALUES (4,'Less than 24 Hours','root@localhost','2014-04-22 16:11:06.');
-------------------------------------------------------------------------------------------------------
DELETE FROM PERIODS;
INSERT INTO periods (PeriodID,Description,IsFloating,PeriodsPerYear,RenewalMonths,RenewalDay,ExpirationTypeID,TouchedBy,TouchedAt) VALUES (1,'Floating Period','T',null,null,null,2,'root@localhost','2014-04-22 16:11:10.');
INSERT INTO periods (PeriodID,Description,IsFloating,PeriodsPerYear,RenewalMonths,RenewalDay,ExpirationTypeID,TouchedBy,TouchedAt) VALUES (2,'Calendar Monthly Period','F',12,'1,2,3,4,5,6,7,8,9,10,11,12',20,1,'root@localhost','2014-03-28 12:10:43.');
INSERT INTO periods (PeriodID,Description,IsFloating,PeriodsPerYear,RenewalMonths,RenewalDay,ExpirationTypeID,TouchedBy,TouchedAt) VALUES (3,'Summer July - August','F',1,null,null,1,'root@localhost','2014-04-01 16:35:38.');

ALTER TABLE periods
ADD CONSTRAINT RefExpirationTypes248
FOREIGN KEY (ExpirationTypeID)
REFERENCES expirationtypes(ExpirationTypeID)
;