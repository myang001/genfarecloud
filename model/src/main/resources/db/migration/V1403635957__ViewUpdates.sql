SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

ALTER TABLE activitydetails
ADD COLUMN PaymentTypeID TINYINT(3) UNSIGNED NULL DEFAULT NULL AFTER AmountReturned,
ADD INDEX RefPaymentTypes313 (PaymentTypeID ASC);

ALTER TABLE OrderStatuses
ALTER COLUMN IsPayment SET DEFAULT 'F';

ALTER TABLE activitydetails
ADD CONSTRAINT RefPaymentTypes313
FOREIGN KEY (PaymentTypeID)
REFERENCES paymenttypes (PaymentTypeID);

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

CREATE OR REPLACE VIEW ActivityMonthly AS
SELECT  Da.Year
, Da.Month
, Ac.ServiceLevelID
, Ac.TicketTypeID
, Ac.DiscountTypeID
, Ac.WalletTypeID
, Ac.OrganizationTypeID
, Ac.LocationTypeID
, Ac.ActivityTypeID
, sum(Ac.CountAcitivity) as CountAcitivity
, sum(Ac.CountUse) as CountUse
, sum(Ac.CountMin) as CountMin
, sum(Ac.CountMax) as CountMax
, min(Ac.AmountMin) as AmountMin
, max(Ac.AmountMax) as AmountMax
, sum(Ac.AmountChargedTotal) as AmountChargedTotal
FROM ActivitySummary Ac
INNER JOIN  Dates Da ON Ac.DateID = Da.DateID
group by Da.Year
, Da.Month
, Ac.ServiceLevelID
, Ac.TicketTypeID
, Ac.DiscountTypeID
, Ac.WalletTypeID
, Ac.OrganizationTypeID
, Ac.LocationTypeID
, Ac.ActivityTypeID
;

CREATE OR REPLACE VIEW ActivityQuarterly AS
SELECT  Da.Year
, Da.Quarter
, Ac.ServiceLevelID
, Ac.TicketTypeID
, Ac.DiscountTypeID
, Ac.WalletTypeID
, Ac.OrganizationTypeID
, Ac.LocationTypeID
, Ac.ActivityTypeID
, sum(Ac.CountAcitivity) as CountAcitivity
, sum(Ac.CountUse) as CountUse
, sum(Ac.CountMin) as CountMin
, sum(Ac.CountMax) as CountMax
, min(Ac.AmountMin) as AmountMin
, max(Ac.AmountMax) as AmountMax
, sum(Ac.AmountChargedTotal) as AmountChargedTotal
FROM ActivitySummary Ac
INNER JOIN  Dates Da ON Ac.DateID = Da.DateID
group by Da.Year
, Da.Quarter
, Ac.ServiceLevelID
, Ac.TicketTypeID
, Ac.DiscountTypeID
, Ac.WalletTypeID
, Ac.OrganizationTypeID
, Ac.LocationTypeID
, Ac.ActivityTypeID
;

CREATE OR REPLACE VIEW ActivityYearly AS
SELECT  Da.Year
, Ac.ServiceLevelID
, Ac.TicketTypeID
, Ac.DiscountTypeID
, Ac.WalletTypeID
, Ac.OrganizationTypeID
, Ac.LocationTypeID
, Ac.ActivityTypeID
, sum(Ac.CountAcitivity) as CountAcitivity
, sum(Ac.CountUse) as CountUse
, sum(Ac.CountMin) as CountMin
, sum(Ac.CountMax) as CountMax
, min(Ac.AmountMin) as AmountMin
, max(Ac.AmountMax) as AmountMax
, sum(Ac.AmountChargedTotal) as AmountChargedTotal
FROM ActivitySummary Ac
INNER JOIN  Dates Da ON Ac.DateID = Da.DateID
group by Da.Year
, Ac.ServiceLevelID
, Ac.TicketTypeID
, Ac.DiscountTypeID
, Ac.WalletTypeID
, Ac.OrganizationTypeID
, Ac.LocationTypeID
, Ac.ActivityTypeID
;

CREATE OR REPLACE VIEW WalletHistory AS
SELECT
a.EquipmentID
,a.RouteID
,ad.DateActivity
,wi.Identifier
,w.WalletID
,w.NickName
,w.DateExpires
,w.WalletStatusID
,ad.Slot
,ad.TicketID
,t.TicketTypeID
,ad.DiscountTypeID
,ad.ActivityTypeID
,ad.AmountCharge
,ad.AmountRemaining
,ad.AmountTendered
,ad.AmountReturned
,ad.PaymentTypeID
,w.WalletTypeID
,ad.PendingID
,ad.PendingStatus
,p.ValueActivity
,p.DateAdded
,p.DateEffective
from activitydetails ad
inner join activities a on ad.ActivityID = a.ActivityID
inner join tickets t on ad.TicketID = t.TicketID
inner join walletidentifiers wi on ad.CardIdentifier = wi.Identifier and IdentifierID = 2
inner join wallets w on wi.walletid = w.WalletID
inner join walletcontents wc on w.WalletID = wc.WalletID and ad.slot = wc.slot and slotstatus = 2
left outer join pendingactivity p on ad.PendingID = p.PendingID
;

