Database Migrations
===================

All database migrations should be present here. Each file should have the following format:

`V1__description.sql`

Where the first part is `V` followed by a sequential version number, then two underscores and a description.