
ALTER TABLE orderitems
  DROP FOREIGN KEY RefOrderItems246;

ALTER TABLE orderitems
  ADD CONSTRAINT RefOrderItems246 FOREIGN KEY (OrderID, ParentItemNumber)
  REFERENCES orderitems (OrderID,OrderItemNumber)
  ON UPDATE NO ACTION
  ON DELETE CASCADE;
