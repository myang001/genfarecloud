ALTER TABLE people ADD Alert char(1) DEFAULT 'F' NOT NULL;
ALTER TABLE peoplelinks ADD IsPrimaryContactMethod char(1) DEFAULT 'T' NOT NULL;
INSERT INTO `peoplelinks`
(`PersonID`,
`LinkTypeID`,
`LinkValue`,
`IsPreferred`,
`TouchedBy`,
`TouchedAt`,
`IsPrimaryContactMethod`)
VALUES
(1,
1,
'+19176207497',
'T',
'root@localhost',
'2014-03-21 15:07:58',
'F');