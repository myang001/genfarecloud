
CREATE TABLE IF NOT EXISTS modules (
  ModuleID TINYINT(3) UNSIGNED NOT NULL AUTO_INCREMENT,
  Name VARCHAR(50) NULL DEFAULT NULL,
  Description VARCHAR(256) NOT NULL,
  TouchedBy VARCHAR(50) NOT NULL,
  TouchedAt DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  CreatedBy VARCHAR(50) NOT NULL,
  CreatedAt DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (ModuleID))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;


CREATE TABLE IF NOT EXISTS components (
  ComponentID SMALLINT(6) NOT NULL AUTO_INCREMENT,
  ModuleID TINYINT(3) UNSIGNED NULL DEFAULT NULL,
  Name VARCHAR(50) NULL DEFAULT NULL,
  Description VARCHAR(256) NOT NULL,
  TouchedBy VARCHAR(50) NOT NULL,
  TouchedAt DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  CreatedBy VARCHAR(50) NOT NULL,
  CreatedAt DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (ComponentID),
  INDEX RefModules393 (ModuleID ASC),
  CONSTRAINT RefModules393
    FOREIGN KEY (ModuleID)
    REFERENCES modules (ModuleID))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

INSERT INTO Modules (ModuleId,Name,Description,TouchedBy,CreatedBy) VALUES(1,'Default','Default Module','admin','admin');
INSERT INTO Components (ComponentID,ModuleId,Name,Description,TouchedBy,CreatedBy) VALUES(1,1,'Default','Default Component','admin','admin');


ALTER TABLE Privileges 
	ADD COLUMN ComponentID SMALLINT(6) NOT NULL AFTER PrivilegeID,
	CHANGE `Type` Permission VARCHAR(50) NOT NULL AFTER ComponentID,
	CHANGE Description Name VARCHAR(50) NOT NULL AFTER Permission,
	ADD COLUMN Description VARCHAR(256) NOT NULL AFTER Name, 
	ADD CONSTRAINT UPermission UNIQUE (Permission)
;

UPDATE Privileges SET ComponentID = 1;

ALTER TABLE Privileges
	ADD CONSTRAINT RefComponents417
    	FOREIGN KEY (ComponentID)
    	REFERENCES components (ComponentID)
;

ALTER TABLE GroupsOfPeople 
	DROP FOREIGN KEY RefGroups189
;

ALTER TABLE GroupPrivileges
	DROP FOREIGN KEY RefGroups194
;

ALTER TABLE Groups
	CHANGE `Type` Role VARCHAR(50) NOT NULL AFTER OrganizationID,
	CHANGE Description Name VARCHAR(50) NOT NULL AFTER Role,
	ADD COLUMN Description VARCHAR(256) NOT NULL AFTER Name, 
	ADD CONSTRAINT URole UNIQUE (Role)
;

ALTER TABLE GroupsOfPeople ADD CONSTRAINT RefGroups189 
    FOREIGN KEY (GroupID)
    REFERENCES Groups(GroupID)
;

ALTER TABLE GroupPrivileges ADD CONSTRAINT RefGroups194 
    FOREIGN KEY (GroupID)
    REFERENCES Groups(GroupID)
;


DELETE FROM Properties;

ALTER TABLE properties 
CHANGE COLUMN Value Value TEXT NOT NULL ,
ADD COLUMN Category VARCHAR(255) NOT NULL DEFAULT 'default' FIRST,
DROP PRIMARY KEY,
ADD PRIMARY KEY (Category, LanguageID),
ADD INDEX RefLanguages315 (LanguageID ASC);

ALTER TABLE properties 
ADD CONSTRAINT RefLanguages315
  FOREIGN KEY (LanguageID)
  REFERENCES languages (LanguageID);
  
  
UPDATE Groups SET Name = 'User' WHERE GroupID = 1;
UPDATE Privileges SET Permission = 'PERM_DEFAULT' WHERE PrivilegeID = 1;