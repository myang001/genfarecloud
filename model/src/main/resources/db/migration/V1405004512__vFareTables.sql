CREATE OR REPLACE VIEW FareTables AS
SELECT t.TicketID, t.Description, t.Price, t.Value, t.ValueUse AS ValueCharged, vd.Description AS ValidDaysDescr, sl.Description AS ServiceLevelDescr, t.DateEffective, t.DateExpires, tp.TimeStart, tp.TimeEnd
FROM tickets t
INNER JOIN tickettypes tt ON  tt.tickettypeid = t.tickettypeid
LEFT OUTER JOIN validdays vd ON vd.ValidDayID = t.ValidDayID
LEFT OUTER JOIN  servicelevels sl ON sl.ServiceLevelID = t.ServiceLevelID
LEFT OUTER JOIN tickettimes tts ON tts.TicketID = t.TicketID
LEFT OUTER JOIN timeperiods tp ON tp.TimePeriodID = tts.TimePeriodID;