ALTER TABLE Groups
  ADD COLUMN Status tinyint(3) unsigned NOT NULL DEFAULT 1 AFTER Description
;
