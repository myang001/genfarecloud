INSERT INTO wallettypes (wallettypeid,Description,AvailableSlots,TouchedBy,TouchedAt, CreatedBy, CreatedAt) VALUES (1,'Magnetic Card',1,'root@localhost','2014-03-27 16:21:10.','root@localhost','2014-03-27 16:21:10.');
INSERT INTO wallettypes (wallettypeid,Description,AvailableSlots,TouchedBy,TouchedAt, CreatedBy, CreatedAt) VALUES (2,'Paper',1,'root@localhost','2014-03-27 16:21:28.','root@localhost','2014-03-27 16:21:10.');
INSERT INTO wallettypes (wallettypeid,Description,AvailableSlots,TouchedBy,TouchedAt, CreatedBy, CreatedAt) VALUES (3,'DesFire Smart Card',1,'root@localhost','2014-03-27 16:21:45.','root@localhost','2014-03-27 16:21:10.');
INSERT INTO wallettypes (wallettypeid,Description,AvailableSlots,TouchedBy,TouchedAt, CreatedBy, CreatedAt) VALUES (4,'Limited Use Contactless Card',1,'root@localhost','2014-03-27 16:22:10.','root@localhost','2014-03-27 16:21:10.');
INSERT INTO wallettypes (wallettypeid,Description,AvailableSlots,TouchedBy,TouchedAt, CreatedBy, CreatedAt) VALUES (5,'Mobile Phone',1,'root@localhost','2014-03-27 16:23:07.','root@localhost','2014-03-27 16:21:10.');
INSERT INTO wallettypes (wallettypeid,Description,AvailableSlots,TouchedBy,TouchedAt, CreatedBy, CreatedAt) VALUES (6,'Adhesive Smart Media',1,'root@localhost','2014-03-27 16:31:10.','root@localhost','2014-03-27 16:21:10.');
INSERT INTO wallettypes (wallettypeid,Description,AvailableSlots,TouchedBy,TouchedAt, CreatedBy, CreatedAt) VALUES (7,'Third Party Issued',1,'root@localhost','2014-03-27 16:33:49.','root@localhost','2014-03-27 16:21:10.');
INSERT INTO wallettypes (wallettypeid,Description,AvailableSlots,TouchedBy,TouchedAt, CreatedBy, CreatedAt) VALUES (8,'Multi Function Smart Card',1,'root@localhost','2014-03-27 16:34:06.','root@localhost','2014-03-27 16:21:10.');
INSERT INTO wallettypes (wallettypeid,Description,AvailableSlots,TouchedBy,TouchedAt, CreatedBy, CreatedAt) VALUES (9,'Near Field Communication Device',1,'root@localhost','2014-03-27 16:34:30.','root@localhost','2014-03-27 16:21:10.');

UPDATE Offerings
SET
wallettypeid = 1
WHERE (description='Fare Card');

UPDATE Offerings
SET
wallettypeid = 4
WHERE (description='LUCC');