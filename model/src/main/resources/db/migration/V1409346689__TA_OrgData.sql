---------------------------Create Groups for the Org-----------------------------------------------------------------------
INSERT INTO groups (OrganizationID,Role,Name,Description,Status,TouchedBy,CreatedBy) VALUES
	('2','ROLE_TA_DEFAULT','TA Default','Transit Agency Default group includes all users within Transit Agency','1','root@localhost','root@localhost');
INSERT INTO groups (OrganizationID,Role,Name,Description,Status,TouchedBy,CreatedBy) VALUES
	('2','ROLE_TA_ADMIN','TA Admin','Transit Agency Administrator','1','root@localhost','root@localhost');
INSERT INTO groups (OrganizationID,Role,Name,Description,Status,TouchedBy,CreatedBy) VALUES
	('2','ROLE_TA_STAFF','TA Staff','Staff Member in Transit Agency','1','root@localhost','root@localhost');
INSERT INTO groups (OrganizationID,Role,Name,Description,Status,TouchedBy,CreatedBy) VALUES
	('2','ROLE_TA_USER','TA User','Network user in Transit Agency','1','root@localhost','root@localhost');
INSERT INTO groups (OrganizationID,Role,Name,Description,Status,TouchedBy,CreatedBy) VALUES
	('2','ROLE_TA_SUPER_USER','TA Super User','Network user in Transit Agency','1','root@localhost','root@localhost');
---------------------------------------------------------------------------------------------------------------------------


---------------------------Assign Group Priveleges-------------------------------------------------------------------------
INSERT INTO groupprivileges (GroupID,PrivilegeID,TouchedBy,CreatedBy) VALUES
        ((select groupid from groups where name='TA Default'),1,'root@localhost','root@localhost');
INSERT INTO groupprivileges (GroupID,PrivilegeID,TouchedBy,CreatedBy) VALUES
        ((select groupid from groups where name='TA Admin'),1,'root@localhost','root@localhost');
INSERT INTO groupprivileges (GroupID,PrivilegeID,TouchedBy,CreatedBy) VALUES
        ((select groupid from groups where name='TA Admin'),2,'root@localhost','root@localhost');
INSERT INTO groupprivileges (GroupID,PrivilegeID,TouchedBy,CreatedBy) VALUES
        ((select groupid from groups where name='TA Admin'),3,'root@localhost','root@localhost');
INSERT INTO groupprivileges (GroupID,PrivilegeID,TouchedBy,CreatedBy) VALUES
        ((select groupid from groups where name='TA Admin'),4,'root@localhost','root@localhost');
INSERT INTO groupprivileges (GroupID,PrivilegeID,TouchedBy,CreatedBy) VALUES
        ((select groupid from groups where name='TA Admin'),5,'root@localhost','root@localhost');
INSERT INTO groupprivileges (GroupID,PrivilegeID,TouchedBy,CreatedBy) VALUES
        ((select groupid from groups where name='TA Admin'),6,'root@localhost','root@localhost');
INSERT INTO groupprivileges (GroupID,PrivilegeID,TouchedBy,CreatedBy) VALUES
        ((select groupid from groups where name='TA Admin'),7,'root@localhost','root@localhost');
INSERT INTO groupprivileges (GroupID,PrivilegeID,TouchedBy,CreatedBy) VALUES
        ((select groupid from groups where name='TA Admin'),8,'root@localhost','root@localhost');
INSERT INTO groupprivileges (GroupID,PrivilegeID,TouchedBy,CreatedBy) VALUES
        ((select groupid from groups where name='TA Admin'),9,'root@localhost','root@localhost');
INSERT INTO groupprivileges (GroupID,PrivilegeID,TouchedBy,CreatedBy) VALUES
        ((select groupid from groups where name='TA Admin'),10,'root@localhost','root@localhost');
INSERT INTO groupprivileges (GroupID,PrivilegeID,TouchedBy,CreatedBy) VALUES
        ((select groupid from groups where name='TA Admin'),11,'root@localhost','root@localhost');
INSERT INTO groupprivileges (GroupID,PrivilegeID,TouchedBy,CreatedBy) VALUES
        ((select groupid from groups where name='TA Admin'),12,'root@localhost','root@localhost');
INSERT INTO groupprivileges (GroupID,PrivilegeID,TouchedBy,CreatedBy) VALUES
        ((select groupid from groups where name='TA Staff'),1,'root@localhost','root@localhost');
INSERT INTO groupprivileges (GroupID,PrivilegeID,TouchedBy,CreatedBy) VALUES
        ((select groupid from groups where name='TA Staff'),2,'root@localhost','root@localhost');
INSERT INTO groupprivileges (GroupID,PrivilegeID,TouchedBy,CreatedBy) VALUES
        ((select groupid from groups where name='TA Staff'),3,'root@localhost','root@localhost');
INSERT INTO groupprivileges (GroupID,PrivilegeID,TouchedBy,CreatedBy) VALUES
        ((select groupid from groups where name='TA Staff'),4,'root@localhost','root@localhost');
INSERT INTO groupprivileges (GroupID,PrivilegeID,TouchedBy,CreatedBy) VALUES
        ((select groupid from groups where name='TA Staff'),5,'root@localhost','root@localhost');
INSERT INTO groupprivileges (GroupID,PrivilegeID,TouchedBy,CreatedBy) VALUES
        ((select groupid from groups where name='TA User'),1,'root@localhost','root@localhost');
INSERT INTO groupprivileges (GroupID,PrivilegeID,TouchedBy,CreatedBy) VALUES
        ((select groupid from groups where name='TA User'),2,'root@localhost','root@localhost');
INSERT INTO groupprivileges (GroupID,PrivilegeID,TouchedBy,CreatedBy) VALUES
        ((select groupid from groups where name='TA Super User'),1,'root@localhost','root@localhost');
INSERT INTO groupprivileges (GroupID,PrivilegeID,TouchedBy,CreatedBy) VALUES
        ((select groupid from groups where name='TA Super User'),2,'root@localhost','root@localhost');
INSERT INTO groupprivileges (GroupID,PrivilegeID,TouchedBy,CreatedBy) VALUES
        ((select groupid from groups where name='TA Super User'),3,'root@localhost','root@localhost');
INSERT INTO groupprivileges (GroupID,PrivilegeID,TouchedBy,CreatedBy) VALUES
        ((select groupid from groups where name='TA Super User'),4,'root@localhost','root@localhost');
INSERT INTO groupprivileges (GroupID,PrivilegeID,TouchedBy,CreatedBy) VALUES
        ((select groupid from groups where name='TA Super User'),5,'root@localhost','root@localhost');
INSERT INTO groupprivileges (GroupID,PrivilegeID,TouchedBy,CreatedBy) VALUES
        ((select groupid from groups where name='TA Super User'),6,'root@localhost','root@localhost');
INSERT INTO groupprivileges (GroupID,PrivilegeID,TouchedBy,CreatedBy) VALUES
        ((select groupid from groups where name='TA Super User'),7,'root@localhost','root@localhost');
INSERT INTO groupprivileges (GroupID,PrivilegeID,TouchedBy,CreatedBy) VALUES
        ((select groupid from groups where name='TA Super User'),8,'root@localhost','root@localhost');
INSERT INTO groupprivileges (GroupID,PrivilegeID,TouchedBy,CreatedBy) VALUES
        ((select groupid from groups where name='TA Super User'),9,'root@localhost','root@localhost');
---------------------------------------------------------------------------------------------------------------------------


---------------------------Create Admin to add to groups-------------------------------------------------------------------
INSERT INTO people (PersonID,Prefix,NameLast,NameMiddle,NameFirst,Suffix,DateOfBirth,Gender,Status,LanguageID,TouchedBy,CreatedBy) VALUES
        ((select max(p.personid) + 1 from people p),'','Admin','','Transit','','1990-01-01','M','1','1','root@localhost','root@localhost');
INSERT INTO peopleaddresses(PersonID,AddressTypeID,AddressLine1,AddressLine2,City,State,PostalCode,Country,TouchedBy,CreatedBy) VALUES
        ((select max(p.personid) from people p),1,'800 Arthur Ave.','','Elk Grove Village','IL','60007','US','root@localhost','root@localhost');
INSERT INTO peoplelinks (PersonID,LinkTypeID,LinkValue,IsPreferred,TouchedBy,CreatedBy) VALUES
        ((select max(p.personid) from people p),6,'admin@ta.com','F','root@localhost','root@localhost');
INSERT INTO peoplelinks (PersonID,LinkTypeID,LinkValue,IsPreferred,TouchedBy,CreatedBy) VALUES
        ((select max(p.personid) from people p),2,'4096203827','T','root@localhost','root@localhost');
INSERT INTO logins (LoginID,PersonID,UserLogin,TouchedBy,CreatedBy) VALUES 
        ((select max(l.loginid)+1 from logins l),(select max(p.personid) from people p),'admin@ta.com','root@localhost','root@localhost');
INSERT INTO authentication (PersonID,AuthenticationTypeID,Response,TouchedBy,CreatedBy) VALUES 
        ((select max(p.personid) from people p),1,'$2a$10$zlA213tFDRqOH0ffTtmnoe6Lp2qSdOUZxVzXbUSpygDjrvU4pxDSK','root@localhost','root@localhost');
---------------------------------------------------------------------------------------------------------------------------


---------------------------Add Admin To Groups-----------------------------------------------------------------------------
INSERT INTO groupsofpeople (GroupID,PersonID,TouchedBy,CreatedBy) VALUES
        ((select groupid from groups where name='TA Default'),(select max(p.personid) from people p),'root@localhost','root@localhost');
INSERT INTO groupsofpeople (GroupID,PersonID,TouchedBy,CreatedBy) VALUES
        ((select groupid from groups where name='TA Admin'),(select max(p.personid) from people p),'root@localhost','root@localhost');
INSERT INTO groupsofpeople (GroupID,PersonID,TouchedBy,CreatedBy) VALUES
        ((select groupid from groups where name='TA Staff'),(select max(p.personid) from people p),'root@localhost','root@localhost');
INSERT INTO groupsofpeople (GroupID,PersonID,TouchedBy,CreatedBy) VALUES
        ((select groupid from groups where name='TA User'),(select max(p.personid) from people p),'root@localhost','root@localhost');
INSERT INTO groupsofpeople (GroupID,PersonID,TouchedBy,CreatedBy) VALUES
        ((select groupid from groups where name='TA Super User'),(select max(p.personid) from people p),'root@localhost','root@localhost');
UPDATE organizations SET maincontact=(select max(p.personid) from people p) WHERE organizationid='2';
---------------------------------------------------------------------------------------------------------------------------


---------------------------Create Normal User and add to groups------------------------------------------------------------
INSERT INTO people (PersonID,Prefix,NameLast,NameMiddle,NameFirst,Suffix,DateOfBirth,Gender,Status,LanguageID,TouchedBy,CreatedBy) VALUES
        ((select max(p.personid) + 1 from people p),'','User','','Transit','','1990-01-01','M','1','1','root@localhost','root@localhost');
INSERT INTO peopleaddresses(PersonID,AddressTypeID,AddressLine1,AddressLine2,City,State,PostalCode,Country,TouchedBy,CreatedBy) VALUES
        ((select max(p.personid) from people p),1,'800 Arthur Ave.','','Elk Grove Village','IL','60007','US','root@localhost','root@localhost');
INSERT INTO peoplelinks (PersonID,LinkTypeID,LinkValue,IsPreferred,TouchedBy,CreatedBy) VALUES
        ((select max(p.personid) from people p),6,'user01@ta.com','F','root@localhost','root@localhost');
INSERT INTO peoplelinks (PersonID,LinkTypeID,LinkValue,IsPreferred,TouchedBy,CreatedBy) VALUES
        ((select max(p.personid) from people p),2,'5096203827','T','root@localhost','root@localhost');
INSERT INTO logins (LoginID,PersonID,UserLogin,TouchedBy,CreatedBy) VALUES 
        ((select max(l.loginid)+1 from logins l),(select max(p.personid) from people p),'user01@ta.com','root@localhost','root@localhost');
INSERT INTO authentication (PersonID,AuthenticationTypeID,Response,TouchedBy,CreatedBy) VALUES 
        ((select max(p.personid) from people p),1,'$2a$10$zlA213tFDRqOH0ffTtmnoe6Lp2qSdOUZxVzXbUSpygDjrvU4pxDSK','root@localhost','root@localhost');
INSERT INTO groupsofpeople (GroupID,PersonID,TouchedBy,CreatedBy) VALUES
        ((select groupid from groups where name='TA Default'),(select max(p.personid) from people p),'root@localhost','root@localhost');
INSERT INTO groupsofpeople (GroupID,PersonID,TouchedBy,CreatedBy) VALUES
        ((select groupid from groups where name='TA User'),(select max(p.personid) from people p),'root@localhost','root@localhost');
---------------------------------------------------------------------------------------------------------------------------


---------------------------Create Staff Member and add to groups------------------------------------------------------------
INSERT INTO people (PersonID,Prefix,NameLast,NameMiddle,NameFirst,Suffix,DateOfBirth,Gender,Status,LanguageID,TouchedBy,CreatedBy) VALUES
        ((select max(p.personid) + 1 from people p),'','Staff','','Transit','','1990-01-01','M','1','1','root@localhost','root@localhost');
INSERT INTO peopleaddresses(PersonID,AddressTypeID,AddressLine1,AddressLine2,City,State,PostalCode,Country,TouchedBy,CreatedBy) VALUES
        ((select max(p.personid) from people p),1,'800 Arthur Ave.','','Elk Grove Village','IL','60007','US','root@localhost','root@localhost');
INSERT INTO peoplelinks (PersonID,LinkTypeID,LinkValue,IsPreferred,TouchedBy,CreatedBy) VALUES
        ((select max(p.personid) from people p),6,'user02@ta.com','F','root@localhost','root@localhost');
INSERT INTO peoplelinks (PersonID,LinkTypeID,LinkValue,IsPreferred,TouchedBy,CreatedBy) VALUES
        ((select max(p.personid) from people p),2,'6096203827','T','root@localhost','root@localhost');
INSERT INTO logins (LoginID,PersonID,UserLogin,TouchedBy,CreatedBy) VALUES 
        ((select max(l.loginid)+1 from logins l),(select max(p.personid) from people p),'user02@ta.com','root@localhost','root@localhost');
INSERT INTO authentication (PersonID,AuthenticationTypeID,Response,TouchedBy,CreatedBy) VALUES 
        ((select max(p.personid) from people p),1,'$2a$10$zlA213tFDRqOH0ffTtmnoe6Lp2qSdOUZxVzXbUSpygDjrvU4pxDSK','root@localhost','root@localhost');
INSERT INTO groupsofpeople (GroupID,PersonID,TouchedBy,CreatedBy) VALUES
        ((select groupid from groups where name='TA Default'),(select max(p.personid) from people p),'root@localhost','root@localhost');
INSERT INTO groupsofpeople (GroupID,PersonID,TouchedBy,CreatedBy) VALUES
        ((select groupid from groups where name='TA User'),(select max(p.personid) from people p),'root@localhost','root@localhost');
INSERT INTO groupsofpeople (GroupID,PersonID,TouchedBy,CreatedBy) VALUES
        ((select groupid from groups where name='TA Staff'),(select max(p.personid) from people p),'root@localhost','root@localhost');
---------------------------------------------------------------------------------------------------------------------------


---------------------------Create Super User and add to groups------------------------------------------------------------
INSERT INTO people (PersonID,Prefix,NameLast,NameMiddle,NameFirst,Suffix,DateOfBirth,Gender,Status,LanguageID,TouchedBy,CreatedBy) VALUES
        ((select max(p.personid) + 1 from people p),'','SuperUser','','Transit','','1990-01-01','M','1','1','root@localhost','root@localhost');
INSERT INTO peopleaddresses(PersonID,AddressTypeID,AddressLine1,AddressLine2,City,State,PostalCode,Country,TouchedBy,CreatedBy) VALUES
        ((select max(p.personid) from people p),1,'800 Arthur Ave.','','Elk Grove Village','IL','60007','US','root@localhost','root@localhost');
INSERT INTO peoplelinks (PersonID,LinkTypeID,LinkValue,IsPreferred,TouchedBy,CreatedBy) VALUES
        ((select max(p.personid) from people p),6,'user03@ta.com','F','root@localhost','root@localhost');
INSERT INTO peoplelinks (PersonID,LinkTypeID,LinkValue,IsPreferred,TouchedBy,CreatedBy) VALUES
        ((select max(p.personid) from people p),2,'7096203827','T','root@localhost','root@localhost');
INSERT INTO logins (LoginID,PersonID,UserLogin,TouchedBy,CreatedBy) VALUES 
        ((select max(l.loginid)+1 from logins l),(select max(p.personid) from people p),'user03@ta.com','root@localhost','root@localhost');
INSERT INTO authentication (PersonID,AuthenticationTypeID,Response,TouchedBy,CreatedBy) VALUES 
        ((select max(p.personid) from people p),1,'$2a$10$zlA213tFDRqOH0ffTtmnoe6Lp2qSdOUZxVzXbUSpygDjrvU4pxDSK','root@localhost','root@localhost');
INSERT INTO groupsofpeople (GroupID,PersonID,TouchedBy,CreatedBy) VALUES
        ((select groupid from groups where name='TA Default'),(select max(p.personid) from people p),'root@localhost','root@localhost');
INSERT INTO groupsofpeople (GroupID,PersonID,TouchedBy,CreatedBy) VALUES
        ((select groupid from groups where name='TA User'),(select max(p.personid) from people p),'root@localhost','root@localhost');
INSERT INTO groupsofpeople (GroupID,PersonID,TouchedBy,CreatedBy) VALUES
        ((select groupid from groups where name='TA Super User'),(select max(p.personid) from people p),'root@localhost','root@localhost');
---------------------------------------------------------------------------------------------------------------------------
