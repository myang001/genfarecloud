ALTER TABLE `people` 
DROP FOREIGN KEY `RefDisabilities207`,
DROP FOREIGN KEY `RefLanguages206`;

ALTER TABLE `tickets` 
DROP FOREIGN KEY `RefTicketTypes116`,
DROP FOREIGN KEY `RefValidDays90`;

ALTER TABLE `activitytypes` 
CHANGE COLUMN `IsRevenue` `IsRevenue` CHAR(1) NOT NULL DEFAULT 'F' ,
CHANGE COLUMN `IsRidership` `IsRidership` CHAR(1) NOT NULL DEFAULT 'F' ;

CREATE TABLE IF NOT EXISTS `crossreferencefile` (
  `MediaID` TINYINT(3) UNSIGNED NOT NULL,
  `PrintedID` VARCHAR(25) NOT NULL,
  `ShippingListID` INT(11) NOT NULL,
  `LotNumber` TINYINT(3) UNSIGNED NOT NULL,
  `CVV` CHAR(4) NULL DEFAULT NULL,
  `ElectronicID` BIGINT(20) NOT NULL,
  PRIMARY KEY (`MediaID`, `PrintedID`),
  INDEX `RefLots232` (`ShippingListID` ASC, `LotNumber` ASC),
  CONSTRAINT `RefLots232`
    FOREIGN KEY (`ShippingListID` , `LotNumber`)
    REFERENCES `lots` (`ShippingListID` , `LotNumber`),
  CONSTRAINT `RefMedia231`
    FOREIGN KEY (`MediaID`)
    REFERENCES `media` (`MediaID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

ALTER TABLE `disabilities` 
CHANGE COLUMN `DisabilityID` `DisabilityID` TINYINT(3) UNSIGNED NOT NULL AUTO_INCREMENT ;

ALTER TABLE `eventdetails` 
CHANGE COLUMN `TSEvent` `TSEvent` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ;

ALTER TABLE `languages` 
CHANGE COLUMN `LanguageID` `LanguageID` TINYINT(3) UNSIGNED NOT NULL AUTO_INCREMENT ;

ALTER TABLE `media` 
ADD COLUMN `QuantityOnHandTotal` INT(11) NULL DEFAULT '0' AFTER `AvailableSlots`;

ALTER TABLE `offerings` 
CHANGE COLUMN `IsSubscribable` `IsSubscribable` CHAR(1) NOT NULL DEFAULT 'F' ,
CHANGE COLUMN `IsBulk` `IsBulk` CHAR(1) NOT NULL DEFAULT 'F' ;

ALTER TABLE `organizationaddresses` 
ADD COLUMN `Country` CHAR(10) NULL DEFAULT NULL AFTER `PostalCode`;

ALTER TABLE `organizationlinks` 
CHANGE COLUMN `IsPreferred` `IsPreferred` CHAR(1) NULL DEFAULT 'F' ;

CREATE TABLE IF NOT EXISTS `organizationofferings` (
  `OrganizationID` INT(11) NOT NULL,
  `OfferingID` TINYINT(3) UNSIGNED NOT NULL,
  `QuantityOnHand` INT(11) NOT NULL DEFAULT '0',
  `TouchedBy` VARCHAR(50) NOT NULL,
  `TouchedAt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`OrganizationID`, `OfferingID`),
  INDEX `RefOfferings235` (`OfferingID` ASC),
  CONSTRAINT `RefOrganizations236`
    FOREIGN KEY (`OrganizationID`)
    REFERENCES `organizations` (`OrganizationID`),
  CONSTRAINT `RefOfferings235`
    FOREIGN KEY (`OfferingID`)
    REFERENCES `offerings` (`OfferingID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

ALTER TABLE `organizations` 
DROP COLUMN `PaymentMethod`,
CHANGE COLUMN `CreditLimit` `CreditLimit` DECIMAL(9,2) NULL DEFAULT NULL ,
CHANGE COLUMN `Balance` `Balance` DECIMAL(9,2) NULL DEFAULT NULL ,
ADD COLUMN `ParentOrganization` INT(11) NULL DEFAULT NULL AFTER `OrganizationID`,
ADD COLUMN `PaymentTypeID` TINYINT(3) UNSIGNED NOT NULL AFTER `BillingMethod`,
ADD INDEX `RefOrganizations220` (`ParentOrganization` ASC),
ADD INDEX `RefPaymentTypes225` (`PaymentTypeID` ASC);

ALTER TABLE `organizationtypes` 
CHANGE COLUMN `IsRetail` `IsRetail` CHAR(1) NOT NULL DEFAULT 'F' ;

ALTER TABLE `paymenttypes` 
CHANGE COLUMN `IsCredit` `IsCredit` CHAR(1) NULL DEFAULT 'T' ;

ALTER TABLE `people` 
CHANGE COLUMN `LanguageID` `LanguageID` TINYINT(3) UNSIGNED NULL DEFAULT NULL ,
CHANGE COLUMN `DisabilityID` `DisabilityID` TINYINT(3) UNSIGNED NULL DEFAULT NULL ;

ALTER TABLE `peopleaddresses` 
ADD COLUMN `Country` CHAR(10) NULL DEFAULT NULL AFTER `PostalCode`;

ALTER TABLE `peoplelinks` 
CHANGE COLUMN `IsPreferred` `IsPreferred` CHAR(1) NULL DEFAULT 'F' ;

ALTER TABLE `periods` 
DROP COLUMN `TicketTypeID`,
ADD COLUMN `PeriodID` TINYINT(3) UNSIGNED NOT NULL AUTO_INCREMENT FIRST,
DROP PRIMARY KEY,
ADD PRIMARY KEY (`PeriodID`);

ALTER TABLE `sales` 
ADD COLUMN `TicketTypeID` TINYINT(3) UNSIGNED NULL DEFAULT NULL AFTER `OfferingID`,
ADD COLUMN `DiscountTypeID` TINYINT(3) UNSIGNED NULL DEFAULT NULL AFTER `TicketTypeID`,
ADD COLUMN `PaymentTypeID` TINYINT(3) UNSIGNED NOT NULL AFTER `DiscountTypeID`,
ADD INDEX `RefPaymentTypes226` (`PaymentTypeID` ASC),
ADD INDEX `RefTicketTypes227` (`TicketTypeID` ASC),
ADD INDEX `RefDiscountTypes228` (`DiscountTypeID` ASC);

ALTER TABLE `stops` 
CHANGE COLUMN `IsAdded` `IsAdded` CHAR(1) NULL DEFAULT 'F' ;

ALTER TABLE `tickets` 
CHANGE COLUMN `TicketTypeID` `TicketTypeID` TINYINT(3) UNSIGNED NULL DEFAULT NULL ,
CHANGE COLUMN `ValidDayID` `ValidDayID` TINYINT(3) UNSIGNED NULL DEFAULT NULL ;

ALTER TABLE `validdays` 
CHANGE COLUMN `IsDate` `IsDate` CHAR(1) NULL DEFAULT 'F' ;

ALTER TABLE `walletcontents` 
CHANGE COLUMN `IsBad` `IsBad` CHAR(1) NOT NULL DEFAULT 'F' ,
CHANGE COLUMN `datestart` `datestart` DATETIME NULL DEFAULT NULL ,
ADD COLUMN `PendingCount` TINYINT(4) NULL DEFAULT NULL AFTER `ValueUse`;

ALTER TABLE `wallets` 
CHANGE COLUMN `IsBad` `IsBad` CHAR(1) NULL DEFAULT 'F' ,
CHANGE COLUMN `IsRegistered` `IsRegistered` CHAR(1) NULL DEFAULT 'F' ,
CHANGE COLUMN `IsThirdParty` `IsThirdParty` CHAR(1) NULL DEFAULT 'F' ;

ALTER TABLE `organizations` 
ADD CONSTRAINT `RefOrganizations220`
  FOREIGN KEY (`ParentOrganization`)
  REFERENCES `organizations` (`OrganizationID`),
ADD CONSTRAINT `RefPaymentTypes225`
  FOREIGN KEY (`PaymentTypeID`)
  REFERENCES `paymenttypes` (`PaymentTypeID`);

ALTER TABLE `people` 
ADD CONSTRAINT `RefDisabilities207`
  FOREIGN KEY (`DisabilityID`)
  REFERENCES `disabilities` (`DisabilityID`),
ADD CONSTRAINT `RefLanguages206`
  FOREIGN KEY (`LanguageID`)
  REFERENCES `languages` (`LanguageID`);

ALTER TABLE `sales` 
ADD CONSTRAINT `RefPaymentTypes226`
  FOREIGN KEY (`PaymentTypeID`)
  REFERENCES `paymenttypes` (`PaymentTypeID`),
ADD CONSTRAINT `RefDiscountTypes228`
  FOREIGN KEY (`DiscountTypeID`)
  REFERENCES `discounttypes` (`DiscountTypeID`),
ADD CONSTRAINT `RefTicketTypes227`
  FOREIGN KEY (`TicketTypeID`)
  REFERENCES `tickettypes` (`TicketTypeID`);

ALTER TABLE `tickets` 
ADD CONSTRAINT `RefTicketTypes116`
  FOREIGN KEY (`TicketTypeID`)
  REFERENCES `tickettypes` (`TicketTypeID`),
ADD CONSTRAINT `RefValidDays90`
  FOREIGN KEY (`ValidDayID`)
  REFERENCES `validdays` (`ValidDayID`);
