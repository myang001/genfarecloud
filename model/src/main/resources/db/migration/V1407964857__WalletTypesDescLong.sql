ALTER TABLE wallettypes ADD DescriptionLong VARCHAR(1024) null AFTER Description;

UPDATE wallettypes 
SET DescriptionLong='Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum id hendrerit ipsum. Interdum et malesuada fames ac ante ipsum primis in faucibus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean semper nisi ipsum, nec molestie metus convallis a. Praesent vel arcu in sem suscipit gravida.'
WHERE wallettypeid='1';

UPDATE wallettypes 
SET DescriptionLong='Integer eget commodo tellus, id faucibus nunc. Suspendisse varius ipsum in ligula auctor, nec hendrerit dolor tincidunt. Integer porta vitae urna non porta. Nam placerat mi velit, id sodales erat ultrices a. Cras ullamcorper nunc id sem egestas, vel imperdiet lacus consequat. Vestibulum consequat malesuada turpis quis tincidunt. Suspendisse sit amet eros nisl.'
WHERE wallettypeid='2';

UPDATE wallettypes 
SET DescriptionLong='Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Praesent eleifend eros in gravida ullamcorper. Phasellus id diam ornare, euismod augue eu, molestie turpis. Nulla euismod faucibus velit ut aliquam. Suspendisse dignissim nisi orci, nec interdum risus imperdiet quis.'
WHERE wallettypeid='3';

UPDATE wallettypes 
SET DescriptionLong='Donec ante sapien, eleifend id dui id, vulputate lacinia lacus. Integer ut risus a lectus consectetur mollis. Suspendisse potenti. Pellentesque vitae quam quis diam pretium scelerisque sit amet a mauris. Suspendisse eget malesuada nulla. Fusce dapibus libero magna, volutpat blandit eros dapibus eu.'
WHERE wallettypeid='4';

UPDATE wallettypes 
SET DescriptionLong='Vivamus faucibus nisl sit amet semper tristique. Cras ullamcorper magna nec nulla ultrices, ac eleifend mauris scelerisque. Donec euismod magna a augue volutpat tincidunt. Pellentesque tincidunt quam a tortor porta, at molestie quam commodo.'
WHERE wallettypeid='5';

UPDATE wallettypes 
SET DescriptionLong='Morbi vehicula eu lorem id sollicitudin. Nunc at ultricies nisl, sed mollis eros. Aliquam viverra justo venenatis hendrerit laoreet. Donec aliquet laoreet leo, id dignissim est suscipit fringilla. Vivamus scelerisque metus purus, non bibendum lorem congue molestie.'
WHERE wallettypeid='6';

UPDATE wallettypes 
SET DescriptionLong='Mauris adipiscing turpis vitae euismod bibendum. Vestibulum non dolor id orci malesuada auctor. Aenean accumsan, ligula eget ornare vulputate, lacus turpis congue elit, sit amet semper nunc velit ut est.'
WHERE wallettypeid='7';

UPDATE wallettypes 
SET DescriptionLong='In a dolor feugiat, vestibulum lacus nec, elementum metus. Phasellus eget fermentum velit, quis lacinia lorem. Nunc nisi tortor, malesuada nec dui ac, blandit aliquet lacus. Nullam ut imperdiet sapien. Pellentesque viverra lacus nec ligula rhoncus consequat. Sed dapibus augue a vestibulum imperdiet.'
WHERE wallettypeid='8';

UPDATE wallettypes 
SET DescriptionLong='Sed elit turpis, pretium sed vestibulum non, tempor et urna. In ut odio ac orci condimentum viverra in laoreet erat. Donec malesuada sem non risus venenatis molestie. Etiam at dolor fringilla, porttitor mauris in, malesuada augue. Aenean ac tellus sit amet lacus eleifend mollis at at magna. Nunc vel enim sed lectus volutpat sodales.'
WHERE wallettypeid='9';
