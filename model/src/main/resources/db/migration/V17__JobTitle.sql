CREATE TABLE `jobTitle` (
  `JobTitleID` int(3) unsigned NOT NULL AUTO_INCREMENT,
  `Description` varchar(50) NOT NULL,
  `TouchedBy` varchar(50) NOT NULL,
  `TouchedAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CreatedBy` varchar(50) NOT NULL,
  `CreatedAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`JobTitleID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

ALTER TABLE people ADD COLUMN JobTitleID int(3) unsigned DEFAULT NULL AFTER DisabilityID;

CREATE INDEX RefJobTitle ON people (JobTitleID);

ALTER TABLE people ADD FOREIGN KEY RefJobTitle (JobTitleID) REFERENCES jobTitle (JobTitleID);