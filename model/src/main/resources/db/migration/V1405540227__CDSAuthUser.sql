CREATE OR REPLACE VIEW CDSAuthUser AS
SELECT p.NameLast AS LastName, p.NameFirst AS FirstName, p.Status AS PersonStatus, o.Name AS OrgName, g.Description AS GroupSecLvlDescr, l.UserLogin AS LoginId, l.TouchedAt AS DateLastAccessed
FROM People p
INNER JOIN GroupsofPeople gop ON p.personid = gop.personid
INNER JOIN Groups g ON g.groupid = gop.groupid
INNER JOIN Logins l ON p.personid = l.personid
INNER JOIN Organizations o ON g.OrganizationID = o.OrganizationID;