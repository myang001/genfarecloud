SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

ALTER TABLE `PendingActivity` 
DROP FOREIGN KEY `RefPendingTypes87`;

ALTER TABLE `ProcessedActivity` 
DROP FOREIGN KEY `RefPendingTypes211`;

ALTER TABLE `Subscriptions` 
DROP FOREIGN KEY `RefCreditCardActivity292`;

ALTER TABLE `Activities` 
ADD COLUMN `RouteID` SMALLINT(6) NULL DEFAULT NULL AFTER `EquipmentID`,
ADD INDEX `RefRoutes299` (`RouteID` ASC);

ALTER TABLE `ActivityDetails` 
DROP COLUMN `TSActivity`,
ADD COLUMN `DateActivity` DATETIME NULL DEFAULT NULL AFTER `Latitude`;

ALTER TABLE `ActivityTypes` 
ADD COLUMN `IsPending` CHAR(1) NOT NULL DEFAULT 'F' AFTER `IsRidership`;

ALTER TABLE `EventDetails` 
DROP COLUMN `TSEvent`,
ADD COLUMN `DateEvent` DATETIME NULL DEFAULT NULL AFTER `EquipmentID`;

ALTER TABLE `Groups` 
ADD COLUMN `Description` VARCHAR(50) NOT NULL AFTER `OrganizationID`;

ALTER TABLE `OrderItems` 
ADD COLUMN `Notes` VARCHAR(4000) NULL DEFAULT NULL AFTER `Status`;

ALTER TABLE `OrderStatuses` 
ADD COLUMN `IsItem` CHAR(1) NOT NULL DEFAULT 'F' AFTER `Description`,
ADD COLUMN `IsFulfillment` CHAR(1) NOT NULL DEFAULT 'F' AFTER `IsItem`;

ALTER TABLE `PendingActivity` 
DROP COLUMN `PendingTypeID`,
ADD COLUMN `ActivityTypeID` TINYINT(3) UNSIGNED NOT NULL AFTER `LoadSequence`,
ADD INDEX `RefActivityTypes309` (`ActivityTypeID` ASC),
DROP INDEX `Ref6287` ;

ALTER TABLE `ProcessedActivity` 
DROP COLUMN `PendingTypeID`,
ADD COLUMN `ActivityTypeID` TINYINT(3) UNSIGNED NOT NULL AFTER `LoadSequence`,
ADD COLUMN `Reason` VARCHAR(64) NULL DEFAULT NULL AFTER `DateExpires`,
ADD INDEX `RefActivityTypes310` (`ActivityTypeID` ASC),
DROP INDEX `RefPendingTypes211` ;

ALTER TABLE `Shipments` 
ADD COLUMN `DateShipped` DATETIME NULL DEFAULT NULL AFTER `OrderItemNumber`;

ALTER TABLE `Subscriptions` 
DROP COLUMN `CreditCardActivityID`,
DROP COLUMN `LastFour`,
DROP COLUMN `DateCCExpires`,
CHANGE COLUMN `SubscriptionID` `SubscriptionID` BIGINT(19) UNSIGNED NULL DEFAULT NULL AUTO_INCREMENT ,
ADD COLUMN `PersonID` INT(11) NOT NULL AFTER `SubscriptionStatusID`,
ADD COLUMN `CardNumber` TINYINT(3) UNSIGNED NOT NULL AFTER `PersonID`,
ADD INDEX `RefPeopleCards311` (`PersonID` ASC, `CardNumber` ASC),
DROP INDEX `RefCreditCardActivity292` ;

ALTER TABLE `WalletContents` 
ADD COLUMN `DateFirstUse` DATETIME NULL DEFAULT NULL AFTER `DateExpires`,
ADD COLUMN `DateLastUse` DATETIME NULL DEFAULT NULL AFTER `DateFirstUse`;

DROP TABLE IF EXISTS `PendingTypes` ;

ALTER TABLE `Activities` 
ADD CONSTRAINT `RefRoutes299`
  FOREIGN KEY (`RouteID`)
  REFERENCES `Routes` (`RouteID`);

ALTER TABLE `PendingActivity` 
ADD CONSTRAINT `RefActivityTypes309`
  FOREIGN KEY (`ActivityTypeID`)
  REFERENCES `ActivityTypes` (`ActivityTypeID`);

ALTER TABLE `ProcessedActivity` 
ADD CONSTRAINT `RefActivityTypes310`
  FOREIGN KEY (`ActivityTypeID`)
  REFERENCES `ActivityTypes` (`ActivityTypeID`);

ALTER TABLE `Subscriptions` 
ADD CONSTRAINT `RefPeopleCards311`
  FOREIGN KEY (`PersonID` , `CardNumber`)
  REFERENCES `PeopleCards` (`PersonID` , `CardNumber`);

CREATE OR REPLACE view WalletHistory as
select
    a.EquipmentID
    ,a.RouteID
    ,ad.DateActivity
    ,wi.Identifier
    ,w.WalletID
    ,ad.Slot
    ,ad.TicketID
    ,t.TicketTypeID
    ,ad.DiscountTypeID
    ,ad.ActivityTypeID
    ,ad.AmountCharge
    ,ad.AmountRemaining
    ,ad.AmountTendered
    ,ad.AmountReturned
    ,w.WalletTypeID
    ,ad.PendingID
    ,ad.PendingStatus
    ,p.ValueActivity
from activitydetails ad
inner join activities a on ad.ActivityID = a.ActivityID
inner join tickets t on ad.TicketID = t.TicketID
inner join walletidentifiers wi on ad.CardIdentifier = wi.Identifier and IdentifierID = 2
inner join wallets w on wi.walletid = w.WalletID
inner join walletcontents wc on w.WalletID = wc.WalletID and ad.slot = wc.slot and slotstatus = 2
left outer join pendingactivity p on ad.PendingID = p.PendingID;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
