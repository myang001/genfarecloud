
ALTER TABLE Organizations
	ADD MainContact INT DEFAULT NULL AFTER CodeThirdParty
;

ALTER TABLE Organizations 
	ADD CONSTRAINT RefOrganizationsMainContact
		FOREIGN KEY (MainContact)
		REFERENCES People(PersonID)
;
