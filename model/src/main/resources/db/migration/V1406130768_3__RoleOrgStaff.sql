/*CREATE ROLE_ORG_STAFF*/
INSERT INTO people (
PersonID,
ParentID,
Prefix,
NameLast,
NameMiddle,
NameFirst,
Suffix,
DateOfBirth,
Gender,
Status,
LanguageID,
DisabilityID,
TouchedBy,
TouchedAt,CreatedBy,CreatedAt) VALUES (
(select max(p.personid) + 1 from people p),
null,
'',
'org-staff@spx.com',
'',
'org-staff@spx.com',
'',
'1900-01-01',
'',
0,
1,
null,
'root@localhost',
'2014-03-26 16:52:15.','root@localhost',
'2014-03-26 17:08:38.');

INSERT INTO peopleaddresses (
PersonID,
AddressTypeID,
AddressLine1,
AddressLine2,
City,
State,
PostalCode,
Country,
TouchedBy,
TouchedAt,CreatedBy,CreatedAt) 
VALUES (
(select max(personid) from people),
1,
'NO:1 Halas Way',
'',
'Lake Forest',
'IL',
'60001',
'US',
'root@localhost',
'2014-03-26 16:52:23.','root@localhost',
'2014-03-26 17:08:38.');

INSERT INTO peoplelinks (PersonID,LinkTypeID,LinkValue,IsPreferred,TouchedBy,TouchedAt,CreatedBy,CreatedAt) VALUES ((select max(personid) from people),5,'org-staff@spx.com','F','root@localhost','2014-03-26 16:52:33.','root@localhost',
'2014-03-26 17:08:38.');

INSERT INTO logins (LoginID,PersonID,UserLogin,TouchedBy,TouchedAt,CreatedBy,CreatedAt) VALUES ((select max(l.loginid) + 1 from logins l),(select max(personid) from people),'org-staff@spx.com','root@localhost','2014-03-26 17:07:04.','root@localhost',
'2014-03-26 17:08:38.');

INSERT INTO authentication (PersonID, AuthenticationTypeID, Response, TouchedBy, TouchedAt,CreatedBy,CreatedAt) VALUES ((select max(personid) from people),(SELECT AuthenticationTypeID from AuthenticationTypes where Description='Password'),'$2a$10$PAtm/bdawX9lopzcRW0lo.kDTOZZVEGqbCOHOuO6uj16NMs79GN0m', 'unassigned','2014-04-14 19:51:04','root@localhost',
'2014-03-26 17:08:38.');

INSERT INTO groups
(GroupID, OrganizationID, Description, TouchedBy,`Type`,CreatedBy,CreatedAt)
VALUES
((select max(g.GroupID) + 1 from groups g),10,'Org Staff','root@localhost','ROLE_ORG_STAFF','root@localhost',
'2014-03-26 17:08:38.');

INSERT INTO `groupsofpeople` 
(`GroupID`, `PersonID`,`TouchedBy`,CreatedBy,CreatedAt) 
VALUES
((select max(g.GroupID) from groups g), (select max(personid) from people), 'root@localhost','root@localhost',
'2014-03-26 17:08:38.');

INSERT INTO `groupsofpeople` 
(`GroupID`, `PersonID`,`TouchedBy`,CreatedBy,CreatedAt) 
VALUES
((select g.GroupID from groups g where g.Type = 'ROLE_DEFAULT'), (select max(personid) from people), 'root@localhost','root@localhost',
'2014-03-26 17:08:38.');