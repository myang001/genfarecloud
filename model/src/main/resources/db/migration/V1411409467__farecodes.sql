--
-- Remove Discounts
--
ALTER TABLE Discounts
	DROP FOREIGN KEY RefDiscountTypes12,
	DROP FOREIGN KEY RefPeople13
;

DELETE FROM Discounts;
DROP TABLE Discounts;


--
-- Rename DiscountTypes to Farecodes
--

-- Update TicketDiscounts
DELETE FROM TicketDiscounts;
ALTER TABLE TicketDiscounts
	DROP FOREIGN KEY RefOrganizations160,
	DROP FOREIGN KEY RefDiscountTypes62,
	DROP COLUMN OrganizationID,
	DROP COLUMN DeviceKey,
	DROP COLUMN Display,
	DROP COLUMN Description,
	CHANGE COLUMN DiscountTypeID FarecodeID TINYINT UNSIGNED
;

RENAME TABLE TicketDiscounts TO TicketsFarecodes;

-- Drop DiscountType FK References And Rename DiscountTypeID Columns
ALTER TABLE ActivityDetails
	DROP FOREIGN KEY RefDiscountTypes127,
	CHANGE COLUMN DiscountTypeID FarecodeID TINYINT UNSIGNED
;

ALTER TABLE ActivitySummary
	DROP FOREIGN KEY RefDiscountTypes72,
	CHANGE COLUMN DiscountTypeID FarecodeID TINYINT UNSIGNED
;

ALTER TABLE OrderItems
	DROP FOREIGN KEY RefDiscountTypes128,
	CHANGE COLUMN DiscountTypeID FarecodeID TINYINT UNSIGNED
;

ALTER TABLE PendingActivity
	DROP FOREIGN KEY RefDiscountTypes129,
	CHANGE COLUMN DiscountTypeID FarecodeID TINYINT UNSIGNED
;

ALTER TABLE ProcessedActivity
	DROP FOREIGN KEY RefDiscountTypes208,
	CHANGE COLUMN DiscountTypeID FarecodeID TINYINT UNSIGNED
;

ALTER TABLE Sales
	DROP FOREIGN KEY RefDiscountTypes228,
	CHANGE COLUMN DiscountTypeID FarecodeID TINYINT UNSIGNED
;

ALTER TABLE WalletContents
	DROP FOREIGN KEY RefDiscountTypes126,
	DROP COLUMN DiscountTypeID
;

ALTER TABLE Wallets
	ADD COLUMN FarecodeID TINYINT UNSIGNED AFTER Nickname
;

-- Change DiscountTypeTable to Farecodes
RENAME TABLE DiscountTypes TO Farecodes;

ALTER TABLE Farecodes
	CHANGE COLUMN DiscountTypeID FarecodeID TINYINT UNSIGNED  AUTO_INCREMENT
;


-- Rebuild SEED data

DELETE FROM Farecodes;
INSERT INTO Farecodes (FarecodeID,Description,Criteria,TermLimit,TouchedBy,CreatedBy) VALUES(1,'Full',NULL,NULL,'admin','admin');
INSERT INTO Farecodes (FarecodeID,Description,Criteria,TermLimit,TouchedBy,CreatedBy) VALUES(2,'Senior',NULL,NULL,'admin','admin');
INSERT INTO Farecodes (FarecodeID,Description,Criteria,TermLimit,TouchedBy,CreatedBy) VALUES(3,'Youth',NULL,NULL,'admin','admin');
INSERT INTO Farecodes (FarecodeID,Description,Criteria,TermLimit,TouchedBy,CreatedBy) VALUES(4,'Veteran',NULL,NULL,'admin','admin');
INSERT INTO Farecodes (FarecodeID,Description,Criteria,TermLimit,TouchedBy,CreatedBy) VALUES(5,'Employee',NULL,NULL,'admin','admin');
INSERT INTO Farecodes (FarecodeID,Description,Criteria,TermLimit,TouchedBy,CreatedBy) VALUES(6,'Employee Sponsored',NULL,NULL,'admin','admin');
INSERT INTO Farecodes (FarecodeID,Description,Criteria,TermLimit,TouchedBy,CreatedBy) VALUES(7,'Disabled',NULL,NULL,'admin','admin');


-- Reinstate Foreign Keys

ALTER TABLE ActivityDetails ADD CONSTRAINT RefActivityDetailsFarecode
    FOREIGN KEY (FarecodeID)
    REFERENCES Farecodes(FarecodeID)
;

ALTER TABLE ActivitySummary ADD CONSTRAINT RefActivitySummaryFarecode
    FOREIGN KEY (FarecodeID)
    REFERENCES Farecodes(FarecodeID)
;

ALTER TABLE OrderItems ADD CONSTRAINT RefOrderItemsFarecode
    FOREIGN KEY (FarecodeID)
    REFERENCES Farecodes(FarecodeID)
;

ALTER TABLE PendingActivity ADD CONSTRAINT RefPendingActivityFarecode
    FOREIGN KEY (FarecodeID)
    REFERENCES Farecodes(FarecodeID)
;

ALTER TABLE ProcessedActivity ADD CONSTRAINT RefProcessedActivityFarecode
    FOREIGN KEY (FarecodeID)
    REFERENCES Farecodes(FarecodeID)
;

ALTER TABLE Sales ADD CONSTRAINT RefSalesFarecode
    FOREIGN KEY (FarecodeID)
    REFERENCES Farecodes(FarecodeID)
;

ALTER TABLE Wallets ADD CONSTRAINT RefWalletsFarecode
    FOREIGN KEY (FarecodeID)
    REFERENCES Farecodes(FarecodeID)
;

ALTER TABLE TicketsFarecodes ADD CONSTRAINT RefTicketsFarecodesFarecode
    FOREIGN KEY (FarecodeID)
    REFERENCES Farecodes(FarecodeID)
;

-- Build ManyToMany Relationship With People
CREATE TABLE PeopleFarecodes (
	PersonID     INT NOT NULL,
	FarecodeID   TINYINT UNSIGNED NOT NULL,
	TouchedBy    VARCHAR(50)    NOT NULL,
    TouchedAt    DATETIME       DEFAULT NOW() NOT NULL,
    CreatedBy    VARCHAR(50)    NOT NULL,
    CreatedAt    DATETIME       DEFAULT NOW() NOT NULL,
    PRIMARY KEY (PersonID,FarecodeID)
)ENGINE=INNODB;

ALTER TABLE PeopleFarecodes ADD CONSTRAINT RefPeopleFarecodesPerson
	FOREIGN KEY (PersonID)
	REFERENCES People(PersonID)
;

ALTER TABLE PeopleFarecodes ADD CONSTRAINT RefPeopleFarecodesFarecode
	FOREIGN KEY (FarecodeID)
	REFERENCES Farecodes(FarecodeID)
;
