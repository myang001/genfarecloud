-- 
-- TABLE: zones 
--

ALTER TABLE zones
  ADD COLUMN Name           VARCHAR(50)    NOT NULL AFTER ZoneID
;

UPDATE zones SET Name='NX Zone 1', Description='Zone 1 description sidfu sdfiuh.' WHERE ZoneID='1';
UPDATE zones SET Name='NX Zone 2', Description='Zone 2 description ffioh wfih gi.' WHERE ZoneID='2';
UPDATE zones SET Name='NX Zone 3', Description='Zone 3 description woetiu wtoiu wetoiu.' WHERE ZoneID='3';