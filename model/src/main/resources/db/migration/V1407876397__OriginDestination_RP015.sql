-- Origin/Destination Report - 
CREATE TABLE  temp_rp015 (walletIdentifier BIGINT(20), TicketDescription VARCHAR(50), Discount VARCHAR(50), AmtCharged DECIMAL (5,2), AmtRemaining DECIMAL (5,2), ActivityType VARCHAR(50), PendingType VARCHAR(50),
 ValueActivity DECIMAL (5,2), EquipmentDesc VARCHAR(50), ActivityDate DATETIME, Location VARCHAR(50));

 
 --  Create View to be used for origin/destination report, RP015
CREATE OR REPLACE VIEW origindestination AS
SELECT walletIdentifier , TicketDescription , Discount , AmtCharged , AmtRemaining , ActivityType , PendingType , ValueActivity , EquipmentDesc , ActivityDate, Location FROM temp_rp015;