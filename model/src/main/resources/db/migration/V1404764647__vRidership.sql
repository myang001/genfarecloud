CREATE OR REPLACE VIEW vRidership AS
SELECT d.DateActivity, act.CountAcitivity AS TimesUsed, t.Description AS TicketDescription
FROM ActivitySummary act, Dates d, Tickets t;