-- 
-- Created an Equipment Configuration table. This should add the flexability to add whatever configuration is needed to any
-- specific piece of equipment.
--




CREATE TABLE Config(
    ConfigID                INT            AUTO_INCREMENT,
    ConfigKey               VARCHAR(50),
    ConfigValue             VARCHAR(50),
    TouchedBy               VARCHAR(50),
    TouchedAt               DATETIME       DEFAULT NOW() NOT NULL,
    CreatedBy               VARCHAR(50),
    CreatedAt               DATETIME       DEFAULT NOW() NOT NULL,
    PRIMARY KEY (ConfigID)
)ENGINE=INNODB
;


CREATE INDEX RefConf ON Config(ConfigID)
;




CREATE TABLE EquipmentConfig(
    EquipmentID             INT            NOT NULL,
    ConfigID            INT            NOT NULL,
    TouchedBy               VARCHAR(50),
    TouchedAt               DATETIME       DEFAULT NOW() NOT NULL,
    CreatedBy               VARCHAR(50),
    CreatedAt               DATETIME       DEFAULT NOW() NOT NULL,
    PRIMARY KEY (EquipmentID, ConfigID)
)ENGINE=INNODB
;


CREATE INDEX RefEquipConf ON EquipmentConfig(EquipmentID)
;


ALTER TABLE `EquipmentConfig`
ADD CONSTRAINT RefEquipConf192
    FOREIGN KEY (EquipmentID)
    REFERENCES Equipment(EquipmentID)
;



ALTER TABLE `EquipmentConfig`
ADD CONSTRAINT RefEquipConf193
    FOREIGN KEY (ConfigID)
    REFERENCES Config(ConfigID)
;


-- 
-- Create a collectoin of equipment and then tie that into group permissions this will allow easy configuration of a new machine 
--


CREATE TABLE EquipmentCollections(
    EquipmentCollectionID           INT            NOT NULL,
    Name                            VARCHAR(50),
    Description                     VARCHAR(50),
    TouchedBy                       VARCHAR(50),
    TouchedAt                       DATETIME       DEFAULT NOW() NOT NULL,
    CreatedBy                       VARCHAR(50),
    CreatedAt                       DATETIME       DEFAULT NOW() NOT NULL,
    PRIMARY KEY (EquipmentCollectionID)
)ENGINE=INNODB
;

CREATE INDEX RefEquipColl ON EquipmentCollections(EquipmentCollectionID)
;


CREATE TABLE GroupsEquipmentCollections(
    GroupID                         INT            NOT NULL,
    EquipmentCollectionID           INT            NOT NULL,
    TouchedBy                       VARCHAR(50),
    TouchedAt                       DATETIME       DEFAULT NOW() NOT NULL,
    CreatedBy                       VARCHAR(50),
    CreatedAt                       DATETIME       DEFAULT NOW() NOT NULL,
    PRIMARY KEY (GroupID, EquipmentCollectionID)
)ENGINE=INNODB
;

CREATE INDEX RefGroupEquipColl1 ON GroupsEquipmentCollections(EquipmentCollectionID)
;

CREATE INDEX RefGroupEquipColl2 ON GroupsEquipmentCollections(GroupID)
;


ALTER TABLE `GroupsEquipmentCollections`
ADD CONSTRAINT RefGroupEquipColl192
    FOREIGN KEY (GroupID)
    REFERENCES Groups(GroupID)
;



ALTER TABLE `GroupsEquipmentCollections`
ADD CONSTRAINT RefGroupEquipColl193
    FOREIGN KEY (EquipmentCollectionID)
    REFERENCES EquipmentCollections(EquipmentCollectionID)
;


CREATE TABLE EquipmentCollectionsEquipment(
    EquipmentCollectionID           INT            NOT NULL,
    EquipmentID                     INT            NOT NULL,
    TouchedBy                       VARCHAR(50),
    TouchedAt                       DATETIME       DEFAULT NOW() NOT NULL,
    CreatedBy                       VARCHAR(50),
    CreatedAt                       DATETIME       DEFAULT NOW() NOT NULL,
    PRIMARY KEY (EquipmentCollectionID, EquipmentID)
)ENGINE=INNODB
;


CREATE INDEX RefEquipCollEquip1 ON EquipmentCollectionsEquipment(EquipmentCollectionID)
;

CREATE INDEX RefEquipCollEquip2 ON EquipmentCollectionsEquipment(EquipmentID)
;


ALTER TABLE `EquipmentCollectionsEquipment`
ADD CONSTRAINT EquipmentCollectionsEquipment1
    FOREIGN KEY (EquipmentCollectionID)
    REFERENCES EquipmentCollections(EquipmentCollectionID)
;


ALTER TABLE `EquipmentCollectionsEquipment`
ADD CONSTRAINT EquipmentCollectionsEquipment2
    FOREIGN KEY (EquipmentID)
    REFERENCES Equipment(EquipmentID)
;



-- Fixing the operators table

ALTER TABLE equipment
ADD OrganizationID int NULL;

CREATE INDEX RefEquipOrg1 ON equipment(OrganizationID)
;

ALTER TABLE equipment
ADD CONSTRAINT EquipmentOrginization
    FOREIGN KEY (OrganizationID)
    REFERENCES `organizations` (OrganizationID)
;

INSERT INTO authenticationtypes (AuthenticationTypeID,Description,Prompt,TouchedBy, CreatedBy) VALUES ('6','Employee Password', 'employee password', 'root@localhost', 'root@localhost');

ALTER TABLE operators ADD GroupID int AFTER OperatorTypeID;


ALTER TABLE operators ADD CONSTRAINT RefOperatorsGroups 
    FOREIGN KEY (GroupID)
    REFERENCES Groups(GroupID)
;

