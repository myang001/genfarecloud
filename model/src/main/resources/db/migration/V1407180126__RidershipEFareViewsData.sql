create or replace view ridership as
select "2014-08-01" DateActivity, 15 TimesUsed, "Mobile" TicketDescription union all
select "2014-08-01", 16, "SmartCard" union all
select "2014-08-02", 17, "Mobile" union all
select "2014-08-02", 18, "SmartCard";
