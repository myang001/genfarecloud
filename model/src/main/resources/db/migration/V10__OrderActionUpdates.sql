
CREATE TABLE OrderItemActions (
	  OrderItemActionID tinyint(3) unsigned NOT NULL,
	  Description varchar(50) DEFAULT NULL,
	  CreatedBy varchar(50) NOT NULL DEFAULT 'admin',
	  CreatedAt datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
	  TouchedBy varchar(50) NOT NULL DEFAULT 'admin',
	  TouchedAt datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	  PRIMARY KEY (`OrderItemActionID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO OrderItemActions (OrderItemActionID, Description) values (1, 'New');
INSERT INTO OrderItemActions (OrderItemActionID, Description) values (2, 'Replace');

ALTER TABLE OrderItems
	ADD COLUMN OrderItemActionID TINYINT(3) UNSIGNED NOT NULL AFTER ParentItemNumber;

UPDATE OrderItems SET OrderItemActionID = 1;
	
ALTER TABLE OrderItems
	ADD CONSTRAINT RefOrderItemsActions81
		FOREIGN KEY (OrderItemActionID)
	    	REFERENCES OrderItemActions(OrderItemActionID);

CREATE INDEX Ref2262 ON OrderItems(OrderItemActionID);

CREATE TABLE OrderTypes (
	OrderTypeID tinyint(3) unsigned NOT NULL,
	Description varchar(50) DEFAULT NULL,
	CreatedBy varchar(50) NOT NULL DEFAULT 'admin',
	CreatedAt datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
	TouchedBy varchar(50) NOT NULL DEFAULT 'admin',
	TouchedAt datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (OrderTypeID)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO OrderTypes (OrderTypeID, Description) values (1, 'Sales Order');
INSERT INTO OrderTypes (OrderTypeID, Description) values (2, 'Service Order');

ALTER TABLE Orders
	ADD COLUMN OrderTypeID TINYINT(3) UNSIGNED NOT NULL AFTER OrderData;

UPDATE Orders SET OrderTypeID = 1;
	
ALTER TABLE Orders
	ADD CONSTRAINT RefOrderTypes80
	    FOREIGN KEY (OrderTypeID)
	    REFERENCES OrderTypes(OrderTypeID);

CREATE INDEX Ref2263 ON Orders(OrderTypeID);

