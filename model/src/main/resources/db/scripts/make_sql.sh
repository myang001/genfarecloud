#!/bin/bash

# Generates SQL files for Flyway using Epoch time. This is to help prevent version conflicts.

usage() { echo "Usage: $0 -t <migration|testdata> [-n <name>]" 1>&2; exit 1; }

type= 
name= 

while getopts t:n: opt; do
  case $opt in
  t)
      type=$OPTARG
      if ! [[ $type == "migration" || $type == "testdata" ]] ; then
	      usage	
      fi
      ;;
  n)
      name=$OPTARG
      ;;
  esac
done

shift $((OPTIND - 1))

if [[ -z $type ]] || [[ -z $name ]]; then
	usage
fi

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
date=$(date +%s)

filename="$DIR/../$type/V$(date +%s)__$name.sql"
echo "Creating file: $filename"
touch $filename

