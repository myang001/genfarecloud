/*Delete logins*/
DELETE from logins where personID between 2 and 198;

/*Delete Peoplelinks*/
DELETE from Peoplelinks where personID between 2 and 198;

/*Delete groupsofpeople*/
DELETE from groupsofpeople where personID between 2 and 198;

/*Delete peopleaddresses*/
DELETE from peopleaddresses where personID between 2 and 198;

/*BEGIN delete operators*/
DELETE from activities where operatorID between 2 and 5;
DELETE from operators where operatorID between 2 and 5;
/*END delete operators*/

/*Detach wallets from users*/
UPDATE wallets
SET
PersonID = null
where personID between 2 and 198;

/*Delete Users*/
DELETE from people where personID between 2 and 198;

/*BEGIN update users info*/
update people
set
NameFirst = 'GFCP',
nameLast = 'Admin' 
where personid = 199;

update people
set
NameFirst = 'Default', 
nameLast = 'User'  
where personid = 200;

update people
set
NameFirst = 'Org', 
nameLast = 'Staff'  
where personid = 201;

update people
set
NameFirst = 'Org', 
nameLast = 'Admin'  
where personid = 202;

update people
set
NameFirst = 'Org', 
nameLast = 'Default'  
where personid = 203;

update people
set
NameFirst = 'Staff', 
nameLast = 'User'  
where personid = 204;

update people
set
NameFirst = 'Admin', 
nameLast = 'User'  
where personid = 205;

update people
set
NameFirst = 'Guest', 
nameLast = 'User'  
where personid = 206;
/*END update users info*/

/*CREATE Rider Role*/
INSERT INTO groups
(GroupID, OrganizationID, Name, Role, Description,TouchedBy,CreatedBy)
VALUES
((select max(g.GroupID) + 1 from groups g),null,'Rider','ROLE_RIDER','Rider', 'root@localhost','root@localhost');

/*CREATE Rider User*/
INSERT INTO people (
PersonID,
ParentID,
Prefix,
NameLast,
NameMiddle,
NameFirst,
Suffix,
DateOfBirth,
Gender,
Status,
LanguageID,
DisabilityID,
TouchedBy,
CreatedBy) VALUES (
(select max(p.personid) + 1 from people p),
null,
'',
'User',
'',
'Rider',
'',
'1900-01-01',
'',
1,
1,
null,
'root@localhost',
'root@localhost');

INSERT INTO peopleaddresses (
PersonID,
AddressTypeID,
AddressLine1,
AddressLine2,
City,
State,
PostalCode,
Country,
TouchedBy,
CreatedBy) 
VALUES (
(select max(personid) from people),
1,
'NO:1 Halas Way',
'',
'Lake Forest',
'IL',
'60001',
'US',
'root@localhost',
'root@localhost');

INSERT INTO peoplelinks (PersonID,LinkTypeID,LinkValue,IsPreferred,TouchedBy,CreatedBy) VALUES ((select max(personid) from people),5,'rider@spx.com','T','root@localhost','root@localhost');

INSERT INTO peoplelinks (PersonID,LinkTypeID,LinkValue,IsPreferred,TouchedBy,CreatedBy) VALUES ((select max(personid) from people),1,'9176208480','T','root@localhost','root@localhost');

INSERT INTO logins (LoginID,PersonID,UserLogin,TouchedBy,CreatedBy) VALUES ((select max(l.loginid) + 1 from logins l),(select max(personid) from people),'rider@spx.com','root@localhost','root@localhost');

INSERT INTO authentication (PersonID, AuthenticationTypeID, Response, TouchedBy, CreatedBy) VALUES ((select max(personid) from people),(SELECT AuthenticationTypeID from AuthenticationTypes where Description='Password'),'$2a$10$PAtm/bdawX9lopzcRW0lo.kDTOZZVEGqbCOHOuO6uj16NMs79GN0m', 'unassigned','root@localhost');

INSERT INTO groupsofpeople 
(GroupID, PersonID, TouchedBy, CreatedBy) 
VALUES
((select max(g.GroupID) from groups g), (select max(personid) from people), 'root@localhost','root@localhost');