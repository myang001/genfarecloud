INSERT INTO ShippingLists (shippingListId,mediaid,quantity,TouchedBy,TouchedAt) VALUES (1,1,500,'root@localhost','2014-04-10 14:04:51.');
INSERT INTO ShippingLists (shippingListId,mediaid,quantity,TouchedBy,TouchedAt) VALUES (2,3,300,'root@localhost','2014-04-10 14:04:51.');
INSERT INTO ShippingLists (shippingListId,mediaid,quantity,TouchedBy,TouchedAt) VALUES (3,4,200,'root@localhost','2014-04-10 14:04:51.');
INSERT INTO ShippingLists (shippingListId,mediaid,quantity,TouchedBy,TouchedAt) VALUES (4,1,600,'root@localhost','2014-04-10 14:04:51.');
INSERT INTO ShippingLists (shippingListId,mediaid,quantity,TouchedBy,TouchedAt) VALUES (5,8,700,'root@localhost','2014-04-10 14:04:51.');

INSERT INTO LocationTypes (locationTypeId,description,TouchedBy,TouchedAt) VALUES (1,'Local','root@localhost','2014-04-10 14:04:51.');
INSERT INTO LocationTypes (locationTypeId,description,TouchedBy,TouchedAt) VALUES (2,'Factory','root@localhost','2014-04-10 14:04:51.');
INSERT INTO LocationTypes (locationTypeId,description,TouchedBy,TouchedAt) VALUES (3,'International','root@localhost','2014-04-10 14:04:51.');
INSERT INTO LocationTypes (locationTypeId,description,TouchedBy,TouchedAt) VALUES (4,'US','root@localhost','2014-04-10 14:04:51.');

INSERT INTO Locations (locationId,locationtypeid,TouchedAt) VALUES (1,1,'2014-04-10 14:04:51.');
INSERT INTO Locations (locationId,locationtypeid,TouchedAt) VALUES (2,2,'2014-04-10 14:04:51.');
INSERT INTO Locations (locationId,locationtypeid,TouchedAt) VALUES (3,3,'2014-04-10 14:04:51.');
INSERT INTO Locations (locationId,locationtypeid,TouchedAt) VALUES (4,2,'2014-04-10 14:04:51.');
INSERT INTO Locations (locationId,locationtypeid,TouchedAt) VALUES (5,3,'2014-04-10 14:04:51.');
INSERT INTO Locations (locationId,locationtypeid,TouchedAt) VALUES (6,1,'2014-04-10 14:04:51.');
INSERT INTO Locations (locationId,locationtypeid,TouchedAt) VALUES (7,2,'2014-04-10 14:04:51.');
INSERT INTO Locations (locationId,locationtypeid,TouchedAt) VALUES (8,1,'2014-04-10 14:04:51.');

INSERT INTO Lots (lotNumber,shippinglistid,locationid,TouchedBy,TouchedAt) VALUES (1,1,3,'root@localhost','2014-04-10 14:04:51.');
INSERT INTO Lots (lotNumber,shippinglistid,locationid,TouchedBy,TouchedAt) VALUES (2,2,1,'root@localhost','2014-04-10 14:04:51.');
INSERT INTO Lots (lotNumber,shippinglistid,locationid,TouchedBy,TouchedAt) VALUES (3,3,4,'root@localhost','2014-04-10 14:04:51.');
INSERT INTO Lots (lotNumber,shippinglistid,locationid,TouchedBy,TouchedAt) VALUES (4,1,5,'root@localhost','2014-04-10 14:04:51.');
INSERT INTO Lots (lotNumber,shippinglistid,locationid,TouchedBy,TouchedAt) VALUES (5,2,7,'root@localhost','2014-04-10 14:04:51.');
INSERT INTO Lots (lotNumber,shippinglistid,locationid,TouchedBy,TouchedAt) VALUES (6,3,8,'root@localhost','2014-04-10 14:04:51.');

INSERT INTO Crossreferencefile (mediaid,printedID,shippinglistid,lotnumber,cvv,electronicid) VALUES (1,1000002506,1,1,'376',1267153507068288);
INSERT INTO Crossreferencefile (mediaid,printedID,shippinglistid,lotnumber,cvv,electronicid) VALUES (3,1000002507,2,2,'296',1260418998348160);
INSERT INTO Crossreferencefile (mediaid,printedID,shippinglistid,lotnumber,cvv,electronicid) VALUES (1,1000002526,1,4,'123',1244138246713728);
INSERT INTO Crossreferencefile (mediaid,printedID,shippinglistid,lotnumber,cvv,electronicid) VALUES (4,1000002529,3,3,'906',1259067553034624);

