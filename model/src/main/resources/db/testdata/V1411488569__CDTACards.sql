/**************************************Cards Number 1**************************************/
INSERT INTO crossreferencefile
(WalletTypeID, PrintedID, ShippingListID, LotNumber, CVV, ElectronicID, CreatedBy, TouchedBy)
VALUES
(3,/*WalletTypeID*/
'1234560001',/*PrintedID*/
1,/*ShippingListID*/
1,/*LotNumber*/
123,/*CVV*/
'1234567891320001', /*ElectronicID*/
'root@localhost',
'root@localhost');

/*CREATE Wallet*/
INSERT INTO wallets
(WalletID,
PersonID,
DateExpires,
LoadSequence,
PurposeID,
WalletStatusID,
WalletTypeID,
FarecodeID,
TouchedBy,
CreatedBy)
VALUES
((select max(w.walletId) + 1 from wallets w),
null,
'2017-12-22 00:00:00.',
2,
(select PurposeID from Purposes where DESCRIPTION like '%normal%'),
(SELECT walletstatusid FROM walletstatus where DESCRIPTION like '%Active%'),
(SELECT wallettypeid FROM wallettypes where DESCRIPTION like '%Desfire%'),
(select farecodeid from Farecodes where DESCRIPTION like '%Full%'),
'root@localhost',
'root@localhost');

/*CREATE WalletIdentifiers*/
INSERT INTO walletidentifiers (WalletID,IdentifierID,Identifier,CreatedBy,TouchedBy) 
VALUES 
((select max(walletId) from wallets),1,'1234567891320001','root@localhost','root@localhost');

/*CREATE Stored Value walletcontents*/
INSERT INTO walletcontents
(WalletContentsID,
WalletID,
Slot,
TicketID,
datestart,
DateExpires,
DateFirstUse,
DateLastUse,
ValueOriginal,
ValueRemaining,
ValueUse,
PendingCount,
SlotStatus,
TouchedBy,
CreatedBy)
VALUES
((select max(wc.WalletContentsID) + 1 from walletcontents wc),
(select max(walletId) from wallets),
1,/*Slot*/
(SELECT ticketID FROM tickets where DESCRIPTION like '%variable store%'),
null, /*datestart*/
null,/*DateExpires*/
'2014-09-23 00:00:00.',/*DateFirstUse*/
'2014-09-23 00:00:00.',/*DateLastUse*/
50,/*ori val*/
45,/*remai val*/
0,
null,
0,/*slot status*/
'root@localhost',
'root@localhost');

/*CREATE 5 D ROLL 31 BASE walletcontents*/
INSERT INTO walletcontents
(WalletContentsID,
WalletID,
Slot,
TicketID,
datestart,
DateExpires,
DateFirstUse,
DateLastUse,
ValueOriginal,
ValueRemaining,
ValueUse,
PendingCount,
SlotStatus,
TouchedBy,
CreatedBy)
VALUES
((select max(wc.WalletContentsID) + 1 from walletcontents wc),
(select max(walletId) from wallets),
2,/*Slot*/
(SELECT ticketID FROM tickets where DESCRIPTION like '%Weekday Full Fare 31 Day%'),
'2014-09-23 00:00:00.', /*datestart*/
'2014-10-23 00:00:00.',/*DateExpires*/
null,/*DateFirstUse*/
null,/*DateLastUse*/
31,/*ori val*/
0,/*remai val*/
0,
null,
0,/*slot status*/
'root@localhost',
'root@localhost');

/**************************************Cards Number 2**************************************/
INSERT INTO crossreferencefile
(WalletTypeID, PrintedID, ShippingListID, LotNumber, CVV, ElectronicID, CreatedBy, TouchedBy)
VALUES
(3,/*WalletTypeID*/
'1234560002',/*PrintedID*/
1,/*ShippingListID*/
1,/*LotNumber*/
123,/*CVV*/
'1234567891320002', /*ElectronicID*/
'root@localhost',
'root@localhost');

/*CREATE Wallet*/
INSERT INTO wallets
(WalletID,
PersonID,
DateExpires,
LoadSequence,
PurposeID,
WalletStatusID,
WalletTypeID,
FarecodeID,
TouchedBy,
CreatedBy)
VALUES
((select max(w.walletId) + 1 from wallets w),
null,
'2017-12-22 00:00:00.',
2,
(select PurposeID from Purposes where DESCRIPTION like '%normal%'),
(SELECT walletstatusid FROM walletstatus where DESCRIPTION like '%Active%'),
(SELECT wallettypeid FROM wallettypes where DESCRIPTION like '%Desfire%'),
(select farecodeid from Farecodes where DESCRIPTION like '%Full%'),
'root@localhost',
'root@localhost');

/*CREATE WalletIdentifiers*/
INSERT INTO walletidentifiers (WalletID,IdentifierID,Identifier,CreatedBy,TouchedBy) 
VALUES 
((select max(walletId) from wallets),1,'1234567891320002','root@localhost','root@localhost');

/*CREATE Stored Value walletcontents*/
INSERT INTO walletcontents
(WalletContentsID,
WalletID,
Slot,
TicketID,
datestart,
DateExpires,
DateFirstUse,
DateLastUse,
ValueOriginal,
ValueRemaining,
ValueUse,
PendingCount,
SlotStatus,
TouchedBy,
CreatedBy)
VALUES
((select max(wc.WalletContentsID) + 1 from walletcontents wc),
(select max(walletId) from wallets),
1,/*Slot*/
(SELECT ticketID FROM tickets where DESCRIPTION like '%variable store%'),
null, /*datestart*/
null,/*DateExpires*/
'2014-09-23 00:00:00.',/*DateFirstUse*/
'2014-09-23 00:00:00.',/*DateLastUse*/
50,/*ori val*/
45,/*remai val*/
0,
null,
0,/*slot status*/
'root@localhost',
'root@localhost');

/**************************************Cards Number 3**************************************/
INSERT INTO crossreferencefile
(WalletTypeID, PrintedID, ShippingListID, LotNumber, CVV, ElectronicID, CreatedBy, TouchedBy)
VALUES
(3,/*WalletTypeID*/
'1234560003',/*PrintedID*/
1,/*ShippingListID*/
1,/*LotNumber*/
123,/*CVV*/
'1234567891320003', /*ElectronicID*/
'root@localhost',
'root@localhost');

/*CREATE Wallet*/
INSERT INTO wallets
(WalletID,
PersonID,
DateExpires,
LoadSequence,
PurposeID,
WalletStatusID,
WalletTypeID,
FarecodeID,
TouchedBy,
CreatedBy)
VALUES
((select max(w.walletId) + 1 from wallets w),
null,
'2017-12-22 00:00:00.',
2,
(select PurposeID from Purposes where DESCRIPTION like '%normal%'),
(SELECT walletstatusid FROM walletstatus where DESCRIPTION like '%Active%'),
(SELECT wallettypeid FROM wallettypes where DESCRIPTION like '%Desfire%'),
(select farecodeid from Farecodes where DESCRIPTION like '%Full%'),
'root@localhost',
'root@localhost');

/*CREATE WalletIdentifiers*/
INSERT INTO walletidentifiers (WalletID,IdentifierID,Identifier,CreatedBy,TouchedBy) 
VALUES 
((select max(walletId) from wallets),1,'1234567891320003','root@localhost','root@localhost');

/*CREATE Stored Value walletcontents*/
INSERT INTO walletcontents
(WalletContentsID,
WalletID,
Slot,
TicketID,
datestart,
DateExpires,
DateFirstUse,
DateLastUse,
ValueOriginal,
ValueRemaining,
ValueUse,
PendingCount,
SlotStatus,
TouchedBy,
CreatedBy)
VALUES
((select max(wc.WalletContentsID) + 1 from walletcontents wc),
(select max(walletId) from wallets),
1,/*Slot*/
(SELECT ticketID FROM tickets where DESCRIPTION like '%variable store%'),
null, /*datestart*/
null,/*DateExpires*/
'2014-09-23 00:00:00.',/*DateFirstUse*/
'2014-09-23 00:00:00.',/*DateLastUse*/
50,/*ori val*/
45,/*remai val*/
0,
null,
0,/*slot status*/
'root@localhost',
'root@localhost');

/*CREATE 5 D ROLL 31 BASE walletcontents ACTIVE*/
INSERT INTO walletcontents
(WalletContentsID,
WalletID,
Slot,
TicketID,
datestart,
DateExpires,
DateFirstUse,
DateLastUse,
ValueOriginal,
ValueRemaining,
ValueUse,
PendingCount,
SlotStatus,
TouchedBy,
CreatedBy)
VALUES
((select max(wc.WalletContentsID) + 1 from walletcontents wc),
(select max(walletId) from wallets),
2,/*Slot*/
(SELECT ticketID FROM tickets where DESCRIPTION like '%Weekday Full Fare 31 Day%'),
'2014-09-23 00:00:00.', /*datestart*/
'2014-10-23 00:00:00.',/*DateExpires*/
null,/*DateFirstUse*/
null,/*DateLastUse*/
31,/*ori val*/
0,/*remai val*/
0,
null,
0,/*slot status*/
'root@localhost',
'root@localhost');

/*CREATE 5 D ROLL 31 BASE walletcontents INACTIVE*/
INSERT INTO walletcontents
(WalletContentsID,
WalletID,
Slot,
TicketID,
datestart,
DateExpires,
DateFirstUse,
DateLastUse,
ValueOriginal,
ValueRemaining,
ValueUse,
PendingCount,
SlotStatus,
TouchedBy,
CreatedBy)
VALUES
((select max(wc.WalletContentsID) + 1 from walletcontents wc),
(select max(walletId) from wallets),
2,/*Slot*/
(SELECT ticketID FROM tickets where DESCRIPTION like '%Weekday Full Fare 31 Day%'),
null, /*datestart*/
null,/*DateExpires*/
null,/*DateFirstUse*/
null,/*DateLastUse*/
31,/*ori val*/
0,/*remai val*/
0,
null,
0,/*slot status*/
'root@localhost',
'root@localhost');

/**************************************Cards Number 4**************************************/
INSERT INTO crossreferencefile
(WalletTypeID, PrintedID, ShippingListID, LotNumber, CVV, ElectronicID, CreatedBy, TouchedBy)
VALUES
(3,/*WalletTypeID*/
'1234560004',/*PrintedID*/
1,/*ShippingListID*/
1,/*LotNumber*/
123,/*CVV*/
'1234567891320004', /*ElectronicID*/
'root@localhost',
'root@localhost');

/*CREATE Wallet*/
INSERT INTO wallets
(WalletID,
PersonID,
DateExpires,
LoadSequence,
PurposeID,
WalletStatusID,
WalletTypeID,
FarecodeID,
TouchedBy,
CreatedBy)
VALUES
((select max(w.walletId) + 1 from wallets w),
null,
'2017-12-22 00:00:00.',
2,
(select PurposeID from Purposes where DESCRIPTION like '%normal%'),
(SELECT walletstatusid FROM walletstatus where DESCRIPTION like '%Active%'),
(SELECT wallettypeid FROM wallettypes where DESCRIPTION like '%Desfire%'),
(select farecodeid from Farecodes where DESCRIPTION like '%Senior%'),
'root@localhost',
'root@localhost');

/*CREATE WalletIdentifiers*/
INSERT INTO walletidentifiers (WalletID,IdentifierID,Identifier,CreatedBy,TouchedBy) 
VALUES 
((select max(walletId) from wallets),1,'1234567891320004','root@localhost','root@localhost');

/*CREATE Stored Value walletcontents*/
INSERT INTO walletcontents
(WalletContentsID,
WalletID,
Slot,
TicketID,
datestart,
DateExpires,
DateFirstUse,
DateLastUse,
ValueOriginal,
ValueRemaining,
ValueUse,
PendingCount,
SlotStatus,
TouchedBy,
CreatedBy)
VALUES
((select max(wc.WalletContentsID) + 1 from walletcontents wc),
(select max(walletId) from wallets),
1,/*Slot*/
(SELECT ticketID FROM tickets where DESCRIPTION like '%variable store%'), /*this should be 'Stored Value HALF FARE'*/
null, /*datestart*/
null,/*DateExpires*/
'2014-09-23 00:00:00.',/*DateFirstUse*/
'2014-09-23 00:00:00.',/*DateLastUse*/
50,/*ori val*/
45,/*remai val*/
0,
null,
0,/*slot status*/
'root@localhost',
'root@localhost');

/*CREATE 5 D ROLL 31 BASE walletcontents*/
INSERT INTO walletcontents
(WalletContentsID,
WalletID,
Slot,
TicketID,
datestart,
DateExpires,
DateFirstUse,
DateLastUse,
ValueOriginal,
ValueRemaining,
ValueUse,
PendingCount,
SlotStatus,
TouchedBy,
CreatedBy)
VALUES
((select max(wc.WalletContentsID) + 1 from walletcontents wc),
(select max(walletId) from wallets),
2,/*Slot*/
(SELECT ticketID FROM tickets where DESCRIPTION like '%Weekday half Fare 31 Day%'),
'2014-09-23 00:00:00.', /*datestart*/
'2014-10-23 00:00:00.',/*DateExpires*/
null,/*DateFirstUse*/
null,/*DateLastUse*/
31,/*ori val*/
0,/*remai val*/
0,
null,
0,/*slot status*/
'root@localhost',
'root@localhost');

/**************************************Cards Number 5**************************************/
INSERT INTO crossreferencefile
(WalletTypeID, PrintedID, ShippingListID, LotNumber, CVV, ElectronicID, CreatedBy, TouchedBy)
VALUES
(3,/*WalletTypeID*/
'1234560005',/*PrintedID*/
1,/*ShippingListID*/
1,/*LotNumber*/
123,/*CVV*/
'1234567891320005', /*ElectronicID*/
'root@localhost',
'root@localhost');

/*CREATE Wallet*/
INSERT INTO wallets
(WalletID,
PersonID,
DateExpires,
LoadSequence,
PurposeID,
WalletStatusID,
WalletTypeID,
FarecodeID,
TouchedBy,
CreatedBy)
VALUES
((select max(w.walletId) + 1 from wallets w),
null,
'2017-12-22 00:00:00.',
2,
(select PurposeID from Purposes where DESCRIPTION like '%normal%'),
(SELECT walletstatusid FROM walletstatus where DESCRIPTION like '%Active%'),
(SELECT wallettypeid FROM wallettypes where DESCRIPTION like '%Desfire%'),
(select farecodeid from Farecodes where DESCRIPTION like '%Senior%'),
'root@localhost',
'root@localhost');

/*CREATE WalletIdentifiers*/
INSERT INTO walletidentifiers (WalletID,IdentifierID,Identifier,CreatedBy,TouchedBy) 
VALUES 
((select max(walletId) from wallets),1,'1234567891320005','root@localhost','root@localhost');

/*CREATE Stored Value walletcontents*/
INSERT INTO walletcontents
(WalletContentsID,
WalletID,
Slot,
TicketID,
datestart,
DateExpires,
DateFirstUse,
DateLastUse,
ValueOriginal,
ValueRemaining,
ValueUse,
PendingCount,
SlotStatus,
TouchedBy,
CreatedBy)
VALUES
((select max(wc.WalletContentsID) + 1 from walletcontents wc),
(select max(walletId) from wallets),
1,/*Slot*/
(SELECT ticketID FROM tickets where DESCRIPTION like '%variable store%'), /*this should be 'Stored Value HALF FARE'*/
null, /*datestart*/
null,/*DateExpires*/
'2014-09-23 00:00:00.',/*DateFirstUse*/
'2014-09-23 00:00:00.',/*DateLastUse*/
50,/*ori val*/
45,/*remai val*/
0,
null,
0,/*slot status*/
'root@localhost',
'root@localhost');

/**************************************Cards Number 6**************************************/
INSERT INTO crossreferencefile
(WalletTypeID, PrintedID, ShippingListID, LotNumber, CVV, ElectronicID, CreatedBy, TouchedBy)
VALUES
(3,/*WalletTypeID*/
'1234560006',/*PrintedID*/
1,/*ShippingListID*/
1,/*LotNumber*/
123,/*CVV*/
'1234567891320006', /*ElectronicID*/
'root@localhost',
'root@localhost');

/*CREATE Wallet*/
INSERT INTO wallets
(WalletID,
PersonID,
DateExpires,
LoadSequence,
PurposeID,
WalletStatusID,
WalletTypeID,
FarecodeID,
TouchedBy,
CreatedBy)
VALUES
((select max(w.walletId) + 1 from wallets w),
null,
'2017-12-22 00:00:00.',
2,
(select PurposeID from Purposes where DESCRIPTION like '%normal%'),
(SELECT walletstatusid FROM walletstatus where DESCRIPTION like '%Active%'),
(SELECT wallettypeid FROM wallettypes where DESCRIPTION like '%Desfire%'),
(select farecodeid from Farecodes where DESCRIPTION like '%Full%'),
'root@localhost',
'root@localhost');

/*CREATE WalletIdentifiers*/
INSERT INTO walletidentifiers (WalletID,IdentifierID,Identifier,CreatedBy,TouchedBy) 
VALUES 
((select max(walletId) from wallets),1,'1234567891320006','root@localhost','root@localhost');

/*CREATE Stored Value walletcontents*/
INSERT INTO walletcontents
(WalletContentsID,
WalletID,
Slot,
TicketID,
datestart,
DateExpires,
DateFirstUse,
DateLastUse,
ValueOriginal,
ValueRemaining,
ValueUse,
PendingCount,
SlotStatus,
TouchedBy,
CreatedBy)
VALUES
((select max(wc.WalletContentsID) + 1 from walletcontents wc),
(select max(walletId) from wallets),
1,/*Slot*/
(SELECT ticketID FROM tickets where DESCRIPTION like '%variable store%'),
null, /*datestart*/
null,/*DateExpires*/
'2014-09-23 00:00:00.',/*DateFirstUse*/
'2014-09-23 00:00:00.',/*DateLastUse*/
50,/*ori val*/
45,/*remai val*/
0,
null,
0,/*slot status*/
'root@localhost',
'root@localhost');

/*CREATE 5 D ROLL 31 BASE walletcontents*/
INSERT INTO walletcontents
(WalletContentsID,
WalletID,
Slot,
TicketID,
datestart,
DateExpires,
DateFirstUse,
DateLastUse,
ValueOriginal,
ValueRemaining,
ValueUse,
PendingCount,
SlotStatus,
TouchedBy,
CreatedBy)
VALUES
((select max(wc.WalletContentsID) + 1 from walletcontents wc),
(select max(walletId) from wallets),
2,/*Slot*/
(SELECT ticketID FROM tickets where DESCRIPTION like '%Weekday Full Fare 31 Day%'),
'2014-09-23 00:00:00.', /*datestart*/
'2014-10-23 00:00:00.',/*DateExpires*/
null,/*DateFirstUse*/
null,/*DateLastUse*/
31,/*ori val*/
0,/*remai val*/
0,
null,
0,/*slot status*/
'root@localhost',
'root@localhost');

/**************************************Cards Number 7**************************************/
INSERT INTO crossreferencefile
(WalletTypeID, PrintedID, ShippingListID, LotNumber, CVV, ElectronicID, CreatedBy, TouchedBy)
VALUES
(3,/*WalletTypeID*/
'1234560007',/*PrintedID*/
1,/*ShippingListID*/
1,/*LotNumber*/
123,/*CVV*/
'1234567891320007', /*ElectronicID*/
'root@localhost',
'root@localhost');

/*CREATE Wallet*/
INSERT INTO wallets
(WalletID,
PersonID,
DateExpires,
LoadSequence,
PurposeID,
WalletStatusID,
WalletTypeID,
FarecodeID,
TouchedBy,
CreatedBy)
VALUES
((select max(w.walletId) + 1 from wallets w),
null,
'2017-12-22 00:00:00.',
2,
(select PurposeID from Purposes where DESCRIPTION like '%normal%'),
(SELECT walletstatusid FROM walletstatus where DESCRIPTION like '%Active%'),
(SELECT wallettypeid FROM wallettypes where DESCRIPTION like '%Desfire%'),
(select farecodeid from Farecodes where DESCRIPTION like '%Full%'),
'root@localhost',
'root@localhost');

/*CREATE WalletIdentifiers*/
INSERT INTO walletidentifiers (WalletID,IdentifierID,Identifier,CreatedBy,TouchedBy) 
VALUES 
((select max(walletId) from wallets),1,'1234567891320007','root@localhost','root@localhost');

/*CREATE Stored Value walletcontents*/
INSERT INTO walletcontents
(WalletContentsID,
WalletID,
Slot,
TicketID,
datestart,
DateExpires,
DateFirstUse,
DateLastUse,
ValueOriginal,
ValueRemaining,
ValueUse,
PendingCount,
SlotStatus,
TouchedBy,
CreatedBy)
VALUES
((select max(wc.WalletContentsID) + 1 from walletcontents wc),
(select max(walletId) from wallets),
1,/*Slot*/
(SELECT ticketID FROM tickets where DESCRIPTION like '%variable store%'),
null, /*datestart*/
null,/*DateExpires*/
'2014-09-23 00:00:00.',/*DateFirstUse*/
'2014-09-23 00:00:00.',/*DateLastUse*/
50,/*ori val*/
45,/*remai val*/
0,
null,
0,/*slot status*/
'root@localhost',
'root@localhost');

/**************************************Cards Number 8**************************************/
INSERT INTO crossreferencefile
(WalletTypeID, PrintedID, ShippingListID, LotNumber, CVV, ElectronicID, CreatedBy, TouchedBy)
VALUES
(3,/*WalletTypeID*/
'1234560008',/*PrintedID*/
1,/*ShippingListID*/
1,/*LotNumber*/
123,/*CVV*/
'1234567891320008', /*ElectronicID*/
'root@localhost',
'root@localhost');

/*CREATE Wallet*/
INSERT INTO wallets
(WalletID,
PersonID,
DateExpires,
LoadSequence,
PurposeID,
WalletStatusID,
WalletTypeID,
FarecodeID,
TouchedBy,
CreatedBy)
VALUES
((select max(w.walletId) + 1 from wallets w),
null,
'2017-12-22 00:00:00.',
2,
(select PurposeID from Purposes where DESCRIPTION like '%normal%'),
(SELECT walletstatusid FROM walletstatus where DESCRIPTION like '%Active%'),
(SELECT wallettypeid FROM wallettypes where DESCRIPTION like '%Desfire%'),
(select farecodeid from Farecodes where DESCRIPTION like '%Full%'),
'root@localhost',
'root@localhost');

/*CREATE WalletIdentifiers*/
INSERT INTO walletidentifiers (WalletID,IdentifierID,Identifier,CreatedBy,TouchedBy) 
VALUES 
((select max(walletId) from wallets),1,'1234567891320008','root@localhost','root@localhost');

/*CREATE Stored Value walletcontents*/
INSERT INTO walletcontents
(WalletContentsID,
WalletID,
Slot,
TicketID,
datestart,
DateExpires,
DateFirstUse,
DateLastUse,
ValueOriginal,
ValueRemaining,
ValueUse,
PendingCount,
SlotStatus,
TouchedBy,
CreatedBy)
VALUES
((select max(wc.WalletContentsID) + 1 from walletcontents wc),
(select max(walletId) from wallets),
1,/*Slot*/
(SELECT ticketID FROM tickets where DESCRIPTION like '%variable store%'),
null, /*datestart*/
null,/*DateExpires*/
'2014-09-23 00:00:00.',/*DateFirstUse*/
'2014-09-23 00:00:00.',/*DateLastUse*/
50,/*ori val*/
45,/*remai val*/
0,
null,
0,/*slot status*/
'root@localhost',
'root@localhost');

/*CREATE 5 D ROLL 31 BASE walletcontents ACTIVE*/
INSERT INTO walletcontents
(WalletContentsID,
WalletID,
Slot,
TicketID,
datestart,
DateExpires,
DateFirstUse,
DateLastUse,
ValueOriginal,
ValueRemaining,
ValueUse,
PendingCount,
SlotStatus,
TouchedBy,
CreatedBy)
VALUES
((select max(wc.WalletContentsID) + 1 from walletcontents wc),
(select max(walletId) from wallets),
2,/*Slot*/
(SELECT ticketID FROM tickets where DESCRIPTION like '%Weekday Full Fare 31 Day%'),
'2014-09-23 00:00:00.', /*datestart*/
'2014-10-23 00:00:00.',/*DateExpires*/
null,/*DateFirstUse*/
null,/*DateLastUse*/
31,/*ori val*/
0,/*remai val*/
0,
null,
0,/*slot status*/
'root@localhost',
'root@localhost');

/*CREATE 5 D ROLL 31 BASE walletcontents INACTIVE*/
INSERT INTO walletcontents
(WalletContentsID,
WalletID,
Slot,
TicketID,
datestart,
DateExpires,
DateFirstUse,
DateLastUse,
ValueOriginal,
ValueRemaining,
ValueUse,
PendingCount,
SlotStatus,
TouchedBy,
CreatedBy)
VALUES
((select max(wc.WalletContentsID) + 1 from walletcontents wc),
(select max(walletId) from wallets),
2,/*Slot*/
(SELECT ticketID FROM tickets where DESCRIPTION like '%Weekday Full Fare 31 Day%'),
null, /*datestart*/
null,/*DateExpires*/
null,/*DateFirstUse*/
null,/*DateLastUse*/
31,/*ori val*/
0,/*remai val*/
0,
null,
0,/*slot status*/
'root@localhost',
'root@localhost');

/**************************************Cards Number 9**************************************/
INSERT INTO crossreferencefile
(WalletTypeID, PrintedID, ShippingListID, LotNumber, CVV, ElectronicID, CreatedBy, TouchedBy)
VALUES
(3,/*WalletTypeID*/
'1234560009',/*PrintedID*/
1,/*ShippingListID*/
1,/*LotNumber*/
123,/*CVV*/
'1234567891320009', /*ElectronicID*/
'root@localhost',
'root@localhost');

/*CREATE Wallet*/
INSERT INTO wallets
(WalletID,
PersonID,
DateExpires,
LoadSequence,
PurposeID,
WalletStatusID,
WalletTypeID,
FarecodeID,
TouchedBy,
CreatedBy)
VALUES
((select max(w.walletId) + 1 from wallets w),
null,
'2017-12-22 00:00:00.',
2,
(select PurposeID from Purposes where DESCRIPTION like '%normal%'),
(SELECT walletstatusid FROM walletstatus where DESCRIPTION like '%Active%'),
(SELECT wallettypeid FROM wallettypes where DESCRIPTION like '%Desfire%'),
(select farecodeid from Farecodes where DESCRIPTION like '%Senior%'),
'root@localhost',
'root@localhost');

/*CREATE WalletIdentifiers*/
INSERT INTO walletidentifiers (WalletID,IdentifierID,Identifier,CreatedBy,TouchedBy) 
VALUES 
((select max(walletId) from wallets),1,'1234567891320009','root@localhost','root@localhost');

/*CREATE Stored Value walletcontents*/
INSERT INTO walletcontents
(WalletContentsID,
WalletID,
Slot,
TicketID,
datestart,
DateExpires,
DateFirstUse,
DateLastUse,
ValueOriginal,
ValueRemaining,
ValueUse,
PendingCount,
SlotStatus,
TouchedBy,
CreatedBy)
VALUES
((select max(wc.WalletContentsID) + 1 from walletcontents wc),
(select max(walletId) from wallets),
1,/*Slot*/
(SELECT ticketID FROM tickets where DESCRIPTION like '%variable store%'), /*this should be 'Stored Value HALF FARE'*/
null, /*datestart*/
null,/*DateExpires*/
'2014-09-23 00:00:00.',/*DateFirstUse*/
'2014-09-23 00:00:00.',/*DateLastUse*/
50,/*ori val*/
45,/*remai val*/
0,
null,
0,/*slot status*/
'root@localhost',
'root@localhost');

/*CREATE 5 D ROLL 31 BASE walletcontents*/
INSERT INTO walletcontents
(WalletContentsID,
WalletID,
Slot,
TicketID,
datestart,
DateExpires,
DateFirstUse,
DateLastUse,
ValueOriginal,
ValueRemaining,
ValueUse,
PendingCount,
SlotStatus,
TouchedBy,
CreatedBy)
VALUES
((select max(wc.WalletContentsID) + 1 from walletcontents wc),
(select max(walletId) from wallets),
2,/*Slot*/
(SELECT ticketID FROM tickets where DESCRIPTION like '%Weekday half Fare 31 Day%'),
'2014-09-23 00:00:00.', /*datestart*/
'2014-10-23 00:00:00.',/*DateExpires*/
null,/*DateFirstUse*/
null,/*DateLastUse*/
31,/*ori val*/
0,/*remai val*/
0,
null,
0,/*slot status*/
'root@localhost',
'root@localhost');

/**************************************Cards Number 10**************************************/
INSERT INTO crossreferencefile
(WalletTypeID, PrintedID, ShippingListID, LotNumber, CVV, ElectronicID, CreatedBy, TouchedBy)
VALUES
(3,/*WalletTypeID*/
'1234560010',/*PrintedID*/
1,/*ShippingListID*/
1,/*LotNumber*/
123,/*CVV*/
'1234567891320010', /*ElectronicID*/
'root@localhost',
'root@localhost');

/*CREATE Wallet*/
INSERT INTO wallets
(WalletID,
PersonID,
DateExpires,
LoadSequence,
PurposeID,
WalletStatusID,
WalletTypeID,
FarecodeID,
TouchedBy,
CreatedBy)
VALUES
((select max(w.walletId) + 1 from wallets w),
null,
'2017-12-22 00:00:00.',
2,
(select PurposeID from Purposes where DESCRIPTION like '%normal%'),
(SELECT walletstatusid FROM walletstatus where DESCRIPTION like '%Active%'),
(SELECT wallettypeid FROM wallettypes where DESCRIPTION like '%Desfire%'),
(select farecodeid from Farecodes where DESCRIPTION like '%Senior%'),
'root@localhost',
'root@localhost');

/*CREATE WalletIdentifiers*/
INSERT INTO walletidentifiers (WalletID,IdentifierID,Identifier,CreatedBy,TouchedBy) 
VALUES 
((select max(walletId) from wallets),1,'1234567891320010','root@localhost','root@localhost');

/*CREATE Stored Value walletcontents*/
INSERT INTO walletcontents
(WalletContentsID,
WalletID,
Slot,
TicketID,
datestart,
DateExpires,
DateFirstUse,
DateLastUse,
ValueOriginal,
ValueRemaining,
ValueUse,
PendingCount,
SlotStatus,
TouchedBy,
CreatedBy)
VALUES
((select max(wc.WalletContentsID) + 1 from walletcontents wc),
(select max(walletId) from wallets),
1,/*Slot*/
(SELECT ticketID FROM tickets where DESCRIPTION like '%variable store%'), /*this should be 'Stored Value HALF FARE'*/
null, /*datestart*/
null,/*DateExpires*/
'2014-09-23 00:00:00.',/*DateFirstUse*/
'2014-09-23 00:00:00.',/*DateLastUse*/
50,/*ori val*/
45,/*remai val*/
0,
null,
0,/*slot status*/
'root@localhost',
'root@localhost');

/**************************************Cards Number 11**************************************/
INSERT INTO crossreferencefile
(WalletTypeID, PrintedID, ShippingListID, LotNumber, CVV, ElectronicID, CreatedBy, TouchedBy)
VALUES
(3,/*WalletTypeID*/
'1234560011',/*PrintedID*/
1,/*ShippingListID*/
1,/*LotNumber*/
123,/*CVV*/
'1234567891320011', /*ElectronicID*/
'root@localhost',
'root@localhost');

/*CREATE Wallet*/
INSERT INTO wallets
(WalletID,
PersonID,
DateExpires,
LoadSequence,
PurposeID,
WalletStatusID,
WalletTypeID,
FarecodeID,
TouchedBy,
CreatedBy)
VALUES
((select max(w.walletId) + 1 from wallets w),
null,
'2017-12-22 00:00:00.',
2,
(select PurposeID from Purposes where DESCRIPTION like '%normal%'),
(SELECT walletstatusid FROM walletstatus where DESCRIPTION like '%Active%'),
(SELECT wallettypeid FROM wallettypes where DESCRIPTION like '%Desfire%'),
(select farecodeid from Farecodes where DESCRIPTION like '%Full%'),
'root@localhost',
'root@localhost');

/*CREATE WalletIdentifiers*/
INSERT INTO walletidentifiers (WalletID,IdentifierID,Identifier,CreatedBy,TouchedBy) 
VALUES 
((select max(walletId) from wallets),1,'1234567891320011','root@localhost','root@localhost');

/*CREATE Stored Value walletcontents*/
INSERT INTO walletcontents
(WalletContentsID,
WalletID,
Slot,
TicketID,
datestart,
DateExpires,
DateFirstUse,
DateLastUse,
ValueOriginal,
ValueRemaining,
ValueUse,
PendingCount,
SlotStatus,
TouchedBy,
CreatedBy)
VALUES
((select max(wc.WalletContentsID) + 1 from walletcontents wc),
(select max(walletId) from wallets),
1,/*Slot*/
(SELECT ticketID FROM tickets where DESCRIPTION like '%variable store%'),
null, /*datestart*/
null,/*DateExpires*/
'2014-09-23 00:00:00.',/*DateFirstUse*/
'2014-09-23 00:00:00.',/*DateLastUse*/
50,/*ori val*/
45,/*remai val*/
0,
null,
0,/*slot status*/
'root@localhost',
'root@localhost');

/*CREATE 5 D ROLL 31 BASE walletcontents*/
INSERT INTO walletcontents
(WalletContentsID,
WalletID,
Slot,
TicketID,
datestart,
DateExpires,
DateFirstUse,
DateLastUse,
ValueOriginal,
ValueRemaining,
ValueUse,
PendingCount,
SlotStatus,
TouchedBy,
CreatedBy)
VALUES
((select max(wc.WalletContentsID) + 1 from walletcontents wc),
(select max(walletId) from wallets),
2,/*Slot*/
(SELECT ticketID FROM tickets where DESCRIPTION like '%Weekday Full Fare 31 Day%'),
'2014-09-23 00:00:00.', /*datestart*/
'2014-10-23 00:00:00.',/*DateExpires*/
null,/*DateFirstUse*/
null,/*DateLastUse*/
31,/*ori val*/
0,/*remai val*/
0,
null,
0,/*slot status*/
'root@localhost',
'root@localhost');

/**************************************Cards Number 12**************************************/
INSERT INTO crossreferencefile
(WalletTypeID, PrintedID, ShippingListID, LotNumber, CVV, ElectronicID, CreatedBy, TouchedBy)
VALUES
(3,/*WalletTypeID*/
'1234560012',/*PrintedID*/
1,/*ShippingListID*/
1,/*LotNumber*/
123,/*CVV*/
'1234567891320012', /*ElectronicID*/
'root@localhost',
'root@localhost');

/*CREATE Wallet*/
INSERT INTO wallets
(WalletID,
PersonID,
DateExpires,
LoadSequence,
PurposeID,
WalletStatusID,
WalletTypeID,
FarecodeID,
TouchedBy,
CreatedBy)
VALUES
((select max(w.walletId) + 1 from wallets w),
null,
'2017-12-22 00:00:00.',
2,
(select PurposeID from Purposes where DESCRIPTION like '%normal%'),
(SELECT walletstatusid FROM walletstatus where DESCRIPTION like '%Active%'),
(SELECT wallettypeid FROM wallettypes where DESCRIPTION like '%Desfire%'),
(select farecodeid from Farecodes where DESCRIPTION like '%Full%'),
'root@localhost',
'root@localhost');

/*CREATE WalletIdentifiers*/
INSERT INTO walletidentifiers (WalletID,IdentifierID,Identifier,CreatedBy,TouchedBy) 
VALUES 
((select max(walletId) from wallets),1,'1234567891320012','root@localhost','root@localhost');

/*CREATE Stored Value walletcontents*/
INSERT INTO walletcontents
(WalletContentsID,
WalletID,
Slot,
TicketID,
datestart,
DateExpires,
DateFirstUse,
DateLastUse,
ValueOriginal,
ValueRemaining,
ValueUse,
PendingCount,
SlotStatus,
TouchedBy,
CreatedBy)
VALUES
((select max(wc.WalletContentsID) + 1 from walletcontents wc),
(select max(walletId) from wallets),
1,/*Slot*/
(SELECT ticketID FROM tickets where DESCRIPTION like '%variable store%'),
null, /*datestart*/
null,/*DateExpires*/
'2014-09-23 00:00:00.',/*DateFirstUse*/
'2014-09-23 00:00:00.',/*DateLastUse*/
50,/*ori val*/
45,/*remai val*/
0,
null,
0,/*slot status*/
'root@localhost',
'root@localhost');

/**************************************Cards Number 13**************************************/
INSERT INTO crossreferencefile
(WalletTypeID, PrintedID, ShippingListID, LotNumber, CVV, ElectronicID, CreatedBy, TouchedBy)
VALUES
(3,/*WalletTypeID*/
'1234560013',/*PrintedID*/
1,/*ShippingListID*/
1,/*LotNumber*/
123,/*CVV*/
'1234567891320013', /*ElectronicID*/
'root@localhost',
'root@localhost');

/*CREATE Wallet*/
INSERT INTO wallets
(WalletID,
PersonID,
DateExpires,
LoadSequence,
PurposeID,
WalletStatusID,
WalletTypeID,
FarecodeID,
TouchedBy,
CreatedBy)
VALUES
((select max(w.walletId) + 1 from wallets w),
null,
'2017-12-22 00:00:00.',
2,
(select PurposeID from Purposes where DESCRIPTION like '%normal%'),
(SELECT walletstatusid FROM walletstatus where DESCRIPTION like '%Active%'),
(SELECT wallettypeid FROM wallettypes where DESCRIPTION like '%Desfire%'),
(select farecodeid from Farecodes where DESCRIPTION like '%Full%'),
'root@localhost',
'root@localhost');

/*CREATE WalletIdentifiers*/
INSERT INTO walletidentifiers (WalletID,IdentifierID,Identifier,CreatedBy,TouchedBy) 
VALUES 
((select max(walletId) from wallets),1,'1234567891320013','root@localhost','root@localhost');

/*CREATE Stored Value walletcontents*/
INSERT INTO walletcontents
(WalletContentsID,
WalletID,
Slot,
TicketID,
datestart,
DateExpires,
DateFirstUse,
DateLastUse,
ValueOriginal,
ValueRemaining,
ValueUse,
PendingCount,
SlotStatus,
TouchedBy,
CreatedBy)
VALUES
((select max(wc.WalletContentsID) + 1 from walletcontents wc),
(select max(walletId) from wallets),
1,/*Slot*/
(SELECT ticketID FROM tickets where DESCRIPTION like '%variable store%'),
null, /*datestart*/
null,/*DateExpires*/
'2014-09-23 00:00:00.',/*DateFirstUse*/
'2014-09-23 00:00:00.',/*DateLastUse*/
50,/*ori val*/
45,/*remai val*/
0,
null,
0,/*slot status*/
'root@localhost',
'root@localhost');

/*CREATE 5 D ROLL 31 BASE walletcontents ACTIVE*/
INSERT INTO walletcontents
(WalletContentsID,
WalletID,
Slot,
TicketID,
datestart,
DateExpires,
DateFirstUse,
DateLastUse,
ValueOriginal,
ValueRemaining,
ValueUse,
PendingCount,
SlotStatus,
TouchedBy,
CreatedBy)
VALUES
((select max(wc.WalletContentsID) + 1 from walletcontents wc),
(select max(walletId) from wallets),
2,/*Slot*/
(SELECT ticketID FROM tickets where DESCRIPTION like '%Weekday Full Fare 31 Day%'),
'2014-09-23 00:00:00.', /*datestart*/
'2014-10-23 00:00:00.',/*DateExpires*/
null,/*DateFirstUse*/
null,/*DateLastUse*/
31,/*ori val*/
0,/*remai val*/
0,
null,
0,/*slot status*/
'root@localhost',
'root@localhost');

/*CREATE 5 D ROLL 31 BASE walletcontents INACTIVE*/
INSERT INTO walletcontents
(WalletContentsID,
WalletID,
Slot,
TicketID,
datestart,
DateExpires,
DateFirstUse,
DateLastUse,
ValueOriginal,
ValueRemaining,
ValueUse,
PendingCount,
SlotStatus,
TouchedBy,
CreatedBy)
VALUES
((select max(wc.WalletContentsID) + 1 from walletcontents wc),
(select max(walletId) from wallets),
2,/*Slot*/
(SELECT ticketID FROM tickets where DESCRIPTION like '%Weekday Full Fare 31 Day%'),
null, /*datestart*/
null,/*DateExpires*/
null,/*DateFirstUse*/
null,/*DateLastUse*/
31,/*ori val*/
0,/*remai val*/
0,
null,
0,/*slot status*/
'root@localhost',
'root@localhost');

/**************************************Cards Number 14**************************************/
INSERT INTO crossreferencefile
(WalletTypeID, PrintedID, ShippingListID, LotNumber, CVV, ElectronicID, CreatedBy, TouchedBy)
VALUES
(3,/*WalletTypeID*/
'1234560014',/*PrintedID*/
1,/*ShippingListID*/
1,/*LotNumber*/
123,/*CVV*/
'1234567891320014', /*ElectronicID*/
'root@localhost',
'root@localhost');

/*CREATE Wallet*/
INSERT INTO wallets
(WalletID,
PersonID,
DateExpires,
LoadSequence,
PurposeID,
WalletStatusID,
WalletTypeID,
FarecodeID,
TouchedBy,
CreatedBy)
VALUES
((select max(w.walletId) + 1 from wallets w),
null,
'2017-12-22 00:00:00.',
2,
(select PurposeID from Purposes where DESCRIPTION like '%normal%'),
(SELECT walletstatusid FROM walletstatus where DESCRIPTION like '%Active%'),
(SELECT wallettypeid FROM wallettypes where DESCRIPTION like '%Desfire%'),
(select farecodeid from Farecodes where DESCRIPTION like '%Senior%'),
'root@localhost',
'root@localhost');

/*CREATE WalletIdentifiers*/
INSERT INTO walletidentifiers (WalletID,IdentifierID,Identifier,CreatedBy,TouchedBy) 
VALUES 
((select max(walletId) from wallets),1,'1234567891320014','root@localhost','root@localhost');

/*CREATE Stored Value walletcontents*/
INSERT INTO walletcontents
(WalletContentsID,
WalletID,
Slot,
TicketID,
datestart,
DateExpires,
DateFirstUse,
DateLastUse,
ValueOriginal,
ValueRemaining,
ValueUse,
PendingCount,
SlotStatus,
TouchedBy,
CreatedBy)
VALUES
((select max(wc.WalletContentsID) + 1 from walletcontents wc),
(select max(walletId) from wallets),
1,/*Slot*/
(SELECT ticketID FROM tickets where DESCRIPTION like '%variable store%'), /*this should be 'Stored Value HALF FARE'*/
null, /*datestart*/
null,/*DateExpires*/
'2014-09-23 00:00:00.',/*DateFirstUse*/
'2014-09-23 00:00:00.',/*DateLastUse*/
50,/*ori val*/
45,/*remai val*/
0,
null,
0,/*slot status*/
'root@localhost',
'root@localhost');

/*CREATE 5 D ROLL 31 BASE walletcontents*/
INSERT INTO walletcontents
(WalletContentsID,
WalletID,
Slot,
TicketID,
datestart,
DateExpires,
DateFirstUse,
DateLastUse,
ValueOriginal,
ValueRemaining,
ValueUse,
PendingCount,
SlotStatus,
TouchedBy,
CreatedBy)
VALUES
((select max(wc.WalletContentsID) + 1 from walletcontents wc),
(select max(walletId) from wallets),
2,/*Slot*/
(SELECT ticketID FROM tickets where DESCRIPTION like '%Weekday half Fare 31 Day%'),
'2014-09-23 00:00:00.', /*datestart*/
'2014-10-23 00:00:00.',/*DateExpires*/
null,/*DateFirstUse*/
null,/*DateLastUse*/
31,/*ori val*/
0,/*remai val*/
0,
null,
0,/*slot status*/
'root@localhost',
'root@localhost');

/**************************************Cards Number 15**************************************/
INSERT INTO crossreferencefile
(WalletTypeID, PrintedID, ShippingListID, LotNumber, CVV, ElectronicID, CreatedBy, TouchedBy)
VALUES
(3,/*WalletTypeID*/
'1234560015',/*PrintedID*/
1,/*ShippingListID*/
1,/*LotNumber*/
123,/*CVV*/
'1234567891320015', /*ElectronicID*/
'root@localhost',
'root@localhost');

/*CREATE Wallet*/
INSERT INTO wallets
(WalletID,
PersonID,
DateExpires,
LoadSequence,
PurposeID,
WalletStatusID,
WalletTypeID,
FarecodeID,
TouchedBy,
CreatedBy)
VALUES
((select max(w.walletId) + 1 from wallets w),
null,
'2017-12-22 00:00:00.',
2,
(select PurposeID from Purposes where DESCRIPTION like '%normal%'),
(SELECT walletstatusid FROM walletstatus where DESCRIPTION like '%Active%'),
(SELECT wallettypeid FROM wallettypes where DESCRIPTION like '%Desfire%'),
(select farecodeid from Farecodes where DESCRIPTION like '%Senior%'),
'root@localhost',
'root@localhost');

/*CREATE WalletIdentifiers*/
INSERT INTO walletidentifiers (WalletID,IdentifierID,Identifier,CreatedBy,TouchedBy) 
VALUES 
((select max(walletId) from wallets),1,'1234567891320015','root@localhost','root@localhost');

/*CREATE Stored Value walletcontents*/
INSERT INTO walletcontents
(WalletContentsID,
WalletID,
Slot,
TicketID,
datestart,
DateExpires,
DateFirstUse,
DateLastUse,
ValueOriginal,
ValueRemaining,
ValueUse,
PendingCount,
SlotStatus,
TouchedBy,
CreatedBy)
VALUES
((select max(wc.WalletContentsID) + 1 from walletcontents wc),
(select max(walletId) from wallets),
1,/*Slot*/
(SELECT ticketID FROM tickets where DESCRIPTION like '%variable store%'), /*this should be 'Stored Value HALF FARE'*/
null, /*datestart*/
null,/*DateExpires*/
'2014-09-23 00:00:00.',/*DateFirstUse*/
'2014-09-23 00:00:00.',/*DateLastUse*/
50,/*ori val*/
45,/*remai val*/
0,
null,
0,/*slot status*/
'root@localhost',
'root@localhost');

/**************************************Cards Number 16**************************************/
INSERT INTO crossreferencefile
(WalletTypeID, PrintedID, ShippingListID, LotNumber, CVV, ElectronicID, CreatedBy, TouchedBy)
VALUES
(3,/*WalletTypeID*/
'1234560016',/*PrintedID*/
1,/*ShippingListID*/
1,/*LotNumber*/
123,/*CVV*/
'1234567891320016', /*ElectronicID*/
'root@localhost',
'root@localhost');

/*CREATE Wallet*/
INSERT INTO wallets
(WalletID,
PersonID,
DateExpires,
LoadSequence,
PurposeID,
WalletStatusID,
WalletTypeID,
FarecodeID,
TouchedBy,
CreatedBy)
VALUES
((select max(w.walletId) + 1 from wallets w),
null,
'2017-12-22 00:00:00.',
2,
(select PurposeID from Purposes where DESCRIPTION like '%normal%'),
(SELECT walletstatusid FROM walletstatus where DESCRIPTION like '%Active%'),
(SELECT wallettypeid FROM wallettypes where DESCRIPTION like '%Desfire%'),
(select farecodeid from Farecodes where DESCRIPTION like '%Full%'),
'root@localhost',
'root@localhost');

/*CREATE WalletIdentifiers*/
INSERT INTO walletidentifiers (WalletID,IdentifierID,Identifier,CreatedBy,TouchedBy) 
VALUES 
((select max(walletId) from wallets),1,'1234567891320016','root@localhost','root@localhost');

/*CREATE Stored Value walletcontents*/
INSERT INTO walletcontents
(WalletContentsID,
WalletID,
Slot,
TicketID,
datestart,
DateExpires,
DateFirstUse,
DateLastUse,
ValueOriginal,
ValueRemaining,
ValueUse,
PendingCount,
SlotStatus,
TouchedBy,
CreatedBy)
VALUES
((select max(wc.WalletContentsID) + 1 from walletcontents wc),
(select max(walletId) from wallets),
1,/*Slot*/
(SELECT ticketID FROM tickets where DESCRIPTION like '%variable store%'),
null, /*datestart*/
null,/*DateExpires*/
'2014-09-23 00:00:00.',/*DateFirstUse*/
'2014-09-23 00:00:00.',/*DateLastUse*/
50,/*ori val*/
45,/*remai val*/
0,
null,
0,/*slot status*/
'root@localhost',
'root@localhost');

/*CREATE 5 D ROLL 31 BASE walletcontents*/
INSERT INTO walletcontents
(WalletContentsID,
WalletID,
Slot,
TicketID,
datestart,
DateExpires,
DateFirstUse,
DateLastUse,
ValueOriginal,
ValueRemaining,
ValueUse,
PendingCount,
SlotStatus,
TouchedBy,
CreatedBy)
VALUES
((select max(wc.WalletContentsID) + 1 from walletcontents wc),
(select max(walletId) from wallets),
2,/*Slot*/
(SELECT ticketID FROM tickets where DESCRIPTION like '%Weekday Full Fare 31 Day%'),
'2014-09-23 00:00:00.', /*datestart*/
'2014-10-23 00:00:00.',/*DateExpires*/
null,/*DateFirstUse*/
null,/*DateLastUse*/
31,/*ori val*/
0,/*remai val*/
0,
null,
0,/*slot status*/
'root@localhost',
'root@localhost');

/**************************************Cards Number 17**************************************/
INSERT INTO crossreferencefile
(WalletTypeID, PrintedID, ShippingListID, LotNumber, CVV, ElectronicID, CreatedBy, TouchedBy)
VALUES
(3,/*WalletTypeID*/
'1234560017',/*PrintedID*/
1,/*ShippingListID*/
1,/*LotNumber*/
123,/*CVV*/
'1234567891320017', /*ElectronicID*/
'root@localhost',
'root@localhost');

/*CREATE Wallet*/
INSERT INTO wallets
(WalletID,
PersonID,
DateExpires,
LoadSequence,
PurposeID,
WalletStatusID,
WalletTypeID,
FarecodeID,
TouchedBy,
CreatedBy)
VALUES
((select max(w.walletId) + 1 from wallets w),
null,
'2017-12-22 00:00:00.',
2,
(select PurposeID from Purposes where DESCRIPTION like '%normal%'),
(SELECT walletstatusid FROM walletstatus where DESCRIPTION like '%Active%'),
(SELECT wallettypeid FROM wallettypes where DESCRIPTION like '%Desfire%'),
(select farecodeid from Farecodes where DESCRIPTION like '%Full%'),
'root@localhost',
'root@localhost');

/*CREATE WalletIdentifiers*/
INSERT INTO walletidentifiers (WalletID,IdentifierID,Identifier,CreatedBy,TouchedBy) 
VALUES 
((select max(walletId) from wallets),1,'1234567891320017','root@localhost','root@localhost');

/*CREATE Stored Value walletcontents*/
INSERT INTO walletcontents
(WalletContentsID,
WalletID,
Slot,
TicketID,
datestart,
DateExpires,
DateFirstUse,
DateLastUse,
ValueOriginal,
ValueRemaining,
ValueUse,
PendingCount,
SlotStatus,
TouchedBy,
CreatedBy)
VALUES
((select max(wc.WalletContentsID) + 1 from walletcontents wc),
(select max(walletId) from wallets),
1,/*Slot*/
(SELECT ticketID FROM tickets where DESCRIPTION like '%variable store%'),
null, /*datestart*/
null,/*DateExpires*/
'2014-09-23 00:00:00.',/*DateFirstUse*/
'2014-09-23 00:00:00.',/*DateLastUse*/
50,/*ori val*/
45,/*remai val*/
0,
null,
0,/*slot status*/
'root@localhost',
'root@localhost');

/**************************************Cards Number 18**************************************/
INSERT INTO crossreferencefile
(WalletTypeID, PrintedID, ShippingListID, LotNumber, CVV, ElectronicID, CreatedBy, TouchedBy)
VALUES
(3,/*WalletTypeID*/
'1234560018',/*PrintedID*/
1,/*ShippingListID*/
1,/*LotNumber*/
123,/*CVV*/
'1234567891320018', /*ElectronicID*/
'root@localhost',
'root@localhost');

/*CREATE Wallet*/
INSERT INTO wallets
(WalletID,
PersonID,
DateExpires,
LoadSequence,
PurposeID,
WalletStatusID,
WalletTypeID,
FarecodeID,
TouchedBy,
CreatedBy)
VALUES
((select max(w.walletId) + 1 from wallets w),
null,
'2017-12-22 00:00:00.',
2,
(select PurposeID from Purposes where DESCRIPTION like '%normal%'),
(SELECT walletstatusid FROM walletstatus where DESCRIPTION like '%Active%'),
(SELECT wallettypeid FROM wallettypes where DESCRIPTION like '%Desfire%'),
(select farecodeid from Farecodes where DESCRIPTION like '%Full%'),
'root@localhost',
'root@localhost');

/*CREATE WalletIdentifiers*/
INSERT INTO walletidentifiers (WalletID,IdentifierID,Identifier,CreatedBy,TouchedBy) 
VALUES 
((select max(walletId) from wallets),1,'1234567891320018','root@localhost','root@localhost');

/*CREATE Stored Value walletcontents*/
INSERT INTO walletcontents
(WalletContentsID,
WalletID,
Slot,
TicketID,
datestart,
DateExpires,
DateFirstUse,
DateLastUse,
ValueOriginal,
ValueRemaining,
ValueUse,
PendingCount,
SlotStatus,
TouchedBy,
CreatedBy)
VALUES
((select max(wc.WalletContentsID) + 1 from walletcontents wc),
(select max(walletId) from wallets),
1,/*Slot*/
(SELECT ticketID FROM tickets where DESCRIPTION like '%variable store%'),
null, /*datestart*/
null,/*DateExpires*/
'2014-09-23 00:00:00.',/*DateFirstUse*/
'2014-09-23 00:00:00.',/*DateLastUse*/
50,/*ori val*/
45,/*remai val*/
0,
null,
0,/*slot status*/
'root@localhost',
'root@localhost');

/*CREATE 5 D ROLL 31 BASE walletcontents ACTIVE*/
INSERT INTO walletcontents
(WalletContentsID,
WalletID,
Slot,
TicketID,
datestart,
DateExpires,
DateFirstUse,
DateLastUse,
ValueOriginal,
ValueRemaining,
ValueUse,
PendingCount,
SlotStatus,
TouchedBy,
CreatedBy)
VALUES
((select max(wc.WalletContentsID) + 1 from walletcontents wc),
(select max(walletId) from wallets),
2,/*Slot*/
(SELECT ticketID FROM tickets where DESCRIPTION like '%Weekday Full Fare 31 Day%'),
'2014-09-23 00:00:00.', /*datestart*/
'2014-10-23 00:00:00.',/*DateExpires*/
null,/*DateFirstUse*/
null,/*DateLastUse*/
31,/*ori val*/
0,/*remai val*/
0,
null,
0,/*slot status*/
'root@localhost',
'root@localhost');

/*CREATE 5 D ROLL 31 BASE walletcontents INACTIVE*/
INSERT INTO walletcontents
(WalletContentsID,
WalletID,
Slot,
TicketID,
datestart,
DateExpires,
DateFirstUse,
DateLastUse,
ValueOriginal,
ValueRemaining,
ValueUse,
PendingCount,
SlotStatus,
TouchedBy,
CreatedBy)
VALUES
((select max(wc.WalletContentsID) + 1 from walletcontents wc),
(select max(walletId) from wallets),
2,/*Slot*/
(SELECT ticketID FROM tickets where DESCRIPTION like '%Weekday Full Fare 31 Day%'),
null, /*datestart*/
null,/*DateExpires*/
null,/*DateFirstUse*/
null,/*DateLastUse*/
31,/*ori val*/
0,/*remai val*/
0,
null,
0,/*slot status*/
'root@localhost',
'root@localhost');

/**************************************Cards Number 19**************************************/
INSERT INTO crossreferencefile
(WalletTypeID, PrintedID, ShippingListID, LotNumber, CVV, ElectronicID, CreatedBy, TouchedBy)
VALUES
(3,/*WalletTypeID*/
'1234560019',/*PrintedID*/
1,/*ShippingListID*/
1,/*LotNumber*/
123,/*CVV*/
'1234567891320019', /*ElectronicID*/
'root@localhost',
'root@localhost');

/*CREATE Wallet*/
INSERT INTO wallets
(WalletID,
PersonID,
DateExpires,
LoadSequence,
PurposeID,
WalletStatusID,
WalletTypeID,
FarecodeID,
TouchedBy,
CreatedBy)
VALUES
((select max(w.walletId) + 1 from wallets w),
null,
'2017-12-22 00:00:00.',
2,
(select PurposeID from Purposes where DESCRIPTION like '%normal%'),
(SELECT walletstatusid FROM walletstatus where DESCRIPTION like '%Active%'),
(SELECT wallettypeid FROM wallettypes where DESCRIPTION like '%Desfire%'),
(select farecodeid from Farecodes where DESCRIPTION like '%Senior%'),
'root@localhost',
'root@localhost');

/*CREATE WalletIdentifiers*/
INSERT INTO walletidentifiers (WalletID,IdentifierID,Identifier,CreatedBy,TouchedBy) 
VALUES 
((select max(walletId) from wallets),1,'1234567891320019','root@localhost','root@localhost');

/*CREATE Stored Value walletcontents*/
INSERT INTO walletcontents
(WalletContentsID,
WalletID,
Slot,
TicketID,
datestart,
DateExpires,
DateFirstUse,
DateLastUse,
ValueOriginal,
ValueRemaining,
ValueUse,
PendingCount,
SlotStatus,
TouchedBy,
CreatedBy)
VALUES
((select max(wc.WalletContentsID) + 1 from walletcontents wc),
(select max(walletId) from wallets),
1,/*Slot*/
(SELECT ticketID FROM tickets where DESCRIPTION like '%variable store%'), /*this should be 'Stored Value HALF FARE'*/
null, /*datestart*/
null,/*DateExpires*/
'2014-09-23 00:00:00.',/*DateFirstUse*/
'2014-09-23 00:00:00.',/*DateLastUse*/
50,/*ori val*/
45,/*remai val*/
0,
null,
0,/*slot status*/
'root@localhost',
'root@localhost');

/*CREATE 5 D ROLL 31 BASE walletcontents*/
INSERT INTO walletcontents
(WalletContentsID,
WalletID,
Slot,
TicketID,
datestart,
DateExpires,
DateFirstUse,
DateLastUse,
ValueOriginal,
ValueRemaining,
ValueUse,
PendingCount,
SlotStatus,
TouchedBy,
CreatedBy)
VALUES
((select max(wc.WalletContentsID) + 1 from walletcontents wc),
(select max(walletId) from wallets),
2,/*Slot*/
(SELECT ticketID FROM tickets where DESCRIPTION like '%Weekday half Fare 31 Day%'),
'2014-09-23 00:00:00.', /*datestart*/
'2014-10-23 00:00:00.',/*DateExpires*/
null,/*DateFirstUse*/
null,/*DateLastUse*/
31,/*ori val*/
0,/*remai val*/
0,
null,
0,/*slot status*/
'root@localhost',
'root@localhost');

/**************************************Cards Number 20**************************************/
INSERT INTO crossreferencefile
(WalletTypeID, PrintedID, ShippingListID, LotNumber, CVV, ElectronicID, CreatedBy, TouchedBy)
VALUES
(3,/*WalletTypeID*/
'1234560020',/*PrintedID*/
1,/*ShippingListID*/
1,/*LotNumber*/
123,/*CVV*/
'1234567891320020', /*ElectronicID*/
'root@localhost',
'root@localhost');

/*CREATE Wallet*/
INSERT INTO wallets
(WalletID,
PersonID,
DateExpires,
LoadSequence,
PurposeID,
WalletStatusID,
WalletTypeID,
FarecodeID,
TouchedBy,
CreatedBy)
VALUES
((select max(w.walletId) + 1 from wallets w),
null,
'2017-12-22 00:00:00.',
2,
(select PurposeID from Purposes where DESCRIPTION like '%normal%'),
(SELECT walletstatusid FROM walletstatus where DESCRIPTION like '%Active%'),
(SELECT wallettypeid FROM wallettypes where DESCRIPTION like '%Desfire%'),
(select farecodeid from Farecodes where DESCRIPTION like '%Senior%'),
'root@localhost',
'root@localhost');

/*CREATE WalletIdentifiers*/
INSERT INTO walletidentifiers (WalletID,IdentifierID,Identifier,CreatedBy,TouchedBy) 
VALUES 
((select max(walletId) from wallets),1,'1234567891320020','root@localhost','root@localhost');

/*CREATE Stored Value walletcontents*/
INSERT INTO walletcontents
(WalletContentsID,
WalletID,
Slot,
TicketID,
datestart,
DateExpires,
DateFirstUse,
DateLastUse,
ValueOriginal,
ValueRemaining,
ValueUse,
PendingCount,
SlotStatus,
TouchedBy,
CreatedBy)
VALUES
((select max(wc.WalletContentsID) + 1 from walletcontents wc),
(select max(walletId) from wallets),
1,/*Slot*/
(SELECT ticketID FROM tickets where DESCRIPTION like '%variable store%'), /*this should be 'Stored Value HALF FARE'*/
null, /*datestart*/
null,/*DateExpires*/
'2014-09-23 00:00:00.',/*DateFirstUse*/
'2014-09-23 00:00:00.',/*DateLastUse*/
50,/*ori val*/
45,/*remai val*/
0,
null,
0,/*slot status*/
'root@localhost',
'root@localhost');

/**************************************Cards Number 21**************************************/
INSERT INTO crossreferencefile
(WalletTypeID, PrintedID, ShippingListID, LotNumber, CVV, ElectronicID, CreatedBy, TouchedBy)
VALUES
(3,/*WalletTypeID*/
'1234560021',/*PrintedID*/
1,/*ShippingListID*/
1,/*LotNumber*/
123,/*CVV*/
'1234567891320021', /*ElectronicID*/
'root@localhost',
'root@localhost');

/*CREATE Wallet*/
INSERT INTO wallets
(WalletID,
PersonID,
DateExpires,
LoadSequence,
PurposeID,
WalletStatusID,
WalletTypeID,
FarecodeID,
TouchedBy,
CreatedBy)
VALUES
((select max(w.walletId) + 1 from wallets w),
null,
'2017-12-22 00:00:00.',
2,
(select PurposeID from Purposes where DESCRIPTION like '%normal%'),
(SELECT walletstatusid FROM walletstatus where DESCRIPTION like '%Active%'),
(SELECT wallettypeid FROM wallettypes where DESCRIPTION like '%Desfire%'),
(select farecodeid from Farecodes where DESCRIPTION like '%Full%'),
'root@localhost',
'root@localhost');

/*CREATE WalletIdentifiers*/
INSERT INTO walletidentifiers (WalletID,IdentifierID,Identifier,CreatedBy,TouchedBy) 
VALUES 
((select max(walletId) from wallets),1,'1234567891320021','root@localhost','root@localhost');

/*CREATE Stored Value walletcontents*/
INSERT INTO walletcontents
(WalletContentsID,
WalletID,
Slot,
TicketID,
datestart,
DateExpires,
DateFirstUse,
DateLastUse,
ValueOriginal,
ValueRemaining,
ValueUse,
PendingCount,
SlotStatus,
TouchedBy,
CreatedBy)
VALUES
((select max(wc.WalletContentsID) + 1 from walletcontents wc),
(select max(walletId) from wallets),
1,/*Slot*/
(SELECT ticketID FROM tickets where DESCRIPTION like '%variable store%'),
null, /*datestart*/
null,/*DateExpires*/
'2014-09-23 00:00:00.',/*DateFirstUse*/
'2014-09-23 00:00:00.',/*DateLastUse*/
50,/*ori val*/
45,/*remai val*/
0,
null,
0,/*slot status*/
'root@localhost',
'root@localhost');

/*CREATE 5 D ROLL 31 BASE walletcontents*/
INSERT INTO walletcontents
(WalletContentsID,
WalletID,
Slot,
TicketID,
datestart,
DateExpires,
DateFirstUse,
DateLastUse,
ValueOriginal,
ValueRemaining,
ValueUse,
PendingCount,
SlotStatus,
TouchedBy,
CreatedBy)
VALUES
((select max(wc.WalletContentsID) + 1 from walletcontents wc),
(select max(walletId) from wallets),
2,/*Slot*/
(SELECT ticketID FROM tickets where DESCRIPTION like '%Weekday Full Fare 31 Day%'),
'2014-09-23 00:00:00.', /*datestart*/
'2014-10-23 00:00:00.',/*DateExpires*/
null,/*DateFirstUse*/
null,/*DateLastUse*/
31,/*ori val*/
0,/*remai val*/
0,
null,
0,/*slot status*/
'root@localhost',
'root@localhost');

/**************************************Cards Number 22**************************************/
INSERT INTO crossreferencefile
(WalletTypeID, PrintedID, ShippingListID, LotNumber, CVV, ElectronicID, CreatedBy, TouchedBy)
VALUES
(3,/*WalletTypeID*/
'1234560022',/*PrintedID*/
1,/*ShippingListID*/
1,/*LotNumber*/
123,/*CVV*/
'1234567891320022', /*ElectronicID*/
'root@localhost',
'root@localhost');

/*CREATE Wallet*/
INSERT INTO wallets
(WalletID,
PersonID,
DateExpires,
LoadSequence,
PurposeID,
WalletStatusID,
WalletTypeID,
FarecodeID,
TouchedBy,
CreatedBy)
VALUES
((select max(w.walletId) + 1 from wallets w),
null,
'2017-12-22 00:00:00.',
2,
(select PurposeID from Purposes where DESCRIPTION like '%normal%'),
(SELECT walletstatusid FROM walletstatus where DESCRIPTION like '%Active%'),
(SELECT wallettypeid FROM wallettypes where DESCRIPTION like '%Desfire%'),
(select farecodeid from Farecodes where DESCRIPTION like '%Full%'),
'root@localhost',
'root@localhost');

/*CREATE WalletIdentifiers*/
INSERT INTO walletidentifiers (WalletID,IdentifierID,Identifier,CreatedBy,TouchedBy) 
VALUES 
((select max(walletId) from wallets),1,'1234567891320022','root@localhost','root@localhost');

/*CREATE Stored Value walletcontents*/
INSERT INTO walletcontents
(WalletContentsID,
WalletID,
Slot,
TicketID,
datestart,
DateExpires,
DateFirstUse,
DateLastUse,
ValueOriginal,
ValueRemaining,
ValueUse,
PendingCount,
SlotStatus,
TouchedBy,
CreatedBy)
VALUES
((select max(wc.WalletContentsID) + 1 from walletcontents wc),
(select max(walletId) from wallets),
1,/*Slot*/
(SELECT ticketID FROM tickets where DESCRIPTION like '%variable store%'),
null, /*datestart*/
null,/*DateExpires*/
'2014-09-23 00:00:00.',/*DateFirstUse*/
'2014-09-23 00:00:00.',/*DateLastUse*/
50,/*ori val*/
45,/*remai val*/
0,
null,
0,/*slot status*/
'root@localhost',
'root@localhost');

/**************************************Cards Number 23**************************************/
INSERT INTO crossreferencefile
(WalletTypeID, PrintedID, ShippingListID, LotNumber, CVV, ElectronicID, CreatedBy, TouchedBy)
VALUES
(3,/*WalletTypeID*/
'1234560023',/*PrintedID*/
1,/*ShippingListID*/
1,/*LotNumber*/
123,/*CVV*/
'1234567891320023', /*ElectronicID*/
'root@localhost',
'root@localhost');

/*CREATE Wallet*/
INSERT INTO wallets
(WalletID,
PersonID,
DateExpires,
LoadSequence,
PurposeID,
WalletStatusID,
WalletTypeID,
FarecodeID,
TouchedBy,
CreatedBy)
VALUES
((select max(w.walletId) + 1 from wallets w),
null,
'2017-12-22 00:00:00.',
2,
(select PurposeID from Purposes where DESCRIPTION like '%normal%'),
(SELECT walletstatusid FROM walletstatus where DESCRIPTION like '%Active%'),
(SELECT wallettypeid FROM wallettypes where DESCRIPTION like '%Desfire%'),
(select farecodeid from Farecodes where DESCRIPTION like '%Full%'),
'root@localhost',
'root@localhost');

/*CREATE WalletIdentifiers*/
INSERT INTO walletidentifiers (WalletID,IdentifierID,Identifier,CreatedBy,TouchedBy) 
VALUES 
((select max(walletId) from wallets),1,'1234567891320023','root@localhost','root@localhost');

/*CREATE Stored Value walletcontents*/
INSERT INTO walletcontents
(WalletContentsID,
WalletID,
Slot,
TicketID,
datestart,
DateExpires,
DateFirstUse,
DateLastUse,
ValueOriginal,
ValueRemaining,
ValueUse,
PendingCount,
SlotStatus,
TouchedBy,
CreatedBy)
VALUES
((select max(wc.WalletContentsID) + 1 from walletcontents wc),
(select max(walletId) from wallets),
1,/*Slot*/
(SELECT ticketID FROM tickets where DESCRIPTION like '%variable store%'),
null, /*datestart*/
null,/*DateExpires*/
'2014-09-23 00:00:00.',/*DateFirstUse*/
'2014-09-23 00:00:00.',/*DateLastUse*/
50,/*ori val*/
45,/*remai val*/
0,
null,
0,/*slot status*/
'root@localhost',
'root@localhost');

/*CREATE 5 D ROLL 31 BASE walletcontents ACTIVE*/
INSERT INTO walletcontents
(WalletContentsID,
WalletID,
Slot,
TicketID,
datestart,
DateExpires,
DateFirstUse,
DateLastUse,
ValueOriginal,
ValueRemaining,
ValueUse,
PendingCount,
SlotStatus,
TouchedBy,
CreatedBy)
VALUES
((select max(wc.WalletContentsID) + 1 from walletcontents wc),
(select max(walletId) from wallets),
2,/*Slot*/
(SELECT ticketID FROM tickets where DESCRIPTION like '%Weekday Full Fare 31 Day%'),
'2014-09-23 00:00:00.', /*datestart*/
'2014-10-23 00:00:00.',/*DateExpires*/
null,/*DateFirstUse*/
null,/*DateLastUse*/
31,/*ori val*/
0,/*remai val*/
0,
null,
0,/*slot status*/
'root@localhost',
'root@localhost');

/*CREATE 5 D ROLL 31 BASE walletcontents INACTIVE*/
INSERT INTO walletcontents
(WalletContentsID,
WalletID,
Slot,
TicketID,
datestart,
DateExpires,
DateFirstUse,
DateLastUse,
ValueOriginal,
ValueRemaining,
ValueUse,
PendingCount,
SlotStatus,
TouchedBy,
CreatedBy)
VALUES
((select max(wc.WalletContentsID) + 1 from walletcontents wc),
(select max(walletId) from wallets),
2,/*Slot*/
(SELECT ticketID FROM tickets where DESCRIPTION like '%Weekday Full Fare 31 Day%'),
null, /*datestart*/
null,/*DateExpires*/
null,/*DateFirstUse*/
null,/*DateLastUse*/
31,/*ori val*/
0,/*remai val*/
0,
null,
0,/*slot status*/
'root@localhost',
'root@localhost');

/**************************************Cards Number 24**************************************/
INSERT INTO crossreferencefile
(WalletTypeID, PrintedID, ShippingListID, LotNumber, CVV, ElectronicID, CreatedBy, TouchedBy)
VALUES
(3,/*WalletTypeID*/
'1234560024',/*PrintedID*/
1,/*ShippingListID*/
1,/*LotNumber*/
123,/*CVV*/
'1234567891320024', /*ElectronicID*/
'root@localhost',
'root@localhost');

/*CREATE Wallet*/
INSERT INTO wallets
(WalletID,
PersonID,
DateExpires,
LoadSequence,
PurposeID,
WalletStatusID,
WalletTypeID,
FarecodeID,
TouchedBy,
CreatedBy)
VALUES
((select max(w.walletId) + 1 from wallets w),
null,
'2017-12-22 00:00:00.',
2,
(select PurposeID from Purposes where DESCRIPTION like '%normal%'),
(SELECT walletstatusid FROM walletstatus where DESCRIPTION like '%Active%'),
(SELECT wallettypeid FROM wallettypes where DESCRIPTION like '%Desfire%'),
(select farecodeid from Farecodes where DESCRIPTION like '%Senior%'),
'root@localhost',
'root@localhost');

/*CREATE WalletIdentifiers*/
INSERT INTO walletidentifiers (WalletID,IdentifierID,Identifier,CreatedBy,TouchedBy) 
VALUES 
((select max(walletId) from wallets),1,'1234567891320024','root@localhost','root@localhost');

/*CREATE Stored Value walletcontents*/
INSERT INTO walletcontents
(WalletContentsID,
WalletID,
Slot,
TicketID,
datestart,
DateExpires,
DateFirstUse,
DateLastUse,
ValueOriginal,
ValueRemaining,
ValueUse,
PendingCount,
SlotStatus,
TouchedBy,
CreatedBy)
VALUES
((select max(wc.WalletContentsID) + 1 from walletcontents wc),
(select max(walletId) from wallets),
1,/*Slot*/
(SELECT ticketID FROM tickets where DESCRIPTION like '%variable store%'), /*this should be 'Stored Value HALF FARE'*/
null, /*datestart*/
null,/*DateExpires*/
'2014-09-23 00:00:00.',/*DateFirstUse*/
'2014-09-23 00:00:00.',/*DateLastUse*/
50,/*ori val*/
45,/*remai val*/
0,
null,
0,/*slot status*/
'root@localhost',
'root@localhost');

/*CREATE 5 D ROLL 31 BASE walletcontents*/
INSERT INTO walletcontents
(WalletContentsID,
WalletID,
Slot,
TicketID,
datestart,
DateExpires,
DateFirstUse,
DateLastUse,
ValueOriginal,
ValueRemaining,
ValueUse,
PendingCount,
SlotStatus,
TouchedBy,
CreatedBy)
VALUES
((select max(wc.WalletContentsID) + 1 from walletcontents wc),
(select max(walletId) from wallets),
2,/*Slot*/
(SELECT ticketID FROM tickets where DESCRIPTION like '%Weekday half Fare 31 Day%'),
'2014-09-23 00:00:00.', /*datestart*/
'2014-10-23 00:00:00.',/*DateExpires*/
null,/*DateFirstUse*/
null,/*DateLastUse*/
31,/*ori val*/
0,/*remai val*/
0,
null,
0,/*slot status*/
'root@localhost',
'root@localhost');

/**************************************Cards Number 25**************************************/
INSERT INTO crossreferencefile
(WalletTypeID, PrintedID, ShippingListID, LotNumber, CVV, ElectronicID, CreatedBy, TouchedBy)
VALUES
(3,/*WalletTypeID*/
'1234560025',/*PrintedID*/
1,/*ShippingListID*/
1,/*LotNumber*/
123,/*CVV*/
'1234567891320025', /*ElectronicID*/
'root@localhost',
'root@localhost');

/*CREATE Wallet*/
INSERT INTO wallets
(WalletID,
PersonID,
DateExpires,
LoadSequence,
PurposeID,
WalletStatusID,
WalletTypeID,
FarecodeID,
TouchedBy,
CreatedBy)
VALUES
((select max(w.walletId) + 1 from wallets w),
null,
'2017-12-22 00:00:00.',
2,
(select PurposeID from Purposes where DESCRIPTION like '%normal%'),
(SELECT walletstatusid FROM walletstatus where DESCRIPTION like '%Active%'),
(SELECT wallettypeid FROM wallettypes where DESCRIPTION like '%Desfire%'),
(select farecodeid from Farecodes where DESCRIPTION like '%Senior%'),
'root@localhost',
'root@localhost');

/*CREATE WalletIdentifiers*/
INSERT INTO walletidentifiers (WalletID,IdentifierID,Identifier,CreatedBy,TouchedBy) 
VALUES 
((select max(walletId) from wallets),1,'1234567891320025','root@localhost','root@localhost');

/*CREATE Stored Value walletcontents*/
INSERT INTO walletcontents
(WalletContentsID,
WalletID,
Slot,
TicketID,
datestart,
DateExpires,
DateFirstUse,
DateLastUse,
ValueOriginal,
ValueRemaining,
ValueUse,
PendingCount,
SlotStatus,
TouchedBy,
CreatedBy)
VALUES
((select max(wc.WalletContentsID) + 1 from walletcontents wc),
(select max(walletId) from wallets),
1,/*Slot*/
(SELECT ticketID FROM tickets where DESCRIPTION like '%variable store%'), /*this should be 'Stored Value HALF FARE'*/
null, /*datestart*/
null,/*DateExpires*/
'2014-09-23 00:00:00.',/*DateFirstUse*/
'2014-09-23 00:00:00.',/*DateLastUse*/
50,/*ori val*/
45,/*remai val*/
0,
null,
0,/*slot status*/
'root@localhost',
'root@localhost');

/**************************************Cards Number 26**************************************/
INSERT INTO crossreferencefile
(WalletTypeID, PrintedID, ShippingListID, LotNumber, CVV, ElectronicID, CreatedBy, TouchedBy)
VALUES
(3,/*WalletTypeID*/
'1234560026',/*PrintedID*/
1,/*ShippingListID*/
1,/*LotNumber*/
123,/*CVV*/
'1234567891320026', /*ElectronicID*/
'root@localhost',
'root@localhost');

/*CREATE Wallet*/
INSERT INTO wallets
(WalletID,
PersonID,
DateExpires,
LoadSequence,
PurposeID,
WalletStatusID,
WalletTypeID,
FarecodeID,
TouchedBy,
CreatedBy)
VALUES
((select max(w.walletId) + 1 from wallets w),
null,
'2017-12-22 00:00:00.',
2,
(select PurposeID from Purposes where DESCRIPTION like '%normal%'),
(SELECT walletstatusid FROM walletstatus where DESCRIPTION like '%Active%'),
(SELECT wallettypeid FROM wallettypes where DESCRIPTION like '%Desfire%'),
(select farecodeid from Farecodes where DESCRIPTION like '%Full%'),
'root@localhost',
'root@localhost');

/*CREATE WalletIdentifiers*/
INSERT INTO walletidentifiers (WalletID,IdentifierID,Identifier,CreatedBy,TouchedBy) 
VALUES 
((select max(walletId) from wallets),1,'1234567891320026','root@localhost','root@localhost');

/*CREATE Stored Value walletcontents*/
INSERT INTO walletcontents
(WalletContentsID,
WalletID,
Slot,
TicketID,
datestart,
DateExpires,
DateFirstUse,
DateLastUse,
ValueOriginal,
ValueRemaining,
ValueUse,
PendingCount,
SlotStatus,
TouchedBy,
CreatedBy)
VALUES
((select max(wc.WalletContentsID) + 1 from walletcontents wc),
(select max(walletId) from wallets),
1,/*Slot*/
(SELECT ticketID FROM tickets where DESCRIPTION like '%variable store%'),
null, /*datestart*/
null,/*DateExpires*/
'2014-09-23 00:00:00.',/*DateFirstUse*/
'2014-09-23 00:00:00.',/*DateLastUse*/
50,/*ori val*/
45,/*remai val*/
0,
null,
0,/*slot status*/
'root@localhost',
'root@localhost');

/*CREATE 5 D ROLL 31 BASE walletcontents*/
INSERT INTO walletcontents
(WalletContentsID,
WalletID,
Slot,
TicketID,
datestart,
DateExpires,
DateFirstUse,
DateLastUse,
ValueOriginal,
ValueRemaining,
ValueUse,
PendingCount,
SlotStatus,
TouchedBy,
CreatedBy)
VALUES
((select max(wc.WalletContentsID) + 1 from walletcontents wc),
(select max(walletId) from wallets),
2,/*Slot*/
(SELECT ticketID FROM tickets where DESCRIPTION like '%Weekday Full Fare 31 Day%'),
'2014-09-23 00:00:00.', /*datestart*/
'2014-10-23 00:00:00.',/*DateExpires*/
null,/*DateFirstUse*/
null,/*DateLastUse*/
31,/*ori val*/
0,/*remai val*/
0,
null,
0,/*slot status*/
'root@localhost',
'root@localhost');

/**************************************Cards Number 27**************************************/
INSERT INTO crossreferencefile
(WalletTypeID, PrintedID, ShippingListID, LotNumber, CVV, ElectronicID, CreatedBy, TouchedBy)
VALUES
(3,/*WalletTypeID*/
'1234560027',/*PrintedID*/
1,/*ShippingListID*/
1,/*LotNumber*/
123,/*CVV*/
'1234567891320027', /*ElectronicID*/
'root@localhost',
'root@localhost');

/*CREATE Wallet*/
INSERT INTO wallets
(WalletID,
PersonID,
DateExpires,
LoadSequence,
PurposeID,
WalletStatusID,
WalletTypeID,
FarecodeID,
TouchedBy,
CreatedBy)
VALUES
((select max(w.walletId) + 1 from wallets w),
null,
'2017-12-22 00:00:00.',
2,
(select PurposeID from Purposes where DESCRIPTION like '%normal%'),
(SELECT walletstatusid FROM walletstatus where DESCRIPTION like '%Active%'),
(SELECT wallettypeid FROM wallettypes where DESCRIPTION like '%Desfire%'),
(select farecodeid from Farecodes where DESCRIPTION like '%Full%'),
'root@localhost',
'root@localhost');

/*CREATE WalletIdentifiers*/
INSERT INTO walletidentifiers (WalletID,IdentifierID,Identifier,CreatedBy,TouchedBy) 
VALUES 
((select max(walletId) from wallets),1,'1234567891320027','root@localhost','root@localhost');

/*CREATE Stored Value walletcontents*/
INSERT INTO walletcontents
(WalletContentsID,
WalletID,
Slot,
TicketID,
datestart,
DateExpires,
DateFirstUse,
DateLastUse,
ValueOriginal,
ValueRemaining,
ValueUse,
PendingCount,
SlotStatus,
TouchedBy,
CreatedBy)
VALUES
((select max(wc.WalletContentsID) + 1 from walletcontents wc),
(select max(walletId) from wallets),
1,/*Slot*/
(SELECT ticketID FROM tickets where DESCRIPTION like '%variable store%'),
null, /*datestart*/
null,/*DateExpires*/
'2014-09-23 00:00:00.',/*DateFirstUse*/
'2014-09-23 00:00:00.',/*DateLastUse*/
50,/*ori val*/
45,/*remai val*/
0,
null,
0,/*slot status*/
'root@localhost',
'root@localhost');

/**************************************Cards Number 28**************************************/
INSERT INTO crossreferencefile
(WalletTypeID, PrintedID, ShippingListID, LotNumber, CVV, ElectronicID, CreatedBy, TouchedBy)
VALUES
(3,/*WalletTypeID*/
'1234560028',/*PrintedID*/
1,/*ShippingListID*/
1,/*LotNumber*/
123,/*CVV*/
'1234567891320028', /*ElectronicID*/
'root@localhost',
'root@localhost');

/*CREATE Wallet*/
INSERT INTO wallets
(WalletID,
PersonID,
DateExpires,
LoadSequence,
PurposeID,
WalletStatusID,
WalletTypeID,
FarecodeID,
TouchedBy,
CreatedBy)
VALUES
((select max(w.walletId) + 1 from wallets w),
null,
'2017-12-22 00:00:00.',
2,
(select PurposeID from Purposes where DESCRIPTION like '%normal%'),
(SELECT walletstatusid FROM walletstatus where DESCRIPTION like '%Active%'),
(SELECT wallettypeid FROM wallettypes where DESCRIPTION like '%Desfire%'),
(select farecodeid from Farecodes where DESCRIPTION like '%Full%'),
'root@localhost',
'root@localhost');

/*CREATE WalletIdentifiers*/
INSERT INTO walletidentifiers (WalletID,IdentifierID,Identifier,CreatedBy,TouchedBy) 
VALUES 
((select max(walletId) from wallets),1,'1234567891320028','root@localhost','root@localhost');

/*CREATE Stored Value walletcontents*/
INSERT INTO walletcontents
(WalletContentsID,
WalletID,
Slot,
TicketID,
datestart,
DateExpires,
DateFirstUse,
DateLastUse,
ValueOriginal,
ValueRemaining,
ValueUse,
PendingCount,
SlotStatus,
TouchedBy,
CreatedBy)
VALUES
((select max(wc.WalletContentsID) + 1 from walletcontents wc),
(select max(walletId) from wallets),
1,/*Slot*/
(SELECT ticketID FROM tickets where DESCRIPTION like '%variable store%'),
null, /*datestart*/
null,/*DateExpires*/
'2014-09-23 00:00:00.',/*DateFirstUse*/
'2014-09-23 00:00:00.',/*DateLastUse*/
50,/*ori val*/
45,/*remai val*/
0,
null,
0,/*slot status*/
'root@localhost',
'root@localhost');

/*CREATE 5 D ROLL 31 BASE walletcontents ACTIVE*/
INSERT INTO walletcontents
(WalletContentsID,
WalletID,
Slot,
TicketID,
datestart,
DateExpires,
DateFirstUse,
DateLastUse,
ValueOriginal,
ValueRemaining,
ValueUse,
PendingCount,
SlotStatus,
TouchedBy,
CreatedBy)
VALUES
((select max(wc.WalletContentsID) + 1 from walletcontents wc),
(select max(walletId) from wallets),
2,/*Slot*/
(SELECT ticketID FROM tickets where DESCRIPTION like '%Weekday Full Fare 31 Day%'),
'2014-09-23 00:00:00.', /*datestart*/
'2014-10-23 00:00:00.',/*DateExpires*/
null,/*DateFirstUse*/
null,/*DateLastUse*/
31,/*ori val*/
0,/*remai val*/
0,
null,
0,/*slot status*/
'root@localhost',
'root@localhost');

/*CREATE 5 D ROLL 31 BASE walletcontents INACTIVE*/
INSERT INTO walletcontents
(WalletContentsID,
WalletID,
Slot,
TicketID,
datestart,
DateExpires,
DateFirstUse,
DateLastUse,
ValueOriginal,
ValueRemaining,
ValueUse,
PendingCount,
SlotStatus,
TouchedBy,
CreatedBy)
VALUES
((select max(wc.WalletContentsID) + 1 from walletcontents wc),
(select max(walletId) from wallets),
2,/*Slot*/
(SELECT ticketID FROM tickets where DESCRIPTION like '%Weekday Full Fare 31 Day%'),
null, /*datestart*/
null,/*DateExpires*/
null,/*DateFirstUse*/
null,/*DateLastUse*/
31,/*ori val*/
0,/*remai val*/
0,
null,
0,/*slot status*/
'root@localhost',
'root@localhost');

/**************************************Cards Number 29**************************************/
INSERT INTO crossreferencefile
(WalletTypeID, PrintedID, ShippingListID, LotNumber, CVV, ElectronicID, CreatedBy, TouchedBy)
VALUES
(3,/*WalletTypeID*/
'1234560029',/*PrintedID*/
1,/*ShippingListID*/
1,/*LotNumber*/
123,/*CVV*/
'1234567891320029', /*ElectronicID*/
'root@localhost',
'root@localhost');

/*CREATE Wallet*/
INSERT INTO wallets
(WalletID,
PersonID,
DateExpires,
LoadSequence,
PurposeID,
WalletStatusID,
WalletTypeID,
FarecodeID,
TouchedBy,
CreatedBy)
VALUES
((select max(w.walletId) + 1 from wallets w),
null,
'2017-12-22 00:00:00.',
2,
(select PurposeID from Purposes where DESCRIPTION like '%normal%'),
(SELECT walletstatusid FROM walletstatus where DESCRIPTION like '%Active%'),
(SELECT wallettypeid FROM wallettypes where DESCRIPTION like '%Desfire%'),
(select farecodeid from Farecodes where DESCRIPTION like '%Senior%'),
'root@localhost',
'root@localhost');

/*CREATE WalletIdentifiers*/
INSERT INTO walletidentifiers (WalletID,IdentifierID,Identifier,CreatedBy,TouchedBy) 
VALUES 
((select max(walletId) from wallets),1,'1234567891320029','root@localhost','root@localhost');

/*CREATE Stored Value walletcontents*/
INSERT INTO walletcontents
(WalletContentsID,
WalletID,
Slot,
TicketID,
datestart,
DateExpires,
DateFirstUse,
DateLastUse,
ValueOriginal,
ValueRemaining,
ValueUse,
PendingCount,
SlotStatus,
TouchedBy,
CreatedBy)
VALUES
((select max(wc.WalletContentsID) + 1 from walletcontents wc),
(select max(walletId) from wallets),
1,/*Slot*/
(SELECT ticketID FROM tickets where DESCRIPTION like '%variable store%'), /*this should be 'Stored Value HALF FARE'*/
null, /*datestart*/
null,/*DateExpires*/
'2014-09-23 00:00:00.',/*DateFirstUse*/
'2014-09-23 00:00:00.',/*DateLastUse*/
50,/*ori val*/
45,/*remai val*/
0,
null,
0,/*slot status*/
'root@localhost',
'root@localhost');

/*CREATE 5 D ROLL 31 BASE walletcontents*/
INSERT INTO walletcontents
(WalletContentsID,
WalletID,
Slot,
TicketID,
datestart,
DateExpires,
DateFirstUse,
DateLastUse,
ValueOriginal,
ValueRemaining,
ValueUse,
PendingCount,
SlotStatus,
TouchedBy,
CreatedBy)
VALUES
((select max(wc.WalletContentsID) + 1 from walletcontents wc),
(select max(walletId) from wallets),
2,/*Slot*/
(SELECT ticketID FROM tickets where DESCRIPTION like '%Weekday half Fare 31 Day%'),
'2014-09-23 00:00:00.', /*datestart*/
'2014-10-23 00:00:00.',/*DateExpires*/
null,/*DateFirstUse*/
null,/*DateLastUse*/
31,/*ori val*/
0,/*remai val*/
0,
null,
0,/*slot status*/
'root@localhost',
'root@localhost');

/**************************************Cards Number 30**************************************/
INSERT INTO crossreferencefile
(WalletTypeID, PrintedID, ShippingListID, LotNumber, CVV, ElectronicID, CreatedBy, TouchedBy)
VALUES
(3,/*WalletTypeID*/
'1234560030',/*PrintedID*/
1,/*ShippingListID*/
1,/*LotNumber*/
123,/*CVV*/
'1234567891320030', /*ElectronicID*/
'root@localhost',
'root@localhost');

/*CREATE Wallet*/
INSERT INTO wallets
(WalletID,
PersonID,
DateExpires,
LoadSequence,
PurposeID,
WalletStatusID,
WalletTypeID,
FarecodeID,
TouchedBy,
CreatedBy)
VALUES
((select max(w.walletId) + 1 from wallets w),
null,
'2017-12-22 00:00:00.',
2,
(select PurposeID from Purposes where DESCRIPTION like '%normal%'),
(SELECT walletstatusid FROM walletstatus where DESCRIPTION like '%Active%'),
(SELECT wallettypeid FROM wallettypes where DESCRIPTION like '%Desfire%'),
(select farecodeid from Farecodes where DESCRIPTION like '%Senior%'),
'root@localhost',
'root@localhost');

/*CREATE WalletIdentifiers*/
INSERT INTO walletidentifiers (WalletID,IdentifierID,Identifier,CreatedBy,TouchedBy) 
VALUES 
((select max(walletId) from wallets),1,'1234567891320030','root@localhost','root@localhost');

/*CREATE Stored Value walletcontents*/
INSERT INTO walletcontents
(WalletContentsID,
WalletID,
Slot,
TicketID,
datestart,
DateExpires,
DateFirstUse,
DateLastUse,
ValueOriginal,
ValueRemaining,
ValueUse,
PendingCount,
SlotStatus,
TouchedBy,
CreatedBy)
VALUES
((select max(wc.WalletContentsID) + 1 from walletcontents wc),
(select max(walletId) from wallets),
1,/*Slot*/
(SELECT ticketID FROM tickets where DESCRIPTION like '%variable store%'), /*this should be 'Stored Value HALF FARE'*/
null, /*datestart*/
null,/*DateExpires*/
'2014-09-23 00:00:00.',/*DateFirstUse*/
'2014-09-23 00:00:00.',/*DateLastUse*/
50,/*ori val*/
45,/*remai val*/
0,
null,
0,/*slot status*/
'root@localhost',
'root@localhost');