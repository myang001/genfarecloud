/*Insert data into crossref file for DES-Smart cards */
INSERT INTO crossreferencefile (WalletTypeID, PrintedID, ShippingListID, LotNumber, CVV, ElectronicID, CreatedBy, TouchedBy) 
VALUES ('3', '9991110001', '1', '1', '123', '1112223334440001', 'localhost', 'localhost'),
('3', '9991110002', '1', '1', '123', '1112223334440002', 'localhost', 'localhost'),
('3', '9991110003', '1', '1', '123', '1112223334440003', 'localhost', 'localhost'),
('3', '9991110004', '1', '1', '123', '1112223334440004', 'localhost', 'localhost'),
('3', '9991110005', '1', '1', '123', '1112223334440005', 'localhost', 'localhost'),
('3', '9991110006', '1', '1', '123', '1112223334440006', 'localhost', 'localhost'),
('3', '9991110007', '1', '1', '123', '1112223334440007', 'localhost', 'localhost'),
('3', '9991110008', '1', '1', '123', '1112223334440008', 'localhost', 'localhost'),
('3', '9991110009', '1', '1', '123', '1112223334440009', 'localhost', 'localhost'),
('3', '9991110010', '1', '1', '123', '1112223334440010', 'localhost', 'localhost'),
('3', '9991110011', '1', '1', '123', '1112223334440011', 'localhost', 'localhost'),
('3', '9991110012', '1', '1', '123', '1112223334440012', 'localhost', 'localhost'),
('3', '9991110013', '1', '1', '123', '1112223334440014', 'localhost', 'localhost'),
('3', '9991110014', '1', '1', '123', '1112223334440014', 'localhost', 'localhost'),
('3', '9991110015', '1', '1', '123', '1112223334440015', 'localhost', 'localhost');

/** create existing desfire card from crossref file **/
INSERT INTO wallets (LoadSequence, PurposeID, WalletStatusID, WalletTypeID, CreatedBy, TouchedBy) 
VALUES ('1', '1', '2', '3', 'localhost', 'localhost');
INSERT INTO walletidentifiers (WalletID, IdentifierID, Identifier, CreatedBy, TouchedBy) VALUES 
((select max(walletId) from wallets), '1', '1112223334440001', 'localhost', 'localhost'),
((select max(walletId) from wallets), '2', '9991110001', 'localhost', 'localhost');


INSERT INTO wallets (LoadSequence, PurposeID, WalletStatusID, WalletTypeID, CreatedBy, TouchedBy) 
VALUES ('1', '1', '2', '3', 'localhost', 'localhost');
INSERT INTO walletidentifiers (WalletID, IdentifierID, Identifier, CreatedBy, TouchedBy) VALUES 
((select max(walletId) from wallets), '1', '1112223334440002', 'localhost', 'localhost'),
((select max(walletId) from wallets), '2', '9991110002', 'localhost', 'localhost');
