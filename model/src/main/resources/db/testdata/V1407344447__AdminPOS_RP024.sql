-- Admin POS report - RP024
CREATE TABLE temp_rp024 (LastName VARCHAR(50), FirstName VARCHAR(50), Status VARCHAR(50), Organization VARCHAR(50), Description VARCHAR(50), GroupDescription VARCHAR(50), LoginId VARCHAR(50), DateLastAccessed DateTime);
INSERT INTO temp_rp024 VALUES("Smith", "John" , "Active", "CDTA" , "Finance", "Admin", "jsmith@abc.com", "2014-08-05 03:14:07");
INSERT INTO temp_rp024 VALUES("Trip", "Bruce" , "Active", "CDTA" , "Driver", "Admin", "btrip@abc.com", "2014-08-05 03:14:07");
INSERT INTO temp_rp024 VALUES("Zeno", "Zidane" , "Active", "CDTA" , "Maintenance", "Admin", "ZZenos@abc.com", "2014-08-05 03:14:07");

CREATE OR REPLACE VIEW AdminPOSUsers AS
SELECT LastName, FirstName, Status, Organization, Description, GroupDescription, LoginId, DateLastAccessed FROM temp_rp024; 
