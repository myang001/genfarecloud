
UPDATE wallets SET Nickname='Nickname here'  WHERE walletid = 128;

-- CardHistory update for walletcontents b/c of new view
UPDATE walletcontents SET slotstatus = 2 WHERE WalletContentsID = 207;

-- CardHistory update for activitydetails b/c of new view
UPDATE activitydetails SET PaymentTypeID = 1 WHERE ActivityID = 2;
UPDATE activitydetails SET DateActivity = now() WHERE ActivityID = 2;

-- CurrentActionList update for pendingactivity 
UPDATE pendingactivity SET DateExpires = now() where walletid = 315;