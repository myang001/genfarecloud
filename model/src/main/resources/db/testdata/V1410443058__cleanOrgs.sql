/*Create Default Parent Organization*/
INSERT INTO organizations
(OrganizationID,
ParentOrganization,
Name,
OrganizationTypeID,
BillingMethod,
PaymentTypeID,
CreditLimit,
Balance,
HasOwnCards,
CodeThirdParty,
MainContact,
Status,
TouchedBy,
CreatedBy)
VALUES
(3,
null,
'Default Parent Organization',
4,
null,
1,
50000.00,
50000.00,
null,
null,
null,
1,
'root@localhost',
'root@localhost');

/*Create Default Child Organization*/
INSERT INTO organizations
(OrganizationID,
ParentOrganization,
Name,
OrganizationTypeID,
BillingMethod,
PaymentTypeID,
CreditLimit,
Balance,
HasOwnCards,
CodeThirdParty,
MainContact,
Status,
TouchedBy,
CreatedBy)
VALUES
(4,
3,
'Default Child Organization',
4,
null,
1,
50000.00,
50000.00,
null,
null,
null,
1,
'root@localhost',
'root@localhost');

/*Detach ticketdiscounts from Orgs*/
UPDATE ticketdiscounts
SET
OrganizationID = null
WHERE OrganizationID is not null;

/*Attach groups to Defaul Child Organization*/
UPDATE groups
SET
OrganizationID = 4
WHERE OrganizationID = 10;

/*Delete Org Addresses*/
truncate table organizationaddresses;

/*Delete Org Links*/
truncate table organizationlinks;

/*Delete all Org, except GFI, Transit Agency, Default Parent and Default Child*/
DELETE FROM organizations
WHERE OrganizationID <> 1 AND OrganizationID <> 2 AND OrganizationID <> 3 AND OrganizationID <> 4;

/*Remove relationship between GFI and Transit Agency (GFI is not Transit Agency father)*/
UPDATE organizations
SET
ParentOrganization = null
WHERE
OrganizationID = 2;