Create table temp_rp014 (EquipmentDesc  Varchar(50), SerialNumber  Varchar(50), ProbeType Varchar(50), Status Varchar(50), LocationDesc Varchar(50), date_time DATETIME, equipmentstatus Varchar(50));
insert into temp_rp014 values("Farebox 13", "001-302-123032" , "Wifi" , "Failed" , "Albany Office" , "2014-05-15 03:14:07" , "Maintenance");
insert into temp_rp014 values("Bus 11", "ABDX124" , "IR Probe" , "OK" , "Garage 12" , "2014-05-16 03:14:07" , "In Service");

CREATE OR REPLACE VIEW devicelastprobed AS
SELECT EquipmentDesc, SerialNumber, ProbeType, Status, LocationDesc, date_time, equipmentstatus from temp_rp014;