INSERT INTO timeperiods
(timeperiodid, Description, timestart, timeend, TouchedBy, CreatedBy) VALUES
(1,'Daily', '08:00:00', '16:00:00', 'dgoedken', 'dgoedken'),
(2,'Weekends Only', '12:00:00', '16:00:00', 'dgoedken', 'dgoedken');

INSERT INTO tickettimes
(ticketid, timeperiodid, valueusediff, TouchedBy, CreatedBy) VALUES
(1, 1, 2.50, 'dgoedken', 'dgoedken'),
(2, 2, 4.50, 'dgoedken', 'dgoedken');


INSERT INTO validdays
(description, validday, touchedby, CreatedBy) VALUES
('Weekends', '1,7', 'dgoedken', 'dgoedken'),
('Mon-Fri', '2,3,4,5', 'dgoedken', 'dgoedken');


UPDATE tickets SET dateeffective = '2014-03-31', dateexpires = '2014-07-31';