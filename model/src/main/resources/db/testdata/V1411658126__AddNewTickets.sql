/**************************************Create two new Ticket**************************************/
/*$50 Stored Value Ticket*/
INSERT INTO tickets
(TicketID,
Description,
DescriptionLong,
TermsConditions,
Price,
Value,
ValueUse,
TicketTypeID,
ModifierID,
ValidDayID,
ServiceLevelID,
ZoneID,
DateEffective,
DateExpires,
ReplenishPrice,
ReplenishValue,
ReplenishThreshold,
Status,
TouchedBy,
CreatedBy)
VALUES
(null,
'$50 Stored Value',
null,
null,
50,
50,
1.5,
1,
null,
null,
1,
null,
'2014-03-31',
'2016-12-31',
null,
null,
null,
0,
'root@localhost',
'root@localhost');

/*CREATE offerings*/
INSERT INTO offerings
(OfferingID,
ParentOfferingID,
Description,
UnitPrice,
WalletTypeID,
TicketID,
IsBulk,
TaxRateID,
QuantityMax,
DateEffective,
DateExpires,
Status,
TouchedBy,
CreatedBy)
VALUES
(null,
null,
'$50 Stored Value',
1.5,
null,
LAST_INSERT_ID(),
'T',
1,
500,
'2014-03-31',
'2016-12-31',
null,
'root@localhost',
'root@localhost');

/*associate channel and offerings*/
INSERT INTO channelofferings
(ChannelID,
OfferingID,
TouchedBy,
CreatedBy)
VALUES
((SELECT ChannelID from Channels where description = 'eFare'),
(SELECT OfferingID from Offerings  where description = '$50 Stored Value'),
'admin',
'admin');

/*associate tickets and farecodes*/
INSERT INTO ticketsfarecodes
(TicketID,
FarecodeID,
TouchedBy,
CreatedBy)
VALUES
((SELECT TicketID from Tickets  where description = '$50 Stored Value'),
(SELECT farecodeid from farecodes where description = 'Full'),
'admin',
'admin');


/**********************$50 Half Fare Stored Value Ticket**********************/
INSERT INTO tickets
(TicketID,
Description,
DescriptionLong,
TermsConditions,
Price,
Value,
ValueUse,
TicketTypeID,
ModifierID,
ValidDayID,
ServiceLevelID,
ZoneID,
DateEffective,
DateExpires,
ReplenishPrice,
ReplenishValue,
ReplenishThreshold,
Status,
TouchedBy,
CreatedBy)
VALUES
(null,
'$50 Half Fare Stored Value',
null,
null,
50,
50,
0.75,
1,
null,
null,
1,
null,
'2014-03-31',
'2016-12-31',
null,
null,
null,
0,
'root@localhost',
'root@localhost');

/*CREATE offerings*/
INSERT INTO offerings
(OfferingID,
ParentOfferingID,
Description,
UnitPrice,
WalletTypeID,
TicketID,
IsBulk,
TaxRateID,
QuantityMax,
DateEffective,
DateExpires,
Status,
TouchedBy,
CreatedBy)
VALUES
(null,
null,
'$50 Half Fare Stored Value',
0.75,
null,
LAST_INSERT_ID(),
'T',
1,
500,
'2014-03-31',
'2016-12-31',
null,
'root@localhost',
'root@localhost');

/*associate channel and offerings*/
INSERT INTO channelofferings
(ChannelID,
OfferingID,
TouchedBy,
CreatedBy)
VALUES
((SELECT ChannelID from Channels where description = 'eFare'),
(SELECT OfferingID from Offerings  where description = '$50 Half Fare Stored Value'),
'admin',
'admin');

/*associate tickets and farecodes*/
INSERT INTO ticketsfarecodes
(TicketID,
FarecodeID,
TouchedBy,
CreatedBy)
VALUES
((SELECT TicketID from Tickets where description = '$50 Half Fare Stored Value'),
(SELECT farecodeid from farecodes where description = 'Senior'),
'admin',
'admin');