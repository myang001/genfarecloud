insert into documents (Category, `Key`, Value, LanguageID, TouchedBy, CreatedBy) VALUES ('faq', 'my.key', 'my key value', 1, 'admin', 'admin');

insert into documents (Category, `Key`, Value, LanguageID, TouchedBy, CreatedBy) VALUES ('faq', 'my.key', 'my key value in another language', 2, 'admin', 'admin');

insert into documents (`Key`, Value, LanguageID, TouchedBy, CreatedBy) VALUES ('my.key', 'my key value in another language', 2, 'admin', 'admin');



insert into properties (`Key`, Value, LanguageID, TouchedBy, CreatedBy) VALUES ('my.key', 'my key value', 1, 'admin', 'admin');

insert into properties (`Key`, Value, LanguageID, TouchedBy, CreatedBy) VALUES ('my.key', 'my key value in another language', 2, 'admin', 'admin');

insert into properties (`Key`, Value, LanguageID, TouchedBy, CreatedBy) VALUES ('replace.card.reason0', 'Lost/Stolen $5.55', 1, 'admin', 'admin');

insert into properties (`Key`, Value, LanguageID, TouchedBy, CreatedBy) VALUES ('replace.card.reason1', 'Damaged $3.33', 1, 'admin', 'admin');

insert into properties (`Key`, Value, LanguageID, TouchedBy, CreatedBy) VALUES ('replace.card.reason3', 'Expired $2.22', 1, 'admin', 'admin');

