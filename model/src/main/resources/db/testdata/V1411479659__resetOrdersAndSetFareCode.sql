/*Delete all order/fulfillment data and start from scratch*/
delete from activitydetails		where activityid > 0;
delete from pendingactivity		where orderid > 0;
delete from shipments			where orderid > 0;
delete from fulfillmentitems	where orderid > 0;
delete from fulfillments		where orderid > 0;
delete from orderitems			where orderid > 0;
delete from payments			where orderid > 0;
delete from orders				where orderid > 0;

ALTER TABLE activitydetails 	AUTO_INCREMENT = 1;
ALTER TABLE pendingactivity 	AUTO_INCREMENT = 1;
ALTER TABLE shipments 			AUTO_INCREMENT = 1;
ALTER TABLE fulfillmentitems 	AUTO_INCREMENT = 1;
ALTER TABLE fulfillments 		AUTO_INCREMENT = 1;
ALTER TABLE orderitems 			AUTO_INCREMENT = 1;
ALTER TABLE payments 			AUTO_INCREMENT = 1;
ALTER TABLE orders 				AUTO_INCREMENT = 1;

/* 
 * Create order data:
 * 
 * Order 1: Pending Fulfillment
 * --------------------------------
 * item0: Wallet 3,  3.00 -- unit price  3.00, amount  30.00, quantity 10
 * item1: Ticket 9, 13.00 -- unit price 13.00, amount 130.00, quantity 1,  parentQuantity 10
 * Order Amount: 160.00
 * 
 * Order 2: Pending Fulfillment
 * --------------------------------
 * item0: Wallet 3,  3.00 -- unit price  3.00, amount  30.00, quantity 10
 * item1: Ticket 5, 55.00 -- unit price 55.00, amount:550.00, quantity  1, parentQuantity 10
 * 
 * Order 3: Pending Autoload
 * --------------------------------
 * item0: NO TYPE Wallet 3,   3.00 -- unit price  3.00, amount  30.00, quantity 1
 * item1: 		  Ticket 4, 10.00  -- unit price 10.00, amount: 10.00, quantity 1, parentQuantity 1
 * Order Amount: 10.00
 * 
 * Order 4: Completed
 * --------------------------------
 * item0: Wallet 3,  3.00 -- unit price  3.00, amount 15.00, quantity 5
 * item1: Ticket 9, 13.00 -- unit price 13.00, amount 65.00, quantity 1,  parentQuantity 5
 * Order Amount: 80.00
 * 
 * Order 5: Canceled
 * --------------------------------
 * item0: Wallet 3,  3.00 -- unit price  3.00, amount  15.00, quantity 5
 * item1: Ticket 5, 55.00 -- unit price 55.00, amount 225.00, quantity 1, parentQuantity 5
 * Order Amount: 240.00
 * 
 */

/*----------------------------------------*/
/*Insert Pending Fulfillment Orders*/
/*----------------------------------------*/
/* ORDER 1*/
INSERT INTO orders (DateOrdered,  AmountOrder,	PersonID,	ShipperID,	OrderTypeID,	Status,	PaymentStatus,	TouchedBy,		CreatedBy,		OrderData) 
VALUES			   ('2014-08-20', 160.00,		208,		2,			1,				8,		11,				'localhost',	'localhost',	'{"lastName":"Bobkins","Type":"Shipping Address","Address":{"postalCode":"60000","state":"Illinois","line1":"123 Bob St","country":"United States","city":"Bobbins","line2":""},"firstName":"Bob"}');

INSERT INTO orderitems 
		 (	OrderID,							OrderItemNumber,	orderitemactionid,	wallettypeid,	Quantity,	ParentItemNumber,	FarecodeID,	ticketID,	UnitPrice,	Amount,	Status,	parentQuantity, orderitemtypeid, touchedby, createdby) 
VALUES 
		 (	(select max(orderid) from orders),	0,					1,					3,				10,			0,					1,			null,		3.00,		30.00,	2,		1,				3,				'admin', 'admin'),
		 (	(select max(orderid) from orders),	1,					1,					null,			1,			0,					1,			9,			13.00,		130.00,	2,		10,				1,				'admin', 'admin');

 
/* ORDER 2*/
INSERT INTO orders (DateOrdered,  AmountOrder,	PersonID,	ShipperID,	OrderTypeID,	Status,	PaymentStatus,	TouchedBy,		CreatedBy,		OrderData) 
VALUES			   ('2014-08-22', 580.00,		null,		1,			1,				8,		11,				'localhost',	'localhost',	'{"lastName":"Bipperton","Type":"Shipping Address","Address":{"postalCode":"60000","state":"Illinois","line1":"123 Bipper St","country":"United States","city":"Bippertonville","line2":""},"firstName":"Bippy"}');

INSERT INTO orderitems 
		 (	OrderID,							OrderItemNumber,	orderitemactionid,	wallettypeid,	Quantity,	ParentItemNumber,	FarecodeID,	ticketID,	UnitPrice,	Amount,	Status,	parentQuantity, orderitemtypeid, touchedby, createdby) 
VALUES 
		 (	(select max(orderid) from orders),	0,					1,					3,				10,			0,					1,			null,		3.00,		30.00,	2,		1,				3,				'admin', 'admin'),
		 (	(select max(orderid) from orders),	1,					1,					null,			1,			0,					1,			5,			55.00,		550.00,	2,		10,				1,				'admin', 'admin');




/*----------------------------------------*/
/*Insert Pending Activity Orders*/
/*----------------------------------------*/
/* ORDER 3*/
INSERT INTO orders (DateOrdered,  AmountOrder, PersonID,	ShipperID,	OrderTypeID,	Status,	PaymentStatus,	TouchedBy,		CreatedBy,		OrderData) 
VALUES			   ('2014-07-28', 10.00,       212,			2,			1,				8,		11,				'localhost',	'localhost',	'{"lastName":"Bobkins","Type":"Shipping Address","Address":{"postalCode":"60000","state":"Illinois","line1":"123 Bob St","country":"United States","city":"Bobbins","line2":""},"firstName":"Bob"}');

INSERT INTO orderitems 
		 (	OrderID,							OrderItemNumber,	orderitemactionid,	Quantity,	ParentItemNumber,	WalletID,							FarecodeID,	ticketID,	UnitPrice,	Amount,	Status,	parentQuantity, orderitemtypeid, touchedby, createdby) 
VALUES 
		 (	(select max(orderid) from orders),	0,					3,					1,			0,				(select max(WalletID) from wallets),	1,			null,		3.00,		3.00,	2,		1,				2,				'admin', 'admin'),
		 (	(select max(orderid) from orders),	1,					1,					1,			0,					null,								1,			4,			10.00,		10.00,	2,		1,				1,				'admin', 'admin');

INSERT INTO pendingactivity 
		(OrderID,							OrderItemNumber,	WalletID,	Slot,	TicketID,	LoadSequence,	ActivityTypeID, TouchedBy, CreatedBy)
values  ((select max(orderid) from orders), 1,					316,		1,		4,			1,				3,				'admin', 'admin');




/*----------------------------------------*/
/*Insert Completed Orders*/
/*----------------------------------------*/
/* Order 4*/
INSERT INTO orders (DateOrdered,  AmountOrder, PersonID,	ShipperID,	OrderTypeID,	Status,	PaymentStatus,	TouchedBy,		CreatedBy,		OrderData) 
VALUES			   ('2014-07-20', 80.00,       null,		1,			1,				6,		11,				'localhost',	'localhost',	'{"lastName":"Bobkins","Type":"Shipping Address","Address":{"postalCode":"60000","state":"Illinois","line1":"123 Bob St","country":"United States","city":"Bobbins","line2":""},"firstName":"Bob"}');

INSERT INTO orderitems 
		 (	OrderID,							OrderItemNumber,	orderitemactionid,	wallettypeid,	Quantity,	ParentItemNumber,	FarecodeID,	ticketID,	UnitPrice,	Amount,	Status,	parentQuantity, orderitemtypeid, touchedby, createdby) 
VALUES 
		 (	(select max(orderid) from orders),	0,					1,					3,				5,			0,					1,			null,		3.00,		15.00,	6,		1,				3,				'admin', 'admin'),
		 (	(select max(orderid) from orders),	1,					1,					null,			1,			0,					1,			9,			13.00,		65.00,	6,		1,				1,				'admin', 'admin');


/*----------------------------------------*/
/*Insert Canceled Orders*/
/*----------------------------------------*/
/* Order 5*/
INSERT INTO orders (DateOrdered,  AmountOrder, PersonID,	ShipperID,	OrderTypeID,	Status,	PaymentStatus,	TouchedBy,		CreatedBy,		OrderData) 
VALUES			   ('2014-07-12', 240.00,       null,		1,			1,				7,		11,				'localhost',	'localhost',	'{"lastName":"Bobkins","Type":"Shipping Address","Address":{"postalCode":"60000","state":"Illinois","line1":"123 Bob St","country":"United States","city":"Bobbins","line2":""},"firstName":"Bob"}');

INSERT INTO orderitems 
		 (	OrderID,							OrderItemNumber,	orderitemactionid,	wallettypeid,	Quantity,	ParentItemNumber,	FarecodeID,	ticketID,	UnitPrice,	Amount,	Status,	parentQuantity, orderitemtypeid, touchedby, createdby) 
VALUES 
	 (	(select max(orderid) from orders),	0,					1,					3,				5,			0,						1,			null,		3.00,		15.00,	7,		1,				3,				'admin', 'admin'),
		 (	(select max(orderid) from orders),	1,					1,					null,			1,			0,					1,			5,			55.00,		225.00,	7,		1,				1,				'admin', 'admin');


/*----------------------------------------*/
/*Insert Fulfillment Data*/
/*----------------------------------------*/
INSERT INTO fulfillments 
		(OrderID,	OrderItemNumber,	Quantity,	QuantityEncoded,	QuantityShipped,	Status,	TouchedBy, TouchedAt, CreatedBy, CreatedAt)
values
		(1,			0,					10,			0,					0,					8,		'admin','2014-06-16 20:56:53','admin','2014-06-16 20:56:53'),
		(1,			1,					1,			0,					0,					8,		'admin','2014-06-16 20:56:53','admin','2014-06-16 20:56:53'),
		(2,			0,					10,			0,					0,					8,		'admin','2014-06-16 20:56:53','admin','2014-06-16 20:56:53'),
		(2,			1,					1,			0,					0,					8,		'admin','2014-06-16 20:56:53','admin','2014-06-16 20:56:53'),
		(4,			0,					5,			5,					5,					6,		'admin','2014-06-16 20:56:53','admin','2014-06-16 20:56:53'),
		(4,			1,					1,			0,					0,					8,		'admin','2014-06-16 20:56:53','admin','2014-06-16 20:56:53');

INSERT INTO fulfillmentitems 
		(OrderID,	OrderItemNumber,	WalletID,								IsShipped,	TouchedBy, TouchedAt, CreatedBy, CreatedAt)
VALUES
		(4,			0,					(select max(walletId) from wallets),	'T',		'admin','2014-06-16 20:56:53','admin','2014-06-16 20:56:53'),
		(4,			0,					(select max(walletId)-1 from wallets),	'T',		'admin','2014-06-16 20:56:53','admin','2014-06-16 20:56:53'),
		(4,			0,					(select max(walletId)-2 from wallets),	'T',		'admin','2014-06-16 20:56:53','admin','2014-06-16 20:56:53'),
		(4,			0,					(select max(walletId)-3 from wallets),	'T',		'admin','2014-06-16 20:56:53','admin','2014-06-16 20:56:53'),
		(4,			0,					(select max(walletId)-4 from wallets),	'T',		'admin','2014-06-16 20:56:53','admin','2014-06-16 20:56:53');

/*
 * re-insert data to activity tables
INSERT INTO pendingactivity
(OrderID, OrderItemNumber, WalletID, Slot, TicketID, FarecodeID, ActivityTypeID, TouchedBy, CreatedBy) VALUES
('1', '0', '315', '10', '5', '1', '1', 'dgoedken', 'dgoedken'),
('1', '1', '315', '10', '6', '1', '1', 'dgoedken', 'dgoedken');
 */

INSERT INTO activitydetails
(ActivityID, Sequence, CardIdentifier, Slot, TicketID, FarecodeID, AmountCharge, AmountRemaining, AmountTendered, AmountReturned, ActivityTypeID, PendingID, PendingStatus, Longtitude, Latitude, DateActivity, TouchedBy, CreatedBy) VALUES
('2',		'0',		'10001110101', '1', '27',		'1',			'5.50',		'42.00',		null,		null,				'1',			(select max(pendingID) from pendingactivity), null, null, null, null, 'dgoedken', 'dgoedken'),
('2', '1', '10001110101', '1', '27', '1', '2.00', '40.00', null, null, '1', (select max(pendingID) from pendingactivity), null, null, null, null, 'dgoedken', 'dgoedken'),
('2', '2', '10001110101', '1', '27', '1', '2.00', '38.00', null, null, '1', (select max(pendingID) from pendingactivity), null, null, null, null, 'dgoedken', 'dgoedken');
