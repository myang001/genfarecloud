-- DROP PROCEDURE IF EXISTS transaction_sp;
-- DELIMITER $$
-- CREATE PROCEDURE transaction_sp() BEGIN
START TRANSACTION; 
-- DECLARE poid INT;
-- DECLARE EXIT HANDLER for SQLEXCEPTION, SQLWARNING 
-- BEGIN ROLLBACK; END

UPDATE walletcontents 
SET DiscountTypeID = '1' where isnull(DiscountTypeID);

INSERT INTO activitytypes
(ActivityTypeID,IsRevenue,IsRidership,IsPending,Description,TouchedBy,CreatedBy) values
('1', 'Y', 'Y', 'F', 'Ridership', 'dgoedken', 'dgoedken'),
('2', 'Y', 'Y', 'F', 'CardHistory', 'dgoedken', 'dgoedken');

INSERT INTO pendingactivity
(PendingID, OrderID, OrderItemNumber, WalletID, Slot, TicketID, DiscountTypeID, ActivityTypeID, TouchedBy, CreatedBy) VALUES
('1', '1', '0', '315', '10', '5', '1', '1', 'dgoedken', 'dgoedken'),
('2', '1', '1', '315', '10', '6', '1', '1', 'dgoedken', 'dgoedken');

INSERT INTO equipmenttypes
(EquipmentTypeID, Description, TouchedBy, CreatedBy) VALUES
('1', 'EquipmentType_Bus', 'dgoedken','dgoedken'),
('2', 'EquipmentType_Bus2', 'dgoedken', 'dgoedken'),
('3', 'EquipmentType_Bus3', 'dgoedken', 'dgoedken');

INSERT INTO manufacturers
(ManufacturerID,Description,TouchedBy,CreatedBy) VALUES
('1', 'Manufacturer_1', 'dgoedken', 'dgoedken'),
('2', 'Manufacturer_2', 'dgoedken', 'dgoedken'),
('3', 'Manufacturer_3', 'dgoedken', 'dgoedken'),
('4', 'Manufacturer_4', 'dgoedken', 'dgoedken'),
('5', 'Manufacturer_5', 'dgoedken', 'dgoedken');

INSERT INTO equipment
(EquipmentID, ContainerID, LocationID, ManufacturerID, EquipmentTypeID, Description, SerialNumber, Version, Status, TouchedBy, CreatedBy) VALUES
('1', null, '1', '1', '1', null, null, null, null, 'dgoedken', 'dgoedken'),
('2', null, '1', '2', '1', null, null, null, null, 'dgoedken', 'dgoedken'),
('3', null, '1', '3', '1', null, null, null, null, 'dgoedken', 'dgoedken'),
('4', null, '1', '4', '1', null, null, null, null, 'dgoedken', 'dgoedken'),
('5', null, '1', '5', '1', null, null, null, null, 'dgoedken', 'dgoedken');

insert into operatortypes (operatortypeid, description, touchedby, createdby) values
('1', 'OperatorType_1', 'dgoedken', 'dgoedken'),
('2', 'OperatorType_2', 'dgoedken', 'dgoedken'),
('3', 'OperatorType_3', 'dgoedken', 'dgoedken'),
('4', 'OperatorType_4', 'dgoedken', 'dgoedken'),
('5', 'OperatorType_5', 'dgoedken', 'dgoedken');

delete from operators;
insert into operators
(operatorid, operatortypeid, touchedby, createdby) values
('1', '1', 'dgoedken', 'dgoedken'),
('2', '1', 'dgoedken', 'dgoedken'),
('3', '2', 'dgoedken', 'dgoedken'),
('4', '2', 'dgoedken', 'dgoedken'),
('5', '3', 'dgoedken', 'dgoedken');

INSERT INTO activities
(ActivityID, OperatorID, EquipmentID, RouteID, DateActivity, Description, TouchedBy, CreatedBy) VALUES
('1', '1', '1', null, '2014-01-14 00:00:00', 'Activity_1', 'dgoedken', 'dgoedken'),
('2', '1', '1', null, '2014-03-13 00:00:00', 'Activity_2', 'dgoedken', 'dgoedken'),
('3', '1', '1', null, '2014-05-11 00:00:00', 'Activity_3', 'dgoedken', 'dgoedken'),
('4', '1', '1', null, '2014-05-17 00:00:00', 'Activity_4', 'dgoedken', 'dgoedken'),
('5', '2', '1', null, '2014-01-14 00:00:00', 'Activity_1', 'dgoedken', 'dgoedken'),
('6', '2', '1', null, '2014-03-13 00:00:00', 'Activity_2', 'dgoedken', 'dgoedken'),
('7', '2', '1', null, '2014-05-11 00:00:00', 'Activity_3', 'dgoedken', 'dgoedken'),
('8', '2', '1', null, '2014-05-17 00:00:00', 'Activity_4', 'dgoedken', 'dgoedken');

INSERT INTO activitydetails
(ActivityID, Sequence, CardIdentifier, Slot, TicketID, DiscountTypeID, AmountCharge, AmountRemaining, AmountTendered, AmountReturned, ActivityTypeID, PendingID, PendingStatus, Longtitude, Latitude, DateActivity, TouchedBy, CreatedBy) VALUES
('2', '0', '10001110101', '1', '27', '1', '5.50', '42.00', null, null, '1', '2', null, null, null, null, 'dgoedken', 'dgoedken'),
('2', '1', '10001110101', '1', '27', '1', '2.00', '40.00', null, null, '1', '2', null, null, null, null, 'dgoedken', 'dgoedken'),
('2', '2', '10001110101', '1', '27', '1', '2.00', '38.00', null, null, '1', '2', null, null, null, null, 'dgoedken', 'dgoedken');

COMMIT;
-- END
-- $$

