INSERT INTO `orders` (`OrderID`,`DateOrdered`,`AmountOrder`,`Notes`,`PersonID`,
	`OrganizationID`,`ShipperID`,`OrderData`,`OrderTypeID`,`Status`,
	`PaymentStatus`,`TouchedBy`,`TouchedAt`,`CreatedBy`,`CreatedAt`)
VALUES (2,'2014-06-07 22:10:14',8.00,NULL,1,
	NULL,NULL,NULL,1,8,
	10,'admin','2014-06-13 20:56:53','admin','2014-06-13 20:56:53');

INSERT INTO `orders` (`OrderID`,`DateOrdered`,`AmountOrder`,`Notes`,`PersonID`,
	`OrganizationID`,`ShipperID`,`OrderData`,`OrderTypeID`,`Status`,
	`PaymentStatus`,`TouchedBy`,`TouchedAt`,`CreatedBy`,`CreatedAt`)
VALUES (3,'2014-06-13 22:10:14',73.00,NULL,1,
	NULL,NULL,NULL,1,8,
	10,'admin','2014-06-13 20:56:53','admin','2014-06-13 20:56:53');

INSERT INTO `orders` (`OrderID`,`DateOrdered`,`AmountOrder`,`Notes`,`PersonID`,
	`OrganizationID`,`ShipperID`,`OrderData`,`OrderTypeID`,`Status`,
	`PaymentStatus`,`TouchedBy`,`TouchedAt`,`CreatedBy`,`CreatedAt`)
VALUES (4,'2014-06-13 22:10:14',65.00,NULL,1,
	NULL,NULL,NULL,1,6,
	10,'admin','2014-06-13 20:56:53','admin','2014-06-13 20:56:53');

INSERT INTO `orders` (`OrderID`,`DateOrdered`,`AmountOrder`,`Notes`,`PersonID`,
	`OrganizationID`,`ShipperID`,`OrderData`,`OrderTypeID`,`Status`,
	`PaymentStatus`,`TouchedBy`,`TouchedAt`,`CreatedBy`,`CreatedAt`)
VALUES (5,'2014-06-15 22:10:14',73.00,NULL,1,
	NULL,NULL,NULL,1,8,
	10,'admin','2014-06-13 20:56:53','admin','2014-06-13 20:56:53');

INSERT INTO `orderitems` (`OrderID`,`OrderItemNumber`,`ParentItemNumber`,`OrderItemActionID`,`OfferingID`,
	`WalletTypeID`,`WalletID`,`TicketID`,`DiscountTypeID`,`Quantity`,`UnitPrice`,`Amount`,`AmountTax`,
	`Status`,`Notes`,`TouchedBy`,`TouchedAt`,`CreatedBy`,`CreatedAt`,`parentQuantity`)
VALUES (2,0,NULL,1,5,
	NULL,315,NULL,NULL,2,4.00,8.00,NULL,
	3,NULL,'admin','2014-05-09 20:56:53','admin','2014-05-09 20:56:53',1);

INSERT INTO `orderitems` (`OrderID`,`OrderItemNumber`,`ParentItemNumber`,`OrderItemActionID`,`OfferingID`,
	`WalletTypeID`,`WalletID`,`TicketID`,`DiscountTypeID`,`Quantity`,`UnitPrice`,`Amount`,`AmountTax`,
	`Status`,`Notes`,`TouchedBy`,`TouchedAt`,`CreatedBy`,`CreatedAt`,`parentQuantity`)
VALUES (3,0,NULL,1,5,
	NULL,315,NULL,NULL,2,4.00,8.00,NULL,
	3,NULL,'admin','2014-06-13 20:56:53','admin','2014-06-13 20:56:53',1);

INSERT INTO `orderitems` (`OrderID`,`OrderItemNumber`,`ParentItemNumber`,`OrderItemActionID`,`OfferingID`,
	`WalletTypeID`,`WalletID`,`TicketID`,`DiscountTypeID`,`Quantity`,`UnitPrice`,`Amount`,`AmountTax`,
	`Status`,`Notes`,`TouchedBy`,`TouchedAt`,`CreatedBy`,`CreatedAt`,`parentQuantity`)
VALUES (3,1,NULL,1,5,
	NULL,315,NULL,NULL,1,65.00,65.00,NULL,
	3,NULL,'admin','2014-06-13 20:56:53','admin','2014-06-13 20:56:53',1);

INSERT INTO `orderitems` (`OrderID`,`OrderItemNumber`,`ParentItemNumber`,`OrderItemActionID`,`OfferingID`,
	`WalletTypeID`,`WalletID`,`TicketID`,`DiscountTypeID`,`Quantity`,`UnitPrice`,`Amount`,`AmountTax`,
	`Status`,`Notes`,`TouchedBy`,`TouchedAt`,`CreatedBy`,`CreatedAt`,`parentQuantity`)
VALUES (4,0,NULL,1,5,
	NULL,315,NULL,NULL,1,65.00,65.00,NULL,
	6,NULL,'admin','2014-06-13 20:56:53','admin','2014-06-13 20:56:53',1);

INSERT INTO `orderitems` (`OrderID`,`OrderItemNumber`,`ParentItemNumber`,`OrderItemActionID`,`OfferingID`,
	`WalletTypeID`,`WalletID`,`TicketID`,`DiscountTypeID`,`Quantity`,`UnitPrice`,`Amount`,`AmountTax`,
	`Status`,`Notes`,`TouchedBy`,`TouchedAt`,`CreatedBy`,`CreatedAt`,`parentQuantity`)
VALUES (5,0,NULL,1,5,
	NULL,315,NULL,NULL,2,4.00,8.00,NULL,
	3,NULL,'admin','2014-06-13 20:56:53','admin','2014-06-13 20:56:53',1);

INSERT INTO `orderitems` (`OrderID`,`OrderItemNumber`,`ParentItemNumber`,`OrderItemActionID`,`OfferingID`,
	`WalletTypeID`,`WalletID`,`TicketID`,`DiscountTypeID`,`Quantity`,`UnitPrice`,`Amount`,`AmountTax`,
	`Status`,`Notes`,`TouchedBy`,`TouchedAt`,`CreatedBy`,`CreatedAt`,`parentQuantity`)
VALUES (5,1,NULL,1,5,
	NULL,315,NULL,NULL,1,65.00,65.00,NULL,
	3,NULL,'admin','2014-06-13 20:56:53','admin','2014-06-13 20:56:53',1);

update orderitems set status = 7 where orderid = 1;

INSERT INTO fulfillments (OrderID, OrderItemNumber, Quantity, QuantityEncoded, QuantityShipped,
	Status, TouchedBy, TouchedAt, CreatedBy, CreatedAt)
VALUES (1, 0, 1, 0, 0,
	7, 'admin','2014-06-16 20:56:53','admin','2014-06-16 20:56:53');
	
	INSERT INTO fulfillments (OrderID, OrderItemNumber, Quantity, QuantityEncoded, QuantityShipped,
	Status, TouchedBy, TouchedAt, CreatedBy, CreatedAt)
VALUES (1, 1, 2, 0, 0,
	7, 'admin','2014-06-16 20:56:53','admin','2014-06-16 20:56:53');

INSERT INTO fulfillments (OrderID, OrderItemNumber, Quantity, QuantityEncoded, QuantityShipped,
	Status, TouchedBy, TouchedAt, CreatedBy, CreatedAt)
VALUES (2, 0, 2, 0, 0,
	2, 'admin','2014-06-16 20:56:53','admin','2014-06-16 20:56:53');

INSERT INTO fulfillments (OrderID, OrderItemNumber, Quantity, QuantityEncoded, QuantityShipped,
	Status, TouchedBy, TouchedAt, CreatedBy, CreatedAt)
VALUES (3, 0, 2, 2, 1,
	8, 'admin','2014-06-16 20:56:53','admin','2014-06-16 20:56:53');

INSERT INTO fulfillments (OrderID, OrderItemNumber, Quantity, QuantityEncoded, QuantityShipped,
	Status, TouchedBy, TouchedAt, CreatedBy, CreatedAt)
VALUES (3, 1, 1, 0, 0,
	8, 'admin','2014-06-16 20:56:53','admin','2014-06-16 20:56:53');

INSERT INTO fulfillments (OrderID, OrderItemNumber, Quantity, QuantityEncoded, QuantityShipped,
	Status, TouchedBy, TouchedAt, CreatedBy, CreatedAt)
VALUES (4, 0, 1, 1, 1,
	6, 'admin','2014-06-16 20:56:53','admin','2014-06-16 20:56:53');

INSERT INTO fulfillments (OrderID, OrderItemNumber, Quantity, QuantityEncoded, QuantityShipped,
	Status, TouchedBy, TouchedAt, CreatedBy, CreatedAt)
VALUES (5, 0, 2, 2, 1,
	8, 'admin','2014-06-16 20:56:53','admin','2014-06-16 20:56:53');

INSERT INTO fulfillments (OrderID, OrderItemNumber, Quantity, QuantityEncoded, QuantityShipped,
	Status, TouchedBy, TouchedAt, CreatedBy, CreatedAt)
VALUES (5, 1, 1, 1, 0,
	8, 'admin','2014-06-16 20:56:53','admin','2014-06-16 20:56:53');

INSERT INTO fulfillmentitems (OrderID, OrderItemNumber, Identifier, IsShipped,
	WalletStatusID, SlotStatus, TouchedBy, TouchedAt, CreatedBy, CreatedAt)
VALUES(3, 0, '301', 'T',
	6, 6, 'admin','2014-06-16 20:56:53','admin','2014-06-16 20:56:53');

INSERT INTO fulfillmentitems (OrderID, OrderItemNumber, Identifier, IsShipped,
	WalletStatusID, SlotStatus, TouchedBy, TouchedAt, CreatedBy, CreatedAt)
VALUES(3, 0, '302', 'F',
	6, 6, 'admin','2014-06-16 20:56:53','admin','2014-06-16 20:56:53');

INSERT INTO fulfillmentitems (OrderID, OrderItemNumber, Identifier, IsShipped,
	WalletStatusID, SlotStatus, TouchedBy, TouchedAt, CreatedBy, CreatedAt)
VALUES(4, 0, '401', 'T',
	6, 6, 'admin','2014-06-16 20:56:53','admin','2014-06-16 20:56:53');

INSERT INTO fulfillmentitems (OrderID, OrderItemNumber, Identifier, IsShipped,
	WalletStatusID, SlotStatus, TouchedBy, TouchedAt, CreatedBy, CreatedAt)
VALUES(5, 0, '501', 'F',
	6, 6, 'admin','2014-06-16 20:56:53','admin','2014-06-16 20:56:53');

INSERT INTO fulfillmentitems (OrderID, OrderItemNumber, Identifier, IsShipped,
	WalletStatusID, SlotStatus, TouchedBy, TouchedAt, CreatedBy, CreatedAt)
VALUES(5, 0, '502', 'T',
	6, 6, 'admin','2014-06-16 20:56:53','admin','2014-06-16 20:56:53');

INSERT INTO fulfillmentitems (OrderID, OrderItemNumber, Identifier, IsShipped,
	WalletStatusID, SlotStatus, TouchedBy, TouchedAt, CreatedBy, CreatedAt)
VALUES(5, 1, '511', 'F',
	6, 6, 'admin','2014-06-16 20:56:53','admin','2014-06-16 20:56:53');
	
INSERT INTO peopleaddresses (`PersonID`, `AddressTypeID`, `AddressLine1`, `AddressLine2`, `City`,
	`State`, `PostalCode`, `Country`, `TouchedBy`, `TouchedAt`, `CreatedBy`)
	VALUES ('1', '2', '123 Shipping St', 'Suite 321', 'Shipsody',
	'CA', '90048', 'US', 'root@localhost', '2014-03-26 16:52:23', 'root@localhost');