--Stored Value Liability
CREATE TABLE temp_rp038 (TicketDesc VARCHAR(50), ValueRemaining DECIMAL(9,2), DiscountType VARCHAR(50), Status VARCHAR(50));
INSERT INTO temp_rp038 VALUES("3 Day Pass", 12500 , "Full Fare", "Inactive");
INSERT INTO temp_rp038 VALUES("Daily Pass", 12500 , "Half Fare", "Inactive");
INSERT INTO temp_rp038 VALUES("Period Pass", 16250 , "Student", "Inactive");
INSERT INTO temp_rp038 VALUES("Stepped Value", 45492.35 , "Full Fare", "Inactive");
INSERT INTO temp_rp038 VALUES("Stored Value", 45492.35 , "Half Fare", "Inactive");

CREATE OR REPLACE VIEW StoredValueLiability AS
SELECT TicketDesc, ValueRemaining, DiscountType, Status FROM temp_rp038;