-- Fraud Detection Report - 3 rows
CREATE TABLE temp_rp016 (walletIdentifier BIGINT(20), TicketDescription VARCHAR(50), MediaType VARCHAR(50), Discount VARCHAR(50), AmtCharged VARCHAR(50), AmtRemaining VARCHAR(50), ActivityType VARCHAR(50),
 ViolationType VARCHAR(50), ValueActivity VARCHAR(50), EquipmentDesc VARCHAR(50), ActivityDate DATETIME, Location VARCHAR(50));
INSERT INTO temp_rp016 VALUES(115, "Local" , "Smart Card", "Full Fare" , "1.00", "199.99", "Tap", "Multi-Use", "1.00", "Bus 126", "2014-08-01", "+42.644298 -73.762894");
INSERT INTO temp_rp016 VALUES(115, "Regional" , "Mobile", "Half Fare" , null, "1.75", "Tap", "Not Issued", "1.75", "Bus 126", "2014-08-15", "+42.644298 -73.762894");
INSERT INTO temp_rp016 VALUES(115, "Parking" , "Mobile", "Student" , "10.00", "12.50", "Load", "Excessive Use", "10.00", "RPoS 432", "2014-08-16", "+42.644298 -73.762894");
