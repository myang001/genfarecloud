INSERT INTO LocationTypes (LocationTypeID,Description,TouchedBy,CreatedBy) VALUES(5,'ResourceLocation','admin','admin');

INSERT INTO Locations (LocationID,Description,LocationTypeID,TouchedBy,CreatedBy) VALUES(9,'ABCDEFG09',5,'admin','admin');
INSERT INTO Locations (LocationID,Description,LocationTypeID,TouchedBy,CreatedBy) VALUES(10,'ABCDEFG10',5,'admin','admin');
INSERT INTO Locations (LocationID,Description,LocationTypeID,TouchedBy,CreatedBy) VALUES(11,'ABCDEFG11',5,'admin','admin');
INSERT INTO Locations (LocationID,Description,LocationTypeID,TouchedBy,CreatedBy) VALUES(12,'ABCDEFG12',5,'admin','admin');

insert into equipmenttypes
(EquipmentTypeID, Description, TouchedBy, CreatedBy) VALUES
('4', 'APOS', 'root@localhost', 'root@localhost'),
('5', 'RPOS', 'root@localhost', 'root@localhost'),
('6', 'FAREBOX', 'root@localhost', 'root@localhost');

-- Create some RPOS/APOS equipment

INSERT INTO equipment
(EquipmentID, ContainerID, LocationID, ManufacturerID, EquipmentTypeID, Description, SerialNumber, Version, Status, OrganizationID, TouchedBy, CreatedBy) VALUES
('6', null, '9', '1', '4', "APOS Box1", '0000000001', '1.0', '2', '2', 'root@localhost', 'root@localhost'),
('7', null, '10', '1', '4', "APOS Box2", '0000000002', '1.0', '2', '2', 'root@localhost', 'root@localhost'),
('8', null, '11', '1', '5', "RPOS Box1", '0000000003', '1.0', '2', '2', 'root@localhost', 'root@localhost'),
('9', null, '12', '1', '5', "RPOS Box2", '0000000004', '1.0', '2', '2', 'root@localhost', 'root@localhost');

INSERT INTO Config
(ConfigID, ConfigKey, ConfigValue, TouchedBy, CreatedBy) VALUES
('1', 'MaxUnsyncedTransactionNumber', '10', 'root@localhost', 'root@localhost'),
('2', 'MaxUnsyncedTransactionValue', '1000', 'root@localhost', 'root@localhost'),
('3', 'MaxUnsyncedTransactionNumber', '15' ,'root@localhost', 'root@localhost'),
('4', 'MaxUnsyncedTransactionValue', '1500', 'root@localhost', 'root@localhost');

INSERT INTO EquipmentConfig
(ConfigID, EquipmentID, TouchedBy, CreatedBy) VALUES
('1', '6', 'root@localhost', 'root@localhost'),
('2', '6', 'root@localhost', 'root@localhost'),
('1', '7', 'root@localhost', 'root@localhost'),
('2', '7', 'root@localhost', 'root@localhost'),
('3', '8', 'root@localhost', 'root@localhost'),
('4', '8', 'root@localhost', 'root@localhost'),
('3', '9', 'root@localhost', 'root@localhost'),
('4', '9', 'root@localhost', 'root@localhost');

INSERT INTO privileges
(PrivilegeID, ComponentID, Permission, Name, Description, TouchedBy, CreatedBy) VALUES
('13', '1', 'PERM_ENGINEER_APOS_EQUIP_ENGI', 'APOS Equipment Engineer', 'Allows user access to APOS Equipment', 'root@localhost', 'root@localhost'),
('14', '1', 'PERM_ENGINEER_RPOS_EQUIP_ENGI', 'RPOS Equipment Engineer', 'Allows user access to RPOS Equipment', 'root@localhost', 'root@localhost');

INSERT INTO groups
(GroupID, OrganizationID, Role, Name, Description, Status, TouchedBy, CreatedBy) VALUES
('20', '2', 'ROLE_APOS_ENGI','APOS Equipment Management', 'Allows access to APOS Equipment', '1', 'root@localhost', 'root@localhost'),
('21', '2', 'ROLE_RPOS_ENGI', 'RPOS Equipment Management', 'Allows access to RPOS Equipment', '1', 'root@localhost', 'root@localhost');

INSERT INTO groupprivileges
(GroupID, PrivilegeID, TouchedBy, CreatedBy) VALUES
('20', '13', 'root@localhost', 'root@localhost'),
('21', '14', 'root@localhost', 'root@localhost');

INSERT INTO EquipmentCollections
(EquipmentCollectionID, Name, Description, TouchedBy, CreatedBy) VALUES
('1', 'APOS Box Collection', 'APOS Boxes', 'root@localhost', 'root@localhost'),
('2', 'RPOS Box Colleciton', 'RPOS Boxes', 'root@localhost', 'root@localhost');

INSERT INTO equipmentcollectionsequipment
(EquipmentID, EquipmentCollectionID, TouchedBy, CreatedBy) VALUES
('6', '1', 'root@localhost', 'root@localhost'),
('7', '1', 'root@localhost', 'root@localhost'),
('8', '2', 'root@localhost', 'root@localhost'),
('9', '2', 'root@localhost', 'root@localhost');

INSERT INTO groupsequipmentcollections
(GroupID, EquipmentCollectionID, TouchedBy, CreatedBy) VALUES
('20', '1', 'root@localhost', 'root@localhost'),
('21', '2', 'root@localhost', 'root@localhost');

INSERT INTO operatortypes
(OperatorTypeID, Description, TouchedBy,CreatedBy) VALUES
('6', 'APOS Operator Admin', 'root@localhost', 'root@localhost'),
('7', 'RPOS Operator Admin', 'root@localhost', 'root@localhost');

INSERT INTO people
(PersonID,Prefix,NameLast,NameMiddle,NameFirst,Suffix,DateOfBirth,Gender,Status,LanguageID,TouchedBy,CreatedBy) VALUES
('216','','Billy','APOS','Engineer','','1990-01-01','M','1','1','root@localhost','root@localhost'),
('217','','Bobby','RPOS','Engineer','','1990-01-01','M','1','1','root@localhost','root@localhost');

INSERT INTO operators
(OperatorID, EmployeeID, pin, walletid, operatortypeid, TouchedBy, CreatedBy) VALUES
('216', null, '0000', null, '6',  'root@localhost', 'root@localhost'),
('217', null, '0000', null, '7',  'root@localhost', 'root@localhost');

INSERT INTO groupsofpeople
(GroupID, PersonID, TouchedBy, CreatedBy) VALUES
('20', '216', 'root@localhost', 'root@localhost'),
('21', '217', 'root@localhost', 'root@localhost');

