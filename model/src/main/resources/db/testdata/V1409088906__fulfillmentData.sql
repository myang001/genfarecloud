/*data for pending fulfillment & pending autoload*/
INSERT INTO orders (DateOrdered, AmountOrder, PersonID, OrderTypeID, Status, PaymentStatus, TouchedBy, CreatedBy) VALUES ('2014-08-20', 50.00, 1, 1, 8, 10, 'admin', 'admin');

INSERT INTO orderitems (OrderID, OrderItemNumber, orderitemactionid, wallettypeid, Quantity, ParentItemNumber, ticketID, UnitPrice, Amount, Status, parentQuantity, orderitemtypeid, touchedby, createdby) 
VALUES 
 ( (select max(orderid) from orders), 0, 1, 3, 10, null, null, 4.00, 4.00, 3, 1, 3, 'admin', 'admin'),
 ( (select max(orderid) from orders), 1, 1, null, 1, 0, 9, 4.00, 4.00, 3, 1, 1, 'admin', 'admin');


INSERT INTO orders (DateOrdered, AmountOrder, PersonID, OrderTypeID, Status, PaymentStatus, TouchedBy, CreatedBy) VALUES ('2014-08-20', 50.00, 1, 1, 8, 10, 'admin', 'admin');

INSERT INTO orderitems (OrderID, OrderItemNumber, orderitemactionid, Quantity, ParentItemNumber, UnitPrice, Amount, WalletID, TicketID, Status, parentQuantity, orderitemtypeid, touchedby, createdby) 
VALUES 
 ( (select max(orderid) from orders), 0, 3, 1, null, 4.00, 4.00, 316, null, 3, 1, 2, 'admin', 'admin'),
 ( (select max(orderid) from orders), 1, 1, 1, 0, 4.00, 4.00, null, 4, 3, 1, 1, 'admin', 'admin');

INSERT INTO pendingactivity (OrderID, OrderItemNumber, WalletID, Slot, TicketID, LoadSequence, ActivityTypeID, TouchedBy, CreatedBy)
values ( (select max(orderid) from orders), 1, 316, 1, 4, 1, 3, 'admin', 'admin');


update fulfillmentitems set walletid = (select max(walletid) from wallets) where walletid = 0;