CREATE TABLE temp_rp023 (LastName VARCHAR(50), FirstName VARCHAR(50), Status VARCHAR(50), Organization VARCHAR(50), Description VARCHAR(50), ContactInfo VARCHAR(50), DateLastAccessed DateTime);
INSERT INTO temp_rp023 VALUES("Smith", "John" , "Active", "CDTA" , "Finance", "jsmith@abc.com", "2014-08-05 03:14:07");
INSERT INTO temp_rp023 VALUES("Trip", "Bruce" , "Active", "CDTA" , "Driver", "btrip@abc.com", "2014-08-05 03:14:07");
INSERT INTO temp_rp023 VALUES("Zeno", "Zidane" , "Active", "CDTA" , "Maintenance", "ZZenos@abc.com", "2014-08-05 03:14:07");

CREATE OR REPLACE VIEW WebPortalUsers AS
SELECT LastName, FirstName, Status, Organization, Description, ContactInfo, DateLastAccessed FROM temp_rp023;
