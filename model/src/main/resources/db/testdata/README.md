Test Data Migrations
--------------------

This folder contains migrations for populating test data into a tenants 
database. These are only executed when the tenant is configured to 
`importTestData=true`. Each script must follow this syntax:

V[version number]__[description].sql

The version number must be relative to the regular database migration scripts.
In other words, a script that inserts people must have a version number later
than the script which creates the people table. The version number should be a
minor version. For example, a script that must run after the `V5` migration can
be prefixed with `V5.1`.
