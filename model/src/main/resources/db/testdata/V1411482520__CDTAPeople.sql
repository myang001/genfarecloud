/**********************************CREATE John Smith User with Full Fare**********************************/
INSERT INTO people (
PersonID,ParentID,Prefix,NameLast,NameMiddle,NameFirst,Suffix,DateOfBirth,Gender,Status,LanguageID,DisabilityID,TouchedBy,CreatedBy) 
VALUES (
(select max(p.personid) + 1 from people p),null,'',
'Smith',
'', 
'John',
'','1900-01-01','',1,1,null,'root@localhost','root@localhost');

INSERT INTO peopleaddresses (
PersonID,AddressTypeID,AddressLine1,AddressLine2,City,State,PostalCode,Country,TouchedBy,CreatedBy) 
VALUES (
(select max(personid) from people),
1,'NO:1 Halas Way','','Lake Forest','IL','60001','US','root@localhost','root@localhost');

INSERT INTO peoplelinks (PersonID,LinkTypeID,LinkValue,IsPreferred,TouchedBy,CreatedBy) 
VALUES (
(select max(personid) from people),5,'john.smith@spxefare.com','T','root@localhost','root@localhost');

INSERT INTO peoplelinks (PersonID,LinkTypeID,LinkValue,IsPreferred,TouchedBy,CreatedBy) 
VALUES (
(select max(personid) from people),1,'9176208480','T','root@localhost','root@localhost');

INSERT INTO logins (LoginID,PersonID,UserLogin,TouchedBy,CreatedBy) 
VALUES (
(select max(l.loginid) + 1 from logins l),(select max(personid) from people),'john.smith@spxefare.com','root@localhost','root@localhost');

INSERT INTO authentication (PersonID, AuthenticationTypeID, Response, TouchedBy, CreatedBy) 
VALUES (
(select max(personid) from people),(SELECT AuthenticationTypeID from AuthenticationTypes where Description='Password'),'$2a$10$tXpY5oz/EUbIasxTE5TG1eX8d7RyjoPxxJt/uDycO12dznNGsKCAu', 'unassigned','root@localhost');

INSERT INTO groupsofpeople (GroupID, PersonID, TouchedBy, CreatedBy) 
VALUES
((select g.GroupID from groups g where g.Role like '%ROLE_RIDER%'), (select max(personid) from people), 'root@localhost','root@localhost');

/*Add Full Fare*/
INSERT INTO PeopleFarecodes 
(PersonID, FarecodeID, TouchedBy, CreatedBy) 
VALUES
((select max(personid) from people),(select farecodeid from Farecodes where DESCRIPTION like '%Full%'),'root@localhost','root@localhost');

/**********************************CREATE Jane Doe User with Full Fare**********************************/
INSERT INTO people (
PersonID,ParentID,Prefix,NameLast,NameMiddle,NameFirst,Suffix,DateOfBirth,Gender,Status,LanguageID,DisabilityID,TouchedBy,CreatedBy) 
VALUES (
(select max(p.personid) + 1 from people p),null,'',
'Doe',
'', 
'Jane',
'','1900-01-01','',1,1,null,'root@localhost','root@localhost');

INSERT INTO peopleaddresses (
PersonID,AddressTypeID,AddressLine1,AddressLine2,City,State,PostalCode,Country,TouchedBy,CreatedBy) 
VALUES (
(select max(personid) from people),
1,'NO:1 Halas Way','','Lake Forest','IL','60001','US','root@localhost','root@localhost');

INSERT INTO peoplelinks (PersonID,LinkTypeID,LinkValue,IsPreferred,TouchedBy,CreatedBy) 
VALUES (
(select max(personid) from people),5,'jane.doe@spxefare.com','T','root@localhost','root@localhost');

INSERT INTO peoplelinks (PersonID,LinkTypeID,LinkValue,IsPreferred,TouchedBy,CreatedBy) 
VALUES (
(select max(personid) from people),1,'9176208480','T','root@localhost','root@localhost');

INSERT INTO logins (LoginID,PersonID,UserLogin,TouchedBy,CreatedBy) 
VALUES (
(select max(l.loginid) + 1 from logins l),(select max(personid) from people),'jane.doe@spxefare.com','root@localhost','root@localhost');

INSERT INTO authentication (PersonID, AuthenticationTypeID, Response, TouchedBy, CreatedBy) 
VALUES (
(select max(personid) from people),(SELECT AuthenticationTypeID from AuthenticationTypes where Description='Password'),'$2a$10$tXpY5oz/EUbIasxTE5TG1eX8d7RyjoPxxJt/uDycO12dznNGsKCAu', 'unassigned','root@localhost');

INSERT INTO groupsofpeople (GroupID, PersonID, TouchedBy, CreatedBy) 
VALUES
((select g.GroupID from groups g where g.Role like '%ROLE_RIDER%'), (select max(personid) from people), 'root@localhost','root@localhost');

/*Add Full Fare*/
INSERT INTO PeopleFarecodes 
(PersonID, FarecodeID, TouchedBy, CreatedBy) 
VALUES
((select max(personid) from people),(select farecodeid from Farecodes where DESCRIPTION like '%Full%'),'root@localhost','root@localhost');

/**********************************CREATE Larry Boy User with Full Fare**********************************/
/*BEGIN Create People*/
INSERT INTO people (
PersonID,ParentID,Prefix,NameLast,NameMiddle,NameFirst,Suffix,DateOfBirth,Gender,Status,LanguageID,DisabilityID,TouchedBy,CreatedBy) 
VALUES (
(select max(p.personid) + 1 from people p),null,'',
'Boy',
'', 
'Larry',
'','1900-01-01','',1,1,null,'root@localhost','root@localhost');

INSERT INTO peopleaddresses (
PersonID,AddressTypeID,AddressLine1,AddressLine2,City,State,PostalCode,Country,TouchedBy,CreatedBy) 
VALUES (
(select max(personid) from people),
1,'NO:1 Halas Way','','Lake Forest','IL','60001','US','root@localhost','root@localhost');

INSERT INTO peoplelinks (PersonID,LinkTypeID,LinkValue,IsPreferred,TouchedBy,CreatedBy) 
VALUES (
(select max(personid) from people),5,'larry.boy@spxefare.com','T','root@localhost','root@localhost');

INSERT INTO peoplelinks (PersonID,LinkTypeID,LinkValue,IsPreferred,TouchedBy,CreatedBy) 
VALUES (
(select max(personid) from people),1,'9176208480','T','root@localhost','root@localhost');
/*END Create People*/

/*BEGIN Create Login*/
INSERT INTO logins (LoginID,PersonID,UserLogin,TouchedBy,CreatedBy) 
VALUES (
(select max(l.loginid) + 1 from logins l),(select max(personid) from people),'larry.boy@spxefare.com','root@localhost','root@localhost');

INSERT INTO authentication (PersonID, AuthenticationTypeID, Response, TouchedBy, CreatedBy) 
VALUES (
(select max(personid) from people),(SELECT AuthenticationTypeID from AuthenticationTypes where Description='Password'),'$2a$10$tXpY5oz/EUbIasxTE5TG1eX8d7RyjoPxxJt/uDycO12dznNGsKCAu', 'unassigned','root@localhost');
/*END Create Login*/

/*Associated RIDER Role*/
INSERT INTO groupsofpeople (GroupID, PersonID, TouchedBy, CreatedBy) 
VALUES
((select g.GroupID from groups g where g.Role like '%ROLE_RIDER%'), (select max(personid) from people), 'root@localhost','root@localhost');

/*Add Full Fare*/
INSERT INTO PeopleFarecodes
(PersonID, FarecodeID, TouchedBy, CreatedBy) 
VALUES
((select max(personid) from people),(select farecodeid from Farecodes where DESCRIPTION like '%Full%'),'root@localhost','root@localhost');

/**********************************CREATE Micky Mouse User with Full Fare**********************************/
/*BEGIN Create People*/
INSERT INTO people (
PersonID,ParentID,Prefix,NameLast,NameMiddle,NameFirst,Suffix,DateOfBirth,Gender,Status,LanguageID,DisabilityID,TouchedBy,CreatedBy) 
VALUES (
(select max(p.personid) + 1 from people p),null,'',
'Mouse',
'', 
'Micky',
'','1900-01-01','',1,1,null,'root@localhost','root@localhost');

INSERT INTO peopleaddresses (
PersonID,AddressTypeID,AddressLine1,AddressLine2,City,State,PostalCode,Country,TouchedBy,CreatedBy) 
VALUES (
(select max(personid) from people),
1,'NO:1 Halas Way','','Lake Forest','IL','60001','US','root@localhost','root@localhost');

INSERT INTO peoplelinks (PersonID,LinkTypeID,LinkValue,IsPreferred,TouchedBy,CreatedBy) 
VALUES (
(select max(personid) from people),5,'micky.mouse@spxefare.com','T','root@localhost','root@localhost');

INSERT INTO peoplelinks (PersonID,LinkTypeID,LinkValue,IsPreferred,TouchedBy,CreatedBy) 
VALUES (
(select max(personid) from people),1,'9176208480','T','root@localhost','root@localhost');
/*END Create People*/

/*BEGIN Create Login*/
INSERT INTO logins (LoginID,PersonID,UserLogin,TouchedBy,CreatedBy) 
VALUES (
(select max(l.loginid) + 1 from logins l),(select max(personid) from people),'micky.mouse@spxefare.com','root@localhost','root@localhost');

INSERT INTO authentication (PersonID, AuthenticationTypeID, Response, TouchedBy, CreatedBy) 
VALUES (
(select max(personid) from people),(SELECT AuthenticationTypeID from AuthenticationTypes where Description='Password'),'$2a$10$tXpY5oz/EUbIasxTE5TG1eX8d7RyjoPxxJt/uDycO12dznNGsKCAu', 'unassigned','root@localhost');
/*END Create Login*/

/*Associated RIDER Role*/
INSERT INTO groupsofpeople (GroupID, PersonID, TouchedBy, CreatedBy) 
VALUES
((select g.GroupID from groups g where g.Role like '%ROLE_RIDER%'), (select max(personid) from people), 'root@localhost','root@localhost');

/*Add Full Fare*/
INSERT INTO PeopleFarecodes
(PersonID, FarecodeID, TouchedBy, CreatedBy) 
VALUES
((select max(personid) from people),(select farecodeid from Farecodes where DESCRIPTION like '%Full%'),'root@localhost','root@localhost');

/**********************************CREATE Lightning McQueen User with Full Fare**********************************/
/*BEGIN Create People*/
INSERT INTO people (
PersonID,ParentID,Prefix,NameLast,NameMiddle,NameFirst,Suffix,DateOfBirth,Gender,Status,LanguageID,DisabilityID,TouchedBy,CreatedBy) 
VALUES (
(select max(p.personid) + 1 from people p),null,'',
'McQueen',
'', 
'Lightning',
'','1900-01-01','',1,1,null,'root@localhost','root@localhost');

INSERT INTO peopleaddresses (
PersonID,AddressTypeID,AddressLine1,AddressLine2,City,State,PostalCode,Country,TouchedBy,CreatedBy) 
VALUES (
(select max(personid) from people),
1,'NO:1 Halas Way','','Lake Forest','IL','60001','US','root@localhost','root@localhost');

INSERT INTO peoplelinks (PersonID,LinkTypeID,LinkValue,IsPreferred,TouchedBy,CreatedBy) 
VALUES (
(select max(personid) from people),5,'Lightning.McQueen@efare.com','T','root@localhost','root@localhost');

INSERT INTO peoplelinks (PersonID,LinkTypeID,LinkValue,IsPreferred,TouchedBy,CreatedBy) 
VALUES (
(select max(personid) from people),1,'9176208480','T','root@localhost','root@localhost');
/*END Create People*/

/*BEGIN Create Login*/
INSERT INTO logins (LoginID,PersonID,UserLogin,TouchedBy,CreatedBy) 
VALUES (
(select max(l.loginid) + 1 from logins l),(select max(personid) from people),'Lightning.McQueen@efare.com','root@localhost','root@localhost');

INSERT INTO authentication (PersonID, AuthenticationTypeID, Response, TouchedBy, CreatedBy) 
VALUES (
(select max(personid) from people),(SELECT AuthenticationTypeID from AuthenticationTypes where Description='Password'),'$2a$10$tXpY5oz/EUbIasxTE5TG1eX8d7RyjoPxxJt/uDycO12dznNGsKCAu', 'unassigned','root@localhost');
/*END Create Login*/

/*Associated RIDER Role*/
INSERT INTO groupsofpeople (GroupID, PersonID, TouchedBy, CreatedBy) 
VALUES
((select g.GroupID from groups g where g.Role like '%ROLE_RIDER%'), (select max(personid) from people), 'root@localhost','root@localhost');

/*Add Full Fare*/
INSERT INTO PeopleFarecodes
(PersonID, FarecodeID, TouchedBy, CreatedBy) 
VALUES
((select max(personid) from people),(select farecodeid from Farecodes where DESCRIPTION like '%Full%'),'root@localhost','root@localhost');

/**********************************CREATE Kung Fu Panda User with Senior Fare**********************************/
/*BEGIN Create People*/
INSERT INTO people (
PersonID,ParentID,Prefix,NameLast,NameMiddle,NameFirst,Suffix,DateOfBirth,Gender,Status,LanguageID,DisabilityID,TouchedBy,CreatedBy) 
VALUES (
(select max(p.personid) + 1 from people p),null,'',
'Panda',
'', 
'Kung Fu',
'','1900-01-01','',1,1,null,'root@localhost','root@localhost');

INSERT INTO peopleaddresses (
PersonID,AddressTypeID,AddressLine1,AddressLine2,City,State,PostalCode,Country,TouchedBy,CreatedBy) 
VALUES (
(select max(personid) from people),
1,'NO:1 Halas Way','','Lake Forest','IL','60001','US','root@localhost','root@localhost');

INSERT INTO peoplelinks (PersonID,LinkTypeID,LinkValue,IsPreferred,TouchedBy,CreatedBy) 
VALUES (
(select max(personid) from people),5,'kung.panda@spxefare.com','T','root@localhost','root@localhost');

INSERT INTO peoplelinks (PersonID,LinkTypeID,LinkValue,IsPreferred,TouchedBy,CreatedBy) 
VALUES (
(select max(personid) from people),1,'9176208480','T','root@localhost','root@localhost');
/*END Create People*/

/*BEGIN Create Login*/
INSERT INTO logins (LoginID,PersonID,UserLogin,TouchedBy,CreatedBy) 
VALUES (
(select max(l.loginid) + 1 from logins l),(select max(personid) from people),'kung.panda@spxefare.com','root@localhost','root@localhost');

INSERT INTO authentication (PersonID, AuthenticationTypeID, Response, TouchedBy, CreatedBy) 
VALUES (
(select max(personid) from people),(SELECT AuthenticationTypeID from AuthenticationTypes where Description='Password'),'$2a$10$tXpY5oz/EUbIasxTE5TG1eX8d7RyjoPxxJt/uDycO12dznNGsKCAu', 'unassigned','root@localhost');
/*END Create Login*/

/*Associated RIDER Role*/
INSERT INTO groupsofpeople (GroupID, PersonID, TouchedBy, CreatedBy) 
VALUES
((select g.GroupID from groups g where g.Role like '%ROLE_RIDER%'), (select max(personid) from people), 'root@localhost','root@localhost');

/*Add Full Fare*/
INSERT INTO PeopleFarecodes
(PersonID, FarecodeID, TouchedBy, CreatedBy) 
VALUES
((select max(personid) from people),(select farecodeid from Farecodes where DESCRIPTION like '%Senior%'),'root@localhost','root@localhost');

/**********************************CREATE Snow White User with Senior Fare**********************************/
/*BEGIN Create People*/
INSERT INTO people (
PersonID,ParentID,Prefix,NameLast,NameMiddle,NameFirst,Suffix,DateOfBirth,Gender,Status,LanguageID,DisabilityID,TouchedBy,CreatedBy) 
VALUES (
(select max(p.personid) + 1 from people p),null,'',
'White',
'', 
'Snow',
'','1900-01-01','',1,1,null,'root@localhost','root@localhost');

INSERT INTO peopleaddresses (
PersonID,AddressTypeID,AddressLine1,AddressLine2,City,State,PostalCode,Country,TouchedBy,CreatedBy) 
VALUES (
(select max(personid) from people),
1,'NO:1 Halas Way','','Lake Forest','IL','60001','US','root@localhost','root@localhost');

INSERT INTO peoplelinks (PersonID,LinkTypeID,LinkValue,IsPreferred,TouchedBy,CreatedBy) 
VALUES (
(select max(personid) from people),5,'snow.white@spxefare.com','T','root@localhost','root@localhost');

INSERT INTO peoplelinks (PersonID,LinkTypeID,LinkValue,IsPreferred,TouchedBy,CreatedBy) 
VALUES (
(select max(personid) from people),1,'9176208480','T','root@localhost','root@localhost');
/*END Create People*/

/*BEGIN Create Login*/
INSERT INTO logins (LoginID,PersonID,UserLogin,TouchedBy,CreatedBy) 
VALUES (
(select max(l.loginid) + 1 from logins l),(select max(personid) from people),'snow.white@spxefare.com','root@localhost','root@localhost');

INSERT INTO authentication (PersonID, AuthenticationTypeID, Response, TouchedBy, CreatedBy) 
VALUES (
(select max(personid) from people),(SELECT AuthenticationTypeID from AuthenticationTypes where Description='Password'),'$2a$10$tXpY5oz/EUbIasxTE5TG1eX8d7RyjoPxxJt/uDycO12dznNGsKCAu', 'unassigned','root@localhost');
/*END Create Login*/

/*Associated RIDER Role*/
INSERT INTO groupsofpeople (GroupID, PersonID, TouchedBy, CreatedBy) 
VALUES
((select g.GroupID from groups g where g.Role like '%ROLE_RIDER%'), (select max(personid) from people), 'root@localhost','root@localhost');

/*Add Full Fare*/
INSERT INTO PeopleFarecodes
(PersonID, FarecodeID, TouchedBy, CreatedBy) 
VALUES
((select max(personid) from people),(select farecodeid from Farecodes where DESCRIPTION like '%Senior%'),'root@localhost','root@localhost');

/**********************************CREATE SpiderMan User with Senior Fare**********************************/
/*BEGIN Create People*/
INSERT INTO people (
PersonID,ParentID,Prefix,NameLast,NameMiddle,NameFirst,Suffix,DateOfBirth,Gender,Status,LanguageID,DisabilityID,TouchedBy,CreatedBy) 
VALUES (
(select max(p.personid) + 1 from people p),null,'',
'Man',
'', 
'Spider',
'','1900-01-01','',1,1,null,'root@localhost','root@localhost');

INSERT INTO peopleaddresses (
PersonID,AddressTypeID,AddressLine1,AddressLine2,City,State,PostalCode,Country,TouchedBy,CreatedBy) 
VALUES (
(select max(personid) from people),
1,'NO:1 Halas Way','','Lake Forest','IL','60001','US','root@localhost','root@localhost');

INSERT INTO peoplelinks (PersonID,LinkTypeID,LinkValue,IsPreferred,TouchedBy,CreatedBy) 
VALUES (
(select max(personid) from people),5,'spider.man@spxefare.com','T','root@localhost','root@localhost');

INSERT INTO peoplelinks (PersonID,LinkTypeID,LinkValue,IsPreferred,TouchedBy,CreatedBy) 
VALUES (
(select max(personid) from people),1,'9176208480','T','root@localhost','root@localhost');
/*END Create People*/

/*BEGIN Create Login*/
INSERT INTO logins (LoginID,PersonID,UserLogin,TouchedBy,CreatedBy) 
VALUES (
(select max(l.loginid) + 1 from logins l),(select max(personid) from people),'spider.man@spxefare.com','root@localhost','root@localhost');

INSERT INTO authentication (PersonID, AuthenticationTypeID, Response, TouchedBy, CreatedBy) 
VALUES (
(select max(personid) from people),(SELECT AuthenticationTypeID from AuthenticationTypes where Description='Password'),'$2a$10$tXpY5oz/EUbIasxTE5TG1eX8d7RyjoPxxJt/uDycO12dznNGsKCAu', 'unassigned','root@localhost');
/*END Create Login*/

/*Associated RIDER Role*/
INSERT INTO groupsofpeople (GroupID, PersonID, TouchedBy, CreatedBy) 
VALUES
((select g.GroupID from groups g where g.Role like '%ROLE_RIDER%'), (select max(personid) from people), 'root@localhost','root@localhost');

/*Add Full Fare*/
INSERT INTO PeopleFarecodes
(PersonID, FarecodeID, TouchedBy, CreatedBy) 
VALUES
((select max(personid) from people),(select farecodeid from Farecodes where DESCRIPTION like '%Senior%'),'root@localhost','root@localhost');

/**********************************CREATE George Jungle User with Senior Fare**********************************/
/*BEGIN Create People*/
INSERT INTO people (
PersonID,ParentID,Prefix,NameLast,NameMiddle,NameFirst,Suffix,DateOfBirth,Gender,Status,LanguageID,DisabilityID,TouchedBy,CreatedBy) 
VALUES (
(select max(p.personid) + 1 from people p),null,'',
'Jungle',
'', 
'George',
'','1900-01-01','',1,1,null,'root@localhost','root@localhost');

INSERT INTO peopleaddresses (
PersonID,AddressTypeID,AddressLine1,AddressLine2,City,State,PostalCode,Country,TouchedBy,CreatedBy) 
VALUES (
(select max(personid) from people),
1,'NO:1 Halas Way','','Lake Forest','IL','60001','US','root@localhost','root@localhost');

INSERT INTO peoplelinks (PersonID,LinkTypeID,LinkValue,IsPreferred,TouchedBy,CreatedBy) 
VALUES (
(select max(personid) from people),5,'george.jungle@spxefare.com','T','root@localhost','root@localhost');

INSERT INTO peoplelinks (PersonID,LinkTypeID,LinkValue,IsPreferred,TouchedBy,CreatedBy) 
VALUES (
(select max(personid) from people),1,'9176208480','T','root@localhost','root@localhost');
/*END Create People*/

/*BEGIN Create Login*/
INSERT INTO logins (LoginID,PersonID,UserLogin,TouchedBy,CreatedBy) 
VALUES (
(select max(l.loginid) + 1 from logins l),(select max(personid) from people),'george.jungle@spxefare.com','root@localhost','root@localhost');

INSERT INTO authentication (PersonID, AuthenticationTypeID, Response, TouchedBy, CreatedBy) 
VALUES (
(select max(personid) from people),(SELECT AuthenticationTypeID from AuthenticationTypes where Description='Password'),'$2a$10$tXpY5oz/EUbIasxTE5TG1eX8d7RyjoPxxJt/uDycO12dznNGsKCAu', 'unassigned','root@localhost');
/*END Create Login*/

/*Associated RIDER Role*/
INSERT INTO groupsofpeople (GroupID, PersonID, TouchedBy, CreatedBy) 
VALUES
((select g.GroupID from groups g where g.Role like '%ROLE_RIDER%'), (select max(personid) from people), 'root@localhost','root@localhost');

/*Add Full Fare*/
INSERT INTO PeopleFarecodes
(PersonID, FarecodeID, TouchedBy, CreatedBy) 
VALUES
((select max(personid) from people),(select farecodeid from Farecodes where DESCRIPTION like '%Senior%'),'root@localhost','root@localhost');

/**********************************CREATE Super Man User with Senior Fare**********************************/
/*BEGIN Create People*/
INSERT INTO people (
PersonID,ParentID,Prefix,NameLast,NameMiddle,NameFirst,Suffix,DateOfBirth,Gender,Status,LanguageID,DisabilityID,TouchedBy,CreatedBy) 
VALUES (
(select max(p.personid) + 1 from people p),null,'',
'Man',
'', 
'Super',
'','1900-01-01','',1,1,null,'root@localhost','root@localhost');

INSERT INTO peopleaddresses (
PersonID,AddressTypeID,AddressLine1,AddressLine2,City,State,PostalCode,Country,TouchedBy,CreatedBy) 
VALUES (
(select max(personid) from people),
1,'NO:1 Halas Way','','Lake Forest','IL','60001','US','root@localhost','root@localhost');

INSERT INTO peoplelinks (PersonID,LinkTypeID,LinkValue,IsPreferred,TouchedBy,CreatedBy) 
VALUES (
(select max(personid) from people),5,'super.man@spxefare.com','T','root@localhost','root@localhost');

INSERT INTO peoplelinks (PersonID,LinkTypeID,LinkValue,IsPreferred,TouchedBy,CreatedBy) 
VALUES (
(select max(personid) from people),1,'9176208480','T','root@localhost','root@localhost');
/*END Create People*/

/*BEGIN Create Login*/
INSERT INTO logins (LoginID,PersonID,UserLogin,TouchedBy,CreatedBy) 
VALUES (
(select max(l.loginid) + 1 from logins l),(select max(personid) from people),'super.man@spxefare.com','root@localhost','root@localhost');

INSERT INTO authentication (PersonID, AuthenticationTypeID, Response, TouchedBy, CreatedBy) 
VALUES (
(select max(personid) from people),(SELECT AuthenticationTypeID from AuthenticationTypes where Description='Password'),'$2a$10$tXpY5oz/EUbIasxTE5TG1eX8d7RyjoPxxJt/uDycO12dznNGsKCAu', 'unassigned','root@localhost');
/*END Create Login*/

/*Associated RIDER Role*/
INSERT INTO groupsofpeople (GroupID, PersonID, TouchedBy, CreatedBy) 
VALUES
((select g.GroupID from groups g where g.Role like '%ROLE_RIDER%'), (select max(personid) from people), 'root@localhost','root@localhost');

/*Add Full Fare*/
INSERT INTO PeopleFarecodes
(PersonID, FarecodeID, TouchedBy, CreatedBy) 
VALUES
((select max(personid) from people),(select farecodeid from Farecodes where DESCRIPTION like '%Senior%'),'root@localhost','root@localhost');

/**********************************CREATE Bat Man User with Disabled Fare**********************************/
/*BEGIN Create People*/
INSERT INTO people (
PersonID,ParentID,Prefix,NameLast,NameMiddle,NameFirst,Suffix,DateOfBirth,Gender,Status,LanguageID,DisabilityID,TouchedBy,CreatedBy) 
VALUES (
(select max(p.personid) + 1 from people p),null,'',
'Man',
'', 
'Bat',
'','1900-01-01','',1,1,null,'root@localhost','root@localhost');

INSERT INTO peopleaddresses (
PersonID,AddressTypeID,AddressLine1,AddressLine2,City,State,PostalCode,Country,TouchedBy,CreatedBy) 
VALUES (
(select max(personid) from people),
1,'NO:1 Halas Way','','Lake Forest','IL','60001','US','root@localhost','root@localhost');

INSERT INTO peoplelinks (PersonID,LinkTypeID,LinkValue,IsPreferred,TouchedBy,CreatedBy) 
VALUES (
(select max(personid) from people),5,'bat.man@spxefare.com','T','root@localhost','root@localhost');

INSERT INTO peoplelinks (PersonID,LinkTypeID,LinkValue,IsPreferred,TouchedBy,CreatedBy) 
VALUES (
(select max(personid) from people),1,'9176208480','T','root@localhost','root@localhost');
/*END Create People*/

/*BEGIN Create Login*/
INSERT INTO logins (LoginID,PersonID,UserLogin,TouchedBy,CreatedBy) 
VALUES (
(select max(l.loginid) + 1 from logins l),(select max(personid) from people),'bat.man@spxefare.com','root@localhost','root@localhost');

INSERT INTO authentication (PersonID, AuthenticationTypeID, Response, TouchedBy, CreatedBy) 
VALUES (
(select max(personid) from people),(SELECT AuthenticationTypeID from AuthenticationTypes where Description='Password'),'$2a$10$tXpY5oz/EUbIasxTE5TG1eX8d7RyjoPxxJt/uDycO12dznNGsKCAu', 'unassigned','root@localhost');
/*END Create Login*/

/*Associated RIDER Role*/
INSERT INTO groupsofpeople (GroupID, PersonID, TouchedBy, CreatedBy) 
VALUES
((select g.GroupID from groups g where g.Role like '%ROLE_RIDER%'), (select max(personid) from people), 'root@localhost','root@localhost');

/*Add Full Fare*/
INSERT INTO PeopleFarecodes
(PersonID, FarecodeID, TouchedBy, CreatedBy) 
VALUES
((select max(personid) from people),(select farecodeid from Farecodes where DESCRIPTION like '%Disabled%'),'root@localhost','root@localhost');

/**********************************CREATE Wonder Woman User with Disabled Fare**********************************/
/*BEGIN Create People*/
INSERT INTO people (
PersonID,ParentID,Prefix,NameLast,NameMiddle,NameFirst,Suffix,DateOfBirth,Gender,Status,LanguageID,DisabilityID,TouchedBy,CreatedBy) 
VALUES (
(select max(p.personid) + 1 from people p),null,'',
'Woman',
'', 
'Wonder',
'','1900-01-01','',1,1,null,'root@localhost','root@localhost');

INSERT INTO peopleaddresses (
PersonID,AddressTypeID,AddressLine1,AddressLine2,City,State,PostalCode,Country,TouchedBy,CreatedBy) 
VALUES (
(select max(personid) from people),
1,'NO:1 Halas Way','','Lake Forest','IL','60001','US','root@localhost','root@localhost');

INSERT INTO peoplelinks (PersonID,LinkTypeID,LinkValue,IsPreferred,TouchedBy,CreatedBy) 
VALUES (
(select max(personid) from people),5,'wonder.woman@spxefare.com','T','root@localhost','root@localhost');

INSERT INTO peoplelinks (PersonID,LinkTypeID,LinkValue,IsPreferred,TouchedBy,CreatedBy) 
VALUES (
(select max(personid) from people),1,'9176208480','T','root@localhost','root@localhost');
/*END Create People*/

/*BEGIN Create Login*/
INSERT INTO logins (LoginID,PersonID,UserLogin,TouchedBy,CreatedBy) 
VALUES (
(select max(l.loginid) + 1 from logins l),(select max(personid) from people),'wonder.woman@spxefare.com','root@localhost','root@localhost');

INSERT INTO authentication (PersonID, AuthenticationTypeID, Response, TouchedBy, CreatedBy) 
VALUES (
(select max(personid) from people),(SELECT AuthenticationTypeID from AuthenticationTypes where Description='Password'),'$2a$10$tXpY5oz/EUbIasxTE5TG1eX8d7RyjoPxxJt/uDycO12dznNGsKCAu', 'unassigned','root@localhost');
/*END Create Login*/

/*Associated RIDER Role*/
INSERT INTO groupsofpeople (GroupID, PersonID, TouchedBy, CreatedBy) 
VALUES
((select g.GroupID from groups g where g.Role like '%ROLE_RIDER%'), (select max(personid) from people), 'root@localhost','root@localhost');

/*Add Full Fare*/
INSERT INTO PeopleFarecodes
(PersonID, FarecodeID, TouchedBy, CreatedBy) 
VALUES
((select max(personid) from people),(select farecodeid from Farecodes where DESCRIPTION like '%Disabled%'),'root@localhost','root@localhost');

/**********************************CREATE Yogi Bear User with Disabled Fare**********************************/
/*BEGIN Create People*/
INSERT INTO people (
PersonID,ParentID,Prefix,NameLast,NameMiddle,NameFirst,Suffix,DateOfBirth,Gender,Status,LanguageID,DisabilityID,TouchedBy,CreatedBy) 
VALUES (
(select max(p.personid) + 1 from people p),null,'',
'Bear',
'', 
'Yogi',
'','1900-01-01','',1,1,null,'root@localhost','root@localhost');

INSERT INTO peopleaddresses (
PersonID,AddressTypeID,AddressLine1,AddressLine2,City,State,PostalCode,Country,TouchedBy,CreatedBy) 
VALUES (
(select max(personid) from people),
1,'NO:1 Halas Way','','Lake Forest','IL','60001','US','root@localhost','root@localhost');

INSERT INTO peoplelinks (PersonID,LinkTypeID,LinkValue,IsPreferred,TouchedBy,CreatedBy) 
VALUES (
(select max(personid) from people),5,'yogi.bear@spxefare.com','T','root@localhost','root@localhost');

INSERT INTO peoplelinks (PersonID,LinkTypeID,LinkValue,IsPreferred,TouchedBy,CreatedBy) 
VALUES (
(select max(personid) from people),1,'9176208480','T','root@localhost','root@localhost');
/*END Create People*/

/*BEGIN Create Login*/
INSERT INTO logins (LoginID,PersonID,UserLogin,TouchedBy,CreatedBy) 
VALUES (
(select max(l.loginid) + 1 from logins l),(select max(personid) from people),'yogi.bear@spxefare.com','root@localhost','root@localhost');

INSERT INTO authentication (PersonID, AuthenticationTypeID, Response, TouchedBy, CreatedBy) 
VALUES (
(select max(personid) from people),(SELECT AuthenticationTypeID from AuthenticationTypes where Description='Password'),'$2a$10$tXpY5oz/EUbIasxTE5TG1eX8d7RyjoPxxJt/uDycO12dznNGsKCAu', 'unassigned','root@localhost');
/*END Create Login*/

/*Associated RIDER Role*/
INSERT INTO groupsofpeople (GroupID, PersonID, TouchedBy, CreatedBy) 
VALUES
((select g.GroupID from groups g where g.Role like '%ROLE_RIDER%'), (select max(personid) from people), 'root@localhost','root@localhost');

/*Add Full Fare*/
INSERT INTO PeopleFarecodes
(PersonID, FarecodeID, TouchedBy, CreatedBy) 
VALUES
((select max(personid) from people),(select farecodeid from Farecodes where DESCRIPTION like '%Disabled%'),'root@localhost','root@localhost');

/**********************************CREATE Boo Bear User with Disabled Fare**********************************/
/*BEGIN Create People*/
INSERT INTO people (
PersonID,ParentID,Prefix,NameLast,NameMiddle,NameFirst,Suffix,DateOfBirth,Gender,Status,LanguageID,DisabilityID,TouchedBy,CreatedBy) 
VALUES (
(select max(p.personid) + 1 from people p),null,'',
'Bear',
'', 
'Boo',
'','1900-01-01','',1,1,null,'root@localhost','root@localhost');

INSERT INTO peopleaddresses (
PersonID,AddressTypeID,AddressLine1,AddressLine2,City,State,PostalCode,Country,TouchedBy,CreatedBy) 
VALUES (
(select max(personid) from people),
1,'NO:1 Halas Way','','Lake Forest','IL','60001','US','root@localhost','root@localhost');

INSERT INTO peoplelinks (PersonID,LinkTypeID,LinkValue,IsPreferred,TouchedBy,CreatedBy) 
VALUES (
(select max(personid) from people),5,'boo.bear@spxefare.com','T','root@localhost','root@localhost');

INSERT INTO peoplelinks (PersonID,LinkTypeID,LinkValue,IsPreferred,TouchedBy,CreatedBy) 
VALUES (
(select max(personid) from people),1,'9176208480','T','root@localhost','root@localhost');
/*END Create People*/

/*BEGIN Create Login*/
INSERT INTO logins (LoginID,PersonID,UserLogin,TouchedBy,CreatedBy) 
VALUES (
(select max(l.loginid) + 1 from logins l),(select max(personid) from people),'boo.bear@spxefare.com','root@localhost','root@localhost');

INSERT INTO authentication (PersonID, AuthenticationTypeID, Response, TouchedBy, CreatedBy) 
VALUES (
(select max(personid) from people),(SELECT AuthenticationTypeID from AuthenticationTypes where Description='Password'),'$2a$10$tXpY5oz/EUbIasxTE5TG1eX8d7RyjoPxxJt/uDycO12dznNGsKCAu', 'unassigned','root@localhost');
/*END Create Login*/

/*Associated RIDER Role*/
INSERT INTO groupsofpeople (GroupID, PersonID, TouchedBy, CreatedBy) 
VALUES
((select g.GroupID from groups g where g.Role like '%ROLE_RIDER%'), (select max(personid) from people), 'root@localhost','root@localhost');

/*Add Full Fare*/
INSERT INTO PeopleFarecodes
(PersonID, FarecodeID, TouchedBy, CreatedBy) 
VALUES
((select max(personid) from people),(select farecodeid from Farecodes where DESCRIPTION like '%Disabled%'),'root@localhost','root@localhost');

/**********************************CREATE Alvin Chipmunk User with Disabled Fare**********************************/
/*BEGIN Create People*/
INSERT INTO people (
PersonID,ParentID,Prefix,NameLast,NameMiddle,NameFirst,Suffix,DateOfBirth,Gender,Status,LanguageID,DisabilityID,TouchedBy,CreatedBy) 
VALUES (
(select max(p.personid) + 1 from people p),null,'',
'Chipmunk',
'', 
'Alvin',
'','1900-01-01','',1,1,null,'root@localhost','root@localhost');

INSERT INTO peopleaddresses (
PersonID,AddressTypeID,AddressLine1,AddressLine2,City,State,PostalCode,Country,TouchedBy,CreatedBy) 
VALUES (
(select max(personid) from people),
1,'NO:1 Halas Way','','Lake Forest','IL','60001','US','root@localhost','root@localhost');

INSERT INTO peoplelinks (PersonID,LinkTypeID,LinkValue,IsPreferred,TouchedBy,CreatedBy) 
VALUES (
(select max(personid) from people),5,'alvin.chipmunk@spxefare.com','T','root@localhost','root@localhost');

INSERT INTO peoplelinks (PersonID,LinkTypeID,LinkValue,IsPreferred,TouchedBy,CreatedBy) 
VALUES (
(select max(personid) from people),1,'9176208480','T','root@localhost','root@localhost');
/*END Create People*/

/*BEGIN Create Login*/
INSERT INTO logins (LoginID,PersonID,UserLogin,TouchedBy,CreatedBy) 
VALUES (
(select max(l.loginid) + 1 from logins l),(select max(personid) from people),'alvin.chipmunk@spxefare.com','root@localhost','root@localhost');

INSERT INTO authentication (PersonID, AuthenticationTypeID, Response, TouchedBy, CreatedBy) 
VALUES (
(select max(personid) from people),(SELECT AuthenticationTypeID from AuthenticationTypes where Description='Password'),'$2a$10$tXpY5oz/EUbIasxTE5TG1eX8d7RyjoPxxJt/uDycO12dznNGsKCAu', 'unassigned','root@localhost');
/*END Create Login*/

/*Associated RIDER Role*/
INSERT INTO groupsofpeople (GroupID, PersonID, TouchedBy, CreatedBy) 
VALUES
((select g.GroupID from groups g where g.Role like '%ROLE_RIDER%'), (select max(personid) from people), 'root@localhost','root@localhost');

/*Add Full Fare*/
INSERT INTO PeopleFarecodes
(PersonID, FarecodeID, TouchedBy, CreatedBy) 
VALUES
((select max(personid) from people),(select farecodeid from Farecodes where DESCRIPTION like '%Disabled%'),'root@localhost','root@localhost');

/**********************************CREATE Donald Duck User with Youth Fare**********************************/
/*BEGIN Create People*/
INSERT INTO people (
PersonID,ParentID,Prefix,NameLast,NameMiddle,NameFirst,Suffix,DateOfBirth,Gender,Status,LanguageID,DisabilityID,TouchedBy,CreatedBy) 
VALUES (
(select max(p.personid) + 1 from people p),null,'',
'Duck',
'', 
'Donald',
'','1900-01-01','',1,1,null,'root@localhost','root@localhost');

INSERT INTO peopleaddresses (
PersonID,AddressTypeID,AddressLine1,AddressLine2,City,State,PostalCode,Country,TouchedBy,CreatedBy) 
VALUES (
(select max(personid) from people),
1,'NO:1 Halas Way','','Lake Forest','IL','60001','US','root@localhost','root@localhost');

INSERT INTO peoplelinks (PersonID,LinkTypeID,LinkValue,IsPreferred,TouchedBy,CreatedBy) 
VALUES (
(select max(personid) from people),5,'donald.duck@spxefare.com','T','root@localhost','root@localhost');

INSERT INTO peoplelinks (PersonID,LinkTypeID,LinkValue,IsPreferred,TouchedBy,CreatedBy) 
VALUES (
(select max(personid) from people),1,'9176208480','T','root@localhost','root@localhost');
/*END Create People*/

/*BEGIN Create Login*/
INSERT INTO logins (LoginID,PersonID,UserLogin,TouchedBy,CreatedBy) 
VALUES (
(select max(l.loginid) + 1 from logins l),(select max(personid) from people),'donald.duck@spxefare.com','root@localhost','root@localhost');

INSERT INTO authentication (PersonID, AuthenticationTypeID, Response, TouchedBy, CreatedBy) 
VALUES (
(select max(personid) from people),(SELECT AuthenticationTypeID from AuthenticationTypes where Description='Password'),'$2a$10$tXpY5oz/EUbIasxTE5TG1eX8d7RyjoPxxJt/uDycO12dznNGsKCAu', 'unassigned','root@localhost');
/*END Create Login*/

/*Associated RIDER Role*/
INSERT INTO groupsofpeople (GroupID, PersonID, TouchedBy, CreatedBy) 
VALUES
((select g.GroupID from groups g where g.Role like '%ROLE_RIDER%'), (select max(personid) from people), 'root@localhost','root@localhost');

/*Add Full Fare*/
INSERT INTO PeopleFarecodes
(PersonID, FarecodeID, TouchedBy, CreatedBy) 
VALUES
((select max(personid) from people),(select farecodeid from Farecodes where DESCRIPTION like '%Youth%'),'root@localhost','root@localhost');

/**********************************CREATE Woody Woodpecker User with Youth Fare**********************************/
/*BEGIN Create People*/
INSERT INTO people (
PersonID,ParentID,Prefix,NameLast,NameMiddle,NameFirst,Suffix,DateOfBirth,Gender,Status,LanguageID,DisabilityID,TouchedBy,CreatedBy) 
VALUES (
(select max(p.personid) + 1 from people p),null,'',
'Woodpecker',
'', 
'Woody',
'','1900-01-01','',1,1,null,'root@localhost','root@localhost');

INSERT INTO peopleaddresses (
PersonID,AddressTypeID,AddressLine1,AddressLine2,City,State,PostalCode,Country,TouchedBy,CreatedBy) 
VALUES (
(select max(personid) from people),
1,'NO:1 Halas Way','','Lake Forest','IL','60001','US','root@localhost','root@localhost');

INSERT INTO peoplelinks (PersonID,LinkTypeID,LinkValue,IsPreferred,TouchedBy,CreatedBy) 
VALUES (
(select max(personid) from people),5,'woody.woodpecker@spxefare.com','T','root@localhost','root@localhost');

INSERT INTO peoplelinks (PersonID,LinkTypeID,LinkValue,IsPreferred,TouchedBy,CreatedBy) 
VALUES (
(select max(personid) from people),1,'9176208480','T','root@localhost','root@localhost');
/*END Create People*/

/*BEGIN Create Login*/
INSERT INTO logins (LoginID,PersonID,UserLogin,TouchedBy,CreatedBy) 
VALUES (
(select max(l.loginid) + 1 from logins l),(select max(personid) from people),'woody.woodpecker@spxefare.com','root@localhost','root@localhost');

INSERT INTO authentication (PersonID, AuthenticationTypeID, Response, TouchedBy, CreatedBy) 
VALUES (
(select max(personid) from people),(SELECT AuthenticationTypeID from AuthenticationTypes where Description='Password'),'$2a$10$tXpY5oz/EUbIasxTE5TG1eX8d7RyjoPxxJt/uDycO12dznNGsKCAu', 'unassigned','root@localhost');
/*END Create Login*/

/*Associated RIDER Role*/
INSERT INTO groupsofpeople (GroupID, PersonID, TouchedBy, CreatedBy) 
VALUES
((select g.GroupID from groups g where g.Role like '%ROLE_RIDER%'), (select max(personid) from people), 'root@localhost','root@localhost');

/*Add Full Fare*/
INSERT INTO PeopleFarecodes
(PersonID, FarecodeID, TouchedBy, CreatedBy) 
VALUES
((select max(personid) from people),(select farecodeid from Farecodes where DESCRIPTION like '%Youth%'),'root@localhost','root@localhost');

/**********************************CREATE Gummy Bear User with Youth Fare**********************************/
/*BEGIN Create People*/
INSERT INTO people (
PersonID,ParentID,Prefix,NameLast,NameMiddle,NameFirst,Suffix,DateOfBirth,Gender,Status,LanguageID,DisabilityID,TouchedBy,CreatedBy) 
VALUES (
(select max(p.personid) + 1 from people p),null,'',
'Bear',
'', 
'Gummy',
'','1900-01-01','',1,1,null,'root@localhost','root@localhost');

INSERT INTO peopleaddresses (
PersonID,AddressTypeID,AddressLine1,AddressLine2,City,State,PostalCode,Country,TouchedBy,CreatedBy) 
VALUES (
(select max(personid) from people),
1,'NO:1 Halas Way','','Lake Forest','IL','60001','US','root@localhost','root@localhost');

INSERT INTO peoplelinks (PersonID,LinkTypeID,LinkValue,IsPreferred,TouchedBy,CreatedBy) 
VALUES (
(select max(personid) from people),5,'gummy.bear@efare.com','T','root@localhost','root@localhost');

INSERT INTO peoplelinks (PersonID,LinkTypeID,LinkValue,IsPreferred,TouchedBy,CreatedBy) 
VALUES (
(select max(personid) from people),1,'9176208480','T','root@localhost','root@localhost');
/*END Create People*/

/*BEGIN Create Login*/
INSERT INTO logins (LoginID,PersonID,UserLogin,TouchedBy,CreatedBy) 
VALUES (
(select max(l.loginid) + 1 from logins l),(select max(personid) from people),'gummy.bear@efare.com','root@localhost','root@localhost');

INSERT INTO authentication (PersonID, AuthenticationTypeID, Response, TouchedBy, CreatedBy) 
VALUES (
(select max(personid) from people),(SELECT AuthenticationTypeID from AuthenticationTypes where Description='Password'),'$2a$10$tXpY5oz/EUbIasxTE5TG1eX8d7RyjoPxxJt/uDycO12dznNGsKCAu', 'unassigned','root@localhost');
/*END Create Login*/

/*Associated RIDER Role*/
INSERT INTO groupsofpeople (GroupID, PersonID, TouchedBy, CreatedBy) 
VALUES
((select g.GroupID from groups g where g.Role like '%ROLE_RIDER%'), (select max(personid) from people), 'root@localhost','root@localhost');

/*Add Full Fare*/
INSERT INTO PeopleFarecodes
(PersonID, FarecodeID, TouchedBy, CreatedBy) 
VALUES
((select max(personid) from people),(select farecodeid from Farecodes where DESCRIPTION like '%Youth%'),'root@localhost','root@localhost');

/**********************************CREATE Tweety Bear User with Youth Fare**********************************/
/*BEGIN Create People*/
INSERT INTO people (
PersonID,ParentID,Prefix,NameLast,NameMiddle,NameFirst,Suffix,DateOfBirth,Gender,Status,LanguageID,DisabilityID,TouchedBy,CreatedBy) 
VALUES (
(select max(p.personid) + 1 from people p),null,'',
'Bear',
'', 
'Tweety',
'','1900-01-01','',1,1,null,'root@localhost','root@localhost');

INSERT INTO peopleaddresses (
PersonID,AddressTypeID,AddressLine1,AddressLine2,City,State,PostalCode,Country,TouchedBy,CreatedBy) 
VALUES (
(select max(personid) from people),
1,'NO:1 Halas Way','','Lake Forest','IL','60001','US','root@localhost','root@localhost');

INSERT INTO peoplelinks (PersonID,LinkTypeID,LinkValue,IsPreferred,TouchedBy,CreatedBy) 
VALUES (
(select max(personid) from people),5,'tweety.bear@efare.com','T','root@localhost','root@localhost');

INSERT INTO peoplelinks (PersonID,LinkTypeID,LinkValue,IsPreferred,TouchedBy,CreatedBy) 
VALUES (
(select max(personid) from people),1,'9176208480','T','root@localhost','root@localhost');
/*END Create People*/

/*BEGIN Create Login*/
INSERT INTO logins (LoginID,PersonID,UserLogin,TouchedBy,CreatedBy) 
VALUES (
(select max(l.loginid) + 1 from logins l),(select max(personid) from people),'tweety.bear@efare.com','root@localhost','root@localhost');

INSERT INTO authentication (PersonID, AuthenticationTypeID, Response, TouchedBy, CreatedBy) 
VALUES (
(select max(personid) from people),(SELECT AuthenticationTypeID from AuthenticationTypes where Description='Password'),'$2a$10$tXpY5oz/EUbIasxTE5TG1eX8d7RyjoPxxJt/uDycO12dznNGsKCAu', 'unassigned','root@localhost');
/*END Create Login*/

/*Associated RIDER Role*/
INSERT INTO groupsofpeople (GroupID, PersonID, TouchedBy, CreatedBy) 
VALUES
((select g.GroupID from groups g where g.Role like '%ROLE_RIDER%'), (select max(personid) from people), 'root@localhost','root@localhost');

/*Add Full Fare*/
INSERT INTO PeopleFarecodes
(PersonID, FarecodeID, TouchedBy, CreatedBy) 
VALUES
((select max(personid) from people),(select farecodeid from Farecodes where DESCRIPTION like '%Youth%'),'root@localhost','root@localhost');

/**********************************CREATE Winnie Pooh User with Youth Fare**********************************/
/*BEGIN Create People*/
INSERT INTO people (
PersonID,ParentID,Prefix,NameLast,NameMiddle,NameFirst,Suffix,DateOfBirth,Gender,Status,LanguageID,DisabilityID,TouchedBy,CreatedBy) 
VALUES (
(select max(p.personid) + 1 from people p),null,'',
'Pooh',
'', 
'Winnie',
'','1900-01-01','',1,1,null,'root@localhost','root@localhost');

INSERT INTO peopleaddresses (
PersonID,AddressTypeID,AddressLine1,AddressLine2,City,State,PostalCode,Country,TouchedBy,CreatedBy) 
VALUES (
(select max(personid) from people),
1,'NO:1 Halas Way','','Lake Forest','IL','60001','US','root@localhost','root@localhost');

INSERT INTO peoplelinks (PersonID,LinkTypeID,LinkValue,IsPreferred,TouchedBy,CreatedBy) 
VALUES (
(select max(personid) from people),5,'winnie.pooh@efare.com','T','root@localhost','root@localhost');

INSERT INTO peoplelinks (PersonID,LinkTypeID,LinkValue,IsPreferred,TouchedBy,CreatedBy) 
VALUES (
(select max(personid) from people),1,'9176208480','T','root@localhost','root@localhost');
/*END Create People*/

/*BEGIN Create Login*/
INSERT INTO logins (LoginID,PersonID,UserLogin,TouchedBy,CreatedBy) 
VALUES (
(select max(l.loginid) + 1 from logins l),(select max(personid) from people),'winnie.pooh@efare.com','root@localhost','root@localhost');

INSERT INTO authentication (PersonID, AuthenticationTypeID, Response, TouchedBy, CreatedBy) 
VALUES (
(select max(personid) from people),(SELECT AuthenticationTypeID from AuthenticationTypes where Description='Password'),'$2a$10$tXpY5oz/EUbIasxTE5TG1eX8d7RyjoPxxJt/uDycO12dznNGsKCAu', 'unassigned','root@localhost');
/*END Create Login*/

/*Associated RIDER Role*/
INSERT INTO groupsofpeople (GroupID, PersonID, TouchedBy, CreatedBy) 
VALUES
((select g.GroupID from groups g where g.Role like '%ROLE_RIDER%'), (select max(personid) from people), 'root@localhost','root@localhost');

/*Add Full Fare*/
INSERT INTO PeopleFarecodes
(PersonID, FarecodeID, TouchedBy, CreatedBy) 
VALUES
((select max(personid) from people),(select farecodeid from Farecodes where DESCRIPTION like '%Youth%'),'root@localhost','root@localhost');
