insert into documents (Category, `Key`, Value, LanguageID, TouchedBy, CreatedBy) VALUES ('faq', 'faq.pageContent', '<div class="container">
        <div>
            <h3>
                <a href="#header1">FareCard Fare Payment, General Information</a>
            </h3>
            <ul>
                <li><a href="#header1question1">What is FareCard?</a></li>
                <li><a href="#header1question2">What is a Smart Card?</a></li>
                <li><a href="#header1question3">How are passes activated?</a></li>
                <li><a href="#header1question4">Where can card or pass expiration dates be checked?</a></li>
                <li><a href="#header1question5">What can be done at the farebox?</a></li>
                <li><a href="#header1question6">What cannot be done at the farebox?</a></li>
                <li><a href="#header1question7">Are there transfer restrictions?</a></li>
                <li><a href="#header1question8">How are transfers issued?</a></li>
            </ul>
            <h3>
                <a href="#header2">What are the differences between FareCard Cards &amp; Tickets?</a>
            </h3>
            <ul>
                <li><a href="#header2question1">What&#39;s a FareCard Card?</a></li>
                <li><a href="#header2question2">What&#39;s a FareCard Ticket?</a></li>
                <li><a href="#header2question3">What&#39;s a 30-Day Pass?</a></li>
                <li><a href="#header2question4">How does the 1-Day Pass work?</a></li>
                <li><a href="#header2question5">Are transfers free?</a></li>
                <li><a href="#header2question6">What Product is Right for Me?</a></li>
                <li><a href="#header2question7">Where to buy FareCard Cards and add passes or cash value?</a></li>
                <li><a href="#header2question8">Where to buy FareCard Tickets and add passes?</a></li>
                <li><a href="#header2question9">Why register a FareCard Card?</a></li>
                <li><a href="#header2question10">1. Balance Protection</a></li>
                <li><a href="#header2question11">2. One Ride Safety Net</a></li>
                <li><a href="#header2question12">3. Auto Buy</a></li>
                <li><a href="#header2question13">When a FareCard Card or FareCard Ticket is lost, is the pass or value lost as well?</a></li>
                <li><a href="#header2question14">What if I board without enough cash value or a valid pass?</a></li>
                <li><a href="#header2question15">Can I automatically add cash value or a pass each month?</a></li>
                <li><a href="#header2question16">How long will the FareCard Card and FareCard Ticket last?</a></li>
                <li><a href="#header2question17">How many passes can I have on my FareCard Card at one time?</a></li>
                <li><a href="#header2question18">What happens if my FareCard Card no longer works?</a></li>
                <li><a href="#header2question19">Link Card</a></li>
            </ul>
        </div>
        <!-- Content -->
        <!-- Create the actual content. Print a header with all of its questions/answers in a single div. -->
        <div>
            <h3 id="header1">FareCard Fare Payment, General Information</h3>
            <h4 id="header1question1">What is FareCard?</h4>
            SunGO utilizes Smart Card technology to provide a convenient way to
            pay and transfer throughout the region with ease on Sun Tran, Sun
            Express, Sun Shuttle and the Sun Link modern streetcar.
            <h4 id="header1question2">What is a Smart Card?</h4>
            A Smart Card resembles a credit card in size and shape, but inside
            has a computer chip instead of a magnetic stripe. For transit, the
            smart card will serve as the fare payment card and can store cash
            value or passes for passenger convenience and boarding ease. Just tap
            the card to the farebox and go!
            <h4 id="header1question3">How are passes activated?</h4>
            When a SunGO Card, SunGO ID &amp; Card or SunGO Ticket is tapped on
            the farebox, the first pass in the queue on the card will be
            activated from that day/time.
            <h4 id="header1question4">Where can card or pass expiration dates be checked?</h4>
            <ul>
                <li>Ticket vending machines at transit centers</li>
                <li>On the bus <span style="font-style: italic">(ask
                        driver for assistance)</span></li>
                <li>Pass sales outlets</li>
                <li>www.suntran.com/sungo</li>
                <li>Special Services Office</li>
                <li>Phone, (520) 792-9222 (TDD: 628-1565)</li>
                <li>Sun Tran Administrative Office, 3920 N. Sun Tran Blvd.,
                    Tucson.</li>
            </ul>

            <h4 id="header1question5">What can be done at the farebox?</h4>
            <ul>
                <li>Activates passes</li>
                <li>Checks the expiration date of all cards and tickets</li>
                <li>Can tell a passenger how much value or time is remaining on
                    a currently active pass</li>
                <li>Accepts bills up to $20. No change given. Although the
                    farebox can accept larger bills, Sun Tran?s policy is for exact
                    change only.</li>
                <li>Validates coins and bills</li>
                <li>When coins and bills are inserted into the farebox, the
                    total inserted will be indicated to the driver on a screen. This
                    will limit the number of passengers paying less than their fare.</li>
            </ul>

            <h4 id="header1question6">What cannot be done at the farebox?</h4>
            <ul>
                <li>Does not issue change</li>
                <li>Does not allow a pass to be purchased on the bus</li>
            </ul>
            <h4 id="header1question7">Are there transfer restrictions?</h4>
            All passengers, regardless of how they pay or whether they pay the
            full fare or economy fare, receive a free transfer. How a transfer is
            received will depend on how the fare is paid.
            <ul>
                <li>Valid for up to 2 trips within 2 hours from the time of
                    issue</li>
                <li>May not be used on the same issuing route for a return trip</li>
                <li>One Way Loop Policy - All operators upon arriving at the
                    end of the line will change their fareboxes to the appropriate
                    direction of their next trip. What operators have passengers do at
                    the end of the line depends on where the passenger boarded and
                    whether they will continue on the route in the opposite direction.</li>
                <li>Surcharges will be required when transferring to select
                    premium service, such as from Sun Tran to Sun Express service</li>
            </ul>

            <h4 id="header1question8">How are transfers issued?</h4>
            <ul>
                <li>Full Fare passengers paying with:
                    <ul>
                        <li>Value on a SunGO Card - transfer will automatically load
                            onto the SunGO Card when tapped on the farebox</li>
                        <li>SunGO Ticket - no reason for a transfer as the tickets
                            are 1-Day or 30-Day period passes</li>
                        <li>Cash, but has SunGO Card - Passenger informs driver they
                            need a transfer, then drops cash in the farebox. When the driver
                            gives the OK, passenger then taps their SunGO Card to the target
                            to load a transfer.</li>
                        <li>Cash, but no SunGO Card - Passenger informs driver they
                            need a transfer, then drops cash in the farebox. The driver will
                            then provide a paper SunGO Transfer Ticket if needed, then taps
                            the ticket to the farebox for the transfer to be loaded on the
                            ticket. Passengers should keep the transfer ticket to load future
                            transfers for up to 30 days from issue.</li>
                    </ul>
                </li>
                <li>Economy Fare passengers paying with:
                    <ul>
                        <li>Value on a SunGO ID &amp; Card - transfer will
                            automatically load onto the SunGO ID &amp; Card when tapped on
                            the farebox</li>
                        <li>Cash, but has SunGO ID &amp; Card - Passenger must tell
                            driver they want to pay cash before tapping their card to the
                            farebox. SunGO ID &amp; Card must be tapped when driver gives the
                            OK as proof of economy fare qualification, then passenger pays
                            cash fare. Driver can load a transfer to the SunGO ID &amp; Card
                            if requested. If the card is tapped to early, it will then have a
                            negative balance.</li>
                        <li>Cash, but no valid SunGO ID &amp; Card - cannot pay the
                            economy fare without the SunGO ID &amp; Card, even if paying
                            cash. Full fare will be required and a SunGO Transfer Ticket will
                            be issued by the driver.</li>
                        <li>SunGO Ticket - no transfer needed as the tickets are
                            1-Day or 30-Day passes. Economy fare passenger can utilize a
                            ticket if received from a nonprofit organization, but a transfer
                            would not be needed with these tickets. Economy passengers with a
                            30-Day Economy Ticket must still tap their SunGO ID &amp; Card to
                            the farebox and show it to the driver as proof they qualify to
                            pay the economy fare. Follow driver?s instructions to activate
                            the 30-Day Economy Ticket.</li>
                    </ul>
                </li>
            </ul>
            <h3 id="header2">What are the differences between FareCard Cards &amp; Tickets?</h3>
            <h4 id="header2question1">What&#39;s a FareCard Card?</h4>
            <ul>
                <li>Long term use. <br />A plastic product much like a credit
                    card, and is designed to last up to 4 years with proper care.
                </li>
                <li>More product choices:
                    <ul>
                        <li>30-Day Pass</li>
                        <li>30-Day Reduced Fare Pass <span style="font-style: italic">(only
                                for qualified individuals with a SunGO ID &amp; Card)</span>
                        </li>
                        <li>30-Day Express Pass</li>
                        <li>1-Day Pass</li>
                        <li>Cash value, up to $200
                            <ul>
                                <li>Automatically deducts appropriate fare from the card
                                    each time you ride.</li>
                                <li>A free transfer will be added to the card when the cash
                                    fare is paid with the SunGO Card. The transfer is good for up
                                    to two trips within two hours.</li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li>There is a $2 surcharge for the card in addition to the
                    pass or cash value added following the initial promotional period.</li>
                <li>Customers can register the card to protect the balance if
                    the card is lost, stolen or damaged.</li>
                <li>SunGO cards offer more flexibility with a built-in chip
                    that allows you to purchase and store up to two passes plus
                    additional cash value on a card.</li>
            </ul>

            <h4 id="header2question2">What&#39;s a FareCard Ticket?</h4>
            <ul>
                <li>Intended for short-term use.<br />A paper product expected
                    to last up to 90 days after first use with proper care.
                </li>
                <li>Limited products:
                    <ul>
                        <li>30-Day Pass</li>
                        <li>1-Day Pass</li>
                    </ul>
                </li>
                <li>There is a $. 40 surcharge for the ticket added to the
                    passes purchased.</li>
                <li>Customers cannot load different passes or cash value on the
                    ticket. If you purchase a 30-day ticket, you may only reload
                    another 30-day pass on that ticket.</li>
                <li>All products on the ticket must be used when the card
                    expires on the 90th day.</li>
                <li>No balance protection.</li>
            </ul>

            <h4 id="header2question3">What&#39;s a 30-Day Pass?</h4>
            The 30-Day Pass offers flexibility and convenience as it can be
            activated any day of the month. When tapped on the farebox, the pass
            is valid for 30 consecutive days from that point.

            <h4 id="header2question4">How does the 1-Day Pass work?</h4>
            The farebox technology allows Sun Tran to offer a day pass that can
            be used for a full 24 hours, giving customers greater value for their
            purchase. Once loaded on a SunGO Card or Ticket, a 1-Day Pass is
            activated when tapped on the farebox for the first time, and is valid
            for 24 hours from that point.

            <h4 id="header2question5">Are transfers free?</h4>
            If paying with a SunGO Card or SunGO ID &amp; Card when boarding the
            bus, a free transfer will be automatically added onto the card.

            <h4 id="header2question6">What Product is Right for Me?</h4>
            A SunGO Card, which is a longer-lasting card, offers flexibility by
            allowing different passes and cash value to be loaded on the card.
            The SunGO ticket is a short-term product that will only allow loading
            of the same pass for up to 90 days. If you purchase a 30-day ticket,
            you may only reload a 30-day pass on that same ticket, and any passes
            must be fully used by the end of the 90th day.We recommend the SunGO
            card for most passengers because of the flexibility to add different
            cash values or passes, ability to register the card to protect the
            balance if lost or stolen, and the card may last up to four years
            with proper care.We recommend the SunGO ticket for visitors to our
            community, or individuals with limited travel needs.

            <h4 id="header2question7">Where to buy FareCard Cards and add passes or cash value?</h4>
            <ul>
                <li>Online</li>
                <li>Ticket Vending Machine - located at each of the three
                    transit centers</li>
                <li>At Retail Sales Outlets - located throughout the community</li>
                <li>Sun Tran&#39;s Administrative Office, 3920 N. Sun Tran Blvd.,
                    Tucson</li>
                <li>By Phone - Call (520) 792-9222 or TDD (520) 628-1565</li>
                <li>By Mail</li>
            </ul>
            <br />
            <h4 id="header2question8">Where to buy FareCard Tickets and add passes?</h4>
            <ul>
                <li>Online (initial purchase only, can not re-load tickets
                    online)</li>
                <li>Ticket Vending Machine - located at each of the three
                    transit centers</li>
                <li>At Retail Sales Outlets - located throughout the community</li>
                <li>Sun Tran&#39;s Administrative Office, 3920 N. Sun Tran Blvd.,
                    Tucson</li>
                <li>Special Services Office, 35 W. Alameda, Tucson</li>
                <li>By Phone - Call (520) 792-9222 or TDD (520) 628-1565</li>
                <li>By Mail</li>
            </ul>

            <h4 id="header2question9">Why register a FareCard Card?</h4>
            <h5>There are three key benefits to taking the time to register
                your SunGO Card.</h5>
            <ul>
                <li>Balance Protection</li>
                <li>One Ride Safety Net</li>
                <li>Auto Buy</li>
            </ul>

            <h4 id="header2question10">1. Balance Protection</h4>
            To protect the cash value and/or passes loaded on your SunGO Card, we
            recommend opening an online account and registering your card. That
            way, if your card is reported lost or stolen, Sun Tran can replace
            the cash value or days remaining.

            <h4 id="header2question11">2. One Ride Safety Net</h4>
            Registered cardholders who board a bus without cash value or a valid
            pass on their SunGO Card will be allowed one ride. Before the SunGO
            Card can be used again, cash value to cover that one ride must be
            added, as well as cash value or a pass to pay for the current ride or
            fare.

            <h4 id="header2question12">3. Auto Buy</h4>
            Eliminate the need to track the cash value or days remaining on your
            SunGO Card. Registered SunGO cardholders can set up an account to
            automatically add value through a secure funds transfer from your
            credit or debit card on a regular basis. Auto Buy adds a
            predetermined cash value when the value on your card is depleted, or
            a new pass is added once the existing product expires.

            <h4 id="header2question13">When a FareCard Card or FareCard Ticket is lost, is the pass or value lost as well?</h4>
            SunGO Card - Not necessarily. To protect the cash value and/or passes
            you have loaded on your SunGO Card, we recommend registering your
            card. That way, if your card is reported lost or stolen, Sun Tran can
            replace the remaining cash value or days. Once reported lost, a new
            SunGO Card can be purchased. Please note there is up to a 48 hour
            delay for the remaining balance on the lost card to be transferred to
            the new card.SunGO Ticket - If you lose a SunGO Ticket, which cannot
            be registered, the pass would be lost. To protect your balance, we
            strongly recommend purchasing the SunGO Card instead of the ticket
            and registering the card with Sun Tran.

            <h4 id="header2question14">What if I board without enough cash value or a valid pass?</h4>
            This feature is only available to registered SunGO cardholders, and
            not those with a SunGO Ticket or an unregistered SunGO Card. All
            SunGO ID &amp; Cards are registered at the time issued and also enjoy
            this benefit. Registered cardholders who board a bus without cash
            value or a valid pass on their SunGO Card will be allowed one ride.
            Before the SunGO Card can be used again, cash value to cover that one
            ride must be added as well as cash value or a pass to pay for the
            current ride.

            <h4 id="header2question15">Can I automatically add cash value or a pass each month?</h4>
            Registered SunGO cardholders can set up an account to automatically
            add value or a pass through a secure funds transfer from your credit
            or debit card on a regular basis called Auto Buy. Auto Buy adds a
            predetermined cash value when the value on your card is depleted, or
            a new pass product is added once the existing product expires.

            <h4 id="header2question16">How long will the FareCard Card and FareCard Ticket last?</h4>
            A SunGO Card may last up to four years, while SunGO tickets may last
            up to 90 days. How long a card lasts depends on how well ittaken care
            of. Washing, bending or cutting the card or ticket will damage the
            internal chip, making them inoperable. Once inoperable, a new card
            would need to be purchased.

            <h4 id="header2question17">How many passes can I have on my FareCard Card at one time?</h4>
            SunGO Cards have a built-in chip that allows you to have one active
            pass, up to two passes in the queue, and additional cash value on a
            card at one time. Passes will be used in the order they are
            purchased.

            <h4 id="header2question18">What happens if my FareCard Card no longer works?</h4>
            If there is no physical damage to the SunGO Card, then bring it to
            the Sun Tran Administrative office. If an evaluation of the card
            indicates the card has a faulty chip, then the SunGO Card will be
            replaced at no charge. Otherwise, a new card can be purchased online,
            at pass sales outlets or at a ticket vending machine at the transit
            centers.

            <h4 id="header2question19">Link Card</h4>
            Cards can be linked to a persons account. You can view and manage
            them from one place. It is convenient for a user to link all their
            cards to a single account.

        </div>
    </div>', 1, 'admin', 'admin');
