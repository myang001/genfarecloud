-- ticketdiscounts
INSERT INTO ticketdiscounts (TicketID, DiscountTypeID, OrganizationID, Display, Description, TouchedBy, CreatedBy) VALUES ('1', '1', '10', 'DeanTicket', 'Deans 90 Percent Off Fare', 'dgoedken', 'dgoedken');
INSERT INTO ticketdiscounts (TicketID, DiscountTypeID, OrganizationID, Display, Description, TouchedBy, CreatedBy) VALUES ('1', '2', '10', 'DeanTicket', 'Deans 90 Percent Off Fare', 'dgoedken', 'dgoedken');

-- peoplecards
INSERT INTO peoplecards (PersonID, CardNumber, PaymentTypeID, LastFour, DateExpires, TouchedBy, CreatedBy) VALUES ('1', '255', '1', '1234', '10/10/15', 'dgoedken', 'dgoedken');

-- subscriptions suscriptionID added was '4', '5'	
INSERT INTO subscriptions (WalletContentsID, PaymentTypeID, personid, cardnumber, ReplenishPrice, ReplenishValue, ReplenishThreshold, SubscriptionStatusID, TouchedBy, CreatedBy) VALUES ('1', '1', '1', '255', '7.50', '7.50', '30.00', '1', 'dgoedken', 'dgoedken');

INSERT INTO subscriptions (WalletContentsID, PaymentTypeID, personid, cardnumber, ReplenishPrice, ReplenishValue, ReplenishThreshold, SubscriptionStatusID, TouchedBy, CreatedBy) VALUES ('79', '1', '1', '255', '7.50', '7.50', '30.00', '1', 'dgoedken', 'dgoedken');

-- Wallets  (update)
UPDATE wallets SET walletstatusid = '1' WHERE walletid = '128'