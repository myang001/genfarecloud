update tickets 
set 
ReplenishPrice = Price, ReplenishValue=3, ReplenishThreshold=1.00, isSubscribable='T'
where description='3 Day Pass';

update tickets 
set 
ReplenishPrice = Price, ReplenishValue=31.00, ReplenishThreshold=3.00, isSubscribable='T' 
where description='Weekday Full Fare 31 Day Rolling Swiper';

update tickets 
set 
ReplenishPrice = Price, ReplenishValue=31.00, ReplenishThreshold=3.00, isSubscribable='T'
where description='Full Fare 31 Day Rolling Swiper';

update tickets 
set 
ReplenishPrice = Price, ReplenishValue=31.00, ReplenishThreshold=3.00, isSubscribable='T' 
where description='Weekday Half Fare 31 Day Rolling Swiper';

update tickets 
set 
ReplenishPrice = Price, ReplenishValue=31.00, ReplenishThreshold=3.00, isSubscribable='T' 
where description='Half Fare 31 Day Rolling Swiper';

update tickets 
set 
ReplenishPrice = Price, ReplenishValue=10.00, ReplenishThreshold=1.00, isSubscribable='T'  
where description='Ten-Trip Pass';

update tickets 
set 
ReplenishPrice = Price, ReplenishValue=13.00, ReplenishThreshold=1.50, isSubscribable='T'  
where description='STAR 13 Trip Pass';

update tickets 
set 
ReplenishPrice = Price, ReplenishValue=10.00, ReplenishThreshold=1.00, isSubscribable='T'  
where description='NZ Zone 1 10 Ride';

update tickets 
set 
ReplenishPrice = Price, ReplenishValue=10.00, ReplenishThreshold=1.00, isSubscribable='T'  
where description='NZ Zone 2 10 Ride';

update tickets 
set 
ReplenishPrice = Price, ReplenishValue=10.00, ReplenishThreshold=1.00, isSubscribable='T'   
where description='NZ Zone 3 10 Ride';

update tickets 
set 
ReplenishPrice = Price, ReplenishValue=10.00, ReplenishThreshold=1.00, isSubscribable='T'   
where description='NZ Zone 1 10 Ride Reduced Fare';

update tickets 
set 
ReplenishPrice = Price, ReplenishValue=10.00, ReplenishThreshold=1.00, isSubscribable='T'  
where description='NZ Zone 2 10 Ride Reduced Fare';

update tickets 
set 
ReplenishPrice = Price, ReplenishValue=10.00, ReplenishThreshold=1.00, isSubscribable='T'  
where description='NZ Zone 3 10 Ride Reduced Fare';

update tickets 
set 
ReplenishPrice = Price, ReplenishValue=31.00, ReplenishThreshold=3.00, isSubscribable='T'  
where description='NZ Zone 1 Monthly Unlimited Rides';

update tickets 
set 
ReplenishPrice = Price, ReplenishValue=31.00, ReplenishThreshold=3.00, isSubscribable='T'  
where description='NZ Zone 2 Monthly Unlimited Rides';

update tickets 
set 
ReplenishPrice = Price, ReplenishValue=31.00, ReplenishThreshold=3.00, isSubscribable='T'  
where description='NZ Zone 3 Monthly Unlimited Rides';