UPDATE equipment SET Description='Equip1', SerialNumber='12345', Version='1.0', Status='1' WHERE EquipmentID='1';
UPDATE equipment SET Description='Equip2', SerialNumber='67890', Version='1.1', Status='1' WHERE EquipmentID='2';
UPDATE equipment SET Description='Equip3', SerialNumber='232323', Version='1.4', Status='1' WHERE EquipmentID='3';
UPDATE equipment SET Description='Equip4', SerialNumber='676876', Version='4.3', Status='1' WHERE EquipmentID='4';
UPDATE equipment SET Description='Equip5', SerialNumber='09876', Version='1.0', Status='1' WHERE EquipmentID='5';

UPDATE locations SET Description='Albany Garage Lower', Longtitude='-73.762894', Latitude='+42.644298' WHERE LocationID='1';

UPDATE equipment SET Status='1' WHERE EquipmentID IN ('1', '2', '3', '4', '5');
