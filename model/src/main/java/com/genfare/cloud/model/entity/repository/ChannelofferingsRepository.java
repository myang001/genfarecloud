package com.genfare.cloud.model.entity.repository;

import org.springframework.data.repository.CrudRepository;

import com.genfare.cloud.model.entity.Channelofferings;
import com.genfare.cloud.model.entity.ChannelofferingsId;

public interface ChannelofferingsRepository
    extends CrudRepository<Channelofferings, ChannelofferingsId> {

}
