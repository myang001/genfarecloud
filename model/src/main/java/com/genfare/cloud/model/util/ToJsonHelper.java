package com.genfare.cloud.model.util;

import java.util.Collection;

import org.json.JSONArray;

public class ToJsonHelper {

    public static String toJsonArray(Collection<?> object) {
        JSONArray js = new JSONArray();

        for (Object o : object) {
            if (o instanceof ToJson) {
                js.put(((ToJson) o).toJson());
            }
        }
        return js.toString();
    }

    public static String cleanMarkup(String json) {
        return json.replaceAll("\\\\", "").replaceAll("\"\\[", "\\[").replaceAll("\\]\"", "\\]").replaceAll("\"\\{",
            "\\{").replaceAll("\\}\"",
            "\\}");
    }
}
