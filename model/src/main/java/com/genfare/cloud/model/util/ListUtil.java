package com.genfare.cloud.model.util;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Provides utility methods for List manipulations
 * 
 * @author jyore
 */
public class ListUtil {

    /**
     * Returns the Union of two Lists
     * 
     * <b>Note:</b> Original data is not consumed or modified in the process
     * 
     * @param a The first list
     * @param b The second list
     * @return The Union of the two Lists
     */
    public static <T> List<T> union(List<T> a, List<T> b) {
        Set<T> set = new HashSet<T>();
        set.addAll(a);
        set.addAll(b);

        return new ArrayList<T>(set);
    }

    /**
     * Returns the Union of two Sets
     * 
     * <b>Note:</b> Original data is not consumed or modified in the process
     * 
     * @param a The first set
     * @param b The second set
     * @return The Union of the two Sets
     */
    public static <T> Set<T> union(Set<T> a, Set<T> b) {
        Set<T> set = new HashSet<T>();
        set.addAll(a);
        set.addAll(b);

        return set;
    }

    /**
     * Returns the Intersection of two Lists
     * 
     * <b>Note:</b> Original data is not consumed or modified in the process
     * 
     * @param a The first list
     * @param b The second list
     * @return The Intersection of the two Lists
     */
    public static <T> List<T> intersection(List<T> a, List<T> b) {
        List<T> list = new ArrayList<T>();

        for (T t : a) {
            if (b.contains(t)) {
                list.add(t);
            }
        }

        return list;
    }

    /**
     * Returns the Intersection of two Sets
     * 
     * <b>Note:</b> Original data is not consumed or modified in the process
     * 
     * @param a The first set
     * @param b The second set
     * @return The Intersection of the two Sets
     */
    public static <T> Set<T> intersection(Set<T> a, Set<T> b) {
        Set<T> set = new HashSet<T>();

        for (T t : a) {
            if (b.contains(t)) {
                set.add(t);
            }
        }

        return set;
    }
}
