package com.genfare.cloud.model.entity.repository;

import org.springframework.data.repository.CrudRepository;

import com.genfare.cloud.model.entity.Events;

public interface EventdetailsRepository
    extends CrudRepository<Events, Integer> {

}
