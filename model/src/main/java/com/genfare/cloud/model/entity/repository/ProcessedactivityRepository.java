package com.genfare.cloud.model.entity.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.genfare.cloud.model.entity.Processedactivity;

public interface ProcessedactivityRepository
    extends CrudRepository<Processedactivity, Integer> {

    @Query("select count(*) from Processedactivity p where p.wallets.walletId = ?")
    public Long getProcessedActivityCount(Long walletId);

}
