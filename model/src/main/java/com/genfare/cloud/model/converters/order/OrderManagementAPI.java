package com.genfare.cloud.model.converters.order;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.genfare.cloud.model.om.OrderType;

@XmlRootElement(name = "OrderManagementAPI")
public class OrderManagementAPI {

    @XmlAttribute(name = "SchemaName")
    private String schemaName;

    @XmlElement(name = "Order", required = true)
    private OrderType orderType;

    @Override
    public String toString() {
        return "MyOrderManagementAPI schemaName=" + schemaName + "\norderType:" + orderType;
    }

    public OrderType getOrderType() {
        return orderType;
    }
}
