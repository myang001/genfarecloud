package com.genfare.cloud.model.service;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.genfare.cloud.model.entity.Farecodes;
import com.genfare.cloud.model.entity.Offerings;
import com.genfare.cloud.model.entity.Tickets;
import com.genfare.cloud.model.entity.Wallets;
import com.genfare.cloud.model.entity.repository.OfferingsRepository;
import com.genfare.cloud.model.entity.repository.PeopleRepository;
import com.genfare.cloud.model.entity.repository.TicketsRepository;
import com.genfare.cloud.model.security.User;
import com.genfare.cloud.model.types.ChannelsType;
import com.genfare.cloud.model.types.FarecodesType;
import com.genfare.cloud.model.types.TickettypesType;

@Transactional
@Service
public class TicketsEligibilityServiceImpl
    implements TicketsEligibilityService {

    @Autowired
    private TicketsRepository ticketsRepository;

    @Autowired
    private OfferingsRepository offeringrepo;

    @Autowired
    private PeopleRepository peopleRepo;

    @Override
    public Iterable<Tickets> getEligibleTickets(Wallets wallets) {
        if (wallets != null) {
            return ticketsRepository.findAll();// Determining by wallet falls under business rules
        } else {
            return ticketsRepository.findAll();
        }
    }

    @Override
    public Tickets getTicketById(Integer ticketId) {
        return ticketsRepository.findOne(ticketId);
    }

    @Override
    public Map<String, Iterable<Offerings>> getAllEligibleTicketOfferings(Wallets wallets, User user) {
        Map<String, Iterable<Offerings>> map = new HashMap<String, Iterable<Offerings>>();

        Byte channel = ChannelsType.EFARE.getIndex().byteValue();
        Byte farecode = FarecodesType.FULL.getIndex().byteValue();
        if (wallets != null) {
            farecode = wallets.getFarecodes().getFarecodeId();
        } else {
            if (user != null && ChannelsType.EFARE.getType().getCanInherit()) {
                List<Farecodes> fcs = peopleRepo.findFarecodesForPerson(user.getId());
                if (fcs.size() > 0) {
                    farecode = fcs.get(0).getFarecodeId();
                }
            }
        }

        Collection<Byte> periods = new HashSet<Byte>();
        periods.add(TickettypesType.FIXED_PERIOD_PASS.getIndex().byteValue());
        periods.add(TickettypesType.FLOATING_PERIOD_PASS.getIndex().byteValue());
        map.put("periodTickets", offeringrepo.findOfferingsFilterChannelAndFarecodeAndType(channel, farecode, periods));

        map.put("rideTickets", offeringrepo.findOfferingsFilterChannelAndFarecodeAndType(channel, farecode, TickettypesType.STORED_RIDE.getIndex().byteValue()));
        map.put("fixedStoredTickets", offeringrepo.findOfferingsFilterChannelAndFarecodeAndType(channel, farecode, TickettypesType.STORED_FIXED_VALUE.getIndex().byteValue()));
        map.put("storedTickets", offeringrepo.findOfferingsFilterChannelAndFarecodeAndType(channel, farecode, TickettypesType.STEPPED_VALUE.getIndex().byteValue()));
        map.put("promoTickets", offeringrepo.findOfferingsFilterChannelAndFarecodeAndType(channel, farecode, TickettypesType.FAREST_FARE.getIndex().byteValue()));
        return map;
    }

}
