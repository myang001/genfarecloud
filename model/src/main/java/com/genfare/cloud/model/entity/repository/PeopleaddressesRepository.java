package com.genfare.cloud.model.entity.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.genfare.cloud.model.entity.Peopleaddresses;

public interface PeopleaddressesRepository
    extends CrudRepository<Peopleaddresses, Integer> {

    @Query("from Peopleaddresses addr where addr.id.personId = ?")
    public List<Peopleaddresses> findAddressesByPersonId(Integer personid);

    @Query("from Peopleaddresses addr where addr.id.personId = ? and addr.id.addressTypeId = ?")
    public Peopleaddresses findAddressByPersonIdAddressTypeId(Integer personId, Integer addressTypeId);
}
