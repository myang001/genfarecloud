package com.genfare.cloud.model.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.genfare.cloud.model.entity.Groupprivileges;
import com.genfare.cloud.model.entity.GroupprivilegesId;
import com.genfare.cloud.model.entity.Groups;
import com.genfare.cloud.model.entity.Privileges;
import com.genfare.cloud.model.entity.repository.GroupprivilegesRepository;
import com.genfare.cloud.model.entity.repository.PrivilegesRepository;

@Service
@Transactional
public class PrivilegesManagementServiceImpl
    implements PrivilegesManagementService {

    @Autowired
    PrivilegesRepository privilegesRepo;

    @Autowired
    GroupprivilegesRepository groupprivilegesRepo;

    @Override
    public List<Privileges> savePrivilegesForGroup(Groups group, JSONObject moduleComponentPrivilegesMapping) {
        List<String> privilegeNames = new ArrayList<String>();

        Iterator<?> moduleKeys = moduleComponentPrivilegesMapping.keys();

        while (moduleKeys.hasNext()) {
            String moduleName = (String) moduleKeys.next();
            JSONObject componentPrivilegesMapping = moduleComponentPrivilegesMapping.getJSONObject(moduleName);

            Iterator<?> componentKeys = componentPrivilegesMapping.keys();

            while (componentKeys.hasNext()) {
                String componentName = (String) componentKeys.next();
                JSONArray privilegesList = componentPrivilegesMapping.getJSONArray(componentName);

                for (int i = 0; i < privilegesList.length(); i++) {
                    privilegeNames.add(privilegesList.getString(i));
                }
            }
        }

        List<Privileges> privileges = new ArrayList<Privileges>();

        for (String name : privilegeNames) {
            Privileges privilege = privilegesRepo.findByName(name.replace('-', ' '));
            if (privilege != null) {
                Groupprivileges gp = new Groupprivileges();
                gp.setId(new GroupprivilegesId(group.getGroupId(), privilege.getPrivilegeId()));
                groupprivilegesRepo.save(gp);
                privileges.add(privilege);
            }
        }

        return privileges;
    }

    @Override
    public void clearPriviligesForGroup(Groups groups) {
        List<Groupprivileges> groupprivileges = groupprivilegesRepo.findByGroupId(groups.getGroupId());
        for (Groupprivileges groupprivilege : groupprivileges) {
            groupprivilegesRepo.delete(groupprivilege);
        }
    }

}
