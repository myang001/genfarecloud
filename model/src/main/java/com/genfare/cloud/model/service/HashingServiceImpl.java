package com.genfare.cloud.model.service;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service("hashingService")
public class HashingServiceImpl
    implements HashingService {

    private static final Logger LOGGER = LoggerFactory.getLogger(HashingServiceImpl.class);

    @Override
    public String md5Hash(String value) {

        try {
            MessageDigest md5 = MessageDigest.getInstance("MD5");
            return hashIt(md5, value);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public String sha1Hash(String value) {

        try {
            MessageDigest sha1 = MessageDigest.getInstance("SHA-1");
            return hashIt(sha1, value);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        }

    }

    @Override
    public String hmacSha1(String key, String message) throws NoSuchAlgorithmException, InvalidKeyException {
        SecretKeySpec keySpec = new SecretKeySpec(key.getBytes(), "HmacSHA1");
        Mac mac = Mac.getInstance("HmacSHA1");
        mac.init(keySpec);
        byte[] result = mac.doFinal(message.getBytes());
        return new String(Base64.encodeBase64(result));
    }

    @Override
    public String springBCryptHash(String value) {
        BCryptPasswordEncoder enc = new BCryptPasswordEncoder();
        return enc.encode(value);
    }

    @Override
    public String springBCryptHash(String value, int strength) {
        BCryptPasswordEncoder enc = new BCryptPasswordEncoder(strength);
        return enc.encode(value);
    }

    @Override
    public String hmacDigest(String msg, String keyString, String algo) {
        String digest = null;
        try {
            SecretKeySpec key = new SecretKeySpec((keyString).getBytes("UTF-8"), algo);
            Mac mac = Mac.getInstance(algo);
            mac.init(key);

            byte[] bytes = mac.doFinal(msg.getBytes("ASCII"));

            StringBuffer hash = new StringBuffer();
            for (int i = 0; i < bytes.length; i++) {
                String hex = Integer.toHexString(0xFF & bytes[i]);
                if (hex.length() == 1) {
                    hash.append('0');
                }
                hash.append(hex);
            }
            digest = hash.toString();
        } catch (UnsupportedEncodingException e) {
        } catch (InvalidKeyException e) {
            LOGGER.debug("Invalid Agency Hash Key: " + e.getLocalizedMessage());
        } catch (NoSuchAlgorithmException e) {
        }
        return digest;
    }

    private static String hashIt(MessageDigest alg, String value) {
        alg.reset();
        alg.update(value.getBytes());
        return byteArray2Hex(alg.digest());
    }

    private static String byteArray2Hex(byte[] hash) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0, l = hash.length; i < l; i++) {
            sb.append(Integer.toString((hash[i] & 0xff) + 0x100, 16).substring(1));
        }
        return sb.toString();
    }
}
