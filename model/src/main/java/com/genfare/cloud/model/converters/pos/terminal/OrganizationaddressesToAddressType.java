package com.genfare.cloud.model.converters.pos.terminal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;

import com.genfare.cloud.model.entity.Organizationaddresses;
import com.genfare.pos.core.model.terminal.AddressType;

public class OrganizationaddressesToAddressType implements
    Converter<Organizationaddresses, AddressType> {

    private static final Logger log = LoggerFactory
        .getLogger(OrganizationaddressesToAddressType.class);

    @Override
    public AddressType convert(Organizationaddresses oa) {
        AddressType at = new AddressType();
        at.setLocationName(oa.getOrganizations().getName());
        at.setAddress1(oa.getAddressLine1());
        at.setAddress2(oa.getAddressLine2());
        at.setCity(oa.getCity());
        at.setPostalCode(oa.getPostalCode());
        at.setCountry(oa.getCountry());
        return at;
    }
}
