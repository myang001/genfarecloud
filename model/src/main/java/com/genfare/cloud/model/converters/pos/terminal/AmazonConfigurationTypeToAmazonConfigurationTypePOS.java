package com.genfare.cloud.model.converters.pos.terminal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;

import com.genfare.cloud.model.tenant.impl.AmazonConfigurationType;

public class AmazonConfigurationTypeToAmazonConfigurationTypePOS
    implements
    Converter<AmazonConfigurationType, com.genfare.pos.core.model.terminal.AmazonConfigurationType> {

    private static final Logger log = LoggerFactory
        .getLogger(AmazonConfigurationTypeToAmazonConfigurationTypePOS.class);

    @Override
    public com.genfare.pos.core.model.terminal.AmazonConfigurationType convert(
        AmazonConfigurationType act) {
        com.genfare.pos.core.model.terminal.AmazonConfigurationType actPOS = new com.genfare.pos.core.model.terminal.AmazonConfigurationType();

        actPOS.setAWSAccessKey(act.getAwsAccessKey());
        actPOS.setAWSSecretKey(act.getAwsSecretKey());
        // I do not see where we are holding the S3 password
        // actPOS.setAwsS3Prefix(act.get);
        actPOS.setAWSSqsPrefix(act.getAwsSqsPrefix());
        actPOS.setAWSSnsPrefix(act.getAwsSnsPrefix());
        actPOS.setCreateOrderDestination(act.getCreateOrderDestination());
        actPOS.setOrderStatusUpdateDestination(act
            .getOrderStatusUpdateDestination());
        // We dont hold eventRecordDestination, personBucket,
        // pendingActivityBucket, fareTableBucket
        // actPOS.setEventRecordDestination(act.get);

        return actPOS;
    }
}
