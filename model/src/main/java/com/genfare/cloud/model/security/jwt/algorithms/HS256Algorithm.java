package com.genfare.cloud.model.security.jwt.algorithms;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Hex;

import com.genfare.cloud.model.security.jwt.interfaces.JwtCryptoAlgorithm;

public class HS256Algorithm implements JwtCryptoAlgorithm {

    public String getType() {
        return "HS256";
    }

    public String encrypt(String key, String data) throws NoSuchAlgorithmException, InvalidKeyException {
        SecretKey secret = new SecretKeySpec(key.getBytes(), "HmacSHA256");
        Mac mac = Mac.getInstance("HmacSHA256");
        mac.init(secret);
        return Hex.encodeHexString(mac.doFinal(data.getBytes()));
    }
}
