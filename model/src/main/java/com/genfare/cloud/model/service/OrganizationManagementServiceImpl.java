package com.genfare.cloud.model.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.genfare.cloud.model.entity.Groups;
import com.genfare.cloud.model.entity.Organizations;
import com.genfare.cloud.model.entity.repository.GroupsRepository;
import com.genfare.cloud.model.security.User;

@Transactional
@Service
public class OrganizationManagementServiceImpl implements OrganizationManagementService {

    @Autowired
    GroupsRepository groupsRepository;

    @Override
    public Organizations getOrganizationForUser(User user) {

        List<Groups> groupsForUser = groupsRepository.findByUserId(user.getId());
        if (groupsForUser.isEmpty()) {
            return null;
        }
        // TODO Handle if not all groups belong to the same organization
        return groupsForUser.get(0).getOrganizations();
    }

    @Override
    public String getOrganizationNameForUser(User user) {
        Organizations organization = getOrganizationForUser(user);
        if (organization != null) {
            return organization.getName();
        }
        return null;
    }

}
