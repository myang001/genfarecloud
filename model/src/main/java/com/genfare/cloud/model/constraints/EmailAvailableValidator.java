package com.genfare.cloud.model.constraints;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import com.genfare.cloud.model.annotation.EmailAvailable;
import com.genfare.cloud.model.contact.Email;
import com.genfare.cloud.model.dto.UserDetails;
import com.genfare.cloud.model.service.EmailService;

public class EmailAvailableValidator
    implements ConstraintValidator<EmailAvailable, Email> {

    @Autowired
    EmailService emailService;

    @Override
    public void initialize(EmailAvailable constraintAnnotation) {

    }

    @Override
    public boolean isValid(Email value, ConstraintValidatorContext context) {

        if (value == null || value.getAddress() == null || value.getAddress().isEmpty()) {
            return true;
        }

        /*
         * Load the current authorization information and make sure that the email address is not the same as the current logged
         * in user
         */
        Authentication springauth = SecurityContextHolder.getContext().getAuthentication();
        if (springauth != null) {
            Object principal = springauth.getPrincipal();
            String username = (principal instanceof String) ? principal.toString() : ((UserDetails) principal).getUsername();

            if (value.getAddress().equals(username)) {
                return true;
            }
        }

        Email e = emailService.findEmail(value.getAddress());
        return e == null;
    }

}
