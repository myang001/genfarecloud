/**
 *
 */
package com.genfare.cloud.model.converters;

import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;

import com.genfare.cloud.model.entity.Creditcardactivity;
import com.genfare.cloud.model.om.CreditCardActivityType;

/**
 * @author mpeter
 */
public class CreditCardActivityToCreditCardActivityTypeType implements
    Converter<Creditcardactivity, CreditCardActivityType> {

    private static final Logger log = LoggerFactory
        .getLogger(CreditCardActivityToCreditCardActivityTypeType.class);

    @Override
    public CreditCardActivityType convert(Creditcardactivity cca) {
        GregorianCalendar c = new GregorianCalendar();

        CreditCardActivityType ccat = new CreditCardActivityType();
        ccat.setCreditCardActivityID(cca.getCreditCardActivityId());
        ccat.setGatewayReturnCode(cca.getGatewayReturnCode());
        ccat.setGatewayReturnText(cca.getGatewayReturnText());
        try {
            c.setTime(cca.getGatewayTimeStamp());
            ccat.setGatewayTimeStamp(DatatypeFactory.newInstance()
                .newXMLGregorianCalendar(c));
        } catch (DatatypeConfigurationException e) {
            log.error("Unable to convert Tickets to TicketType", e);
            return null;
        }
        return ccat;
    }

}
