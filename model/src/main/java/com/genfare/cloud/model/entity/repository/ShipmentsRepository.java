package com.genfare.cloud.model.entity.repository;

import org.springframework.data.repository.CrudRepository;

import com.genfare.cloud.model.entity.Shipments;

public interface ShipmentsRepository
    extends CrudRepository<Shipments, Integer> {

}
