package com.genfare.cloud.model.security.jwt;

import org.json.JSONObject;

public class JwtHeader {

    private final String alg;
    private final String typ;

    private JwtHeader(Builder builder) {
        this.alg = builder.alg;
        this.typ = builder.typ;
    }

    public String getAlg() {
        return alg;
    }

    public String getTyp() {
        return typ;
    }

    public String toJson() {
        return new JSONObject(this).toString();
    }

    public static JwtHeader fromJson(String json) {
        JSONObject obj = new JSONObject(json);
        return new JwtHeader.Builder(
            obj.getString("alg"),
            obj.getString("typ")).build();
    }

    public static class Builder {
        private String alg;
        private String typ;

        public Builder(String alg, String typ) {
            this.alg = alg;
            this.typ = typ;
        }

        public JwtHeader build() {
            return new JwtHeader(this);
        }
    }
}
