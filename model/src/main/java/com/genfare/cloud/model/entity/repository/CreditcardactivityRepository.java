package com.genfare.cloud.model.entity.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.jpa.repository.Query;
import com.genfare.cloud.model.entity.Creditcardactivity;

public interface CreditcardactivityRepository
    extends CrudRepository<Creditcardactivity, Long> {

    @Query("select c.gatewayTransactionNumber from Creditcardactivity c where c.token = ?")
    public Long selectTransactionTagByToken(String token);

}
