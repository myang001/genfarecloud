package com.genfare.cloud.model.entity.repository;

import org.springframework.data.repository.CrudRepository;

import com.genfare.cloud.model.entity.OrganizationStatus;

public interface OrganizationstatusRepository extends CrudRepository<OrganizationStatus, Byte> {

}
