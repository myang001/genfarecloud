package com.genfare.cloud.model.converters.pos.activity;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;

import com.genfare.cloud.model.entity.Activitytypes;
import com.genfare.pos.core.model.pending.ActivityTypeType;

public class ActivityTypesToActivityTypeTypeConverter implements
    Converter<Activitytypes, ActivityTypeType> {

    private static final Logger log = LoggerFactory
        .getLogger(ActivityTypesToActivityTypeTypeConverter.class);

    @Override
    public ActivityTypeType convert(Activitytypes at) {
        ActivityTypeType att = new ActivityTypeType();

        att.setActivityTypeId(at.getActivityTypeId());
        att.setDescription(at.getDescription());

        return att;
    }
}
