package com.genfare.cloud.model.entity.repository;

import org.springframework.data.repository.CrudRepository;

import com.genfare.cloud.model.entity.PeopleFarecodes;
import com.genfare.cloud.model.entity.PeopleFarecodesId;

public interface PeopleFarecodesRepository
    extends CrudRepository<PeopleFarecodes, PeopleFarecodesId> {

}
