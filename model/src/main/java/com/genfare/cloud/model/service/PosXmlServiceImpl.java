package com.genfare.cloud.model.service;

import java.io.StringWriter;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.genfare.cloud.model.converters.pos.activity.ActivityToActivityTypeConverter;
import com.genfare.cloud.model.converters.pos.activity.PendingActivitiesToPendingActivityTypeConverter;
import com.genfare.cloud.model.converters.pos.fare.OfferingsToOfferingTypeConverter;
import com.genfare.cloud.model.converters.pos.fare.TaxratesToTaxRateTypeConverter;
import com.genfare.cloud.model.converters.pos.fare.TickettypesToTicketTypeTypeConverter;
import com.genfare.cloud.model.converters.pos.fare.WallettypesToWalletTypeTypeConverter;
import com.genfare.cloud.model.converters.pos.fare.ZoneToZoneTypeConverter;
import com.genfare.cloud.model.converters.pos.person.PeopleToPersonTypeConverter;
import com.genfare.cloud.model.converters.pos.terminal.TenantConfigurationImplAndEquipmentToTerminalConfigAPIType;
import com.genfare.cloud.model.entity.Activities;
import com.genfare.cloud.model.entity.Equipment;
import com.genfare.cloud.model.entity.Offerings;
import com.genfare.cloud.model.entity.Operators;
import com.genfare.cloud.model.entity.Pendingactivity;
import com.genfare.cloud.model.entity.Taxrates;
import com.genfare.cloud.model.entity.Tickettypes;
import com.genfare.cloud.model.entity.Wallettypes;
import com.genfare.cloud.model.entity.Zones;
import com.genfare.cloud.model.tenant.impl.TenantConfigurationImpl;
import com.genfare.pos.core.model.activity.ActivityAPIType;
import com.genfare.pos.core.model.activity.ActivityType;
import com.genfare.pos.core.model.common.TicketTypeType;
import com.genfare.pos.core.model.common.ZoneType;
import com.genfare.pos.core.model.fare.FareTableAPIType;
import com.genfare.pos.core.model.fare.OfferingType;
import com.genfare.pos.core.model.fare.TaxRateType;
import com.genfare.pos.core.model.fare.WalletTypeType;
import com.genfare.pos.core.model.pending.PendingActivityAPIType;
import com.genfare.pos.core.model.pending.PendingActivityType;
import com.genfare.pos.core.model.person.PersonAPIType;
import com.genfare.pos.core.model.person.PersonType;
import com.genfare.pos.core.model.terminal.TerminalConfigAPIType;

public class PosXmlServiceImpl implements PosXmlService {

    private static final Logger log = LoggerFactory
        .getLogger(PosXmlServiceImpl.class);

    @Override
    public String generateFareTableXml(List<Wallettypes> walletTypes,
        List<Zones> zones, List<Taxrates> taxRates,
        List<Tickettypes> ticketTypes, List<Offerings> offerings) {
        GregorianCalendar c = new GregorianCalendar();
        c.setTime(new Date());

        com.genfare.pos.core.model.fare.ObjectFactory of = new com.genfare.pos.core.model.fare.ObjectFactory();
        FareTableAPIType ft = of.createFareTableAPIType();

        List<WalletTypeType> listWalletTypes = ft.getWalletType();
        WallettypesToWalletTypeTypeConverter wttConverter = new WallettypesToWalletTypeTypeConverter();
        for (Wallettypes wt : walletTypes) {
            listWalletTypes.add(wttConverter.convert(wt));
        }

        List<ZoneType> listZoneTypes = ft.getZone();
        ZoneToZoneTypeConverter ztConverter = new ZoneToZoneTypeConverter();
        for (Zones z : zones) {
            listZoneTypes.add(ztConverter.convert(z));
        }

        List<TaxRateType> listTaxRateTypes = ft.getTaxRate();
        TaxratesToTaxRateTypeConverter trConverter = new TaxratesToTaxRateTypeConverter();
        for (Taxrates tr : taxRates) {
            listTaxRateTypes.add(trConverter.convert(tr));
        }

        List<TicketTypeType> listTicketTypes = ft.getTicketType();
        TickettypesToTicketTypeTypeConverter ttConverter = new TickettypesToTicketTypeTypeConverter();
        for (Tickettypes tt : ticketTypes) {
            listTicketTypes.add(ttConverter.convert(tt));
        }

        List<OfferingType> listOfferingTypes = ft.getOffering();
        OfferingsToOfferingTypeConverter otConverter = new OfferingsToOfferingTypeConverter();
        for (Offerings o : offerings) {
            listOfferingTypes.add(otConverter.convert(o));
        }

        try {
            ft.setLastModifiedDate(DatatypeFactory.newInstance()
                .newXMLGregorianCalendar(c));
        } catch (DatatypeConfigurationException e) {
            log.error("Error building date", e);
            return null;
        }

        JAXBContext jaxbContext = null;
        try {
            jaxbContext = JAXBContext.newInstance(FareTableAPIType.class);
            JAXBElement<FareTableAPIType> elem = of.createFareTableAPI(ft);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            final StringWriter stringWriter = new StringWriter();
            jaxbMarshaller.marshal(elem, stringWriter);

            return stringWriter.toString();
        } catch (JAXBException e) {
            log.error("Error Creating POS FareTable XML", e);
        }

        return null;
    }

    // Again this needs to take equipment into consideration I think but I will
    // talk about this on Monday
    @Override
    public String generatePersonXml(Operators o) {
        GregorianCalendar c = new GregorianCalendar();
        c.setTime(new Date());

        com.genfare.pos.core.model.person.ObjectFactory of = new com.genfare.pos.core.model.person.ObjectFactory();
        PersonAPIType pat = of.createPersonAPIType();

        List<PersonType> pTypeList = pat.getPerson();
        PeopleToPersonTypeConverter p2ptc = new PeopleToPersonTypeConverter();
        pTypeList.add(p2ptc.convert(o));

        JAXBContext jaxbContext = null;
        try {
            jaxbContext = JAXBContext.newInstance(PersonAPIType.class);
            JAXBElement<PersonAPIType> elem = of.createPersonAPI(pat);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            final StringWriter stringWriter = new StringWriter();
            jaxbMarshaller.marshal(elem, stringWriter);

            return stringWriter.toString();
        } catch (JAXBException e) {
            log.error("Error Creating POS Person XML", e);
        }

        return null;
    }

    @Override
    public String generateActivityXml(Activities a) {
        GregorianCalendar c = new GregorianCalendar();
        c.setTime(new Date());

        com.genfare.pos.core.model.activity.ObjectFactory of = new com.genfare.pos.core.model.activity.ObjectFactory();
        ActivityAPIType aat = of.createActivityAPIType();

        List<ActivityType> ats = aat.getActivity();
        ActivityToActivityTypeConverter a2atc = new ActivityToActivityTypeConverter();
        ats.add(a2atc.convert(a));

        JAXBContext jaxbContext = null;
        try {
            jaxbContext = JAXBContext.newInstance(ActivityAPIType.class);
            JAXBElement<ActivityAPIType> elem = of.createActivityAPI(aat);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            final StringWriter stringWriter = new StringWriter();
            jaxbMarshaller.marshal(elem, stringWriter);

            return stringWriter.toString();
        } catch (JAXBException e) {
            log.error("Error Creating POS Activity XML", e);
        }

        return null;
    }

    @Override
    public String generatePendingActivityXml(Pendingactivity pa) {
        GregorianCalendar c = new GregorianCalendar();
        c.setTime(new Date());

        com.genfare.pos.core.model.pending.ObjectFactory of = new com.genfare.pos.core.model.pending.ObjectFactory();
        PendingActivityAPIType paat = of.createPendingActivityAPIType();

        List<PendingActivityType> pats = paat.getPendingActivity();
        PendingActivitiesToPendingActivityTypeConverter pa2patc = new PendingActivitiesToPendingActivityTypeConverter();
        pats.add(pa2patc.convert(pa));

        JAXBContext jaxbContext = null;
        try {
            jaxbContext = JAXBContext.newInstance(PendingActivityAPIType.class);
            JAXBElement<PendingActivityAPIType> elem = of
                .createPendingActivityAPI(paat);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            final StringWriter stringWriter = new StringWriter();
            jaxbMarshaller.marshal(elem, stringWriter);

            return stringWriter.toString();
        } catch (JAXBException e) {
            log.error("Error Creating POS PendingActivity XML", e);
        }

        return null;
    }

    @Override
    public String generateTerminalConfigAPI(TenantConfigurationImpl tci,
        Equipment eq) {
        GregorianCalendar c = new GregorianCalendar();
        c.setTime(new Date());

        com.genfare.pos.core.model.terminal.ObjectFactory of = new com.genfare.pos.core.model.terminal.ObjectFactory();
        TerminalConfigAPIType tcat = of.createTerminalConfigAPIType();
        TenantConfigurationImplAndEquipmentToTerminalConfigAPIType tciae2tcat = new TenantConfigurationImplAndEquipmentToTerminalConfigAPIType();
        tcat = tciae2tcat.convert(tci, eq);

        JAXBContext jaxbContext = null;
        try {
            jaxbContext = JAXBContext.newInstance(TerminalConfigAPIType.class);
            JAXBElement<TerminalConfigAPIType> elem = of
                .createTerminalConfigAPI(tcat);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            final StringWriter stringWriter = new StringWriter();
            jaxbMarshaller.marshal(elem, stringWriter);

            return stringWriter.toString();
        } catch (JAXBException e) {
            log.error("Error Creating POS PendingActivity XML", e);
        }

        return null;
    }
}
