package com.genfare.cloud.model.converters;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class DateConverter {
    public static final TimeZone timeZone = TimeZone.getTimeZone("GMT");
    public static final String DatePattern = "yyyy-MM-dd HH:mm:ss z";
    private final DateFormat df = new SimpleDateFormat(DatePattern);

    public DateConverter() {
        df.setTimeZone(timeZone);
    }

    public static void main(String[] argv) throws Exception {
        DateConverter c = new DateConverter();

        Date date0 = new Date();
        System.out.println("date0=" + date0);

        String str01 = c.format(date0);
        System.out.println("str01=" + str01);

        Date date1 = c.parse(str01);
        System.out.println("date1=" + date1);
    }

    public String format(Date date) {
        return df.format(date);
    }

    public Date parse(String source) throws ParseException {
        return df.parse(source);
    }
}
