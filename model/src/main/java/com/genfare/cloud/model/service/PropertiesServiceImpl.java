package com.genfare.cloud.model.service;

import java.util.List;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.genfare.cloud.model.entity.Languages;
import com.genfare.cloud.model.entity.Properties;
import com.genfare.cloud.model.entity.repository.LanguagesRepository;
import com.genfare.cloud.model.entity.repository.PropertiesRepository;

@Service
public class PropertiesServiceImpl
    implements PropertiesService {

    @Autowired
    private PropertiesRepository propertiesRepository;

    @Autowired
    LanguagesRepository languagesRepository;

    @Override
    public List<Properties> getProperties(String locale)
        throws EntityNotFoundException {
        Languages language = languagesRepository.findLanguage(locale);
        List<Properties> list = propertiesRepository.findPropertiesByLanguage(language);
        return list;
    }

    @Override
    public String getProperty(String locale, String key)
        throws EntityNotFoundException {
        Languages language = languagesRepository.findLanguage(locale);
        return propertiesRepository.findPropertyByLanguageAndKey(language, key);
    }
}
