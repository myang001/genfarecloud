package com.genfare.cloud.model.entity.repository;

import org.springframework.data.repository.CrudRepository;

import com.genfare.cloud.model.entity.Organizationcards;

public interface OrganizationcardsRepository
    extends CrudRepository<Organizationcards, Integer> {

}
