package com.genfare.cloud.model.converters.pos.fare;

import org.springframework.core.convert.converter.Converter;

import com.genfare.cloud.model.entity.Wallettypes;
import com.genfare.pos.core.model.fare.WalletTypeType;

public class WallettypesToWalletTypeTypeConverter implements Converter<Wallettypes, WalletTypeType> {

    @Override
    public WalletTypeType convert(Wallettypes wt) {
        WalletTypeType wtt = new WalletTypeType();
        wtt.setWalletTypeId(wt.getWalletTypeId());
        wtt.setDescription(wt.getDescription());

        return wtt;
    }

}
