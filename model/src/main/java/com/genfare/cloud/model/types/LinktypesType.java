package com.genfare.cloud.model.types;

import java.util.EnumSet;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.genfare.cloud.model.entity.Linktypes;
import com.genfare.cloud.model.entity.repository.LinktypesRepository;

public enum LinktypesType {
    HOME_PHONE(1),
    WORK_PHONE(2),
    MOBILE_PHONE(3),
    WORK_EMAIL(4),
    PERSONAL_EMAIL(5),
    DEFAULT_EMAIL(6);

    private final Integer index;

    private Linktypes type;

    private LinktypesType(Integer index) {
        this.index = index;
    }

    public Integer getIndex() {
        return index;
    }

    public Linktypes getType() {
        return type;
    }

    public static LinktypesType findType(String description) {
        for (LinktypesType type : EnumSet.allOf(LinktypesType.class)) {
            if (type.type.getDescription().equals(description)) {
                return type;
            }
        }
        return null;
    }

    @Component
    public static class Injector {
        @Autowired
        private LinktypesRepository repo;

        @PostConstruct
        public void setup() {
            for (LinktypesType type : EnumSet.allOf(LinktypesType.class)) {
                type.type = repo.findOne(type.index);
            }
        }
    }
}
