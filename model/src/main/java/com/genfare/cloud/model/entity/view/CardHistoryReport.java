package com.genfare.cloud.model.entity.view;

import java.math.BigDecimal;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Immutable;

@Entity
@Immutable
@Table(name = "CardHistoryReport")
public class CardHistoryReport {

    private CardHistoryReportId id;
    private String cardNumber;
    private BigDecimal valuePlusMinus;
    private String paymentType;
    private String product;
    private BigDecimal amountRemaining;
    private String equipmentDescription;
    private String routeNumber;
    private Integer personId;

    public CardHistoryReport() {
    }

    public CardHistoryReport(CardHistoryReportId id, String cardNumber,
        BigDecimal valuePlusMinus, String paymentType, String product,
        BigDecimal amountRemaining, String equipmentDescription,
        String routeNumber, Integer personId) {
        this.id = id;
        this.cardNumber = cardNumber;
        this.valuePlusMinus = valuePlusMinus;
        this.paymentType = paymentType;
        this.product = product;
        this.amountRemaining = amountRemaining;
        this.equipmentDescription = equipmentDescription;
        this.routeNumber = routeNumber;
        this.personId = personId;
    }

    @EmbeddedId
    @AttributeOverrides({ @AttributeOverride(name = "dateTime", column = @Column(name = "DateTime", nullable = false)),
        @AttributeOverride(name = "event", column = @Column(name = "Event", nullable = false)) })
    public CardHistoryReportId getId() {
        return id;
    }

    public void setId(CardHistoryReportId id) {
        this.id = id;
    }

    @Column(name = "CardNumber")
    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    @Column(name = "ValuePlusMinus")
    public BigDecimal getValuePlusMinus() {
        return valuePlusMinus;
    }

    public void setValuePlusMinus(BigDecimal valuePlusMinus) {
        this.valuePlusMinus = valuePlusMinus;
    }

    @Column(name = "PaymentType")
    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    @Column(name = "Product")
    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    @Column(name = "AmountRemaining")
    public BigDecimal getAmountRemaining() {
        return amountRemaining;
    }

    public void setAmountRemaining(BigDecimal amountRemaining) {
        this.amountRemaining = amountRemaining;
    }

    @Column(name = "EquipmentDescription")
    public String getEquipmentDescription() {
        return equipmentDescription;
    }

    public void setEquipmentDescription(String equipmentDescription) {
        this.equipmentDescription = equipmentDescription;
    }

    @Column(name = "Route")
    public String getRouteNumber() {
        return routeNumber;
    }

    public void setRouteNumber(String routeNumber) {
        this.routeNumber = routeNumber;
    }

    @Column(name = "PersonId")
    public Integer getPersonId() {
        return personId;
    }

    public void setPersonId(Integer personId) {
        this.personId = personId;
    }

}
