package com.genfare.cloud.model.util;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.NotImplementedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.genfare.cloud.model.contact.Address;
import com.genfare.cloud.model.contact.Email;
import com.genfare.cloud.model.entity.Orderitems;
import com.genfare.cloud.model.entity.Orders;
import com.genfare.cloud.model.entity.Peoplecards;
import com.genfare.cloud.model.entity.PeoplecardsId;
import com.genfare.cloud.model.entity.repository.PaymenttypesRepository;
import com.genfare.cloud.model.entity.repository.PeopleaddressesRepository;
import com.genfare.cloud.model.entity.repository.PeoplecardsRepository;
import com.genfare.cloud.model.security.User;
import com.genfare.cloud.model.tenant.TenantConfiguration;

@Component(value = "FIS")
public class CreditServiceUtilFIS
    implements CreditServiceUtils {

    private static final String SERVICE_NAME = "FIS";

    private static final String FORWARD_TO_PARAM = "UserPart4";

    private static final String USER_DATA = "UserPart4";

    @Autowired
    private TenantConfiguration tenantConfig;

    @Autowired
    private PeoplecardsRepository peoplecardsRepo;

    @Autowired
    private PaymenttypesRepository paymentTypesRepo;

    @Autowired
    private PeopleaddressesRepository peopleaddressesRepository;

    String creditServicel2gbillingname = "BillingName";

    String creditServicel2gbillingaddress = "BillingAddress";

    String creditServicel2gbillingcity = "BillingCity";

    String creditServicel2gbillingcountry = "BillingCountry";

    String creditServicel2gbillingstate = "BillingState";

    String creditServicel2gbillingzip = "BillingZip";

    String creditServicel2gbillingemail = "BillingEmail";

    String creditServicel2gbillingphone = "BillingPhone";

    String creditServicel2gmaskedcreditcardnumber = "MaskedPaymentAccountNumber";

    String creditServicel2gtransactionid = "TransactionId";

    String creditServicel2gtransactiondatestamp = "TransactionDateStamp";

    String creditServicel2gauthorizationnumber = "AuthorizationNumber";

    String creditServicel2gcreditcardtypecode = "PaymentMethodCode";

    @Override
    public String getServiceName() {
        return SERVICE_NAME;
    }

    private String getLineItems(Orders order) {
        // Line items are a requirement for FIS. The amount billed is calculated by them based on the line item data
        String packed_line_items = new String();
        int item_count = 1;
        packed_line_items = "[";
        for (Orderitems oi : order.getOrderitemses()) {
            if ((oi.getParentItemNumber() == null || (oi.getParentItemNumber() == oi.getId().getOrderItemNumber()))
                && oi.getQuantity() != 0) {
                if (item_count > 1)
                    packed_line_items += ",";
                packed_line_items +=
                    "" + item_count++ + "*[" + oi.getWallettypes().getDescription() + "]*[" + oi.getUnitPrice() + "]~["
                        + oi.getQuantity() + "]";
                for (Orderitems child : order.getOrderitemses()) {
                    if (child.getParentItemNumber() == oi.getId().getOrderItemNumber()
                        && child.getId().getOrderItemNumber() != oi.getId().getOrderItemNumber()
                        && child.getQuantity() != 0) {
                        if (item_count > 1)
                            packed_line_items += ",";
                        packed_line_items +=
                            "" + item_count++ + "*[" + child.getTickets().getDescription() + "]*["
                                + child.getUnitPrice() + "]~[" + child.getQuantity() * oi.getQuantity() + "]";

                    }
                }

            }
        }
        packed_line_items += "]";

        return packed_line_items;
    }

    @Override
    public boolean validatePost(HttpServletRequest request, String RelayResponseKey) {
        // If FIS sends security validation, check it here...
        // Currently FIS does not send any code
        return true;
    }

    @Override
    public Map<String, Object> getFormData(Orders order, User user, String returnContext, String userData) {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("PaymentAmount", order.getAmountOrder().toString());
        map.put("PaymentMethod", "100");
        map.put("L2GMerchantCode", tenantConfig.getCreditServiceMerchantCode());
        map.put("Password", tenantConfig.getCreditServicePassword());
        map.put("SettleMerchantCode", tenantConfig.getCreditServiceSettleMerchantCode());
        map.put("ReturnURL", tenantConfig.getCreditServiceReturnURL()); // Perhaps provide an input to change this?
        map.put("CancelURL", tenantConfig.getCreditServiceCancelURL()); // Perhaps provide an input to change this?
        map.put("UserPart2", order.getOrderId().toString());
        map.put("UserPart3", "Test Post Order");
        map.put(FORWARD_TO_PARAM, returnContext);

        Address address = null;
        if (user != null) {
            address = user.getAddress();
            map.put("BillingName", String.format("%s %s", user.getFirstName(), user.getLastName()));
            Email pe = user.getEmail();
            if (null != pe) {
                map.put("BillingEmail", pe.getAddress());
            }
        } else {
            map.put("BillingName", "");
            map.put("BillingEmail", "");
        }

        if (address != null) {
            map.put("BillingCountry", address.getCountry());
            map.put("BillingAddress", address.getLine1());
            map.put("BillingCity", address.getCity());
            map.put("BillingProvidence", "");
            map.put("BillingState", address.getState());
            map.put("BillingPostalCode", address.getPostalCode());
        } else {
            map.put("BillingCountry", "");
            map.put("BillingAddress", "");
            map.put("BillingCity", "");
            map.put("BillingProvidence", "");
            map.put("BillingState", "");
            map.put("BillingPostalCode", "");
        }

        Map<String, Object> lineItems = new HashMap<String, Object>();
        lineItems.put("isList", false);
        lineItems.put("items", getLineItems(order));
        lineItems.put("name", "LineItems");

        map.put("LineItems", lineItems);

        return map;
    }

    @Override
    public OrderPayment getOrderPayment(HttpServletRequest request) {
        OrderPayment order = new OrderPayment();

        String orderIdStr = request.getParameter("UserPart2");
        order.setOrderId(Integer.parseInt(orderIdStr));

        String totalAmountStr = request.getParameter("TotalAmount");
        // TODO
        // order.setAmount(Money.of(CurrencyUnit.USD, Double.parseDouble(totalAmountStr)).getAmountMinorInt());

        order.setAuthorizationNumber(request.getParameter(creditServicel2gauthorizationnumber));

        order.setMaskedCreditCardNumber(request.getParameter(creditServicel2gmaskedcreditcardnumber));

        order.setPaymentType(request.getParameter(creditServicel2gcreditcardtypecode));

        String transactionIdStr = request.getParameter(creditServicel2gtransactionid);

        if (null != transactionIdStr && !transactionIdStr.isEmpty())
            order.setTransactionId(Long.parseLong(transactionIdStr));

        // FIS only returns successful transactions, just use this code to put in the rc field in the
        // order_cc table
        // order.setRc(GFIConstants.L2G_RESPONSE_CODE_SUCCESS);
        // order.setRcText("Approved");
        order.setStatus(1);

        return order;

    }

    @Override
    public Contact getContact(HttpServletRequest request) {
        Contact contact = new Contact();
        String[] values = request.getParameter(creditServicel2gbillingname).split(" ", 2);

        if (null != values && null != values[0]) {
            contact.setFirstName(values[0]);
        }
        if (values.length > 1) {
            contact.setLastName(values[values.length - 1]);
        }
        contact.setAddress1(request.getParameter(creditServicel2gbillingaddress));
        contact.setAddress2("");
        contact.setCity(request.getParameter(creditServicel2gbillingcity));
        contact.setCountry(request.getParameter(creditServicel2gbillingcountry));
        contact.setState(request.getParameter(creditServicel2gbillingstate));
        contact.setZip(request.getParameter(creditServicel2gbillingzip));
        contact.setEmail(request.getParameter(creditServicel2gbillingemail));
        // User Registration sets the day phone as phone2 which is the secondary phone
        // TODO deal with phone
        // contact.setSecondaryPhone(PhoneNumberUtil.digitsOnly(request.getParameter(env.getProperty("creditService.l2g.billing.phone"))));

        return contact;
    }

    @Override
    public UserBilling getUserBilling(HttpServletRequest request) {
        UserBilling billing = new UserBilling();

        String transactionIdStr = request.getParameter(creditServicel2gtransactionid);
        if (null != transactionIdStr && !transactionIdStr.isEmpty())
            billing.setToken(Long.valueOf(request.getParameter(creditServicel2gtransactionid)));
        billing.setAuthorizationNumber(request.getParameter(creditServicel2gauthorizationnumber));
        billing.setMaskedCreditCardNumber(request.getParameter(creditServicel2gmaskedcreditcardnumber));
        // TODO
        // billing.setDefaultBilling(user.isGuestUser() ? false : true);
        billing.setPaymentType(request.getParameter(creditServicel2gcreditcardtypecode));
        billing.setServiceName("FIS");
        // TODO
        // billing.setUserId(user.getUser_Id());
        // TODO double check that this contact_id is not the shopping contact Id
        // billing.setContactId(user.getContactId());
        return billing;
    }

    @Override
    public String getForwardToParam() {
        return FORWARD_TO_PARAM;
    }

    @Override
    public Map<String, Object> getAuthOnlyFormData(User user, String returnContext, String userData) {
        throw new NotImplementedException("CreditServiceUtilFIS#getAuthOnlyFormData is currently not implemented");
    }

    @Override
    public String getUserDataField() {
        return USER_DATA;
    }

    @Override
    public boolean isAuthOnly(HttpServletRequest request) {
        return false;// FIS currently does not support auth only
    }

    @Override
    public Peoplecards createPeopleCard(User user, HttpServletRequest request) {
        Peoplecards card = new Peoplecards();
        card.setId(new PeoplecardsId(user.getId(), peoplecardsRepo.getPersonCardCount(user.getId()).byteValue()));
        card.setPaymenttypes(paymentTypesRepo.getPaymenttypeByDescription(getCardType(request))); // TODO, probably
                                                                                                  // needs to be typed
                                                                                                  // in enum

        String cardNum = request.getParameter(creditServicel2gmaskedcreditcardnumber);
        card.setLastFour(cardNum.substring(12));
        card.setToken(request.getParameter(creditServicel2gtransactionid));

        String exp = request.getParameter("Expiry_Date");
        Calendar calendar = Calendar.getInstance();
        calendar.clear();
        if (null != exp) {
            calendar.set(Calendar.MONTH, Integer.parseInt(exp.substring(0, 2)));
            calendar.set(Calendar.YEAR, Integer.parseInt("20" + exp.substring(2)));
            calendar.set(Calendar.DATE, -1); // Sets to last day of month, which is the last valid day the card can be
                                             // used

        } else {
            calendar.set(Calendar.YEAR, Integer.parseInt("2015"));

        }
        card.setDateExpires(calendar.getTime());

        return peoplecardsRepo.save(card);
    }

    @Override
    public String getCardType(HttpServletRequest request) {
        return request.getParameter(creditServicel2gcreditcardtypecode);
    }

}
