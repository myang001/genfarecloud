package com.genfare.cloud.model.converters.pos.person;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;

import com.genfare.cloud.model.entity.Components;
import com.genfare.pos.core.model.person.ComponentType;

public class ComponentsToComponentTypeConverter implements
    Converter<Components, ComponentType> {

    private static final Logger log = LoggerFactory
        .getLogger(ComponentsToComponentTypeConverter.class);

    @Override
    public ComponentType convert(Components c) {
        ComponentType ct = new ComponentType();
        ct.setComponentId(c.getComponentId());
        ModulesToModuleTypeConverter m2mtc = new ModulesToModuleTypeConverter();

        ct.setModule(m2mtc.convert(c.getModules()));
        ct.setName(c.getName());
        return ct;
    }
}
