package com.genfare.cloud.model.security.jwt;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.binary.StringUtils;

import com.genfare.cloud.model.security.jwt.algorithms.HS256Algorithm;
import com.genfare.cloud.model.security.jwt.interfaces.JwtCryptoAlgorithm;

public class JwtToken {
    private JwtHeader header;
    private JwtClaims claims;
    private final String issuer;
    private final String request;
    private final String method;
    private final Long ttl;
    private String sub = null;
    private JwtCryptoAlgorithm algorithm = new HS256Algorithm();

    public JwtToken(String issuer, String request, String method, Long ttl) {
        this.issuer = issuer;
        this.request = request;
        this.method = method;
        this.ttl = ttl;
    }

    public JwtToken algorithm(JwtCryptoAlgorithm algorithm) {
        this.algorithm = algorithm;
        return this;
    }

    public JwtToken sub(String sub) {
        this.sub = sub;
        return this;
    }

    public JwtHeader getHeader() {
        return this.header;
    }

    public JwtClaims getClaims() {
        return this.claims;
    }

    private static String qsh(String request, String method) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        md.update((method + "&" + request + "&").getBytes("UTF-8"));
        return Hex.encodeHexString(md.digest());
    }

    public String token(String secret) {
        long issueAt = System.currentTimeMillis() / 1000L;
        header = new JwtHeader.Builder(algorithm.getType(), "JWT").build();

        try {
            claims = new JwtClaims.Builder(issuer, issueAt, issueAt + ttl, qsh(request, method)).sub(sub).build();
            String data = Base64.encodeBase64URLSafeString(header.toJson().getBytes()) + "." +
                Base64.encodeBase64URLSafeString(claims.toJson().getBytes());
            String signature = algorithm.encrypt(secret, data);
            return data + "." + signature;
        } catch (Exception e) {
            return null;
        }
    }

    public static Boolean verify(String token, String secret) {
        return verify(token, secret, new HS256Algorithm());
    }

    public static Boolean verify(String token, String secret, JwtCryptoAlgorithm alg) {
        try {
            String[] s = token.split("\\.");

            if (s.length != 3) {
                return false;
            }

            String first = s[0] + "." + s[1];
            String second = s[2];
            String signature = alg.encrypt(secret, first);

            JwtClaims claims = JwtClaims.fromJson(StringUtils.newStringUtf8(Base64.decodeBase64(s[1])));
            return (signature.equals(second) && (claims.getExp() > System.currentTimeMillis() / 1000L));
        } catch (Exception e) {
            return false;
        }
    }

    public static Boolean verifyQueryHash(String token, String request, String method) {
        try {
            String[] s = token.split("\\.");

            if (s.length != 3) {
                return false;
            }

            JwtClaims claims = JwtClaims.fromJson(StringUtils.newStringUtf8(Base64.decodeBase64(s[1])));
            String hash = qsh(request, method);

            return claims.getQsh().equals(hash);
        } catch (Exception e) {
            return false;
        }
    }

    public static JwtHeader parseHeader(String json) {
        String[] ts = json.split("\\.");

        if (ts.length != 3) {
            return null;
        }

        return JwtHeader.fromJson(StringUtils.newStringUtf8(Base64.decodeBase64(ts[0])));
    }

    public static JwtClaims parseClaims(String json) {
        String[] ts = json.split("\\.");

        if (ts.length != 3) {
            return null;
        }

        return JwtClaims.fromJson(StringUtils.newStringUtf8(Base64.decodeBase64(ts[1])));
    }
}
