package com.genfare.cloud.model.entity.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.genfare.cloud.model.entity.Addresstypes;

public interface AddresstypesRepository
    extends CrudRepository<Addresstypes, Integer> {

    @Query("from Addresstypes addrtype where addrtype.description = ?")
    public Addresstypes findTypeByDescription(String description);
}
