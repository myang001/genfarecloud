package com.genfare.cloud.model.converters.pos.fare;

import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;

import com.genfare.cloud.model.converters.pos.common.TicketsToTicketTypeConverter;
import com.genfare.cloud.model.entity.Offerings;
import com.genfare.cloud.model.entity.Tickets;
import com.genfare.pos.core.model.fare.OfferingType;

public class OfferingsToOfferingTypeConverter implements
    Converter<Offerings, OfferingType> {

    private static final Logger log = LoggerFactory
        .getLogger(OfferingsToOfferingTypeConverter.class);

    @Override
    public OfferingType convert(Offerings o) {
        GregorianCalendar c = new GregorianCalendar();
        OfferingType ot = new OfferingType();

        ot.setOfferingId(o.getOfferingId());
        ot.setDescription(o.getDescription());
        ot.setIsMerchandise(o.getIsMerchandise());
        ot.setIsTaxable(o.getIsTaxable());

        Offerings p = o.getOfferings();
        if (p != null) {
            ot.setParentOfferingId(p.getOfferingId());
        }

        ot.setQuanitityMax(o.getQuantityMax());
        ot.setStatus(o.getStatus());
        TaxratesToTaxRateTypeConverter tr2trtc = new TaxratesToTaxRateTypeConverter();
        ot.setTaxRate(tr2trtc.convert(o.getTaxrates()));
        WallettypesToWalletTypeTypeConverter wt2wttc = new WallettypesToWalletTypeTypeConverter();
        ot.setWalletType(wt2wttc.convert(o.getWallettypes()));

        TicketsToTicketTypeConverter ttc = new TicketsToTicketTypeConverter();
        Tickets t = o.getTickets();
        if (t != null) {
            ot.setTicket(ttc.convert(t));
        }

        ot.setUnitCost(ot.getUnitCost());
        ot.setUnitPrice(ot.getUnitPrice());

        try {
            c.setTime(o.getDateEffective());
            ot.setDateEffective(DatatypeFactory.newInstance()
                .newXMLGregorianCalendar(c));

            c.setTime(o.getDateExpires());
            ot.setDateExpires(DatatypeFactory.newInstance()
                .newXMLGregorianCalendar(c));
        } catch (DatatypeConfigurationException e) {
            log.error("Unable to convert Offerings to OfferingType", e);
            return null;
        }

        return ot;
    }

}
