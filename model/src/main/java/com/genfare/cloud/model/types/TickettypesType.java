package com.genfare.cloud.model.types;

import java.util.EnumSet;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.genfare.cloud.model.entity.Tickettypes;
import com.genfare.cloud.model.entity.repository.TickettypesRepository;

public enum TickettypesType {
    STORED_FIXED_VALUE(1),
    STORED_RIDE(2),
    FLOATING_PERIOD_PASS(3),
    STEPPED_VALUE(4),
    FAREST_FARE(5),
    FIXED_PERIOD_PASS(6);

    private final Integer index;

    private Tickettypes type;

    private TickettypesType(Integer index) {
        this.index = index;
    }

    public Integer getIndex() {
        return index;
    }

    public Tickettypes getType() {
        return type;
    }

    @Component
    public static class Injector {
        @Autowired
        private TickettypesRepository repo;

        @PostConstruct
        public void setup() {
            for (TickettypesType type : EnumSet.allOf(TickettypesType.class)) {
                type.type = repo.findOne(type.index.byteValue());
            }
        }
    }
}
