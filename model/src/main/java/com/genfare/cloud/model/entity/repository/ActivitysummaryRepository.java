package com.genfare.cloud.model.entity.repository;

import org.springframework.data.repository.CrudRepository;

import com.genfare.cloud.model.entity.Activitysummary;

public interface ActivitysummaryRepository
    extends CrudRepository<Activitysummary, Integer> {

}
