package com.genfare.cloud.model.types;

import java.util.EnumSet;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.genfare.cloud.model.entity.Addresstypes;
import com.genfare.cloud.model.entity.repository.AddresstypesRepository;

public enum AddresstypesType {
    BILLING(1),
    SHIPPING(2),
    HOME(3),
    WORK(4),
    SCHOOL(5);

    private final Integer index;

    private Addresstypes type;

    private AddresstypesType(Integer index) {
        this.index = index;
    }

    public Integer getIndex() {
        return index;
    }

    public Addresstypes getType() {
        return type;
    }

    public static AddresstypesType findType(String description) {
        for (AddresstypesType type : EnumSet.allOf(AddresstypesType.class)) {
            if (type.type.getDescription().equals(description)) {
                return type;
            }
        }
        return null;
    }

    @Component
    public static class Injector {
        @Autowired
        private AddresstypesRepository repo;

        @PostConstruct
        public void setup() {
            for (AddresstypesType type : EnumSet.allOf(AddresstypesType.class)) {
                type.type = repo.findOne(type.index);
            }
        }
    }
}
