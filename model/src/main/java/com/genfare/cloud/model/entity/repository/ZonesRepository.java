package com.genfare.cloud.model.entity.repository;

import org.springframework.data.repository.CrudRepository;

import com.genfare.cloud.model.entity.Zones;

public interface ZonesRepository
    extends CrudRepository<Zones, Integer> {

}
