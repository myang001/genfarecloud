package com.genfare.cloud.model.converters.pos.activity;

import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;

import com.genfare.cloud.model.converters.pos.common.TicketsToTicketTypeConverter;
import com.genfare.cloud.model.entity.Pendingactivity;
import com.genfare.cloud.model.entity.Walletidentifiers;
import com.genfare.cloud.model.entity.Wallets;
import com.genfare.pos.core.model.pending.PendingActivityType;

public class PendingActivitiesToPendingActivityTypeConverter implements
    Converter<Pendingactivity, PendingActivityType> {

    private static final Logger log = LoggerFactory
        .getLogger(PendingActivitiesToPendingActivityTypeConverter.class);

    @Override
    public PendingActivityType convert(Pendingactivity pa) {
        GregorianCalendar c = new GregorianCalendar();
        PendingActivityType pat = new PendingActivityType();
        pat.setPendingId(pa.getPendingId());
        Wallets w = pa.getWallets();
        pat.setWalletId(w.getWalletId());
        String printedId = "";
        for (Walletidentifiers wi : w.getWalletidentifierses()) {
            if (wi.getIdentifiertypes().getIdentifierId() == 2) {
                printedId = wi.getIdentifier();
                break;
            }
        }
        pat.setPrintedId(printedId);
        pat.setSlot(pa.getSlot());
        TicketsToTicketTypeConverter tc = new TicketsToTicketTypeConverter();
        pat.setTicket(tc.convert(pa.getTickets()));
        pat.setValueActivity(pa.getValueActivity().toString());
        pat.setLoadSequence(pa.getLoadSequence());
        ActivityTypesToActivityTypeTypeConverter attattc = new ActivityTypesToActivityTypeTypeConverter();
        pat.setActivityType(attattc.convert(pa.getActivitytypes()));

        try {
            c.setTime(pa.getDateEffective());
            pat.setDateEffective(DatatypeFactory.newInstance()
                .newXMLGregorianCalendar(c));
            c.setTime(pa.getDateExpires());
            pat.setDateExpires(DatatypeFactory.newInstance()
                .newXMLGregorianCalendar(c));
        } catch (DatatypeConfigurationException e) {
            log.error("Error converting date", e);
            return null;
        }

        return pat;
    }
}
