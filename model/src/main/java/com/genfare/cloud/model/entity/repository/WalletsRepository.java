package com.genfare.cloud.model.entity.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.genfare.cloud.model.entity.People;
import com.genfare.cloud.model.entity.Wallets;

public interface WalletsRepository
    extends CrudRepository<Wallets, Integer> {

    @Query("from Wallets w where w.people.personId = ?")
    public List<Wallets> getWalletsForUser(Integer id);

    @Query("select w from Wallets w where w.walletId = ?")
    public Wallets findByID(Long ID);

    @Query("select w from Walletidentifiers wi Inner join wi.wallets w where wi.id.identifierId = 1 and wi.identifier = ?")
    public Wallets findByElectronicId(String electronicId);

    @Query("select w.people from Wallets w where w.walletId = ?")
    public People findPersonForWallet(Long Id);
}
