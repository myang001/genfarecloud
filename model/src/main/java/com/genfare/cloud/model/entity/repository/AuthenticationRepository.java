package com.genfare.cloud.model.entity.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.genfare.cloud.model.entity.Authentication;

public interface AuthenticationRepository
    extends CrudRepository<Authentication, Integer> {

    @Query("from Authentication auth where auth.authenticationtypes.description = 'Password' and auth.id.personId = ?")
    public Authentication findPasswordByPeopleID(Integer id);

    @Query("select auth.response from Authentication auth where auth.authenticationtypes.description = 'Password' and auth.id.personId = ?")
    public String findPasswordHashByPeopleID(Integer id);

    @Query("from Authentication auth where auth.authenticationtypes.description = 'Security Question' and auth.id.personId = ?")
    public List<Authentication> findSecturityQuestionsByPeopleID(Integer id);

    @Query("from Authentication auth where auth.people.personId = :person and auth.authenticationtypes.authenticationTypeId = :type")
    public Authentication findByUserResponseType(@Param("person") Integer personId, @Param("type") Integer authenticationTypeId);

    @Query("select auth from Authentication auth where auth.authenticationtypes.description = 'Email Verification Token' and auth.id.personId = ?")
    public Authentication findEmailVerificaitonTokenByPeopleID(Integer id);

    @Query("from Authentication auth where auth.authenticationtypes.description = 'Email Verification Token' and auth.response = ?")
    public Authentication findEmailVerificaitonToken(String token);

    @Query("from Authentication auth where auth.authenticationtypes.description = 'Password Recovery' and auth.response = ?")
    public Authentication findPasswordRecoveryVerificaitonToken(String token);

    @Modifying
    @Query("delete from Authentication auth where auth.response = ?")
    public void deleteByToken(String token);
}
