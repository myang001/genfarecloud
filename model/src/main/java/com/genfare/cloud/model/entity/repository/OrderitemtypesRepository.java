package com.genfare.cloud.model.entity.repository;

import org.springframework.data.repository.CrudRepository;

import com.genfare.cloud.model.entity.Orderitemtypes;

public interface OrderitemtypesRepository
    extends CrudRepository<Orderitemtypes, Byte> {

}
