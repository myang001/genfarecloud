package com.genfare.cloud.model.contact;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import com.genfare.cloud.model.annotation.PhoneNumber;

public class Phone {

    @NotBlank(message = "{register.phone.invalid}")
    @PhoneNumber(message = "{register.phone.invalid}")
    private String number;

    @Length(max = 50, message = "{register.phone.description.length}")
    private String description = "Home Phone";

    public Phone() {
    }

    public Phone(String description) {
        this.description = description;
    }

    public Phone(String number, String description) {
        this.number = number;
        this.description = description;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
