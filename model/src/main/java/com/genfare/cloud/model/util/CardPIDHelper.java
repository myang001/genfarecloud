package com.genfare.cloud.model.util;

public class CardPIDHelper {

    public static String leftPadID(Long pid, int length, char padChar) {
        String pidstr = Long.toString(pid);
        if (pidstr.length() < length) {
            StringBuilder pidBuilder = new StringBuilder();
            int remaining = length - pidstr.length();
            for (int i = 0; i < remaining; i++) {
                pidBuilder.append(padChar);
            }
            pidBuilder.append(pid);
            pidstr = pidBuilder.toString();
        }
        return pidstr;
    }

}