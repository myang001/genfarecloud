package com.genfare.cloud.model.entity.repository;

import org.springframework.data.repository.CrudRepository;

import com.genfare.cloud.model.entity.Shippinglists;

public interface ShippinglistsRepository
    extends CrudRepository<Shippinglists, Integer> {

}
