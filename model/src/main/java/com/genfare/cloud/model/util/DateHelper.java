package com.genfare.cloud.model.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateHelper {

    public static final String format = "MM/dd/yyyy";
    public static final String tFormat = "HH:mm:ss";
    public static final SimpleDateFormat dateFormat = new SimpleDateFormat(format);
    public static final SimpleDateFormat timeFormat = new SimpleDateFormat(tFormat);

    /**
     * Takes a @java.util.Date and returns midnight of that day (0:00am the next morning)
     * 
     * @param date
     */
    public static Date toNextMorning(Date date) {
        if (date == null) {
            return null;
        }
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DATE, 1);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.HOUR, 0);
        c.set(Calendar.SECOND, 0);
        return c.getTime();
    }

    public static String getDateAsString(Date date) {
        if (date == null) {
            return null;
        }
        return (dateFormat.format(date));
    }

    public static String getTimeAsString(Date date) {
        if (date == null) {
            return null;
        }
        return (timeFormat.format(date));
    }
}
