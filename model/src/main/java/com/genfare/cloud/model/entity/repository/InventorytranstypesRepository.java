package com.genfare.cloud.model.entity.repository;

import org.springframework.data.repository.CrudRepository;

import com.genfare.cloud.model.entity.Inventorytranstypes;

public interface InventorytranstypesRepository
    extends CrudRepository<Inventorytranstypes, Byte> {

}
