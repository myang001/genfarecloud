package com.genfare.cloud.model.entity.repository;

import org.springframework.data.repository.CrudRepository;

import com.genfare.cloud.model.entity.Orderitemactions;

public interface OrderitemactionsRepository
    extends CrudRepository<Orderitemactions, Byte> {

}
