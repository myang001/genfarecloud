package com.genfare.cloud.model.entity.repository;

import org.springframework.data.repository.CrudRepository;

import com.genfare.cloud.model.entity.Fulfillments;
import com.genfare.cloud.model.entity.OrderitemsId;

public interface FulfillmentsRepository
    extends CrudRepository<Fulfillments, OrderitemsId> {

}
