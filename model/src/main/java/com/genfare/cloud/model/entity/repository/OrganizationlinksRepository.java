package com.genfare.cloud.model.entity.repository;

import org.springframework.data.repository.CrudRepository;

import com.genfare.cloud.model.entity.Organizationlinks;

public interface OrganizationlinksRepository
    extends CrudRepository<Organizationlinks, Integer> {

}
