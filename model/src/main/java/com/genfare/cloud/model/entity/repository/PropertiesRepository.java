package com.genfare.cloud.model.entity.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.genfare.cloud.model.entity.Languages;
import com.genfare.cloud.model.entity.Properties;
import com.genfare.cloud.model.entity.PropertiesID;

public interface PropertiesRepository
    extends CrudRepository<Properties, PropertiesID> {

    @Query("from Properties d where d.languages= ?")
    public List<Properties> findPropertiesByLanguage(Languages languages);

    @Query("select d.pageValue from Properties d where d.languages= ?1 and d.pageKey=?2")
    public String findPropertyByLanguageAndKey(Languages languages, String pageKey);
}
