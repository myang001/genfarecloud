package com.genfare.cloud.model.service;

import java.util.Date;

import com.genfare.cloud.model.entity.Peoplecards;
import com.genfare.cloud.model.security.User;

public interface CreditCardManagementService {
    public Peoplecards getLastUsedCardByUser(User user);

    public Date getCardExpiration(Peoplecards card);
}
