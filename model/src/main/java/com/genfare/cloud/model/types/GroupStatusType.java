package com.genfare.cloud.model.types;

import java.util.EnumSet;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.genfare.cloud.model.entity.GroupStatus;
import com.genfare.cloud.model.entity.repository.GroupStatusRepository;

public enum GroupStatusType {
    ACTIVE(1),
    DISABLED(2);

    private final Integer index;

    private GroupStatus status;

    private GroupStatusType(Integer index) {
        this.index = index;
    }

    public Integer getIndex() {
        return index;
    }

    public GroupStatus getStatus() {
        return status;
    }

    public static GroupStatus getStatus(Byte s) {
        for (GroupStatusType type : GroupStatusType.values()) {
            if (s == type.index.byteValue()) {
                return type.status;
            }
        }
        throw new IllegalArgumentException("illegal group status index: " + s);
    }

    @Component
    public static class Injector {
        @Autowired
        private GroupStatusRepository repo;

        @PostConstruct
        public void setup() {
            for (GroupStatusType type : EnumSet.allOf(GroupStatusType.class)) {
                type.status = repo.findOne(type.index.byteValue());
            }
        }
    }
}
