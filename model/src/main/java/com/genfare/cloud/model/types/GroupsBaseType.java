package com.genfare.cloud.model.types;

import java.util.EnumSet;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.genfare.cloud.model.entity.Groups;
import com.genfare.cloud.model.entity.repository.GroupsRepository;

public enum GroupsBaseType {
    RIDER(19);

    private final Integer index;

    private Groups type;

    private GroupsBaseType(Integer index) {
        this.index = index;
    }

    public Integer getIndex() {
        return index;
    }

    public Groups getType() {
        return type;
    }

    @Component
    public static class Injector {
        @Autowired
        private GroupsRepository repo;

        @PostConstruct
        public void setup() {
            for (GroupsBaseType type : EnumSet.allOf(GroupsBaseType.class)) {
                type.type = repo.findOne(type.index);
            }
        }
    }
}
