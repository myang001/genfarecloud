/**
 *
 */
package com.genfare.cloud.model.converters;

import org.springframework.core.convert.converter.Converter;

import com.genfare.cloud.model.entity.Paymenttypes;
import com.genfare.cloud.model.om.PaymentTypeType;

/**
 * @author mpeter
 */
public class PaymentTypeToPaymentTypeType implements
    Converter<Paymenttypes, PaymentTypeType> {

    @Override
    public PaymentTypeType convert(Paymenttypes pt) {
        PaymentTypeType ptt = new PaymentTypeType();
        ptt.setDescription(pt.getDescription());
        ptt.setIsCredit(pt.getIsCredit());
        ptt.setPaymentTypeID(pt.getPaymentTypeId());
        return ptt;
    }

}
