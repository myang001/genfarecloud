package com.genfare.cloud.model.types;

public enum GenderType {
    MALE("m"),
    FEMALE("f"),
    UNSPECIFIED("");

    private final String value;

    private GenderType(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }

    public static GenderType getEnum(String value) {
        for (GenderType v : values()) {
            if (v.toString().equalsIgnoreCase(value)) {
                return v;
            }
        }
        return null;
    }

}
