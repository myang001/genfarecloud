package com.genfare.cloud.model.tenant.impl;

import java.io.File;
import java.io.IOException;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.stereotype.Component;

import com.genfare.cloud.model.tenant.TenantConfiguration;
import com.genfare.cloud.model.tenant.TenantConfigurationRepository;

@Component
public class ResourceTenantConfigurationRepository
    implements TenantConfigurationRepository {

    private static final Logger logger = LoggerFactory.getLogger(ResourceTenantConfigurationRepository.class);

    List<TenantConfiguration> tenants;

    AbstractMap<String, TenantConfiguration> configByDomain;

    AbstractMap<String, TenantConfiguration> configBySchema;

    TenantConfiguration defaultTenant;

    public ResourceTenantConfigurationRepository() {
        configByDomain = new java.util.HashMap<String, TenantConfiguration>();
        configBySchema = new java.util.HashMap<String, TenantConfiguration>();
        PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
        tenants = new ArrayList<TenantConfiguration>();

        try {
            List<Resource> tenantList = null;
            /*
             * if system property aws.Environment exists, use the file in model.jar/tenant/<env>/*.xml otherwise, use what's in
             * model.jar/tenant/*.xml.
             */
            String env = System.getProperty("aws.Environment");
            if (env == null) {
                tenantList = Arrays.asList(resolver.getResources("tenant/*xml"));
                logger.warn("Using DEPRECATED tenantList: " + tenantList);
            } else {
                tenantList = Arrays.asList(resolver.getResources("tenant/" + env + "/" + "*xml"));
                logger.info("Using tenantList: " + tenantList);
            }

            for (Resource tenantResource : tenantList) {
                File tenantFile = tenantResource.getFile();
                JAXBContext context;
                try {
                    context = JAXBContext.newInstance(TenantConfigurationImpl.class.getPackage().getName());
                    @SuppressWarnings("unchecked")
                    JAXBElement<TenantConfigurationImpl> element =
                        (JAXBElement<TenantConfigurationImpl>) context.createUnmarshaller().unmarshal(tenantFile);
                    TenantConfiguration config = element.getValue();

                    tenants.add(config);
                    for (DomainNameType domain : config.getDomainNames().getDomainName()) {
                        configByDomain.put(domain.getValue(), config);
                    }
                    configBySchema.put(config.getSchemaName(), config);
                    if (config.isDefaultTenant()) {
                        defaultTenant = config;
                    }

                    logger.info("Loaded tenant configuration for " + config.getTenantName());
                } catch (JAXBException e) {
                    logger.error("Could not load tenant configuration", e);
                }
            }
        } catch (IOException e) {
            logger.error("Could not load tenant configuration", e);
        }
    }

    @Override
    public TenantConfiguration getByDomainName(String domainName) {
        TenantConfiguration config = configByDomain.get(domainName);
        if (config == null) {
            if (defaultTenant != null) {
                logger.warn("Default tenant loaded because there is no configuration for " + domainName);
                return defaultTenant;
            }
            throw new UnknownTenantException(domainName);
        }
        return config;
    }

    @Override
    public TenantConfiguration getBySchemaName(String schemaName) {
        TenantConfiguration config = configBySchema.get(schemaName);
        if (config == null) {
            if (defaultTenant != null) {
                logger.warn("Default tenant loaded because there is no configuration for " + schemaName);
                return defaultTenant;
            }
            throw new UnknownTenantException(schemaName);
        }
        return config;
    }

    @Override
    public Collection<TenantConfiguration> getAll() {
        return tenants;
    }

    @Override
    public TenantConfiguration getDefaultTenant() {
        return defaultTenant;
    }

}
