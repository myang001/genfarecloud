package com.genfare.cloud.model.types;

import java.util.EnumSet;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.genfare.cloud.model.entity.Purposes;
import com.genfare.cloud.model.entity.repository.PurposesRepository;

public enum PurposesType {
    NORMAL_USE(1),
    DRIVER_ID(2),
    MAINTENANCE_ID(3),
    TEST_PURPOSES(4),
    POS_CLERK_ID(5);

    private final Integer index;

    private Purposes type;

    private PurposesType(Integer index) {
        this.index = index;
    }

    public Integer getIndex() {
        return index;
    }

    public Purposes getType() {
        return type;
    }

    public static PurposesType findType(String description) {
        for (PurposesType type : EnumSet.allOf(PurposesType.class)) {
            if (type.type.getDescription().equals(description)) {
                return type;
            }
        }
        return null;
    }

    @Component
    public static class Injector {
        @Autowired
        private PurposesRepository repo;

        @PostConstruct
        public void setup() {
            for (PurposesType type : EnumSet.allOf(PurposesType.class)) {
                type.type = repo.findOne(type.index.byteValue());
            }
        }
    }
}
