package com.genfare.cloud.model.entity.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.genfare.cloud.model.entity.JobTitle;

public interface JobTitleRepository extends CrudRepository<JobTitle, Integer> {

    @Query("from JobTitle o where o.description = ?")
    public JobTitle findByName(String name);

    @Query("select o.description from JobTitle o where o.description like :description")
    public List<String> findJobTitleLikeName(@Param("description") String description);

}
