package com.genfare.cloud.model.security.jwt.interfaces;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

public interface JwtCryptoAlgorithm {
    String getType();

    String encrypt(String key, String data) throws NoSuchAlgorithmException, InvalidKeyException;
}
