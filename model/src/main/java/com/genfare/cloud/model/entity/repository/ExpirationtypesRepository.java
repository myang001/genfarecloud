package com.genfare.cloud.model.entity.repository;

import org.springframework.data.repository.CrudRepository;

import com.genfare.cloud.model.entity.Expirationtypes;

public interface ExpirationtypesRepository
    extends CrudRepository<Expirationtypes, Byte> {

}
