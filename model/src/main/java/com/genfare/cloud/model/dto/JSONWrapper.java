package com.genfare.cloud.model.dto;

public class JSONWrapper<T> {

    private T value;

    private String status;

    private String errors;

    public JSONWrapper(T value, String status) {
        this.value = value;
        this.status = status;
    }

    public JSONWrapper(T value, String status, String errors) {
        this.value = value;
        this.status = status;
        this.errors = errors;
    }

    public T getValue() {
        return value;
    }

    public String getStatus() {
        return status;
    }

    public String getErrors() {
        return errors;
    }
}
