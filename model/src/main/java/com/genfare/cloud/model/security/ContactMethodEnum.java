package com.genfare.cloud.model.security;

public enum ContactMethodEnum {

    EMAIL(1, "Email"),
    SMS(2, "SMS");

    private int id;

    private String description;

    private ContactMethodEnum(int id, String description) {
        this.setId(id);
        this.setDescription(description);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
