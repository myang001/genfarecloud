package com.genfare.cloud.model.entity.repository;

import org.springframework.data.repository.CrudRepository;

import com.genfare.cloud.model.entity.Activitydetails;

public interface ActivitydetailsRepository
    extends CrudRepository<Activitydetails, Integer> {

}
