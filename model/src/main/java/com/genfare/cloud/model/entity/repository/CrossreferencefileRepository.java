package com.genfare.cloud.model.entity.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.genfare.cloud.model.entity.Crossreferencefile;
import com.genfare.cloud.model.entity.CrossreferencefileId;

public interface CrossreferencefileRepository
    extends CrudRepository<Crossreferencefile, CrossreferencefileId> {

    @Query("from Crossreferencefile c where c.cvv = ?1 and c.id.printedId = ?2")
    public Crossreferencefile findByCvvAndPrintedId(String cvv, String printedId);

    @Query("from Crossreferencefile c where c.id.printedId = ?")
    public Crossreferencefile findByPrintedId(String printedId);

}
