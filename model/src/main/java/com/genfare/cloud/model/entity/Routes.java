package com.genfare.cloud.model.entity;

// Generated Mar 19, 2014 9:47:20 AM by Hibernate Tools 4.0.0

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.genfare.cloud.model.entity.inter.Auditable;
import com.genfare.cloud.model.listener.AuditListener;

/**
 * Routes generated by hbm2java
 */
@Entity
@Table(name = "routes")
@EntityListeners(AuditListener.class)
public class Routes
    implements java.io.Serializable, Auditable {

    private static final long serialVersionUID = 1L;

    private Short routeId;

    private String routeNumber;

    private String description;

    private String touchedBy;

    private Date touchedAt;

    private String createdBy;

    private Date createdAt;

    private Set<Stops> stopses = new HashSet<Stops>(0);

    public Routes() {
    }

    public Routes(String routeNumber, String description, Set<Stops> stopses) {
        this.routeNumber = routeNumber;
        this.description = description;
        this.stopses = stopses;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "RouteID", unique = true, nullable = false)
    public Short getRouteId() {
        return this.routeId;
    }

    public void setRouteId(Short routeId) {
        this.routeId = routeId;
    }

    @Column(name = "RouteNumber", length = 10)
    public String getRouteNumber() {
        return this.routeNumber;
    }

    public void setRouteNumber(String routeNumber) {
        this.routeNumber = routeNumber;
    }

    @Column(name = "Description", length = 50)
    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name = "TouchedBy", length = 50)
    public String getTouchedBy() {
        return this.touchedBy;
    }

    @Override
    public void setTouchedBy(String touchedBy) {
        this.touchedBy = touchedBy;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "TouchedAt", nullable = false, length = 19)
    public Date getTouchedAt() {
        return this.touchedAt;
    }

    @Override
    public void setTouchedAt(Date touchedAt) {
        this.touchedAt = touchedAt;
    }

    @Override
    @Column(name = "CreatedBy", nullable = false, length = 50)
    public String getCreatedBy() {
        return this.createdBy;
    }

    @Override
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Override
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CreatedAt", nullable = false, length = 19)
    public Date getCreatedAt() {
        return this.createdAt;
    }

    @Override
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "routes")
    public Set<Stops> getStopses() {
        return this.stopses;
    }

    public void setStopses(Set<Stops> stopses) {
        this.stopses = stopses;
    }

}
