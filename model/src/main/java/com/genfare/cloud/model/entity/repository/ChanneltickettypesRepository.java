package com.genfare.cloud.model.entity.repository;

import org.springframework.data.repository.CrudRepository;

import com.genfare.cloud.model.entity.Channeltickettypes;
import com.genfare.cloud.model.entity.ChanneltickettypesId;

public interface ChanneltickettypesRepository
    extends CrudRepository<Channeltickettypes, ChanneltickettypesId> {

}
