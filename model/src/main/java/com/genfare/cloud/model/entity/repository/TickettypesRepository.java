package com.genfare.cloud.model.entity.repository;

import org.springframework.data.repository.CrudRepository;

import com.genfare.cloud.model.entity.Tickettypes;

public interface TickettypesRepository
    extends CrudRepository<Tickettypes, Byte> {

}
