package com.genfare.cloud.model.service;

import java.util.List;

import com.genfare.cloud.model.entity.Orders;
import com.genfare.cloud.model.entity.Peoplecards;
import com.genfare.cloud.model.entity.Subscriptions;
import com.genfare.cloud.model.entity.Walletcontents;
import com.genfare.cloud.model.entity.Wallets;
import com.genfare.cloud.model.exception.InvalidOrderException;
import com.genfare.cloud.model.security.User;

public interface SubscriptionService {
    public List<String> getWhenOptions(Walletcontents product);

    Orders createSubscriptionOrder(User user, Peoplecards card, Walletcontents walletContent, String expires,
        String price, String value, String threshold) throws InvalidOrderException;

    Orders modifySubscriptionOrder(User user, Peoplecards card, Walletcontents walletContent, String expires,
        String price, String value, String threshold) throws InvalidOrderException;

    Orders cancelSubscriptionOrder(User user, Peoplecards card, Walletcontents walletContent, String expires,
        String price, String value, String threshold) throws InvalidOrderException;

    List<Orders> cancelSubscriptionForWalletOrder(User user, Wallets wallet) throws InvalidOrderException;

    public String getStatus(Subscriptions subscription);
}