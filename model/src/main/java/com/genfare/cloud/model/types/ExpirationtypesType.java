package com.genfare.cloud.model.types;

import java.util.EnumSet;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.genfare.cloud.model.entity.Expirationtypes;
import com.genfare.cloud.model.entity.repository.ExpirationtypesRepository;

public enum ExpirationtypesType {
    MIDNIGHT(1),
    END_OF_DAY(2),
    HOURS_24(3),
    LESS_THAN_HOURS_24(4);

    private final Integer index;

    private Expirationtypes type;

    private ExpirationtypesType(Integer index) {
        this.index = index;
    }

    public Integer getIndex() {
        return index;
    }

    public Expirationtypes getType() {
        return type;
    }

    @Component
    public static class Injector {
        @Autowired
        private ExpirationtypesRepository repo;

        @PostConstruct
        public void setup() {
            for (ExpirationtypesType type : EnumSet.allOf(ExpirationtypesType.class)) {
                type.type = repo.findOne(type.index.byteValue());
            }
        }
    }
}
