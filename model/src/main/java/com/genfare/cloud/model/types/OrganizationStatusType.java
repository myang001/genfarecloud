package com.genfare.cloud.model.types;

import java.util.EnumSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.annotation.PostConstruct;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.genfare.cloud.model.entity.OrganizationStatus;
import com.genfare.cloud.model.entity.repository.OrganizationstatusRepository;

public enum OrganizationStatusType {
    ACTIVE(1),
    DISABLED(2);

    private final Integer index;

    private OrganizationStatus status;

    private static Map<String, OrganizationStatus> statusStrings = new LinkedHashMap<String, OrganizationStatus>();

    private OrganizationStatusType(Integer index) {
        this.index = index;
    }

    public Integer getIndex() {
        return index;
    }

    public OrganizationStatus getStatus() {
        return status;
    }

    public static OrganizationStatus findStatus(String status) {
        return statusStrings.get(status);
    }

    public static Iterable<String> getStatuses() {
        return statusStrings.keySet();
    }

    @Component
    public static class Injector {
        @Autowired
        private OrganizationstatusRepository repo;

        @PostConstruct
        public void setup() {
            EnumSet<OrganizationStatusType> set = EnumSet.allOf(OrganizationStatusType.class);
            Iterator<OrganizationStatusType> it = set.iterator();
            while (it.hasNext()) {
                OrganizationStatusType status = it.next();
                status.status = repo.findOne(status.index.byteValue());
                statusStrings.put(StringUtils.capitalize(status.toString().toLowerCase()), status.status);
            }
        }
    }
}
