package com.genfare.cloud.model.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpSession;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.genfare.cloud.model.entity.Farecodes;
import com.genfare.cloud.model.entity.Offerings;
import com.genfare.cloud.model.entity.Orderitems;
import com.genfare.cloud.model.entity.OrderitemsId;
import com.genfare.cloud.model.entity.Orders;
import com.genfare.cloud.model.entity.Orderstatuses;
import com.genfare.cloud.model.entity.Ordertypes;
import com.genfare.cloud.model.entity.Payments;
import com.genfare.cloud.model.entity.Shippers;
import com.genfare.cloud.model.entity.Wallets;
import com.genfare.cloud.model.entity.repository.FulfillmentsRepository;
import com.genfare.cloud.model.entity.repository.OfferingsRepository;
import com.genfare.cloud.model.entity.repository.OrderitemsRepository;
import com.genfare.cloud.model.entity.repository.OrdersRepository;
import com.genfare.cloud.model.entity.repository.OrderstatusesRepository;
import com.genfare.cloud.model.entity.repository.PaymentsRepository;
import com.genfare.cloud.model.entity.repository.PeopleRepository;
import com.genfare.cloud.model.entity.repository.ShippersRepository;
import com.genfare.cloud.model.entity.repository.WalletstatusRepository;
import com.genfare.cloud.model.exception.InvalidOrderException;
import com.genfare.cloud.model.security.User;
import com.genfare.cloud.model.types.ActivitytypesType;
import com.genfare.cloud.model.types.ChannelsType;
import com.genfare.cloud.model.types.FarecodesType;
import com.genfare.cloud.model.types.FulfillmentSearchType;
import com.genfare.cloud.model.types.OrderItemStatusType;
import com.genfare.cloud.model.types.OrderPaymentStatusType;
import com.genfare.cloud.model.types.OrderStatusType;
import com.genfare.cloud.model.types.OrderitemactionsType;
import com.genfare.cloud.model.types.OrderitemtypesType;
import com.genfare.cloud.model.types.OrdertypesType;
import com.genfare.cloud.model.util.Contact;
import com.genfare.cloud.model.util.DateHelper;

/**
 * Service for managing orders
 * 
 * @see OrderManagementService
 * @author jyore
 * @version 1.0
 */
@Transactional
@Service
public class OrderManagementServiceImpl implements OrderManagementService {

    private static final Logger log = LoggerFactory.getLogger(OrderManagementServiceImpl.class);

    private static final Byte COMPLETED = OrderStatusType.COMPLETE.getIndex().byteValue();

    private static final Byte CANCELED = OrderStatusType.CANCELLED.getIndex().byteValue();

    private static final Byte IN_PROGRESS = OrderStatusType.IN_PROGRESS.getIndex().byteValue();

    private static final Byte FULFILLMENT = OrderStatusType.FULFILLMENT.getIndex().byteValue();

    private static final Byte ALL = FulfillmentSearchType.ALL.getIndex().byteValue();

    private static final Byte NEW_ACTION = OrderitemactionsType.NEW.getIndex().byteValue();

    private static final Byte WALLET_TYPE = OrderitemtypesType.WALLETTYPE.getIndex().byteValue();

    private static final Byte ADD_VALUE = ActivitytypesType.ADDVALUE.getIndex().byteValue();

    private static final Byte ADD_PRODUCT = ActivitytypesType.ADDPRODUCT.getIndex().byteValue();

    @Autowired
    private OrdersRepository ordersRepository;

    @Autowired
    private OrderitemsRepository orderitemsRepository;

    @Autowired
    private FulfillmentsRepository fulfillmentsRepository;

    @Autowired
    private PeopleRepository peoplerepository;

    @Autowired
    private OrderstatusesRepository orderstatusesRepository;

    @Autowired
    private PaymentsRepository paymentsrepository;

    @Autowired
    private ShippersRepository shippersRepository;

    @Autowired
    private WalletManagementService walletManagementService;

    @Autowired
    WalletstatusRepository walletstatusRepository;

    @Autowired
    private OfferingsRepository offersRepository;

    @Autowired
    private BusinessRulesService rulesService;

    @Override
    public Iterable<Shippers> getShippers() throws EntityNotFoundException {
        Iterable<Shippers> shippers = shippersRepository.findAll();
        if (null == shippers) {
            throw new EntityNotFoundException("Shippers not found ");
        }
        return shippers;
    }

    @Override
    public Orders getOrder(Integer orderId) throws EntityNotFoundException {
        Orders order = ordersRepository.findOne(orderId);
        if (null == order) {
            throw new EntityNotFoundException("Order not found for order number: " + orderId);
        }
        return order;
    }

    @Override
    public Orders getCurrentOrder(User user, HttpSession session, Ordertypes type) {

        if (user != null) {
            return ordersRepository.findCurrentOrderByUserForType(user.getId(), type.getOrderTypeId());
        }

        if (session != null) {
            Integer orderId = (Integer) session.getAttribute("orderId");
            if (orderId != null) {
                return getOrder(orderId);
            }
        }

        return null;
    }

    @Override
    public Orders getCurrentOrCreate(User user, HttpSession session, Ordertypes type) {
        Orders o = getCurrentOrder(user, session, type);
        if (o == null) {
            Orders neworder = new Orders(type, OrderStatusType.UNSUBMITTED.getStatus(), OrderPaymentStatusType.UNPAID.getStatus());
            neworder.setDateOrdered(new Date());
            o = saveOrder(neworder, user, session);
        }
        return o;
    }

    @Override
    public Orders saveOrder(Orders order, Object... meta) {

        // If order doesn't have an ID, then save now
        if (order.getOrderId() == null) {
            order = ordersRepository.save(order);
        }

        for (Object o : meta) {
            if (o != null) {
                if (o instanceof User) {
                    User u = (User) o;
                    order.setPeople(peoplerepository.findOne(u.getId()));
                } else if (o instanceof HttpSession) {
                    HttpSession session = (HttpSession) o;
                    session.setAttribute("orderId", order.getOrderId());
                } else {
                    log.debug("Ignoring argument: " + o.getClass());
                }
            }
        }

        order.setAmountOrder(getTotalAmount(order));
        return ordersRepository.save(order);
    }

    @Override
    public Orders addOrderItem(Orders order, Orderitems orderitem, Object... meta) throws InvalidOrderException {
        List<Orderitems> items = order.getOrderitemses();

        if (orderitem.getTickets() != null) {
            for (Orderitems item : items) {
                if (orderitem.getTickets() != null && item.getTickets() != null) {
                    if (orderitem.getParentItemNumber().equals(item.getParentItemNumber())) {
                        // Parents are the same
                        if (orderitem.getTickets() != null && item.getTickets() != null) {
                            if (orderitem.getTickets().getDescription().equals(item.getTickets().getDescription())) {
                                if (orderitem.getTickets().getDescription().equals("Variable Stored Value")) {
                                    BigDecimal newAmount = item.getAmount().add(orderitem.getAmount());
                                    item.setAmount(newAmount);
                                    BigDecimal newPrice = item.getUnitPrice().add(orderitem.getUnitPrice());
                                    item.setUnitPrice(newPrice);
                                    return saveOrder(order, meta);
                                } else {
                                    item.setQuantity((short) (item.getQuantity() + orderitem.getQuantity()));
                                    if (!rulesService.isValid(order)) {
                                        item.setQuantity((short) (item.getQuantity() - orderitem.getQuantity()));
                                        throw new InvalidOrderException("Invalid order");
                                    }
                                    return saveOrder(order, meta);
                                }
                            }
                        }
                    }
                } else {
                    // Some other offer
                    if ((item.getTickets() != null) && orderitem.getTickets().getDescription().equals(item.getTickets().getDescription())) {
                        item.setQuantity((short) (item.getQuantity() + orderitem.getQuantity()));
                        if (!rulesService.isValid(order)) {
                            item.setQuantity((short) (item.getQuantity() - orderitem.getQuantity()));
                            throw new InvalidOrderException("Invalid order");
                        }
                        return saveOrder(order, meta);
                    }
                }
            }
        }
        log.info("I am saving this order");
        orderitem = orderitemsRepository.save(orderitem);
        items.add(orderitem);
        return saveOrder(order, meta);
    }

    @Override
    public Orders addOrderItems(Orders order, Iterable<Orderitems> items, Object... meta) throws InvalidOrderException {
        for (Orderitems oi : items) {
            order = addOrderItem(order, oi, meta);
        }
        return order;
    }

    @Override
    public Orderitems retrieveOrderItem(OrderitemsId id) {
        return orderitemsRepository.findOne(id);
    }

    @Override
    public Orders removeOrderItem(Orders order, Orderitems orderitem, Object... meta) {
        List<Orderitems> items = sortOrderItems(order);
        List<Orderitems> toDelete = new ArrayList<Orderitems>();
        List<Orderitems> toKeep = new ArrayList<Orderitems>();
        List<Orderitems> parentOnlyItems = new ArrayList<Orderitems>();

        Orderitems parent = null;

        for (Orderitems item : items) {
            if (orderItemParentOnly(item)) {
                parentOnlyItems.add(item);
                continue;
            } else if (orderitem.getParentItemNumber().equals(orderitem.getId().getOrderItemNumber())) {
                // Parent Item
                if (item.getId().getOrderItemNumber().equals(orderitem.getId().getOrderItemNumber())) {
                    // The actual parent, store for later
                    parent = item;
                } else if (orderitem.getId().getOrderItemNumber().equals(item.getParentItemNumber())) {
                    // Child of the parent, delete
                    toDelete.add(item);
                } else {
                    toKeep.add(item);
                }
            } else {
                if (orderitem.getId().getOrderItemNumber().equals(item.getId().getOrderItemNumber())) {
                    // Item Match, delete
                    toDelete.add(item);
                } else {
                    // Not to be deleted, keep it
                    toKeep.add(item);
                }
            }
        }

        Orderitems emptyParentItem = null;
        for (Orderitems parentItem : parentOnlyItems) {
            boolean keep = false;
            for (Orderitems item : toKeep) {
                if (item.getParentItemNumber() == parentItem.getId().getOrderItemNumber() && item.getId().getOrderItemNumber() != parentItem.getId().getOrderItemNumber()) {
                    keep = true;
                }
            }
            if (keep) {
                toKeep.add(parentItem);
            } else {
                emptyParentItem = parentItem;
            }
        }

        orderitemsRepository.delete(toDelete);
        if (parent != null) {
            orderitemsRepository.delete(parent);
        }
        if (emptyParentItem != null) {
            orderitemsRepository.delete(emptyParentItem);
        }
        if (!orderRequiresShipping(order)) {
            order.setShippers(null);
        }
        order.setOrderitemses(toKeep);
        return saveOrder(order, meta);
    }

    @Override
    public Orders removeOrderItems(Orders order, Iterable<Orderitems> items, Object... meta) {
        for (Orderitems oi : items) {
            order = removeOrderItem(order, oi, meta);
        }
        if (!orderRequiresShipping(order)) {
            order.setShippers(null);
        }
        return order;
    }

    @Override
    public Orders removeAllOrderItems(Orders order, Object... meta) {
        List<Orderitems> items = sortOrderItems(order);
        List<Orderitems> toDelete = new ArrayList<Orderitems>();

        for (Orderitems item : items) {
            if (item.getId().getOrderItemNumber().equals(item.getParentItemNumber()) || item.getParentItemNumber() == null) {
                toDelete.add(item);
            }
        }
        order.setShippers(null);
        orderitemsRepository.delete(toDelete);
        order.setOrderitemses(new ArrayList<Orderitems>());
        return saveOrder(order, meta);
    }

    @Override
    public List<Orderitems> sortOrderItems(Orders order) {

        List<Orderitems> list = new ArrayList<Orderitems>();
        list.addAll(order.getOrderitemses());

        Comparator<Orderitems> comparator = new Comparator<Orderitems>() {

            @Override
            public int compare(Orderitems o1, Orderitems o2) {

                Byte x1 = o1.getId().getOrderItemNumber();
                Byte x2 = o2.getId().getOrderItemNumber();
                int sComp = x1.compareTo(x2);

                if (sComp != 0) {
                    return sComp;
                } else {
                    x1 = o1.getParentItemNumber();
                    x2 = o2.getParentItemNumber();
                    return x1.compareTo(x2);
                }
            }
        };

        Collections.sort(list, comparator);
        return list;
    }

    // TODO: Add in discount and tax to calculation
    private BigDecimal getTotalAmount(Orders order) {
        BigDecimal total = new BigDecimal(0.0);
        Iterator<Orderitems> it = order.getOrderitemses().iterator();
        while (it.hasNext()) {
            Orderitems oi = it.next();
            if (!orderItemParentOnly(oi)) {
                total = total.add(oi.getAmount());
            }
        }

        return total;
    }

    private Orders updateOrderStatus(Orders order) {
        return ordersRepository.save(deriveOrderStatus(order));
    }

    @Override
    public Orders updateOrderItemStatus(Orderitems item, OrderItemStatusType status) {
        if (status.getIndex().byteValue() == COMPLETED || status.getIndex().byteValue() == CANCELED) {
            if (item.getFulfillments() != null) {
                item.getFulfillments().setOrderstatuses(status.getStatus());
                fulfillmentsRepository.save(item.getFulfillments());
            }
        }
        item.setOrderstatuses(status.getStatus());
        item = orderitemsRepository.save(item);
        return updateOrderStatus(item.getOrders());
    }

    @Override
    public Orders updateOrderStatuses(Orders order, OrderPaymentStatusType paymentStatus, OrderItemStatusType itemStatus) {
        Iterable<Orderitems> items = order.getOrderitemses();
        Orderstatuses status = itemStatus.getStatus();

        for (Orderitems item : items) {
            item.setOrderstatuses(status);
            item = orderitemsRepository.save(item);
        }

        order.setPaymentStatus(paymentStatus.getStatus());
        return updateOrderStatus(order);
    }

    @Override
    public Orders addOrderPayment(Orders order, Payments payment, Object... meta) {
        Set<Payments> payments = order.getPaymentses();

        payment = paymentsrepository.save(payment);
        payments.add(payment);
        return saveOrder(order, meta);

    }

    @Override
    public Orders addOrderPayments(Orders order, Iterable<Payments> payments, Object... meta) {
        for (Payments op : payments) {
            order = addOrderPayment(order, op, meta);
        }
        return order;
    }

    @Override
    public Orders updateOrderItemStatuses(Orders order, OrderItemStatusType status) {
        Iterable<Orderitems> items = order.getOrderitemses();
        Orderstatuses statusObj = status.getStatus();

        for (Orderitems item : items) {
            item.setOrderstatuses(statusObj);
            item = orderitemsRepository.save(item);
        }

        return updateOrderStatus(order);
    }

    @Override
    public boolean orderRequiresShipping(Orders order) {
        Iterable<Orderitems> items = order.getOrderitemses();

        for (Orderitems item : items) {
            if (item.getOrderitemtypes().getOrderItemTypeId() == OrderitemtypesType.WALLETTYPE.getType().getOrderItemTypeId()
                || (item.getOrderitemtypes().getOrderItemTypeId() == OrderitemtypesType.WALLET.getType().getOrderItemTypeId() && item.getOrderitemactions().getOrderItemActionId() == OrderitemactionsType.REPLACE
                    .getType().getOrderItemActionId())) {
                return true;
            }
        }

        return false;
    }

    private Orders deriveOrderStatus(Orders order) {
        List<Orderitems> items = order.getOrderitemses();

        int cancelled = 0;

        for (Orderitems item : items) {
            Integer status = new Integer(item.getOrderstatuses().getStatus());
            if (status.equals(OrderItemStatusType.UNSUBMITTED.getIndex())) {
                order.setOrderstatuses(OrderStatusType.UNSUBMITTED.getStatus());
                return order;
            } else if (status.equals(OrderItemStatusType.ABANDONED.getIndex())) {
                order.setOrderstatuses(OrderStatusType.ABANDONED.getStatus());
                return order;
            } else if (status.equals(OrderItemStatusType.SUBMITTED.getIndex()) || status.equals(OrderItemStatusType.FULFILLMENT.getIndex())
                || status.equals(OrderItemStatusType.REJECTED.getIndex())) {
                order.setOrderstatuses(OrderStatusType.IN_PROGRESS.getStatus());
                return order;
            } else if (status.equals(OrderItemStatusType.HOLD.getIndex())) {
                order.setOrderstatuses(OrderStatusType.HOLD.getStatus());
                return order;
            } else {
                if (status.equals(OrderItemStatusType.CANCELLED.getIndex())) {
                    cancelled += 1;
                }
            }
        }

        if (cancelled == items.size()) {
            order.setOrderstatuses(OrderStatusType.CANCELLED.getStatus());
        } else {
            order.setOrderstatuses(OrderStatusType.COMPLETE.getStatus());
        }
        return order;
    }

    @Override
    public boolean isCardBeingReplaced(User user, Wallets wallet) {
        if (user == null || wallet == null) {
            return false;
        }
        List<Orders> orders = ordersRepository.findByUserId(user.getId());
        for (Orders o : orders) {
            if ((o.getOrderitemses() != null) && (o.getOrderstatuses().getDescription().equals(OrderStatusType.HOLD.getStatus().getDescription()))) {
                for (Orderitems oi : o.getOrderitemses()) {
                    if (oi.getWallets().getWalletId().equals(wallet.getWalletId())) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    @Override
    public Orders updateCart(Orders order, User user, HttpSession session) {
        Orders savedorder = getOrder(order.getOrderId());
        List<Orderitems> items = order.getOrderitemses();
        List<Orderitems> saveditems = sortOrderItems(savedorder);
        List<Orderitems> toSave = new ArrayList<Orderitems>();
        List<Orderitems> toDelete = new ArrayList<Orderitems>();

        for (int i = 0, l = items.size(); i < l; ++i) {
            log.debug("There are: " + items.size() + " items in the order");
            Orderitems current = saveditems.get(i);
            Short quantity = items.get(i).getQuantity();
            if (!quantity.equals(new Short((short) 0))) {
                log.debug("Setting new quantity");
                if (current.getParentItemNumber() != current.getId().getOrderItemNumber()) {
                    Short parentQuantity = items.get(current.getParentItemNumber()).getQuantity();
                    if (parentQuantity.equals(new Short((short) 0))) {
                        toDelete.add(current);
                    } else {
                        current.setQuantity(quantity);
                        current.setParentQuantity(parentQuantity);
                        current.setAmount(new BigDecimal(quantity).multiply(new BigDecimal(parentQuantity)).multiply(current.getUnitPrice()));
                        toSave.add(current);
                    }
                } else {
                    current.setQuantity(quantity);
                    current.setAmount(new BigDecimal(quantity).multiply(new BigDecimal(current.getParentQuantity())).multiply(current.getUnitPrice()));
                    toSave.add(current);
                }
            } else {
                toDelete.add(current);
            }
        }
        orderitemsRepository.save(toSave);
        removeOrderItems(savedorder, toDelete, user, session);
        savedorder.setOrderitemses(toSave);
        if (!orderRequiresShipping(order)) {
            order.setShippers(null);
        }
        order = saveOrder(savedorder, user, session);
        return order;
    }

    @Transactional(readOnly = true)
    @Override
    public List<Orders> getFulfillmentOrdersByFilter(Integer orderId, Date fromDate, Date toDate, Byte status) throws EntityNotFoundException {

        // set toDate based on end of the current day (start of next day)
        toDate = DateHelper.toNextMorning(toDate);

        List<Orders> ordersList = null;
        if (log.isInfoEnabled()) {
            log.info("Searching for orders with the following queries:");
            log.info("orderId: " + orderId);
            log.info("fromDate: " + fromDate);
            log.info("toDate: " + toDate);
            log.info("status: " + status);
        }

        if (status != null && status != ALL) {
            if (FulfillmentSearchType.PENDING_AUTOLOAD.getIndex().byteValue() == status) {
                ordersList = ordersRepository.findPendingAutoload(orderId, fromDate, toDate, ADD_VALUE, ADD_PRODUCT, IN_PROGRESS, FULFILLMENT);
            } else if (FulfillmentSearchType.PENDING_FULFILLMENT.getIndex().byteValue() == status) {
                ordersList = ordersRepository.findPendingFulfillment(orderId, fromDate, toDate, NEW_ACTION, WALLET_TYPE, IN_PROGRESS, FULFILLMENT);
            } else if (FulfillmentSearchType.COMPLETED.getIndex().byteValue() == status) {
                ordersList = ordersRepository.findOrdersByFilter(orderId, fromDate, toDate, COMPLETED, COMPLETED, CANCELED, IN_PROGRESS, FULFILLMENT);
            } else if (FulfillmentSearchType.CANCELED.getIndex().byteValue() == status) {
                ordersList = ordersRepository.findOrdersByFilter(orderId, fromDate, toDate, CANCELED, COMPLETED, CANCELED, IN_PROGRESS, FULFILLMENT);
            }
        } else {
            ordersList = ordersRepository.findOrdersByFilter(orderId, fromDate, toDate, null, COMPLETED, CANCELED, IN_PROGRESS, FULFILLMENT);
        }

        if (log.isInfoEnabled()) {
            log.info("Found " + ordersList.size() + " orders");
            for (Orders order : ordersList) {
                log.info("-----------------\nOrder\n-----------------");
                log.info("Order ID: " + order.getOrderId());
                log.info("Date: " + order.getDateOrdered());
            }
        }

        return ordersList;
    }

    @Override
    public List<Orderstatuses> getFulfillmentStatuses() throws EntityNotFoundException {
        List<Orderstatuses> statuses = new ArrayList<>();
        for (FulfillmentSearchType type : FulfillmentSearchType.values()) {
            Orderstatuses status = new Orderstatuses();
            status.setStatus(type.getIndex().byteValue());
            status.setDescription(type.getCasedName());
            statuses.add(status);
        }
        return statuses;
    }

    @Override
    public List<Wallets> getWalletsForOrder(Orders order) {
        List<Wallets> wallets = new ArrayList<Wallets>();
        for (Orderitems oi : order.getOrderitemses()) {
            if (oi.getOrderitemtypes() == OrderitemtypesType.WALLET.getType()) {
                Wallets w = oi.getWallets();
                if (w != null && (wallets.contains(w) == false)) {
                    wallets.add(w);
                }
            }
        }
        return wallets;
    }

    @Override
    public Iterable<Orders> getOrders(List<Integer> orderIds) throws EntityNotFoundException {
        Iterable<Orders> orders = ordersRepository.findAll(orderIds);
        if (null == orders) {
            throw new EntityNotFoundException("No orders found for order numbers: " + orderIds);
        }
        return orders;
    }

    @Override
    public Orderitems getOrCreateOrderItemForExistingCard(Orders order, Wallets wallet) {
        Orderitems existing = orderitemsRepository.findByOrderIdWalletId(order.getOrderId(), wallet.getWalletId());
        if (existing != null) {
            return existing;
        }

        short shortQuantity = 1;
        Offerings offer = offersRepository.findByWalletType(wallet.getWallettypes().getWalletTypeId());
        Integer orderItemNumber = new Integer(order.getOrderitemses().size());
        short parentQuantity = 1;

        Orderitems orderitem =
            new Orderitems(new OrderitemsId(order.getOrderId(), orderItemNumber.byteValue()), OrderitemactionsType.CHANGE.getType(), OrderitemtypesType.WALLET.getType(),
                orderItemNumber.byteValue(), parentQuantity, OrderItemStatusType.UNSUBMITTED.getStatus(), order, shortQuantity, offer.getUnitPrice(), offer.getUnitPrice()
                    .multiply(new BigDecimal(shortQuantity)));
        orderitem.setWallets(wallet);

        return orderitem;
    }

    @Override
    public Integer orderItemsCount(Orders order) {
        int count = 0;
        if (order != null) {
            for (Orderitems oi : order.getOrderitemses()) {
                if (!orderItemParentOnly(oi)) {
                    count += 1;
                }
            }
        }
        return count;
    }

    private boolean orderItemParentOnly(Orderitems orderItem) {
        if ((orderItem.getOrderitemtypes().getOrderItemTypeId() == OrderitemtypesType.WALLET.getType().getOrderItemTypeId())
            && (orderItem.getOrderitemactions().getOrderItemActionId() != OrderitemactionsType.REPLACE.getType().getOrderItemActionId())) {
            return true;
        }
        return false;
    }

    @Override
    public Orderitems updateOrderItemFarecode(Orderitems oi, User user) {
        Farecodes farecode = FarecodesType.FULL.getType();

        Wallets w = null;

        if (oi.getParentItemNumber() != null && !oi.getParentItemNumber().equals(oi.getId().getOrderItemNumber())) {
            Orderitems parent = orderitemsRepository.findOne(new OrderitemsId(oi.getId().getOrderId(), oi.getParentItemNumber()));
            w = parent.getWallets();
        } else {
            w = oi.getWallets();
        }

        if (w != null) {
            farecode = w.getFarecodes();
        } else {
            if (user != null && ChannelsType.EFARE.getType().getCanInherit()) {
                List<Farecodes> fcs = peoplerepository.findFarecodesForPerson(user.getId());
                if (fcs.size() > 0) {
                    farecode = fcs.get(0);
                }
            }
        }

        oi.setFarecodes(farecode);

        return oi;
    }

    @Override
    public Orders processReplaceOrder(User user, HttpSession session, Orders order) {
        Wallets w = order.getOrderitemses().get(0).getWallets();
        Orders checkConflict = getCurrentOrder(user, session, OrdertypesType.SALES.getType());
        if (checkConflict != null) {
            for (Orderitems oi : checkConflict.getOrderitemses()) {
                if (oi.getWallets() != null && oi.getWallets().getWalletId().equals(w.getWalletId())) {
                    log.debug("OrderItem removed from cart");
                    removeOrderItem(checkConflict, oi);
                }
            }
        }
        return order;
    }

    @Override
    public Contact getOrderShipping(Orders o) {
        Contact c = new Contact();

        JSONObject data = new JSONObject(o.getOrderData());
        JSONObject address = (JSONObject) data.get("Address");
        c.setFirstName(data.getString("firstName"));
        c.setLastName(data.getString("lastName"));
        c.setAddress1(address.getString("line1"));
        c.setAddress2(address.getString("line2"));
        c.setCity(address.getString("city"));
        c.setState(address.getString("state"));
        c.setZip(address.getString("postalCode"));
        c.setCountry(address.getString("country"));
        return c;
    }

    @Override
    public short validateQuantity(String quantity) {
        short shortQuantity = 0;
        try {
            shortQuantity = (quantity.equals("")) ? 0 : Short.parseShort(quantity);
        } catch (NumberFormatException e) {
            shortQuantity = -1;
        }
        return shortQuantity;
    }

    @Override
    public String checkRules(Orderitems orderitem, Orders order, User user, HttpSession session) {
        try {
            if (!rulesService.isValid(orderitem)) {
                return "itemError";
            }
            orderitem = updateOrderItemFarecode(orderitem, user);
            addOrderItem(order, orderitem, user, session);
            return null;
        } catch (InvalidOrderException ex) {
            log.info(ex.getMessage());
            return "orderError";
        }
    }

}
