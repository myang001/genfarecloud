package com.genfare.cloud.model.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the equipmentcollections database table.
 * 
 */
@Entity
@Table(name = "equipmentcollections")
@NamedQuery(name = "Equipmentcollections.findAll", query = "SELECT e FROM Equipmentcollections e")
public class Equipmentcollections implements Serializable {
    private static final long serialVersionUID = 1L;
    private int equipmentCollectionID;
    private Date createdAt;
    private String createdBy;
    private String description;
    private String name;
    private Date touchedAt;
    private String touchedBy;
    private Set<Equipment> equipments;
    private Set<Groups> groupss;

    public Equipmentcollections() {
    }

    @Id
    @Column(name = "EquipmentCollectionID")
    public int getEquipmentCollectionID() {
        return this.equipmentCollectionID;
    }

    public void setEquipmentCollectionID(int equipmentCollectionID) {
        this.equipmentCollectionID = equipmentCollectionID;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CreatedAt")
    public Date getCreatedAt() {
        return this.createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    @Column(name = "CreatedBy")
    public String getCreatedBy() {
        return this.createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Column(name = "Description")
    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name = "Name")
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "TouchedAt")
    public Date getTouchedAt() {
        return this.touchedAt;
    }

    public void setTouchedAt(Date touchedAt) {
        this.touchedAt = touchedAt;
    }

    @Column(name = "TouchedBy")
    public String getTouchedBy() {
        return this.touchedBy;
    }

    public void setTouchedBy(String touchedBy) {
        this.touchedBy = touchedBy;
    }

    @ManyToMany(cascade = { CascadeType.ALL })
    @JoinTable(name = "equipmentcollectionsequipment", joinColumns = { @JoinColumn(name = "EquipmentID") }, inverseJoinColumns = { @JoinColumn(name = "EquipmentCollectionID") })
    public Set<Equipment> getEquipments() {
        return equipments;
    }

    public void setEquipments(Set<Equipment> equipments) {
        this.equipments = equipments;
    }

    @ManyToMany(cascade = { CascadeType.ALL })
    @JoinTable(name = "groupsequipmentcollections", joinColumns = { @JoinColumn(name = "GroupID") }, inverseJoinColumns = { @JoinColumn(name = "EquipmentCollectionID") })
    public Set<Groups> getGroupss() {
        return groupss;
    }

    public void setGroupss(Set<Groups> groupss) {
        this.groupss = groupss;
    }

}