package com.genfare.cloud.model.entity.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.genfare.cloud.model.entity.Privileges;

public interface PrivilegesRepository
    extends CrudRepository<Privileges, Integer> {

    @Query("from Privileges p where p.name = ?")
    public Privileges findByName(String name);

    @Query("from Privileges p where p.permission = ?")
    public Privileges findByPermission(String name);
}
