package com.genfare.cloud.model.converters;

import java.io.StringReader;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import com.genfare.cloud.model.converters.order.OrderManagementAPI;
import com.genfare.cloud.model.entity.Farecodes;
import com.genfare.cloud.model.entity.Offerings;
import com.genfare.cloud.model.entity.Orderitemactions;
import com.genfare.cloud.model.entity.Orderitems;
import com.genfare.cloud.model.entity.OrderitemsId;
import com.genfare.cloud.model.entity.Orders;
import com.genfare.cloud.model.entity.Orderstatuses;
import com.genfare.cloud.model.entity.Tickets;
import com.genfare.cloud.model.entity.Wallets;
import com.genfare.cloud.model.entity.Wallettypes;
import com.genfare.cloud.model.om.FarecodeType;
import com.genfare.cloud.model.om.OfferingType;
import com.genfare.cloud.model.om.OrderDetailsType;
import com.genfare.cloud.model.om.OrderHeaderType;
import com.genfare.cloud.model.om.OrderItemType;
import com.genfare.cloud.model.om.OrderType;
import com.genfare.cloud.model.om.TicketType;
import com.genfare.cloud.model.om.WalletType;
import com.genfare.cloud.model.om.WallettypeType;

public class OrderTypeToOrderConverter {

    // public static void main(String[] argv) throws Exception{
    // File file=new File("src/test/resources/test.order.xml");
    // BufferedReader br=new BufferedReader(new FileReader(file));
    // String xml="";
    // while(true){
    // String str=br.readLine();
    // if(str==null)
    // break;
    // xml+=str+"\n";
    // }
    //
    // OrderTypeToOrderConverter c=new OrderTypeToOrderConverter();
    // Orders order=c.convert(xml);
    // System.out.println(order.info());
    // }

    public Orders convert(String xml) throws Exception {
        StringReader reader = new StringReader(xml);

        JAXBContext jaxbContext = JAXBContext.newInstance(OrderManagementAPI.class);

        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

        OrderManagementAPI api = (OrderManagementAPI) jaxbUnmarshaller.unmarshal(reader);

        OrderType orderType = api.getOrderType();
        String orderID = orderType.getOrderID();
        int orderId = new Integer(orderID);
        OrderHeaderType orderHeader = orderType.getOrderHeader();
        OrderDetailsType orderDetails = orderType.getOrderDetails();

        Orders order = new Orders();
        order.setOrderId(orderId);

        String dateStr = orderHeader.getDateOrdered();
        DateConverter dateConverter = new DateConverter();
        Date dateOrdered = dateConverter.parse(dateStr);
        order.setDateOrdered(dateOrdered);

        Float amountOrder = orderHeader.getAmount();
        order.setAmountOrder(new BigDecimal(amountOrder));

        String notes = orderHeader.getNotes();
        order.setNotes(notes);

        String orderData = orderHeader.getData();
        order.setOrderData(orderData);

        String orderStausDescription = orderHeader.getOrderStatus();
        Orderstatuses orderstatuses = new Orderstatuses();
        orderstatuses.setDescription(orderStausDescription);
        order.setOrderstatuses(orderstatuses);

        List<OrderItemType> orderItemTypeList = orderDetails.getOrderItem();
        List<Orderitems> orderitemsList = order.getOrderitemses();
        for (OrderItemType oit : orderItemTypeList) {
            Orderitems oi = convert(orderId, oit);
            orderitemsList.add(oi);
        }

        return order;
    }

    private Orderitems convert(int orderId, OrderItemType oit) {
        Orderitems oi = new Orderitems();

        Integer oin = oit.getOrderItemNumber();
        if (oin != null) {
            int n = oin;
            OrderitemsId orderitemsId = new OrderitemsId(orderId, (byte) n);
            oi.setId(orderitemsId);
        }

        String action = oit.getAction();
        if (action != null) {
            Orderitemactions orderitemactions = new Orderitemactions();
            orderitemactions.setDescription(action);
            oi.setOrderitemactions(orderitemactions);
        }

        Float amount = oit.getAmount();
        if (amount != null)
            oi.setAmount(new BigDecimal(amount));

        Float amountTax = oit.getAmountTax();
        if (amountTax != null)
            oi.setAmountTax(new BigDecimal(amountTax));

        FarecodeType farecode = oit.getFarecode();
        if (farecode != null) {
            Farecodes farecodes = new Farecodes(farecode.getDescription());
            int farecodeID = farecode.getFarecodeID();
            farecodes.setFarecodeId((byte) farecodeID);
            oi.setFarecodes(farecodes);
        }

        WalletType wt = oit.getWallet();
        if (wt != null) {
            WalletsToWalletType c = new WalletsToWalletType();
            Wallets w = c.convert(wt);
            oi.setWallets(w);
        }

        TicketType tt = oit.getTicket();
        if (tt != null) {
            TicketsToTicketType c = new TicketsToTicketType();
            Tickets tickets = c.convert(tt);
            oi.setTickets(tickets);
        }

        WallettypeType wType = oit.getWallettype();
        if (wType != null) {
            WallettypesToWallettypeType cWalletType = new WallettypesToWallettypeType();
            Wallettypes wallettypes = cWalletType.convert(wType);
            oi.setWallettypes(wallettypes);
        }

        OfferingType ot = oit.getOffering();
        if (ot != null) {
            OfferingsToOfferingTypeConverter cOffering = new OfferingsToOfferingTypeConverter();
            Offerings offerings = cOffering.convert(ot);
            oi.setOfferings(offerings);
        }

        Integer pin = oit.getParentItemNumber();
        if (pin != null) {
            int parentItemNumber = pin;
            oi.setParentItemNumber((byte) parentItemNumber);
        }

        Integer qty = oit.getQuantity();
        if (qty != null) {
            int q = qty;
            oi.setQuantity((short) q);
        }

        String status = oit.getStatus();
        if (status != null) {
            Orderstatuses s = new Orderstatuses();
            s.setDescription(status);
            oi.setOrderstatuses(s);
        }

        Float price = oit.getUnitPrice();
        if (price != null)
            oi.setUnitPrice(new BigDecimal(price));

        return oi;
    }
}