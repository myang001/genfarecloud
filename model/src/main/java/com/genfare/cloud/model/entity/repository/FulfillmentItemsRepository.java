package com.genfare.cloud.model.entity.repository;

import org.springframework.data.repository.CrudRepository;

import com.genfare.cloud.model.entity.FulfillmentItems;
import com.genfare.cloud.model.entity.OrderitemsId;

public interface FulfillmentItemsRepository
    extends CrudRepository<FulfillmentItems, OrderitemsId> {

}
