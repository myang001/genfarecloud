package com.genfare.cloud.model.security;

/**
 * @author martin
 */
public enum SocialMediaService {
    FACEBOOK,
    TWITTER
}
