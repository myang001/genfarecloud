package com.genfare.cloud.model.exception;

public class InvalidOrderException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = 5098789088427395759L;

    public InvalidOrderException(String msg) {
        super(msg);
    }

}
