package com.genfare.cloud.model.listener;

import java.util.Date;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

import org.springframework.security.core.context.SecurityContextHolder;

import com.genfare.cloud.model.dto.UserDetails;
import com.genfare.cloud.model.entity.inter.Auditable;

public class AuditListener {

    @PreUpdate
    public void onPreUpdate(Auditable entity) {
        Auditable auditable = (Auditable) entity;
        auditable.setCreatedBy(auditable.getCreatedBy());
        auditable.setCreatedAt(auditable.getCreatedAt());
        Date date = new Date();
        if (SecurityContextHolder.getContext().getAuthentication() != null) {
            Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            if (principal instanceof String) {
                auditable.setTouchedBy(principal.toString());
            } else {
                auditable.setTouchedBy(((UserDetails) principal).getUsername());
            }
        }
        auditable.setTouchedAt(date);
    }

    @PrePersist
    public void onPreInsert(Auditable entity) {
        Auditable auditable = (Auditable) entity;
        Date date = new Date();
        if (SecurityContextHolder.getContext().getAuthentication() != null) {
            Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            if (principal instanceof String) {
                auditable.setCreatedBy(principal.toString());
                auditable.setTouchedBy(principal.toString());
            } else {
                auditable.setCreatedBy(((UserDetails) principal).getUsername());
                auditable.setTouchedBy(((UserDetails) principal).getUsername());
            }
        }
        auditable.setCreatedAt(date);
        auditable.setTouchedAt(date);
    }

}
