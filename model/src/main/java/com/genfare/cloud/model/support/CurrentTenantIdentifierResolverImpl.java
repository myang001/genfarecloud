package com.genfare.cloud.model.support;

import org.hibernate.context.spi.CurrentTenantIdentifierResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.genfare.cloud.model.tenant.TenantConfiguration;
import com.genfare.cloud.model.tenant.impl.ResourceTenantConfigurationRepository;

@Component
public class CurrentTenantIdentifierResolverImpl
    implements CurrentTenantIdentifierResolver {

    private static final Logger LOGGER = LoggerFactory.getLogger(CurrentTenantIdentifierResolverImpl.class);

    @Autowired
    private TenantConfiguration tenantConfiguration;

    public CurrentTenantIdentifierResolverImpl() {
        LOGGER.info("CREATING CurrentTenantIdentifierResolverImpl");
    }

    @Override
    public String resolveCurrentTenantIdentifier() {
        String tenant = null;
        try {
            tenant = tenantConfiguration.getSchemaName();
        } catch (Exception e) {
            LOGGER.error("No tenant configuration loaded", e);
            ResourceTenantConfigurationRepository resource = new ResourceTenantConfigurationRepository();
            tenantConfiguration = resource.getDefaultTenant();
            tenant = tenantConfiguration.getSchemaName();
        }
        return tenant;
    }

    @Override
    public boolean validateExistingCurrentSessions() {
        return true;
    }

}