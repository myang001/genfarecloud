package com.genfare.cloud.model.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.genfare.cloud.model.dto.UserDetails;
import com.genfare.cloud.model.entity.repository.LoginsRepository;
import com.genfare.cloud.model.service.OrderManagementService;
import com.genfare.cloud.model.types.OrdertypesType;

public class ModelHandlerInterceptor
    extends HandlerInterceptorAdapter {

    private LoginsRepository loginsRepository;

    private OrderManagementService orderManagementService;

    public void setLoginsRepository(LoginsRepository loginsRepository) {
        this.loginsRepository = loginsRepository;
    }

    public void setOrderManagementService(
        OrderManagementService orderManagementService) {
        this.orderManagementService = orderManagementService;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, final Object handler,
        ModelAndView modelAndView)
        throws Exception {

        if (response.getStatus() == 200) {

            String name = "Guest";
            String items = "0";

            if (SecurityContextHolder.getContext().getAuthentication().getPrincipal() != "anonymousUser") {
                String user = ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername();
                name = loginsRepository.findFirstLastNameFromLogin(user);
                items = loginsRepository.findOrderItemCountFromLogin(user).toString();
            } else {
                items = orderManagementService.orderItemsCount(orderManagementService.getCurrentOrder(null, request.getSession(), OrdertypesType.SALES.getType())).toString();
            }

            modelAndView.getModelMap().addAttribute("name", name);
            modelAndView.getModelMap().addAttribute("numOfItems", items);
        }
    }

}
