package com.genfare.cloud.model.converters.pos.person;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;

import com.genfare.cloud.model.entity.Modules;
import com.genfare.pos.core.model.person.ModuleType;

public class ModulesToModuleTypeConverter implements
    Converter<Modules, ModuleType> {

    private static final Logger log = LoggerFactory
        .getLogger(ModulesToModuleTypeConverter.class);

    @Override
    public ModuleType convert(Modules m) {
        ModuleType mt = new ModuleType();
        mt.setModuleId(m.getModuleId());
        mt.setName(m.getName());
        return mt;
    }
}
