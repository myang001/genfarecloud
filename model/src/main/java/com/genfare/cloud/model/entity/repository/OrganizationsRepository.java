package com.genfare.cloud.model.entity.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.genfare.cloud.model.entity.Organizations;

public interface OrganizationsRepository
    extends CrudRepository<Organizations, Integer> {

    @Query("from Organizations o where o.name = ?")
    public Organizations findByName(String name);

    @Query("select o.name from Organizations o where o.name like :name")
    public List<String> findOrgsLikeName(@Param("name") String name);
}
