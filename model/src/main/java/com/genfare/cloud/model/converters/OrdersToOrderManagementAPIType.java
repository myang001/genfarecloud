/**
 *
 */
package com.genfare.cloud.model.converters;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.genfare.cloud.model.entity.Orderitems;
import com.genfare.cloud.model.entity.Orders;
import com.genfare.cloud.model.om.ObjectFactory;
import com.genfare.cloud.model.om.OrderDetailsType;
import com.genfare.cloud.model.om.OrderHeaderType;
import com.genfare.cloud.model.om.OrderItemType;
import com.genfare.cloud.model.om.OrderManagementAPIType;
import com.genfare.cloud.model.om.OrderType;
import com.genfare.cloud.model.om.OrdertypeType;

/**
 * @author mpeter
 */
@Component
public class OrdersToOrderManagementAPIType
    implements Converter<Orders, OrderManagementAPIType> {

    @Override
    public OrderManagementAPIType convert(Orders order) {
        ObjectFactory of = new ObjectFactory();
        OrderManagementAPIType omapi = of.createOrderManagementAPIType();
        OrderType orderType = of.createOrderType();
        orderType.setOrderID(order.getOrderId().toString());

        OrderHeaderType orderHeaderType = of.createOrderHeaderType();

        // TODO: replace with retrieval from entity
        Date date = order.getDateOrdered();
        try {
            DateConverter dateConverter = new DateConverter();
            String dateStr = dateConverter.format(date);
            orderHeaderType.setDateOrdered(dateStr);
        } catch (Exception e) {
            throw new IllegalArgumentException("Unable to convert date: " + date);
        }

        if (order.getAmountOrder() != null) {
            orderHeaderType.setAmount(order.getAmountOrder().floatValue());
        }

        orderHeaderType.setNotes(order.getNotes());
        orderHeaderType.setData(order.getOrderData());
        orderHeaderType.setOrderStatus(order.getOrderstatuses().getDescription());

        OrdertypeType ordertypeType = of.createOrdertypeType();
        ordertypeType.setDescription(order.getOrdertypes().getDescription());
        ordertypeType.setOrdertypeID(order.getOrdertypes().getOrderTypeId());
        orderHeaderType.setOrdertype(ordertypeType);

        orderType.setOrderHeader(orderHeaderType);

        OrderDetailsType orderDetailsType = of.createOrderDetailsType();
        List<OrderItemType> orderItemTypeList = orderDetailsType.getOrderItem();
        orderType.setOrderDetails(orderDetailsType);

        Iterator<Orderitems> oisList = order.getOrderitemses().iterator();
        while (oisList.hasNext()) {
            Orderitems orderitems = (Orderitems) oisList.next();
            OrderItemsToOrderItemType oic = new OrderItemsToOrderItemType();
            OrderItemType oit = oic.convert(orderitems);
            orderItemTypeList.add(oit);
        }
        orderType.setOrderDetails(orderDetailsType);
        omapi.setOrder(orderType);
        return omapi;
    }
}