package com.genfare.cloud.model.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.genfare.cloud.model.entity.Components;
import com.genfare.cloud.model.entity.Groupprivileges;
import com.genfare.cloud.model.entity.GroupprivilegesId;
import com.genfare.cloud.model.entity.Groups;
import com.genfare.cloud.model.entity.Groupsofpeople;
import com.genfare.cloud.model.entity.GroupsofpeopleId;
import com.genfare.cloud.model.entity.Modules;
import com.genfare.cloud.model.entity.Organizations;
import com.genfare.cloud.model.entity.People;
import com.genfare.cloud.model.entity.Privileges;
import com.genfare.cloud.model.entity.repository.GroupprivilegesRepository;
import com.genfare.cloud.model.entity.repository.GroupsRepository;
import com.genfare.cloud.model.entity.repository.GroupsofpeopleRepository;
import com.genfare.cloud.model.entity.repository.PeopleRepository;
import com.genfare.cloud.model.entity.repository.PrivilegesRepository;
import com.genfare.cloud.model.security.User;
import com.genfare.cloud.model.types.GroupStatusType;

@Service
@Transactional
public class GroupPermissionsServiceImpl implements GroupPermissionsService {

    @Autowired
    private PeopleRepository peopleRepo;

    @Autowired
    private GroupsRepository groupsRepo;

    @Autowired
    private PrivilegesRepository privsRepo;

    @Autowired
    private GroupsofpeopleRepository groupsPeopleRepo;

    @Autowired
    private GroupprivilegesRepository groupPrivsRepo;

    @Override
    public List<Modules> sortModules(List<Modules> modules) {
        for (Modules m : modules) {
            List<Components> components = (List<Components>) m.getComponentses();
            for (Components c : components) {
                Collections.sort(c.getPrivilegeses(), new Comparator<Privileges>() {
                    @Override
                    public int compare(Privileges o1, Privileges o2) {
                        return o1.getPrivilegeId().compareTo(o2.getPrivilegeId());
                    }
                });
            }
            Collections.sort(components, new Comparator<Components>() {
                @Override
                public int compare(Components o1, Components o2) {
                    return o1.getName().compareTo(o2.getName());
                }
            });
        }

        Collections.sort(modules, new Comparator<Modules>() {
            @Override
            public int compare(Modules o1, Modules o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });

        return modules;
    }

    @Override
    public boolean groupHasPermission(Groups group, Privileges privilege) {

        for (Groupprivileges gp : group.getGroupprivilegeses()) {
            if (gp.getPrivileges().equals(privilege)) {
                return true;
            }
        }

        return false;
    }

    @Override
    public void addUserToGroup(User user, String groupName) {
        People p = peopleRepo.findByPersonId(user.getId());
        Groups g = groupsRepo.findGroupByName(groupName);

        Groupsofpeople gp = new Groupsofpeople(new GroupsofpeopleId(g.getGroupId(), p.getPersonId()), g, p);
        groupsPeopleRepo.save(gp);
    }

    @Override
    public void removeUserFromGroup(User user, String groupName) {
        People p = peopleRepo.findOne(user.getId());
        Groups g = groupsRepo.findGroupByName(groupName);

        groupsPeopleRepo.delete(new GroupsofpeopleId(g.getGroupId(), p.getPersonId()));
    }

    @Override
    public String[] getComponentsPermission(Components components) {
        List<String> permission = new ArrayList<String>();
        for (Privileges privileges : components.getPrivilegeses()) {
            permission.add(privileges.getPermission());
        }
        return permission.toArray(new String[permission.size()]);
    }

    @Override
    public String[] getModulePermission(Modules modules) {
        Set<String> permission = new HashSet<String>();
        for (Components components : modules.getComponentses()) {
            for (Privileges privileges : components.getPrivilegeses()) {
                permission.add(privileges.getPermission());
            }
        }
        return permission.toArray(new String[permission.size()]);
    }

    @Override
    public Groups createDefaultGroupForOrg(Organizations o) {
        String role = String.format("ROLE_%s_DEFAULT", o.getName().replace(" ", "_").toUpperCase());
        String name = "Default Group";
        Groups g = new Groups(role, name, "Default access group for " + o.getName());
        g.setOrganizations(o);
        g.setStatus(GroupStatusType.ACTIVE.getStatus());
        g = groupsRepo.save(g);

        Privileges p = privsRepo.findByPermission("PERM_ADMIN_ORGMGMT_VIEW_ORG");
        Groupprivileges gp = new Groupprivileges(new GroupprivilegesId(g.getGroupId(), p.getPrivilegeId()), g, p);
        groupPrivsRepo.save(gp);
        return g;

    }
}
