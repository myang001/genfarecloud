package com.genfare.cloud.model.types;

import java.util.EnumSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.genfare.cloud.model.entity.PeopleStatus;
import com.genfare.cloud.model.entity.repository.PeoplestatusRepository;

public enum PeopleStatusType {
    ACTIVE(1),
    DISABLED(2),
    PENDING(3),
    LOCKED(4),
    EXPIRED(5);

    private final Integer index;

    private PeopleStatus status;

    private static Map<String, PeopleStatus> statusStrings = new LinkedHashMap<String, PeopleStatus>();

    private PeopleStatusType(Integer index) {
        this.index = index;
    }

    public Integer getIndex() {
        return index;
    }

    public PeopleStatus getStatus() {
        return status;
    }

    public static PeopleStatus findStatus(String status) {
        return statusStrings.get(status);
    }

    public static PeopleStatusType findEnumStatus(Byte status) {
        for (PeopleStatusType type : PeopleStatusType.values()) {
            if (status == type.index.byteValue()) {
                return type;
            }
        }
        throw new IllegalArgumentException("illegal group type index: " + status);
    }

    public static Iterable<String> getStatuses() {
        return statusStrings.keySet();
    }

    @Component
    public static class Injector {
        @Autowired
        private PeoplestatusRepository repo;

        @PostConstruct
        public void setup() {
            EnumSet<PeopleStatusType> set = EnumSet.allOf(PeopleStatusType.class);
            Iterator<PeopleStatusType> it = set.iterator();
            while (it.hasNext()) {
                PeopleStatusType status = it.next();
                status.status = repo.findOne(status.index.byteValue());
                statusStrings.put(StringUtils.capitalize(status.toString().toLowerCase()), status.status);
            }
        }
    }
}
