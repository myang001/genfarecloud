package com.genfare.cloud.model.converters;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.springframework.core.convert.converter.Converter;

public class StringToDateConverter
    implements Converter<String, Date> {

    @Override
    public Date convert(String input) {
        if (StringUtils.isBlank(input)) {
            return null;
        }
        SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy"); // TODO: Make configurable
        try {
            return format.parse(input);
        } catch (ParseException e) {
            throw new IllegalArgumentException("Not a valid date for format MM/dd/yyyy");
        }
    }

}
