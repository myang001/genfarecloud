package com.genfare.cloud.model.service;

/**
 * The exception is thrown when the email given during the registration phase is already found from the database.
 * 
 * @author martin
 */
public class DuplicateEmailException
    extends Exception {

    private static final long serialVersionUID = -8220771783650071438L;

    public DuplicateEmailException(String message) {
        super(message);
    }
}
