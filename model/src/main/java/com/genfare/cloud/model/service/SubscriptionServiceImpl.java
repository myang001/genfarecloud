package com.genfare.cloud.model.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.genfare.cloud.model.entity.Orderitems;
import com.genfare.cloud.model.entity.OrderitemsId;
import com.genfare.cloud.model.entity.Orders;
import com.genfare.cloud.model.entity.Peoplecards;
import com.genfare.cloud.model.entity.Subscriptions;
import com.genfare.cloud.model.entity.Tickets;
import com.genfare.cloud.model.entity.Walletcontents;
import com.genfare.cloud.model.entity.Wallets;
import com.genfare.cloud.model.entity.repository.OfferingsRepository;
import com.genfare.cloud.model.entity.repository.PeopleRepository;
import com.genfare.cloud.model.entity.repository.SubscriptionsRepository;
import com.genfare.cloud.model.entity.repository.SubscriptionstatusRepository;
import com.genfare.cloud.model.exception.InvalidOrderException;
import com.genfare.cloud.model.security.User;
import com.genfare.cloud.model.tenant.TenantConfiguration;
import com.genfare.cloud.model.types.OrderItemStatusType;
import com.genfare.cloud.model.types.OrderPaymentStatusType;
import com.genfare.cloud.model.types.OrderitemactionsType;
import com.genfare.cloud.model.types.OrderitemtypesType;
import com.genfare.cloud.model.types.OrdertypesType;
import com.genfare.cloud.model.types.SubscriptionstatusType;

@Transactional
@Service
public class SubscriptionServiceImpl
    implements SubscriptionService {

    private static final Logger log = LoggerFactory.getLogger(SubscriptionServiceImpl.class);

    @Autowired
    private SubscriptionstatusRepository subStatusRepo;

    @Autowired
    private SubscriptionsRepository subRepo;

    @Autowired
    private PeopleRepository peopleRepo;

    @Autowired
    private OrderManagementService ordermanager;

    @Autowired
    private OfferingsRepository offerRepo;

    @Autowired
    private TenantConfiguration tenantConfig;

    @Override
    public List<String> getWhenOptions(Walletcontents product) {
        List<String> options = new ArrayList<String>();
        String type = product.getTickets().getTickettypes().getDescription();
        if (type.equals("Stored Ride")) {
            options.add("0 rides left");
            options.add("1 ride left");
            options.add("2 rides left");
        }
        if (type.equals("Stored Fixed Value") || type.equals("Stepped Value")) {
            options.add("$1.00 left");
            options.add("$5.00 left");
            options.add("$10.00 left");
        }
        if (type.equals("Period Pass")) {
            options.add("1 day left");
            options.add("2 days left");
            options.add("5 days left");
            options.add("10 days left");
        }
        return options;
    }

    private String getJSONData(String cardid, String wcid, String expires, String price, String value, String threshold) {
        return new JSONObject().put("peoplecardsId", cardid).put("walletcontentsId", wcid).put("replenishDetails",
            new JSONObject().put("expiration",
                expires).put("price",
                price).put("value",
                value).put("threshold",
                threshold)).toString();
    }

    @Override
    public Orders createSubscriptionOrder(User user, Peoplecards card, Walletcontents walletContent, String expires,
        String price, String value, String threshold) throws InvalidOrderException {

        Tickets ticket = walletContent.getTickets();

        Orders sub = generateSubscriptionOrder(user, card, walletContent, expires, price, value, threshold);

        Orderitems walletOrderItem =
            new Orderitems(new OrderitemsId(sub.getOrderId(), (byte) 0), OrderitemactionsType.CHANGE.getType(),
                OrderitemtypesType.WALLET.getType(), (byte) 0, (short) 0,
                OrderItemStatusType.SUBMITTED.getStatus(), sub, (short) 1, new BigDecimal(0),
                new BigDecimal(0));
        walletOrderItem.setWallets(walletContent.getWallets());
        Orderitems ticketOrderItem =
            new Orderitems(new OrderitemsId(sub.getOrderId(), (byte) 1), OrderitemactionsType.NEW.getType(),
                OrderitemtypesType.TICKET.getType(), (byte) 0, (short) 0,
                OrderItemStatusType.SUBMITTED.getStatus(), sub, (short) 1, new BigDecimal(0),
                new BigDecimal(0));
        ticketOrderItem.setOfferings(offerRepo.findOne(new Byte((byte) 29)));
        ticketOrderItem.setTickets(ticket);
        List<Orderitems> items = new ArrayList<Orderitems>();
        items.add(walletOrderItem);
        items.add(ticketOrderItem);
        ordermanager.addOrderItems(sub, items);
        ordermanager.updateOrderStatuses(sub, OrderPaymentStatusType.UNPAID, OrderItemStatusType.SUBMITTED);

        return sub;
    }

    @Override
    public Orders modifySubscriptionOrder(User user, Peoplecards card, Walletcontents walletContent, String expires,
        String price, String value, String threshold) throws InvalidOrderException {
        Tickets ticket = walletContent.getTickets();

        Orders sub = generateSubscriptionOrder(user, card, walletContent, expires, price, value, threshold);

        Orderitems walletOrderItem =
            new Orderitems(new OrderitemsId(sub.getOrderId(), (byte) 0), OrderitemactionsType.CHANGE.getType(),
                OrderitemtypesType.WALLET.getType(), (byte) 0, (short) 0,
                OrderItemStatusType.SUBMITTED.getStatus(), sub, (short) 1, new BigDecimal(0),
                new BigDecimal(0));
        walletOrderItem.setWallets(walletContent.getWallets());
        Orderitems orderItem =
            new Orderitems(new OrderitemsId(sub.getOrderId(), (byte) 1), OrderitemactionsType.CHANGE.getType(),
                OrderitemtypesType.TICKET.getType(), (byte) 0, (short) 0,
                OrderItemStatusType.SUBMITTED.getStatus(), sub, (short) 1, new BigDecimal(0),
                new BigDecimal(0));
        orderItem.setOfferings(offerRepo.findOne(new Byte((byte) 29)));
        orderItem.setTickets(ticket);
        List<Orderitems> items = new ArrayList<Orderitems>();
        items.add(walletOrderItem);
        items.add(orderItem);
        ordermanager.addOrderItems(sub, items);
        ordermanager.updateOrderStatuses(sub, OrderPaymentStatusType.UNPAID, OrderItemStatusType.SUBMITTED);
        return sub;
    }

    @Override
    public Orders cancelSubscriptionOrder(User user, Peoplecards card, Walletcontents walletContent, String expires,
        String price, String value, String threshold) throws InvalidOrderException {
        Tickets ticket = walletContent.getTickets();

        Orders sub = generateSubscriptionOrder(user, card, walletContent, expires, price, value, threshold);

        Orderitems walletOrderItem =
            new Orderitems(new OrderitemsId(sub.getOrderId(), (byte) 0), OrderitemactionsType.CANCEL.getType(),
                OrderitemtypesType.WALLET.getType(), (byte) 0, (short) 0,
                OrderItemStatusType.SUBMITTED.getStatus(), sub, (short) 1, new BigDecimal(0),
                new BigDecimal(0));
        walletOrderItem.setWallets(walletContent.getWallets());
        Orderitems orderItem =
            new Orderitems(new OrderitemsId(sub.getOrderId(), (byte) 1), OrderitemactionsType.CANCEL.getType(),
                OrderitemtypesType.TICKET.getType(), (byte) 0, (short) 0,
                OrderItemStatusType.SUBMITTED.getStatus(), sub, (short) 1, new BigDecimal(0),
                new BigDecimal(0));
        orderItem.setOfferings(offerRepo.findOne(new Byte((byte) 29)));
        orderItem.setTickets(ticket);
        List<Orderitems> items = new ArrayList<Orderitems>();
        items.add(walletOrderItem);
        items.add(orderItem);
        ordermanager.addOrderItems(sub, items);
        ordermanager.updateOrderStatuses(sub, OrderPaymentStatusType.UNPAID, OrderItemStatusType.SUBMITTED);
        return sub;
    }

    @Override
    public List<Orders> cancelSubscriptionForWalletOrder(User user, Wallets wallet) throws InvalidOrderException {
        List<Orders> ret = new ArrayList<Orders>();
        for (Walletcontents wc : wallet.getWalletcontentses()) {
            Set<Subscriptions> sub = wc.getSubscriptionses();
            if (sub != null) {
                for (Subscriptions s : sub) {
                    if (!s.getSubscriptionstatus().getDescription().equals(SubscriptionstatusType.CANCELLED.getStatus().getDescription())) {
                        Peoplecards pc = s.getPeoplecards();
                        Date de = pc.getDateExpires();
                        Orders o = cancelSubscriptionOrder(user, pc, wc, de.toString(), s.getReplenishPrice().toString(),
                            s.getReplenishValue().toString(), s.getReplenishThreshold().toString());
                        ret.add(o);
                    }
                }
            }
        }
        return ret;
    }

    @Override
    public String getStatus(Subscriptions subscription) {
        String statusDescription = subscription.getSubscriptionstatus().getDescription();
        if (statusDescription.equals("New") || statusDescription.equals("Active")) {
            return "On";
        }
        return "Off";
    }

    private Orders generateSubscriptionOrder(User user, Peoplecards card, Walletcontents walletContent, String expires,
        String price, String value, String threshold) {
        Orders ret = ordermanager.getCurrentOrCreate(null, null, OrdertypesType.SERVICE.getType());
        ret.setAmountOrder(new BigDecimal(0));
        ret.setDateOrdered(new Date());
        ret.setPeople(peopleRepo.findOne(user.getId()));
        ret.setOrderData(getJSONData(new Integer(card.getId().getCardNumber()).toString(),
            walletContent.getWalletContentsId().toString(), expires, price, value, threshold));
        return ret;

    }

}