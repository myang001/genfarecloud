/**
 *
 */
package com.genfare.cloud.model.converters;

import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;

import com.genfare.cloud.model.entity.Payments;
import com.genfare.cloud.model.om.PaymentType;

/**
 * @author mpeter
 */
public class PaymentToPaymentType implements Converter<Payments, PaymentType> {

    private static final Logger log = LoggerFactory
        .getLogger(CreditCardActivityToCreditCardActivityTypeType.class);

    @Override
    public PaymentType convert(Payments p) {
        GregorianCalendar c = new GregorianCalendar();

        PaymentType pt = new PaymentType();
        pt.setAmmount(p.getAmount());
        CreditCardActivityToCreditCardActivityTypeType ccac = new CreditCardActivityToCreditCardActivityTypeType();
        pt.setCreditCardActivityType(ccac.convert(p.getCreditcardactivity()));

        try {
            c.setTime(p.getDatePayment());
            pt.setDatePayment(DatatypeFactory.newInstance()
                .newXMLGregorianCalendar(c));
        } catch (DatatypeConfigurationException e) {
            log.error("Unable to convert Tickets to TicketType", e);
            return null;
        }
        pt.setOrderID(p.getId().getOrderId());
        pt.setPayentNumber(p.getId().getPaymentNumber());
        PaymentTypeToPaymentTypeType ptc = new PaymentTypeToPaymentTypeType();
        pt.setPaymentTypeType(ptc.convert(p.getPaymenttypes()));
        pt.setReferenceNumber(p.getReferenceNumber());
        return pt;
    }
}
