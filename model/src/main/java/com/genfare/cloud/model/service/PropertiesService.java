package com.genfare.cloud.model.service;

import java.util.List;

import javax.persistence.EntityNotFoundException;

import com.genfare.cloud.model.entity.Properties;

public interface PropertiesService {

    public List<Properties> getProperties(String locale)
        throws EntityNotFoundException;

    public String getProperty(String locale, String key)
        throws EntityNotFoundException;
}