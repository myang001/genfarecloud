package com.genfare.cloud.model.service;

import com.genfare.cloud.model.entity.Organizations;
import com.genfare.cloud.model.security.User;

public interface OrganizationManagementService {

    public Organizations getOrganizationForUser(User user);

    public String getOrganizationNameForUser(User user);
}
