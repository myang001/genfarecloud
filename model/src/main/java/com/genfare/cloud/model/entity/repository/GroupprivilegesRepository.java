package com.genfare.cloud.model.entity.repository;

import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import com.genfare.cloud.model.entity.Groupprivileges;

public interface GroupprivilegesRepository
    extends CrudRepository<Groupprivileges, Integer> {

    @Query("from Groupprivileges gp where gp.groups.groupId = ?")
    List<Groupprivileges> findByGroupId(Integer id);

}
