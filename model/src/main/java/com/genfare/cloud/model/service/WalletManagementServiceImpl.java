package com.genfare.cloud.model.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.persistence.EntityNotFoundException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.genfare.cloud.model.entity.Crossreferencefile;
import com.genfare.cloud.model.entity.Logins;
import com.genfare.cloud.model.entity.Orderitems;
import com.genfare.cloud.model.entity.People;
import com.genfare.cloud.model.entity.Tickets;
import com.genfare.cloud.model.entity.Walletcontents;
import com.genfare.cloud.model.entity.Walletidentifiers;
import com.genfare.cloud.model.entity.Wallets;
import com.genfare.cloud.model.entity.repository.CrossreferencefileRepository;
import com.genfare.cloud.model.entity.repository.LoginsRepository;
import com.genfare.cloud.model.entity.repository.OrderitemsRepository;
import com.genfare.cloud.model.entity.repository.WalletidentifiersRepository;
import com.genfare.cloud.model.entity.repository.WalletsRepository;
import com.genfare.cloud.model.exception.WalletHasPeopleAssignedException;
import com.genfare.cloud.model.security.User;
import com.genfare.cloud.model.types.IdentifiertypesType;
import com.genfare.cloud.model.types.OrderItemStatusType;
import com.genfare.cloud.model.types.OrderitemtypesType;

@Transactional
@Service("walletManagementService")
public class WalletManagementServiceImpl
    implements WalletManagementService {

    private static final Logger log = LoggerFactory.getLogger(UserManagementServiceImpl.class);

    @Autowired
    private WalletidentifiersRepository walletidentifiersRepository;

    @Autowired
    private WalletsRepository walletsRepository;

    @Autowired
    private LoginsRepository loginsRepository;

    @Autowired
    private CrossreferencefileRepository crossreferencefileRepository;

    @Autowired
    private OrderitemsRepository orderitemsRepository;

    // Will assemble wallet from this starting point
    // First hit will be WalletIdentifiers
    @Override
    public Wallets getWallet(String PID)
        throws EntityNotFoundException {

        Walletidentifiers wi = walletidentifiersRepository.findByPID(PID);

        if (wi == null) {
            log.debug("Could not find Wallet Identifier with PID: " + PID);
            throw new EntityNotFoundException("Could not find a wallet identifier with PID: " + PID);
        }

        Wallets w = wi.getWallets();

        log.debug("Found Wallet with PID: " + PID);
        return w;

    }

    @Override
    public Wallets getWallet(String cvvNumber, String cardNumber) {

        Crossreferencefile crossreferencefile =
            crossreferencefileRepository.findByCvvAndPrintedId(cvvNumber, cardNumber);
        if (crossreferencefile == null) {
            log.debug("Could not find Wallet with CVV: " + cvvNumber + " and Card Number " + cardNumber);
            throw new EntityNotFoundException("Could not find Wallet with CVV: " + cvvNumber + " and Card Number "
                + cardNumber);
        }

        Wallets wallets = walletsRepository.findByElectronicId(crossreferencefile.getElectronicId().toString());
        log.debug("Found Wallet with CVV: " + cvvNumber + " and Card Number " + cardNumber);
        return wallets;
    }

    public Logins getLogin(String login)
        throws EntityNotFoundException {
        Logins l = loginsRepository.findByUserLogin(login);

        if (l == null) {
            log.debug("Could not find User with login: " + login);
            throw new EntityNotFoundException("Could not find a user with login: " + login);
        }

        log.debug("Found User with login: " + login);
        return l;
    }

    @Override
    public List<Wallets> getWalletsForUser(User user) {
        return walletsRepository.getWalletsForUser(user.getId());
    }

    @Override
    public boolean hasValidContentsId(User user, String walletContentsId) {
        try {
            if (user == null || walletContentsId == null)
                return false;
            List<Wallets> walletList = walletsRepository.getWalletsForUser(user.getId());
            if (walletList == null)
                return false;
            for (Wallets w : walletList) {
                Set<Walletcontents> set = w.getWalletcontentses();
                for (Walletcontents wc : set) {
                    String id = "" + wc.getWalletContentsId();
                    if (id.equals(walletContentsId))
                        return true;
                }
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public Wallets getWalletForUser(User user, String identifier) {
        return walletidentifiersRepository.getWalletForUser(user.getId(), identifier);
    }

    @Override
    public List<Wallets> sortWallets(Collection<Wallets> wallets) {
        List<Wallets> list = new ArrayList<Wallets>();
        list.addAll(wallets);

        // TODO: Determine best sorting strategy
        Comparator<Wallets> comparator = new Comparator<Wallets>() {

            @Override
            public int compare(Wallets w1, Wallets w2) {
                Long id1 = w1.getWalletId();
                Long id2 = w2.getWalletId();
                return id1.compareTo(id2);
            }
        };

        Collections.sort(list, comparator);
        return list;
    }

    @Override
    public Wallets getWalletByLogin(String login)
        throws EntityNotFoundException {

        Logins l = getLogin(login); // If not found, an error will be thrown

        People p = l.getPeople();

        // TODO add get of wallet searching by People
        return null;
    }

    @Override
    // TODO: saveWallet does nothing!
        public
        void saveWallet(Wallets wallet) {

    }

    @Override
    public BigDecimal getStoredValueContents(Wallets wallet) {
        Set<Walletcontents> contents = new HashSet<Walletcontents>();
        BigDecimal balance = new BigDecimal(0.00);
        if (wallet == null) {
            log.debug("Could not find walletcontents for null wallet");
            return balance;
        } else {
            if (wallet.getWalletcontentses() != null) {
                contents = wallet.getWalletcontentses();
            }
            for (Walletcontents w : contents) {
                Tickets t = w.getTickets();
                if (t.getDescription().equals("Variable Stored Value")) {
                    balance = w.getValueRemaining();
                }
            }
            return balance;
        }
    }

    @Override
    public Wallets registerWallet(User user, String cvvNumber, String cardNumber)
        throws WalletHasPeopleAssignedException, EntityNotFoundException {
        return registerWallet(user, cvvNumber, cardNumber, cardNumber);
    }

    @Override
    public Wallets registerWallet(User user, String cvvNumber, String cardNumber, String nickname)
        throws WalletHasPeopleAssignedException, EntityNotFoundException {
        Wallets wallet = getWallet(cvvNumber, cardNumber);
        if (wallet.getPeople() != null) {
            throw new WalletHasPeopleAssignedException();
        }
        Logins findByUserLogin = loginsRepository.findByUserLogin(user.getLogin());
        People people = findByUserLogin.getPeople();
        wallet.setPeople(people);
        wallet.setIsRegistered(true);
        wallet.setNickname(nickname);
        return walletsRepository.save(wallet);
    }

    @Override
    public String getPrintedId(String walletId) {
        String cardNumber = null;
        if (walletId != null) {
            Long id = Long.valueOf(walletId).longValue();
            Wallets wallet = walletsRepository.findByID(id);
            for (Walletidentifiers i : wallet.getWalletidentifierses()) {
                if (i.getIdentifiertypes().getDescription().equals("Printed ID")) {
                    cardNumber = i.getIdentifier();
                }
            }
        }
        return cardNumber;
    }

    @Override
    public Map<String, List<Walletcontents>> paramaterizeWalletContents(Wallets wallet) {
        Map<String, List<Walletcontents>> current = new HashMap<String, List<Walletcontents>>();

        Set<Walletcontents> contents = wallet.getWalletcontentses();
        Iterator<Walletcontents> it = contents.iterator();
        while (it.hasNext()) {
            Walletcontents content = it.next();
            String key = content.getTickets().getTickettypes().getDescription();

            if (!current.containsKey(key)) {
                List<Walletcontents> list = new ArrayList<Walletcontents>();
                current.put(key, list);
            }
            current.get(key).add(content);
        }

        // Sort the lists so printing to view makes sense
        for (String key : current.keySet()) {
            Collections.sort(current.get(key), new Comparator<Walletcontents>() {
                @Override
                public int compare(Walletcontents wc1, Walletcontents wc2) {
                    return wc1.getWalletContentsId().compareTo(wc2.getWalletContentsId());
                }
            });
        }

        return current;
    }

    @Override
    public Map<String, List<Orderitems>> paramaterizePendingItems(Wallets wallet) {
        Map<String, List<Orderitems>> processing = new HashMap<String, List<Orderitems>>();

        Byte[] validStatuses =
        { OrderItemStatusType.FULFILLMENT.getIndex().byteValue(),
            OrderItemStatusType.REJECTED.getIndex().byteValue(),
            OrderItemStatusType.SUBMITTED.getIndex().byteValue() };
        Collection<Byte> setList = new HashSet<Byte>(Arrays.asList(validStatuses));

        List<Orderitems> items = orderitemsRepository.findByWalletAndStatusSet(wallet.getWalletId(), setList);
        log.debug("ORDERITEMS: " + items.size());
        for (Orderitems item : items) {
            if ((item.getOrderitemtypes().getOrderItemTypeId() == OrderitemtypesType.TICKET.getType().getOrderItemTypeId())
                && !item.getOrders().getOrdertypes().getDescription().equals("Service Order")) {
                String key = item.getTickets().getTickettypes().getDescription();

                if (!processing.containsKey(key)) {
                    List<Orderitems> list = new ArrayList<Orderitems>();
                    processing.put(key, list);
                }
                processing.get(key).add(item);
            }
        }

        for (String key : processing.keySet()) {
            Collections.sort(processing.get(key), new Comparator<Orderitems>() {
                @Override
                public int compare(Orderitems oi1, Orderitems oi2) {
                    // TODO: Determine if this is best sorting mechanism

                    return oi1.getTouchedAt().compareTo(oi2.getTouchedAt());
                }
            });
        }

        return processing;
    }

    @Override
    public int getPendingItemsCount(Walletcontents wc) {
        Collection<Byte> setList = new HashSet<Byte>();
        setList.add(OrderItemStatusType.FULFILLMENT.getIndex().byteValue());
        setList.add(OrderItemStatusType.SUBMITTED.getIndex().byteValue());

        Wallets w = wc.getWallets();
        List<Orderitems> items = orderitemsRepository.findByWalletAndStatusSet(w.getWalletId(), setList);
        log.debug("ORDERITEMS: " + items.size());
        int count = 0;
        for (Orderitems item : items) {

            Tickets t = item.getTickets();
            if (null != t) {
                Integer ti = item.getTickets().getTicketId();
                Integer wcti = wc.getTickets().getTicketId();
                if (ti == wcti)
                    count++;
            }
        }

        return count;
    }

    @Override
    public List<Orderitems> getPendingItems(Walletcontents wc) {
        Collection<Byte> setList = new HashSet<Byte>();
        setList.add(OrderItemStatusType.FULFILLMENT.getIndex().byteValue());
        setList.add(OrderItemStatusType.SUBMITTED.getIndex().byteValue());

        Wallets w = wc.getWallets();
        List<Orderitems> items = orderitemsRepository.findByWalletAndTicketAndStatusSet(w.getWalletId(), wc.getTickets().getTicketId(), setList);
        log.debug("ORDERITEMS: " + items.size());

        return items;
    }

    @Override
    public boolean walletVisibleForUser(Wallets wallet, User user) {
        if (wallet == null) {
            return false;
        }
        if (wallet.getPeople() != null) {
            if (user == null || !wallet.getPeople().getPersonId().equals(user.getId())) {
                // Wallet is registered to another user, current user/guest cannot see it
                return false;
            }
        }
        return true;
    }

    @Override
    public Map<String, String> getWalletSet(User user) {
        Map<String, String> map = new TreeMap<String, String>();
        List<Wallets> wList = getWalletsForUser(user);
        for (Wallets w : wList) {
            Set<Walletidentifiers> set = w.getWalletidentifierses();
            for (Walletidentifiers id : set) {
                if (IdentifiertypesType.PRINTED_ID.getIndex().byteValue() == id.getIdentifiertypes().getIdentifierId().byteValue()) {
                    String strID = id.getIdentifier();
                    map.put(strID, w.getNickname());
                }
            }
        }
        return map;
    }
}
