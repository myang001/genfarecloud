package com.genfare.cloud.model.entity;

// Generated Mar 19, 2014 9:47:20 AM by Hibernate Tools 4.0.0

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class TicketsFarecodesId
    implements java.io.Serializable {

    private Integer ticketId;

    private Byte farecodeId;

    public TicketsFarecodesId() {
    }

    public TicketsFarecodesId(Integer ticketId, Byte farecodeId) {
        this.ticketId = ticketId;
        this.farecodeId = farecodeId;
    }

    @Column(name = "TicketID", nullable = false)
    public Integer getTicketId() {
        return this.ticketId;
    }

    public void setTicketId(Integer ticketId) {
        this.ticketId = ticketId;
    }

    @Column(name = "FarecodeID", nullable = false)
    public Byte getFarecodeId() {
        return this.farecodeId;
    }

    public void setFarecodeId(Byte farecodeId) {
        this.farecodeId = farecodeId;
    }

    @Override
    public boolean equals(Object other) {
        if ((this == other))
            return true;
        if ((other == null))
            return false;
        if (!(other instanceof TicketsFarecodesId))
            return false;
        TicketsFarecodesId castOther = (TicketsFarecodesId) other;

        return (this.getTicketId() == castOther.getTicketId())
            && (this.getFarecodeId() == castOther.getFarecodeId());
    }

    @Override
    public int hashCode() {
        Integer result = 17;

        result = 37 * result + this.getTicketId();
        result = 37 * result + this.getFarecodeId();
        return result;
    }

}
