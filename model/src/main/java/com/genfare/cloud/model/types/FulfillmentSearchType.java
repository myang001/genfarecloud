package com.genfare.cloud.model.types;

public enum FulfillmentSearchType {
    ALL(1),
    PENDING_AUTOLOAD(2),
    PENDING_FULFILLMENT(3),
    COMPLETED(4),
    CANCELED(5);

    private final Integer index;

    private FulfillmentSearchType(Integer index) {
        this.index = index;
    }

    public Integer getIndex() {
        return index;
    }

    public String getCasedName() {
        StringBuffer name = new StringBuffer(this.name().toLowerCase());
        name.setCharAt(0, Character.toUpperCase(name.charAt(0)));
        if (name.indexOf("_") >= 0) {

            for (int i = 0; i < name.length(); i++) {
                if (name.charAt(i) == '_') {
                    name.setCharAt(i + 1, Character.toUpperCase(name.charAt(i + 1)));
                    name.replace(i, i + 1, " ");
                }
            }
        }
        return name.toString();
    }
}
