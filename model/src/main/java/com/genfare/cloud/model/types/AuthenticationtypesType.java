package com.genfare.cloud.model.types;

import java.util.EnumSet;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.genfare.cloud.model.entity.Authenticationtypes;
import com.genfare.cloud.model.entity.repository.AuthenticationtypesRepository;

public enum AuthenticationtypesType {
    PASSWORD(1),
    EMAIL_TOKEN(3),
    PASSWORD_RECOVERY(5);

    private final Integer index;

    private Authenticationtypes type;

    private AuthenticationtypesType(Integer index) {
        this.index = index;
    }

    public Integer getIndex() {
        return index;
    }

    public Authenticationtypes getType() {
        return type;
    }

    @Component
    public static class Injector {
        @Autowired
        private AuthenticationtypesRepository repo;

        @PostConstruct
        public void setup() {
            for (AuthenticationtypesType type : EnumSet.allOf(AuthenticationtypesType.class)) {
                type.type = repo.findOne(type.index);
            }
        }
    }
}
