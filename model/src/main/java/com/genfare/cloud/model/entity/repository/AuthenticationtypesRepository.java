package com.genfare.cloud.model.entity.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.genfare.cloud.model.entity.Authenticationtypes;

public interface AuthenticationtypesRepository
    extends CrudRepository<Authenticationtypes, Integer> {

    @Query("from Authenticationtypes authtype inner join authtype.authentications auth where auth.id.personId = ?")
    public List<Authenticationtypes> findSecturityQuestionsByPeopleID(Integer id);

    @Query("from Authenticationtypes authtype where authtype.description = 'Security Question'")
    public List<Authenticationtypes> findSecurityQuestions();

    @Query("from Authenticationtypes authtype where authtype.description = 'Security Question' and authtype.prompt = :prompt")
    public Authenticationtypes findSecurityQuestionByPrompt(@Param("prompt") String prompt);

    @Query("from Authenticationtypes authtype where authtype.description = 'Password'")
    public Authenticationtypes findPassowrdType();

    @Query("from Authenticationtypes authtype where authtype.description = 'Email Verification Token'")
    public Authenticationtypes findEmailVerificationTokenType();
}
