package com.genfare.cloud.model.entity.repository;

import org.springframework.data.repository.CrudRepository;

import com.genfare.cloud.model.entity.Thresholdsteps;

public interface ThresholdstepsRepository
    extends CrudRepository<Thresholdsteps, Integer> {

}
