package com.genfare.cloud.model.entity.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.genfare.cloud.model.entity.Tickets;

public interface TicketsRepository
    extends CrudRepository<Tickets, Integer> {

    @Query("from Tickets t inner join t.walletcontentses wc where wc.wallets.walletId = ?")
    public Iterable<Tickets> getTicketsByWalletId(Long walletId);
}