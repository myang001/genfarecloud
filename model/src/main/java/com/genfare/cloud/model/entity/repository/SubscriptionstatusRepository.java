package com.genfare.cloud.model.entity.repository;

import org.springframework.data.repository.CrudRepository;

import com.genfare.cloud.model.entity.Subscriptionstatus;

public interface SubscriptionstatusRepository
    extends CrudRepository<Subscriptionstatus, Byte> {

}
