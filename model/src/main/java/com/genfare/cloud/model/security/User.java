package com.genfare.cloud.model.security;

import java.util.Date;
import java.util.LinkedHashSet;
import java.util.Set;

import com.genfare.cloud.model.contact.Address;
import com.genfare.cloud.model.contact.Email;
import com.genfare.cloud.model.contact.Phone;
import com.genfare.cloud.model.entity.Farecodes;
import com.genfare.cloud.model.entity.Groups;
import com.genfare.cloud.model.types.GenderType;
import com.genfare.cloud.model.types.LanguagesType;
import com.genfare.cloud.model.types.PeopleStatusType;

/**
 * Facade class to represent a User in the system
 * 
 * @author jyore
 */
public class User {

    private final Integer id;
    private final String login;
    private final String prefix;
    private final String firstName;
    private final String middleName;
    private final String lastName;
    private final String suffix;
    private final Date dateOfBirth;
    private final PeopleStatusType status;
    private final GenderType gender;
    private final LanguagesType language;
    private final Boolean alert;
    private final String employeeId;
    private final String jobTitle;
    private final ContactMethodEnum primaryContactMethod;
    private final Email email;
    private final Phone phone;
    private final Address address;
    private final Set<Groups> groups;
    private final Set<Farecodes> farecodes;

    private User(UserBuilder builder) {
        this.id = builder.id;
        this.login = builder.login;
        this.prefix = builder.prefix;
        this.firstName = builder.firstName;
        this.middleName = builder.middleName;
        this.lastName = builder.lastName;
        this.suffix = builder.suffix;
        this.dateOfBirth = builder.dateOfBirth;
        this.status = builder.status;
        this.gender = builder.gender;
        this.language = builder.language;
        this.alert = builder.alert;
        this.employeeId = builder.employeeId;
        this.jobTitle = builder.jobTitle;
        this.primaryContactMethod = builder.primaryContactMethod;
        this.email = builder.email;
        this.phone = builder.phone;
        this.address = builder.address;
        this.groups = builder.groups;
        this.farecodes = builder.farecodes;
    }

    /* Getters */

    public Integer getId() {
        return id;
    }

    public String getLogin() {
        return login;
    }

    public String getPrefix() {
        return prefix;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getSuffix() {
        return suffix;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public PeopleStatusType getStatus() {
        return status;
    }

    public GenderType getGender() {
        return gender;
    }

    public LanguagesType getLanguage() {
        return language;
    }

    public Boolean getAlert() {
        return alert;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public ContactMethodEnum getPrimaryContactMethod() {
        return primaryContactMethod;
    }

    public Email getEmail() {
        return email;
    }

    public Phone getPhone() {
        return phone;
    }

    public Address getAddress() {
        return address;
    }

    public Set<Groups> getGroups() {
        return groups;
    }

    public Set<Farecodes> getFarecodes() {
        return farecodes;
    }

    /**
     * Builder class to build a new user
     * 
     * @see <a href="http://en.wikipedia.org/wiki/Builder_pattern">Builder Patter</a>
     * 
     * @author jyore
     * 
     */
    public static class UserBuilder {
        private final Integer id;
        private final String login;
        private String prefix = "";
        private final String firstName;
        private String middleName = "";
        private final String lastName;
        private String suffix = "";
        private Date dateOfBirth = null;
        private PeopleStatusType status = PeopleStatusType.ACTIVE;
        private GenderType gender = GenderType.UNSPECIFIED;
        private LanguagesType language = LanguagesType.ENGLISH;
        private Boolean alert = false;
        private String employeeId = null;
        private String jobTitle = null;
        private ContactMethodEnum primaryContactMethod = ContactMethodEnum.EMAIL;
        private final Email email;
        private final Phone phone;
        private final Address address;
        private final Set<Groups> groups = new LinkedHashSet<Groups>();
        private final Set<Farecodes> farecodes = new LinkedHashSet<Farecodes>();

        /**
         * Create a builder with the required parameters
         * 
         * @param id The id of the user
         * @param login The login name of the user
         * @param firstName The user's first name
         * @param lastName The users's last name
         * @param email The email address for the user
         * @param phone The phone number for the user
         * @param address The <i>shipping</i> address of the user
         */
        public UserBuilder(Integer id, String login, String firstName,
            String lastName, Email email, Phone phone, Address address) {
            this.id = id;
            this.login = login;
            this.firstName = firstName;
            this.lastName = lastName;
            this.email = email;
            this.phone = phone;
            this.address = address;
        }

        /**
         * Create a builder with the required parameters copied from a user
         * 
         * @param user
         */
        public UserBuilder(User user) {
            this.id = user.getId();
            this.login = user.getLogin();
            this.firstName = user.getFirstName();
            this.lastName = user.getLastName();
            this.email = user.getEmail();
            this.phone = user.getPhone();
            this.address = user.getAddress();
            this.alert = user.getAlert();
            this.dateOfBirth = user.getDateOfBirth();
            this.employeeId = user.getEmployeeId();
            this.farecodes.addAll(user.getFarecodes());
            this.gender = user.getGender();
            this.groups.addAll(user.getGroups());
            this.jobTitle = user.getJobTitle();
            this.language = user.getLanguage();
            this.middleName = user.getMiddleName();
            this.prefix = user.getPrefix();
            this.primaryContactMethod = user.getPrimaryContactMethod();
            this.status = user.getStatus();
            this.suffix = user.getSuffix();
        }

        /**
         * Create a builder with the optional parameters copied from a user
         * 
         * @param user
         * @param login
         * @param firstName
         * @param lastName
         * @param email
         * @param phone
         * @param address
         */
        public UserBuilder(User user, String login, String firstName,
            String lastName, Email email, Phone phone, Address address) {
            this.id = user.getId();
            this.login = login;
            this.firstName = firstName;
            this.lastName = lastName;
            this.email = email;
            this.phone = phone;
            this.address = address;
            this.alert = user.getAlert();
            this.dateOfBirth = user.getDateOfBirth();
            this.employeeId = user.getEmployeeId();
            this.farecodes.addAll(user.getFarecodes());
            this.gender = user.getGender();
            this.groups.addAll(user.getGroups());
            this.jobTitle = user.getJobTitle();
            this.language = user.getLanguage();
            this.middleName = user.getMiddleName();
            this.prefix = user.getPrefix();
            this.primaryContactMethod = user.getPrimaryContactMethod();
            this.status = user.getStatus();
            this.suffix = user.getSuffix();
        }

        /**
         * Update the prefix name for the user
         * 
         * @param prefix
         * @return A reference back to the builder
         */
        public UserBuilder prefix(String prefix) {
            this.prefix = prefix;
            return this;
        }

        /**
         * Update the middle name for the user
         * 
         * @param middleName
         * @return A reference back to the builder
         */
        public UserBuilder middleName(String middleName) {
            this.middleName = middleName;
            return this;
        }

        /**
         * Update the suffix name for the user
         * 
         * @param suffix
         * @return A reference back to the builder
         */
        public UserBuilder suffix(String suffix) {
            this.suffix = suffix;
            return this;
        }

        /**
         * Update the date of birth for the user
         * 
         * @param dateOfBirth
         * @return A reference back to the builder
         */
        public UserBuilder dateOfBirth(Date dateOfBirth) {
            this.dateOfBirth = dateOfBirth;
            return this;
        }

        /**
         * Update the status of a user
         * 
         * @param status
         * @return A reference back to the builder
         */
        public UserBuilder status(PeopleStatusType status) {
            this.status = status;
            return this;
        }

        /**
         * Update the gender of a user
         * 
         * @param gender
         * @return A reference back to the builder
         */
        public UserBuilder gender(GenderType gender) {
            this.gender = gender;
            return this;
        }

        /**
         * update the language of a user
         * 
         * @param language
         * @return A reference back to the builder
         */
        public UserBuilder language(LanguagesType language) {
            this.language = language;
            return this;
        }

        /**
         * Update the alert of a user
         * 
         * @param alert
         * @return A reference back to the builder
         */
        public UserBuilder alert(Boolean alert) {
            this.alert = alert;
            return this;
        }

        /**
         * Update the employee id of a user
         * 
         * @param employeeId
         * @return A reference back to the builder
         */
        public UserBuilder employeeId(String employeeId) {
            this.employeeId = employeeId;
            return this;
        }

        /**
         * Update the job title of the user
         * 
         * @param jobTitle
         * @return A reference back to the builder
         */
        public UserBuilder jobTitle(String jobTitle) {
            this.jobTitle = jobTitle;
            return this;
        }

        /**
         * Update the primary contact method of the user
         * 
         * @param primaryContactMethod
         * @return A reference back to the builder
         */
        public UserBuilder primaryContactMethod(
            ContactMethodEnum primaryContactMethod) {
            this.primaryContactMethod = primaryContactMethod;
            return this;
        }

        /**
         * Update the groups of a user
         * 
         * @param groups
         * @return A reference back to the builder
         */
        public UserBuilder groups(Set<Groups> groups) {
            this.groups.addAll(groups);
            return this;
        }

        /**
         * Update the farecodes of a user
         * 
         * @param farecodes
         * @return A reference back to the builder
         */
        public UserBuilder farecodes(Set<Farecodes> farecodes) {
            this.farecodes.addAll(farecodes);
            return this;
        }

        /**
         * Build the user object
         * 
         * @return The built user object
         */
        public User build() {
            return new User(this);
        }
    }

}
