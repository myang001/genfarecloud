package com.genfare.cloud.model.entity.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.genfare.cloud.model.entity.Modules;

public interface ModulesRepository
    extends CrudRepository<Modules, Byte> {

    @Query("from Modules m where m.name = ?")
    public Modules findByName(String name);
}
