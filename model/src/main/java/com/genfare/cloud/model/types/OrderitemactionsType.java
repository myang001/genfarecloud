package com.genfare.cloud.model.types;

import java.util.EnumSet;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.genfare.cloud.model.entity.Orderitemactions;
import com.genfare.cloud.model.entity.repository.OrderitemactionsRepository;

public enum OrderitemactionsType {
    NEW(1),
    REPLACE(2),
    CHANGE(3),
    CANCEL(4);

    private final Integer index;

    private Orderitemactions action;

    private OrderitemactionsType(Integer index) {
        this.index = index;
    }

    public Integer getIndex() {
        return index;
    }

    public Orderitemactions getType() {
        return action;
    }

    @Component
    public static class Injector {
        @Autowired
        private OrderitemactionsRepository repo;

        @PostConstruct
        public void setup() {
            for (OrderitemactionsType action : EnumSet.allOf(OrderitemactionsType.class)) {
                action.action = repo.findOne(action.index.byteValue());
            }
        }
    }
}
