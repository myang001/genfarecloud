package com.genfare.cloud.model.service;

import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.map.CaseInsensitiveMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.genfare.cloud.model.entity.Orders;
import com.genfare.cloud.model.entity.Peoplecards;
import com.genfare.cloud.model.security.User;
import com.genfare.cloud.model.tenant.TenantConfiguration;
import com.genfare.cloud.model.util.Contact;
import com.genfare.cloud.model.util.CreditServiceUtils;
import com.genfare.cloud.model.util.OrderPayment;
import com.genfare.cloud.model.util.UserBilling;

@Service
public class CreditServiceServiceImpl
    implements CreditServiceService {

    @Autowired
    private List<CreditServiceUtils> utilList;

    @Autowired
    private TenantConfiguration tenantConfig;

    private static final CaseInsensitiveMap utils = new CaseInsensitiveMap();

    @PostConstruct
    public void setup() {
        // Map has to be built after the class hass been constructed to avoid NPE on construct
        for (CreditServiceUtils util : utilList) {
            utils.put(util.getServiceName(), util);
        }
    }

    private CreditServiceUtils getUtil()
        throws UnsupportedOperationException {
        String service = tenantConfig.getCreditServiceName();
        if (utils.containsKey(service)) {
            return (CreditServiceUtils) utils.get(service);
        }

        throw new UnsupportedOperationException("Invalid Credit Service Name '" + service + "' in Tenant Configuration");
    }

    @Override
    public String getServiceName()
        throws UnsupportedOperationException {
        return getUtil().getServiceName();
    }

    @Override
    public Map<String, Object> getAuthOnlyFormData(User user, String returnContext, String userData)
        throws UnsupportedOperationException {
        return getUtil().getAuthOnlyFormData(user, returnContext, userData);
    }

    @Override
    public Map<String, Object> getFormData(Orders order, User user, String returnContext, String userData)
        throws UnsupportedOperationException {
        return getUtil().getFormData(order, user, returnContext, userData);
    }

    @Override
    public boolean validatePost(HttpServletRequest request, String relayResponseKey)
        throws UnsupportedOperationException {
        return getUtil().validatePost(request, relayResponseKey);
    }

    @Override
    public OrderPayment getOrderPayment(HttpServletRequest request) {
        return getUtil().getOrderPayment(request);
    }

    @Override
    public Contact getContact(HttpServletRequest request) {
        return getUtil().getContact(request);
    }

    @Override
    public UserBilling getUserBilling(HttpServletRequest request) {
        return getUtil().getUserBilling(request);
    }

    @Override
    public String getForwardToParam() {
        return getUtil().getForwardToParam();
    }

    @Override
    public String getUserDataField() {
        return getUtil().getUserDataField();
    }

    @Override
    public boolean isAuthOnly(HttpServletRequest request) {
        return getUtil().isAuthOnly(request);
    }

    @Override
    public Peoplecards createPeopleCard(User user, HttpServletRequest request) {
        return getUtil().createPeopleCard(user, request);
    }

    @Override
    public String getCardType(HttpServletRequest request) {
        return getUtil().getCardType(request);
    }
}
