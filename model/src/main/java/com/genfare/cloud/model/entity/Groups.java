package com.genfare.cloud.model.entity;

// Generated Mar 19, 2014 9:47:20 AM by Hibernate Tools 4.0.0

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.genfare.cloud.model.entity.inter.Auditable;
import com.genfare.cloud.model.listener.AuditListener;

/**
 * Groups generated by hbm2java
 */
@Entity
@Table(name = "groups")
@EntityListeners(AuditListener.class)
public class Groups implements java.io.Serializable, Auditable {

    private static final long serialVersionUID = 1L;

    private Integer groupId;

    private Organizations organizations;

    private String role;

    private String name;

    private String description;

    private GroupStatus status;

    private String touchedBy;

    private Date touchedAt;

    private String createdBy;

    private Date createdAt;

    private Set<Groupprivileges> groupprivilegeses = new HashSet<Groupprivileges>(
        0);

    private Set<Groupsofpeople> groupsofpeoples = new HashSet<Groupsofpeople>(0);

    private Set<People> people = new HashSet<People>();

    private Set<Privileges> privileges = new HashSet<Privileges>(0);

    private Set<Equipmentcollections> equipmentcollectionss;

    public Groups() {
    }

    public Groups(String role, String name, String description) {
        this.role = role;
        this.name = name;
        this.description = description;
    }

    public Groups(Organizations organizations, String role, String name,
        String description, Set<Groupprivileges> groupprivilegeses,
        Set<Groupsofpeople> groupsofpeoples, Set<Privileges> privileges) {
        this.organizations = organizations;
        this.role = role;
        this.name = name;
        this.description = description;
        this.groupprivilegeses = groupprivilegeses;
        this.groupsofpeoples = groupsofpeoples;
        this.setPrivileges(privileges);
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "GroupID", unique = true, nullable = false)
    public Integer getGroupId() {
        return this.groupId;
    }

    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "OrganizationID")
    public Organizations getOrganizations() {
        return this.organizations;
    }

    public void setOrganizations(Organizations organizations) {
        this.organizations = organizations;
    }

    @Column(name = "Description", length = 50)
    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "Status", nullable = false)
    public GroupStatus getStatus() {
        return status;
    }

    public void setStatus(GroupStatus status) {
        this.status = status;
    }

    @Column(name = "Role", unique = true, nullable = false, length = 50)
    public String getRole() {
        return this.role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Column(name = "Name", nullable = false, length = 50)
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "TouchedBy", nullable = false, length = 50)
    public String getTouchedBy() {
        return this.touchedBy;
    }

    @Override
    public void setTouchedBy(String touchedBy) {
        this.touchedBy = touchedBy;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "TouchedAt", nullable = false, length = 19)
    public Date getTouchedAt() {
        return this.touchedAt;
    }

    @Override
    public void setTouchedAt(Date touchedAt) {
        this.touchedAt = touchedAt;
    }

    @Override
    @Column(name = "CreatedBy", nullable = false, length = 50)
    public String getCreatedBy() {
        return this.createdBy;
    }

    @Override
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Override
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CreatedAt", nullable = false, length = 19)
    public Date getCreatedAt() {
        return this.createdAt;
    }

    @Override
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "groups", cascade = CascadeType.ALL)
    public Set<Groupprivileges> getGroupprivilegeses() {
        return this.groupprivilegeses;
    }

    public void setGroupprivilegeses(Set<Groupprivileges> groupprivilegeses) {
        this.groupprivilegeses = groupprivilegeses;
    }

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "groups", cascade = CascadeType.ALL)
    public Set<Groupsofpeople> getGroupsofpeoples() {
        return this.groupsofpeoples;
    }

    public void setGroupsofpeoples(Set<Groupsofpeople> groupsofpeoples) {
        this.groupsofpeoples = groupsofpeoples;
    }

    @ManyToMany(mappedBy = "groups", cascade = CascadeType.ALL)
    public Set<People> getPeople() {
        return people;
    }

    public void setPeople(Set<People> people) {
        this.people = people;
    }

    @ManyToMany(cascade = { CascadeType.ALL })
    @JoinTable(name = "groupprivileges", joinColumns = { @JoinColumn(name = "GroupID") }, inverseJoinColumns = { @JoinColumn(name = "PrivilegeID") })
    public Set<Privileges> getPrivileges() {
        return privileges;
    }

    public void setPrivileges(Set<Privileges> privileges) {
        this.privileges = privileges;
    }

    @ManyToMany(cascade = { CascadeType.ALL })
    @JoinTable(name = "groupsequipmentcollections", joinColumns = { @JoinColumn(name = "EquipmentCollectionID") }, inverseJoinColumns = { @JoinColumn(name = "GroupID") })
    public Set<Equipmentcollections> getEquipmentcollectionss() {
        return equipmentcollectionss;
    }

    public void setEquipmentcollectionss(
        Set<Equipmentcollections> equipmentcollectionss) {
        this.equipmentcollectionss = equipmentcollectionss;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((role == null) ? 0 : role.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Groups other = (Groups) obj;
        if (role == null) {
            if (other.role != null)
                return false;
        } else if (!role.equals(other.role))
            return false;
        return true;
    }
}
