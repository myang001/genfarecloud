package com.genfare.cloud.model.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.genfare.cloud.model.entity.inter.Auditable;
import com.genfare.cloud.model.listener.AuditListener;

@Entity
@Table(name = "equipmentstatuses")
@EntityListeners(AuditListener.class)
public class EquipmentStatuses
    implements java.io.Serializable, Auditable {

    private Byte status;

    private String description;

    private String touchedBy;

    private Date touchedAt;

    private String createdBy;

    private Date createdAt;

    public EquipmentStatuses() {

    }

    public EquipmentStatuses(String description) {
        this.description = description;
    }

    public EquipmentStatuses(Byte status, String description, String touchedBy, Date touchedAt, String createdBy,
        Date createdAt) {
        this.status = status;
        this.description = description;
        this.touchedBy = touchedBy;
        this.touchedAt = touchedAt;
        this.createdBy = createdBy;
        this.createdAt = createdAt;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "Status", unique = true, nullable = false)
    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    @Column(name = "Description", nullable = false, length = 50)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name = "TouchedBy", nullable = false, length = 50)
    public String getTouchedBy() {
        return touchedBy;
    }

    public void setTouchedBy(String touchedBy) {
        this.touchedBy = touchedBy;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "TouchedAt", nullable = false, length = 19)
    public Date getTouchedAt() {
        return touchedAt;
    }

    public void setTouchedAt(Date touchedAt) {
        this.touchedAt = touchedAt;
    }

    @Column(name = "CreatedBy", nullable = false, length = 50)
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CreatedAt", nullable = false, length = 19)
    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

}
