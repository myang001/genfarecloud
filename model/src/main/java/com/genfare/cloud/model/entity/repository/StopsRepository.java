package com.genfare.cloud.model.entity.repository;

import org.springframework.data.repository.CrudRepository;

import com.genfare.cloud.model.entity.Stops;

public interface StopsRepository
    extends CrudRepository<Stops, Integer> {

}
