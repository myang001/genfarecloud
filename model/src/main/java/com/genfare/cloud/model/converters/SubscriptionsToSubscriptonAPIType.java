package com.genfare.cloud.model.converters;

import org.springframework.core.convert.converter.Converter;

import com.genfare.cloud.model.entity.Subscriptions;
import com.genfare.cloud.model.om.ObjectFactory;
import com.genfare.cloud.model.om.SubscriptionAPIType;

public class SubscriptionsToSubscriptonAPIType implements Converter<Subscriptions, SubscriptionAPIType> {

    @Override
    public SubscriptionAPIType convert(Subscriptions subscription) {
        ObjectFactory of = new ObjectFactory();
        SubscriptionAPIType type = of.createSubscriptionAPIType();
        type.setId(subscription.getSubscriptionId().toString());
        return type;
    }

}
