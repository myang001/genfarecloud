/**
 *
 */
package com.genfare.cloud.model.converters;

import org.springframework.core.convert.converter.Converter;

import com.genfare.cloud.model.entity.Orderitems;
import com.genfare.cloud.model.om.ObjectFactory;
import com.genfare.cloud.model.om.OrderItemType;

/**
 * @author mpeter
 */
public class OrderItemsToOrderItemType
    implements Converter<Orderitems, OrderItemType> {

    @Override
    public OrderItemType convert(Orderitems oi) {

        ObjectFactory of = new ObjectFactory();
        OrderItemType oit = of.createOrderItemType();
        if (oi.getId() != null) {
            oit.setOrderItemNumber(oi.getId().getOrderItemNumber());
        }
        if (oi.getOrderitemactions() != null) {
            oit.setAction(oi.getOrderitemactions().getDescription());
        }
        if (oi.getAmount() != null) {
            oit.setAmount(oi.getAmount().floatValue());
        }
        if (oi.getAmountTax() != null) {
            oit.setAmountTax(oi.getAmountTax().floatValue());
        }
        if (oi.getFarecodes() != null) {
            FarecodesToFarecodeType dtc = new FarecodesToFarecodeType();
            oit.setFarecode(dtc.convert(oi.getFarecodes()));
        }
        oit.setStatus(oi.getOrderstatuses().getDescription());
        if (oi.getUnitPrice() != null) {
            oit.setUnitPrice(oi.getUnitPrice().floatValue());
        }
        if (oi.getWallets() != null) {
            WalletsToWalletType wtc = new WalletsToWalletType();
            oit.setWallet(wtc.convert(oi.getWallets()));
        }
        if (oi.getTickets() != null) {
            TicketsToTicketType ttt = new TicketsToTicketType();
            oit.setTicket(ttt.convert(oi.getTickets()));
        }
        if (oi.getWallettypes() != null) {
            WallettypesToWallettypeType wtt = new WallettypesToWallettypeType();
            oit.setWallettype(wtt.convert(oi.getWallettypes()));
        }
        if (oi.getOfferings() != null) {
            OfferingsToOfferingTypeConverter ott = new OfferingsToOfferingTypeConverter();
            oit.setOffering(ott.convert(oi.getOfferings()));
        }
        if (oi.getParentItemNumber() != null) {
            oit.setParentItemNumber(oi.getParentItemNumber().intValue());
        } else {
            oit.setParentItemNumber(oi.getId().getOrderItemNumber().intValue());
        }
        if (oi.getQuantity() != null) {
            oit.setQuantity(oi.getQuantity().intValue());
        }
        if (oi.getOrderstatuses() != null) {
            oit.setStatus(oi.getOrderstatuses().getDescription());
        }
        if (oi.getUnitPrice() != null) {
            oit.setUnitPrice(oi.getUnitPrice().floatValue());
        }
        return oit;
    }
}
