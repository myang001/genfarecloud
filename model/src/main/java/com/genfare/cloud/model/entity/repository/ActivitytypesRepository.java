package com.genfare.cloud.model.entity.repository;

import org.springframework.data.repository.CrudRepository;

import com.genfare.cloud.model.entity.Activitytypes;

public interface ActivitytypesRepository
    extends CrudRepository<Activitytypes, Byte> {
}
