package com.genfare.cloud.model.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.genfare.cloud.model.entity.inter.Auditable;
import com.genfare.cloud.model.listener.AuditListener;

@Entity
@Table(name = "organizationstatus")
@EntityListeners(AuditListener.class)
public class OrganizationStatus implements java.io.Serializable, Auditable {

    private Byte status;
    private String description;
    private String touchedBy;
    private Date touchedAt;
    private String createdBy;
    private Date createdAt;
    private Set<Organizations> organizations = new HashSet<Organizations>(0);

    public OrganizationStatus() {

    }

    public OrganizationStatus(String description) {
        this.description = description;
    }

    public OrganizationStatus(String description, Set<Organizations> organizations) {
        this.description = description;
        this.organizations = organizations;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "Status", unique = true, nullable = false)
    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    @Column(name = "Description", nullable = false, length = 50)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name = "TouchedBy", nullable = false, length = 50)
    public String getTouchedBy() {
        return touchedBy;
    }

    public void setTouchedBy(String touchedBy) {
        this.touchedBy = touchedBy;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "TouchedAt", nullable = false, length = 19)
    public Date getTouchedAt() {
        return touchedAt;
    }

    public void setTouchedAt(Date touchedAt) {
        this.touchedAt = touchedAt;
    }

    @Column(name = "CreatedBy", nullable = false, length = 50)
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CreatedAt", nullable = false, length = 19)
    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "status")
    public Set<Organizations> getOrganizations() {
        return organizations;
    }

    public void setOrganizations(Set<Organizations> organizations) {
        this.organizations = organizations;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((status == null) ? 0 : status.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        OrganizationStatus other = (OrganizationStatus) obj;
        if (status == null) {
            if (other.status != null)
                return false;
        } else if (!status.equals(other.status))
            return false;
        return true;
    }

}
