package com.genfare.cloud.model.util;

import java.util.ArrayList;
import java.util.List;

public class SearchUtil {

    public static <T> List<T> and(List<List<T>> results) {

        if (results.isEmpty()) {
            return null;
        }

        List<T> ret = new ArrayList<T>();

        for (List<T> r : results) {
            if (r.isEmpty()) {
                continue;
            }

            if (ret.isEmpty()) {
                ret = r;
            } else {
                ret = ListUtil.intersection(ret, r);
            }
        }

        return ret;
    }

    public static <T> List<T> or(List<List<T>> results) {

        if (results.isEmpty()) {
            return null;
        }

        List<T> ret = new ArrayList<T>();

        for (List<T> r : results) {
            ret = ListUtil.union(ret, r);
        }

        return ret;
    }
}
