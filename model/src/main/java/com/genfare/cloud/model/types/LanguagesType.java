package com.genfare.cloud.model.types;

import java.util.EnumSet;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.genfare.cloud.model.entity.Languages;
import com.genfare.cloud.model.entity.repository.LanguagesRepository;

public enum LanguagesType {
    ENGLISH(1),
    FRENCH(2),
    GERMAN(3);

    private final Integer index;

    private Languages type;

    private LanguagesType(Integer index) {
        this.index = index;
    }

    public Integer getIndex() {
        return index;
    }

    public Languages getType() {
        return type;
    }

    public static Languages getType(Byte s) {
        for (LanguagesType type : LanguagesType.values()) {
            if (s == type.index.byteValue()) {
                return type.type;
            }
        }
        throw new IllegalArgumentException("illegal group type index: " + s);
    }

    public static LanguagesType getEnumType(Byte s) {
        for (LanguagesType type : LanguagesType.values()) {
            if (s == type.index.byteValue()) {
                return type;
            }
        }
        throw new IllegalArgumentException("illegal group type index: " + s);
    }

    @Component
    public static class Injector {
        @Autowired
        private LanguagesRepository repo;

        @PostConstruct
        public void setup() {
            for (LanguagesType type : EnumSet.allOf(LanguagesType.class)) {
                type.type = repo.findOne(type.index.byteValue());
            }
        }
    }
}
