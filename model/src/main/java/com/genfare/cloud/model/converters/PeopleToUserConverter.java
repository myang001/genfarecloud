package com.genfare.cloud.model.converters;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;

import com.genfare.cloud.model.contact.Address;
import com.genfare.cloud.model.contact.Email;
import com.genfare.cloud.model.contact.Phone;
import com.genfare.cloud.model.entity.Logins;
import com.genfare.cloud.model.entity.People;
import com.genfare.cloud.model.entity.Peopleaddresses;
import com.genfare.cloud.model.entity.Peoplelinks;
import com.genfare.cloud.model.security.ContactMethodEnum;
import com.genfare.cloud.model.security.User;
import com.genfare.cloud.model.security.User.UserBuilder;
import com.genfare.cloud.model.types.AddresstypesType;
import com.genfare.cloud.model.types.LanguagesType;
import com.genfare.cloud.model.types.LinktypesType;
import com.genfare.cloud.model.types.PeopleStatusType;

public class PeopleToUserConverter implements Converter<People, User> {

    private static final Logger log = LoggerFactory.getLogger(PeopleToUserConverter.class);

    @Override
    public User convert(People person) {

        if (person == null) {
            return null;
        }

        ContactMethodEnum primaryMethod = ContactMethodEnum.EMAIL;
        Email email = new Email();
        Phone phone = new Phone();
        String jobTitle = person.getJobTitle() == null ? null : person.getJobTitle().getDescription();
        extractLinks(person, email, phone, primaryMethod);

        // @formatter:off
        UserBuilder builder = new User.UserBuilder(
            person.getPersonId(),
            extractUsername(person),
            person.getNameFirst(),
            person.getNameLast(),
            email,
            phone,
            extractAddress(person)
            )
                .alert(person.isAlert())
                .dateOfBirth(person.getDateOfBirth())
                .employeeId(person.getEmployeeId())
                .farecodes(person.getFarecodes())
                .gender(person.getGender())
                .groups(person.getGroups())
                .jobTitle(jobTitle)
                .language(LanguagesType.getEnumType(person.getLanguages().getLanguageId()))
                .middleName(person.getNameMiddle())
                .prefix(person.getPrefix())
                .primaryContactMethod(primaryMethod)
                .status(PeopleStatusType.findEnumStatus(person.getStatus().getStatus()))
                .suffix(person.getSuffix());
        // @formatter:on

        return builder.build();
    }

    private static Address extractAddress(People person) {

        for (Peopleaddresses a : person.getPeopleaddresseses()) {
            if (a.getAddresstypes().getAddressTypeId().equals(AddresstypesType.SHIPPING.getIndex())) {
                return PeopleaddressesToAddressConverter.convert(a);
            }
        }
        return new Address();
    }

    private static void extractLinks(People person, Email email, Phone phone,
        ContactMethodEnum primaryMethod) {

        boolean eset = false;
        boolean pset = false;

        for (Peoplelinks p : person.getPeoplelinkses()) {

            if (p.getLinktypes().getLinkTypeId().equals(LinktypesType.PERSONAL_EMAIL.getIndex())) {
                email.setAddress(p.getLinkValue());
                email.setDescription(p.getLinktypes().getDescription());
                if (p.getIsPrimaryContactMethod()) {
                    primaryMethod = ContactMethodEnum.EMAIL;
                }
                eset = true;
            } else if (p.getLinktypes().getLinkTypeId().equals(LinktypesType.HOME_PHONE.getIndex())) {
                phone.setDescription(p.getLinktypes().getDescription());
                phone.setNumber(p.getLinkValue());
                if (p.getIsPrimaryContactMethod()) {
                    primaryMethod = ContactMethodEnum.SMS;
                }
                pset = true;
            }

            if (eset && pset) {
                break;
            }
        }
    }

    private static String extractUsername(People person) {
        /*
         * Assumes using emails for login
         */
        for (Logins l : person.getLoginses()) {
            if (l.getUserLogin().contains("@")) {
                return l.getUserLogin();
            }
        }
        return null;
    }

}
