package com.genfare.cloud.model.entity;

import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.genfare.cloud.model.entity.inter.Auditable;
import com.genfare.cloud.model.listener.AuditListener;

@Entity
@Table(name = "documents")
@EntityListeners(AuditListener.class)
public class Documents
    implements java.io.Serializable, Auditable {
    private static final long serialVersionUID = 1L;

    private DocumentsID id;

    private String pageValue;

    private String touchedBy;

    private Date touchedAt;

    private String createdBy;

    private Date createdAt;

    public Documents() {
    }

    public Documents(DocumentsID id, String pageValue) {
        this.id = id;
        this.pageValue = pageValue;
    }

    @EmbeddedId
    @AttributeOverrides({ @AttributeOverride(name = "category", column = @Column(name = "Category", nullable = false)),
        @AttributeOverride(name = "pageKey", column = @Column(name = "pageKey", nullable = false)),
        @AttributeOverride(name = "language", column = @Column(name = "LanguageID", nullable = false)) })
    public DocumentsID getId() {
        return this.id;
    }

    public void setId(DocumentsID id) {
        this.id = id;
    }

    @Column(name = "PageValue", nullable = false, length = 65535)
    public String getPageValue() {
        return this.pageValue;
    }

    public void setPageValue(String pageValue) {
        this.pageValue = pageValue;
    }

    @Column(name = "TouchedBy", nullable = false, length = 50)
    public String getTouchedBy() {
        return this.touchedBy;
    }

    @Override
    public void setTouchedBy(String touchedBy) {
        this.touchedBy = touchedBy;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "TouchedAt", nullable = false, length = 19)
    public Date getTouchedAt() {
        return this.touchedAt;
    }

    @Override
    public void setTouchedAt(Date touchedAt) {
        this.touchedAt = touchedAt;
    }

    @Override
    @Column(name = "CreatedBy", nullable = false, length = 50)
    public String getCreatedBy() {
        return this.createdBy;
    }

    @Override
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Override
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CreatedAt", nullable = false, length = 19)
    public Date getCreatedAt() {
        return this.createdAt;
    }

    @Override
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

}