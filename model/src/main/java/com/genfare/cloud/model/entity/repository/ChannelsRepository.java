package com.genfare.cloud.model.entity.repository;

import org.springframework.data.repository.CrudRepository;

import com.genfare.cloud.model.entity.Channels;

public interface ChannelsRepository
    extends CrudRepository<Channels, Byte> {

}
