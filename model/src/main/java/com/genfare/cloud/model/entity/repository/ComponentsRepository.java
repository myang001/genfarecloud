package com.genfare.cloud.model.entity.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.jpa.repository.Query;

import com.genfare.cloud.model.entity.Components;
import com.genfare.cloud.model.entity.Modules;

public interface ComponentsRepository
    extends CrudRepository<Components, Short> {

    @Query("from Components where name = ?")
    public Components findByName(String name);

    @Query("select distinct c from Privileges p inner join p.components c inner join p.groups g inner join g.people pe where pe.personId = ?1 and c.modules = ?2")
    List<Components> findComponentsByUserAndModule(Integer id, Modules m);

}
