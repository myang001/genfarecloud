package com.genfare.cloud.model.entity.repository;

import org.springframework.data.repository.CrudRepository;

import com.genfare.cloud.model.entity.Validdays;

public interface ValiddaysRepository
    extends CrudRepository<Validdays, Integer> {

}
