package com.genfare.cloud.model.converters.pos.common;

import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;

import com.genfare.cloud.model.converters.pos.fare.FareCodeToFareCodeTypeConverter;
import com.genfare.cloud.model.converters.pos.fare.TickettypesToTicketTypeTypeConverter;
import com.genfare.cloud.model.converters.pos.fare.ZoneToZoneTypeConverter;
import com.genfare.cloud.model.entity.Farecodes;
import com.genfare.cloud.model.entity.Tickets;
import com.genfare.pos.core.model.common.TicketType;

public class TicketsToTicketTypeConverter implements
    Converter<Tickets, TicketType> {

    private static final Logger log = LoggerFactory
        .getLogger(TicketsToTicketTypeConverter.class);

    @Override
    public TicketType convert(Tickets t) {
        GregorianCalendar c = new GregorianCalendar();
        TicketType tt = new TicketType();

        tt.setTicketId(t.getTicketId());
        tt.setDescription(t.getDescription());
        tt.setDescriptionLong(t.getDescriptionLong());
        // tt.setExpirationTypeId();
        tt.setIsSubscribable(t.getIsSubscribable());
        tt.setPrice(t.getPrice());
        tt.setReplenishThreshold(t.getReplenishThreshold());
        tt.setReplenishValue(t.getReplenishValue());
        TickettypesToTicketTypeTypeConverter tt2tttc = new TickettypesToTicketTypeTypeConverter();
        tt.setTicketType(tt2tttc.convert(t.getTickettypes()));
        tt.setValue(t.getValue());
        ZoneToZoneTypeConverter z2ztc = new ZoneToZoneTypeConverter();
        tt.setZone(z2ztc.convert(t.getZones()));
        FareCodeToFareCodeTypeConverter fc2fctc = new FareCodeToFareCodeTypeConverter();
        for (Farecodes fc : t.getFarecodes()) {
            tt.getFareCode().add(fc2fctc.convert(fc));
        }

        try {
            c.setTime(t.getDateEffective());
            tt.setDateStart(DatatypeFactory.newInstance()
                .newXMLGregorianCalendar(c));

            c.setTime(t.getDateExpires());
            tt.setDateExpires(DatatypeFactory.newInstance()
                .newXMLGregorianCalendar(c));
        } catch (DatatypeConfigurationException e) {
            log.error("Unable to convert Tickets to TicketType", e);
            return null;
        }

        return tt;
    }

}
