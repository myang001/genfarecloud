package com.genfare.cloud.model.types;

import com.genfare.cloud.model.entity.Orderstatuses;

public enum OrderItemStatusType {

    UNSUBMITTED(OrderStatusBaseType.UNSUBMITTED),
    SUBMITTED(OrderStatusBaseType.SUBMITTED),
    FULFILLMENT(OrderStatusBaseType.FULFILLMENT),
    REJECTED(OrderStatusBaseType.REJECTED),
    ABANDONED(OrderStatusBaseType.ABANDONED),
    COMPLETE(OrderStatusBaseType.COMPLETE),
    CANCELLED(OrderStatusBaseType.CANCELLED),
    IN_PROGRESS(OrderStatusBaseType.IN_PROGRESS),
    UNPAID(OrderStatusBaseType.UNPAID),
    INVOICED(OrderStatusBaseType.INVOICED),
    PAID(OrderStatusBaseType.PAID),
    PENDING_ACTIVITY(OrderStatusBaseType.PENDING_ACTIVITY),
    HOLD(OrderStatusBaseType.HOLD);

    private final OrderStatusBaseType type;

    private OrderItemStatusType(OrderStatusBaseType type) {
        this.type = type;
    }

    /**
     * Gets the ordinal for the base type as in the DB
     * 
     * @return the position of the base type
     */
    public Integer getIndex() {
        return type.getIndex();
    }

    /**
     * Gets the entity representing the status
     * 
     * @return The Orderstatus entity
     */
    public Orderstatuses getStatus() {
        return type.getStatus();
    }
}
