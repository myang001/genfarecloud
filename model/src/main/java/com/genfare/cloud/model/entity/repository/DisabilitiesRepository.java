package com.genfare.cloud.model.entity.repository;

import org.springframework.data.repository.CrudRepository;

import com.genfare.cloud.model.entity.Disabilities;

public interface DisabilitiesRepository
    extends CrudRepository<Disabilities, Integer> {

}
