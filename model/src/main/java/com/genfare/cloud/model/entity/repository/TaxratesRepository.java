package com.genfare.cloud.model.entity.repository;

import org.springframework.data.repository.CrudRepository;

import com.genfare.cloud.model.entity.Taxrates;

public interface TaxratesRepository
    extends CrudRepository<Taxrates, Integer> {

}
