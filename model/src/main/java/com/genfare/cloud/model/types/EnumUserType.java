package com.genfare.cloud.model.types;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.usertype.UserType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EnumUserType<E extends Enum<E>>
    implements UserType {

    private static final Logger LOGGER = LoggerFactory.getLogger(EnumUserType.class);

    private Class<E> clazz = null;

    protected EnumUserType(Class<E> c) {
        this.clazz = c;
    }

    private static final int[] SQL_TYPES = { Types.VARCHAR };

    @Override
    public int[] sqlTypes() {
        return SQL_TYPES;
    }

    @Override
    public Class returnedClass() {
        return clazz;
    }

    @Override
    public Object deepCopy(Object value)
        throws HibernateException {
        return value;
    }

    @Override
    public boolean isMutable() {
        return false;
    }

    @Override
    public Object assemble(Serializable cached, Object owner)
        throws HibernateException {
        return cached;
    }

    @Override
    public Serializable disassemble(Object value)
        throws HibernateException {
        return (Serializable) value;
    }

    @Override
    public Object replace(Object original, Object target, Object owner)
        throws HibernateException {
        return original;
    }

    @Override
    public int hashCode(Object x)
        throws HibernateException {
        return x.hashCode();
    }

    @Override
    public boolean equals(Object x, Object y)
        throws HibernateException {
        if (x == y)
            return true;
        if (null == x || null == y)
            return false;
        return x.equals(y);
    }

    @Override
    public Object nullSafeGet(ResultSet resultSet, String[] names, SessionImplementor sessionImplementor, Object owner)
        throws HibernateException, SQLException {
        String name = resultSet.getString(names[0]);
        E result = null;
        if (!resultSet.wasNull()) {
            try {
                result = clazz.cast(clazz.getMethod("getEnum", String.class).invoke(null, name));
            } catch (NoSuchMethodException | SecurityException | IllegalAccessException | InvocationTargetException e) {
                LOGGER.debug("Invokation of getEnum failed, falling back to Enum.valueOf lookup", e);
                result = Enum.valueOf(clazz, name);
            }
        }
        return result;
    }

    @Override
    public void nullSafeSet(PreparedStatement preparedStatement, Object value, int index,
        SessionImplementor sessionImplementor)
        throws HibernateException, SQLException {
        if (null == value) {
            preparedStatement.setNull(index, Types.VARCHAR);
        } else {
            preparedStatement.setString(index, ((Enum) value).toString());
        }
    }
}