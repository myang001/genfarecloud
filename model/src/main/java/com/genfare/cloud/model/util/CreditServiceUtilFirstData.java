package com.genfare.cloud.model.util;

import java.math.BigDecimal;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.genfare.cloud.model.contact.Address;
import com.genfare.cloud.model.entity.Orderitems;
import com.genfare.cloud.model.entity.Orders;
import com.genfare.cloud.model.entity.Peoplecards;
import com.genfare.cloud.model.entity.PeoplecardsId;
import com.genfare.cloud.model.entity.Shippers;
import com.genfare.cloud.model.entity.repository.PaymenttypesRepository;
import com.genfare.cloud.model.entity.repository.PeoplecardsRepository;
import com.genfare.cloud.model.security.User;
import com.genfare.cloud.model.service.HashingService;
import com.genfare.cloud.model.tenant.TenantConfiguration;
import com.genfare.cloud.model.types.OrderitemactionsType;

@Component(value = "FirstData")
public class CreditServiceUtilFirstData
    implements CreditServiceUtils {

    private static final String SERVICE_NAME = "FirstData";

    private static final String FORWARD_TO_PARAM = "x_user1";

    private static final String USER_DATA = "x_user2";

    private static final Logger LOGGER = LoggerFactory.getLogger(CreditServiceUtilFirstData.class);

    @Autowired
    private HashingService hasher;

    @Autowired
    private TenantConfiguration tenantConfig;

    @Autowired
    private PeoplecardsRepository peoplecardsRepo;

    @Autowired
    private PaymenttypesRepository paymentTypesRepo;

    String creditServiceBillingFirstName = "x_first_name";

    String creditServiceBillingLastName = "x_last_name";

    String creditServiceBillingAddress = "x_address";

    String creditServiceBillingCity = "x_city";

    String creditServiceBillingCountry = "x_country";

    String creditServiceBillingState = "x_state";

    String creditServiceBillingZip = "x_zip";

    String creditServiceBillingEmail = "x_email";

    String creditServiceBillingPhone = "x_phone";

    String creditServiceMaskedCreditCardNumber = "Card_Number";// transarmor_token

    String creditServiceTransactionId = "Transaction_Tag";

    String creditServiceTransactionDatestamp = "x_fp_timestamp";

    String creditServiceAuthorizationNumber = "Authorization_Num";

    String creditServiceCreditCardTypeCode = "TransactionCardType";

    String creditServiceCreditCardExp = "Expiry_Date";

    String referenceNumber = "SequenceNo";

    @Override
    public String getServiceName() {
        return SERVICE_NAME;
    }

    private List<String> getLineItems(Orders order) {
        // Item ID<|>Item Title<|>Item Description<|>Quantity<|>Unit Price
        List<String> line_items = new ArrayList<String>();
        int item_count = 1;
        for (Orderitems oi : order.getOrderitemses()) {
            // Not child
            if (oi.getParentItemNumber() == null || (oi.getParentItemNumber() == oi.getId().getOrderItemNumber())
                && (oi.getQuantity() != 0)) {
                // Standard parent
                if (oi.getWallettypes() != null) {
                    line_items.add("" + item_count++ + "<|>" + oi.getWallettypes().getDescription() + "<|>"
                        + oi.getWallettypes().getDescription() + "<|>" + oi.getQuantity() + "<|>" + oi.getUnitPrice());
                }
                // Item added to existing card
                else if (oi.getOrderitemactions().getDescription().equals(OrderitemactionsType.REPLACE.getType().getDescription())) {
                    line_items.add("" + item_count++ + "<|>" + oi.getOrderitemactions().getDescription() + "<|>"
                        + oi.getWallets().getWallettypes().getDescription() + "<|>" + oi.getQuantity() + "<|>" + oi.getUnitPrice());
                }
                else if (oi.getTickets() != null) {
                    line_items.add("" + item_count++ + "<|>" + oi.getTickets().getDescription() + "<|>"
                        + oi.getTickets().getDescription() + "<|>" + oi.getQuantity() + "<|>" + oi.getUnitPrice());
                }
                for (Orderitems oic : order.getOrderitemses()) {
                    // Child
                    if (oic.getParentItemNumber() == oi.getId().getOrderItemNumber()
                        && oic.getId().getOrderItemNumber() != oi.getId().getOrderItemNumber()
                        && oic.getQuantity() != 0) {
                        line_items.add("" + item_count++ + "<|>" + oic.getTickets().getDescription() + "<|>    "
                            + oic.getTickets().getDescription() + "<|>" + oic.getQuantity() * oi.getQuantity()
                            + "<|>" + oic.getUnitPrice());
                    }
                }
            }

        }

        Shippers os = order.getShippers();
        if (null != os) {
            line_items.add("" + item_count++ + "<|>" + os.getDescription() + "<|>    " + os.getDescription()
                + "<|>1<|>" + os.getRate());
        }

        return line_items;
    }

    private Long getTimestamp() {
        Date date = new Date();
        return new Long(date.getTime() / 1000);
    }

    private String getSequence() {
        SecureRandom rand = new SecureRandom();
        byte[] sequence = new byte[8];
        rand.nextBytes(sequence);
        return sequence.toString();
    }

    private String calculateHash(String sequence, String timestamp, String amount) {

        // x_login^x_fp_sequence^x_fp_timestamp^x_amount^x_currency
        String s =
            new StringBuilder().append(tenantConfig.getCreditServiceXLogin()).append("^").append(sequence).append("^").append(timestamp).append("^").append(amount).append("^")
                .append(tenantConfig.getCreditServiceXCurrencyCode()).toString();

        return hasher.hmacDigest(s, tenantConfig.getCreditServiceAgencyKey(), "HmacMD5");
    }

    @Override
    public Map<String, Object> getAuthOnlyFormData(User user, String returnContext, String userData) {

        Orders order = new Orders();
        order.setAmountOrder(new BigDecimal(0));
        order.setOrderId(0);

        Map<String, Object> map = getFormData(order, user, returnContext, userData);
        map.put("x_type", "AUTH_ONLY");

        return map;
    }

    @Override
    public String getUserDataField() {
        return USER_DATA;
    }

    @Override
    public Map<String, Object> getFormData(Orders order, User user, String returnContext, String userData) {
        Map<String, Object> map = new HashMap<String, Object>();

        String timestamp = getTimestamp().toString();
        String sequence = getSequence();

        BigDecimal amt = order.getAmountOrder();
        Shippers os = order.getShippers();
        if (null != os) {
            amt = amt.add(os.getRate());
        }

        map.put("x_login", tenantConfig.getCreditServiceXLogin());
        map.put("x_fp_timestamp", timestamp.toString());
        map.put("x_fp_sequence", sequence);
        map.put("x_fp_hash", calculateHash(sequence, timestamp, amt.toString()));
        map.put("x_amount", amt.toString());
        map.put("x_currency_code", tenantConfig.getCreditServiceXCurrencyCode());
        map.put("x_test_request", tenantConfig.getCreditServiceXTestRequest());
        map.put("x_relay_response", tenantConfig.getCreditServiceXRelayResponse());
        map.put("x_po_number", order.getOrderId().toString());
        map.put("x_show_form", tenantConfig.getCreditServiceXShowForm());
        map.put(FORWARD_TO_PARAM, returnContext);
        map.put(USER_DATA, (userData != null) ? userData : "");

        Address address = null;
        if (user != null) {
            address = user.getAddress();
            map.put("x_first_name", user.getFirstName());
            map.put("x_last_name", user.getLastName());
        } else {
            map.put("x_first_name", "");
            map.put("x_last_name", "");
        }

        if (address != null) {
            map.put("x_country", address.getCountry());
            map.put("x_address", address.getLine1());
            map.put("x_city", address.getCity());
            map.put("x_state", address.getState());
            map.put("x_zip", address.getPostalCode());
        } else {
            map.put("x_country", "");
            map.put("x_address", "");
            map.put("x_city", "");
            map.put("x_state", "");
            map.put("x_zip", "");
        }

        Map<String, Object> lineItems = new HashMap<String, Object>();
        lineItems.put("isList", true);
        lineItems.put("items", getLineItems(order));
        lineItems.put("name", "x_line_item");

        map.put("LineItems", lineItems);

        return map;
    }

    @Override
    public boolean validatePost(HttpServletRequest request, String RelayResponseKey) {

        String MD5_Hash = request.getParameter("x_MD5_Hash");
        String x_login = request.getParameter("x_login");
        String x_trans_id = request.getParameter("x_trans_id");
        String x_amount = request.getParameter("x_amount");

        if (MD5_Hash == null || x_login == null || x_trans_id == null || x_amount == null) {
            return false;
        }

        String hash_concat = RelayResponseKey + x_login + x_trans_id + x_amount;
        String reply_content_hash = hasher.md5Hash(hash_concat);
        LOGGER.debug("BEFORE HASH CHECK RETURN: " + reply_content_hash + "," + reply_content_hash.equals(MD5_Hash));
        return (reply_content_hash.equals(MD5_Hash) || isAuthOnly(request));
    }

    @Override
    public OrderPayment getOrderPayment(HttpServletRequest request) {
        OrderPayment order = new OrderPayment();

        String orderIdStr = request.getParameter("x_po_number");
        order.setOrderId(Integer.parseInt(orderIdStr));

        order.setAuthorizationNumber(request.getParameter(creditServiceAuthorizationNumber));

        order.setMaskedCreditCardNumber(request.getParameter(creditServiceMaskedCreditCardNumber));

        order.setPaymentType(request.getParameter(creditServiceCreditCardTypeCode));

        order.setReferenceNumber(request.getParameter(referenceNumber));

        order.setRc(Integer.parseInt(request.getParameter("x_response_code")));

        order.setRcText(request.getParameter("x_response_reason_text"));

        order.setAmount(new BigDecimal(request.getParameter("x_amount")));

        String transactionIdStr = request.getParameter(creditServiceTransactionId);

        if (null != transactionIdStr && !transactionIdStr.isEmpty())
            order.setTransactionId(Long.parseLong(transactionIdStr));

        order.setStatus(1);

        return order;
    }

    @Override
    public Contact getContact(HttpServletRequest request) {
        Contact contact = new Contact();

        contact.setFirstName(request.getParameter(creditServiceBillingFirstName));
        contact.setLastName(request.getParameter(creditServiceBillingLastName));
        contact.setAddress1(request.getParameter(creditServiceBillingAddress));
        contact.setAddress2("");
        contact.setCity(request.getParameter(creditServiceBillingCity));
        contact.setCountry(request.getParameter(creditServiceBillingCountry));
        contact.setState(request.getParameter(creditServiceBillingState));
        contact.setZip(request.getParameter(creditServiceBillingZip));
        contact.setEmail(request.getParameter(creditServiceBillingEmail));

        return contact;
    }

    @Override
    public UserBilling getUserBilling(HttpServletRequest request) {
        UserBilling billing = new UserBilling();

        String transactionIdStr = request.getParameter(creditServiceTransactionId);
        if (null != transactionIdStr && !transactionIdStr.isEmpty()) {
            billing.setToken(Long.valueOf(request.getParameter(creditServiceTransactionId)));
        }
        billing.setAuthorizationNumber(request.getParameter(creditServiceAuthorizationNumber));
        billing.setMaskedCreditCardNumber(request.getParameter(creditServiceMaskedCreditCardNumber));
        // TODO
        // billing.setDefaultBilling(user.isGuestUser() ? false : true);
        billing.setPaymentType(request.getParameter(creditServiceCreditCardTypeCode));
        billing.setServiceName("First Data");
        // TODO
        // billing.setUserId(user.getUser_Id());
        // TODO double check that this contact_id is not the shopping contact Id
        // billing.setContactId(user.getContactId());
        return billing;
    }

    @Override
    public String getForwardToParam() {
        return FORWARD_TO_PARAM;
    }

    @Override
    public boolean isAuthOnly(HttpServletRequest request) {
        String x_type = request.getParameter("x_type");
        return (x_type != null && x_type.equals("AUTH_ONLY"));
    }

    @Override
    public Peoplecards createPeopleCard(User user, HttpServletRequest request) {
        Peoplecards card = new Peoplecards();
        card.setId(new PeoplecardsId(user.getId(), peoplecardsRepo.getPersonCardCount(user.getId()).byteValue()));
        card.setPaymenttypes(paymentTypesRepo.getPaymenttypeByDescription(getCardType(request))); // TODO, probably
                                                                                                  // needs to be typed
                                                                                                  // in enum

        String cardNum = request.getParameter("Card_Number");
        card.setLastFour(cardNum.substring(12));
        card.setToken(request.getParameter(creditServiceMaskedCreditCardNumber));

        String exp = request.getParameter("Expiry_Date");
        Calendar calendar = Calendar.getInstance();
        calendar.clear();
        calendar.set(Calendar.MONTH, Integer.parseInt(exp.substring(0, 2)));
        calendar.set(Calendar.YEAR, Integer.parseInt("20" + exp.substring(2)));
        calendar.set(Calendar.DATE, -1); // Sets to last day of month, which is the last valid day the card can be used

        card.setDateExpires(calendar.getTime());

        return peoplecardsRepo.save(card);
    }

    @Override
    public String getCardType(HttpServletRequest request) {
        return request.getParameter("TransactionCardType");
    }

    /*
     * void dumpRequestParameters(HttpServletRequest request){ String totalAmountStr = request.getParameter("TotalAmount");
     * Iterable<String> namemap = request.getParameterMap().keySet(); for(String n: namemap){ LOGGER.debug("NAMES: " + n); } }
     */
}
