package com.genfare.cloud.model.entity.repository;

import org.springframework.data.repository.CrudRepository;

import com.genfare.cloud.model.entity.Ordertypes;

public interface OrdertypesRepository
    extends CrudRepository<Ordertypes, Byte> {

}
