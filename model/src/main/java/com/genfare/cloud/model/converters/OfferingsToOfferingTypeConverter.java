/**
 *
 */
package com.genfare.cloud.model.converters;

import org.springframework.core.convert.converter.Converter;

import com.genfare.cloud.model.entity.Offerings;
import com.genfare.cloud.model.om.OfferingType;

/**
 * @author mpeter
 */
public class OfferingsToOfferingTypeConverter
    implements Converter<Offerings, OfferingType> {

    @Override
    public OfferingType convert(Offerings o) {
        OfferingType ot = new OfferingType();
        ot.setOfferingID(o.getOfferingId().intValue());
        ot.setDescription(o.getDescription());
        return ot;
    }

    public Offerings convert(OfferingType ot) {
        Offerings o = new Offerings();
        int id = ot.getOfferingID();
        o.setOfferingId((byte) id);
        o.setDescription(ot.getDescription());
        return o;
    }
}
