package com.genfare.cloud.model.util;

import java.io.Serializable;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

public class UserBilling
    implements Serializable {
    private static final long serialVersionUID = 35109543524560885L;

    /**
     * The userId is a required field if updating/deleting, but can be null when inserting
     */
    @Min(0)
    private int userId;

    @Min(0)
    private int contactId;

    @NotNull
    private Boolean defaultBilling;

    private Long token;

    private String authorizationNumber;

    private String maskedCreditCardNumber;

    private String paymentType;

    private String serviceName;

    @Valid
    private Contact address;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getContactId() {
        return contactId;
    }

    public void setContactId(int contactId) {
        this.contactId = contactId;
    }

    public Boolean isDefaultBilling() {
        return defaultBilling;
    }

    public void setDefaultBilling(Boolean defaultBilling) {
        this.defaultBilling = defaultBilling;
    }

    public Long getToken() {
        return token;
    }

    public void setToken(Long token) {
        this.token = token;
    }

    public String getAuthorizationNumber() {
        return authorizationNumber;
    }

    public void setAuthorizationNumber(String authorizationNumber) {
        this.authorizationNumber = authorizationNumber;
    }

    public String getMaskedCreditCardNumber() {
        return maskedCreditCardNumber;
    }

    public void setMaskedCreditCardNumber(String maskedCreditCardNumber) {
        this.maskedCreditCardNumber = maskedCreditCardNumber;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public Contact getAddress() {
        return address;
    }

    public void setAddress(Contact contact) {
        this.address = contact;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        return new HashCodeBuilder(1, prime).append(address).append(defaultBilling).append(maskedCreditCardNumber).append(paymentType).append(token).append(authorizationNumber)
            .append(serviceName).append(contactId).append(userId).toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof UserBilling)) {
            return false;
        }
        UserBilling other = (UserBilling) obj;

        return new EqualsBuilder().append(address, other.address).append(contactId, other.contactId).append(defaultBilling,
            other.defaultBilling).append(maskedCreditCardNumber,
            other.maskedCreditCardNumber).append(paymentType,
            other.paymentType).append(token,
            other.token).append(authorizationNumber,
            other.authorizationNumber).append(serviceName,
            other.serviceName).append(userId,
            other.userId).isEquals();

    }

    @Override
    public String toString() {
        // TODO replace RESTServiceUtils.toJson(this) method
        return null;
    }

}
