package com.genfare.cloud.model.entity.repository;

import org.springframework.data.repository.CrudRepository;

import com.genfare.cloud.model.entity.SchemaVersion;

public interface SchemaVersionRepository
    extends CrudRepository<SchemaVersion, Integer> {

}
