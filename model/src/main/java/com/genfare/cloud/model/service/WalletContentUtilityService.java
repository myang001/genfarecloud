package com.genfare.cloud.model.service;

import com.genfare.cloud.model.entity.Offerings;
import com.genfare.cloud.model.entity.Walletcontents;

public interface WalletContentUtilityService {

    String getBalance(Walletcontents item);

    String getBalanceValue(Walletcontents item);

    String getBalanceType(Walletcontents item);

    String getStatus(Walletcontents item);

    String getSubscriptionStatus(Walletcontents item);

    String getOffering(Walletcontents item);

    String getReplenishValue(Walletcontents content);

    String getReplenishPrice(Walletcontents content);

    String getReplenishThreshold(Walletcontents content);

    Offerings buyMoreOffering(Walletcontents content);

}
