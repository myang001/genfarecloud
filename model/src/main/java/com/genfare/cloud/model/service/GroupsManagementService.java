package com.genfare.cloud.model.service;

import com.genfare.cloud.model.entity.Groups;

public interface GroupsManagementService {

    boolean isGroupEnabled(Groups group);

    void toggleGroupStatus(Groups group);

    public String generateRoleForGroup(String name);
}
