package com.genfare.cloud.model.entity;

// Generated May 28, 2014 9:50:04 PM by Hibernate Tools 3.2.2.GA

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Type;

import com.genfare.cloud.model.entity.inter.Auditable;
import com.genfare.cloud.model.listener.AuditListener;

/**
 * Wallettypes generated by hbm2java
 */
@Entity
@Table(name = "wallettypes")
@EntityListeners(AuditListener.class)
public class Wallettypes
    implements java.io.Serializable, Auditable {

    private Byte walletTypeId;

    private String description;

    private String descriptionLong;

    private Byte availableSlots;

    private Boolean isWriteable;

    private Boolean isSellable;

    private Integer quantityOnHandTotal;

    private String touchedBy;

    private Date touchedAt;

    private String createdBy;

    private Date createdAt;

    private Set<Shippinglists> shippinglistses = new HashSet<Shippinglists>(0);

    private Set<Activitysummary> activitysummaries = new HashSet<Activitysummary>(0);

    private Set<Offerings> offeringses = new HashSet<Offerings>(0);

    private Set<Crossreferencefile> crossreferencefiles = new HashSet<Crossreferencefile>(0);

    private Set<Channelwallettypes> channelwallettypeses = new HashSet<Channelwallettypes>(0);

    private Set<Wallets> walletses = new HashSet<Wallets>(0);

    private Set<Orderitems> orderitemses = new HashSet<Orderitems>(0);

    public Wallettypes() {
    }

    public Wallettypes(String description, Byte availableSlots, Boolean isWriteable) {
        this.description = description;
        this.availableSlots = availableSlots;
        this.isWriteable = isWriteable;
    }

    public Wallettypes(String description, Byte availableSlots, Boolean isWriteable, Boolean isSellable,
        Integer quantityOnHandTotal, Set<Shippinglists> shippinglistses,
        Set<Activitysummary> activitysummaries, Set<Offerings> offeringses,
        Set<Crossreferencefile> crossreferencefiles, Set<Channelwallettypes> channelwallettypeses,
        Set<Wallets> walletses, Set<Orderitems> orderitemses) {
        this.description = description;
        this.availableSlots = availableSlots;
        this.isWriteable = isWriteable;
        this.isSellable = isSellable;
        this.quantityOnHandTotal = quantityOnHandTotal;
        this.shippinglistses = shippinglistses;
        this.activitysummaries = activitysummaries;
        this.offeringses = offeringses;
        this.crossreferencefiles = crossreferencefiles;
        this.channelwallettypeses = channelwallettypeses;
        this.walletses = walletses;
        this.orderitemses = orderitemses;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "WalletTypeID", unique = true, nullable = false)
    public Byte getWalletTypeId() {
        return this.walletTypeId;
    }

    public void setWalletTypeId(Byte walletTypeId) {
        this.walletTypeId = walletTypeId;
    }

    @Column(name = "Description", nullable = false, length = 50)
    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name = "DescriptionLong", nullable = true, length = 1024)
    public String getDescriptionLong() {
        return this.descriptionLong;
    }

    public void setDescriptionLong(String descriptionLong) {
        this.descriptionLong = descriptionLong;
    }

    @Column(name = "AvailableSlots", nullable = false)
    public Byte getAvailableSlots() {
        return this.availableSlots;
    }

    public void setAvailableSlots(Byte availableSlots) {
        this.availableSlots = availableSlots;
    }

    @Type(type = "true_false")
    @Column(name = "IsWriteable", nullable = false, length = 1)
    public Boolean getIsWriteable() {
        return this.isWriteable;
    }

    public void setIsWriteable(Boolean isWriteable) {
        this.isWriteable = isWriteable;
    }

    @Type(type = "true_false")
    @Column(name = "IsSellable", length = 1)
    public Boolean getIsSellable() {
        return this.isSellable;
    }

    public void setIsSellable(Boolean isSellable) {
        this.isSellable = isSellable;
    }

    @Column(name = "QuantityOnHandTotal")
    public Integer getQuantityOnHandTotal() {
        return this.quantityOnHandTotal;
    }

    public void setQuantityOnHandTotal(Integer quantityOnHandTotal) {
        this.quantityOnHandTotal = quantityOnHandTotal;
    }

    @Column(name = "TouchedBy", nullable = false, length = 50)
    public String getTouchedBy() {
        return this.touchedBy;
    }

    @Override
    public void setTouchedBy(String touchedBy) {
        this.touchedBy = touchedBy;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "TouchedAt", nullable = false, length = 19)
    public Date getTouchedAt() {
        return this.touchedAt;
    }

    @Override
    public void setTouchedAt(Date touchedAt) {
        this.touchedAt = touchedAt;
    }

    @Override
    @Column(name = "CreatedBy", nullable = false, length = 50)
    public String getCreatedBy() {
        return this.createdBy;
    }

    @Override
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Override
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CreatedAt", nullable = false, length = 19)
    public Date getCreatedAt() {
        return this.createdAt;
    }

    @Override
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "wallettypes")
    public Set<Shippinglists> getShippinglistses() {
        return this.shippinglistses;
    }

    public void setShippinglistses(Set<Shippinglists> shippinglistses) {
        this.shippinglistses = shippinglistses;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "wallettypes")
    public Set<Activitysummary> getActivitysummaries() {
        return this.activitysummaries;
    }

    public void setActivitysummaries(Set<Activitysummary> activitysummaries) {
        this.activitysummaries = activitysummaries;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "wallettypes")
    public Set<Offerings> getOfferingses() {
        return this.offeringses;
    }

    public void setOfferingses(Set<Offerings> offeringses) {
        this.offeringses = offeringses;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "wallettypes")
    public Set<Crossreferencefile> getCrossreferencefiles() {
        return this.crossreferencefiles;
    }

    public void setCrossreferencefiles(Set<Crossreferencefile> crossreferencefiles) {
        this.crossreferencefiles = crossreferencefiles;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "wallettypes")
    public Set<Channelwallettypes> getChannelwallettypeses() {
        return this.channelwallettypeses;
    }

    public void setChannelwallettypeses(Set<Channelwallettypes> channelwallettypeses) {
        this.channelwallettypeses = channelwallettypeses;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "wallettypes")
    public Set<Wallets> getWalletses() {
        return this.walletses;
    }

    public void setWalletses(Set<Wallets> walletses) {
        this.walletses = walletses;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "wallettypes")
    public Set<Orderitems> getOrderitemses() {
        return this.orderitemses;
    }

    public void setOrderitemses(Set<Orderitems> orderitemses) {
        this.orderitemses = orderitemses;
    }

}
