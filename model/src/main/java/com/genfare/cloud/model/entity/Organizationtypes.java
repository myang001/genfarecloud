package com.genfare.cloud.model.entity;

// Generated Mar 19, 2014 9:47:20 AM by Hibernate Tools 4.0.0

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Type;

import com.genfare.cloud.model.entity.inter.Auditable;
import com.genfare.cloud.model.listener.AuditListener;

/**
 * Organizationtypes generated by hbm2java
 */
@Entity
@Table(name = "organizationtypes")
@EntityListeners(AuditListener.class)
public class Organizationtypes
    implements java.io.Serializable, Auditable {

    private Byte organizationTypeId;

    private String description;

    private Boolean isRetail;

    private String touchedBy;

    private Date touchedAt;

    private String createdBy;

    private Date createdAt;

    private Set<Organizations> organizationses = new HashSet<Organizations>(0);

    private Set<Activitysummary> activitysummaries = new HashSet<Activitysummary>(0);

    public Organizationtypes() {
    }

    public Organizationtypes(String description, Boolean isRetail) {
        this.description = description;
        this.isRetail = isRetail;
    }

    public Organizationtypes(String description, Boolean isRetail, Set<Organizations> organizationses,
        Set<Activitysummary> activitysummaries) {
        this.description = description;
        this.isRetail = isRetail;
        this.organizationses = organizationses;
        this.activitysummaries = activitysummaries;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "OrganizationTypeID", unique = true, nullable = false)
    public Byte getOrganizationTypeId() {
        return this.organizationTypeId;
    }

    public void setOrganizationTypeId(Byte organizationTypeId) {
        this.organizationTypeId = organizationTypeId;
    }

    @Column(name = "Description", nullable = false, length = 50)
    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name = "IsRetail", nullable = false, length = 1)
    @Type(type = "true_false")
    public Boolean getIsRetail() {
        return this.isRetail;
    }

    public void setIsRetail(Boolean isRetail) {
        this.isRetail = isRetail;
    }

    @Column(name = "TouchedBy", nullable = false, length = 50)
    public String getTouchedBy() {
        return this.touchedBy;
    }

    @Override
    public void setTouchedBy(String touchedBy) {
        this.touchedBy = touchedBy;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "TouchedAt", nullable = false, length = 19)
    public Date getTouchedAt() {
        return this.touchedAt;
    }

    @Override
    public void setTouchedAt(Date touchedAt) {
        this.touchedAt = touchedAt;
    }

    @Override
    @Column(name = "CreatedBy", nullable = false, length = 50)
    public String getCreatedBy() {
        return this.createdBy;
    }

    @Override
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Override
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CreatedAt", nullable = false, length = 19)
    public Date getCreatedAt() {
        return this.createdAt;
    }

    @Override
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "organizationtypes")
    public Set<Organizations> getOrganizationses() {
        return this.organizationses;
    }

    public void setOrganizationses(Set<Organizations> organizationses) {
        this.organizationses = organizationses;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "organizationtypes")
    public Set<Activitysummary> getActivitysummaries() {
        return this.activitysummaries;
    }

    public void setActivitysummaries(Set<Activitysummary> activitysummaries) {
        this.activitysummaries = activitysummaries;
    }

}
