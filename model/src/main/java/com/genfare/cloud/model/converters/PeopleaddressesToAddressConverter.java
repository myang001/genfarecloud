package com.genfare.cloud.model.converters;

import com.genfare.cloud.model.contact.Address;
import com.genfare.cloud.model.entity.Peopleaddresses;

public class PeopleaddressesToAddressConverter {

    public static Address convert(Peopleaddresses pa) {
        Address a = new Address();
        a.setDescription(pa.getAddresstypes().getDescription());
        a.setLine1(pa.getAddressLine1());
        a.setLine2(pa.getAddressLine2());
        a.setCity(pa.getCity());
        a.setState(pa.getState());
        a.setPostalCode(pa.getPostalCode());
        a.setCountry(pa.getCountry());
        return a;
    }
}
