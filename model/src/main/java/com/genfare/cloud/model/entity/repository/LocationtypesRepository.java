package com.genfare.cloud.model.entity.repository;

import org.springframework.data.repository.CrudRepository;

import com.genfare.cloud.model.entity.Locationtypes;

public interface LocationtypesRepository
    extends CrudRepository<Locationtypes, Byte> {

}
