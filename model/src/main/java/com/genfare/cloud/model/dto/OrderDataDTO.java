package com.genfare.cloud.model.dto;

import com.genfare.cloud.model.contact.Address;

public class OrderDataDTO {

    /*
     * {"lastName":"Bobkins", "Type":"Shipping Address", "Address":{ "postalCode":"60000", "state":"Illinois", "line1":
     * "123 Bob St", "country":"United States", "city":"Bobbins", "line2":""}, "firstName":"Bob"}
     */

    private String lastName;

    private String firstName;

    private Address shipping;

    public OrderDataDTO() {
        shipping = new Address();
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public Address getShipping() {
        return shipping;
    }

    public void setShipping(Address shipping) {
        this.shipping = shipping;
    }

    @Override
    public String toString() {
        return "OrderDataDTO [lastName=" + lastName + ", firstName=" + firstName + ", shipping=" + shipping + "]";
    }

}
