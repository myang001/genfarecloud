/**
 *
 */
package com.genfare.cloud.model.converters;

import org.springframework.core.convert.converter.Converter;

import com.genfare.cloud.model.entity.Tickets;
import com.genfare.cloud.model.om.TicketType;

/**
 * @author mpeter
 */
public class TicketsToTicketType implements Converter<Tickets, TicketType> {

    @Override
    public TicketType convert(Tickets t) {
        TicketType tt = new TicketType();
        tt.setTicketID(t.getTicketId().intValue());
        tt.setDescription(t.getDescription());
        return tt;
    }

    public Tickets convert(TicketType tt) {
        Tickets t = new Tickets();
        int ticketId = tt.getTicketID();
        t.setTicketId(ticketId);
        t.setDescription(tt.getDescription());
        return t;
    }
}
