package com.genfare.cloud.model.entity.repository;

import org.springframework.data.repository.CrudRepository;

import com.genfare.cloud.model.entity.Activityattributes;

public interface ActivityattributesRepository
    extends CrudRepository<Activityattributes, Integer> {

}
