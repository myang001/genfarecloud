/**
 *
 */
package com.genfare.cloud.model.converters;

import org.springframework.core.convert.converter.Converter;

import com.genfare.cloud.model.entity.Farecodes;
import com.genfare.cloud.model.om.FarecodeType;

/**
 * @author mpeter
 */
public class FarecodesToFarecodeType
    implements Converter<Farecodes, FarecodeType> {

    @Override
    public FarecodeType convert(Farecodes fcs) {
        FarecodeType fc = new FarecodeType();
        if (fcs.getFarecodeId() != null) {
            fc.setFarecodeID(fcs.getFarecodeId().intValue());
            fc.setDescription(fcs.getDescription());
        }
        return fc;
    }
}