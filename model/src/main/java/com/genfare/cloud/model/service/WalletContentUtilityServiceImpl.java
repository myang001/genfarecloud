package com.genfare.cloud.model.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.genfare.cloud.model.entity.Offerings;
import com.genfare.cloud.model.entity.Subscriptions;
import com.genfare.cloud.model.entity.Walletcontents;
import com.genfare.cloud.model.entity.repository.OfferingsRepository;
import com.genfare.cloud.model.entity.repository.SubscriptionsRepository;
import com.genfare.cloud.model.types.ChannelsType;
import com.genfare.cloud.model.types.SubscriptionstatusType;
import com.genfare.cloud.model.types.TickettypesType;

@Service("walletContentUtilityService")
public class WalletContentUtilityServiceImpl
    implements WalletContentUtilityService {

    @Autowired
    private SubscriptionsRepository subscriptionsRepository;

    @Autowired
    private OfferingsRepository offeringsRepository;

    @Override
    public String getBalance(Walletcontents item) {
        String bal = getBalanceValue(item);
        String typ = getBalanceType(item);
        return typ.replace("%s", bal);
    }

    @Override
    public String getBalanceValue(Walletcontents item) {
        Integer type = Integer.valueOf(item.getTickets().getTickettypes().getTicketTypeId().intValue());
        if (type.equals(TickettypesType.FLOATING_PERIOD_PASS.getIndex())) {
            Date di = item.getDateStart();
            Date df = item.getDateExpires();

            if (di == null || df == null) {
                return "N/A";
            }
            long dt = df.getTime() - di.getTime(); // In millis

            return new Long(dt / 86400000L).toString();
        } else if (type.equals(TickettypesType.FIXED_PERIOD_PASS.getIndex())) {
            Date di = item.getDateStart();
            Date df = item.getDateExpires();

            if (di == null || df == null) {
                return "N/A";
            }
            return "";
        } else if (type.equals(TickettypesType.STORED_RIDE.getIndex())) {
            return new Integer(item.getValueRemaining().intValue()).toString();
        } else {
            return item.getValueRemaining().toString();
        }
    }

    @Override
    public String getBalanceType(Walletcontents item) {
        Integer type = Integer.valueOf(item.getTickets().getTickettypes().getTicketTypeId().intValue());
        if (type.equals(TickettypesType.FLOATING_PERIOD_PASS.getIndex())) {
            Date di = item.getDateStart();
            Date df = item.getDateExpires();

            if (di == null || df == null) {
                return "N/A";
            }
            return "%s days";

        } else if (type.equals(TickettypesType.FIXED_PERIOD_PASS.getIndex())) {
            Date di = item.getDateStart();
            Date df = item.getDateExpires();

            if (di == null || df == null) {
                return "N/A";
            }
            Calendar c = Calendar.getInstance();
            c.setTimeInMillis(df.getTime());

            return c.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault());

        } else if (type.equals(TickettypesType.STORED_RIDE.getIndex())) {
            return "%s rides";
        } else {
            return "$%s";
        }
    }

    @Override
    public String getStatus(Walletcontents item) {
        Integer type = Integer.valueOf(item.getTickets().getTickettypes().getTicketTypeId().intValue());
        if (type.equals(TickettypesType.FLOATING_PERIOD_PASS.getIndex()) ||
            type.equals(TickettypesType.FIXED_PERIOD_PASS.getIndex())) {

            return (item.getDateStart() == null) ? "Inactive" : "Active";
        }

        return "Active";

    }

    @Override
    public String getSubscriptionStatus(Walletcontents item) {
        Subscriptions foundSubscription = subscriptionsRepository.findByWalletContentsId(item.getWalletContentsId());
        if ((foundSubscription != null) && (!foundSubscription.getSubscriptionstatus().equals(SubscriptionstatusType.CANCELLED.getStatus()))) {
            return "Active";
        }

        return "Inactive";
    }

    @Override
    public String getOffering(Walletcontents item) {
        // TODO: Until rules are clearer, assuming first in list
        List<Offerings> offers = new ArrayList<Offerings>(item.getTickets().getOfferingses());
        return offers.get(0).getOfferingId().toString();
    }

    @Override
    public String getReplenishValue(Walletcontents content) {
        return content.getTickets().getReplenishValue().toString();
    }

    @Override
    public String getReplenishPrice(Walletcontents content) {
        return content.getTickets().getReplenishPrice().toString();
    }

    @Override
    public String getReplenishThreshold(Walletcontents content) {
        String type = content.getTickets().getTickettypes().getDescription();
        BigDecimal thresh = content.getTickets().getReplenishThreshold();
        if (type.equals("Period Pass") || type.equals("Stored Ride")) {
            return new Integer(thresh.intValue()).toString();
        }
        return thresh.toString();
    }

    @Override
    public Offerings buyMoreOffering(Walletcontents content) {

        Byte channel = ChannelsType.EFARE.getType().getChannelId();
        Byte farecode = content.getWallets().getFarecodes().getFarecodeId();
        Integer ticket = content.getTickets().getTicketId();

        List<Offerings> o = offeringsRepository.findOfferingsByTicketEligibleForBuyMore(channel, farecode, ticket);
        return (o.size() > 0 ? o.get(0) : null);
    }
}