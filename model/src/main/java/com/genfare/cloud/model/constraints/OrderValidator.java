package com.genfare.cloud.model.constraints;

import java.util.List;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

import com.genfare.cloud.model.annotation.OrderValid;
import com.genfare.cloud.model.entity.Orderitems;
import com.genfare.cloud.model.service.BusinessRulesService;

public class OrderValidator
    implements ConstraintValidator<OrderValid, List<Orderitems>> {

    @Autowired
    BusinessRulesService businessRulesManager;

    @Override
    public void initialize(OrderValid constraintAnnotation) {

    }

    @Override
    public boolean isValid(List<Orderitems> value, ConstraintValidatorContext context) {
        if (value != null) {
            for (Orderitems item : value) {
                if (!businessRulesManager.isValid(item)) {
                    return false;
                }
            }
        }
        return true;
    }

}
