package com.genfare.cloud.model.entity.repository;

import java.math.BigDecimal;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import com.genfare.cloud.model.entity.Subscriptions;
import com.genfare.cloud.model.entity.Subscriptionstatus;

public interface SubscriptionsRepository
    extends CrudRepository<Subscriptions, Long> {

    @Query("from Subscriptions s where s.walletcontents.walletContentsId = ?")
    public Subscriptions findByWalletContentsId(Long id);

    @Query("select s.subscriptionstatus from Subscriptions s where s.subscriptionId = ?")
    public Subscriptionstatus findStatusForSubscription(Long id);

    @Query("select s.replenishThreshold from Subscriptions s where s.subscriptionId = ?")
    public BigDecimal findThresholdForSubscription(Long id);

}
