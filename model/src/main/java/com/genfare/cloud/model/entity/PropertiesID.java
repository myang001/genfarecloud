package com.genfare.cloud.model.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class PropertiesID
    implements java.io.Serializable {

    private static final long serialVersionUID = 1L;

    private String category;

    private Byte languageID;

    public PropertiesID() {
    }

    public PropertiesID(String category, Byte languageID) {
        this.category = category;
        this.languageID = languageID;
    }

    @Column(name = "Category", nullable = false)
    public String getCategory() {
        return this.category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @Column(name = "LanguageID", nullable = false)
    public Byte getLanguageID() {
        return this.languageID;
    }

    public void setLanguageID(Byte languageID) {
        this.languageID = languageID;
    }

    @Override
    public boolean equals(Object other) {
        if ((this == other))
            return true;
        if ((other == null))
            return false;
        if (!(other instanceof PropertiesID))
            return false;
        PropertiesID castOther = (PropertiesID) other;

        return (this.category == castOther.category) && (this.languageID == castOther.languageID);
    }

    @Override
    public int hashCode() {
        int result = 17;

        result = 37 * result + category.hashCode();
        result = 37 * result + languageID;
        return result;
    }

}
