package com.genfare.cloud.model.entity;

// Generated Mar 19, 2014 9:47:20 AM by Hibernate Tools 4.0.0

import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Type;

import com.genfare.cloud.model.entity.inter.Auditable;
import com.genfare.cloud.model.listener.AuditListener;

/**
 * Organizationlinks generated by hbm2java
 */
@Entity
@Table(name = "organizationlinks")
@EntityListeners(AuditListener.class)
public class Organizationlinks
    implements java.io.Serializable, Auditable {

    private OrganizationlinksId id;

    private Organizations organizations;

    private Linktypes linktypes;

    private String linkValue;

    private Boolean isPreferred;

    private String touchedBy;

    private Date touchedAt;

    private String createdBy;

    private Date createdAt;

    public Organizationlinks() {
    }

    public Organizationlinks(OrganizationlinksId id, Organizations organizations, Linktypes linktypes) {
        this.id = id;
        this.organizations = organizations;
        this.linktypes = linktypes;
    }

    public Organizationlinks(OrganizationlinksId id, Organizations organizations, Linktypes linktypes,
        String linkValue, Boolean isPreferred, String touchedBy, Date touchedAt, String createdBy,
        Date createdAt) {
        this.id = id;
        this.organizations = organizations;
        this.linktypes = linktypes;
        this.linkValue = linkValue;
        this.isPreferred = isPreferred;
        this.touchedBy = touchedBy;
        this.touchedAt = touchedAt;
        this.createdBy = createdBy;
        this.createdAt = createdAt;
    }

    @EmbeddedId
    @AttributeOverrides({
        @AttributeOverride(name = "organizationId", column = @Column(name = "OrganizationID", nullable = false)),
        @AttributeOverride(name = "linkTypeId", column = @Column(name = "LinkTypeID", nullable = false)) })
    public OrganizationlinksId getId() {
        return this.id;
    }

    public void setId(OrganizationlinksId id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "OrganizationID", nullable = false, insertable = false, updatable = false)
    public Organizations getOrganizations() {
        return this.organizations;
    }

    public void setOrganizations(Organizations organizations) {
        this.organizations = organizations;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "LinkTypeID", nullable = false, insertable = false, updatable = false)
    public Linktypes getLinktypes() {
        return this.linktypes;
    }

    public void setLinktypes(Linktypes linktypes) {
        this.linktypes = linktypes;
    }

    @Column(name = "LinkValue", length = 256)
    public String getLinkValue() {
        return this.linkValue;
    }

    public void setLinkValue(String linkValue) {
        this.linkValue = linkValue;
    }

    @Column(name = "IsPreferred", length = 1)
    @Type(type = "true_false")
    public Boolean getIsPreferred() {
        return this.isPreferred;
    }

    public void setIsPreferred(Boolean isPreferred) {
        this.isPreferred = isPreferred;
    }

    @Column(name = "TouchedBy", nullable = false, length = 50)
    public String getTouchedBy() {
        return this.touchedBy;
    }

    @Override
    public void setTouchedBy(String touchedBy) {
        this.touchedBy = touchedBy;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "TouchedAt", nullable = false, length = 19)
    public Date getTouchedAt() {
        return this.touchedAt;
    }

    @Override
    public void setTouchedAt(Date touchedAt) {
        this.touchedAt = touchedAt;
    }

    @Override
    @Column(name = "CreatedBy", nullable = false, length = 50)
    public String getCreatedBy() {
        return this.createdBy;
    }

    @Override
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Override
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CreatedAt", nullable = false, length = 19)
    public Date getCreatedAt() {
        return this.createdAt;
    }

    @Override
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

}
