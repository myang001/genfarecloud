package com.genfare.cloud.model.support;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.flywaydb.core.Flyway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.genfare.cloud.model.tenant.TenantConfiguration;
import com.genfare.cloud.model.tenant.TenantConfigurationRepository;

/**
 * Starts the flyway migration on startup.
 */
@Component
public class FlywayStartup {

    @Autowired
    DataSource dataSource;

    @Autowired
    TenantConfigurationRepository tenantConfiguration;

    @PostConstruct
    public void MigrateDatabase() {
        for (TenantConfiguration config : tenantConfiguration.getAll()) {
            Flyway flyway = new Flyway();
            flyway.setDataSource(dataSource);
            flyway.setSchemas(config.getSchemaName());
            flyway.setValidateOnMigrate(false);

            if (config.isImportTestData()) {
                flyway.setLocations("db.migration", "db.testdata");
            } else {
                flyway.setLocations("db.migration");
            }
            flyway.migrate();
        }
    }
}
