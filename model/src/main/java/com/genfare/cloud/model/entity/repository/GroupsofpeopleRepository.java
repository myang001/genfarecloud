package com.genfare.cloud.model.entity.repository;

import org.springframework.data.repository.CrudRepository;

import com.genfare.cloud.model.entity.Groupsofpeople;
import com.genfare.cloud.model.entity.GroupsofpeopleId;

public interface GroupsofpeopleRepository
    extends CrudRepository<Groupsofpeople, GroupsofpeopleId> {

}
