package com.genfare.cloud.model.entity.repository;

import org.springframework.data.repository.CrudRepository;

import com.genfare.cloud.model.entity.Sales;

public interface SalesRepository
    extends CrudRepository<Sales, Integer> {

}
