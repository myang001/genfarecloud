package com.genfare.cloud.model.constraints;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

import com.genfare.cloud.model.annotation.PhoneNumber;
import com.genfare.cloud.model.service.EmailService;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.PhoneNumberUtil.PhoneNumberFormat;

public class PhoneNumberValidator
    implements ConstraintValidator<PhoneNumber, String> {

    @Autowired
    EmailService emailService;

    @Override
    public void initialize(PhoneNumber constraintAnnotation) {

    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {

        if (value == null || value.isEmpty()) {
            return true;
        }

        PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
        com.google.i18n.phonenumbers.Phonenumber.PhoneNumber phoneNumber;
        try {
            phoneNumber = phoneUtil.parse(value, "US");

            if (!phoneUtil.isValidNumber(phoneNumber)) {
                return false;
            }

            value = phoneUtil.format(phoneNumber, PhoneNumberFormat.E164);
        } catch (NumberParseException e) {
            return false;
        }

        return true;
    }

}
