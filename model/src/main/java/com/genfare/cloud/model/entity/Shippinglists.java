package com.genfare.cloud.model.entity;

// Generated Mar 19, 2014 9:47:20 AM by Hibernate Tools 4.0.0

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.genfare.cloud.model.entity.inter.Auditable;
import com.genfare.cloud.model.listener.AuditListener;

/**
 * Shippinglists generated by hbm2java
 */
@Entity
@Table(name = "shippinglists")
@EntityListeners(AuditListener.class)
public class Shippinglists
    implements java.io.Serializable, Auditable {

    private Integer shippingListId;

    private Wallettypes wallettypes;

    private String supplier;

    private Date dateShipped;

    private Date dateReceived;

    private Byte numberPackages;

    private Integer quantity;

    private String touchedBy;

    private Date touchedAt;

    private String createdBy;

    private Date createdAt;

    private Set<Lots> lotses = new HashSet<Lots>(0);

    public Shippinglists() {
    }

    public Shippinglists(Wallettypes wallettypes, String supplier, Date dateShipped, Date dateReceived,
        Byte numberPackages, Integer quantity, Set<Lots> lotses) {
        this.wallettypes = wallettypes;
        this.supplier = supplier;
        this.dateShipped = dateShipped;
        this.dateReceived = dateReceived;
        this.numberPackages = numberPackages;
        this.quantity = quantity;
        this.lotses = lotses;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "ShippingListID", unique = true, nullable = false)
    public Integer getShippingListId() {
        return this.shippingListId;
    }

    public void setShippingListId(Integer shippingListId) {
        this.shippingListId = shippingListId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "WalletTypeID")
    public Wallettypes getWallettypes() {
        return this.wallettypes;
    }

    public void setWallettypes(Wallettypes wallettypes) {
        this.wallettypes = wallettypes;
    }

    @Column(name = "Supplier", length = 50)
    public String getSupplier() {
        return this.supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "DateShipped", length = 10)
    public Date getDateShipped() {
        return this.dateShipped;
    }

    public void setDateShipped(Date dateShipped) {
        this.dateShipped = dateShipped;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "DateReceived", length = 10)
    public Date getDateReceived() {
        return this.dateReceived;
    }

    public void setDateReceived(Date dateReceived) {
        this.dateReceived = dateReceived;
    }

    @Column(name = "NumberPackages")
    public Byte getNumberPackages() {
        return this.numberPackages;
    }

    public void setNumberPackages(Byte numberPackages) {
        this.numberPackages = numberPackages;
    }

    @Column(name = "Quantity")
    public Integer getQuantity() {
        return this.quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    @Column(name = "TouchedBy", nullable = false, length = 50)
    public String getTouchedBy() {
        return this.touchedBy;
    }

    @Override
    public void setTouchedBy(String touchedBy) {
        this.touchedBy = touchedBy;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "TouchedAt", nullable = false, length = 19)
    public Date getTouchedAt() {
        return this.touchedAt;
    }

    @Override
    public void setTouchedAt(Date touchedAt) {
        this.touchedAt = touchedAt;
    }

    @Override
    @Column(name = "CreatedBy", nullable = false, length = 50)
    public String getCreatedBy() {
        return this.createdBy;
    }

    @Override
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Override
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CreatedAt", nullable = false, length = 19)
    public Date getCreatedAt() {
        return this.createdAt;
    }

    @Override
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "shippinglists")
    public Set<Lots> getLotses() {
        return this.lotses;
    }

    public void setLotses(Set<Lots> lotses) {
        this.lotses = lotses;
    }

}
