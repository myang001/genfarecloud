package com.genfare.cloud.model.entity.repository;

import org.springframework.data.repository.CrudRepository;

import com.genfare.cloud.model.entity.Operators;

public interface OperatorsRepository
    extends CrudRepository<Operators, Integer> {

}
