package com.genfare.cloud.model.service;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.genfare.cloud.model.entity.Orderitems;
import com.genfare.cloud.model.entity.Orders;
import com.genfare.cloud.model.entity.Tickets;
import com.genfare.cloud.model.entity.Tickettypes;
import com.genfare.cloud.model.entity.Walletcontents;
import com.genfare.cloud.model.entity.Wallets;
import com.genfare.cloud.model.entity.Wallettypes;
import com.genfare.cloud.model.tenant.TenantConfiguration;
import com.genfare.cloud.model.tenant.impl.BusinessRuleConfigurationType;
import com.genfare.cloud.model.tenant.impl.BusinessRuleTicketTypeLimitsType;
import com.genfare.cloud.model.tenant.impl.BusinessRuleValueTicketValueLimitsType;
import com.genfare.cloud.model.types.TickettypesType;

@Service
public class BusinessRulesServiceImpl
    implements BusinessRulesService {

    private static final Logger LOGGER = LoggerFactory.getLogger(BusinessRulesServiceImpl.class);

    @Autowired
    private WalletManagementService walletManagementService;

    @Autowired
    private OrderManagementService orderManagementService;

    @Autowired
    private TenantConfiguration tenantconfiguration;

    @Override
    public boolean addMoreProducts(Orders o) {
        int itemCount = 0;
        int availableSlots = 0;

        if (null == o) {
            return true;
        }
        List<Orderitems> oi = o.getOrderitemses();
        for (Orderitems i : oi) {
            Wallettypes wt = i.getWallettypes();
            if (null != wt) {
                availableSlots = wt.getAvailableSlots();
            }
        }
        for (Orderitems i : oi) {
            Wallettypes wt = i.getWallettypes();
            if (null == wt) {
                itemCount++;
            }
        }
        if (availableSlots <= itemCount) {
            return false;
        }

        return true;
    }

    @Override
    public boolean purchaseNewProducts(Wallets w) {

        if (productCountMaxExceeded(w)) {
            return false;
        }

        return true;
    }

    @Override
    public boolean selectNewProducts(Wallets w) {

        if (productCountMaxExceeded(w)) {
            return false;
        }

        return true;
    }

    @Override
    public boolean buyMoreProducts(Walletcontents wc) {

        if (productTypeLimitExceeded(wc)) {
            return false;
        }

        if (pendingMaxExceeded(wc)) {
            return false;
        }

        if (valueProductValueExceeded(wc)) {
            return false;
        }

        if (periodCardLifeExceeded(wc)) {
            return false;
        }

        return true;
    }

    @Override
    public boolean isValid(Orderitems oi) {
        if (oi != null) {
            Wallets w = oi.getWallets();
            if (oi.getTickets() != null) { // its a ticket orderitem, not a card orderitem
                // new card
                if (null == w) {
                    if (productTypeLimitExceeded(oi)) {
                        return false;
                    }

                    if (valueProductValueExceeded(oi)) {
                        return false;
                    }
                }
                // existing card
                else {
                    // new or existing product
                    // should be only one entry
                    if (purchaseNewProducts(w)) {
                        return false;
                    }
                    for (Walletcontents wc : oi.getTickets().getWalletcontentses()) {
                        if (buyMoreProducts(wc)) {
                            return false;
                        }
                    }
                }
            }
        }
        return true;
    }

    @Override
    public boolean isValid(Orders o) {
        if (o != null) {
            for (Orderitems oi : o.getOrderitemses()) {
                if (false == isValid(oi)) {
                    LOGGER.debug("order not valid");
                    return false;
                }
            }
        }
        return true;
    }

    boolean productTypeLimitExceeded(Walletcontents wc) {
        int pendingCount = walletManagementService.getPendingItemsCount(wc);
        List<BusinessRuleTicketTypeLimitsType> brtt = tenantconfiguration.getBusinessRuleTicketTypeLimits();
        for (BusinessRuleTicketTypeLimitsType limit : brtt) {
            if (limit.getTicketId() == wc.getTickets().getTickettypes().getTicketTypeId()) {
                if (limit.getTicketLimit() >= pendingCount)
                    return false;
            }
        }

        LOGGER.debug("Walletcontents, product type limit exceeded");
        return true;
    }

    boolean productTypeLimitExceeded(Orderitems oi) {
        List<BusinessRuleTicketTypeLimitsType> brtt = tenantconfiguration.getBusinessRuleTicketTypeLimits();
        for (BusinessRuleTicketTypeLimitsType limit : brtt) {
            if (limit.getTicketId() == oi.getTickets().getTickettypes().getTicketTypeId()) {
                if (limit.getTicketLimit() >= oi.getQuantity())
                    return false;
            }
        }

        LOGGER.debug("Orderitems, product type limit exceeded");
        return true;
    }

    boolean valueProductValueExceeded(Walletcontents wc) {

        // TODO add tenant config listing value types and their limits
        List<Orderitems> oi = walletManagementService.getPendingItems(wc);
        int orderValue = 0;
        for (Orderitems i : oi) {
            orderValue += i.getAmount().intValue();
        }

        List<BusinessRuleValueTicketValueLimitsType> brvtl = tenantconfiguration.getBusinessRuleValueTicketValueLimits();
        for (BusinessRuleValueTicketValueLimitsType limit : brvtl) {
            if (limit.getTicketId() == wc.getTickets().getTickettypes().getTicketTypeId()) {
                if (limit.getValueLimit() < (wc.getValueRemaining().intValue() + orderValue))
                    return true;
            }
        }

        LOGGER.debug("Walletcontents, value product value exceeded");
        return false;
    }

    boolean valueProductValueExceeded(Orderitems oi) {

        List<BusinessRuleValueTicketValueLimitsType> brvtl = tenantconfiguration.getBusinessRuleValueTicketValueLimits();
        for (BusinessRuleValueTicketValueLimitsType limit : brvtl) {
            if (limit.getTicketId() == oi.getTickets().getTickettypes().getTicketTypeId()) {
                if (limit.getValueLimit() < (oi.getAmount().intValue()))
                    return true;
            }
        }

        LOGGER.debug("Orderitems, value product value exceeded");
        return false;
    }

    boolean pendingMaxExceeded(Walletcontents wc) {
        int pendingCount = walletManagementService.getPendingItemsCount(wc);
        BusinessRuleConfigurationType br = tenantconfiguration.getBusinessRuleConfiguration();
        int pcLimit = br.getRulePendingCount();

        if (null != wc && null == wc.getPendingCount()) {
            return false;
        }

        if (null != wc && (wc.getPendingCount() + pendingCount) < pcLimit) {

            return false;
        }

        LOGGER.debug("pending max exceeded");
        return true;
    }

    boolean productCountMaxExceeded(Wallets w) {
        if (w == null) {
            return false;
        }

        BusinessRuleConfigurationType br = tenantconfiguration.getBusinessRuleConfiguration();
        int productCount = br.getRuleProductCount();

        // TODO make tenant config limits associated with wallet type
        if (w.getWalletcontentses().size() < productCount) {
            return false;
        }

        LOGGER.debug("product count max exceeded");
        return true;
    }

    private boolean periodCardLifeExceeded(Walletcontents wc) {
        Tickettypes type = wc.getTickets().getTickettypes();
        if (!type.equals(TickettypesType.FIXED_PERIOD_PASS.getIndex()) &&
            !type.equals(TickettypesType.FLOATING_PERIOD_PASS.getIndex())) {
            return false;
        }

        BusinessRuleConfigurationType br = tenantconfiguration.getBusinessRuleConfiguration();
        int secPerDay = 86400;
        long msRemainingActive = 0L;
        long daysRemainingActive = 0L;
        int pp = br.getRuleProbePeriod();
        int pendingOrderCount = walletManagementService.getPendingItemsCount(wc);

        Byte pc = wc.getPendingCount();
        pc = (null == pc) ? 0 : pc;
        Wallets wallet = wc.getWallets();
        Date walletExpiration = wallet.getDateExpires();
        Tickets ticket = wc.getTickets();

        // Days remaining on an active pass
        Date today = new Date();
        if (null != wc.getDateStart()) {
            msRemainingActive = wc.getDateStart().getTime() - today.getTime();
            daysRemainingActive = (msRemainingActive / (secPerDay * 1000));
        }

        long daysOwned = (ticket.getValue().intValue() * (pc + pendingOrderCount)) + daysRemainingActive;
        long daysTilExpired = (walletExpiration.getTime() - today.getTime()) / (1000 * secPerDay);
        // Rolling start period pass, Test is current days plus one instance of this ticket value (this purchase)
        if (type.equals(TickettypesType.FIXED_PERIOD_PASS.getIndex()) &&
            (daysTilExpired > (daysOwned + ticket.getValue().intValue()))) {
            return false;
        }

        // Fixed period pass, DateExpires
        if (type.equals(TickettypesType.FIXED_PERIOD_PASS.getIndex()) &&
            ticket.getDateExpires().before(walletExpiration)) {
            return false;
        }

        LOGGER.debug("product period exceeded wallet life");
        return true;
    }

}
