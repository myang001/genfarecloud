package com.genfare.cloud.model.entity.repository;

import org.springframework.data.repository.CrudRepository;

import com.genfare.cloud.model.entity.Organizationtypes;

public interface OrganizationtypesRepository
    extends CrudRepository<Organizationtypes, Byte> {

}
