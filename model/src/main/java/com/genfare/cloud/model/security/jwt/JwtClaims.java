package com.genfare.cloud.model.security.jwt;

import org.json.JSONObject;

public class JwtClaims {

    private final String iss;
    private final Long iat;
    private final Long exp;
    private final String qsh;
    private final String sub;

    private JwtClaims(Builder builder) {
        this.iss = builder.iss;
        this.iat = builder.iat;
        this.exp = builder.exp;
        this.qsh = builder.qsh;
        this.sub = builder.sub;
    }

    public String getIss() {
        return iss;
    }

    public Long getIat() {
        return iat;
    }

    public Long getExp() {
        return exp;
    }

    public String getQsh() {
        return qsh;
    }

    public String getSub() {
        return sub;
    }

    public String toJson() {
        return new JSONObject(this).toString();
    }

    public static JwtClaims fromJson(String json) {
        JSONObject obj = new JSONObject(json);
        JwtClaims.Builder claims = new JwtClaims.Builder(
            obj.getString("iss"),
            obj.getLong("iat"),
            obj.getLong("exp"),
            obj.getString("qsh")
            );
        if (obj.has("sub")) {
            claims = claims.sub(obj.getString("sub"));
        }
        return claims.build();
    }

    public static class Builder {
        private String iss;
        private Long iat;
        private Long exp;
        private String qsh;
        private String sub;

        public Builder(String iss, Long iat, Long exp, String qsh) {
            this.iss = iss;
            this.iat = iat;
            this.exp = exp;
            this.qsh = qsh;
        }

        public Builder sub(String sub) {
            this.sub = sub;
            return this;
        }

        public JwtClaims build() {
            return new JwtClaims(this);
        }
    }
}
