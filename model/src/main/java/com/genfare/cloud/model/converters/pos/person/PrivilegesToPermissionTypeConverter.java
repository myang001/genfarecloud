package com.genfare.cloud.model.converters.pos.person;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;

import com.genfare.cloud.model.entity.Privileges;
import com.genfare.pos.core.model.person.PermissionType;

public class PrivilegesToPermissionTypeConverter implements
    Converter<Privileges, PermissionType> {

    private static final Logger log = LoggerFactory
        .getLogger(PrivilegesToPermissionTypeConverter.class);

    @Override
    public PermissionType convert(Privileges p) {
        PermissionType pt = new PermissionType();
        pt.setPermissionId(p.getPrivilegeId());
        pt.setName(p.getName());
        ComponentsToComponentTypeConverter c2ctc = new ComponentsToComponentTypeConverter();
        pt.setComponent(c2ctc.convert(p.getComponents()));
        return pt;
    }
}
