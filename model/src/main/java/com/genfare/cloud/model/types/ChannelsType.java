package com.genfare.cloud.model.types;

import java.util.EnumSet;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.genfare.cloud.model.entity.Channels;
import com.genfare.cloud.model.entity.repository.ChannelsRepository;

public enum ChannelsType {
    EFARE(1);

    private final Integer index;

    private Channels type;

    private ChannelsType(Integer index) {
        this.index = index;
    }

    public Integer getIndex() {
        return index;
    }

    public Channels getType() {
        return type;
    }

    public static ChannelsType findType(String description) {
        for (ChannelsType type : EnumSet.allOf(ChannelsType.class)) {
            if (type.type.getDescription().equals(description)) {
                return type;
            }
        }
        return null;
    }

    @Component
    public static class Injector {
        @Autowired
        private ChannelsRepository repo;

        @PostConstruct
        public void setup() {
            for (ChannelsType type : EnumSet.allOf(ChannelsType.class)) {
                type.type = repo.findOne(type.index.byteValue());
            }
        }
    }
}
