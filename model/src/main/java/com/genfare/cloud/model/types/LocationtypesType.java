package com.genfare.cloud.model.types;

import java.util.EnumSet;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.genfare.cloud.model.entity.Locationtypes;
import com.genfare.cloud.model.entity.repository.LocationtypesRepository;

public enum LocationtypesType {
    LOCAL(1),
    FACTORY(2),
    INTERNATIONAL(3),
    US(4),
    ResourceLocation(5);

    private final Integer index;

    private Locationtypes type;

    private LocationtypesType(Integer index) {
        this.index = index;
    }

    public Integer getIndex() {
        return index;
    }

    public Locationtypes getType() {
        return type;
    }

    @Component
    public static class Injector {
        @Autowired
        private LocationtypesRepository repo;

        @PostConstruct
        public void setup() {
            for (LocationtypesType type : EnumSet.allOf(LocationtypesType.class)) {
                type.type = repo.findOne(type.index.byteValue());
            }
        }
    }
}
