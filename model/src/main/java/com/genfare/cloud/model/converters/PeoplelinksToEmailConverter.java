package com.genfare.cloud.model.converters;

import com.genfare.cloud.model.contact.Email;
import com.genfare.cloud.model.entity.Peoplelinks;

public class PeoplelinksToEmailConverter {

    public static Email convert(Peoplelinks p) {

        if (p == null) {
            return null;
        }

        Email e = new Email();
        e.setAddress(p.getLinkValue());
        e.setDescription(p.getLinktypes().getDescription());
        return e;
    }

}
