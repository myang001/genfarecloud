package com.genfare.cloud.model.types;

import java.util.EnumSet;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.genfare.cloud.model.entity.Farecodes;
import com.genfare.cloud.model.entity.repository.FarecodesRepository;

public enum FarecodesType {
    FULL(1),
    SENIOR(2),
    YOUTH(3),
    VETERAN(4),
    EMPLOYEE(5),
    EMPLOYER_SPONSORED(6),
    DISABLED(7);

    private final Integer index;

    private Farecodes type;

    private FarecodesType(Integer index) {
        this.index = index;
    }

    public Integer getIndex() {
        return index;
    }

    public Farecodes getType() {
        return type;
    }

    @Component
    public static class Injector {
        @Autowired
        private FarecodesRepository repo;

        @PostConstruct
        public void setup() {
            for (FarecodesType type : EnumSet.allOf(FarecodesType.class)) {
                type.type = repo.findOne(type.index.byteValue());
            }
        }
    }
}