package com.genfare.cloud.model.annotation;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.genfare.cloud.model.constraints.EmailAvailableValidator;

@Target({ FIELD, ANNOTATION_TYPE })
@Retention(RUNTIME)
@Constraint(validatedBy = EmailAvailableValidator.class)
public @interface EmailAvailable {

    String message() default "{validator.emailavailable}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
