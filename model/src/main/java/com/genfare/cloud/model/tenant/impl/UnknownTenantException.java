package com.genfare.cloud.model.tenant.impl;

public class UnknownTenantException
    extends RuntimeException {

    private static final long serialVersionUID = 5042401014596892024L;

    private String tenantIdentifier;

    public UnknownTenantException(String tenantIdentifier) {
        this.tenantIdentifier = tenantIdentifier;
    }

    @Override
    public String getMessage() {
        return "Could not find tenant with identifier '" + tenantIdentifier + "'";
    }
}
