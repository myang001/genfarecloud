@javax.xml.bind.annotation.XmlSchema(
    namespace = "http://genfare.com/cloud/model/om",
    xmlns = {
        @javax.xml.bind.annotation.XmlNs(prefix = "om", namespaceURI = "http://genfare.com/cloud/model/om")
    },
    elementFormDefault = javax.xml.bind.annotation.XmlNsForm.QUALIFIED)
package com.genfare.cloud.model.converters.order;

