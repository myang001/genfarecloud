package com.genfare.cloud.model.entity;

// Generated Mar 19, 2014 9:47:20 AM by Hibernate Tools 4.0.0

import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.genfare.cloud.model.entity.inter.Auditable;
import com.genfare.cloud.model.listener.AuditListener;

@Entity
@Table(name = "ticketsfarecodes")
@EntityListeners(AuditListener.class)
public class TicketsFarecodes
    implements java.io.Serializable, Auditable {

    private TicketsFarecodesId id;

    private Tickets tickets;

    private Farecodes farecodes;

    private String touchedBy;

    private Date touchedAt;

    private String createdBy;

    private Date createdAt;

    public TicketsFarecodes() {
    }

    public TicketsFarecodes(TicketsFarecodesId id, Tickets tickets, Farecodes farecodes) {
        this.id = id;
        this.tickets = tickets;
        this.farecodes = farecodes;
    }

    @EmbeddedId
    @AttributeOverrides({ @AttributeOverride(name = "ticketId", column = @Column(name = "TicketID", nullable = false)),
        @AttributeOverride(name = "farecodeId", column = @Column(name = "FarecodeID", nullable = false)) })
    public TicketsFarecodesId getId() {
        return this.id;
    }

    public void setId(TicketsFarecodesId id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TicketID", nullable = false, insertable = false, updatable = false)
    public Tickets getTickets() {
        return this.tickets;
    }

    public void setTickets(Tickets tickets) {
        this.tickets = tickets;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FarecodeID", nullable = false, insertable = false, updatable = false)
    public Farecodes getFarecodes() {
        return this.farecodes;
    }

    public void setFarecodes(Farecodes farecodes) {
        this.farecodes = farecodes;
    }

    @Column(name = "TouchedBy", nullable = false, length = 50)
    public String getTouchedBy() {
        return this.touchedBy;
    }

    @Override
    public void setTouchedBy(String touchedBy) {
        this.touchedBy = touchedBy;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "TouchedAt", nullable = false, length = 19)
    public Date getTouchedAt() {
        return this.touchedAt;
    }

    @Override
    public void setTouchedAt(Date touchedAt) {
        this.touchedAt = touchedAt;
    }

    @Override
    @Column(name = "CreatedBy", nullable = false, length = 50)
    public String getCreatedBy() {
        return this.createdBy;
    }

    @Override
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Override
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CreatedAt", nullable = false, length = 19)
    public Date getCreatedAt() {
        return this.createdAt;
    }

    @Override
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

}
