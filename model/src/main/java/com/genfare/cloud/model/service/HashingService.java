package com.genfare.cloud.model.service;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

public interface HashingService {

    public String md5Hash(String value);

    public String sha1Hash(String value);

    public String springBCryptHash(String value);

    public String springBCryptHash(String value, int strength);

    public String hmacDigest(String msg, String keyString, String algo);

    public String hmacSha1(String key, String message) throws NoSuchAlgorithmException, InvalidKeyException;
}
