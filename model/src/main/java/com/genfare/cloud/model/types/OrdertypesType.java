package com.genfare.cloud.model.types;

import java.util.EnumSet;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.genfare.cloud.model.entity.Ordertypes;
import com.genfare.cloud.model.entity.repository.OrdertypesRepository;

public enum OrdertypesType {
    SALES(1),
    SERVICE(2);

    private final Integer index;

    private Ordertypes type;

    private OrdertypesType(Integer index) {
        this.index = index;
    }

    public Integer getIndex() {
        return index;
    }

    public Ordertypes getType() {
        return type;
    }

    @Component
    public static class Injector {
        @Autowired
        private OrdertypesRepository repo;

        @PostConstruct
        public void setup() {
            for (OrdertypesType type : EnumSet.allOf(OrdertypesType.class)) {
                type.type = repo.findOne(type.index.byteValue());
            }
        }
    }
}
