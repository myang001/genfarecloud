package com.genfare.cloud.model.converters.pos.activity;

import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;

import com.genfare.cloud.model.entity.Activities;
import com.genfare.cloud.model.entity.Activityattributes;
import com.genfare.pos.core.model.activity.ActivityType;

public class ActivityToActivityTypeConverter implements
    Converter<Activities, ActivityType> {

    private static final Logger log = LoggerFactory
        .getLogger(ActivityToActivityTypeConverter.class);

    @Override
    public ActivityType convert(Activities a) {
        GregorianCalendar c = new GregorianCalendar();
        ActivityType at = new ActivityType();

        at.setEquipmentId(a.getEquipment().getEquipmentId());
        at.setPersonId(a.getOperators().getOperatorId());
        try {
            c.setTime(a.getDateActivity());
            at.setDateActivity(DatatypeFactory.newInstance()
                .newXMLGregorianCalendar(c));
        } catch (DatatypeConfigurationException e) {
            log.error("Error converting date", e);
            return null;
        }

        at.setDescription(a.getDescription());
        ActivityAttributesToActivityAttributesTypeConverter aac = new ActivityAttributesToActivityAttributesTypeConverter();
        for (Activityattributes aa : a.getActivityattributeses()) {
            at.getActivityAttribute().add(aac.convert(aa));
        }

        return at;
    }
}
