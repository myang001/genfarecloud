package com.genfare.cloud.model.entity.repository;

import org.springframework.data.repository.CrudRepository;

import com.genfare.cloud.model.entity.Purposes;

public interface PurposesRepository
    extends CrudRepository<Purposes, Byte> {

}
