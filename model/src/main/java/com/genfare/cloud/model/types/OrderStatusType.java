package com.genfare.cloud.model.types;

import com.genfare.cloud.model.entity.Orderstatuses;

public enum OrderStatusType {
    SUBMITTED(OrderStatusBaseType.SUBMITTED),
    UNSUBMITTED(OrderStatusBaseType.UNSUBMITTED),
    IN_PROGRESS(OrderStatusBaseType.IN_PROGRESS),
    FULFILLMENT(
        OrderStatusBaseType.FULFILLMENT),
    ABANDONED(OrderStatusBaseType.ABANDONED),
    COMPLETE(
        OrderStatusBaseType.COMPLETE),
    CANCELLED(OrderStatusBaseType.CANCELLED),
    HOLD(OrderStatusBaseType.HOLD);

    private final OrderStatusBaseType type;

    private OrderStatusType(OrderStatusBaseType type) {
        this.type = type;
    }

    /**
     * Gets the ordinal for the base type as in the DB
     * 
     * @return the position of the base type
     */
    public Integer getIndex() {
        return type.getIndex();
    }

    /**
     * Gets the entity representing the status
     * 
     * @return The Orderstatus entity
     */
    public Orderstatuses getStatus() {
        return type.getStatus();
    }
}
