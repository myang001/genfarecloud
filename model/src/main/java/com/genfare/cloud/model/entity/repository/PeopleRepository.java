package com.genfare.cloud.model.entity.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.genfare.cloud.model.contact.Address;
import com.genfare.cloud.model.entity.Farecodes;
import com.genfare.cloud.model.entity.Groups;
import com.genfare.cloud.model.entity.People;

public interface PeopleRepository
    extends CrudRepository<People, Integer> {

    @Query("from Peopleaddresses address where address.id.personId = ?")
    public List<Address> findAddressByPersonId(Integer id);

    @Query("select g.people from Groupsofpeople g where g.groups.groupId = ?")
    public List<People> findPeopleByGroup(Integer groupid);

    @Query("from People p where p.status.status = ?")
    public List<People> findPeopleByStatus(Byte status);

    @Query("from People p where p.nameFirst like :name or p.nameLast like :name")
    public List<People> findPeopleByFirstOrLastName(@Param("name") String name);

    @Query("from People p where p.nameFirst = :first and p.nameLast = :last")
    public List<People> getPeopleByFirstAndLastName(@Param("first") String first, @Param("last") String last);

    @Query("select p from People p join p.groups g where p.nameFirst = :first and p.nameLast = :last and g.organizations.organizationId = :org")
    public People getPeopleByFirstAndLastNameInOrg(@Param("first") String first, @Param("last") String last, @Param("org") Integer orgId);

    @Query("select distinct p from People p join p.groups g where p.nameFirst like :first or p.nameLast like :last and g.organizations.organizationId = :org")
    public List<People> searchPeopleByFirstOrLastNameInOrg(@Param("first") String first, @Param("last") String last, @Param("org") Integer orgId);

    @Query("select pl.people from Peoplelinks pl where pl.linkValue = ?")
    public People findPeopleByExactLink(String link);

    @Query("select pl.people from Peoplelinks pl where pl.linkValue like :link")
    public List<People> findPeopleByLink(@Param("link") String link);

    @Query("select gp.groups from People p join p.groupsofpeoples gp where p.personId = ?")
    public List<Groups> findGroupsForPerson(Integer id);

    @Query("select o.people from Orders o where o.orderId = ?")
    public People findPersonByOrder(Integer orderId);

    @Query("from People p left join fetch p.groups where p.personId = ?")
    public People findOneWithGroups(Integer id);

    @Query("from People p where p.personId = ?")
    public People findByPersonId(Integer id);

    @Query("select p.farecodes from People p where p.personId = ?")
    public List<Farecodes> findFarecodesForPerson(Integer id);

    @Query("select p from People p inner join p.loginses l where l.userLogin = ?")
    public People findByLogin(String login);

}
