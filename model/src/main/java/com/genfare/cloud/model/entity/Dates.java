package com.genfare.cloud.model.entity;

// Generated Mar 19, 2014 9:47:20 AM by Hibernate Tools 4.0.0

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Dates generated by hbm2java
 */
@Entity
@Table(name = "dates")
public class Dates
    implements java.io.Serializable {

    private Integer dateId;

    private Date dateActivity;

    private Date dateTransit;

    private short year;

    private Byte month;

    private Byte quarter;

    private Set<Sales> saleses = new HashSet<Sales>(0);

    private Set<Activitysummary> activitysummaries = new HashSet<Activitysummary>(0);

    public Dates() {
    }

    public Dates(short year) {
        this.year = year;
    }

    public Dates(Date dateActivity, Date dateTransit, short year, Byte month, Byte quarter, Set<Sales> saleses,
        Set<Activitysummary> activitysummaries) {
        this.dateActivity = dateActivity;
        this.dateTransit = dateTransit;
        this.year = year;
        this.month = month;
        this.quarter = quarter;
        this.saleses = saleses;
        this.activitysummaries = activitysummaries;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "DateID", unique = true, nullable = false)
    public Integer getDateId() {
        return this.dateId;
    }

    public void setDateId(Integer dateId) {
        this.dateId = dateId;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DateActivity", length = 19)
    public Date getDateActivity() {
        return this.dateActivity;
    }

    public void setDateActivity(Date dateActivity) {
        this.dateActivity = dateActivity;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "DateTransit", length = 10)
    public Date getDateTransit() {
        return this.dateTransit;
    }

    public void setDateTransit(Date dateTransit) {
        this.dateTransit = dateTransit;
    }

    @Column(name = "Year", nullable = false)
    public short getYear() {
        return this.year;
    }

    public void setYear(short year) {
        this.year = year;
    }

    @Column(name = "Month")
    public Byte getMonth() {
        return this.month;
    }

    public void setMonth(Byte month) {
        this.month = month;
    }

    @Column(name = "Quarter")
    public Byte getQuarter() {
        return this.quarter;
    }

    public void setQuarter(Byte quarter) {
        this.quarter = quarter;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "dates")
    public Set<Sales> getSaleses() {
        return this.saleses;
    }

    public void setSaleses(Set<Sales> saleses) {
        this.saleses = saleses;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "dates")
    public Set<Activitysummary> getActivitysummaries() {
        return this.activitysummaries;
    }

    public void setActivitysummaries(Set<Activitysummary> activitysummaries) {
        this.activitysummaries = activitysummaries;
    }

}
