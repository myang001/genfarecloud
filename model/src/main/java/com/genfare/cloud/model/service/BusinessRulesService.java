package com.genfare.cloud.model.service;

import com.genfare.cloud.model.entity.Orderitems;
import com.genfare.cloud.model.entity.Orders;
import com.genfare.cloud.model.entity.Walletcontents;
import com.genfare.cloud.model.entity.Wallets;

public interface BusinessRulesService {
    public boolean purchaseNewProducts(Wallets w);

    public boolean buyMoreProducts(Walletcontents wc);

    public boolean selectNewProducts(Wallets w);

    public boolean isValid(Orderitems oi);

    public boolean isValid(Orders o);

    public boolean addMoreProducts(Orders o);

}
