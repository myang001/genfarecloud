package com.genfare.cloud.model.entity;

// Generated Mar 19, 2014 9:47:20 AM by Hibernate Tools 4.0.0

import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.genfare.cloud.model.entity.inter.Auditable;
import com.genfare.cloud.model.listener.AuditListener;

/**
 * Groupprivileges generated by hbm2java
 */
@Entity
@Table(name = "groupprivileges")
@EntityListeners(AuditListener.class)
public class Groupprivileges
    implements java.io.Serializable, Auditable {

    private GroupprivilegesId id;

    private Groups groups;

    private Privileges privileges;

    private String touchedBy;

    private Date touchedAt;

    private String createdBy;

    private Date createdAt;

    public Groupprivileges() {
    }

    public Groupprivileges(GroupprivilegesId id, Groups groups, Privileges privileges) {
        this.id = id;
        this.groups = groups;
        this.privileges = privileges;
    }

    @EmbeddedId
    @AttributeOverrides({ @AttributeOverride(name = "groupId", column = @Column(name = "GroupID", nullable = false)),
        @AttributeOverride(name = "privilegeId", column = @Column(name = "PrivilegeID", nullable = false)) })
    public GroupprivilegesId getId() {
        return this.id;
    }

    public void setId(GroupprivilegesId id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "GroupID", nullable = false, insertable = false, updatable = false)
    public Groups getGroups() {
        return this.groups;
    }

    public void setGroups(Groups groups) {
        this.groups = groups;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "PrivilegeID", nullable = false, insertable = false, updatable = false)
    public Privileges getPrivileges() {
        return this.privileges;
    }

    public void setPrivileges(Privileges privileges) {
        this.privileges = privileges;
    }

    @Column(name = "TouchedBy", nullable = false, length = 50)
    public String getTouchedBy() {
        return this.touchedBy;
    }

    @Override
    public void setTouchedBy(String touchedBy) {
        this.touchedBy = touchedBy;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "TouchedAt", nullable = false, length = 19)
    public Date getTouchedAt() {
        return this.touchedAt;
    }

    @Override
    public void setTouchedAt(Date touchedAt) {
        this.touchedAt = touchedAt;
    }

    @Override
    @Column(name = "CreatedBy", nullable = false, length = 50)
    public String getCreatedBy() {
        return this.createdBy;
    }

    @Override
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Override
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CreatedAt", nullable = false, length = 19)
    public Date getCreatedAt() {
        return this.createdAt;
    }

    @Override
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

}
