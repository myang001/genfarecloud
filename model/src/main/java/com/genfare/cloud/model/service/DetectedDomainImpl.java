package com.genfare.cloud.model.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

import com.genfare.cloud.model.tenant.DetectedDomain;

@Component
@Scope(value = "request", proxyMode = ScopedProxyMode.INTERFACES)
public class DetectedDomainImpl
    implements DetectedDomain {

    private static final Logger LOGGER = LoggerFactory.getLogger(DetectedDomainImpl.class);

    private String domain;

    public DetectedDomainImpl() {
        LOGGER.info("CREATE DetectedDomainImpl");
    }

    public DetectedDomainImpl(String domain) {
        super();
        this.domain = domain;
    }

    @Override
    public String getDomain() {
        return domain;
    }

    @Override
    public void setDomain(String domain) {
        this.domain = domain;
    }

}
