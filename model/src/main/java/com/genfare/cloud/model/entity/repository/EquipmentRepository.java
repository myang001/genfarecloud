package com.genfare.cloud.model.entity.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.genfare.cloud.model.entity.Equipment;

public interface EquipmentRepository
    extends CrudRepository<Equipment, Integer> {

    @Query("from Equipment e where e.serialNumber = ?")
    public List<Equipment> findByEquipmentSerial(String serial);
}
