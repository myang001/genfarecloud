/**
 *
 */
package com.genfare.cloud.model.converters;

import com.genfare.cloud.model.entity.People;
import com.genfare.cloud.model.om.ObjectFactory;
import com.genfare.cloud.model.om.PeopleType;

/**
 * @author mpeter
 */
public class PeopleToPeopleType {

    /**
     * @param people
     * @return
     */
    public PeopleType convert(People p) {
        ObjectFactory of = new ObjectFactory();
        PeopleType pt = of.createPeopleType();

        pt.setPrefix(p.getPrefix());
        pt.setFirst(p.getNameFirst());
        pt.setMiddle(p.getNameMiddle());
        pt.setLast(p.getNameLast());
        pt.setSuffix(p.getSuffix());
        return pt;
    }
}
