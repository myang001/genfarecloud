package com.genfare.cloud.model.converters.pos.person;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;

import com.genfare.cloud.model.entity.Groups;
import com.genfare.cloud.model.entity.Privileges;
import com.genfare.pos.core.model.person.GroupType;

public class GroupsToGroupTypeConverter implements Converter<Groups, GroupType> {

    private static final Logger log = LoggerFactory
        .getLogger(GroupsToGroupTypeConverter.class);

    @Override
    public GroupType convert(Groups g) {
        GroupType gt = new GroupType();
        gt.setGroupId(g.getGroupId());
        gt.setName(g.getName());
        PrivilegesToPermissionTypeConverter p2ptc = new PrivilegesToPermissionTypeConverter();
        for (Privileges p : g.getPrivileges()) {
            gt.getPermission().add(p2ptc.convert(p));
        }
        return gt;
    }
}
