package com.genfare.cloud.model.entity.repository;

import org.springframework.data.repository.CrudRepository;

import com.genfare.cloud.model.entity.GroupStatus;

public interface GroupStatusRepository extends CrudRepository<GroupStatus, Byte> {

}
