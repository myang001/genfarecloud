package com.genfare.cloud.model.entity.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.genfare.cloud.model.entity.Orders;

public interface OrdersRepository
    extends CrudRepository<Orders, Integer>, JpaSpecificationExecutor<Orders> {

    @Query("from Orders o where o.people.personId = ?")
    public List<Orders> findByUserId(Integer userId);

    @Query("from Orders o where o.people.personId = ? and o.orderstatuses.status = 1")
    public Orders findCurrentOrderByUserId(Integer userId);

    @Query("from Orders o where o.people.personId = :user and o.orderstatuses.status = 1 and o.ordertypes.orderTypeId = :type")
    public Orders findCurrentOrderByUserForType(@Param("user") Integer userId, @Param("type") Byte type);

    @Query("select distinct o from Orders o join o.orderstatuses s where (:orderId IS NULL OR o.orderId = :orderId) and (:fromDate IS NULL OR o.dateOrdered >= :fromDate) and (:toDate IS NULL OR o.dateOrdered < :toDate) and ( (:orderStatus IS NULL AND (s.status = :progress OR s.status = :fulfillment OR s.status = :completed OR s.status = :canceled)) OR s.status = :orderStatus)")
    public
        List<Orders> findOrdersByFilter(@Param("orderId") Integer orderId, @Param("fromDate") Date fromDate, @Param("toDate") Date toDate, @Param("orderStatus") Byte orderStatus,
            @Param("completed") Byte completed, @Param("canceled") Byte canceled, @Param("progress") Byte progress, @Param("fulfillment") Byte fulfillment);

    @Query("select distinct o from Orders o join o.orderitemses i join i.orderstatuses s join i.orderitemactions a join i.orderitemtypes t where (:orderId IS NULL OR o.orderId = :orderId) and (:fromDate IS NULL OR o.dateOrdered >= :fromDate) and (:toDate IS NULL OR o.dateOrdered < :toDate) and (s.status = :progress OR s.status = :fulfillment) and (a.orderItemActionId = :newAction) and (t.orderItemTypeId = :cardType)")
    public
        List<Orders> findPendingFulfillment(@Param("orderId") Integer orderId, @Param("fromDate") Date fromDate, @Param("toDate") Date toDate, @Param("newAction") Byte newAction,
            @Param("cardType") Byte CardType, @Param("progress") Byte progress, @Param("fulfillment") Byte fulfillment);

    @Query("select distinct o from Orders o join o.orderitemses i join i.orderstatuses s join i.pendingactivities p join p.activitytypes a where (:orderId IS NULL OR o.orderId = :orderId) and (:fromDate IS NULL OR o.dateOrdered >= :fromDate) and (:toDate IS NULL OR o.dateOrdered < :toDate) and (s.status = :progress OR s.status = :fulfillment) and (a.activityTypeId = :addValue OR a.activityTypeId = :addProduct)")
    public
        List<Orders> findPendingAutoload(@Param("orderId") Integer orderId, @Param("fromDate") Date fromDate, @Param("toDate") Date toDate, @Param("addValue") Byte addValue,
            @Param("addProduct") Byte addProduct, @Param("progress") Byte progress, @Param("fulfillment") Byte fulfillment);

}
