package com.genfare.cloud.model.types;

import java.util.EnumSet;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.annotation.PostConstruct;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.genfare.cloud.model.entity.Organizationtypes;
import com.genfare.cloud.model.entity.repository.OrganizationtypesRepository;

public enum OrganizationtypesType {
    SCHOOL(1),
    VENDOR(2),
    SPONSORING_EMPLOYER(3),
    NON_PROFIT(4);

    private final Integer index;

    private Organizationtypes type;

    private static Map<String, Organizationtypes> typeStrings = new LinkedHashMap<String, Organizationtypes>();

    public static Iterable<String> getTypes() {
        return typeStrings.keySet();
    }

    private OrganizationtypesType(Integer index) {
        this.index = index;
    }

    public Integer getIndex() {
        return index;
    }

    public Organizationtypes getType() {
        return type;
    }

    public static Organizationtypes findType(String type) {
        return typeStrings.get(type);
    }

    @Component
    public static class Injector {
        @Autowired
        private OrganizationtypesRepository repo;

        @PostConstruct
        public void setup() {
            for (OrganizationtypesType type : EnumSet.allOf(OrganizationtypesType.class)) {
                type.type = repo.findOne(type.index.byteValue());
                typeStrings.put(StringUtils.capitalize(type.toString().toLowerCase()), type.type);
            }
        }
    }
}
