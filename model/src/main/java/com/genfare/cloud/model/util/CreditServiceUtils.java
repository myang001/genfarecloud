package com.genfare.cloud.model.util;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.genfare.cloud.model.entity.Orders;
import com.genfare.cloud.model.entity.Peoplecards;
import com.genfare.cloud.model.security.User;

public interface CreditServiceUtils {

    String getServiceName();

    boolean validatePost(HttpServletRequest request, String relayResponseKey);

    Map<String, Object> getFormData(Orders order, User user, String returnContext, String userData);

    OrderPayment getOrderPayment(HttpServletRequest request);

    Contact getContact(HttpServletRequest request);

    UserBilling getUserBilling(HttpServletRequest request);

    String getForwardToParam();

    Map<String, Object> getAuthOnlyFormData(User user, String returnContext, String userData);

    String getUserDataField();

    boolean isAuthOnly(HttpServletRequest request);

    Peoplecards createPeopleCard(User user, HttpServletRequest request);

    public String getCardType(HttpServletRequest request);
}
