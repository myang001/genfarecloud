/**
 *
 */
package com.genfare.cloud.model.converters;

import org.springframework.core.convert.converter.Converter;

import com.genfare.cloud.model.entity.Organizations;
import com.genfare.cloud.model.om.ObjectFactory;
import com.genfare.cloud.model.om.OrganizationType;

/**
 * @author mpeter
 */
public class OrganizationToOrganizationType
    implements Converter<Organizations, OrganizationType> {

    @Override
    public OrganizationType convert(Organizations org) {
        ObjectFactory of = new ObjectFactory();
        OrganizationType ot = of.createOrganizationType();

        ot.setOrganizationID(org.getOrganizationId());
        ot.setName(org.getName());
        ot.setOrganizationType(org.getOrganizationtypes().getDescription());

        return ot;
    }
}
