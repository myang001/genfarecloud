package com.genfare.cloud.model.entity.repository;

import org.springframework.data.repository.CrudRepository;

import com.genfare.cloud.model.entity.Shippers;

public interface ShippersRepository
    extends CrudRepository<Shippers, Integer> {

}
