package com.genfare.cloud.model.support;

import java.util.Properties;

import javax.annotation.Resource;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author martin
 */
@Configuration
@EnableJpaRepositories(basePackages = { "com.genfare.cloud.repository", "com.genfare.cloud.admin.repository",
    "com.genfare.cloud.model.entity.repository" })
@EnableTransactionManagement
public class PersistenceContext {

    private static final String[] PROPERTY_PACKAGES_TO_SCAN = { "com.genfare.cloud.model" };

    private static final String PROPERTY_NAME_DATABASE_DRIVER = "db.jndi";

    private static final String PROPERTY_NAME_HIBERNATE_DIALECT = "hibernate.dialect";

    private static final String PROPERTY_NAME_HIBERNATE_FORMAT_SQL = "hibernate.format_sql";

    private static final String PROPERTY_NAME_HIBERNATE_HBM2DDL_AUTO = "hibernate.hbm2ddl.auto";

    private static final String PROPERTY_NAME_HIBERNATE_NAMING_STRATEGY = "hibernate.ejb.naming_strategy";

    private static final String PROPERTY_NAME_HIBERNATE_SHOW_SQL = "hibernate.show_sql";

    @Resource
    private Environment env;

    @Autowired
    private MultiTenantConnectionProviderImpl multiTenantConnectionProviderImpl;

    @Autowired
    private CurrentTenantIdentifierResolverImpl currentTenantIdentifierResolverImpl;

    @Bean
    public DataSource dataSource()
        throws NamingException {
        Context initialContext = new InitialContext();
        DataSource source = (DataSource) initialContext.lookup("java:jboss/datasources/GenfareCloudDS");
        return source;
    }

    @Bean
    public JpaTransactionManager transactionManager()
        throws NamingException {
        JpaTransactionManager transactionManager = new JpaTransactionManager();

        transactionManager.setEntityManagerFactory(entityManagerFactory().getObject());

        return transactionManager;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory()
        throws NamingException {
        LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
        entityManagerFactoryBean.setDataSource(dataSource());
        entityManagerFactoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        entityManagerFactoryBean.setPackagesToScan(PROPERTY_PACKAGES_TO_SCAN);

        Properties jpaProperties = new Properties();
        jpaProperties.put(PROPERTY_NAME_HIBERNATE_DIALECT, env.getRequiredProperty(PROPERTY_NAME_HIBERNATE_DIALECT));
        jpaProperties.put(PROPERTY_NAME_HIBERNATE_FORMAT_SQL,
            env.getRequiredProperty(PROPERTY_NAME_HIBERNATE_FORMAT_SQL));
        // jpaProperties.put(PROPERTY_NAME_HIBERNATE_HBM2DDL_AUTO,
        // env.getRequiredProperty(PROPERTY_NAME_HIBERNATE_HBM2DDL_AUTO));
        jpaProperties.put(PROPERTY_NAME_HIBERNATE_NAMING_STRATEGY,
            env.getRequiredProperty(PROPERTY_NAME_HIBERNATE_NAMING_STRATEGY));
        jpaProperties.put(PROPERTY_NAME_HIBERNATE_SHOW_SQL, env.getRequiredProperty(PROPERTY_NAME_HIBERNATE_SHOW_SQL));

        jpaProperties.put("hibernate.multiTenancy", "SCHEMA");
        jpaProperties.put("hibernate.multi_tenant_connection_provider", multiTenantConnectionProviderImpl);
        jpaProperties.put("hibernate.tenant_identifier_resolver", currentTenantIdentifierResolverImpl);

        entityManagerFactoryBean.setJpaProperties(jpaProperties);

        return entityManagerFactoryBean;
    }

}