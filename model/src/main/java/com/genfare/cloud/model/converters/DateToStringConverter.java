package com.genfare.cloud.model.converters;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.core.convert.converter.Converter;

public class DateToStringConverter
    implements Converter<Date, String> {

    @Override
    public String convert(Date input) {
        SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy"); // TODO: Make configurable
        return format.format(input);
    }

}
