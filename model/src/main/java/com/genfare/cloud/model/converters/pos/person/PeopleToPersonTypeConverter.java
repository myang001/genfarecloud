package com.genfare.cloud.model.converters.pos.person;

import java.util.GregorianCalendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;

import com.genfare.cloud.model.entity.Authentication;
import com.genfare.cloud.model.entity.Groups;
import com.genfare.cloud.model.entity.Operators;
import com.genfare.pos.core.model.person.PersonType;

public class PeopleToPersonTypeConverter implements
    Converter<Operators, PersonType> {

    private static final Logger log = LoggerFactory
        .getLogger(PeopleToPersonTypeConverter.class);

    @Override
    public PersonType convert(Operators o) {
        GregorianCalendar c = new GregorianCalendar();
        PersonType pt = new PersonType();

        pt.setPersonId(o.getOperatorId());
        if (!o.getPeople().getLoginses().isEmpty()) {
            pt.setUsername(o.getPeople().getLoginses().iterator().next()
                .getUserLogin());
        }
        for (Authentication a : o.getPeople().getAuthentications()) {
            if (a.getAuthenticationtypes().getAuthenticationTypeId() == 6) {
                pt.setHashedPassword(a.getResponse());
            }
        }

        // This needs to be changed to take equipment into consideration rather
        // than returning all the groups and picking the first one
        if (o.getPeople().getGroups().size() > 0) {

            Groups g = o.getPeople().getGroups().iterator().next();
            GroupsToGroupTypeConverter g2gtc = new GroupsToGroupTypeConverter();
            pt.setGroup(g2gtc.convert(g));
        }

        return pt;
    }
}
