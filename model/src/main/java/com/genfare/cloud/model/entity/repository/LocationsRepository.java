package com.genfare.cloud.model.entity.repository;

import org.springframework.data.repository.CrudRepository;

import com.genfare.cloud.model.entity.Locations;

public interface LocationsRepository
    extends CrudRepository<Locations, Integer> {

}
