package com.genfare.cloud.model.entity.repository;

import org.springframework.data.repository.CrudRepository;

import com.genfare.cloud.model.entity.Periods;

public interface PeriodsRepository
    extends CrudRepository<Periods, Integer> {

}
