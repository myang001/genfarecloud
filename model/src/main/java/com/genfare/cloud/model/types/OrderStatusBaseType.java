package com.genfare.cloud.model.types;

import java.util.EnumSet;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.genfare.cloud.model.entity.Orderstatuses;
import com.genfare.cloud.model.entity.repository.OrderstatusesRepository;

public enum OrderStatusBaseType {
    UNSUBMITTED(1),
    SUBMITTED(2),
    FULFILLMENT(3),
    REJECTED(4),
    ABANDONED(5),
    COMPLETE(6),
    CANCELLED(7),
    IN_PROGRESS(8),
    UNPAID(9),
    INVOICED(10),
    PAID(11),
    PENDING_ACTIVITY(12),
    HOLD(13);

    private final Integer index;

    private Orderstatuses status;

    private OrderStatusBaseType(Integer index) {
        this.index = index;
    }

    public Orderstatuses getStatus() {
        return status;
    }

    public Integer getIndex() {
        return index;
    }

    @Component
    public static class Injector {
        @Autowired
        private OrderstatusesRepository statusRepo;

        @PostConstruct
        public void setup() {
            for (OrderStatusBaseType status : EnumSet.allOf(OrderStatusBaseType.class)) {
                status.status = statusRepo.findOne(status.index.byteValue());
            }
        }
    }
}
