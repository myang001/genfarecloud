package com.genfare.cloud.model.service;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpSession;

import com.genfare.cloud.model.entity.Orderitems;
import com.genfare.cloud.model.entity.OrderitemsId;
import com.genfare.cloud.model.entity.Orders;
import com.genfare.cloud.model.entity.Orderstatuses;
import com.genfare.cloud.model.entity.Ordertypes;
import com.genfare.cloud.model.entity.Payments;
import com.genfare.cloud.model.entity.Shippers;
import com.genfare.cloud.model.entity.Wallets;
import com.genfare.cloud.model.exception.InvalidOrderException;
import com.genfare.cloud.model.security.User;
import com.genfare.cloud.model.types.OrderItemStatusType;
import com.genfare.cloud.model.types.OrderPaymentStatusType;
import com.genfare.cloud.model.util.Contact;
//TODO incomplete
//import com.genfare.cloud.model.util.OrderPayment;

/**
 * Service interface for managing orders
 * 
 * @author jyore
 * @version 1.0
 */
public interface OrderManagementService {

    /**
     * Get shippers
     * 
     * @return The Shippers or null
     * @throws EntityNotFoundException
     */
    Iterable<Shippers> getShippers() throws EntityNotFoundException;

    /**
     * Get an order
     * 
     * @param orderId The ID of the order to look up
     * @return The Order or null
     * @throws EntityNotFoundException
     */
    Orders getOrder(Integer orderId) throws EntityNotFoundException;

    /**
     * Gets orders by Ids
     * 
     * @param orderIds The list of IDs of the orders to look up
     * @return The iterable list of orders or null
     * @throws EntityNotFoundException
     */
    Iterable<Orders> getOrders(List<Integer> orderIds) throws EntityNotFoundException;

    /**
     * Get an order
     * 
     * @param orderId The ID of the order to look up
     * @param fromDate Get all orders starting at a certain date
     * @param toDate Get all orders until a certain date
     * @param status Get all orders with specified status
     * 
     * @return A list of Orders
     * 
     * @throws EntityNotFoundException
     */
    List<Orders> getFulfillmentOrdersByFilter(Integer orderId, Date fromDate, Date toDate, Byte status) throws EntityNotFoundException;

    /**
     * Get the current order being worked on from user or session
     * 
     * @param user The user to lookup an order for (can be null for guest or unknown users)
     * @param session The HTTP session to add the order to
     * @return The Order or null
     */
    Orders getCurrentOrder(User user, HttpSession session, Ordertypes type);

    /**
     * Get the current order OR create a new order
     * 
     * @param user The user to lookup an order for (can be null for guest or unknown users)
     * @param session The HTTP session to add the order to
     * @return The Order
     */
    Orders getCurrentOrCreate(User user, HttpSession session, Ordertypes type);

    /**
     * Save an order to the database
     * 
     * @param order The order to save
     * @param meta Meta data for binding orders. If the meta data contains a User object, it will bind the Order to that user If
     *        the meta data contains an HttpSession object, it will bind the Order to that HttpSession
     * @return The saved entity
     */
    Orders saveOrder(Orders order, Object... meta);

    /**
     * Add an Order Item to an Order. This will call saveOrder automatically
     * 
     * @see OrderManagementService#saveOrder(Orders, Object...)
     * @param order The order that the item should be added to
     * @param orderitem The order item to add to the order
     * @param meta Meta data to pass to the saveOrder method
     * @return The updated order
     * @throws Exception
     */
    Orders addOrderItem(Orders order, Orderitems orderitem, Object... meta) throws InvalidOrderException;

    /**
     * Add multiple Order Items to an Order. This will call saveOrder automatically
     * 
     * @see OrderManagementService#saveOrder(Orders, Object...)
     * @param order The order that the item should be added to
     * @param items The order items to add to the order
     * @param meta Meta data to pass to the saveOrder method
     * @return The updated order
     * @throws Exception
     */
    Orders addOrderItems(Orders order, Iterable<Orderitems> items, Object... meta) throws InvalidOrderException;

    /**
     * Retrieve an Order Item from the database by ID
     * 
     * @param id of the Order Item
     * @return The Orderitem or null
     */
    Orderitems retrieveOrderItem(OrderitemsId id);

    /**
     * Remove and Order Item from an Order. This will call saveOrder automatically
     * 
     * @see OrderManagementService#saveOrder(Orders, Object...)
     * @param order The order to operate on
     * @param orderitems The order item to remove
     * @param meta Meta data to pass to the saveOrder method
     * @return The updated order
     */
    Orders removeOrderItem(Orders order, Orderitems orderitems, Object... meta);

    /**
     * Sort the order items by item number and parent item number
     * 
     * @param order The Order to sort the items for
     * @return A List of Orderitems
     */
    List<Orderitems> sortOrderItems(Orders order);

    /**
     * Remove and Order Item from an Order. This will call saveOrder automatically
     * 
     * @see OrderManagementService#saveOrder(Orders, Object...)
     * @param order The order to operate on
     * @param orderitems The order items to remove
     * @param meta Meta data to pass to the saveOrder method
     * @return The updated order
     */
    Orders removeOrderItems(Orders order, Iterable<Orderitems> items, Object... meta);

    /**
     * Removes all order items safely
     * 
     * @param order The order to operate on
     * @param meta Meta data to pass to the saveOrder method
     * @return The updated order
     */
    Orders removeAllOrderItems(Orders order, Object... meta);

    /**
     * Update an Order Item Status This will automatically derive and update the Order status as well
     * 
     * @param item The Order Item to update
     * @param status The Status to apply
     * @return The updated Order
     */
    Orders updateOrderItemStatus(Orderitems item, OrderItemStatusType status);

    /**
     * Update Order Item Statuses This will automatically derive and update the Order status as well
     * 
     * @param order The order whose items will be updated
     * @param status The status to give the items
     * @return The updated Order
     */
    Orders updateOrderItemStatuses(Orders order, OrderItemStatusType status);

    /**
     * Updates all OrderItems in an Order with a status as well as the Order's Payment and overall status
     * 
     * @param order The Order to update
     * @param paymentStatus The payment status to give the order
     * @param itemStatus The item status to give all items
     * @return The updated order
     */
    Orders updateOrderStatuses(Orders order, OrderPaymentStatusType paymentStatus, OrderItemStatusType itemStatus);

    /**
     * Updates an Order with payment information such as credit card info and return code
     * 
     * @param order The Order to update
     * @param orderPayment The payment info to give the order
     * @return The updated order
     */
    Orders addOrderPayment(Orders order, Payments payment, Object... meta);

    /**
     * Updates an Order with payments information such as credit card info and return code
     * 
     * @param order The Order to update
     * @param orderPayment The payments info to give the order
     * @return The updated order
     */
    Orders addOrderPayments(Orders order, Iterable<Payments> payments, Object... meta);

    /**
     * Returns true if Order has item(s) that require shipping
     * 
     * @param order The Order to check
     * @return true if order contains an item that requires shipping
     */
    boolean orderRequiresShipping(Orders order);

    boolean isCardBeingReplaced(User user, Wallets wallet);

    Orders updateCart(Orders order, User user, HttpSession session);

    /**
     * Returns all orderStatuses with an isFulfillment of 'T'
     * 
     * @return
     * @throws EntityNotFoundException
     */
    List<Orderstatuses> getFulfillmentStatuses() throws EntityNotFoundException;

    List<Wallets> getWalletsForOrder(Orders order);

    Orderitems getOrCreateOrderItemForExistingCard(Orders order, Wallets wallet);

    Integer orderItemsCount(Orders order);

    Orderitems updateOrderItemFarecode(Orderitems oi, User user);

    Orders processReplaceOrder(User user, HttpSession session, Orders order);

    Contact getOrderShipping(Orders o);

    short validateQuantity(String quantity);

    String checkRules(Orderitems orderitem, Orders order, User user, HttpSession session);

}
