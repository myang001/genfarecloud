package com.genfare.cloud.model.entity.repository;

import org.springframework.data.repository.CrudRepository;

import com.genfare.cloud.model.entity.Servicedifferentials;
import com.genfare.cloud.model.entity.ServicedifferentialsId;

public interface ServicedifferentialsRepository
    extends CrudRepository<Servicedifferentials, ServicedifferentialsId> {

}
