package com.genfare.cloud.model.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.genfare.cloud.model.dto.UserDetails;
import com.genfare.cloud.model.entity.Groupprivileges;
import com.genfare.cloud.model.entity.Groups;
import com.genfare.cloud.model.entity.OrganizationStatus;
import com.genfare.cloud.model.entity.Organizations;
import com.genfare.cloud.model.entity.repository.AuthenticationRepository;
import com.genfare.cloud.model.exception.EmailNotVerifiedException;
import com.genfare.cloud.model.security.User;
import com.genfare.cloud.model.types.OrganizationStatusType;

@Service("userDetailsAssembler")
public class UserDetailsAssembler {

    private static final Logger log = LoggerFactory.getLogger(UserDetailsAssembler.class);

    @Autowired
    private EmailService emailservice;

    @Autowired
    private AuthenticationRepository authenticationRepository;

    @Transactional(readOnly = true)
    public UserDetails buildUserFromUserEntity(User user)
        throws EmailNotVerifiedException {

        // Build Granted Authorities list from our list of roles
        Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        for (Groups groups : user.getGroups()) {
            authorities.add(new SimpleGrantedAuthority(groups.getRole()));
            for (Groupprivileges groupprivileges : groups.getGroupprivilegeses()) {
                authorities.add(new SimpleGrantedAuthority(groupprivileges.getPrivileges().getPermission()));
            }
        }

        if (authenticationRepository.findEmailVerificaitonTokenByPeopleID(user.getId()) != null) {
            throw new EmailNotVerifiedException("Email address has not been verified");
        }
        // NOTE: Account control can be done by setting the booleans to false
        // and setting up an authentication handler in security config that
        // will return specific views based on which value is false. Also will
        // not allow a user to access any other part of the site until they
        // become true again. So eventually needs to be DB driven.
        UserDetails u = new UserDetails(
            user.getLogin(), // username
            authenticationRepository.findPasswordHashByPeopleID(user.getId()), // password
            isEnable(user), // enabled
            true, // accountNonExpired,
            true, // credentialsNonExpired,
            true, // accountNonLocked,
            authorities // Granted Authorities
            );
        return u;
    }

    private boolean isEnable(User user) {
        OrganizationStatus activeStatus = OrganizationStatusType.ACTIVE.getStatus();
        Set<Groups> groupsList = user.getGroups();
        boolean isEnable = groupsList.isEmpty();
        for (Groups groups : groupsList) {
            Organizations organizations = groups.getOrganizations();
            isEnable |= organizations == null || activeStatus.equals(organizations.getStatus());
        }
        return isEnable;
    }
}
