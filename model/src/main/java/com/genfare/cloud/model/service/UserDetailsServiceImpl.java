package com.genfare.cloud.model.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.genfare.cloud.model.exception.EmailNotVerifiedException;
import com.genfare.cloud.model.security.User;

public class UserDetailsServiceImpl
    implements UserDetailsService {

    private static final Logger log = LoggerFactory.getLogger(UserDetailsServiceImpl.class);

    @Autowired
    private UserManagementService usermanager;

    @Autowired
    private EmailService emailservice;

    @Autowired
    private UserDetailsAssembler assembler;

    @Override
    public UserDetails loadUserByUsername(String username)
        throws UsernameNotFoundException, EmailNotVerifiedException {

        log.info("Attempting to load user for authentication: " + username);
        User user = usermanager.getUser(username);

        if (user == null || !user.getLogin().equals(username)) {
            log.debug("Failed to load user: " + username);
            throw new UsernameNotFoundException("user not found with login: " + username);
        }

        return assembler.buildUserFromUserEntity(user);
    }

}
