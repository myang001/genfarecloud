package com.genfare.cloud.model.types;

import java.util.EnumSet;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.genfare.cloud.model.entity.Subscriptionstatus;
import com.genfare.cloud.model.entity.repository.SubscriptionstatusRepository;

public enum SubscriptionstatusType {
    NEW(1),
    ACTIVE(2),
    CANCELLED(3);

    private final Integer index;

    private Subscriptionstatus status;

    private SubscriptionstatusType(Integer index) {
        this.index = index;
    }

    public Integer getIndex() {
        return index;
    }

    public Subscriptionstatus getStatus() {
        return status;
    }

    @Component
    public static class Injector {
        @Autowired
        private SubscriptionstatusRepository repo;

        @PostConstruct
        public void setup() {
            for (SubscriptionstatusType status : EnumSet.allOf(SubscriptionstatusType.class)) {
                status.status = repo.findOne(status.index.byteValue());
            }
        }
    }
}
