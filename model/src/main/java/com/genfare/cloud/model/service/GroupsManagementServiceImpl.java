package com.genfare.cloud.model.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.genfare.cloud.model.entity.GroupStatus;
import com.genfare.cloud.model.entity.Groups;
import com.genfare.cloud.model.entity.repository.GroupsRepository;
import com.genfare.cloud.model.types.GroupStatusType;

@Service
@Transactional
public class GroupsManagementServiceImpl
    implements GroupsManagementService {

    private static final Logger LOGGER = LoggerFactory.getLogger(GroupsManagementServiceImpl.class);

    @Autowired
    GroupsRepository groupsRepository;

    @Override
    public boolean isGroupEnabled(Groups group) {
        return !group.getStatus().equals(GroupStatusType.DISABLED.getStatus());
    }

    @Override
    public void toggleGroupStatus(Groups group) {
        GroupStatus status = group.getStatus();
        status.toggleStatus();
    }

    @Override
    public String generateRoleForGroup(String name) {
        if (name == null || name.isEmpty()) {
            return null;
        }

        String result = "ROLE_" + name.replace(" ", "_").toUpperCase();

        return result;
    }
}
