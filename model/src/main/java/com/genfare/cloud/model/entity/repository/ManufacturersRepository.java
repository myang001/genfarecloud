package com.genfare.cloud.model.entity.repository;

import org.springframework.data.repository.CrudRepository;

import com.genfare.cloud.model.entity.Manufacturers;

public interface ManufacturersRepository
    extends CrudRepository<Manufacturers, Integer> {

}
