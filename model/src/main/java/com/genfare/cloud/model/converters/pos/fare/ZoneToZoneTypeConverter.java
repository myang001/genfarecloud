package com.genfare.cloud.model.converters.pos.fare;

import org.springframework.core.convert.converter.Converter;

import com.genfare.cloud.model.entity.Zones;
import com.genfare.pos.core.model.common.ZoneType;

public class ZoneToZoneTypeConverter implements Converter<Zones, ZoneType> {

    @Override
    public ZoneType convert(Zones z) {
        ZoneType zt = new ZoneType();
        zt.setZoneId(z.getZoneId());
        zt.setDescription(z.getDescription());

        return zt;
    }

}
