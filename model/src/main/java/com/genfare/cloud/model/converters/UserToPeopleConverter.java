package com.genfare.cloud.model.converters;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.springframework.core.convert.converter.Converter;

import com.genfare.cloud.model.contact.Address;
import com.genfare.cloud.model.contact.Email;
import com.genfare.cloud.model.contact.Phone;
import com.genfare.cloud.model.entity.Farecodes;
import com.genfare.cloud.model.entity.Groups;
import com.genfare.cloud.model.entity.Groupsofpeople;
import com.genfare.cloud.model.entity.GroupsofpeopleId;
import com.genfare.cloud.model.entity.JobTitle;
import com.genfare.cloud.model.entity.Logins;
import com.genfare.cloud.model.entity.People;
import com.genfare.cloud.model.entity.PeopleFarecodes;
import com.genfare.cloud.model.entity.PeopleFarecodesId;
import com.genfare.cloud.model.entity.Peopleaddresses;
import com.genfare.cloud.model.entity.PeopleaddressesId;
import com.genfare.cloud.model.entity.Peoplelinks;
import com.genfare.cloud.model.entity.PeoplelinksId;
import com.genfare.cloud.model.entity.repository.JobTitleRepository;
import com.genfare.cloud.model.entity.repository.PeopleRepository;
import com.genfare.cloud.model.security.ContactMethodEnum;
import com.genfare.cloud.model.security.User;
import com.genfare.cloud.model.types.AddresstypesType;
import com.genfare.cloud.model.types.LinktypesType;

public class UserToPeopleConverter implements Converter<User, People> {

    private PeopleRepository peopleRepository;
    private JobTitleRepository jobTitleRepository;

    public void setRepos(PeopleRepository peopleRepository, JobTitleRepository jobTitleRepository) {
        this.peopleRepository = peopleRepository;
        this.jobTitleRepository = jobTitleRepository;
    }

    @Override
    public People convert(User u) {

        if (u == null) {
            return null;
        }

        People p = null;
        if (u.getId() == null) {
            // we save here to secure an id that can be referenced when adding references later
            p = peopleRepository.save(new People());
        } else {
            p = peopleRepository.findOne(u.getId());
        }

        p.setAlert(u.getAlert());
        p.setDateOfBirth(u.getDateOfBirth());
        p.setEmployeeId(u.getEmployeeId());
        p.setGender(u.getGender());
        p.setLanguages(u.getLanguage().getType());
        p.setNameFirst(u.getFirstName());
        p.setNameMiddle(u.getMiddleName());
        p.setNameLast(u.getLastName());
        p.setPrefix(u.getPrefix());
        p.setStatus(u.getStatus().getStatus());
        p.setSuffix(u.getSuffix());

        p = updateLogin(p, u.getLogin());
        p = updateLinks(p, u.getPrimaryContactMethod(), u.getEmail(), u.getPhone());
        p = updateAddress(p, u.getAddress());
        p = updateJobTitle(p, u.getJobTitle());
        p = updateGroups(p, u.getGroups());
        p = updateFarecodes(p, u.getFarecodes());

        return p;
    }

    private People updateLogin(People p, String login) {

        login = login.toLowerCase();

        // Assumes email as login
        Set<Logins> logins = p.getLoginses();
        if (logins.size() > 0) {
            for (Logins l : logins) {
                if (l.getUserLogin().contains("@")) {
                    if (!l.getUserLogin().equals(login)) {
                        l.setUserLogin(login);
                    }
                    break;
                }
            }
        } else {
            logins.add(new Logins(p, login));
        }

        return p;
    }

    private People updateLinks(People p, ContactMethodEnum primary, Email email, Phone phone) {

        Set<Peoplelinks> links = p.getPeoplelinkses();
        if (links.size() > 0) {
            for (Peoplelinks l : links) {
                if (l.getLinktypes().getDescription().equals(email.getDescription())) {
                    l.setLinkValue(email.getAddress());
                } else if (l.getLinktypes().getDescription().equals(phone.getDescription())) {
                    l.setLinkValue(phone.getNumber());
                }
            }
        } else {
            LinktypesType etype = LinktypesType.PERSONAL_EMAIL;
            LinktypesType ptype = LinktypesType.HOME_PHONE;

            links.add(new Peoplelinks(new PeoplelinksId(p.getPersonId(), etype.getIndex()), etype.getType(), p, email.getAddress(), false, isPrimary(primary, etype)));
            links.add(new Peoplelinks(new PeoplelinksId(p.getPersonId(), ptype.getIndex()), ptype.getType(), p, phone.getNumber(), false, isPrimary(primary, etype)));
        }

        return p;
    }

    private Boolean isPrimary(ContactMethodEnum primary, LinktypesType type) {
        if (primary == ContactMethodEnum.SMS) {
            return type == LinktypesType.HOME_PHONE;
        } else {
            return type == LinktypesType.PERSONAL_EMAIL;
        }
    }

    private People updateAddress(People p, Address address) {

        Set<Peopleaddresses> addresses = p.getPeopleaddresseses();

        if (addresses.size() > 0) {
            for (Peopleaddresses addy : addresses) {
                if (addy.getAddresstypes().getDescription().equals(address.getDescription())) {
                    addy.setAddressLine1(address.getLine1());
                    addy.setAddressLine2(address.getLine2());
                    addy.setCity(address.getCity());
                    addy.setState(address.getState());
                    addy.setPostalCode(address.getPostalCode());
                    addy.setCountry(address.getCountry());
                    break;
                }
            }
        } else {
            AddresstypesType type = AddresstypesType.SHIPPING;
            addresses.add(new Peopleaddresses(
                new PeopleaddressesId(p.getPersonId(), type.getIndex()),
                type.getType(),
                p,
                address.getLine1(),
                address.getLine2(),
                address.getCity(),
                address.getState(),
                address.getPostalCode(),
                address.getCountry()
                ));
        }

        return p;
    }

    private People updateJobTitle(People p, String jobTitle) {
        JobTitle jt = jobTitleRepository.findByName(jobTitle);
        if (jt != null && jt.getDescription().equals(jobTitle)) {
            p.setJobTitle(jt);
        }

        return p;
    }

    private People updateGroups(People p, Set<Groups> groups) {
        /**
         * Have to manage PeopleFarecodes instead of just Farecodes pending Hibernate ticket: HHH-9429
         */

        /*
         * Interesting looping required due to the fact that we cannot add a 'new' entity of the same ID as one already existing
         * without error. This forces us to keep the current entity saved inside the Person entity. We also do not want to keep
         * the elements in the Person entity that are no longer Associated with the user and add any new ones not there
         */
        Set<Groupsofpeople> gop = p.getGroupsofpeoples();
        Set<Groupsofpeople> ugop = new HashSet<Groupsofpeople>();
        for (Groups g : groups) {
            boolean found = false;
            Groupsofpeople ngp = new Groupsofpeople(new GroupsofpeopleId(g.getGroupId(), p.getPersonId()), g, p);

            Iterator<Groupsofpeople> it = gop.iterator();
            while (it.hasNext()) {
                Groupsofpeople egp = it.next();
                if (egp.equals(ngp)) {
                    ugop.add(egp);
                    found = true;
                    break;
                }
            }

            if (!found) {
                ugop.add(ngp);
            }
        }

        p.setGroupsofpeoples(ugop);

        return p;
    }

    private People updateFarecodes(People p, Set<Farecodes> farecodes) {
        /**
         * Have to manage PeopleFarecodes instead of just Farecodes pending Hibernate ticket: HHH-9429
         */

        /*
         * Interesting looping required due to the fact that we cannot add a 'new' entity of the same ID as one already existing
         * without error. This forces us to keep the current entity saved inside the Person entity. We also do not want to keep
         * the elements in the Person entity that are no longer Associated with the user and add any new ones not there
         */
        Set<PeopleFarecodes> pof = p.getPeopleFarecodes();
        Set<PeopleFarecodes> upof = new HashSet<PeopleFarecodes>();
        for (Farecodes f : farecodes) {
            boolean found = false;
            PeopleFarecodes npf = new PeopleFarecodes(new PeopleFarecodesId(p.getPersonId(), f.getFarecodeId()), p, f);

            Iterator<PeopleFarecodes> it = pof.iterator();
            while (it.hasNext()) {
                PeopleFarecodes epf = it.next();
                if (epf.equals(npf)) {
                    upof.add(epf);
                    found = true;
                    break;
                }
            }

            if (!found) {
                upof.add(npf);
            }
        }

        p.setPeopleFarecodes(upof);

        return p;
    }
}
