package com.genfare.cloud.model.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.genfare.cloud.model.dto.UserDetails;
import com.genfare.cloud.model.entity.Modules;
import com.genfare.cloud.model.entity.repository.ModulesRepository;
import com.genfare.cloud.model.security.User;
import com.genfare.cloud.model.service.UserManagementService;

public class AdminModelHandlerInterceptor extends HandlerInterceptorAdapter {

    private static final Logger LOGGER = LoggerFactory.getLogger(AdminModelHandlerInterceptor.class);

    UserManagementService userManagementService;
    ModulesRepository modulesRepository;

    public void setUserManagementService(UserManagementService service) {
        this.userManagementService = service;
    }

    public void setModulesRepository(ModulesRepository m) {
        this.modulesRepository = m;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, final Object handler,
        ModelAndView modelAndView)
        throws Exception {
        User user = null;
        if (SecurityContextHolder.getContext().getAuthentication().getPrincipal() != "anonymousUser") {
            user = userManagementService.getUser(((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername());
        }
        String url = request.getRequestURL().toString();
        LOGGER.info("Request url: " + url);
        Modules module = null;
        if (url.contains("usermanagement")) {
            module = modulesRepository.findByName("Admin");
        }
        else if (url.contains("csr")) {
            module = modulesRepository.findByName("Customer Service");
        }
        modelAndView.getModelMap().addAttribute("componentses", userManagementService.getComponentsForUser(module, user));
    }

}
