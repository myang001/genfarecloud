package com.genfare.cloud.model.service;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityNotFoundException;

import com.genfare.cloud.model.entity.Orderitems;
import com.genfare.cloud.model.entity.Walletcontents;
import com.genfare.cloud.model.entity.Wallets;
import com.genfare.cloud.model.exception.WalletHasPeopleAssignedException;
import com.genfare.cloud.model.security.User;

public interface WalletManagementService {
    public Wallets getWallet(String PID)
        throws EntityNotFoundException;

    public Wallets getWallet(String cvvNumber, String cardNumber);

    public Wallets getWalletByLogin(String login)
        throws EntityNotFoundException;

    public void saveWallet(Wallets wallet);

    public Wallets registerWallet(User user, String cvvNumber, String cardNumber)
        throws WalletHasPeopleAssignedException, EntityNotFoundException;

    public Wallets registerWallet(User user, String cvvNumber, String cardNumber, String nickname)
        throws WalletHasPeopleAssignedException, EntityNotFoundException;

    public BigDecimal getStoredValueContents(Wallets wallet);

    public List<Wallets> getWalletsForUser(User user);

    public boolean hasValidContentsId(User user, String walletContentsId);

    public Wallets getWalletForUser(User user, String identifier);

    public List<Wallets> sortWallets(Collection<Wallets> wallets);

    public String getPrintedId(String walletId);

    public Map<String, List<Walletcontents>> paramaterizeWalletContents(Wallets wallet);

    public Map<String, List<Orderitems>> paramaterizePendingItems(Wallets wallet);

    public boolean walletVisibleForUser(Wallets wallet, User user);

    public Map<String, String> getWalletSet(User user);

    public int getPendingItemsCount(Walletcontents wc);

    public List<Orderitems> getPendingItems(Walletcontents wc);
}