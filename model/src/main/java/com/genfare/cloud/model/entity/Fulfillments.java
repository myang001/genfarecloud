package com.genfare.cloud.model.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.genfare.cloud.model.entity.inter.Auditable;
import com.genfare.cloud.model.listener.AuditListener;

/**
 * Fulfillments manually created
 * 
 * @author alimoussa
 */
@Entity
@Table(name = "fulfillments")
@EntityListeners(AuditListener.class)
public class Fulfillments
    implements java.io.Serializable, Auditable {

    private OrderitemsId id;

    private Orderstatuses orderstatuses;

    private String touchedBy;

    private Short quantity;

    private Short quantityEncoded;

    private Short quantityShipped;

    private List<FulfillmentItems> fulfillmentItems = new ArrayList<FulfillmentItems>();

    private Orderitems orderitems;

    private Date touchedAt;

    private String createdBy;

    private Date createdAt;

    public Fulfillments() {
    }

    public Fulfillments(OrderitemsId id) {
        this.id = id;
    }

    @EmbeddedId
    @AttributeOverrides({ @AttributeOverride(name = "orderId", column = @Column(name = "OrderID", nullable = false)),
        @AttributeOverride(name = "orderItemNumber", column = @Column(name = "OrderItemNumber", nullable = false)) })
    public OrderitemsId getId() {
        return this.id;
    }

    public void setId(OrderitemsId id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "Status", nullable = false)
    public Orderstatuses getOrderstatuses() {
        return this.orderstatuses;
    }

    public void setOrderstatuses(Orderstatuses orderstatuses) {
        this.orderstatuses = orderstatuses;
    }

    @Column(name = "Quantity", nullable = false)
    public Short getQuantity() {
        return this.quantity;
    }

    public void setQuantity(Short quantity) {
        this.quantity = quantity;
    }

    @Column(name = "QuantityEncoded", nullable = false)
    public Short getQuantityEncoded() {
        return this.quantityEncoded;
    }

    public void setQuantityEncoded(Short quantityEncoded) {
        this.quantityEncoded = quantityEncoded;
    }

    @Column(name = "QuantityShipped", nullable = false)
    public Short getQuantityShipped() {
        return this.quantityShipped;
    }

    public void setQuantityShipped(Short quantityShipped) {
        this.quantityShipped = quantityShipped;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumns({ @JoinColumn(name = "OrderID", nullable = false, insertable = false, updatable = false),
        @JoinColumn(name = "OrderItemNumber", nullable = false, insertable = false, updatable = false) })
    public Orderitems getOrderitems() {
        return this.orderitems;
    }

    public void setOrderitems(Orderitems orderitems) {
        this.orderitems = orderitems;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "fulfillments")
    public List<FulfillmentItems> getFulfillmentItems() {
        return fulfillmentItems;
    }

    public void setFulfillmentItems(List<FulfillmentItems> fulfillmentItems) {
        this.fulfillmentItems = fulfillmentItems;
    }

    @Column(name = "TouchedBy", nullable = false, length = 50)
    public String getTouchedBy() {
        return this.touchedBy;
    }

    @Override
    public void setTouchedBy(String touchedBy) {
        this.touchedBy = touchedBy;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "TouchedAt", nullable = false, length = 19)
    public Date getTouchedAt() {
        return this.touchedAt;
    }

    @Override
    public void setTouchedAt(Date touchedAt) {
        this.touchedAt = touchedAt;
    }

    @Override
    @Column(name = "CreatedBy", nullable = false, length = 50)
    public String getCreatedBy() {
        return this.createdBy;
    }

    @Override
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Override
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CreatedAt", nullable = false, length = 19)
    public Date getCreatedAt() {
        return this.createdAt;
    }

    @Override
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

}
