package com.genfare.cloud.model.tenant;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

@Component
@Scope("request")
public class TenantConfigurationFactory {

    @Autowired
    TenantConfigurationRepository configLoader;

    @Autowired
    DetectedDomain detectedDomain;

    @Bean
    @Scope(value = "request", proxyMode = ScopedProxyMode.INTERFACES)
    public TenantConfiguration getTenantConfig() {
        return configLoader.getByDomainName(detectedDomain.getDomain());
    }

}
