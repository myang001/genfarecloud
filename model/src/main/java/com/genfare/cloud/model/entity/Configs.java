package com.genfare.cloud.model.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the config database table.
 * 
 */
@Entity
@Table(name = "config")
@NamedQuery(name = "Configs.findAll", query = "SELECT c FROM Configs c")
public class Configs implements Serializable {
    private static final long serialVersionUID = 1L;
    private int configID;
    private String configKey;
    private String configValue;
    private Date createdAt;
    private String createdBy;
    private Date touchedAt;
    private String touchedBy;

    public Configs() {
    }

    @Id
    @Column(name = "ConfigID")
    public int getConfigID() {
        return this.configID;
    }

    public void setConfigID(int configID) {
        this.configID = configID;
    }

    @Column(name = "ConfigKey")
    public String getConfigKey() {
        return this.configKey;
    }

    public void setConfigKey(String configKey) {
        this.configKey = configKey;
    }

    @Column(name = "ConfigValue")
    public String getConfigValue() {
        return this.configValue;
    }

    public void setConfigValue(String configValue) {
        this.configValue = configValue;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CreatedAt")
    public Date getCreatedAt() {
        return this.createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    @Column(name = "CreatedBy")
    public String getCreatedBy() {
        return this.createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "TouchedAt")
    public Date getTouchedAt() {
        return this.touchedAt;
    }

    public void setTouchedAt(Date touchedAt) {
        this.touchedAt = touchedAt;
    }

    @Column(name = "TouchedBy")
    public String getTouchedBy() {
        return this.touchedBy;
    }

    public void setTouchedBy(String touchedBy) {
        this.touchedBy = touchedBy;
    }

}