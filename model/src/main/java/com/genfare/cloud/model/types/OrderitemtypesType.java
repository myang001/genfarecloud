package com.genfare.cloud.model.types;

import java.util.EnumSet;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.genfare.cloud.model.entity.Orderitemtypes;
import com.genfare.cloud.model.entity.repository.OrderitemtypesRepository;

public enum OrderitemtypesType {
    TICKET(1),
    WALLET(2),
    WALLETTYPE(3);

    private final Integer index;

    private Orderitemtypes type;

    private OrderitemtypesType(Integer index) {
        this.index = index;
    }

    public Integer getIndex() {
        return index;
    }

    public Orderitemtypes getType() {
        return type;
    }

    @Component
    public static class Injector {
        @Autowired
        private OrderitemtypesRepository repo;

        @PostConstruct
        public void setup() {
            for (OrderitemtypesType type : EnumSet.allOf(OrderitemtypesType.class)) {
                type.type = repo.findOne(type.index.byteValue());
            }
        }
    }
}
