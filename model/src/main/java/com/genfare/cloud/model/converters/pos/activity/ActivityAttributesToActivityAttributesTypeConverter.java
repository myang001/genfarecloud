package com.genfare.cloud.model.converters.pos.activity;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;

import com.genfare.cloud.model.entity.Activityattributes;
import com.genfare.pos.core.model.activity.ActivityAttributeType;

public class ActivityAttributesToActivityAttributesTypeConverter implements
    Converter<Activityattributes, ActivityAttributeType> {

    private static final Logger log = LoggerFactory
        .getLogger(ActivityAttributesToActivityAttributesTypeConverter.class);

    @Override
    public ActivityAttributeType convert(Activityattributes aa) {
        ActivityAttributeType aat = new ActivityAttributeType();

        aat.setAttributeId(aa.getId().getAttributeId());
        aat.setAttributeValue(aa.getAttributeValue());

        return aat;
    }
}
