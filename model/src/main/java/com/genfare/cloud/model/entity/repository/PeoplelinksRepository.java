package com.genfare.cloud.model.entity.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.genfare.cloud.model.entity.Peoplelinks;

public interface PeoplelinksRepository
    extends CrudRepository<Peoplelinks, Integer> {

    @Query("from Peoplelinks l where l.linktypes.description like '%Email' and l.id.personId = ?")
    public List<Peoplelinks> findEmailsByPersonId(Integer id);

    @Query("from Peoplelinks l where l.linktypes.description like '%Phone' and l.id.personId = ?")
    public List<Peoplelinks> findPhonesByPersonId(Integer id);

    @Query("from Peoplelinks l where l.linktypes.description like '%Email' and l.linkValue = ?")
    public Peoplelinks findEmail(String email);

    @Query("from Peoplelinks l where l.id.personId = ? and l.id.linkTypeId = ?")
    public Peoplelinks findByPersonIdLinkTypeId(Integer personId, Integer linkTypeId);

    @Query("from Peoplelinks l where l.id.personId = ? and l.isPrimaryContactMethod = true")
    public Peoplelinks findPrimaryContactMethodByPersonId(Integer personId);
}
