package com.genfare.cloud.model.service;

import java.util.List;

import com.genfare.cloud.model.entity.Activities;
import com.genfare.cloud.model.entity.Equipment;
import com.genfare.cloud.model.entity.Offerings;
import com.genfare.cloud.model.entity.Operators;
import com.genfare.cloud.model.entity.Pendingactivity;
import com.genfare.cloud.model.entity.Taxrates;
import com.genfare.cloud.model.entity.Tickettypes;
import com.genfare.cloud.model.entity.Wallettypes;
import com.genfare.cloud.model.entity.Zones;
import com.genfare.cloud.model.tenant.impl.TenantConfigurationImpl;

public interface PosXmlService {

    String generateFareTableXml(List<Wallettypes> walletTypes,
        List<Zones> zones, List<Taxrates> taxRates,
        List<Tickettypes> ticketTypes, List<Offerings> offerings);

    String generatePersonXml(Operators o);

    String generateActivityXml(Activities a);

    String generatePendingActivityXml(Pendingactivity pa);

    String generateTerminalConfigAPI(TenantConfigurationImpl tci, Equipment eq);

}
