package com.genfare.cloud.model.wallet;

import java.util.Date;

public class Wallet {
    private long walletId;

    private Integer personId;

    private Date dateExpires;

    private boolean isBad;

    private boolean isRegistered;

    private boolean isThirdParty;

    public long getWalletId() {
        return walletId;
    }

    public void setWalletId(long walletId) {
        this.walletId = walletId;
    }

    public Integer getPersonId() {
        return personId;
    }

    public void setPersonId(Integer personId) {
        this.personId = personId;
    }

    public Date getDateExpires() {
        return dateExpires;
    }

    public void setDateExpires(Date dateExpires) {
        this.dateExpires = dateExpires;
    }

    public boolean isBad() {
        return isBad;
    }

    public void setBad(boolean isBad) {
        this.isBad = isBad;
    }

    public boolean isRegistered() {
        return isRegistered;
    }

    public void setRegistered(boolean isRegistered) {
        this.isRegistered = isRegistered;
    }

    public boolean isThirdParty() {
        return isThirdParty;
    }

    public void setThirdParty(boolean isThirdParty) {
        this.isThirdParty = isThirdParty;
    }

}
