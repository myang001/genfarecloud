package com.genfare.cloud.model.support;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.hibernate.service.jdbc.connections.spi.MultiTenantConnectionProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MultiTenantConnectionProviderImpl
    implements MultiTenantConnectionProvider {

    private static final long serialVersionUID = -5322258703724204214L;

    private static final Logger LOGGER = LoggerFactory.getLogger(MultiTenantConnectionProviderImpl.class);

    @Autowired
    private DataSource ds;

    public MultiTenantConnectionProviderImpl() {
        LOGGER.info("CREATING MultiTenantConnectionProviderImpl");
    }

    @Override
    public Connection getConnection(String tenantId)
        throws SQLException {
        LOGGER.info("Setting connection to " + tenantId);
        final Connection conn = getAnyConnection();
        conn.createStatement().execute("USE " + tenantId);
        // conn.createStatement().execute("SET SCHEMA " + tenantId);
        // conn.setSchema(tenantId);
        return conn;
    }

    @Override
    public void releaseConnection(String tenantId, Connection conn)
        throws SQLException {
        releaseAnyConnection(conn);
    }

    @Override
    public boolean supportsAggressiveRelease() {
        return true;
    }

    @Override
    public boolean isUnwrappableAs(@SuppressWarnings("rawtypes") Class arg0) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public <T> T unwrap(Class<T> arg0) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Connection getAnyConnection()
        throws SQLException {
        return ds.getConnection();
    }

    @Override
    public void releaseAnyConnection(Connection conn)
        throws SQLException {
        // TODO: Dont know if this puts the connection back in the pool or need
        // to destroy it
        // conn.createStatement().execute("USE test");
        // conn.createStatement().execute("SET SCHEMA test");
        conn.close();
        LOGGER.info("releasing connection");
    }

}