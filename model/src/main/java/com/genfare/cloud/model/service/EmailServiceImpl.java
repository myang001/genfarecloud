package com.genfare.cloud.model.service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;

import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.thymeleaf.context.AbstractContext;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring3.SpringTemplateEngine;

import com.genfare.cloud.model.contact.Email;
import com.genfare.cloud.model.converters.PeoplelinksToEmailConverter;
import com.genfare.cloud.model.entity.Authentication;
import com.genfare.cloud.model.entity.AuthenticationId;
import com.genfare.cloud.model.entity.repository.AuthenticationRepository;
import com.genfare.cloud.model.entity.repository.PeoplelinksRepository;
import com.genfare.cloud.model.security.User;
import com.genfare.cloud.model.tenant.DetectedDomain;
import com.genfare.cloud.model.tenant.TenantConfiguration;
import com.genfare.cloud.model.tenant.impl.DomainNameType;
import com.genfare.cloud.model.types.AuthenticationtypesType;

/**
 * Service that provides methods for handling email operations
 * 
 * @see EmailService
 * @author jyore
 * @version 1.0
 */
@Service("emailService")
public class EmailServiceImpl
    implements EmailService {

    private static final Logger log = LoggerFactory.getLogger(EmailServiceImpl.class);

    @Autowired
    private AuthenticationRepository authenticationRepository;

    @Autowired
    private SpringTemplateEngine templateEngine;

    @Autowired
    private PeoplelinksRepository peoplelinksRepository;

    @Autowired
    private TenantConfiguration tenantconfiguration;

    @Autowired
    private DetectedDomain detectedDomain;

    private AbstractContext context;

    private JavaMailSenderImpl createMailSender() {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setHost(tenantconfiguration.getEmailProperties().getSmtpHost());
        mailSender.setPort(Integer.parseInt(tenantconfiguration.getEmailProperties().getSmtpPort()));
        mailSender.setUsername(tenantconfiguration.getEmailProperties().getSmtpSmtpUsername());
        mailSender.setPassword(tenantconfiguration.getEmailProperties().getSmtpSmtpPassword());
        mailSender.setProtocol(tenantconfiguration.getEmailProperties().getSmtpProtocol());

        Properties props = mailSender.getJavaMailProperties();
        props.put("mail.smtp.auth", tenantconfiguration.getEmailProperties().getMailSmtpAuth());
        props.put("mail.smtp.ssl.enable", tenantconfiguration.getEmailProperties().getMailSmtpSslEnable());
        props.put("mail.transport.protocol", tenantconfiguration.getEmailProperties().getMailTransportProtocol());
        props.put("mail.debug", tenantconfiguration.getEmailProperties().getMailDebug());
        return mailSender;
    }

    private String renderEmail(String view, Map<String, ?> parameters) {
        final AbstractContext ctx = getContext();
        ctx.setVariables(parameters);
        return templateEngine.process(view, ctx);
    }

    /**
     * Sends an email to the preferred address of a user
     * 
     * @see EmailService#sendEmail(User, String, String, Map) <p>
     *      By default, the following parameters are added to the map for Thymeleaf template
     *      <ul>
     *      <li><b>to</b> - The email TO address</li>
     *      <li><b>user</b> - The user object the email is being set to</li>
     *      <li><b>domain</b> - The detected domain
     *      </ul>
     *      </p>
     */
    @Override
    public void sendEmail(User to, String subject, String view, Map<String, Object> parameters) {

        try {
            Email email = to.getEmail();

            parameters.put("to", email.getAddress());
            parameters.put("user", to);
            List<DomainNameType> domains = tenantconfiguration.getDomainNames().getDomainName();
            String primaryDomain = detectedDomain.getDomain();
            for (DomainNameType d : domains) {
                if (d.isPrimary()) {
                    primaryDomain = d.getValue();
                }
            }
            parameters.put("domain", primaryDomain);

            final String body = renderEmail(view, parameters);
            JavaMailSenderImpl mailSender = createMailSender();
            MimeMessage mimeMessage = mailSender.createMimeMessage();
            MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, true, "UTF-8");
            mimeMessageHelper.setFrom(tenantconfiguration.getEmailFromAddress());
            mimeMessageHelper.setTo(email.getAddress());
            mimeMessageHelper.setSubject(subject);
            MimeBodyPart mbp = new MimeBodyPart();
            mbp.setContent(body, "text/html");
            MimeMultipart mp = new MimeMultipart();
            mp.addBodyPart(mbp);
            mimeMessage.setContent(mp);
            mimeMessage.saveChanges();
            mbp.setHeader("Content-Transfer-Encoding", "base64");// must be after saveChanges in order to override the
            // encoding
            mailSender.send(mimeMessage);
        } catch (Exception e) {
            log.error("The email was not sent", e);
        }
    }

    @Override
    public void generateVerificationToken(User user) {
        UUID key = UUID.randomUUID();

        Authentication auth = authenticationRepository.findEmailVerificaitonTokenByPeopleID(user.getId());
        if (auth == null) {
            auth = new Authentication();
        }

        auth.setId(new AuthenticationId(user.getId(),
            AuthenticationtypesType.EMAIL_TOKEN.getType().getAuthenticationTypeId()));
        auth.setResponse(key.toString().replaceAll("-", ""));
        authenticationRepository.save(auth);

        Map<String, Object> params = new HashMap<String, Object>();
        params.put("token", auth.getResponse());
        sendEmail(user, "Thank you for registering with eFare", "registration", params);
    }

    private String generateToken(User user) {
        UUID key = UUID.randomUUID();

        Authentication auth = authenticationRepository.findByUserResponseType(user.getId(), AuthenticationtypesType.PASSWORD_RECOVERY.getType().getAuthenticationTypeId());
        if (auth == null) {
            auth = new Authentication();
            auth.setId(new AuthenticationId(user.getId(),
                AuthenticationtypesType.PASSWORD_RECOVERY.getType().getAuthenticationTypeId()));
        }
        auth.setResponse(key.toString().replaceAll("-", ""));
        authenticationRepository.save(auth);
        return auth.getResponse();
    }

    @Override
    public void generatePasswordRecoveryVerificationToken(User user) {
        String generateToken = generateToken(user);
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("token", generateToken);
        sendEmail(user, "Password Reset", "passwordRecovery", params);
    }

    @Override
    public void generateSetPasswordVerificationToken(User user) {
        String generateToken = generateToken(user);
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("token", generateToken);
        sendEmail(user, "Set New Password", "setPassword", params);
    }

    private boolean isTokenExpired(Authentication auth) {
        Date tokenDate = auth.getTouchedAt();
        Date now = new Date();

        long diff = (now.getTime() - tokenDate.getTime()) / (1000 * 60 * 60);
        return (diff >= 24) ? true : false;
    }

    @Override
    public boolean verifyToken(String token) {
        Authentication auth = authenticationRepository.findEmailVerificaitonToken(token);
        if (auth == null) {
            return false;
        }

        if (isTokenExpired(auth)) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public boolean verifyPasswordRecoveryToken(String token) {
        Authentication auth = authenticationRepository.findPasswordRecoveryVerificaitonToken(token);
        if (auth == null) {
            return false;
        }

        if (isTokenExpired(auth)) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public boolean verifySetPasswordToken(String token) {
        return verifyPasswordRecoveryToken(token);
    }

    @Override
    @Transactional
    public void deleteToken(String token) {
        authenticationRepository.deleteByToken(token);
    }

    @Override
    public Email findEmail(String email) {
        return PeoplelinksToEmailConverter.convert(peoplelinksRepository.findEmail(email));
    }

    @Override
    public AbstractContext getContext() {
        if (context == null) {
            return new Context(LocaleContextHolder.getLocale());
        }
        return context;
    }

    @Override
    public void setContext(AbstractContext context) {
        this.context = context;
    }

}
