package com.genfare.cloud.model.service;

import java.util.List;

import org.json.JSONObject;

import com.genfare.cloud.model.entity.Groups;
import com.genfare.cloud.model.entity.Privileges;

public interface PrivilegesManagementService {

    List<Privileges> savePrivilegesForGroup(Groups group, JSONObject moduleComponentPrivilegesMapping);

    void clearPriviligesForGroup(Groups groups);
}
