package com.genfare.cloud.model.util;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.NotImplementedException;
import org.springframework.stereotype.Component;

import com.genfare.cloud.model.entity.Orders;
import com.genfare.cloud.model.entity.Peoplecards;
import com.genfare.cloud.model.security.User;

@Component(value = "Exact")
public class CreditServiceUtilExact
    implements CreditServiceUtils {

    private static final String SERVICE_NAME = "Exact";

    @Override
    public String getServiceName() {
        return SERVICE_NAME;
    }

    @Override
    public boolean validatePost(HttpServletRequest request, String relayResponseKey) {
        throw new NotImplementedException("CreditserviceUtilExact#validatePost currently not implemented");
    }

    @Override
    public Map<String, Object> getFormData(Orders order, User user, String returnContext, String userData) {
        throw new NotImplementedException("CreditserviceUtilExact#getFormData currently not implemented");
    }

    @Override
    public OrderPayment getOrderPayment(HttpServletRequest request) {
        throw new NotImplementedException("CreditserviceUtilExact#getOrderPayment currently not implemented");
    }

    @Override
    public Contact getContact(HttpServletRequest request) {
        throw new NotImplementedException("CreditserviceUtilExact#getContact currently not implemented");
    }

    @Override
    public UserBilling getUserBilling(HttpServletRequest request) {
        throw new NotImplementedException("CreditserviceUtilExact#getUserBilling currently not implemented");
    }

    @Override
    public String getForwardToParam() {
        throw new NotImplementedException("CreditserviceUtilExact#getForwardToParam currently not implemented");
    }

    @Override
    public Map<String, Object> getAuthOnlyFormData(User user, String returnContext, String userData) {
        throw new NotImplementedException("CreditserviceUtilExact#getAuthOnlyFormData currently not implemented");
    }

    @Override
    public String getUserDataField() {
        throw new NotImplementedException("CreditserviceUtilExact#getUserDataField currently not implemented");
    }

    @Override
    public boolean isAuthOnly(HttpServletRequest request) {
        throw new NotImplementedException("CreditserviceUtilExact#isAuthOnly currently not implemented");
    }

    @Override
    public Peoplecards createPeopleCard(User user, HttpServletRequest request) {
        throw new NotImplementedException("CreditserviceUtilExact#getPeopleCard currently not implemented");
    }

    @Override
    public String getCardType(HttpServletRequest request) {
        throw new NotImplementedException("CreditserviceUtilExact#getCardType currently not implemented");
    }
}
