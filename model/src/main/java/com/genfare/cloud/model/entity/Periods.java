package com.genfare.cloud.model.entity;

// Generated Mar 19, 2014 9:47:20 AM by Hibernate Tools 4.0.0

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Type;

import com.genfare.cloud.model.entity.inter.Auditable;
import com.genfare.cloud.model.listener.AuditListener;

/**
 * Periods generated by hbm2java
 */
@Entity
@Table(name = "periods")
@EntityListeners(AuditListener.class)
public class Periods
    implements java.io.Serializable, Auditable {

    private Byte periodId;

    private Expirationtypes expirationtypes;

    private String description;

    private Boolean isFloating;

    private Byte periodsPerYear;

    private String renewalMonths;

    private Byte renewalDay;

    private String touchedBy;

    private Date touchedAt;

    private String createdBy;

    private Date createdAt;

    public Periods() {
    }

    public Periods(Expirationtypes expirationtypes, String description) {
        this.expirationtypes = expirationtypes;
        this.description = description;
    }

    public Periods(Expirationtypes expirationtypes, String description, Boolean isFloating, Byte periodsPerYear,
        String renewalMonths, Byte renewalDay) {
        this.expirationtypes = expirationtypes;
        this.description = description;
        this.isFloating = isFloating;
        this.periodsPerYear = periodsPerYear;
        this.renewalMonths = renewalMonths;
        this.renewalDay = renewalDay;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "PeriodID", unique = true, nullable = false)
    public Byte getPeriodId() {
        return this.periodId;
    }

    public void setPeriodId(Byte periodId) {
        this.periodId = periodId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ExpirationTypeID", nullable = false)
    public Expirationtypes getExpirationtypes() {
        return this.expirationtypes;
    }

    public void setExpirationtypes(Expirationtypes expirationtypes) {
        this.expirationtypes = expirationtypes;
    }

    @Column(name = "Description", nullable = false, length = 50)
    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name = "IsFloating", length = 1)
    @Type(type = "true_false")
    public Boolean getIsFloating() {
        return this.isFloating;
    }

    public void setIsFloating(Boolean isFloating) {
        this.isFloating = isFloating;
    }

    @Column(name = "PeriodsPerYear")
    public Byte getPeriodsPerYear() {
        return this.periodsPerYear;
    }

    public void setPeriodsPerYear(Byte periodsPerYear) {
        this.periodsPerYear = periodsPerYear;
    }

    @Column(name = "RenewalMonths", length = 30)
    public String getRenewalMonths() {
        return this.renewalMonths;
    }

    public void setRenewalMonths(String renewalMonths) {
        this.renewalMonths = renewalMonths;
    }

    @Column(name = "RenewalDay")
    public Byte getRenewalDay() {
        return this.renewalDay;
    }

    public void setRenewalDay(Byte renewalDay) {
        this.renewalDay = renewalDay;
    }

    @Column(name = "TouchedBy", nullable = false, length = 50)
    public String getTouchedBy() {
        return this.touchedBy;
    }

    @Override
    public void setTouchedBy(String touchedBy) {
        this.touchedBy = touchedBy;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "TouchedAt", nullable = false, length = 19)
    public Date getTouchedAt() {
        return this.touchedAt;
    }

    @Override
    public void setTouchedAt(Date touchedAt) {
        this.touchedAt = touchedAt;
    }

    @Override
    @Column(name = "CreatedBy", nullable = false, length = 50)
    public String getCreatedBy() {
        return this.createdBy;
    }

    @Override
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Override
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CreatedAt", nullable = false, length = 19)
    public Date getCreatedAt() {
        return this.createdAt;
    }

    @Override
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

}
