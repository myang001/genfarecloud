package com.genfare.cloud.model.filter;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.genfare.cloud.model.tenant.DetectedDomain;

@Component
public class DomainRequestFilter
    implements Filter {

    private static final Logger LOGGER = Logger.getLogger(DomainRequestFilter.class);

    private DetectedDomain detectedDomain;

    public DomainRequestFilter() {
        super();
        LOGGER.info("DomainRequestFilter");
    }

    @Override
    public void destroy() {
    }

    @Override
    public void init(FilterConfig arg0)
        throws ServletException {
    }

    @Override
    public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain filterChain)
        throws IOException, ServletException {
        final HttpServletRequest httpRequest = (HttpServletRequest) request;
        try {
            LOGGER.info("Old request URL : " + getDetectedDomain().getDomain());
            String domainName = getDomainName(httpRequest.getRequestURL());
            getDetectedDomain().setDomain(domainName);
            LOGGER.info("New request URL : " + domainName);
        } catch (URISyntaxException e) {
            LOGGER.error(e);
        }
        filterChain.doFilter(request, response);
    }

    private String getDomainName(StringBuffer url)
        throws URISyntaxException {
        URI uri = new URI(url.toString());
        String domain = uri.getHost();
        domain = domain.startsWith("www.") ? domain.substring(4) : domain;
        return domain.split("\\.")[0];
    }

    public DetectedDomain getDetectedDomain() {
        return detectedDomain;
    }

    @Autowired
    public void setDetectedDomain(DetectedDomain detectedDomain) {
        this.detectedDomain = detectedDomain;
    }

}