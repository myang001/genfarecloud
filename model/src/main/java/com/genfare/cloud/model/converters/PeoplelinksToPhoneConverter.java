package com.genfare.cloud.model.converters;

import com.genfare.cloud.model.contact.Phone;
import com.genfare.cloud.model.entity.Peoplelinks;

public class PeoplelinksToPhoneConverter {

    public static Phone convert(Peoplelinks p) {
        Phone ph = new Phone();
        ph.setDescription(p.getLinktypes().getDescription());
        ph.setNumber(p.getLinkValue());
        return ph;
    }
}
