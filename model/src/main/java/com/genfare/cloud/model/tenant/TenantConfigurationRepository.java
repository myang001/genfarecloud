package com.genfare.cloud.model.tenant;

import java.util.Collection;

public interface TenantConfigurationRepository {
    public TenantConfiguration getByDomainName(String domainName);

    public TenantConfiguration getBySchemaName(String schemaName);

    public Collection<TenantConfiguration> getAll();

    public TenantConfiguration getDefaultTenant();
}
