package com.genfare.cloud.model.service;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.PersistenceException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.genfare.cloud.model.converters.PeopleToUserConverter;
import com.genfare.cloud.model.converters.UserToPeopleConverter;
import com.genfare.cloud.model.dto.UserDetails;
import com.genfare.cloud.model.entity.Authentication;
import com.genfare.cloud.model.entity.AuthenticationId;
import com.genfare.cloud.model.entity.Authenticationtypes;
import com.genfare.cloud.model.entity.Components;
import com.genfare.cloud.model.entity.Groupprivileges;
import com.genfare.cloud.model.entity.Groups;
import com.genfare.cloud.model.entity.Modules;
import com.genfare.cloud.model.entity.People;
import com.genfare.cloud.model.entity.repository.AuthenticationRepository;
import com.genfare.cloud.model.entity.repository.AuthenticationtypesRepository;
import com.genfare.cloud.model.entity.repository.ComponentsRepository;
import com.genfare.cloud.model.entity.repository.JobTitleRepository;
import com.genfare.cloud.model.entity.repository.LoginsRepository;
import com.genfare.cloud.model.entity.repository.PeopleRepository;
import com.genfare.cloud.model.security.User;
import com.genfare.cloud.model.types.AuthenticationtypesType;

/**
 * Implementation service of {@link com.genfare.cloud.model.service.UserManagementService}
 * 
 * 
 * @author jyore
 */
@Service
@Transactional
public class UserManagementServiceImpl
    implements UserManagementService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserManagementServiceImpl.class);

    @Autowired
    private PeopleRepository peopleRepository;

    @Autowired
    private JobTitleRepository jobTitleRepository;

    @Autowired
    private LoginsRepository loginsRepository;

    @Autowired
    private AuthenticationRepository authenticationRepository;

    @Autowired
    private AuthenticationtypesRepository authenticationtypesRepository;

    @Autowired
    private HashingService hashingService;

    @Autowired
    private ComponentsRepository componentsRepository;

    @Autowired
    UserDetailsAssembler userDetailsAssembler;

    @Override
    public User getUser(Integer id) {
        PeopleToUserConverter people2user = new PeopleToUserConverter();
        return people2user.convert(peopleRepository.findOne(id));
    }

    @Override
    public User getUser(String login) {
        PeopleToUserConverter people2user = new PeopleToUserConverter();
        return people2user.convert(peopleRepository.findByLogin(login));
    }

    @Override
    public User getUser(Principal principal) {
        if (principal == null) {
            return null;
        }
        PeopleToUserConverter people2user = new PeopleToUserConverter();
        return people2user.convert(peopleRepository.findByLogin(principal.getName()));
    }

    @Override
    public User saveUser(User user) throws PersistenceException {
        try {
            PeopleToUserConverter people2user = new PeopleToUserConverter();
            UserToPeopleConverter user2people = new UserToPeopleConverter();
            user2people.setRepos(peopleRepository, jobTitleRepository);
            return people2user.convert(peopleRepository.save(user2people.convert(user)));
        } catch (Exception e) {
            LOGGER.error(String.format("Failed to save user (id: %s, login: %s)", user.getId(), user.getLogin()), e);
            throw new PersistenceException(e);
        }
    }

    @Override
    public Integer getUserId(Principal principal) {
        return loginsRepository.findPersonIdFromLogin(principal.getName());
    }

    @Override
    public void savePassword(User user, String password) throws PersistenceException {
        try {
            AuthenticationtypesType type = AuthenticationtypesType.PASSWORD;

            UserToPeopleConverter user2people = new UserToPeopleConverter();
            user2people.setRepos(peopleRepository, jobTitleRepository);
            People p = user2people.convert(user);

            Authentication a = authenticationRepository.findPasswordByPeopleID(user.getId());
            if (a == null) {
                a = new Authentication(new AuthenticationId(user.getId(), type.getIndex()), type.getType(), p);
            }

            a.setResponse(hashingService.springBCryptHash(password, 10));
            authenticationRepository.save(a);
        } catch (Exception e) {
            LOGGER.error(String.format("Failed to save user's password (id: %s, login: %s)", user.getId(), user.getLogin()), e);
            throw new PersistenceException(e);
        }
    }

    @Override
    public void saveSecurityQuestion(User user, String question, String answer) throws PersistenceException {
        try {
            Authenticationtypes at = authenticationtypesRepository.findSecurityQuestionByPrompt(question);

            UserToPeopleConverter user2people = new UserToPeopleConverter();
            user2people.setRepos(peopleRepository, jobTitleRepository);
            People p = user2people.convert(user);

            Authentication a = authenticationRepository.findByUserResponseType(user.getId(), at.getAuthenticationTypeId());
            if (a == null) {
                a = new Authentication(new AuthenticationId(user.getId(), at.getAuthenticationTypeId()), at, p);
            }
            a.setResponse(hashingService.springBCryptHash(answer, 10));
            authenticationRepository.save(a);
        } catch (Exception e) {
            LOGGER.error(String.format("Failed to save user's security question (id: %s, login: %s)", user.getId(),
                user.getLogin()), e);
            throw new PersistenceException(e);
        }
    }

    @Override
    public User updatePasswordUsingRecoveryToken(String recoveryToken, String newPassword) {
        Authentication passwordRecoveryToken = authenticationRepository.findPasswordRecoveryVerificaitonToken(recoveryToken);
        authenticationRepository.deleteByToken(recoveryToken);

        User user = getUser(passwordRecoveryToken.getId().getPersonId());
        savePassword(user, newPassword);

        return user;
    }

    @Override
    public List<Components> getComponentsForUser(Modules m, User u) {
        List<Components> components = new ArrayList<Components>();
        if (u != null) {
            components = componentsRepository.findComponentsByUserAndModule(
                u.getId(), m);
        }
        return components;
    }

    @Override
    public void reloadUser(User user) {
        UserDetails details = userDetailsAssembler.buildUserFromUserEntity(user);
        SecurityContextHolder.getContext().setAuthentication(
            new UsernamePasswordAuthenticationToken(details, details.getPassword(), details.getAuthorities())
            );
    }

    @Override
    public void reloadGroupAuthorities(Groups group, List<Object> principals) {
        for (Object principal : principals) {
            UserDetails userDetails = (UserDetails) principal;
            for (GrantedAuthority grantedAuthority : userDetails
                .getAuthorities()) {
                if (grantedAuthority.getAuthority().equals(group.getRole())) {
                    User user = getUser(userDetails.getUsername());

                    Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
                    for (Groups groups : user.getGroups()) {
                        authorities.add(new SimpleGrantedAuthority(groups
                            .getRole()));
                        for (Groupprivileges groupprivileges : groups
                            .getGroupprivilegeses()) {
                            authorities.add(new SimpleGrantedAuthority(
                                groupprivileges.getPrivileges()
                                    .getPermission()));
                        }
                    }

                    userDetails.setAuthorities(authorities);
                    break;
                }
            }
        }
    }

    @Override
    public void reloadAuthorities(User user, List<Object> principals) {
        for (Object principal : principals) {
            UserDetails userDetails = (UserDetails) principal;
            if (userDetails.getUsername().equals(user.getLogin())) {
                User userReload = getUser(userDetails.getUsername());
                Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
                for (Groups groups : userReload.getGroups()) {
                    authorities
                        .add(new SimpleGrantedAuthority(groups.getRole()));
                    for (Groupprivileges groupprivileges : groups
                        .getGroupprivilegeses()) {
                        authorities
                            .add(new SimpleGrantedAuthority(groupprivileges
                                .getPrivileges().getPermission()));
                    }
                }
                userDetails.setAuthorities(authorities);
                break;
            }
        }
    }

    @Override
    public List<User> getUsersByGroup(Integer groupId) {
        PeopleToUserConverter converter = new PeopleToUserConverter();
        List<User> users = new ArrayList<User>();
        List<People> people = peopleRepository.findPeopleByGroup(groupId);

        for (People p : people) {
            users.add(converter.convert(p));
        }

        return users;
    }
}
