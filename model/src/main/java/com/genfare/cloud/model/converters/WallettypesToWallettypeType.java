/**
 *
 */
package com.genfare.cloud.model.converters;

import org.springframework.core.convert.converter.Converter;

import com.genfare.cloud.model.entity.Wallettypes;
import com.genfare.cloud.model.om.WallettypeType;

/**
 * @author mpeter
 */
public class WallettypesToWallettypeType
    implements Converter<Wallettypes, WallettypeType> {

    @Override
    public WallettypeType convert(Wallettypes w) {
        WallettypeType wt = new WallettypeType();
        wt.setWallettypeID(w.getWalletTypeId().intValue());
        wt.setDescription(w.getDescription());
        return wt;
    }

    public Wallettypes convert(WallettypeType wt) {
        Wallettypes w = new Wallettypes();
        int id = wt.getWallettypeID();
        w.setWalletTypeId((byte) id);
        w.setDescription(wt.getDescription());
        return w;
    }
}
