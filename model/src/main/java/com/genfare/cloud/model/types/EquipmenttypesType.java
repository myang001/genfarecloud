package com.genfare.cloud.model.types;

import java.util.EnumSet;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.genfare.cloud.model.entity.Equipmenttypes;
import com.genfare.cloud.model.entity.repository.EquipmenttypesRepository;

public enum EquipmenttypesType {
    ENCODER(1),
    FACTORY(2),
    INTERNATIONAL(3),
    APOS(4),
    RPOS(5),
    FareBox(6);

    private final Integer index;

    private Equipmenttypes type;

    private EquipmenttypesType(Integer index) {
        this.index = index;
    }

    public Integer getIndex() {
        return index;
    }

    public Equipmenttypes getType() {
        return type;
    }

    @Component
    public static class Injector {
        @Autowired
        private EquipmenttypesRepository repo;

        @PostConstruct
        public void setup() {
            for (EquipmenttypesType type : EnumSet.allOf(EquipmenttypesType.class)) {
                type.type = repo.findOne(type.index.shortValue());
            }
        }
    }
}
