package com.genfare.cloud.model.entity.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.genfare.cloud.model.entity.Walletidentifiers;
import com.genfare.cloud.model.entity.Wallets;

public interface WalletidentifiersRepository
    extends CrudRepository<Walletidentifiers, Integer> {

    @Query("select wi from Walletidentifiers wi where wi.identifier = ?")
    public Walletidentifiers findByPID(String PID);

    @Query("select wi.wallets from Walletidentifiers wi where wi.identifier = ?2 and wi.wallets.people.personId = ?1")
    public Wallets getWalletForUser(Integer id, String identifier);

    @Query("select wi.identifier from Walletidentifiers wi where wi.identifiertypes.identifierId = 2 and wi.wallets.walletId = ?")
    public String getPrintedIdForWallet(Integer WalletId);
}
