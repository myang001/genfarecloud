package com.genfare.cloud.model.service;

import java.util.List;

import com.genfare.cloud.model.entity.Components;
import com.genfare.cloud.model.entity.Groups;
import com.genfare.cloud.model.entity.Modules;
import com.genfare.cloud.model.entity.Organizations;
import com.genfare.cloud.model.entity.Privileges;
import com.genfare.cloud.model.security.User;

public interface GroupPermissionsService {

    List<Modules> sortModules(List<Modules> modules);

    boolean groupHasPermission(Groups group, Privileges privilege);

    void addUserToGroup(User user, String groupName);

    void removeUserFromGroup(User user, String groupName);

    String[] getComponentsPermission(Components components);

    String[] getModulePermission(Modules modules);

    Groups createDefaultGroupForOrg(Organizations o);
}
