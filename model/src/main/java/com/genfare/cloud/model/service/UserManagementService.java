package com.genfare.cloud.model.service;

import java.security.Principal;
import java.util.List;

import javax.persistence.PersistenceException;

import com.genfare.cloud.model.entity.Components;
import com.genfare.cloud.model.entity.Groups;
import com.genfare.cloud.model.entity.Modules;
import com.genfare.cloud.model.security.User;

/**
 * Service interface providing business logic for user management tasks
 * 
 * @author jyore
 */
public interface UserManagementService {

    /**
     * Get a user from the database, by ID
     * 
     * @param id The id of the user to retrieve
     * @return The user instance or null
     */
    User getUser(Integer id);

    /**
     * Get a user from the database, by Username
     * 
     * @param username The username of the user to retreive
     * @return The user instance or null
     */
    User getUser(String username);

    /**
     * Get a user from the database, by {@link java.security.Principal}
     * 
     * @param principal The {@link java.security.Principal} to retrieve
     * @return The user instance or null
     */
    User getUser(Principal principal);

    /**
     * Save/Persist a user
     * 
     * @param user The user to save
     * @return The saved user
     * @throws PersistenceException - if the insert|update path fails
     */
    User saveUser(User user) throws PersistenceException;

    /**
     * Get a user's ID only
     * 
     * @param principal The {@link java.security.Principal} to search
     * @return The User's ID
     */
    Integer getUserId(Principal principal);

    /**
     * Add/Update a password for a user
     * 
     * @param user
     * @param password2
     * @throws PersistenceException - if the insert|update path fails
     */
    void savePassword(User user, String password2) throws PersistenceException;

    /**
     * Add/Update a security question for a user
     * 
     * @param user
     * @param question
     * @param answer
     * @throws PersistenceException - if the insert|update path fails
     */
    void saveSecurityQuestion(User user, String question, String answer) throws PersistenceException;

    /**
     * Deletes password recovery token and sets a new password
     * 
     * @param recoveryToken
     * @param newPassword
     * @return The user effected
     */
    User updatePasswordUsingRecoveryToken(String recoveryToken,
        String newPassword);

    /**
     * Gets the available components for a logged in user
     * 
     * @param m Module to scan in
     * @param u The user to check
     * @return A list of Components
     */
    List<Components> getComponentsForUser(Modules m, User u);

    /**
     * Reloads a user in the security layer
     * 
     * @param user
     */
    void reloadUser(User user);

    /**
     * Reloads security principals for all users in a group
     * 
     * @param group
     * @param principals
     */
    void reloadGroupAuthorities(Groups group, List<Object> principals);

    /**
     * Reloads security principals for a user
     * 
     * @param user
     * @param principals
     */
    void reloadAuthorities(User user, List<Object> principals);

    /**
     * Retrieves a list of Users for a group
     * 
     * @param groupId The group id to search
     * @return The list of users
     */
    List<User> getUsersByGroup(Integer groupId);

}
