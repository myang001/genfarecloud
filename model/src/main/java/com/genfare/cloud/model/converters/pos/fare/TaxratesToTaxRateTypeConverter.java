package com.genfare.cloud.model.converters.pos.fare;

import org.springframework.core.convert.converter.Converter;

import com.genfare.cloud.model.entity.Taxrates;
import com.genfare.pos.core.model.fare.TaxRateType;

public class TaxratesToTaxRateTypeConverter implements Converter<Taxrates, TaxRateType> {

    @Override
    public TaxRateType convert(Taxrates tr) {
        TaxRateType trt = new TaxRateType();
        trt.setTaxRateId(tr.getTaxRateId());
        trt.setDescription(tr.getDescription());
        trt.setRate1(tr.getRate1());
        trt.setRate2(tr.getRate2());
        trt.setRate3(tr.getRate3());
        trt.setRate4(tr.getRate4());
        trt.setRate5(tr.getRate5());

        return trt;
    }

}
