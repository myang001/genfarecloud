package com.genfare.cloud.model.entity.repository;

import org.springframework.data.repository.CrudRepository;

import com.genfare.cloud.model.entity.Caps;

public interface CapsRepository
    extends CrudRepository<Caps, Integer> {

}
