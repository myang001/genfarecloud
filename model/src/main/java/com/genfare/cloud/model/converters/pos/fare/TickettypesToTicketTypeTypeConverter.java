package com.genfare.cloud.model.converters.pos.fare;

import org.springframework.core.convert.converter.Converter;

import com.genfare.cloud.model.entity.Tickettypes;
import com.genfare.pos.core.model.common.TicketTypeType;

public class TickettypesToTicketTypeTypeConverter implements
    Converter<Tickettypes, TicketTypeType> {

    @Override
    public TicketTypeType convert(Tickettypes tt) {
        TicketTypeType ttt = new TicketTypeType();
        ttt.setTicketTypeId(tt.getTicketTypeId());
        ttt.setDescription(tt.getDescription());

        return ttt;
    }

}
