package com.genfare.cloud.model.entity.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.genfare.cloud.model.entity.Orderstatuses;

public interface OrderstatusesRepository
    extends CrudRepository<Orderstatuses, Byte> {

    @Query("from Orderstatuses o where o.isFulfillment = 'T'")
    public List<Orderstatuses> findFulfillmentStatuses();

}