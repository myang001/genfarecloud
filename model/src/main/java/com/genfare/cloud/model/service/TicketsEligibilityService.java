package com.genfare.cloud.model.service;

import java.util.Map;

import com.genfare.cloud.model.entity.Offerings;
import com.genfare.cloud.model.entity.Tickets;
import com.genfare.cloud.model.entity.Wallets;
import com.genfare.cloud.model.security.User;

public interface TicketsEligibilityService {

    public Iterable<Tickets> getEligibleTickets(Wallets wallets);

    public Tickets getTicketById(Integer ticketId);

    public Map<String, Iterable<Offerings>> getAllEligibleTicketOfferings(Wallets wallets, User user);

}
