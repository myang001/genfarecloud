package com.genfare.cloud.model.entity.view;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Immutable;

import com.genfare.cloud.model.entity.OrderitemsId;

@Entity
@Immutable
@Table(name = "orderhistoryreport")
public class OrderHistoryReport {

    private OrderitemsId id;
    private Date dateOrdered;
    private BigDecimal amountOrder;
    private String status;
    private Integer personId;
    private Integer parentItemNumber;
    private Integer quantity;
    private BigDecimal unitPrice;
    private BigDecimal amount;
    private String nickName;
    private String cardNumber;
    private String walletDescription;
    private String walletTypeDescription;
    private String offeringDescription;
    private String ticketDescription;

    @EmbeddedId
    @AttributeOverrides({ @AttributeOverride(name = "orderId", column = @Column(name = "OrderID", nullable = false)),
        @AttributeOverride(name = "orderItemNumber", column = @Column(name = "OrderItemNumber", nullable = false)) })
    public OrderitemsId getId() {
        return id;
    }

    public void setId(OrderitemsId orderId) {
        this.id = orderId;
    }

    @Column(name = "DateOrdered")
    public Date getDateOrdered() {
        return dateOrdered;
    }

    public void setDateOrdered(Date dateOrdered) {
        this.dateOrdered = dateOrdered;
    }

    @Column(name = "AmountOrder")
    public BigDecimal getAmountOrder() {
        return amountOrder;
    }

    public void setAmountOrder(BigDecimal amountOrder) {
        this.amountOrder = amountOrder;
    }

    @Column(name = "Status")
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Column(name = "PersonId")
    public Integer getPersonId() {
        return personId;
    }

    public void setPersonId(Integer personId) {
        this.personId = personId;
    }

    @Column(name = "ParentItemNumber")
    public Integer getParentItemNumber() {
        return parentItemNumber;
    }

    public void setParentItemNumber(Integer parentItemNumber) {
        this.parentItemNumber = parentItemNumber;
    }

    @Column(name = "Quantity")
    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    @Column(name = "UnitPrice")
    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

    @Column(name = "Amount")
    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @Column(name = "NickName")
    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    @Column(name = "CardNumber")
    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    @Column(name = "WalletDescription")
    public String getWalletDescription() {
        return walletDescription;
    }

    public void setWalletDescription(String walletDescription) {
        this.walletDescription = walletDescription;
    }

    @Column(name = "WalletTypeDescription")
    public String getWalletTypeDescription() {
        return walletTypeDescription;
    }

    public void setWalletTypeDescription(String walletTypeDescription) {
        this.walletTypeDescription = walletTypeDescription;
    }

    @Column(name = "OfferingDescription")
    public String getOfferingDescription() {
        return offeringDescription;
    }

    public void setOfferingDescription(String offeringDescription) {
        this.offeringDescription = offeringDescription;
    }

    @Column(name = "TicketDescription")
    public String getTicketDescription() {
        return ticketDescription;
    }

    public void setTicketDescription(String ticketDescription) {
        this.ticketDescription = ticketDescription;
    }

}
