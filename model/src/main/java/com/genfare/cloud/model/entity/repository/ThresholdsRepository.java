package com.genfare.cloud.model.entity.repository;

import org.springframework.data.repository.CrudRepository;

import com.genfare.cloud.model.entity.Thresholds;

public interface ThresholdsRepository
    extends CrudRepository<Thresholds, Integer> {

}
