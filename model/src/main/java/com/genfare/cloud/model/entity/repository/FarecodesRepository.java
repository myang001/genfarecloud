package com.genfare.cloud.model.entity.repository;

import org.springframework.data.repository.CrudRepository;

import com.genfare.cloud.model.entity.Farecodes;

public interface FarecodesRepository
    extends CrudRepository<Farecodes, Byte> {

}