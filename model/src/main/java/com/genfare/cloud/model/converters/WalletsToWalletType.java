/**
 *
 */
package com.genfare.cloud.model.converters;

import org.springframework.core.convert.converter.Converter;

import com.genfare.cloud.model.entity.Wallets;
import com.genfare.cloud.model.om.WalletType;

/**
 * @author mpeter
 */
public class WalletsToWalletType
    implements Converter<Wallets, WalletType> {

    @Override
    public WalletType convert(Wallets w) {
        WalletType wt = new WalletType();
        wt.setWalletID(w.getWalletId().intValue());
        return wt;
    }

    public Wallets convert(WalletType wt) {
        Wallets w = new Wallets();
        int id = wt.getWalletID();
        w.setWalletId(new Long(id));
        return w;
    }
}
