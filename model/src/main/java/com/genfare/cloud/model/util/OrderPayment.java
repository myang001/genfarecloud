package com.genfare.cloud.model.util;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * A OrderPayment represents part of an order. The payment information for the order to be precise.
 */
public class OrderPayment
    implements Serializable {
    private static final long serialVersionUID = 8149618840198915118L;

    private int orderId;

    private BigDecimal amount;

    private long transactionId;

    private String referenceNumber;

    private String authorizationNumber;

    private String maskedCreditCardNumber;

    private String paymentType;

    private int rc;

    private String rcText;

    private int status;

    private String serviceName;

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public long getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(long transactionId) {
        this.transactionId = transactionId;
    }

    public String getAuthorizationNumber() {
        return authorizationNumber;
    }

    public void setAuthorizationNumber(String authorizationNumber) {
        this.authorizationNumber = authorizationNumber;
    }

    public String getMaskedCreditCardNumber() {
        return maskedCreditCardNumber;
    }

    public void setMaskedCreditCardNumber(String maskedCreditCardNumber) {
        this.maskedCreditCardNumber = maskedCreditCardNumber;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public int getRc() {
        return rc;
    }

    public void setRc(int rc) {
        this.rc = rc;
    }

    public String getRcText() {
        return rcText;
    }

    public void setRcText(String rcText) {
        this.rcText = rcText;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    @Override
    public String toString() {
        // TODO replace RESTServiceUtils.toJson(this) method
        return null;
    }

    public String getReferenceNumber() {
        return referenceNumber;
    }

    public void setReferenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

}
