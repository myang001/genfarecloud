package com.genfare.cloud.model.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.genfare.cloud.model.tenant.TenantConfiguration;

@Component
public class SessionFilter
    implements Filter {

    private static final Logger LOGGER = LoggerFactory.getLogger(SessionFilter.class);

    @Autowired
    private TenantConfiguration tenantConfiguration;

    public SessionFilter() {
        super();
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
        throws IOException, ServletException {

        HttpServletRequest request = (HttpServletRequest) req;
        HttpSession session = request.getSession();

        if (session != null) {
            try {
                int seconds = tenantConfiguration.getWebSessionTimeout() * 60; // minutes to seconds
                if (session.getMaxInactiveInterval() != seconds) {
                    session.setMaxInactiveInterval(seconds);
                    LOGGER.debug("Set Interval to: " + seconds + "s");
                }
            } catch (Exception e) {
                LOGGER.debug("Could not find Web Session Timeout in Tenant Configuration, using server default");
            }
        }

        chain.doFilter(req, res);
    }

    @Override
    public void init(FilterConfig config)
        throws ServletException {

    }

    @Override
    public void destroy() {

    }
}