package com.genfare.cloud.model.entity.repository;

import org.springframework.data.repository.CrudRepository;

import com.genfare.cloud.model.entity.Equipmenttypes;

public interface EquipmenttypesRepository
    extends CrudRepository<Equipmenttypes, Short> {

}
