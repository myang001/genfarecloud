package com.genfare.cloud.model.converters.pos.fare;

import org.springframework.core.convert.converter.Converter;

import com.genfare.cloud.model.entity.Farecodes;
import com.genfare.pos.core.model.common.FareCodeType;

public class FareCodeToFareCodeTypeConverter implements
    Converter<Farecodes, FareCodeType> {

    @Override
    public FareCodeType convert(Farecodes fc) {
        FareCodeType fct = new FareCodeType();
        fct.setFareCodeId(fc.getFarecodeId());
        fct.setDescription(fc.getDescription());

        return fct;
    }

}
