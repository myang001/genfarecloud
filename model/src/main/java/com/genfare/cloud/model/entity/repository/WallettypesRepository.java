package com.genfare.cloud.model.entity.repository;

import org.springframework.data.repository.CrudRepository;

import com.genfare.cloud.model.entity.Wallettypes;

public interface WallettypesRepository
    extends CrudRepository<Wallettypes, Byte> {

}
