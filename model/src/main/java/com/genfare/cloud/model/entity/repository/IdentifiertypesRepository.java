package com.genfare.cloud.model.entity.repository;

import org.springframework.data.repository.CrudRepository;

import com.genfare.cloud.model.entity.Identifiertypes;

public interface IdentifiertypesRepository
    extends CrudRepository<Identifiertypes, Byte> {

}
