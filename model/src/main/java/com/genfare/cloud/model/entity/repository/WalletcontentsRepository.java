package com.genfare.cloud.model.entity.repository;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.genfare.cloud.model.entity.Tickets;
import com.genfare.cloud.model.entity.Walletcontents;
import com.genfare.cloud.model.entity.Wallets;

public interface WalletcontentsRepository
    extends CrudRepository<Walletcontents, Long> {

    @Query("select wc.valueRemaining from Walletcontents wc where wc.walletContentsId = ?")
    public BigDecimal findValueRemainingForWalletcontent(Long id);

    @Query("select wc.wallets from Walletcontents wc where wc.walletContentsId = ?")
    public Wallets findWalletsForWalletcontent(Long id);

    @Query("select wc.tickets from Walletcontents wc where wc.walletContentsId = ?")
    public Tickets findTicketForWalletcontent(Long id);

    @Query("from Walletcontents wc where wc.wallets.walletId = ?")
    public List<Walletcontents> findWalletContentForWallet(Long Id);
}
