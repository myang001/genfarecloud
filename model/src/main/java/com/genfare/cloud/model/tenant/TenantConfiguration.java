package com.genfare.cloud.model.tenant;

import java.util.List;

import com.genfare.cloud.model.tenant.impl.AmazonConfigurationType;
import com.genfare.cloud.model.tenant.impl.BusinessRuleConfigurationType;
import com.genfare.cloud.model.tenant.impl.BusinessRuleTicketTypeLimitsType;
import com.genfare.cloud.model.tenant.impl.BusinessRuleValueTicketValueLimitsType;
import com.genfare.cloud.model.tenant.impl.DomainListType;
import com.genfare.cloud.model.tenant.impl.EmailPropertiesType;
import com.genfare.cloud.model.tenant.impl.SsoConfigurationType;

public interface TenantConfiguration {

    public abstract String getTenantName();

    public abstract DomainListType getDomainNames();

    public abstract String getSchemaName();

    public abstract boolean isDefaultTenant();

    public abstract boolean isImportTestData();

    public abstract long getSchedulerInterval();

    // Email
    public abstract String getEmailFromAddress();

    // Session
    public abstract int getWebSessionTimeout();

    // Amazon configuration
    public abstract AmazonConfigurationType getAmazonConfiguration();

    // Email Service
    public abstract EmailPropertiesType getEmailProperties();

    // SSO Configuration
    public abstract SsoConfigurationType getSsoConfiguration();

    // Business Rules Configuration
    public abstract BusinessRuleConfigurationType getBusinessRuleConfiguration();

    public abstract List<BusinessRuleTicketTypeLimitsType> getBusinessRuleTicketTypeLimits();

    public abstract List<BusinessRuleValueTicketValueLimitsType> getBusinessRuleValueTicketValueLimits();

    // Credit Services
    public abstract String getCreditServiceName();

    public abstract String getCreditServicePostURL();

    public abstract String getCreditServiceAPIURL();

    public abstract String getCreditServiceCartPage();

    public abstract String getCreditServiceReturnURL();

    public abstract String getCreditServiceCancelURL();

    public abstract String getCreditServiceMerchantCode();

    public abstract String getCreditServicePassword();

    public abstract String getCreditServiceSettleMerchantCode();

    public abstract String getCreditServiceXLogin();

    public abstract String getCreditServiceXShowForm();

    public abstract String getCreditServiceXCurrencyCode();

    public abstract String getCreditServiceXTestRequest();

    public abstract String getCreditServiceXRelayResponse();

    public abstract String getCreditServiceAgencyKey();

    public abstract String getCreditServiceRelayResponseKey();

    public abstract String getCreditServiceGatewayId();

    public abstract String getCreditServiceAPIPassword();

    public abstract String getCreditServiceAPIKey();

    public abstract String getReportOrganization();

    public abstract int getLockOrderTimeout();

    public abstract boolean isEncodeFirstUse();

}
