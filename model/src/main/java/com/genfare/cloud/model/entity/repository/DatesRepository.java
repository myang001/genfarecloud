package com.genfare.cloud.model.entity.repository;

import org.springframework.data.repository.CrudRepository;

import com.genfare.cloud.model.entity.Dates;

public interface DatesRepository
    extends CrudRepository<Dates, Integer> {

}
