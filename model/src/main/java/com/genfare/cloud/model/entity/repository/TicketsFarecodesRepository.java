package com.genfare.cloud.model.entity.repository;

import org.springframework.data.repository.CrudRepository;

import com.genfare.cloud.model.entity.TicketsFarecodes;
import com.genfare.cloud.model.entity.TicketsFarecodesId;

public interface TicketsFarecodesRepository
    extends CrudRepository<TicketsFarecodes, TicketsFarecodesId> {

}
