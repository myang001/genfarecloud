package com.genfare.cloud.model.util;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

/**
 * A resource representing contact information for a user.
 */
public class Contact
    implements Serializable {

    private static final long serialVersionUID = 3776702961609571787L;

    private Integer contact_id;

    @Size(max = 8)
    private String title;

    @NotNull
    @Size(max = 64)
    private String first_name;

    @Size(max = 64)
    private String middle_name;

    @NotNull
    @Size(max = 64)
    private String last_name;

    @Size(max = 8)
    private String suffix;

    @NotNull
    @Size(max = 256)
    private String address1;

    @Size(max = 256)
    private String address2;

    @Size(max = 256)
    private String address3;

    @NotNull
    @Size(max = 64)
    private String city;

    @NotNull
    @Size(max = 2)
    private String state;

    @Size(max = 16)
    private String zip;

    @NotNull
    @Size(max = 2)
    private String country;

    @Size(max = 256)
    private String email;

    @Size(max = 16)
    private String primary_phone;

    @Size(max = 16)
    private String secondary_phone;

    private Integer pref;

    public Integer getContactId() {
        return contact_id;
    }

    public void setContactId(Integer id) {
        this.contact_id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFirstName() {
        return first_name;
    }

    public void setFirstName(String fname) {
        this.first_name = fname;
    }

    public String getMiddleName() {
        return middle_name;
    }

    public void setMiddleName(String mname) {
        this.middle_name = mname;
    }

    public String getLastName() {
        return last_name;
    }

    public void setLastName(String lname) {
        this.last_name = lname;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String addr1) {
        this.address1 = addr1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String addr2) {
        this.address2 = addr2;
    }

    public String getAddress3() {
        return address3;
    }

    public void setAddress3(String addr3) {
        this.address3 = addr3;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPrimaryPhone() {
        return primary_phone;
    }

    public void setPrimaryPhone(String phone1) {
        this.primary_phone = phone1;
    }

    public String getSecondaryPhone() {
        return secondary_phone;
    }

    public void setSecondaryPhone(String phone2) {
        this.secondary_phone = phone2;
    }

    public Integer getPref() {
        return pref;
    }

    public void setPref(Integer pref) {
        this.pref = pref;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }

        Contact rhs = (Contact) obj;

        return new EqualsBuilder().append(address1, rhs.address1).append(address2, rhs.address2).append(address3,
            rhs.address3).append(city,
            rhs.city).append(contact_id,
            rhs.contact_id).append(country,
            rhs.country).append(email,
            rhs.email).append(first_name,
            rhs.first_name).append(last_name,
            rhs.last_name).append(middle_name,
            rhs.middle_name).append(primary_phone,
            rhs.primary_phone).append(secondary_phone,
            rhs.secondary_phone).append(pref,
            rhs.pref).append(state,
            rhs.state).append(suffix,
            rhs.suffix).append(title,
            rhs.title).append(zip,
            rhs.zip).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(1, 31).append(address1).append(address2).append(address3).append(city).append(contact_id).append(country).append(email).append(first_name)
            .append(last_name).append(middle_name).append(primary_phone).append(secondary_phone).append(pref).append(state).append(suffix).append(title).append(zip).toHashCode();
    }

    @Override
    public String toString() {
        return "Contact [first_name=" + first_name + ", last_name=" + last_name + ", address1=" + address1 + ", address2=" + address2 + ", city=" + city + ", state=" + state
            + ", zip=" + zip + ", country=" + country + "]";
    }
}
