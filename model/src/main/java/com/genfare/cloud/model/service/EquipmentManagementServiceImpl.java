package com.genfare.cloud.model.service;

import java.util.List;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.genfare.cloud.model.entity.Equipment;
import com.genfare.cloud.model.entity.Locations;
import com.genfare.cloud.model.entity.Manufacturers;
import com.genfare.cloud.model.entity.repository.EquipmentRepository;
import com.genfare.cloud.model.types.EquipmentStatusesType;
import com.genfare.cloud.model.types.EquipmenttypesType;

/**
 * Service interface for managing orders
 * 
 * @author jyore
 * @version 1.0
 */
@Transactional
@Service
public class EquipmentManagementServiceImpl
    implements EquipmentManagementService {

    @Autowired
    private EquipmentRepository eqRepo;

    @Override
    public Equipment getEquipmentBySerial(String serial)
        throws EntityNotFoundException {
        List<Equipment> eqs = eqRepo.findByEquipmentSerial(serial);
        if (eqs == null || eqs.isEmpty()) {
            return storeNewEncoder(serial);
        } else {
            return eqs.get(0);
        }
    }

    @Override
    public Equipment storeNewEncoder(String serial) {

        // TODO most of this data is being hardcoded until the proper types are setup
        Manufacturers m = new Manufacturers();
        m.setManufacturerId(new Integer(1).byteValue());
        Locations l = new Locations();
        l.setLocationId(new Integer(1).byteValue());

        Equipment e = new Equipment();
        e.setSerialNumber(serial.toUpperCase());
        e.setLocations(l);
        e.setEquipmenttypes(EquipmenttypesType.ENCODER.getType());
        e.setStatus(EquipmentStatusesType.OK.getIndex().byteValue());
        e.setManufacturers(m);
        return eqRepo.save(e);

    }
}
