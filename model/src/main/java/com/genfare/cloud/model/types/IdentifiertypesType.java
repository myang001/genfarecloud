package com.genfare.cloud.model.types;

import java.util.EnumSet;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.genfare.cloud.model.entity.Identifiertypes;
import com.genfare.cloud.model.entity.repository.IdentifiertypesRepository;

public enum IdentifiertypesType {
    ELECTRONIC_ID(1),
    PRINTED_ID(2),
    MAGNETIC_STRIP(3),
    MOBILE_PHONE(4),
    BAR_CODE(5),
    QR_CODE(6);

    private final Integer index;

    private Identifiertypes type;

    private IdentifiertypesType(Integer index) {
        this.index = index;
    }

    public Integer getIndex() {
        return index;
    }

    public Identifiertypes getType() {
        return type;
    }

    @Component
    public static class Injector {
        @Autowired
        private IdentifiertypesRepository repo;

        @PostConstruct
        public void setup() {
            for (IdentifiertypesType type : EnumSet.allOf(IdentifiertypesType.class)) {
                type.type = repo.findOne(type.index.byteValue());
            }
        }
    }
}
