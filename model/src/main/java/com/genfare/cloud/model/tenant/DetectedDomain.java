package com.genfare.cloud.model.tenant;

public interface DetectedDomain {

    public String getDomain();

    public void setDomain(String domain);

}
