package com.genfare.cloud.model.entity.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.genfare.cloud.model.entity.Paymenttypes;

public interface PaymenttypesRepository
    extends CrudRepository<Paymenttypes, Byte> {

    @Query("from Paymenttypes p where p.description = ?")
    public Paymenttypes getPaymenttypeByDescription(String description);
}
