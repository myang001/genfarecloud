package com.genfare.cloud.model.entity.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.genfare.cloud.model.entity.Groups;

public interface GroupsRepository
    extends CrudRepository<Groups, Integer> {

    @Query("select gop.groups from Groupsofpeople gop where gop.people.personId = ?")
    public List<Groups> findByUserId(Integer id);

    @Query("from Groups g where g.status = 1 and g.organizations.organizationId = ? order by g.name asc")
    public Page<Groups> findEnabledByOrganizationIdOrderByNameAsc(Integer id, Pageable pageable);

    @Query("from Groups g where g.status = 1 and g.organizations.organizationId = ?")
    public List<Groups> findGroupsByOrganization(Integer id);

    @Query("from Groups g where g.name = ?")
    public Groups findGroupByName(String name);
}
