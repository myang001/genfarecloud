package com.genfare.cloud.model.entity.inter;

import java.util.Date;

public interface Auditable {
    void setTouchedBy(String touchedBy);

    void setCreatedBy(String createdBy);

    String getCreatedBy();

    void setCreatedAt(Date date);

    Date getCreatedAt();

    void setTouchedAt(Date date);
}
