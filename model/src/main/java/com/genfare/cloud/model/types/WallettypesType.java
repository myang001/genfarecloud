package com.genfare.cloud.model.types;

import java.util.EnumSet;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.genfare.cloud.model.entity.Wallettypes;
import com.genfare.cloud.model.entity.repository.WallettypesRepository;

public enum WallettypesType {
    FARE_CARD(1),
    PAPER(2),
    DESFIRE_SMART_CARD(3),
    LUCC(4),
    MOBILE_PHONE(5),
    ADHESIVE_SMART_MEDIA(6),
    THIRD_PARTY(7),
    MUILTI_FUNCTION_SMART_CARD(8),
    NEAR_FIELD_COMM_DEVICE(9);

    private final Integer index;

    private Wallettypes type;

    private WallettypesType(Integer index) {
        this.index = index;
    }

    public Integer getIndex() {
        return index;
    }

    public Wallettypes getType() {
        return type;
    }

    @Component
    public static class Injector {
        @Autowired
        private WallettypesRepository repo;

        @PostConstruct
        public void setup() {
            for (WallettypesType type : EnumSet.allOf(WallettypesType.class)) {
                type.type = repo.findOne(type.index.byteValue());
            }
        }
    }
}
