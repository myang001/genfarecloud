package com.genfare.cloud.model.entity.repository;

import org.springframework.data.repository.CrudRepository;

import com.genfare.cloud.model.entity.Activities;

public interface ActivitiesRepository
    extends CrudRepository<Activities, Integer> {

}
