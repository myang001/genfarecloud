package com.genfare.cloud.model.entity.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.genfare.cloud.model.entity.Linktypes;

public interface LinktypesRepository
    extends CrudRepository<Linktypes, Integer> {

    @Query("from Linktypes lt where lt.description = ?")
    public Linktypes findLinkByDescription(String description);
}
