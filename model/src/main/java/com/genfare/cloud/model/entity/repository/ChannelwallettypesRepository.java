package com.genfare.cloud.model.entity.repository;

import org.springframework.data.repository.CrudRepository;

import com.genfare.cloud.model.entity.Channelwallettypes;
import com.genfare.cloud.model.entity.ChannelwallettypesId;

public interface ChannelwallettypesRepository
    extends CrudRepository<Channelwallettypes, ChannelwallettypesId> {

}
