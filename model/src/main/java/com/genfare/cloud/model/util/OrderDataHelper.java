package com.genfare.cloud.model.util;

import org.json.JSONObject;

import com.genfare.cloud.model.contact.Address;
import com.genfare.cloud.model.dto.OrderDataDTO;

public class OrderDataHelper {

    public static OrderDataDTO populateOrderData(String orderData) {
        OrderDataDTO odto = new OrderDataDTO();

        JSONObject dataJS = new JSONObject(orderData);
        JSONObject addr = (JSONObject) dataJS.get("Address");

        Address shipping = new Address();
        shipping.setCity((String) addr.get("city"));
        shipping.setPostalCode((String) addr.get("postalCode"));
        shipping.setState((String) addr.get("state"));
        shipping.setLine1((String) addr.get("line1"));
        shipping.setLine2((String) addr.get("line2"));
        shipping.setCountry((String) addr.get("country"));

        odto.setLastName((String) dataJS.get("lastName"));
        odto.setFirstName((String) dataJS.get("firstName"));
        odto.setShipping(shipping);
        return odto;
    }

    public static void main(String[] args) {
        String s =
            "{'lastName':'Bobkins','Type':'Shipping Address','Address':"
                + "{'postalCode':'60000','state':'Illinois','line1':'123 Bob St','country':'United States',"
                + "'city':'Bobbins','line2':''},'firstName':'Bob'}";

        JSONObject dataJS = new JSONObject(s);
        System.out.println(dataJS.get("lastName"));

    }

}
