package com.genfare.cloud.model.entity.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.genfare.cloud.model.entity.Pendingactivity;

public interface PendingactivityRepository
    extends CrudRepository<Pendingactivity, Integer> {

    @Query("select count(*) from Pendingactivity p where p.wallets.walletId = ?")
    public Long getPendingActivityCountForWallets(Long walletId);

}
