package com.genfare.cloud.model.types;

import java.util.EnumSet;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.genfare.cloud.model.entity.Paymenttypes;
import com.genfare.cloud.model.entity.repository.PaymenttypesRepository;

public enum PaymenttypesType {
    INVOICED(1),
    CASH(2),
    CHECK(3),
    AMERICAN_EXPRESS(4),
    VISA(5),
    MASTER_CARD(6);

    private final Integer index;

    private Paymenttypes type;

    private PaymenttypesType(Integer index) {
        this.index = index;
    }

    public Integer getIndex() {
        return index;
    }

    public Paymenttypes getType() {
        return type;
    }

    @Component
    public static class Injector {
        @Autowired
        private PaymenttypesRepository repo;

        @PostConstruct
        public void setup() {
            for (PaymenttypesType type : EnumSet.allOf(PaymenttypesType.class)) {
                type.type = repo.findOne(type.index.byteValue());
            }
        }
    }
}
