package com.genfare.cloud.model.service;

import java.util.Map;

import org.thymeleaf.context.AbstractContext;

import com.genfare.cloud.model.contact.Email;
import com.genfare.cloud.model.security.User;

/**
 * Service Interface for handling email operations
 * 
 * @author jyore
 * @version 1.0
 */
public interface EmailService {

    /**
     * Generates a verification token and automatically sends a confirmation email
     * 
     * @param user The user to generate the token for
     */
    public void generateVerificationToken(User user);

    /**
     * Verify a token
     * 
     * @param token
     * @return True if the token matches or False if it is expired or does not match
     */
    public boolean verifyToken(String token);

    /**
     * Delete a token
     * 
     * @param token The token to delete
     */
    public void deleteToken(String token);

    /**
     * Sends an email to a user
     * 
     * @param to The user to send the email to (uses preferred email)
     * @param subject The subject of the email
     * @param view The name of the Thymeleaf template to render for the email content
     * @param parameters A map of parameters to inject into the Thymeleaf template for reference
     * @author jyore
     */
    public void sendEmail(User to, String subject, String view, Map<String, Object> parameters);

    /**
     * Find an email address
     * 
     * @param email The address to find
     * @return The Email object found or null
     */
    public Email findEmail(String email);

    /**
     * Generates a verification token and automatically sends a confirmation email to recovery your password
     * 
     * @param user The user to generate the token for
     */
    public void generatePasswordRecoveryVerificationToken(User user);

    /**
     * Generates a verification token and automatically sends a confirmation email to set your new password
     * 
     * @param user The user to generate the token for
     */
    public void generateSetPasswordVerificationToken(User user);

    /**
     * Verify a token for Password Recovery
     * 
     * @param token
     * @return True if the token matches or False if it is expired or does not match
     */
    public boolean verifyPasswordRecoveryToken(String token);

    /**
     * Verify a token for Set Recovery
     * 
     * @param token
     * @return True if the token matches or False if it is expired or does not match
     */
    public boolean verifySetPasswordToken(String token);

    AbstractContext getContext();

    void setContext(AbstractContext context);
}
