package com.genfare.cloud.model.entity.repository;

import org.springframework.data.repository.CrudRepository;

import com.genfare.cloud.model.entity.Operatortypes;

public interface OperatortypesRepository
    extends CrudRepository<Operatortypes, Byte> {

}
