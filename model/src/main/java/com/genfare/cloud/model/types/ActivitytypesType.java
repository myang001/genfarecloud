package com.genfare.cloud.model.types;

import java.util.EnumSet;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.genfare.cloud.model.entity.Activitytypes;
import com.genfare.cloud.model.entity.repository.ActivitytypesRepository;

public enum ActivitytypesType {

    RIDERSHIP(1),
    CARDHISTORY(2),
    ADDVALUE(3),
    ADDPRODUCT(4),
    CARDREGISTRATION(5),
    CARD_UNREGISTRATION(6),
    CARD_BADLIST(7),
    CARD_GOODLIST(8),
    PRODUCT_BADLIST(9),
    PRODUCT_GOODLIST(10),
    SET_LOAD_SEQUENCE_NUMBER(11),
    SET_PRIOD_PRODUCT_START_DATE(12),
    SET_LANGUAGE(13),
    SET_PRODUCT_DESIGNATOR(14),
    SET_PRODUCT_EXP_TYPE(15),
    SET_PRODUCT_PRIORITY(16),
    SET_USER_PROFILE_EXPIRATION_DATE(17),
    SET_PRODUCT_TIED_TO_USER_PROFILE_FLAG(18),
    CLEAR_PRODUCT_TIED_TO_USER_PROFILE_FLAG(19),
    SET_ACCESSIBILITY(20);

    private final Integer index;

    private Activitytypes type;

    private ActivitytypesType(Integer index) {
        this.index = index;
    }

    public Integer getIndex() {
        return index;
    }

    public Activitytypes getType() {
        return type;
    }

    @Component
    public static class Injector {
        @Autowired
        private ActivitytypesRepository repo;

        @PostConstruct
        public void setup() {
            for (ActivitytypesType type : EnumSet.allOf(ActivitytypesType.class)) {
                type.type = repo.findOne(type.index.byteValue());
            }
        }
    }
}
