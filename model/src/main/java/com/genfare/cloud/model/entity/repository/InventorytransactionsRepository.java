package com.genfare.cloud.model.entity.repository;

import org.springframework.data.repository.CrudRepository;

import com.genfare.cloud.model.entity.Inventorytransactions;

public interface InventorytransactionsRepository
    extends CrudRepository<Inventorytransactions, Integer> {

}
