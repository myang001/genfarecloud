package com.genfare.cloud.model.entity;

// Generated Mar 19, 2014 9:47:20 AM by Hibernate Tools 4.0.0

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.genfare.cloud.model.entity.inter.Auditable;
import com.genfare.cloud.model.listener.AuditListener;

/**
 * Lots generated by hbm2java
 */
@Entity
@Table(name = "lots")
@EntityListeners(AuditListener.class)
public class Lots
    implements java.io.Serializable, Auditable {

    private LotsId id;

    private Shippinglists shippinglists;

    private Locations locations;

    private Integer quantity;

    private String numberStarting;

    private String numberEnding;

    private String touchedBy;

    private Date touchedAt;

    private String createdBy;

    private Date createdAt;

    private Set<Inventorytransactions> inventorytransactionses = new HashSet<Inventorytransactions>(0);

    private Set<Crossreferencefile> crossreferencefiles = new HashSet<Crossreferencefile>(0);

    public Lots() {
    }

    public Lots(LotsId id, Shippinglists shippinglists, Locations locations) {
        this.id = id;
        this.shippinglists = shippinglists;
        this.locations = locations;
    }

    public Lots(LotsId id, Shippinglists shippinglists, Locations locations, Integer quantity, String numberStarting,
        String numberEnding, Set<Inventorytransactions> inventorytransactionses,
        Set<Crossreferencefile> crossreferencefiles) {
        this.id = id;
        this.shippinglists = shippinglists;
        this.locations = locations;
        this.quantity = quantity;
        this.numberStarting = numberStarting;
        this.numberEnding = numberEnding;
        this.inventorytransactionses = inventorytransactionses;
        this.crossreferencefiles = crossreferencefiles;
    }

    @EmbeddedId
    @AttributeOverrides({
        @AttributeOverride(name = "shippingListId", column = @Column(name = "ShippingListID", nullable = false)),
        @AttributeOverride(name = "lotNumber", column = @Column(name = "LotNumber", nullable = false)) })
    public LotsId getId() {
        return this.id;
    }

    public void setId(LotsId id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ShippingListID", nullable = false, insertable = false, updatable = false)
    public Shippinglists getShippinglists() {
        return this.shippinglists;
    }

    public void setShippinglists(Shippinglists shippinglists) {
        this.shippinglists = shippinglists;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "LocationID", nullable = false)
    public Locations getLocations() {
        return this.locations;
    }

    public void setLocations(Locations locations) {
        this.locations = locations;
    }

    @Column(name = "Quantity")
    public Integer getQuantity() {
        return this.quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    @Column(name = "NumberStarting", length = 50)
    public String getNumberStarting() {
        return this.numberStarting;
    }

    public void setNumberStarting(String numberStarting) {
        this.numberStarting = numberStarting;
    }

    @Column(name = "NumberEnding", length = 50)
    public String getNumberEnding() {
        return this.numberEnding;
    }

    public void setNumberEnding(String numberEnding) {
        this.numberEnding = numberEnding;
    }

    @Column(name = "TouchedBy", nullable = false, length = 50)
    public String getTouchedBy() {
        return this.touchedBy;
    }

    @Override
    public void setTouchedBy(String touchedBy) {
        this.touchedBy = touchedBy;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "TouchedAt", nullable = false, length = 19)
    public Date getTouchedAt() {
        return this.touchedAt;
    }

    @Override
    public void setTouchedAt(Date touchedAt) {
        this.touchedAt = touchedAt;
    }

    @Override
    @Column(name = "CreatedBy", nullable = false, length = 50)
    public String getCreatedBy() {
        return this.createdBy;
    }

    @Override
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Override
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CreatedAt", nullable = false, length = 19)
    public Date getCreatedAt() {
        return this.createdAt;
    }

    @Override
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "lots")
    public Set<Inventorytransactions> getInventorytransactionses() {
        return this.inventorytransactionses;
    }

    public void setInventorytransactionses(Set<Inventorytransactions> inventorytransactionses) {
        this.inventorytransactionses = inventorytransactionses;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "lots")
    public Set<Crossreferencefile> getCrossreferencefiles() {
        return this.crossreferencefiles;
    }

    public void setCrossreferencefiles(Set<Crossreferencefile> crossreferencefiles) {
        this.crossreferencefiles = crossreferencefiles;
    }

}
