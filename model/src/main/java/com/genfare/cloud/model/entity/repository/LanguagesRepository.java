package com.genfare.cloud.model.entity.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.genfare.cloud.model.entity.Languages;

public interface LanguagesRepository
    extends CrudRepository<Languages, Byte> {

    @Query("from Languages l where l.locale = ?")
    public Languages findLanguage(String locale);
}
