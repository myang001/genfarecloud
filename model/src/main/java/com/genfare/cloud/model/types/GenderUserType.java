package com.genfare.cloud.model.types;

public class GenderUserType
    extends EnumUserType<GenderType> {
    public GenderUserType() {
        super(GenderType.class);
    }
}