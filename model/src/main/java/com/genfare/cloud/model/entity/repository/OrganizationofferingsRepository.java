package com.genfare.cloud.model.entity.repository;

import org.springframework.data.repository.CrudRepository;

import com.genfare.cloud.model.entity.Organizationofferings;
import com.genfare.cloud.model.entity.OrganizationofferingsId;

public interface OrganizationofferingsRepository
    extends CrudRepository<Organizationofferings, OrganizationofferingsId> {

}
