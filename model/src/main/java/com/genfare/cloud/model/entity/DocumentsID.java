package com.genfare.cloud.model.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Embeddable
public class DocumentsID
    implements java.io.Serializable {

    private static final long serialVersionUID = 1L;

    private String category;

    private String pageKey;

    private Languages language;

    public DocumentsID() {
    }

    public DocumentsID(String category, Languages language, String pageKey) {
        this.category = category;
        this.language = language;
        this.pageKey = pageKey;
    }

    @Column(name = "Category", nullable = false)
    public String getCategory() {
        return this.category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @Column(name = "PageKey", nullable = false)
    public String getPageKey() {
        return pageKey;
    }

    public void setPageKey(String pageKey) {
        this.pageKey = pageKey;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "LanguageID", nullable = false)
    public Languages getLanguage() {
        return this.language;
    }

    public void setLanguage(Languages language) {
        this.language = language;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
            + ((category == null) ? 0 : category.hashCode());
        result = prime * result + ((pageKey == null) ? 0 : pageKey.hashCode());
        result = prime * result + language.getLanguageId();
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        DocumentsID other = (DocumentsID) obj;
        if (category == null) {
            if (other.category != null)
                return false;
        } else if (!category.equals(other.category))
            return false;
        if (pageKey == null) {
            if (other.pageKey != null)
                return false;
        } else if (!pageKey.equals(other.pageKey))
            return false;
        if (language != other.language)
            return false;
        return true;
    }
}