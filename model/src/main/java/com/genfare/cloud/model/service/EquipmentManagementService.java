package com.genfare.cloud.model.service;

import com.genfare.cloud.model.entity.Equipment;

/**
 * Service interface for managing orders
 * 
 * @author amoussa
 * @version 1.0
 */
public interface EquipmentManagementService {

    /**
     * Get Equipment by Serial Number
     * 
     * @return The equipment or null
     * @throws EntityNotFoundException
     */
    Equipment getEquipmentBySerial(String serial);

    /**
     * Save equipment in DB
     * 
     * @param serial
     * @return
     */
    Equipment storeNewEncoder(String serial);

}
