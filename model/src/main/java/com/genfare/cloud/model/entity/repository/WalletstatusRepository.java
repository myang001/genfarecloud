package com.genfare.cloud.model.entity.repository;

import org.springframework.data.repository.CrudRepository;

import com.genfare.cloud.model.entity.Walletstatus;

public interface WalletstatusRepository
    extends CrudRepository<Walletstatus, Byte> {

}
