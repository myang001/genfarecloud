package com.genfare.cloud.model.entity.repository;

import java.util.Collection;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.genfare.cloud.model.entity.Offerings;

public interface OfferingsRepository
    extends CrudRepository<Offerings, Byte> {

    @Query("from Offerings o where o.tickets.tickettypes.description = ?")
    public Iterable<Offerings> findOfferingsByTicketType(String description);

    @Query("from Offerings o where o.tickets.tickettypes.description like ?")
    public Iterable<Offerings> findOfferingsBySimTicketTypes(String description);

    @Query("select o from Offerings o inner join o.channelofferings co where co.id.channelId = ? and o.wallettypes.walletTypeId <> 'NULL' and o.dateEffective <= CURRENT_TIMESTAMP and o.dateExpires >= CURRENT_TIMESTAMP")
    public
        Iterable<Offerings> findWallettypeOfferings(Byte channelId);

    @Query("from Offerings o where o.description = ?")
    public Offerings findByDescription(String description);

    @Query("select distinct o from Offerings o, TicketsFarecodes tf, Tickets t inner join o.channelofferings co where tf.id.ticketId = o.tickets.ticketId and t.ticketId = o.tickets.ticketId and co.id.channelId = :channelId and tf.id.farecodeId = :farecode and o.dateEffective <= CURRENT_TIMESTAMP and o.dateExpires >= CURRENT_TIMESTAMP")
    public
        List<Offerings> findOfferingsFilterChannelAndFarecode(@Param("channelId") Byte channelId, @Param("farecode") Byte farecode);

    @Query("select distinct o from Offerings o, TicketsFarecodes tf, Tickets t inner join o.channelofferings co where tf.id.ticketId = o.tickets.ticketId and t.ticketId = o.tickets.ticketId and co.id.channelId = :channelId and tf.id.farecodeId = :farecode and t.tickettypes.ticketTypeId = :ticketTypeId and o.dateEffective <= CURRENT_TIMESTAMP and o.dateExpires >= CURRENT_TIMESTAMP")
    public
        List<Offerings>
        findOfferingsFilterChannelAndFarecodeAndType(@Param("channelId") Byte channelId, @Param("farecode") Byte farecode, @Param("ticketTypeId") Byte ticketTypeId);

    @Query("select distinct o from Offerings o, TicketsFarecodes tf, Tickets t inner join o.channelofferings co where tf.id.ticketId = o.tickets.ticketId and t.ticketId = o.tickets.ticketId and co.id.channelId = :channelId and tf.id.farecodeId = :farecode and t.tickettypes.ticketTypeId in (:ticketTypeId) and o.dateEffective <= CURRENT_TIMESTAMP and o.dateExpires >= CURRENT_TIMESTAMP")
    public
        List<Offerings> findOfferingsFilterChannelAndFarecodeAndType(@Param("channelId") Byte channelId, @Param("farecode") Byte farecode,
            @Param("ticketTypeId") Collection<Byte> ticketTypeId);

    @Query("from Offerings o where o.wallettypes.walletTypeId = ?")
    public Offerings findByWalletType(Byte walletTypeId);

    @Query("select distinct o from Offerings o, TicketsFarecodes tf inner join o.channelofferings co inner join o.tickets where tf.id.ticketId = o.tickets.ticketId and co.id.channelId = :channelId and tf.id.farecodeId = :farecodeId and o.tickets.ticketId = :ticketId and o.dateEffective <= CURRENT_TIMESTAMP and o.dateExpires >= CURRENT_TIMESTAMP")
    public
        List<Offerings> findOfferingsByTicketEligibleForBuyMore(@Param("channelId") Byte channelId, @Param("farecodeId") Byte farecode, @Param("ticketId") Integer ticketId);
}
