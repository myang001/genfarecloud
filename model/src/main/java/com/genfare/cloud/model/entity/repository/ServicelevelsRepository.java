package com.genfare.cloud.model.entity.repository;

import org.springframework.data.repository.CrudRepository;

import com.genfare.cloud.model.entity.Servicelevels;

public interface ServicelevelsRepository
    extends CrudRepository<Servicelevels, Integer> {

}
