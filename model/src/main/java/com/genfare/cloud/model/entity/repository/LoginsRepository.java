package com.genfare.cloud.model.entity.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.genfare.cloud.model.entity.Logins;

public interface LoginsRepository
    extends CrudRepository<Logins, Integer> {

    @Query("select l from Logins l where l.userLogin = ?")
    public Logins findByUserLogin(String userLogin);

    @Query("select p.personId from Logins l join l.people p where l.userLogin = ?")
    public Integer findPersonIdFromLogin(String userLogin);

    @Query("select CONCAT(p.nameFirst,' ',p.nameLast) from People p inner join p.loginses l where l.userLogin = ?")
    public String findFirstLastNameFromLogin(String login);

    @Query("select count(*) from Orderitems oi, Logins l inner join oi.orders o where oi.wallets = null and o.people.personId = l.people.personId and o.orderstatuses.status = 1 and l.userLogin = ?")
    public
        Long findOrderItemCountFromLogin(String login);
}