package com.genfare.cloud.model.converters.pos.activity;

import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;

import com.genfare.cloud.model.entity.Processedactivity;
import com.genfare.pos.core.model.processed.ProcessedActivityType;

public class ProcessedActivitiesToProcessedActivityTypeConverter implements
    Converter<Processedactivity, ProcessedActivityType> {

    private static final Logger log = LoggerFactory
        .getLogger(ProcessedActivitiesToProcessedActivityTypeConverter.class);

    @Override
    public ProcessedActivityType convert(Processedactivity pa) {
        GregorianCalendar c = new GregorianCalendar();
        ProcessedActivityType pat = new ProcessedActivityType();

        pat.setPendingId(pa.getPendingId().intValue());
        pat.setPendingStatus(pa.getPendingStatus());

        try {
            c.setTime(pa.getDateApplied());
            pat.setDateApplied(DatatypeFactory.newInstance()
                .newXMLGregorianCalendar(c));
            c.setTime(pa.getDateExpires());
        } catch (DatatypeConfigurationException e) {
            log.error("Error converting date", e);
            return null;
        }

        return pat;
    }
}
