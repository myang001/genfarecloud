package com.genfare.cloud.model.support;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@ComponentScan(basePackages = { "com.genfare.cloud.model.support", "com.genfare.cloud.model.tenant",
    "com.genfare.cloud.model.util", "com.genfare.cloud.model.types" })
@EnableJpaRepositories(basePackages = { "com.genfare.cloud.model.tenant" })
@PropertySource("classpath:db/config/orm.properties")
public class ModelApplicationContext {

    @Bean
    public PropertySourcesPlaceholderConfigurer propertyPlaceHolderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }
}