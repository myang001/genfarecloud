package com.genfare.cloud.model.entity.repository;

import org.springframework.data.repository.CrudRepository;

import com.genfare.cloud.model.entity.Timeperiods;

public interface TimeperiodsRepository
    extends CrudRepository<Timeperiods, Integer> {

}
