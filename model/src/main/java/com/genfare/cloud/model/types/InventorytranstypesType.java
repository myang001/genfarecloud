package com.genfare.cloud.model.types;

import java.util.EnumSet;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.genfare.cloud.model.entity.Inventorytranstypes;
import com.genfare.cloud.model.entity.repository.InventorytranstypesRepository;

public enum InventorytranstypesType {
    ;

    private final Integer index;

    private Inventorytranstypes type;

    private InventorytranstypesType(Integer index) {
        this.index = index;
    }

    public Integer getIndex() {
        return index;
    }

    public Inventorytranstypes getType() {
        return type;
    }

    @Component
    public static class Injector {
        @Autowired
        private InventorytranstypesRepository repo;

        @PostConstruct
        public void setup() {
            for (InventorytranstypesType type : EnumSet.allOf(InventorytranstypesType.class)) {
                type.type = repo.findOne(type.index.byteValue());
            }
        }
    }
}
