package com.genfare.cloud.model.entity.repository;

import org.springframework.data.repository.CrudRepository;

import com.genfare.cloud.model.entity.Routes;

public interface RoutesRepository
    extends CrudRepository<Routes, Integer> {

}
