package com.genfare.cloud.model.types;

import com.genfare.cloud.model.entity.Orderstatuses;

public enum OrderPaymentStatusType {
    UNPAID(OrderStatusBaseType.UNPAID),
    INVOICED(OrderStatusBaseType.INVOICED),
    PAID(OrderStatusBaseType.PAID);

    private final OrderStatusBaseType type;

    private OrderPaymentStatusType(OrderStatusBaseType type) {
        this.type = type;
    }

    /**
     * Gets the ordinal for the base type as in the DB
     * 
     * @return the position of the base type
     */
    public Integer getIndex() {
        return type.getIndex();
    }

    /**
     * Gets the entity representing the status
     * 
     * @return The Orderstatus entity
     */
    public Orderstatuses getStatus() {
        return type.getStatus();
    }
}
