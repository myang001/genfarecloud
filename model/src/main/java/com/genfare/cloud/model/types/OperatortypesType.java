package com.genfare.cloud.model.types;

import java.util.EnumSet;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.genfare.cloud.model.entity.Operatortypes;
import com.genfare.cloud.model.entity.repository.OperatortypesRepository;

public enum OperatortypesType {
    ;

    private final Integer index;

    private Operatortypes type;

    private OperatortypesType(Integer index) {
        this.index = index;
    }

    public Integer getIndex() {
        return index;
    }

    public Operatortypes getType() {
        return type;
    }

    @Component
    public static class Injector {
        @Autowired
        private OperatortypesRepository repo;

        @PostConstruct
        public void setup() {
            for (OperatortypesType type : EnumSet.allOf(OperatortypesType.class)) {
                type.type = repo.findOne(type.index.byteValue());
            }
        }
    }
}
