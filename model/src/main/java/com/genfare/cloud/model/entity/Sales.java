package com.genfare.cloud.model.entity;

// Generated Apr 14, 2014 9:57:20 AM by Hibernate Tools 3.4.0.CR1

import static javax.persistence.GenerationType.IDENTITY;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Sales generated by hbm2java
 */
@Entity
@Table(name = "sales")
public class Sales
    implements java.io.Serializable {

    private Long salesId;

    private Tickettypes tickettypes;

    private Equipment equipment;

    private Offerings offerings;

    private Paymenttypes paymenttypes;

    private Dates dates;

    private Orderitems orderitems;

    private Farecodes farecodes;

    private short quantity;

    private BigDecimal unitPrice;

    private BigDecimal amount;

    private BigDecimal amountShipping;

    private BigDecimal amountTax;

    public Sales() {
    }

    public Sales(Equipment equipment, Offerings offerings, Paymenttypes paymenttypes, Dates dates, short quantity,
        BigDecimal unitPrice, BigDecimal amount) {
        this.equipment = equipment;
        this.offerings = offerings;
        this.paymenttypes = paymenttypes;
        this.dates = dates;
        this.quantity = quantity;
        this.unitPrice = unitPrice;
        this.amount = amount;
    }

    public Sales(Tickettypes tickettypes, Equipment equipment, Offerings offerings, Paymenttypes paymenttypes,
        Dates dates, Orderitems orderitems, Farecodes farecodes, short quantity, BigDecimal unitPrice,
        BigDecimal amount, BigDecimal amountShipping, BigDecimal amountTax) {
        this.tickettypes = tickettypes;
        this.equipment = equipment;
        this.offerings = offerings;
        this.paymenttypes = paymenttypes;
        this.dates = dates;
        this.orderitems = orderitems;
        this.farecodes = farecodes;
        this.quantity = quantity;
        this.unitPrice = unitPrice;
        this.amount = amount;
        this.amountShipping = amountShipping;
        this.amountTax = amountTax;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "SalesID", unique = true, nullable = false)
    public Long getSalesId() {
        return this.salesId;
    }

    public void setSalesId(Long salesId) {
        this.salesId = salesId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TicketTypeID")
    public Tickettypes getTickettypes() {
        return this.tickettypes;
    }

    public void setTickettypes(Tickettypes tickettypes) {
        this.tickettypes = tickettypes;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "EquipmentID", nullable = false)
    public Equipment getEquipment() {
        return this.equipment;
    }

    public void setEquipment(Equipment equipment) {
        this.equipment = equipment;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "OfferingID", nullable = false)
    public Offerings getOfferings() {
        return this.offerings;
    }

    public void setOfferings(Offerings offerings) {
        this.offerings = offerings;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PaymentTypeID", nullable = false)
    public Paymenttypes getPaymenttypes() {
        return this.paymenttypes;
    }

    public void setPaymenttypes(Paymenttypes paymenttypes) {
        this.paymenttypes = paymenttypes;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DateID", nullable = false)
    public Dates getDates() {
        return this.dates;
    }

    public void setDates(Dates dates) {
        this.dates = dates;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumns({ @JoinColumn(name = "OrderID", referencedColumnName = "OrderID"),
        @JoinColumn(name = "OrderItemNumber", referencedColumnName = "OrderItemNumber") })
    public Orderitems getOrderitems() {
        return this.orderitems;
    }

    public void setOrderitems(Orderitems orderitems) {
        this.orderitems = orderitems;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FarecodeID")
    public Farecodes getFarecodes() {
        return this.farecodes;
    }

    public void setFarecodes(Farecodes farecodes) {
        this.farecodes = farecodes;
    }

    @Column(name = "Quantity", nullable = false)
    public short getQuantity() {
        return this.quantity;
    }

    public void setQuantity(short quantity) {
        this.quantity = quantity;
    }

    @Column(name = "UnitPrice", nullable = false, precision = 5)
    public BigDecimal getUnitPrice() {
        return this.unitPrice;
    }

    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

    @Column(name = "Amount", nullable = false, precision = 7)
    public BigDecimal getAmount() {
        return this.amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @Column(name = "AmountShipping", precision = 5)
    public BigDecimal getAmountShipping() {
        return this.amountShipping;
    }

    public void setAmountShipping(BigDecimal amountShipping) {
        this.amountShipping = amountShipping;
    }

    @Column(name = "AmountTax", precision = 5)
    public BigDecimal getAmountTax() {
        return this.amountTax;
    }

    public void setAmountTax(BigDecimal amountTax) {
        this.amountTax = amountTax;
    }

}
