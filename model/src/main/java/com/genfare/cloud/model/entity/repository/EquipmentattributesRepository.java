package com.genfare.cloud.model.entity.repository;

import org.springframework.data.repository.CrudRepository;

import com.genfare.cloud.model.entity.Equipmentattributes;

public interface EquipmentattributesRepository
    extends CrudRepository<Equipmentattributes, Integer> {

}
