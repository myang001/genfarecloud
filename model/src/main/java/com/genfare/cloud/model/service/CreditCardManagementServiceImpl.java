package com.genfare.cloud.model.service;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.genfare.cloud.model.entity.Peoplecards;
import com.genfare.cloud.model.entity.repository.PeoplecardsRepository;
import com.genfare.cloud.model.security.User;

@Transactional
@Service
public class CreditCardManagementServiceImpl
    implements CreditCardManagementService {

    @Autowired
    private PeoplecardsRepository peopleCardsRepository;

    @Override
    public Peoplecards getLastUsedCardByUser(User user) {
        return peopleCardsRepository.getLastUsedByUser(user.getId());
    }

    @Override
    public Date getCardExpiration(Peoplecards card) {
        return card.getDateExpires();
    }

}
