package com.genfare.cloud.model.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.genfare.cloud.model.entity.inter.Auditable;
import com.genfare.cloud.model.listener.AuditListener;

/**
 * FulfillmentItems manually created
 * 
 * @author alimoussa
 */
@Entity
@Table(name = "fulfillmentitems")
@EntityListeners(AuditListener.class)
public class FulfillmentItems
    implements java.io.Serializable, Auditable {

    private Long fulfillmentID;

    private Integer orderID;

    private Byte orderItemNumber;

    private Wallets wallet;

    private Fulfillments fulfillments;

    private Boolean isShipped;

    private String touchedBy;

    private Date touchedAt;

    private String createdBy;

    private Date createdAt;

    public FulfillmentItems() {
    }

    public FulfillmentItems(Long fulfillmentID) {
        this.fulfillmentID = fulfillmentID;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "FulfillmentID", unique = true, nullable = false)
    public Long getFulfillmentID() {
        return this.fulfillmentID;
    }

    public void setFulfillmentID(Long fulfillmentID) {
        this.fulfillmentID = fulfillmentID;
    }

    @Column(name = "OrderID", nullable = false)
    public Integer getOrderID() {
        return orderID;
    }

    public void setOrderID(Integer orderID) {
        this.orderID = orderID;
    }

    @Column(name = "OrderItemNumber", nullable = false)
    public Byte getOrderItemNumber() {
        return orderItemNumber;
    }

    public void setOrderItemNumber(Byte orderItemNumber) {
        this.orderItemNumber = orderItemNumber;
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "WalletID")
    public Wallets getWallet() {
        return this.wallet;
    }

    public void setWallet(Wallets wallet) {
        this.wallet = wallet;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumns({ @JoinColumn(name = "OrderID", nullable = false, insertable = false, updatable = false),
        @JoinColumn(name = "OrderItemNumber", nullable = false, insertable = false, updatable = false) })
    public Fulfillments getFulfillments() {
        return fulfillments;
    }

    public void setFulfillments(Fulfillments fulfillments) {
        this.fulfillments = fulfillments;
    }

    @Column(name = "IsShipped", nullable = false)
    public Boolean getIsShipped() {
        return isShipped;
    }

    public void setIsShipped(Boolean isShipped) {
        this.isShipped = isShipped;
    }

    @Column(name = "TouchedBy", nullable = false, length = 50)
    public String getTouchedBy() {
        return this.touchedBy;
    }

    @Override
    public void setTouchedBy(String touchedBy) {
        this.touchedBy = touchedBy;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "TouchedAt", nullable = false, length = 19)
    public Date getTouchedAt() {
        return this.touchedAt;
    }

    @Override
    public void setTouchedAt(Date touchedAt) {
        this.touchedAt = touchedAt;
    }

    @Override
    @Column(name = "CreatedBy", nullable = false, length = 50)
    public String getCreatedBy() {
        return this.createdBy;
    }

    @Override
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Override
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CreatedAt", nullable = false, length = 19)
    public Date getCreatedAt() {
        return this.createdAt;
    }

    @Override
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

}
