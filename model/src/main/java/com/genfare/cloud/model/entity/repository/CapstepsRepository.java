package com.genfare.cloud.model.entity.repository;

import org.springframework.data.repository.CrudRepository;

import com.genfare.cloud.model.entity.Capsteps;

public interface CapstepsRepository
    extends CrudRepository<Capsteps, Integer> {

}
