package com.genfare.cloud.model.types;

import java.util.EnumSet;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.genfare.cloud.model.entity.Walletstatus;
import com.genfare.cloud.model.entity.repository.WalletstatusRepository;

public enum WalletstatusType {
    UNKNOWN(1),
    ACTIVE(2),
    LOST(3),
    STOLEN(4),
    DAMAGED(5),
    DEFECTIVE(6),
    EXPIRED(7);

    private final Integer index;

    private Walletstatus status;

    private WalletstatusType(Integer index) {
        this.index = index;
    }

    public Integer getIndex() {
        return index;
    }

    public Walletstatus getStatus() {
        return status;
    }

    @Component
    public static class Injector {
        @Autowired
        private WalletstatusRepository repo;

        @PostConstruct
        public void setup() {
            for (WalletstatusType status : EnumSet.allOf(WalletstatusType.class)) {
                status.status = repo.findOne(status.index.byteValue());
            }
        }
    }
}
