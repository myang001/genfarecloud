package com.genfare.cloud.model.types;

import java.util.EnumSet;
import java.util.HashSet;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.genfare.cloud.model.entity.EquipmentStatuses;
import com.genfare.cloud.model.entity.repository.EquipmentStatusesRepository;

public enum EquipmentStatusesType {
    OK(1);

    private final Integer index;

    private EquipmentStatuses status;

    private static Set<String> statusStrings = new HashSet<String>();

    private EquipmentStatusesType(Integer index) {
        this.index = index;
    }

    public Integer getIndex() {
        return index;
    }

    public EquipmentStatuses getStatus() {
        return status;
    }

    @Component
    public static class Injector {
        @Autowired
        private EquipmentStatusesRepository statusRepo;

        @PostConstruct
        public void setup() {
            for (EquipmentStatusesType status : EnumSet.allOf(EquipmentStatusesType.class)) {
                status.status = statusRepo.findOne(status.index.byteValue());
            }
        }
    }
}
