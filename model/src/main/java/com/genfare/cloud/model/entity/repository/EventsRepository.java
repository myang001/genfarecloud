package com.genfare.cloud.model.entity.repository;

import org.springframework.data.repository.CrudRepository;

import com.genfare.cloud.model.entity.EventTypes;

public interface EventsRepository
    extends CrudRepository<EventTypes, Integer> {

}
