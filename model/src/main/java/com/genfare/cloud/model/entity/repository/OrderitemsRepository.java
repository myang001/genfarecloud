package com.genfare.cloud.model.entity.repository;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.genfare.cloud.model.entity.Orderitems;
import com.genfare.cloud.model.entity.OrderitemsId;

public interface OrderitemsRepository
    extends CrudRepository<Orderitems, OrderitemsId> {

    @Query("from Orderitems oi where oi.id.orderId = ?")
    public List<Orderitems> findByOrderId(Integer orderId);

    @Query("from Orderitems oi where oi.offerings.offeringId = ?2 and oi.orders.orderId = ?1")
    public Orderitems findByOfferingId(Integer orderId, Byte offeringId);

    // HQL requires named params instead of ordinals to use sets
    @Query("select oi from Orderitems oi, Orderitems op "
        + "where op.wallets.walletId = :wallet "
        + "and oi.orders.orderId = op.orders.orderId "
        + "and oi.parentItemNumber = op.id.orderItemNumber "
        + "and oi.orderstatuses.status in (:statusSet)")
    public List<Orderitems> findByWalletAndStatusSet(@Param("wallet") Long walletId, @Param("statusSet") Collection<Byte> statusSet);

    @Query("select op from Orderitems oi, Orderitems op "
        + "where op.wallets.walletId = :wallet "
        + "and oi.tickets.ticketId = :ticket "
        + "and oi.orders.orderId = op.orders.orderId "
        + "and oi.orderstatuses.status in (:statusSet)")
    public List<Orderitems> findByWalletAndTicketAndStatusSet(@Param("wallet") Long walletId, @Param("ticket") Integer ticketId, @Param("statusSet") Collection<Byte> statusSet);

    @Query("from Orderitems oi where oi.orders.orderId = ? and oi.wallets.walletId = ?")
    public Orderitems findByOrderIdWalletId(Integer orderId, Long walletId);

    @Query("select oi.tickets.value from Orderitems oi where oi.id.orderId = ?1 and oi.id.orderItemNumber = ?2")
    public BigDecimal findValueForOrderItem(Integer orderId, byte orderItemNumber);

}
