package com.genfare.cloud.model.converters.pos.terminal;

import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.genfare.cloud.model.entity.Configs;
import com.genfare.cloud.model.entity.Equipment;
import com.genfare.cloud.model.tenant.impl.TenantConfigurationImpl;
import com.genfare.pos.core.model.terminal.TerminalConfigAPIType;

public class TenantConfigurationImplAndEquipmentToTerminalConfigAPIType {

    private static final Logger log = LoggerFactory
        .getLogger(TenantConfigurationImplAndEquipmentToTerminalConfigAPIType.class);

    public TerminalConfigAPIType convert(TenantConfigurationImpl tci,
        Equipment eq) {
        TerminalConfigAPIType tcat = new TerminalConfigAPIType();

        GregorianCalendar c = new GregorianCalendar();
        try {
            c.setTime(eq.getTouchedAt());

            tcat.setLastModifiedDate(DatatypeFactory.newInstance()
                .newXMLGregorianCalendar(c));
        } catch (DatatypeConfigurationException e) {
            log.error("Error converting date", e);
            return null;
        }
        tcat.setEquipmentId(eq.getEquipmentId());
        tcat.setEquipmentTypeId(eq.getEquipmenttypes().getEquipmentTypeId());
        tcat.setAgencyId(eq.getOrganizations().getOrganizationId());
        tcat.setAgencyName(eq.getOrganizations().getName());
        for (Configs conf : eq.getConfigss()) {
            if (conf.getConfigKey().equalsIgnoreCase(
                "MaxUnsyncedTransactionNumber")) {
                try {
                    tcat.setMaxUnsyncedTransactionNumber(Integer.valueOf(conf
                        .getConfigValue()));
                } catch (NumberFormatException e) {
                    log.error("MaxUnsyncedTransactionNumber Config value not an integer "
                        + e);
                }
            }
            if (conf.getConfigKey().equalsIgnoreCase(
                "MaxUnsyncedTransactionValue")) {
                try {
                    tcat.setMaxUnsyncedTransactionValue(Integer.valueOf(conf
                        .getConfigValue()));
                } catch (NumberFormatException e) {
                    log.error("MaxUnsyncedTransactionValue Config value not an integer "
                        + e);
                }
            }

        }
        // tcat.setMaxUnsyncedTransactionNumber(e.getE);

        OrganizationaddressesToAddressType oa2at = new OrganizationaddressesToAddressType();
        if (!eq.getOrganizations().getOrganizationaddresseses().isEmpty()) {
            tcat.setLocationAddress(oa2at.convert(eq.getOrganizations()
                .getOrganizationaddresseses().iterator().next()));
        }
        AmazonConfigurationTypeToAmazonConfigurationTypePOS ac2actPOS = new AmazonConfigurationTypeToAmazonConfigurationTypePOS();
        tcat.setAmazonConfiguration(ac2actPOS.convert(tci
            .getAmazonConfiguration()));
        tcat.setCreditServiceName(tci.getCreditServiceName());
        tcat.setCreditServicePostURL(tci.getCreditServicePostURL());
        // We do not have creditServiceAPIKey
        // tcat.setCreditServiceAPIKey(tci.setCredService);
        tcat.setCreditServiceCartPage(tci.getCreditServiceCartPage());
        tcat.setCreditServiceReturnURL(tci.getCreditServiceReturnURL());
        tcat.setCreditServiceCancelURL(tci.getCreditServiceCancelURL());
        tcat.setCreditServiceMerchantCode(tci.getCreditServiceMerchantCode());
        tcat.setCreditServicePassword(tci.getCreditServicePassword());
        tcat.setCreditServiceSettleMerchantCode(tci
            .getCreditServiceSettleMerchantCode());
        tcat.setCreditServiceXLogin(tci.getCreditServiceXLogin());
        tcat.setCreditServiceXShowForm(tci.getCreditServiceXShowForm());
        tcat.setCreditServiceXCurrencyCode(tci.getCreditServiceXCurrencyCode());
        tcat.setCreditServiceXTestRequest(tci.getCreditServiceXTestRequest());
        tcat.setCreditServiceXRelayResponse(tci
            .getCreditServiceXRelayResponse());
        tcat.setCreditServiceAgencyKey(tci.getCreditServiceAgencyKey());
        tcat.setCreditServiceRelayResponseKey(tci
            .getCreditServiceRelayResponseKey());
        // Another field we don not have
        // tcat.setCreditServiceGatewayId(tci.getCreditService);
        // tcat.setCreditServiceAPIPassword();
        // tcat.setCreditServiceAPIKey(tci.getcreditServiceAPIKey);

        return tcat;
    }
}
