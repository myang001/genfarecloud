package com.genfare.cloud.model.entity.repository;

import java.util.Date;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.genfare.cloud.model.entity.Peoplecards;
import com.genfare.cloud.model.entity.PeoplecardsId;

public interface PeoplecardsRepository
    extends CrudRepository<Peoplecards, PeoplecardsId> {

    @Query("select count(*) from Peoplecards p where p.id.personId = ?")
    public Long getPersonCardCount(Integer personId);

    @Query("from Peoplecards p where p.id.personId = ? and p.touchedAt = (select max(subp.touchedAt) from Peoplecards as subp)")
    public Peoplecards getLastUsedByUser(Integer personId);

    @Query("select p.dateExpires from Peoplecards p where p.id.personId = ?1 and p.id.cardNumber = ?2")
    public Date getExpirationForPeopleCard(Integer personId, Integer cardNumber);

    @Query("select p.paymenttypes.description from Peoplecards p where p.id.personId = ?1 and p.id.cardNumber = ?2")
    public String getPaymentTypeForPeopleCard(Integer personId, Integer cardNumber);

    @Query("select p.token from Peoplecards p where p.id.personId = ?1 and p.id.cardNumber = ?2")
    public String getTokenForPeopleCard(Integer personId, Integer cardNumber);
}
