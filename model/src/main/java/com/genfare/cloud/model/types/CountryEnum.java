package com.genfare.cloud.model.types;

public enum CountryEnum {

    UNITED_STATES(1, "United States"),
    CANADE(2, "Canada");

    private int id;

    private String description;

    private CountryEnum(int id, String description) {
        this.id = id;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }
}
