package com.genfare.cloud.model.entity.repository;

import org.springframework.data.repository.CrudRepository;

import com.genfare.cloud.model.entity.Documents;
import com.genfare.cloud.model.entity.DocumentsID;

public interface DocumentsRepository extends CrudRepository<Documents, DocumentsID> {

}