package com.genfare.cloud.model.service;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.genfare.cloud.model.entity.Orders;
import com.genfare.cloud.model.entity.repository.OrderitemactionsRepository;
import com.genfare.cloud.model.entity.repository.OrderitemsRepository;
import com.genfare.cloud.model.entity.repository.OrderitemtypesRepository;
import com.genfare.cloud.model.entity.repository.OrdersRepository;
import com.genfare.cloud.model.entity.repository.OrderstatusesRepository;
import com.genfare.cloud.model.entity.repository.OrdertypesRepository;
import com.genfare.cloud.model.entity.repository.PeopleRepository;
import com.genfare.cloud.model.types.ActivitytypesType;
import com.genfare.cloud.model.types.FulfillmentSearchType;
import com.genfare.cloud.model.types.OrderStatusType;
import com.genfare.cloud.model.types.OrderitemactionsType;
import com.genfare.cloud.model.types.OrderitemtypesType;

@RunWith(MockitoJUnitRunner.class)
public class OrderManagmentServiceTest {

    private static final Byte COMPLETED = OrderStatusType.COMPLETE.getIndex().byteValue();

    private static final Byte CANCELED = OrderStatusType.CANCELLED.getIndex().byteValue();

    private static final Byte INPROGRESS = OrderStatusType.IN_PROGRESS.getIndex().byteValue();

    private static final Byte FULFILLMENT = OrderStatusType.FULFILLMENT.getIndex().byteValue();

    private static final Byte ALL = FulfillmentSearchType.ALL.getIndex().byteValue();

    private static final Byte NEW_ACTION = OrderitemactionsType.NEW.getIndex().byteValue();

    private static final Byte WALLET_TYPE = OrderitemtypesType.WALLETTYPE.getIndex().byteValue();

    private static final Byte ADD_VALUE = ActivitytypesType.ADDVALUE.getIndex().byteValue();

    private static final Byte ADD_PRODUCT = ActivitytypesType.ADDPRODUCT.getIndex().byteValue();

    private static final Byte PENDING_AUTOLOAD = FulfillmentSearchType.PENDING_AUTOLOAD.getIndex().byteValue();

    private static final Byte PENDING_FULFILLMENT = FulfillmentSearchType.PENDING_FULFILLMENT.getIndex().byteValue();

    private static final Byte COMPLETED_SEARCH = FulfillmentSearchType.COMPLETED.getIndex().byteValue();

    private static final Byte CANCELED_SEARCH = FulfillmentSearchType.CANCELED.getIndex().byteValue();

    @Mock
    private OrdersRepository ordersRepository;

    @Mock
    private OrderitemsRepository orderitemsRepository;

    @Mock
    private PeopleRepository peoplerepository;

    @Mock
    private OrderstatusesRepository orderstatusesRepository;

    @Mock
    private OrdertypesRepository ordertypesRepository;

    @Mock
    private OrderitemactionsRepository orderitemactionsRepository;

    @Mock
    private OrderitemtypesRepository orderitemtypesRepository;

    @InjectMocks
    OrderManagementService oms = new OrderManagementServiceImpl();

    @Test
    public void testGetOrder() {

        Integer id = 1;
        Orders o = new Orders();
        o.setOrderId(id);

        Mockito.when(ordersRepository.findOne(id)).thenReturn(o);
        Orders oo = oms.getOrder(id);

        Integer testId = oo.getOrderId();

        Assert.assertEquals(testId, id);

    }

    @Test
    public void testAddOrderItem() {
        // TODO Need to decide strategy for mocking getAmountTotal
        /*
         * Byte statusId = new Integer(1).byteValue(); Orderstatuses status = new Orderstatuses(statusId);
         * Mockito.when(orderstatusesRepository.findOne(statusId)).thenReturn(status); Ordertypes ordertype = new
         * Ordertypes((byte) 1); Mockito.when(ordertypesRepository.findOne((byte) 1)).thenReturn(ordertype); Orders order = new
         * Orders(ordertypesRepository.findOne((byte) 1), orderstatusesRepository.findOne(statusId),
         * orderstatusesRepository.findOne(statusId)); order.setOrderId(100); Tickets ticket = new Tickets(new
         * BigDecimal(10.0),true); ticket.setDescription("Card"); Orderitemactions action = new Orderitemactions((byte) 1);
         * Mockito.when(orderitemactionsRepository.findOne((byte) 1)).thenReturn(action); Orderitemtypes orderitemtype = new
         * Orderitemtypes((byte) 1); Mockito.when(orderitemtypesRepository.findOne((byte) 1)).thenReturn(orderitemtype);
         * Orderitems item = new Orderitems(new OrderitemsId(100, statusId), action, orderitemtype, statusId, (short) 1,
         * orderstatusesRepository.findOne(statusId), order, (short) 1, new BigDecimal(10.0), new BigDecimal(10.0));
         * item.setTickets(ticket); Orders orderout = new Orders(ordertypesRepository.findOne((byte) 1),
         * orderstatusesRepository.findOne(statusId), orderstatusesRepository.findOne(statusId)); orderout.setOrderId(100);
         * orderout.getOrderitemses().add(item); Mockito.when(orderitemsRepository.save(item)).thenReturn(item);
         * Mockito.when(ordersRepository.save(order)).thenReturn(orderout); Orders result = oms.addOrderItem(order, item);
         * Assert.assertNotNull(result); Assert.assertEquals(result.getOrderitemses().get(0),
         * orderout.getOrderitemses().get(0));
         */
    }

    @Test
    public void testSaveOrder() {
        Integer id = 1;
        Orders o = new Orders();
        o.setOrderId(id);

        Mockito.when(ordersRepository.findOne(id)).thenReturn(o);
        Orders oo = oms.getOrder(id);

        Integer testId = oo.getOrderId();

        Assert.assertEquals(testId, id);
    }

    @Test
    public void testGetOrderByFilter() {
        Integer orderId = 1;
        List<Orders> results = null;

        Orders order = new Orders();
        order.setOrderId(1);

        List<Orders> orders = new ArrayList<>();
        orders.add(order);

        Mockito.when(ordersRepository.findPendingFulfillment(orderId, null, null, NEW_ACTION, WALLET_TYPE, INPROGRESS,
            FULFILLMENT)).thenReturn(orders);
        results = oms.getFulfillmentOrdersByFilter(orderId, null, null, PENDING_FULFILLMENT);
        Assert.assertEquals(results.get(0).getOrderId(), orderId);

        Mockito.when(ordersRepository.findPendingAutoload(orderId, null, null, ADD_VALUE, ADD_PRODUCT, INPROGRESS,
            FULFILLMENT)).thenReturn(orders);
        results = oms.getFulfillmentOrdersByFilter(orderId, null, null, PENDING_AUTOLOAD);
        Assert.assertEquals(results.get(0).getOrderId(), orderId);

        Mockito.when(ordersRepository.findOrdersByFilter(orderId, null, null, COMPLETED, COMPLETED, CANCELED,
            INPROGRESS, FULFILLMENT)).thenReturn(orders);
        results = oms.getFulfillmentOrdersByFilter(orderId, null, null, COMPLETED_SEARCH);
        Assert.assertEquals(results.get(0).getOrderId(), orderId);

        Mockito.when(ordersRepository.findOrdersByFilter(orderId, null, null, CANCELED, COMPLETED, CANCELED, INPROGRESS,
            FULFILLMENT)).thenReturn(orders);
        results = oms.getFulfillmentOrdersByFilter(orderId, null, null, CANCELED_SEARCH);
        Assert.assertEquals(results.get(0).getOrderId(), orderId);

        Mockito.when(ordersRepository.findOrdersByFilter(orderId, null, null, null, COMPLETED, CANCELED, INPROGRESS,
            FULFILLMENT)).thenReturn(orders);
        results = oms.getFulfillmentOrdersByFilter(orderId, null, null, ALL);
        Assert.assertEquals(results.get(0).getOrderId(), orderId);

    }

}