package com.genfare.cloud.model.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class SearchUtilTest {

    private List<List<Integer>> results;

    @Before
    public void setup() {
        results = new ArrayList<List<Integer>>();
        results.add(new ArrayList<Integer>(Arrays.asList(0, 1, 3, 5)));
        results.add(new ArrayList<Integer>(Arrays.asList(0, 2, 4, 6)));
    }

    @Test
    public void testOr() {
        List<Integer> expected = new ArrayList<Integer>(Arrays.asList(0, 1, 2, 3, 4, 5, 6));
        Assert.assertEquals(expected, SearchUtil.or(results));
    }

    @Test
    public void testAnd() {
        List<Integer> expected = new ArrayList<Integer>(Arrays.asList(0));
        Assert.assertEquals(expected, SearchUtil.and(results));
    }
}
