package com.genfare.cloud.model.security.jwt;

import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

public class JwtClaimsTest {

    private String json = "{\"exp\":2,\"iss\":\"TEST\",\"qsh\":\"TEST\",\"iat\":1,\"sub\":\"TESTS\"}";

    @Test
    public void convert() {
        Assert.assertEquals(new JSONObject(JwtClaims.fromJson(json).toJson()).toString(), new JSONObject(json).toString());
    }
}
