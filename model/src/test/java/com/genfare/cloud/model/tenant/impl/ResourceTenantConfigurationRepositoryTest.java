package com.genfare.cloud.model.tenant.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Collection;

import org.junit.Test;

import com.genfare.cloud.model.tenant.TenantConfiguration;

public class ResourceTenantConfigurationRepositoryTest {

    ResourceTenantConfigurationRepository cut;

    @Test
    public void testResourceTenantConfigurationRepository() {
        cut = new ResourceTenantConfigurationRepository();
    }

    @Test
    public void testGetByDomainName() {
        cut = new ResourceTenantConfigurationRepository();
        TenantConfiguration config = cut.getByDomainName("localhost");
        assertEquals("Test Tenant", config.getTenantName());
    }

    @Test
    public void testGetAll() {
        cut = new ResourceTenantConfigurationRepository();
        Collection<TenantConfiguration> configs = cut.getAll();
        assertTrue(configs.size() > 0);
    }

    public void testGetDefaultTenant() {
        cut = new ResourceTenantConfigurationRepository();
        TenantConfiguration config = cut.getByDomainName("Does Not Exist");
        assertEquals("Test Tenant", config.getTenantName());
        assertTrue(config.isDefaultTenant());
    }

}
