package com.genfare.cloud.model.security.jwt;

import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

public class JwtHeaderTest {

    private String json = "{\"alg\":\"TEST\",\"typ\":\"TEST\"}";

    @Test
    public void convert() {
        Assert.assertEquals(new JSONObject(JwtHeader.fromJson(json).toJson()).toString(), new JSONObject(json).toString());
    }

}
