package com.genfare.cloud.model.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

import javax.persistence.EntityNotFoundException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.genfare.cloud.model.contact.Address;
import com.genfare.cloud.model.contact.Email;
import com.genfare.cloud.model.contact.Phone;
import com.genfare.cloud.model.entity.Crossreferencefile;
import com.genfare.cloud.model.entity.CrossreferencefileId;
import com.genfare.cloud.model.entity.Logins;
import com.genfare.cloud.model.entity.People;
import com.genfare.cloud.model.entity.Walletidentifiers;
import com.genfare.cloud.model.entity.Wallets;
import com.genfare.cloud.model.entity.repository.CrossreferencefileRepository;
import com.genfare.cloud.model.entity.repository.LoginsRepository;
import com.genfare.cloud.model.entity.repository.WalletidentifiersRepository;
import com.genfare.cloud.model.entity.repository.WalletsRepository;
import com.genfare.cloud.model.exception.WalletHasPeopleAssignedException;
import com.genfare.cloud.model.security.User;
import com.genfare.cloud.model.security.User.UserBuilder;

@RunWith(MockitoJUnitRunner.class)
public class WalletManagmentServiceTest {

    @Mock
    private WalletidentifiersRepository walletidentifiersRepository;

    @Mock
    private WalletsRepository walletsRepository;

    @Mock
    private LoginsRepository loginsRepository;

    @Mock
    private CrossreferencefileRepository crossreferencefileRepository;

    @InjectMocks
    WalletManagementService wms = new WalletManagementServiceImpl();

    @Test
    public void testGetWallet() {

        String pid = "10101";
        Walletidentifiers wi = new Walletidentifiers();
        Wallets w = new Wallets();
        wi.setWallets(w);
        when(walletidentifiersRepository.findByPID(pid)).thenReturn(wi);
        Wallets mw = wms.getWallet(pid);

        assertEquals(w, mw);
    }

    @Test
    public void getWalletWithCvvNumberTest() {

        String cardNumber = "10101";
        String cvvNumber = "1111";

        CrossreferencefileId crossreferencefileId = new CrossreferencefileId();
        crossreferencefileId.setPrintedId("10101");
        Crossreferencefile crossreferencefile = new Crossreferencefile();
        crossreferencefile.setId(crossreferencefileId);
        crossreferencefile.setElectronicId(123456789L);
        crossreferencefile.setCvv("1111");
        when(crossreferencefileRepository.findByCvvAndPrintedId(cvvNumber, cardNumber)).thenReturn(crossreferencefile);

        Wallets w = new Wallets();
        when(walletsRepository.findByElectronicId("123456789")).thenReturn(w);
        Wallets mw = wms.getWallet(cvvNumber, cardNumber);

        assertEquals(w, mw);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetNoWallet() {

        when(walletidentifiersRepository.findByPID("1")).thenReturn(null);
        wms.getWallet("1");

    }

    @Test
    public void registerWalletWithCvvNumberTest()
        throws WalletHasPeopleAssignedException {

        Logins logins = new Logins();
        logins.setLoginId(1);
        logins.setUserLogin("username@example.com");
        People people = new People();
        logins.setPeople(people);
        when(loginsRepository.findByUserLogin("username@example.com")).thenReturn(logins);

        String cardNumber = "1000002506";
        String cvvNumber = "376";

        CrossreferencefileId crossreferencefileId = new CrossreferencefileId();
        crossreferencefileId.setPrintedId(cardNumber);
        Crossreferencefile crossreferencefile = new Crossreferencefile();
        crossreferencefile.setId(crossreferencefileId);
        crossreferencefile.setElectronicId(123456789L);
        crossreferencefile.setCvv(cvvNumber);
        when(crossreferencefileRepository.findByCvvAndPrintedId(cvvNumber, cardNumber)).thenReturn(crossreferencefile);

        Wallets w = new Wallets();
        when(walletsRepository.findByElectronicId("123456789")).thenReturn(w);
        when(walletsRepository.save(w)).thenReturn(w);

        User user = new UserBuilder(1, "username@example.com", "first", "last", new Email(), new Phone(), new Address()).build();

        Wallets registerWallet = wms.registerWallet(user, "376", "1000002506", "Test Card");

        assertEquals(registerWallet.getIsRegistered(), true);
        assertNotNull(registerWallet.getPeople());

    }

    @Test(expected = EntityNotFoundException.class)
    public void registerWalletWithCvvNumberNotFoundTest()
        throws WalletHasPeopleAssignedException {

        Logins logins = new Logins();
        logins.setLoginId(1);
        logins.setUserLogin("1");
        People people = new People();
        logins.setPeople(people);
        when(loginsRepository.findByUserLogin("1")).thenReturn(logins);

        String cardNumber = "10101";
        String cvvNumber = "1111";

        CrossreferencefileId crossreferencefileId = new CrossreferencefileId();
        crossreferencefileId.setPrintedId("10101");
        Crossreferencefile crossreferencefile = new Crossreferencefile();
        crossreferencefile.setId(crossreferencefileId);
        crossreferencefile.setElectronicId(123456789L);
        crossreferencefile.setCvv("1111");
        when(crossreferencefileRepository.findByCvvAndPrintedId(cvvNumber, cardNumber)).thenReturn(crossreferencefile);

        Wallets w = new Wallets();
        when(walletsRepository.findByElectronicId("123456789")).thenReturn(w);
        when(walletsRepository.save(w)).thenReturn(w);

        User user = new UserBuilder(1, "username@example.com", "first", "last", new Email(), new Phone(), new Address()).build();

        Wallets registerWallet = wms.registerWallet(user, "1111", "101", "Test Card");

        assertEquals(registerWallet.getIsRegistered(), true);
        assertNotNull(registerWallet.getPeople());
    }

    @Test(expected = WalletHasPeopleAssignedException.class)
    public void registerWalletWithCvvNumberHasPeopleAssignedTest()
        throws WalletHasPeopleAssignedException {

        Logins logins = new Logins();
        logins.setLoginId(1);
        logins.setUserLogin("1");
        People people = new People();
        logins.setPeople(people);
        when(loginsRepository.findByUserLogin("1")).thenReturn(logins);

        String cardNumber = "10101";
        String cvvNumber = "1111";

        CrossreferencefileId crossreferencefileId = new CrossreferencefileId();
        crossreferencefileId.setPrintedId("10101");
        Crossreferencefile crossreferencefile = new Crossreferencefile();
        crossreferencefile.setId(crossreferencefileId);
        crossreferencefile.setElectronicId(123456789L);
        crossreferencefile.setCvv("1111");
        when(crossreferencefileRepository.findByCvvAndPrintedId(cvvNumber, cardNumber)).thenReturn(crossreferencefile);

        Wallets w = new Wallets();
        w.setPeople(new People());
        when(walletsRepository.findByElectronicId("123456789")).thenReturn(w);
        when(walletsRepository.save(w)).thenReturn(w);

        User user = new UserBuilder(1, "username@example.com", "first", "last", new Email(), new Phone(), new Address()).build();

        Wallets registerWallet = wms.registerWallet(user, "1111", "10101", "Test Card");

        assertEquals(registerWallet.getIsRegistered(), true);
        assertNotNull(registerWallet.getPeople());
    }

}
