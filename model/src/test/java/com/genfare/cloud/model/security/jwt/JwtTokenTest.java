package com.genfare.cloud.model.security.jwt;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.genfare.cloud.model.security.jwt.algorithms.HS256Algorithm;
import com.genfare.cloud.model.security.jwt.interfaces.JwtCryptoAlgorithm;

public class JwtTokenTest {
    private final String issuer = "issuer";
    private final String request = "/rest/resource";
    private final String method = "GET";
    private final Long ttl = 180L;
    private final String sub = "User Content";
    private final String secret = "secret";
    private JwtToken token;
    private JwtCryptoAlgorithm algorithm;

    @Before
    public void setup() {
        algorithm = new HS256Algorithm();
        token = new JwtToken(issuer, request, method, ttl).sub(sub).algorithm(algorithm);
    }

    @Test
    public void token() {
        String t = token.token(secret);
        Assert.assertTrue(JwtToken.verify(t, secret));
        Assert.assertTrue(JwtToken.verifyQueryHash(t, request, method));
    }

    @Test
    public void parseHeader() {
        JwtHeader head = JwtToken.parseHeader(token.token(secret));
        Assert.assertEquals(head.getAlg(), new HS256Algorithm().getType());
        Assert.assertEquals(head.getTyp(), "JWT");
    }

    @Test
    public void parseClaims() {
        JwtClaims claim = JwtToken.parseClaims(token.token(secret));
        Assert.assertEquals(claim.getSub(), sub);
    }
}
