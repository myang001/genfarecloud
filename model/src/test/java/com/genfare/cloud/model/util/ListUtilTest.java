package com.genfare.cloud.model.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

public class ListUtilTest {

    private List<Integer> a = new ArrayList<Integer>(Arrays.asList(0, 1, 3, 5));
    private List<Integer> b = new ArrayList<Integer>(Arrays.asList(0, 2, 4, 6));

    @Test
    public void testUnion() {
        List<Integer> expected = new ArrayList<Integer>(Arrays.asList(0, 1, 2, 3, 4, 5, 6));
        List<Integer> result = ListUtil.union(a, b);
        Assert.assertEquals(expected, result);
    }

    @Test
    public void testIntersection() {
        List<Integer> expected = new ArrayList<Integer>(Arrays.asList(0));
        List<Integer> result = ListUtil.intersection(a, b);
        Assert.assertEquals(expected, result);
    }
}
