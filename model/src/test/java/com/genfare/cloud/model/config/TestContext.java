package com.genfare.cloud.model.config;

import static org.mockito.Mockito.mock;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.genfare.cloud.model.entity.repository.AddresstypesRepository;
import com.genfare.cloud.model.entity.repository.AuthenticationRepository;
import com.genfare.cloud.model.entity.repository.AuthenticationtypesRepository;
import com.genfare.cloud.model.entity.repository.LanguagesRepository;
import com.genfare.cloud.model.entity.repository.LinktypesRepository;
import com.genfare.cloud.model.entity.repository.LoginsRepository;
import com.genfare.cloud.model.entity.repository.OrderitemsRepository;
import com.genfare.cloud.model.entity.repository.OrdersRepository;
import com.genfare.cloud.model.entity.repository.PeopleRepository;
import com.genfare.cloud.model.entity.repository.PeopleaddressesRepository;
import com.genfare.cloud.model.entity.repository.PeoplelinksRepository;
import com.genfare.cloud.model.entity.repository.WalletidentifiersRepository;
import com.genfare.cloud.model.entity.repository.WalletsRepository;

@Configuration
@ComponentScan(basePackages = { "com.genfare.cloud.model.service", "com.genfare.cloud.model.entity.repository" })
public class TestContext {

    @Bean
    public PeopleRepository createMockPersonRepostory() {
        return null;
    };

    @Bean
    public LoginsRepository createMockLoginsRepostory() {
        return mock(LoginsRepository.class);
    };

    @Bean
    public PeoplelinksRepository createMockPeoplelinksRepostory() {
        return null;
    };

    @Bean
    public LinktypesRepository createLinktypesRepository() {
        return null;
    }

    @Bean
    public AuthenticationRepository createAuthenticationRepository() {
        return null;
    }

    @Bean
    public AuthenticationtypesRepository createAuthenticationtypesRepository() {
        return null;
    }

    @Bean
    public PeopleaddressesRepository createPeopleaddressesRepository() {
        return null;
    }

    @Bean
    public AddresstypesRepository createAddresstypesRepository() {
        return null;
    }

    @Bean
    public LanguagesRepository languagesRepository() {
        return null;
    }

    @Bean
    public WalletidentifiersRepository createWalletidentifiersRepository() {
        return mock(WalletidentifiersRepository.class);
    }

    @Bean
    public WalletsRepository createWalletsRepository() {
        return mock(WalletsRepository.class);
    }

    @Bean
    public OrdersRepository createOrdersRepository() {
        return mock(OrdersRepository.class);
    }

    @Bean
    public OrderitemsRepository createOrderitemsRepository() {
        return mock(OrderitemsRepository.class);
    }

}