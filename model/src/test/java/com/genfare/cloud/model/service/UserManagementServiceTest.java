package com.genfare.cloud.model.service;

import java.security.Principal;
import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.genfare.cloud.model.contact.Address;
import com.genfare.cloud.model.contact.Email;
import com.genfare.cloud.model.contact.Phone;
import com.genfare.cloud.model.entity.Addresstypes;
import com.genfare.cloud.model.entity.Languages;
import com.genfare.cloud.model.entity.Linktypes;
import com.genfare.cloud.model.entity.People;
import com.genfare.cloud.model.entity.PeopleStatus;
import com.genfare.cloud.model.entity.Peopleaddresses;
import com.genfare.cloud.model.entity.PeopleaddressesId;
import com.genfare.cloud.model.entity.Peoplelinks;
import com.genfare.cloud.model.entity.PeoplelinksId;
import com.genfare.cloud.model.entity.repository.JobTitleRepository;
import com.genfare.cloud.model.entity.repository.PeopleRepository;
import com.genfare.cloud.model.security.User;
import com.genfare.cloud.model.security.User.UserBuilder;
import com.genfare.cloud.model.types.AddresstypesType;
import com.genfare.cloud.model.types.LanguagesType;

@RunWith(MockitoJUnitRunner.class)
public class UserManagementServiceTest {

    @Mock
    PeopleRepository peopleRepository;

    @Mock
    JobTitleRepository jobTitleRepository;

    @InjectMocks
    UserManagementService userManager = new UserManagementServiceImpl();

    private People p = new People();
    private String userLogin = "test@example.com";
    private String userLoginBad = "bad@example.com";
    private Email email = new Email(userLogin, "Personal Email");
    private Phone phone = new Phone("1234567890", "Home Phone");
    private Address address = new Address("Shipping Address", "123 Test St.", "", "City", "ST", "12345", "United States");
    private Languages l = null;

    @Before
    public void setup()
        throws Exception {

        l = new Languages("English", "en _US", (byte) 1, null, null);
        l.setLanguageId((byte) 1);

        PeopleStatus ps = new PeopleStatus("Active");
        ps.setStatus((byte) 1);

        p.setPersonId(1);
        p.setNameFirst("Test");
        p.setNameLast("User");
        p.setLanguages(l);
        p.setStatus(ps);

        Set<Peoplelinks> links = p.getPeoplelinkses();
        Linktypes emailType = new Linktypes("Personal Email", "Email");
        emailType.setLinkTypeId(5);
        links.add(new Peoplelinks(new PeoplelinksId(p.getPersonId(), emailType.getLinkTypeId()), emailType, p, email.getAddress(), false, true));

        Linktypes phoneType = new Linktypes("Home Phone", "Phone");
        phoneType.setLinkTypeId(1);
        links.add(new Peoplelinks(new PeoplelinksId(p.getPersonId(), phoneType.getLinkTypeId()), phoneType, p, email.getAddress(), false, false));

        Set<Peopleaddresses> addresses = p.getPeopleaddresseses();
        Addresstypes addressType = new Addresstypes("Shipping Address");
        addressType.setAddressTypeId(2);
        addresses.add(new Peopleaddresses(new PeopleaddressesId(p.getPersonId(), addressType.getAddressTypeId()), addressType, p));
    }

    @Test
    public void testGetUser() {

        Principal principal = new Principal() {
            @Override
            public String getName() {
                return "test@example.com";
            }
        };

        // By ID
        Mockito.when(peopleRepository.findOne(1)).thenReturn(p);
        Mockito.when(peopleRepository.findOne(2)).thenReturn(null);
        Assert.assertNotNull(userManager.getUser(1));
        Assert.assertNull(userManager.getUser(2));

        // By Username
        Mockito.when(peopleRepository.findByLogin(userLogin)).thenReturn(p);
        Mockito.when(peopleRepository.findByLogin(userLoginBad)).thenReturn(null);
        Assert.assertNotNull(userManager.getUser(userLogin));

        // By Principal
        Mockito.when(peopleRepository.findByLogin(userLogin)).thenReturn(p);
        Assert.assertNotNull(userManager.getUser(principal));
        Principal principalbad = null;
        Assert.assertNull(userManager.getUser(principalbad));

    }

}
