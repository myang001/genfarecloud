package com.genfare.cloud.model.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.genfare.cloud.model.entity.Tickets;
import com.genfare.cloud.model.entity.repository.TicketsRepository;

@RunWith(MockitoJUnitRunner.class)
public class TicketsEligibilityServiceTest {

    @Mock
    private TicketsRepository ticketsRepository;

    @InjectMocks
    private TicketsEligibilityService ticketsEligibilityService = new TicketsEligibilityServiceImpl();

    @Before
    public void setup() {
        List<Tickets> ticketsList = new ArrayList<>();
        Tickets tickets = new Tickets();
        ticketsList.add(tickets);
        when(ticketsRepository.findAll()).thenReturn(ticketsList);
    }

    @Test
    public void test() {
        Iterable<Tickets> eligibleTickets = ticketsEligibilityService.getEligibleTickets(null);
        Iterator<Tickets> iterator = eligibleTickets.iterator();
        assertEquals(true, iterator.hasNext());
        iterator.next();
        assertEquals(false, iterator.hasNext());
    }

}
