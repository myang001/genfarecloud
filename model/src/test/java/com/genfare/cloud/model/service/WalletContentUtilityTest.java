package com.genfare.cloud.model.service;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.genfare.cloud.model.entity.Tickets;
import com.genfare.cloud.model.entity.Tickettypes;
import com.genfare.cloud.model.entity.Walletcontents;
import com.genfare.cloud.model.entity.repository.SubscriptionsRepository;
import com.genfare.cloud.model.types.TickettypesType;

@RunWith(MockitoJUnitRunner.class)
public class WalletContentUtilityTest {

    @InjectMocks
    WalletContentUtilityService wcus = new WalletContentUtilityServiceImpl();

    @Mock
    private SubscriptionsRepository subscriptionsRepository;

    private Walletcontents walletcontents = new Walletcontents();
    private SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");

    @Test
    public void testGetFloatingPerPassBalance() {
        wcus = new WalletContentUtilityServiceImpl();
        walletcontents = new Walletcontents();
        try {
            walletcontents.setDateStart(df.parse("2014-09-01"));// 9-1-2014
            walletcontents.setDateExpires(df.parse("2014-10-01"));// 10-1-2014
        } catch (ParseException e) {

        }
        walletcontents.setTickets(new Tickets());
        walletcontents.getTickets().setTickettypes(new Tickettypes());
        walletcontents.getTickets().getTickettypes().setTicketTypeId(Byte.valueOf("3"));

        assertEquals("30 days", wcus.getBalance(walletcontents));
    }

    @Test
    public void testGetFixedPerPassBalance() {
        wcus = new WalletContentUtilityServiceImpl();
        walletcontents = new Walletcontents();
        try {
            walletcontents.setDateStart(df.parse("2014-09-01"));// 9-1-2014
            walletcontents.setDateExpires(df.parse("2014-10-01"));// 10-1-2014
        } catch (ParseException e) {

        }
        walletcontents.setTickets(new Tickets());
        walletcontents.getTickets().setTickettypes(new Tickettypes());
        walletcontents.getTickets().getTickettypes().setTicketTypeId(Byte.valueOf("6"));
        assertEquals("October", wcus.getBalance(walletcontents));
    }

    @Test
    public void testGetStoredRideBalance() {
        wcus = new WalletContentUtilityServiceImpl();
        walletcontents = new Walletcontents();
        walletcontents.setTickets(new Tickets());
        walletcontents.getTickets().setTickettypes(new Tickettypes());
        walletcontents.getTickets().getTickettypes().setTicketTypeId(Byte.valueOf("2"));
        walletcontents.setValueRemaining(BigDecimal.valueOf(7));
        assertEquals("7 rides", wcus.getBalance(walletcontents));
    }

    @Test
    public void testGetStoredValueBalance() {
        wcus = new WalletContentUtilityServiceImpl();
        walletcontents = new Walletcontents();
        walletcontents.setTickets(new Tickets());
        walletcontents.getTickets().setTickettypes(new Tickettypes());
        walletcontents.getTickets().getTickettypes().setTicketTypeId(Byte.valueOf("1"));
        walletcontents.setValueRemaining(BigDecimal.valueOf(10));
        assertEquals("$10", wcus.getBalance(walletcontents));
    }
}
