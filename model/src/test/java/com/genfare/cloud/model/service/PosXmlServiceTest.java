package com.genfare.cloud.model.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.genfare.cloud.model.entity.Activities;
import com.genfare.cloud.model.entity.Activityattributes;
import com.genfare.cloud.model.entity.ActivityattributesId;
import com.genfare.cloud.model.entity.Activitytypes;
import com.genfare.cloud.model.entity.Authentication;
import com.genfare.cloud.model.entity.Authenticationtypes;
import com.genfare.cloud.model.entity.Components;
import com.genfare.cloud.model.entity.Configs;
import com.genfare.cloud.model.entity.Equipment;
import com.genfare.cloud.model.entity.Equipmenttypes;
import com.genfare.cloud.model.entity.Farecodes;
import com.genfare.cloud.model.entity.Groups;
import com.genfare.cloud.model.entity.Identifiertypes;
import com.genfare.cloud.model.entity.Logins;
import com.genfare.cloud.model.entity.Modules;
import com.genfare.cloud.model.entity.Offerings;
import com.genfare.cloud.model.entity.Operators;
import com.genfare.cloud.model.entity.Organizationaddresses;
import com.genfare.cloud.model.entity.Organizations;
import com.genfare.cloud.model.entity.Pendingactivity;
import com.genfare.cloud.model.entity.People;
import com.genfare.cloud.model.entity.Privileges;
import com.genfare.cloud.model.entity.Taxrates;
import com.genfare.cloud.model.entity.Tickets;
import com.genfare.cloud.model.entity.Tickettypes;
import com.genfare.cloud.model.entity.Walletidentifiers;
import com.genfare.cloud.model.entity.Wallets;
import com.genfare.cloud.model.entity.Wallettypes;
import com.genfare.cloud.model.entity.Zones;
import com.genfare.cloud.model.tenant.impl.AmazonConfigurationType;
import com.genfare.cloud.model.tenant.impl.TenantConfigurationImpl;
import com.genfare.cloud.model.util.XMLUtil;

@RunWith(MockitoJUnitRunner.class)
public class PosXmlServiceTest {

    private final PosXmlService posXml = new PosXmlServiceImpl();

    @Test
    public void testFareTableXML() {
        List<Wallettypes> walletTypes = new ArrayList<Wallettypes>();
        List<Zones> zones = new ArrayList<Zones>();
        List<Taxrates> taxRates = new ArrayList<Taxrates>();
        List<Tickettypes> ticketTypes = new ArrayList<Tickettypes>();
        List<Offerings> offerings = new ArrayList<Offerings>();
        List<Farecodes> farecodes = new ArrayList<Farecodes>();

        Wallettypes wt = new Wallettypes("DesFire Smart Card", (byte) 10, true);
        wt.setWalletTypeId((byte) 3);
        walletTypes.add(wt);

        Zones z = new Zones((byte) 1, "Zone 1", "Testing");
        zones.add(z);

        Taxrates tr = new Taxrates(new BigDecimal(0.0), new BigDecimal(0.0),
            new BigDecimal(0.0), new BigDecimal(0.0), new BigDecimal(0.0));
        tr.setTaxRateId((short) 1);
        taxRates.add(tr);

        Tickettypes tt = new Tickettypes("Stored Fixed Value");
        tt.setTicketTypeId((byte) 1);
        ticketTypes.add(tt);

        Offerings o = new Offerings(new BigDecimal(3.00), true, false, false,
            (short) 500);
        o.setOfferingId((byte) 1);
        o.setDescription("Fare Card");
        o.setUnitCost(new BigDecimal(0.0));
        o.setWallettypes(wt);
        o.setStatus((byte) 1);
        o.setTaxrates(tr);
        o.setDateEffective(new Date());
        o.setDateExpires(new Date(o.getDateEffective().getTime() + 30 * 24 * 60
            * 60 * 1000));
        offerings.add(o);

        Tickets t = new Tickets();
        t.setTicketId(1);
        t.setDescription("Base Fare");
        t.setDescriptionLong("");
        t.setPrice(BigDecimal.valueOf(1.50));
        t.setValue(BigDecimal.valueOf(1.50));
        t.setDateEffective(new Date());
        t.setDateExpires(new Date(2016 - 12 - 31));
        t.setZones(z);
        t.setIsSubscribable(false);
        t.setReplenishValue(BigDecimal.ZERO);
        t.setReplenishThreshold(BigDecimal.ZERO);
        t.setTickettypes(tt);
        t.setFarecodes(new HashSet<Farecodes>());

        o.setTickets(t);

        Farecodes fc1 = new Farecodes();
        fc1.setFarecodeId((byte) 1);
        fc1.setDescription("Full Fare");
        t.getFarecodes().add(fc1);

        Farecodes fc2 = new Farecodes();
        fc2.setFarecodeId((byte) 2);
        fc2.setDescription("Youth");
        t.getFarecodes().add(fc2);

        String xml = posXml.generateFareTableXml(walletTypes, zones, taxRates,
            ticketTypes, offerings);
        Assert.assertNotNull(xml);
        System.out.println(XMLUtil.prettyFormat(xml));

    }

    @Test
    public void testPersonXml() {

        Groups g = new Groups("ROLE_APOS_ENGI", "Apos Engineer",
            "Allows access to APOS Equipment");
        g.setGroupId(20);
        Set<Groups> gs = new HashSet<Groups>();
        gs.add(g);
        // Modules m = new Modules();

        People p = new People();
        p.setPersonId(216);
        p.setGroups(gs);

        Set<Logins> ls = new HashSet<Logins>();
        Logins l = new Logins(1, p, "someemail@domain.com");
        ls.add(l);
        p.setLoginses(ls);

        Authenticationtypes at = new Authenticationtypes();
        at.setAuthenticationTypeId(6);
        at.setDescription("Employee Password");

        Set<Authentication> as = new HashSet<Authentication>();
        Authentication a = new Authentication();
        a.setAuthenticationtypes(at);
        a.setResponse("$2a$10$zlA213tFDRqOH0ffTtmnoe6Lp2qSdOUZxVzXbUSpygDjrvU4pxDSK");
        a.setPeople(p);
        as.add(a);
        p.setAuthentications(as);

        Modules m = new Modules();
        m.setName("APOS Equipment Engineer");
        m.setDescription("APOS Equipment Engineer Module");
        m.setModuleId((byte) 4);

        Components c = new Components();
        c.setComponentId((short) 5);
        c.setModules(m);
        c.setName("APOS Equipment Management");
        c.setDescription("Allows access to APOS Equipment");

        Set<Privileges> privs = new HashSet<Privileges>();
        Privileges priv = new Privileges();
        priv.setName("PERM_ENGINEER_APOS_EQUIP_ENGI");
        priv.setPrivilegeId((short) 13);
        priv.setComponents(c);
        privs.add(priv);
        g.setPrivileges(privs);

        Operators o = new Operators();
        o.setOperatorId(216);
        o.setPeople(p);

        String xml = posXml.generatePersonXml(o);
        Assert.assertNotNull(xml);
        System.out.println(XMLUtil.prettyFormat(xml));
    }

    @Test
    public void testActivityXml() {

        Equipment e = new Equipment();
        e.setEquipmentId(7);

        Operators o = new Operators();
        o.setOperatorId(216);

        Set<Activityattributes> aas = new HashSet<Activityattributes>();
        Activityattributes aa = new Activityattributes();
        aa.setAttributeValue("Test Attribute");
        aa.setId(new ActivityattributesId(2, (byte) 1));
        aas.add(aa);

        Activities a = new Activities();
        a.setEquipment(e);
        a.setOperators(o);
        a.setDateActivity(new Date());
        a.setDescription("Activity_2");
        a.setActivityattributeses(aas);

        String xml = posXml.generateActivityXml(a);
        Assert.assertNotNull(xml);
        System.out.println(XMLUtil.prettyFormat(xml));

    }

    @Test
    public void testPendingActivityXml() {

        Tickettypes tt = new Tickettypes("Stored Fixed Value");
        tt.setTicketTypeId((byte) 1);

        Zones z = new Zones((byte) 1, "Zone 1", "Testing");

        Tickets t = new Tickets();
        t.setTicketId(1);
        t.setDescription("");
        t.setDescriptionLong("");
        t.setPrice(BigDecimal.valueOf(0));
        t.setValue(BigDecimal.valueOf(0));
        t.setDateEffective(new Date());
        t.setDateExpires(new Date());
        t.setZones(z);
        t.setIsSubscribable(false);
        t.setReplenishValue(BigDecimal.ZERO);
        t.setReplenishThreshold(BigDecimal.ZERO);
        t.setTickettypes(tt);

        Identifiertypes it = new Identifiertypes();
        it.setDescription("Printed ID");
        it.setIdentifierId((byte) 2);

        Set<Walletidentifiers> wis = new HashSet<Walletidentifiers>();
        Walletidentifiers wi = new Walletidentifiers();
        wi.setIdentifiertypes(it);
        wis.add(wi);

        Wallets w = new Wallets();
        w.setWalletId((long) 316);
        w.setWalletidentifierses(wis);

        Activitytypes at = new Activitytypes();
        at.setDescription("Add Value");
        at.setActivityTypeId((byte) 3);

        Pendingactivity pa = new Pendingactivity();
        pa.setPendingId((long) 2);
        pa.setWallets(w);
        pa.setSlot((byte) 1);
        pa.setTickets(t);
        pa.setValueActivity(BigDecimal.valueOf(0));
        pa.setLoadSequence((byte) 1);
        pa.setActivitytypes(at);
        pa.setDateEffective(new Date());
        pa.setDateExpires(new Date());
        pa.setActivitytypes(at);

        String xml = posXml.generatePendingActivityXml(pa);
        Assert.assertNotNull(xml);
        System.out.println(XMLUtil.prettyFormat(xml));

    }

    @Test
    public void testTerminalConfig() {

        Set<Organizationaddresses> oas = new HashSet<Organizationaddresses>();
        Organizationaddresses oa = new Organizationaddresses();
        oa.setAddressLine1("751 Pratt Blvd");
        oa.setAddressLine2("");
        oa.setCity("Elk Grove Vlg");
        oa.setCity("IL");
        oa.setPostalCode("60007");
        oas.add(oa);

        Organizations o = new Organizations();
        o.setOrganizationaddresseses(oas);
        o.setOrganizationId(12);
        o.setName("SPX-Genfare");
        o.setOrganizationaddresseses(oas);
        oa.setOrganizations(o);

        Equipmenttypes et = new Equipmenttypes();
        et.setDescription("Apos Box Type");
        et.setEquipmentTypeId((short) 4);

        Set<Configs> configs = new HashSet<Configs>();
        Configs numberC = new Configs();
        numberC.setConfigKey("MaxUnsyncedTransactionNumber");
        numberC.setConfigValue("1");
        configs.add(numberC);
        Configs valueC = new Configs();
        valueC.setConfigKey("MaxUnsyncedTransactionValue");
        valueC.setConfigValue("151");
        configs.add(valueC);

        Equipment eq = new Equipment();
        eq.setTouchedAt(new Date());
        eq.setEquipmentId(6);
        eq.setEquipmenttypes(et);
        eq.setOrganizations(o);
        eq.setConfigss(configs);

        AmazonConfigurationType act = new AmazonConfigurationType();
        act.setAwsAccessKey("AKIAIZTZFMPFMMTFSE6Q");
        act.setAwsSecretKey("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");
        act.setAwsSqsPrefix("intg");
        act.setAwsSnsPrefix("intg");

        TenantConfigurationImpl tci = new TenantConfigurationImpl();
        tci.setAmazonConfiguration(act);
        tci.setCreditServiceName("FirstData");
        tci.setCreditServicePostURL("https://demo.globalgatewaye4.firstdata.com/pay");
        tci.setCreditServiceCartPage("cart/cartFD");
        tci.setCreditServiceReturnURL("");
        tci.setCreditServiceCancelURL("");
        tci.setCreditServiceMerchantCode("");
        tci.setCreditServicePassword("");
        tci.setCreditServiceSettleMerchantCode("");

        tci.setCreditServiceXLogin("HCO-SPX-D-282");
        tci.setCreditServiceXShowForm("PAYMENT_FORM");
        tci.setCreditServiceXCurrencyCode("USD");
        tci.setCreditServiceXTestRequest("TRUE");
        tci.setCreditServiceXRelayResponse("TRUE");
        tci.setCreditServiceAgencyKey("hKIBbgUXZN2ImZmYzUOY");
        tci.setCreditServiceRelayResponseKey("Cq75hzAAbUtvcgEHBDlu");

        String xml = posXml.generateTerminalConfigAPI(tci, eq);
        Assert.assertNotNull(xml);
        System.out.println(XMLUtil.prettyFormat(xml));

    }
}