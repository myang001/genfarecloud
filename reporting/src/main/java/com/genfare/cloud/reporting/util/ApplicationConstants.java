package com.genfare.cloud.reporting.util;

public class ApplicationConstants {

    public static final String JASPER_LOCAL_HOST = "http://localhost:8080/jasperserver-pro/rest_v2";

    public static final String JASPER_SUPERUSER_NAME = "superuser";

    public static final String JASPER_SUPERUSER_PWD = "pass1word";

    // public static final String JASPER_SUPERUSER_NAME = "SUPERUSER";
    // public static final String JASPER_SUPERUSER_NAME = "SUPERUSER";

    public static final String JASPER_REPORT_LOCATION_CDTA_ALBANY = "/organizations/NY_Albnay_CDTA/Reports";

    public static final String RPT_CUST_ORDER_HISTORY = "/eFare-OrderHistory_Report";

    public static final String CONNECTION_FAILED = "CONNECTION_FAILED";

    public static final String HTML = ".html";

    public static final String PDF = ".pdf";

    // Inside Message sub-JSON/XML string

    // public static String PARSER_TYPE_JSON = "\" : \"";
    // public static String PARSER_TYPE_XML = ">";
    // public static final String SUCCESS_NOTIFICATION = "00"; // notification success code is
    // public static final String PROCESS_QUEUE_COMPLETE = "PROCESS_QUEUE_PENDING"; // success status for process queue
    // public static final String PROCESS_QUEUE_PENDING = "PROCESS_QUEUE_COMPLETE";

}
