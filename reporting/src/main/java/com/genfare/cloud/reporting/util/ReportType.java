package com.genfare.cloud.reporting.util;

public enum ReportType {
    CSV,
    DOCX,
    EXCEL,
    HTML,
    ODS,
    ODT,
    PDF,
    RTF,
    XLSX
}
