

class fuse::service inherits fuse {

    case $fuse::install {

        source: {

            # copy the libs
            file { 'fuse.wrapper.libs':
                path    => "${fuse::real_fuse_dir}/lib",
                source  => "puppet:///modules/fuse/lib",
                recurse => true,
                require => Class['fuse::install'],
            }

            # copy the wrapper
            file { 'fuse.wrapper':
                ensure  => $fuse::manage_file,
                path    => "${fuse::real_fuse_dir}/bin/fuse-wrapper",
                mode    => '0755',
                owner   => $fuse::process_user,
                group   => $fuse::process_user,
                require => File['fuse.wrapper.libs'],
                source  => "puppet:///modules/fuse/fuse-wrapper",
            }

            # populate the wrapper conf
            file { 'fuse.wrapper.conf':
                ensure  => $fuse::manage_file,
                path    => "${fuse::real_fuse_dir}/etc/fuse-wrapper.conf",
                mode    => '0664',
                owner   => $fuse::process_user,
                group   => $fuse::process_user,
                require => File['fuse.wrapper'],
                content => template('fuse/fuse-wrapper.conf.erb'),
            }

            #Install service
            service { 'fuse':
                ensure     => $fuse::manage_service_ensure,
                name       => $fuse::service,
                enable     => $fuse::manage_service_enable,
                hasstatus  => $fuse::service_status,
                pattern    => $fuse::process,
                require    => File['fuse-service_link'],
            }
              
            file { 'fuse.service':
                ensure  => $jboss::manage_file,
                path    => "${fuse::real_fuse_dir}/bin/fuse-service",
                mode    => '0755',
                owner   => $fuse::process_user,
                group   => $fuse::process_user,
                require => File['fuse.wrapper.conf'],
                notify  => $fuse::manage_service_autorestart,
                content => template('fuse/fuse-service.erb'),
                audit   => $fuse::manage_audit,
            }

            file { 'fuse-service_link':
                ensure  => "${fuse::real_fuse_dir}/bin/fuse-service",
                path    => "/etc/init.d/${service}",
                require => File['fuse.service'],
            }

        }
    }
}
