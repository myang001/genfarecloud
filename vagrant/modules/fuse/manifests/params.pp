

class fuse::params {

    $version = '6'
    $install = 'source'
    $install_source = ''
    $install_destination = ''
    $install_dirname = 'fuse'
    $install_precommand = ''
    $install_postcommand = ''
    $created_dirname = 'jboss-fuse-6.1.0.redhat-379'
    $process_user = ''
    $admin_user = ''
    $admin_password = 'password'
    $console_port = '8181'
    $console_enable = true
    $bindaddr = '127.0.0.1'
    $bindaddr_admin_console = '127.0.0.1'
    $my_class = ''
    $puppi = false
    $puppi_helper = 'phpapp'
    $absent = false
    $disable = false
    $disableboot = false
    $audit_only = false


    $package = $::operatingsystem ? {
        default => 'fuse',
    }

    $service = $::operatingsystem ? {
        default => 'fuse',
    }

    $service_status = $::operatingsystem ? {
        default => true,
    }

    $process = $::operatingsystem ? {
        default => 'java',
    }

    $process_args = $::operatingsystem ? {
        default => 'fuse',
    }
    
}
