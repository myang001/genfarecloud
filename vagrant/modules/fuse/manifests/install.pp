

class fuse::install inherits fuse {

    case $fuse::install {

        source: {

            puppi::netinstall { 'netinstall_fuse' :
                url                  => $fuse::real_install_source,
                destination_dir      => $fuse::real_install_destination,
                preextract_command   => $fuse::install_precommand,
                postextract_command  => $fuse::real_install_postcommand,
                extracted_dir        => $fuse::real_created_dirname,
                owner                => $fuse::process_user,
                group                => $fuse::process_user,
                require              => User[$fuse::process_user],
            }

            file { 'fuse_link':
                ensure  => "${fuse::real_install_destination}/${fuse::real_created_dirname}",
                path    => "${fuse::real_install_destination}/${fuse::install_dirname}",
            }

        }

        default: { }
    }
}
