

class fuse::config inherits fuse {

    case $fuse::install {

        source, configure: {

            if $fuse::admin_user != '' {
                #Add admin user to properties file
                file { 'fuse.config.admin.user':
                    path     => "${fuse::real_fuse_dir}/etc/users.properties",
                    mode     => '0644',
                    owner    => $fuse::process_user,
                    group    => $fuse::process_user,
                    require  => Class['fuse::install'],
                    content  => template('fuse/users.properties.erb'),
                }
            }

            #Configure console
            file { 'fuse.config.admin.console':
                path     => "${fuse::real_fuse_dir}/etc/org.ops4j.pax.web.cfg",
                mode     => '0644',
                owner    => $fuse::process_user,
                group    => $fuse::process_user,
                require  => Class['fuse::install'],
                content  => template('fuse/org.ops4j.pax.web.cfg.erb'),
            }
        }
    }
}
