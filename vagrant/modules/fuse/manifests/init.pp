


class fuse (

    $version                     = params_lookup( 'version' ), 
    $install                     = params_lookup( 'install' ), 
    $install_source              = params_lookup( 'install_source' ), 
    $install_destination         = params_lookup( 'install_destination' ), 
    $install_precommand          = params_lookup( 'install_precommand' ),
    $install_postcommand         = params_lookup( 'install_postcommand' ),
    $install_dirname             = params_lookup( 'install_dirname' ),
    $created_dirname             = params_lookup( 'created_dirname' ),
    $admin_user                  = params_lookup( 'admin_user' ),
    $admin_password              = params_lookup( 'admin_password' ),
    $console_port                = params_lookup( 'console_port' ),
    $console_enable              = params_lookup( 'console_enable' ),
    $puppi                       = params_lookup( 'puppi','global' ),
    $puppi_helper                = params_lookup( 'puppi_helper','global' ),
    $package                     = params_lookup( 'package' ),
    $service                     = params_lookup( 'service' ),
    $service_status              = params_lookup( 'service_status' ),
    $process                     = params_lookup( 'process' ),
    $process_args                = params_lookup( 'process_args' ),
    $process_user                = params_lookup( 'process_user' ),
    $disable                     = params_lookup( 'disable' ),
    $disableboot                 = params_lookup( 'disableboot' ),
    $my_class                    = params_lookup( 'my_class' ),
    $absent                      = params_lookup( 'absent' ),
    $audit_only                  = params_lookup( 'audit_only' ),
    $service_autorestart         = params_lookup( 'service_autorestart', 'global')


) inherits fuse::params {

    $bool_absent=any2bool($absent)
    $bool_disable=any2bool($disable)
    $bool_disableboot=any2bool($disableboot)
    $bool_puppi=any2bool($puppi)
    $bool_audit_only=any2bool($audit_only)
    $bool_service_autorestart=any2bool($service_autorestart)

    $real_process_user = $process_user ? {
        ''      => 'fuse',
        default => $process_user,
    }
    
    $real_created_dirname = $fuse::created_dirname ? {
        ''      => 'jboss-fuse-6.1.0.redhat-379',
        default => $fuse::created_dirname,
    }

    $real_install_source = $fuse::install_source

    $real_install_destination = $fuse::install_destination ? {
        ''      => "/opt",
        default => $fuse::install_destination,
    }


    $real_install_postcommand = $fuse::install_postcommand ? {
        ''      => "chown -R ${fuse::real_process_user} ${fuse::real_install_destination}/${fuse::real_created_dirname}",
        default => $fuse::install_postcommand,
    }

    $real_fuse_dir = "${fuse::real_install_destination}/${fuse::real_created_dirname}"

    $manage_file = $fuse::bool_absent ? {
        true    => 'absent',
        default => 'present',
    }

    $manage_service_enable = $fuse::bool_disableboot ? {
        true    => false,
        default => $fuse::bool_disable ? {
            true    => false,
            default => $fuse::bool_absent ? {
                true    => false,
                false   => true,
            },
        },
    }

    $manage_service_ensure = $fuse::bool_disable ? {
        true    => 'stopped',
        default => $fuse::bool_absent ? {
            true    => 'stopped',
            default => 'running',
        },
    }

    $manage_service_autorestart = $fuse::bool_service_autorestart ? {
        true    => $disable ? {
            ''      => undef,
            default => Service[fuse],
        },
        false   => undef,
    }

    $manage_audit = $fuse::bool_audit_only ? {
        true    => 'all',
        false   => undef,
    }


    require fuse::install
    require fuse::config

    if $disable != '' {
        require fuse::service
    }

    if $fuse::myclass {
        include $fuse::my_class
    }

    if $fuse::bool_puppi == true {
        $classvars = get_class_args()
        puppi::ze { 'fuse' :
            ensure        => $fuse::manage_file,
            variables     => $classvars,
            helper        => $fuse::puppi_helper,
        }
    }
        
}
