include rvm

rvm_system_ruby {'ruby-2.1.2':
    ensure      => 'present',
    default_use => true;
}

rvm_gem {'puppet-librarian':
    name         => 'librarian-puppet',
    ruby_version => 'ruby-2.1.2',
    ensure       => latest,
    require      => Rvm_system_ruby['ruby-2.1.2'];
}

package { "git": }
package { "augeas": }
package { "multitail": }

exec { 'install development tools':
  unless  => '/usr/bin/yum grouplist "Development tools" | /bin/grep "^Installed Groups"',
  command => '/usr/bin/yum -y groupinstall "Development tools"',
}

file { "/etc/motd":
  content => "Welcome to the GenfareCloud Development Environment, RIP Egon!
  Project source code is mounted in /vagrant"
}

class { '::epel': }
-> package { "python-pip": }
-> package { "awscli": provider => "pip" }
-> file { "/home/vagrant/.aws":
  ensure => 'directory'
}
-> file { 'awscli-config':
  path => '/home/vagrant/.aws/config',
  ensure => 'file',
  content => "[default]
output = json
region = us-east-1
aws_access_key_id = AKIAIWKBYQKJHCGJ7LTQ
aws_secret_access_key = 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"
}

service { "iptables":
  enable => false,
  ensure => stopped
}

$awsreleases = {
  id       => "aws-releases",
  username => "AKIAIWKBYQKJHCGJ7LTQ",
  password => "Td7zbBtAYYs8T+0FFZ4AwF12AzmdgKBodVEQKeqt"
}

$awssnapshots = {
  id       => "aws-snapshots",
  username => "AKIAIWKBYQKJHCGJ7LTQ",
  password => "Td7zbBtAYYs8T+0FFZ4AwF12AzmdgKBodVEQKeqt"
}

$genfarenexus = {
  id       => "genfarenexus",
  username => "deployment",
  password => "2th3G3nf@reCloud",
  url      => "http://repository.gfcp.io/nexus/content/groups/Genfare/",

  releases => {enabled => true},
  snapshots => {enabled => true}
}

class { "maven::maven": }

maven::settings { 'maven-user-settings' :
  servers => [$genfarenexus, $awssnapshots, $awsreleases],
  user    => 'vagrant'
}
-> file { 'mavencache':
  ensure => link,
  path => '/home/vagrant/.m2/repository',
  target => '/tmp/maven-cache'
}
-> Maven <| |>

exec { "download mysql repo":
  creates => "/tmp/download-cache/mysql-community-release-el6-5.noarch.rpm",
  command => "/usr/bin/aws s3 cp s3://genfare-deployment-artifacts/mysql-community-release-el6-5.noarch.rpm /tmp/download-cache",
  user => 'vagrant',
  timeout => 0,
  require => [Package['awscli'], File['awscli-config']]
}
-> package { 'mysql-community-release':
  ensure => installed,
  source => '/tmp/download-cache/mysql-community-release-el6-5.noarch.rpm',
  provider => rpm
}
-> package { 'mysql-community-server':
  ensure => latest
}
-> class { '::mysql::server':
  package_manage => false,
  root_password => 'vagrant',
  override_options => {
    mysqld => {
      "default-storage-engine" => "INNODB",
      "lower_case_table_names" => "1",
      "bind_address" => "0.0.0.0"
    }
  }
}

class { '::mysql::client':
  package_manage => false
}

-> mysql_database { 'test' :
  ensure => 'present'
}

-> mysql_database { 'cdta' :
  ensure => 'present'
}

-> mysql_user { 'vagrant@%' :
  ensure => 'present',
  password_hash => mysql_password('vagrant'),
  provider      => mysql
}
-> mysql_user { 'vagrant@localhost' :
  ensure => 'present',
  password_hash => mysql_password('vagrant'),
  provider      => mysql
}

-> mysql_grant { 'vagrant@%/*.*':
  ensure => 'present',
  options => ['GRANT'],
  privileges => ['ALL'],
  user       => 'vagrant@%',
  table      => '*.*',
  provider   => mysql
}

-> mysql_grant { 'vagrant@localhost/*.*':
  ensure => 'present',
  options => ['GRANT'],
  privileges => ['ALL'],
  user       => 'vagrant@localhost',
  table      => '*.*',
  provider   => mysql
}

exec { "download jboss":
  creates => "/var/tmp/jboss-eap-6.2.0.zip",
  command => "/usr/bin/aws s3 cp s3://genfare-deployment-artifacts/jboss-eap-6.2.0.zip /var/tmp",
  user => 'vagrant',
  timeout => 0,
  require => [Package['awscli'], File['awscli-config']],
  before => Class['jboss::install']
}

package { "java-1.7.0-openjdk-devel" :
  ensure => installed
}
-> class { 'jboss':
  install => 'source',
  install_source => 'file://tmp/jboss-eap-6.2.0.zip',
  created_dirname => 'jboss-eap-6.2',
  version => '7',
  bindaddr => '0.0.0.0',
  bindaddr_admin_console => '0.0.0.0'
}
-> file { "/opt/jboss/standalone/configuration/mgmt-users.properties":
  content => "admin=fef4a2d5399b026a4f9e26aa72447eb7"
}
-> maven { "/opt/jboss/modules/com/mysql/main/mysql-connector-java-5.1.29.jar":
  groupid => "mysql",
  artifactid => "mysql-connector-java",
  version => "5.1.29",
  packaging => "jar",
  require => Class['maven::maven']
}
-> file { "/opt/jboss/modules/com/mysql/main/module.xml":
  content => '<?xml version="1.0" encoding="UTF-8"?>
<module xmlns="urn:jboss:module:1.0" name="com.mysql">
  <resources>
    <resource-root path="mysql-connector-java-5.1.29.jar"/>
  </resources>
  <dependencies>
    <module name="javax.api"/>
    <module name="javax.transaction.api"/>
  </dependencies>
</module>'
}
-> maven { "/opt/jboss/modules/org/quartz-scheduler/main/quartz-1.8.6.jar":
  groupid => "org.quartz-scheduler",
  artifactid => "quartz",
  version => "1.8.6",
  packaging => "jar",
  require => Class['maven::maven']
}
-> maven { "/opt/jboss/modules/org/quartz-scheduler/main/quartz-jboss-1.8.6.jar":
  groupid => "org.quartz-scheduler",
  artifactid => "quartz-jboss",
  version => "1.8.6",
  packaging => "jar",
  require => Class['maven::maven']
}
-> file { "/opt/jboss/modules/org/quartz-scheduler/main/module.xml":
  content => '<?xml version="1.0" encoding="UTF-8"?>
<module xmlns="urn:jboss:module:1.0" name="org.quartz-scheduler">
<resources>
<resource-root path="quartz-1.8.6.jar"/>
<resource-root path="quartz-jboss-1.8.6.jar"/>
</resources>
<dependencies>
<module name="org.slf4j"/>
</dependencies>
</module>'
}
-> file { "jboss-variables":
  path    => '/etc/default/jboss',
  ensure  => 'file',
  content => "JBOSS_CONFIG=standalone-ha.xml\nJBOSS_ADDITIONAL_OPTIONS='-Djboss.node.name=gfcp-local'",
}
-> file { '/opt/jboss/bin/standalone.conf':
  source  => '/vagrant/vagrant/files/standalone.conf',
  owner   => 'jboss',
  group   => 'jboss',
  mode    => '0755',
}
-> exec { 'jboss-stop':
  command => '/sbin/service jboss stop',
}
-> exec { "wipe-standalone":
  command => '/bin/cp -f /opt/jboss/standalone/configuration/standalone_xml_history/standalone-ha.initial.xml /opt/jboss/standalone/configuration/standalone-ha.xml',
  require => Service['jboss'],
}
-> exec { 'jboss-start':
  command => '/sbin/service jboss start',
}
-> exec { "configure-eap":
  command => '/opt/jboss/bin/jboss-cli.sh --connect --user=admin --password=password --file=/vagrant/vagrant/files/jboss.cli',
  require => Service['jboss'],
  notify  => Exec['reload-check']
}

exec {'reload-check':
  onlyif    => "/opt/jboss/bin/jboss-cli.sh --connect --user=admin --password=password \":read-attribute(name=server-state)\" | grep reload-required",
  command   => '/sbin/service jboss restart',
  logoutput => true
}

exec { "download fuse":
  creates    => "/var/tmp/jboss-fuse-full-6.1.0.redhat-379.zip",
  command    => "/usr/bin/aws s3 cp s3://genfare-deployment-artifacts/jboss-fuse-full-6.1.0.redhat-379.zip /var/tmp",
  user       => 'vagrant',
  timeout    => 0,
  require    => [Package['awscli'], File['awscli-config']],
  before     => Class['fuse::install']
}

class { 'fuse':
  install         => 'source',
  install_source  => 'file://tmp/jboss-fuse-full-6.1.0.redhat-379.zip',
  created_dirname => 'jboss-fuse-6.1.0.redhat-379',
  version         => '6',
  process_user    => 'jboss',
  admin_user      => 'admin',
  admin_password  => 'password',
  disableboot     => 'true',
}

file { "/etc/httpd/conf/htpasswd":
  mode => 644
}

httpauth { 'gfcp':
  file     => '/etc/httpd/conf/htpasswd',
  password => 'genfare2014!',
  realm => 'realm',
  mechanism => basic,
  ensure => present,
}

class { 'apache':
  default_vhost => true,
}

class { 'mod_cluster': }

service { 'network':
  ensure  => 'running',
}

