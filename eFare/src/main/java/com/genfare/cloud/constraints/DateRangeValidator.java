package com.genfare.cloud.constraints;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import org.apache.commons.beanutils.BeanUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import com.genfare.cloud.annotation.DateRange;

public class DateRangeValidator
    implements ConstraintValidator<DateRange, Object> {

    private String begdate;

    private String enddate;

    private String range;

    protected static final DateTimeFormatter formatter = DateTimeFormat.forPattern("MM/dd/yyyy");

    public DateRangeValidator() {
    }

    public DateRangeValidator(String begdate, String enddate, String range) {
        this.begdate = begdate;
        this.enddate = enddate;
        this.range = range;
    }

    @Override
    public void initialize(final DateRange constraintAnnotation) {
        begdate = constraintAnnotation.begdate();
        enddate = constraintAnnotation.enddate();
        range = constraintAnnotation.range();
    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
        try {
            final Object beg = BeanUtils.getProperty(value, begdate);
            final Object end = BeanUtils.getProperty(value, enddate);
            final Object r = BeanUtils.getProperty(value, range);
            DateTime first = formatter.parseDateTime((String) beg).withTime(23, 59, 59, 999);
            DateTime second = formatter.parseDateTime((String) end).withTime(23, 59, 59, 999);

            if (second.isAfter(DateTime.now().plusDays(1)) || first.isAfter(DateTime.now().plusDays(1))
                || first.isBefore(DateTime.now().minusMonths(Integer.parseInt((String) r)).minusDays(1))
                || second.isBefore(first)) {
                return false;
            }
            else {
                return true;
            }
        } catch (final Exception ignore) {
            // ignore
        }
        return true;
    }
}
