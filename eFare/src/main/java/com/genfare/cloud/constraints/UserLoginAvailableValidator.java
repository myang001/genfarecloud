package com.genfare.cloud.constraints;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

import com.genfare.cloud.annotation.UserLoginAvailable;
import com.genfare.cloud.model.service.UserManagementService;

public class UserLoginAvailableValidator
    implements ConstraintValidator<UserLoginAvailable, String> {

    @Autowired
    UserManagementService usermanager;

    @Override
    public void initialize(UserLoginAvailable constraintAnnotation) {

    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {

        if (value == null || value.isEmpty()) {
            return true;
        }

        return usermanager.getUser(value) == null;

    }

}
