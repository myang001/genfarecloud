package com.genfare.cloud.resolvers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.thymeleaf.Arguments;
import org.thymeleaf.messageresolver.AbstractMessageResolver;
import org.thymeleaf.messageresolver.MessageResolution;

import com.genfare.cloud.model.service.PropertiesService;

public class DatabaseMessageResolver
    extends AbstractMessageResolver {
    private static final Logger LOGGER = LoggerFactory.getLogger(DatabaseMessageResolver.class);

    private PropertiesService propertiesService;

    public void setService(PropertiesService propertiesService) {
        this.propertiesService = propertiesService;
    }

    @Override
    public MessageResolution resolveMessage(Arguments arguments, String key, Object[] messageParameters) {

        checkInitialized();
        if (propertiesService == null) {
            return null;
        }
        String result = propertiesService.getProperty(arguments.getContext().getLocale().toString(), key);
        return (result == null || result.isEmpty()) ? null : new MessageResolution(result);
    }
}