package com.genfare.cloud.repository;

import java.util.List;
import java.util.Set;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.encrypt.TextEncryptor;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionFactoryLocator;
import org.springframework.social.connect.ConnectionRepository;
import org.springframework.social.connect.jdbc.JdbcUsersConnectionRepository;

import com.genfare.cloud.model.tenant.TenantConfiguration;

/**
 * Class to manage MultiTenant schema change
 * 
 * @author martin
 */
public class MultiTenantJdbcUsersConnectionRepository
    extends JdbcUsersConnectionRepository {

    @Autowired
    private TenantConfiguration tenantConfiguration;

    public MultiTenantJdbcUsersConnectionRepository(DataSource dataSource,
        ConnectionFactoryLocator connectionFactoryLocator,
        TextEncryptor textEncryptor) {
        super(dataSource, connectionFactoryLocator, textEncryptor);
    }

    @Override
    public List<String> findUserIdsWithConnection(Connection<?> connection) {
        setTablePrefix(tenantConfiguration.getSchemaName() + ".");
        return super.findUserIdsWithConnection(connection);
    }

    @Override
    public Set<String> findUserIdsConnectedTo(String providerId, Set<String> providerUserIds) {
        setTablePrefix(tenantConfiguration.getSchemaName() + ".");
        return super.findUserIdsConnectedTo(providerId, providerUserIds);
    }

    @Override
    public ConnectionRepository createConnectionRepository(String userId) {
        setTablePrefix(tenantConfiguration.getSchemaName() + ".");
        return super.createConnectionRepository(userId);
    }

}
