package com.genfare.cloud.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.genfare.cloud.model.security.User;

/**
 * @author martin
 */
public interface UserRepository
    extends JpaRepository<User, Long> {

    public User findByLogin(String email);

}
