package com.genfare.cloud.annotation;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.genfare.cloud.constraints.UserLoginAvailableValidator;

@Target({ FIELD, ANNOTATION_TYPE })
@Retention(RUNTIME)
@Constraint(validatedBy = UserLoginAvailableValidator.class)
public @interface UserLoginAvailable {

    String message() default "{validator.userloginavailable}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
