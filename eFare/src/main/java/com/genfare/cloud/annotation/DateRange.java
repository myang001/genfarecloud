package com.genfare.cloud.annotation;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.genfare.cloud.constraints.DateRangeValidator;

/**
 * Validation annotation to validate a Date Range. Dates must comply this constrains: End date later than Beginning Date End
 * date in future Start date in future Date range that is 3 months less. Example:
 * 
 * @DateRange.List({@DateRange(begdate="begdate",enddate="enddate", 
 *                                                                  message="Please select a date range that is 3 months or less"
 *                                                                  )})
 */

@Target({ TYPE, ANNOTATION_TYPE })
@Retention(RUNTIME)
@Constraint(validatedBy = DateRangeValidator.class)
public @interface DateRange {

    String message() default "{validator.dateRange}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    /**
     * @return The Beginning Date
     */
    String begdate();

    /**
     * @return The End Date
     */
    String enddate();

    String range() default "3";

    @Target({ TYPE, ANNOTATION_TYPE })
    @Retention(RUNTIME)
    @Documented
    @interface List {
        DateRange[] value();
    }

}
