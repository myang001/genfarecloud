package com.genfare.cloud.controller;

import java.security.Principal;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.genfare.cloud.form.EditUserForm;
import com.genfare.cloud.model.contact.Email;
import com.genfare.cloud.model.entity.DocumentsID;
import com.genfare.cloud.model.entity.repository.DocumentsRepository;
import com.genfare.cloud.model.entity.repository.LanguagesRepository;
import com.genfare.cloud.model.security.User;
import com.genfare.cloud.model.security.User.UserBuilder;
import com.genfare.cloud.model.service.EmailService;
import com.genfare.cloud.model.service.UserDetailsAssembler;
import com.genfare.cloud.model.service.UserManagementService;
import com.genfare.cloud.model.service.WalletManagementService;
import com.genfare.cloud.model.tenant.TenantConfiguration;
import com.genfare.cloud.model.types.IdentifiertypesType;
import com.genfare.cloud.reports.base.AbstractReport;
import com.genfare.cloud.reports.service.ReportRegistry;

@Controller
@RequestMapping("/")
public class RootController {

    private static final Logger LOGGER = Logger.getLogger(RootController.class);

    @Autowired
    private TenantConfiguration tenantConfiguration;

    @Autowired
    private UserManagementService usermanager;

    @Autowired
    private WalletManagementService walletmanager;

    @Autowired
    private EmailService emailservice;

    @Autowired
    private UserDetailsAssembler assembler;

    @PersistenceContext
    private EntityManager em;

    @Autowired
    private ReportRegistry reportRegistry;

    @Autowired
    private DocumentsRepository docs;

    @Autowired
    private LanguagesRepository languages;

    @RequestMapping(method = RequestMethod.GET)
    public String getRoot(@ModelAttribute("editUserForm") EditUserForm editUserForm,
        @RequestParam(value = "emailChange", required = false, defaultValue = "false") boolean emailChange, ModelMap model, Principal principal, HttpServletRequest request) {
        User user = usermanager.getUser(principal);
        if (user != null) {

            model.addAttribute("user", user);

            editUserForm = new EditUserForm(user);
            model.addAttribute("editUserForm", editUserForm);

            model.addAttribute("wallets", walletmanager.sortWallets(walletmanager.getWalletsForUser(user)));
            model.addAttribute("identifierType", IdentifiertypesType.PRINTED_ID.getIndex());

            AbstractReport report = reportRegistry.getReport("cardhistory");
            model.addAllAttributes(report.query(em, user.getId(), request));

            model.addAttribute("emailChange", emailChange);

            return "home/user";
        }

        return "home/home";
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(method = RequestMethod.POST)
    public String updateUser(@ModelAttribute("editUserForm") @Valid EditUserForm editUserForm, BindingResult result, ModelMap model, Principal principal, RedirectAttributes attrs,
        HttpServletRequest request) {

        User user = usermanager.getUser(principal);

        if (result.hasErrors()) {
            model.addAttribute("user", user);
            editUserForm.setEmail(user.getEmail());
            model.addAttribute("editUserForm", editUserForm);

            model.addAttribute("wallets", walletmanager.sortWallets(walletmanager.getWalletsForUser(user)));
            model.addAttribute("identifierType", IdentifiertypesType.PRINTED_ID.getIndex());

            AbstractReport report = reportRegistry.getReport("cardhistory");
            model.addAllAttributes(report.query(em, user.getId(), request));
            return "home/user";
        }

        String oldLogin = principal.getName();
        user = editUserForm.updateUser(user);
        usermanager.saveUser(user);

        if (!oldLogin.equals(user.getLogin())) {
            usermanager.reloadUser(user);
            sendEmails(oldLogin, user);
            return "redirect:/?emailChange=true#profile";
        }

        return "redirect:/#profile";
    }

    @RequestMapping(value = "/faq", method = RequestMethod.GET)
    public String getFAQs(ModelMap model, Principal principal, HttpServletRequest request) {
        DocumentsID id = new DocumentsID("faq", languages.findOne(new Byte((byte) 1)), "faq.pageContent");
        model.addAttribute("pageContent", docs.findOne(id).getPageValue());
        return "content/faq";
    }

    private void sendEmails(String oldLogin, User user) {
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("efareWebsite", "");
        parameters.put("oldEmail", oldLogin);
        parameters.put("newEmail", user.getLogin());
        parameters.put("telephone", "");
        parameters.put("transitAgency", "");
        emailservice.sendEmail(user, "Change User Login", "updateuserlogin", parameters);

        Email email = new Email();
        email.setAddress(oldLogin);
        User oldUser = new UserBuilder(
            user.getId(),
            user.getLogin(),
            user.getFirstName(),
            user.getLastName(),
            email,
            user.getPhone(),
            user.getAddress()
            ).build();

        emailservice.sendEmail(oldUser, "Change User Login", "updateuserlogin", parameters);
    }
}