package com.genfare.cloud.controller;

import java.math.BigDecimal;
import java.security.Principal;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.genfare.cloud.dto.SelectionData;
import com.genfare.cloud.form.CardRegistrationForm;
import com.genfare.cloud.form.SearchCardForm;
import com.genfare.cloud.form.SignupForm;
import com.genfare.cloud.model.contact.Address;
import com.genfare.cloud.model.entity.Orderitems;
import com.genfare.cloud.model.entity.OrderitemsId;
import com.genfare.cloud.model.entity.Orders;
import com.genfare.cloud.model.entity.Shippers;
import com.genfare.cloud.model.entity.Wallets;
import com.genfare.cloud.model.entity.repository.OfferingsRepository;
import com.genfare.cloud.model.entity.repository.OrderitemsRepository;
import com.genfare.cloud.model.entity.repository.PeopleRepository;
import com.genfare.cloud.model.entity.repository.ShippersRepository;
import com.genfare.cloud.model.entity.repository.TicketsRepository;
import com.genfare.cloud.model.exception.InvalidOrderException;
import com.genfare.cloud.model.exception.WalletHasPeopleAssignedException;
import com.genfare.cloud.model.security.User;
import com.genfare.cloud.model.service.BusinessRulesService;
import com.genfare.cloud.model.service.CreditServiceService;
import com.genfare.cloud.model.service.OrderManagementService;
import com.genfare.cloud.model.service.UserManagementService;
import com.genfare.cloud.model.service.WalletContentUtilityService;
import com.genfare.cloud.model.service.WalletManagementService;
import com.genfare.cloud.model.tenant.TenantConfiguration;
import com.genfare.cloud.model.types.OrderItemStatusType;
import com.genfare.cloud.model.types.OrderitemactionsType;
import com.genfare.cloud.model.types.OrderitemtypesType;
import com.genfare.cloud.model.types.OrdertypesType;
import com.genfare.cloud.model.types.WalletstatusType;

@Controller
@RequestMapping("/card")
public class CardController {

    private static final Logger LOGGER = LoggerFactory.getLogger(CardController.class);

    @Autowired
    private WalletManagementService walletManagementService;

    @Autowired
    private UserManagementService usermanager;

    @Autowired
    private WalletContentUtilityService walletContentUtil;

    @Autowired
    private TicketsRepository ticketsRepository;

    @Autowired
    private PeopleRepository peopleRepository;

    @Autowired
    private ShippersRepository shippersRepository;

    @Autowired
    private OfferingsRepository offersRepository;

    @Autowired
    private TenantConfiguration tenantConfiguration;

    @Autowired
    private CreditServiceService creditServiceService;

    @Autowired
    private OrderManagementService orderManagementService;

    @Autowired
    private OrderitemsRepository orderItemRepo;

    @Autowired
    protected MessageSource resource;

    @Autowired
    private BusinessRulesService businessRulesService;

    @RequestMapping(method = RequestMethod.GET)
    public String getRoot(Principal principal) {
        return "forward:" + ((principal == null) ? "/card/search" : "/");
    }

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public String getCardSearch(SearchCardForm model) {
        return "search/card";
    }

    @RequestMapping(value = "/search", method = RequestMethod.POST)
    public String searchCard(@Valid SearchCardForm searchCardForm, CardRegistrationForm registrationForm, BindingResult result, Principal principal,
        Model model) {

        User user = usermanager.getUser(principal);
        Wallets wallet = null;
        if (result.hasErrors()) {
            return "search/card";

        }
        try {
            if (user != null) {
                // Registered User Only Can Do this for their own card
                wallet = walletManagementService.getWalletForUser(user, searchCardForm.getCardNumber());
            }
            if (wallet == null) {
                wallet =
                    walletManagementService.getWallet(searchCardForm.getCvvNumber().toString(),
                        searchCardForm.getCardNumber().toString());
                if (walletManagementService.walletVisibleForUser(wallet, user) == false) {
                    wallet = null;
                }
            }

            if (wallet == null) {
                LOGGER.info("No Result");
                Locale locale = LocaleContextHolder.getLocale();
                model.addAttribute("error", resource.getMessage("searchCard.result.notResult", null, locale));
                return "search/card";
            }

            LOGGER.debug("Wallet ID: " + wallet.getWalletId());
            model.addAttribute("wallet", wallet);
        } catch (EntityNotFoundException e) {
            LOGGER.info("No Result");
            Locale locale = LocaleContextHolder.getLocale();
            model.addAttribute("error", resource.getMessage("searchCard.result.notResult", null, locale));
        }
        return "search/card";
    }

    @RequestMapping(value = "/details", method = RequestMethod.GET)
    public String cardDetailLookup(@RequestParam(value = "cardnumber", required = false) String card, @RequestParam(
        value = "cvv", required = false) String cvv, Principal principal, ModelMap model) {

        User user = usermanager.getUser(principal);
        Wallets wallet = null;

        if (card == null || card.isEmpty()) {
            return "redirect:/card/search";
        }

        if (cvv == null || cvv.isEmpty()) {
            // No CVV Supplied
            if (user != null) {
                // Registered User Only Can Do this for their own card
                wallet = walletManagementService.getWalletForUser(user, card);
            }
        } else {
            // CVV Supplied from search page
            wallet = walletManagementService.getWallet(cvv, card);
            if (walletManagementService.walletVisibleForUser(wallet, user) == false) {
                wallet = null;
            }
            if (user != null) {
                CardRegistrationForm form = new CardRegistrationForm();
                form.setCardNumber(card);
                form.setCvvNumber(cvv);
                model.addAttribute("registrationForm", form);
            }
        }

        if (wallet == null) {
            return "redirect:/card/search";
        } else if (orderManagementService.isCardBeingReplaced(user, wallet)) {
            model.addAttribute("replacingCard", true);
        }

        model.addAttribute("wallet", wallet);
        model.addAttribute("cardNumber", card);
        model.addAttribute("current", walletManagementService.paramaterizeWalletContents(wallet));
        model.addAttribute("processing", walletManagementService.paramaterizePendingItems(wallet));
        model.addAttribute("walletContentUtil", walletContentUtil);
        model.addAttribute("rules", businessRulesService);
        return (wallet.getPeople() == null) ? "card/detailsUnreg" : "card/details";
    }

    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public String showCardSearch(CardRegistrationForm model) {
        return "register/register";
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String registerCard(@Valid CardRegistrationForm cardRegistrationForm, BindingResult result, Model model,
        Principal principal) {

        if (result.hasErrors()) {
            return "register/register";
        }
        User user = usermanager.getUser(principal);
        try {
            walletManagementService.registerWallet(user, cardRegistrationForm.getCvvNumber(),
                cardRegistrationForm.getCardNumber(),
                cardRegistrationForm.getNickname());
        } catch (EntityNotFoundException e) {
            LOGGER.info("No Card Found");
            model.addAttribute("error", "cardRegistration.error.card_not_found_or_has_people_assigned");
            return "register/register";
        } catch (WalletHasPeopleAssignedException e) {
            LOGGER.info("Card has assigned People");
            model.addAttribute("error", "cardRegistration.error.card_not_found_or_has_people_assigned");
            return "register/register";
        }
        return "redirect:/";
    }

    @RequestMapping(value = "/replace", method = RequestMethod.GET)
    public String replaceCard(ModelMap model, HttpSession session, Principal principal, HttpServletRequest req) {
        String cardnumber = req.getParameter("cardnumber");

        User user = usermanager.getUser(principal);
        if (user == null) {
            return "redirect:/login";
        }
        model.addAttribute("cardList", createCardList(user, cardnumber));
        return "products/replaceCard";
    }

    private List<SelectionData> createCardList(User user, String selectedCardNumber) {
        List<SelectionData> list = new ArrayList<SelectionData>();
        if (user == null)
            return list;
        Map<String, String> map = walletManagementService.getWalletSet(user);
        for (Entry<String, String> e : map.entrySet()) {
            String strID = e.getKey();
            String strText = e.getValue();
            if (selectedCardNumber != null)
                list.add(new SelectionData(strID, strText, selectedCardNumber.equals(strID)));
            else
                list.add(new SelectionData(strID, strText, false));
        }
        return list;
    }

    @RequestMapping(value = "/replace", method = RequestMethod.POST)
    public String replaceCardReview(ModelMap model, HttpSession session, Principal principal, HttpServletRequest req) throws InvalidOrderException {
        String PID = req.getParameter("card");
        Wallets w = walletManagementService.getWallet(PID);
        User user = usermanager.getUser(principal);
        if (!user.getId().equals(w.getPeople().getPersonId()) || orderManagementService.isCardBeingReplaced(user, w)) {
            return "redirect:/card/replace"; // show error
        }
        String cardName = w.getNickname();
        String[] reason = req.getParameter("reason").split("\\s");
        String price = reason[1];
        String info = reason[0];

        w.setWalletstatus(WalletstatusType.DAMAGED.getStatus()); // Hardcoded until dropdown is database driven

        model.addAttribute("cardnumber", PID);
        model.addAttribute("cardname", cardName);
        model.addAttribute("price", price);

        SignupForm shipping_form = new SignupForm();
        model.addAttribute("user", user);

        if (null != user) {
            shipping_form.setAddress(user.getAddress());
            shipping_form.setLastName(user.getLastName());
            shipping_form.setFirstName(user.getFirstName());
        }

        model.addAttribute("shippingForm", shipping_form);

        Locale locale = new Locale("en", "US");
        NumberFormat currencyFormat = NumberFormat.getCurrencyInstance(locale);
        BigDecimal cost;
        try {
            cost = new BigDecimal("" + currencyFormat.parse(price));
        } catch (ParseException e) {
            throw new IllegalArgumentException("" + e);
        }

        Orders order = orderManagementService.getCurrentOrCreate(null, null, OrdertypesType.SERVICE.getType());
        order.setPeople(peopleRepository.findOne(user.getId()));
        Orderitems replace =
            new Orderitems(new OrderitemsId(order.getOrderId(), (byte) 0), OrderitemactionsType.REPLACE.getType(),
                OrderitemtypesType.WALLET.getType(), (byte) 0, (short) 1,
                OrderItemStatusType.UNSUBMITTED.getStatus(), order, (short) 1, cost, cost);
        replace.setWallets(w);
        orderManagementService.addOrderItem(order, replace);
        model.addAttribute("orderCount", orderManagementService.orderItemsCount(order));
        model.addAttribute("order", order);
        model.addAttribute("shippers", shippersRepository.findAll());
        Iterable<Shippers> shippers = shippersRepository.findAll();
        Shippers os = order.getShippers();
        for (Shippers shipper : shippers) {
            if (null != os && os.getShipperId() == shipper.getShipperId()) {
                model.addAttribute("selectedShipper", shipper);
            }
        }

        model.addAttribute("creditServiceData",
            creditServiceService.getFormData(order, user, "/store/cart/success", null));
        model.addAttribute("postURL", tenantConfiguration.getCreditServicePostURL());

        return "products/replaceShipping";
    }

    @RequestMapping(value = "/replace/shipping", method = RequestMethod.POST)
    public String shippingPost(Principal principal, HttpSession session, HttpServletRequest request, ModelMap model) {
        SignupForm shipping_form = new SignupForm();
        User user = usermanager.getUser(principal);
        Orders order = orderManagementService.getOrder(new Integer(request.getParameter("orderid")));
        model.addAttribute("order", order);

        model.addAttribute("user", user);
        String country = null;
        if (request.getParameter("address.country") != null) {
            country = request.getParameter("address.country").toString();
        }
        if ((null != user) || (request.getParameter("update") != null)) {
            Address a1 = new Address();
            a1.setLine1(request.getParameter("address.line1"));
            a1.setLine2(request.getParameter("address.line2"));
            a1.setCity(request.getParameter("address.city"));
            a1.setState(request.getParameter("address.state"));
            a1.setPostalCode(request.getParameter("address.postalCode"));
            a1.setCountry(country);
            shipping_form.setAddress(a1);
            shipping_form.setLastName(request.getParameter("lastName"));
            shipping_form.setFirstName(request.getParameter("firstName"));
        }

        model.addAttribute("shippingForm", shipping_form);

        // @formatter:off
        String jsonString =
            new JSONObject().put("Type", "Shipping Address")
                .put("firstName", request.getParameter("firstName"))
                .put("lastName", request.getParameter("lastName"))
                .put("Address", new JSONObject()
                    .put("line1", request.getParameter("address.line1"))
                    .put("line2", request.getParameter("address.line2"))
                    .put("city", request.getParameter("address.city"))
                    .put("state", request.getParameter("address.state"))
                    .put("postalCode", request.getParameter("address.postalCode"))
                    .put("country", country)).toString();
        order.setOrderData(jsonString);
        // @formatter:on

        String shippingMethod = request.getParameter("method");
        Iterable<Shippers> shippers = shippersRepository.findAll();
        model.addAttribute("shippers", shippersRepository.findAll());
        for (Shippers shipper : shippers) {
            if (shipper.getDescription().equals(shippingMethod) && orderManagementService.orderRequiresShipping(order)) {
                order.setShippers(shipper);
                model.addAttribute("selectedShipper", shipper);
            }
        }

        orderManagementService.saveOrder(order);
        model.addAttribute("creditServiceData",
            creditServiceService.getFormData(order, user, "/store/cart/success", null));
        model.addAttribute("postURL", tenantConfiguration.getCreditServicePostURL());

        if (request.getParameter("update") != null) {

            return "products/replaceShipping";
        }
        else {

            model.addAllAttributes(request.getParameterMap());
            return "cart/cartShippingAutopost";
        }

    }
}
