package com.genfare.cloud.controller;

import java.security.Principal;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.thymeleaf.context.WebContext;

import com.genfare.cloud.form.EmailForm;
import com.genfare.cloud.model.security.User;
import com.genfare.cloud.model.service.EmailService;
import com.genfare.cloud.model.service.UserManagementService;

@Controller
@RequestMapping("/verify")
public class EmailVerificationController {

    private static final Logger log = LoggerFactory.getLogger(EmailVerificationController.class);

    @Autowired
    UserManagementService usermanager;

    @Autowired
    EmailService emailservice;

    @RequestMapping(method = RequestMethod.GET)
    public String emailNotVerified(@RequestParam(value = "token", required = false, defaultValue = "") String token, Principal principal) {
        try {
            if (principal != null) {
                return "redirect:/";
            }

            if (token == null || token.isEmpty()) {
                return "email/notVerified";
            }

            if (emailservice.verifyToken(token)) {
                emailservice.deleteToken(token);
                return "redirect:/login?verified=true";
            }

            return "email/verificationFailed";
        } catch (Exception e) {
            throw new EmailException("GFCP-83001E", "This is a custom message: " + e);
        }
    }

    @RequestMapping(value = "/resend", method = RequestMethod.GET)
    public String resendToken(@RequestParam(value = "email", required = false, defaultValue = "") String email, ModelMap map, HttpServletRequest request,
        HttpServletResponse response) {
        if (!email.equals("")) {
            EmailForm emailForm = new EmailForm();
            emailForm.setEmail(email);
            generateNewToken(emailForm, new BeanPropertyBindingResult(emailForm, "emailForm"), request, response);
            map.addAttribute("email", email);
            map.addAttribute("success", true);
            return "email/thanksForRegistering";
        }
        try {
            map.addAttribute("emailForm", new EmailForm());
            return "email/resendVerification";
        } catch (Exception e) {
            throw new EmailException("E888", "This is custom message: " + e);
        }
    }

    @RequestMapping(value = "/resend", method = RequestMethod.POST)
    public String generateNewToken(@Valid EmailForm emailForm, BindingResult result, HttpServletRequest request,
        HttpServletResponse response) {
        try {
            if (result.hasErrors()) {
                // Just tell them we sent it, but do not actually hit our DB for no reason
                return "email/verificationResent";
            }

            try {
                User user = usermanager.getUser(emailForm.getEmail());
                emailservice.setContext(new WebContext(request, response, request.getServletContext()));
                emailservice.generateVerificationToken(user);
                log.debug("New Email Verification Token Set for User: " + user.getLogin());
            } catch (EntityNotFoundException e) {
                log.debug("Not Generating Token for Unknown User");
            }

            return "email/verificationResent";
        } catch (Exception e) {
            throw new EmailException("E888", "This is custom message: " + e);
        }
    }
}
