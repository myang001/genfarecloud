package com.genfare.cloud.controller;

import java.math.BigDecimal;
import java.security.Principal;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.genfare.cloud.form.SignupForm;
import com.genfare.cloud.model.contact.Address;
import com.genfare.cloud.model.entity.Farecodes;
import com.genfare.cloud.model.entity.Offerings;
import com.genfare.cloud.model.entity.Orderitems;
import com.genfare.cloud.model.entity.OrderitemsId;
import com.genfare.cloud.model.entity.Orders;
import com.genfare.cloud.model.entity.Orders.Shopping;
import com.genfare.cloud.model.entity.Shippers;
import com.genfare.cloud.model.entity.Tickets;
import com.genfare.cloud.model.entity.Wallets;
import com.genfare.cloud.model.entity.Wallettypes;
import com.genfare.cloud.model.entity.repository.OfferingsRepository;
import com.genfare.cloud.model.entity.repository.OrderitemsRepository;
import com.genfare.cloud.model.entity.repository.OrderstatusesRepository;
import com.genfare.cloud.model.entity.repository.PeopleRepository;
import com.genfare.cloud.model.entity.repository.ShippersRepository;
import com.genfare.cloud.model.entity.repository.TicketsRepository;
import com.genfare.cloud.model.entity.repository.WalletsRepository;
import com.genfare.cloud.model.entity.repository.WallettypesRepository;
import com.genfare.cloud.model.entity.repository.ZonesRepository;
import com.genfare.cloud.model.exception.InvalidOrderException;
import com.genfare.cloud.model.security.User;
import com.genfare.cloud.model.service.BusinessRulesService;
import com.genfare.cloud.model.service.CreditServiceService;
import com.genfare.cloud.model.service.OrderManagementService;
import com.genfare.cloud.model.service.TicketsEligibilityService;
import com.genfare.cloud.model.service.UserManagementService;
import com.genfare.cloud.model.service.WalletManagementService;
import com.genfare.cloud.model.tenant.TenantConfiguration;
import com.genfare.cloud.model.types.ChannelsType;
import com.genfare.cloud.model.types.FarecodesType;
import com.genfare.cloud.model.types.IdentifiertypesType;
import com.genfare.cloud.model.types.OrderItemStatusType;
import com.genfare.cloud.model.types.OrderStatusType;
import com.genfare.cloud.model.types.OrderitemactionsType;
import com.genfare.cloud.model.types.OrderitemtypesType;
import com.genfare.cloud.model.types.OrdertypesType;
import com.genfare.cloud.model.util.Contact;

@Controller
@RequestMapping("/store")
@SessionAttributes("order")
public class StoreController {

    private static final Logger LOGGER = LoggerFactory.getLogger(StoreController.class);

    @Autowired
    private TicketsEligibilityService ticketsEligibilityService;

    @Autowired
    private WalletManagementService walletManagementService;

    @Autowired
    private WalletsRepository walletsRepository;

    @Autowired
    private OfferingsRepository offersRepository;

    @Autowired
    private UserManagementService usermanager;

    @Autowired
    private OrderManagementService ordermanager;

    @Autowired
    private OrderstatusesRepository orderstatusesRepository;

    @Autowired
    private OrderitemsRepository orderitemsRepository;

    @Autowired
    private ZonesRepository zoneRepository;

    @Autowired
    private ShippersRepository shippersRepository;

    @Autowired
    private TenantConfiguration tenantConfiguration;

    @Autowired
    private CreditServiceService creditServiceService;

    @Autowired
    private BusinessRulesService businessRulesService;

    @Autowired
    private WallettypesRepository wallettypesRepository;

    @Autowired
    private TicketsRepository ticketRepository;

    @Autowired
    private PeopleRepository peopleRepository;

    @Autowired
    private BusinessRulesService rulesService;

    // Shopping landing
    @RequestMapping(method = RequestMethod.GET)
    public String getProducts(HttpSession session, Principal principal) {
        return (principal != null) ? "products/filter" : "redirect:/";
    }

    // Shopping landing select (filter page)
    @RequestMapping(method = RequestMethod.POST)
    public String viewNewCardOrSearch(HttpSession session, HttpServletRequest request) {
        if (request.getParameter("cardChoice").equals("new")) {
            return "redirect:/store/cards";
        } else {
            return "redirect:/card/register";
        }
    }

    // Card Shopping
    @RequestMapping(value = "/cards", method = RequestMethod.GET)
    public String viewAvailableCards(ModelMap model, HttpSession session, Principal principal) {
        User user = usermanager.getUser(principal);
        Iterable<Offerings> offers = offersRepository.findWallettypeOfferings(ChannelsType.EFARE.getIndex().byteValue());

        Orders order = ordermanager.getCurrentOrCreate(user, session, OrdertypesType.SALES.getType());
        model.addAttribute("user", user);
        model.addAttribute("offers", offers);
        model.addAttribute("order", order);
        return "products/selectCard";
    }

    // Card Shopping select a new card to buy
    @RequestMapping(value = "/cards", method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.MOVED_PERMANENTLY)
    public String selectCard(HttpSession session, HttpServletRequest request, ModelMap model, RedirectAttributes attrs, Principal principal) {
        User user = usermanager.getUser(principal);
        Orders order = ordermanager.getCurrentOrCreate(user, session, OrdertypesType.SALES.getType());
        String quantity = request.getParameter("quantity");
        short shortQuantity = ordermanager.validateQuantity(quantity);
        if (quantity.isEmpty() || shortQuantity <= 0) {
            return "redirect:/store/cards";
        }
        Offerings offer = offersRepository.findOne(new Byte(request.getParameter("offerId")));
        Wallettypes walletType = offer.getWallettypes();
        Integer orderItemNumber = new Integer(order.getOrderitemses().size());
        Orderitems orderItem =
            new Orderitems(new OrderitemsId(order.getOrderId(), orderItemNumber.byteValue()), OrderitemactionsType.NEW.getType(), OrderitemtypesType.WALLETTYPE.getType(),
                orderItemNumber.byteValue(), OrderItemStatusType.UNSUBMITTED.getIndex().shortValue(), OrderItemStatusType.UNSUBMITTED.getStatus(), order, shortQuantity,
                offer.getUnitPrice(), offer.getUnitPrice().multiply(new BigDecimal(shortQuantity)));
        orderItem.setWallettypes(walletType);
        orderItem = ordermanager.updateOrderItemFarecode(orderItem, user);
        String success = ordermanager.checkRules(orderItem, order, user, session);
        if (success != null) {
            if (success.equals("itemError")) {
                attrs.addFlashAttribute("itemError", true);
            }
            else {
                attrs.addFlashAttribute("orderError", true);
            }
            return "redirect:cards";
        }
        // The description must be retrieved for the confirmation page
        if (orderItem.getWallettypes() != null) {
            orderItem.getWallettypes().getDescription();
        }
        attrs.addFlashAttribute("item", orderItem);
        attrs.addFlashAttribute("farecode", orderItem.getFarecodes());

        if ((offer.getDescription().contains("Card")) || (offer.getDescription().contains("LUCC"))) {
            attrs.addFlashAttribute("parent", orderItem.getId().getOrderItemNumber());
            return "redirect:confirm";
        }
        return "redirect:confirm";
    }

    // Show available products
    @RequestMapping(value = "/products", method = RequestMethod.GET)
    public String getAddProductsPage(@RequestParam(value = "parent", required = false) String parent, @RequestParam(value = "wallet", required = false) String walletId,
        HttpServletRequest request, HttpSession session, Principal principal, ModelMap model) {
        User user = usermanager.getUser(principal);
        Wallets wallet = null;
        if (parent != null) {
            model.addAttribute("parent", parent);
        }
        if (walletId != null) {
            Long id = Long.valueOf(walletId).longValue();
            wallet = walletsRepository.findByID(id);
            model.addAttribute("cardnumber", walletManagementService.getPrintedId(walletId));
            model.addAttribute("wallet", walletId);
            model.addAttribute("farecode", wallet.getFarecodes());
            if (ordermanager.isCardBeingReplaced(user, wallet)) {
                model.addAttribute("replaceError", true);
            }
        } else {
            if (user != null && ChannelsType.EFARE.getType().getCanInherit()) {
                Set<Farecodes> farecodes = user.getFarecodes();
                Farecodes f = FarecodesType.FULL.getType();
                if (farecodes.size() > 0) {
                    // Get first
                    for (Farecodes ff : farecodes) {
                        f = ff;
                        break;
                    }
                }
                model.addAttribute("farecode", f);
            } else {
                model.addAttribute("farecode", FarecodesType.FULL.getType());
            }
        }

        model.addAttribute("order", ordermanager.getCurrentOrCreate(user, session, OrdertypesType.SALES.getType()));
        model.addAttribute("storedValue", walletManagementService.getStoredValueContents(wallet));
        model.addAllAttributes(ticketsEligibilityService.getAllEligibleTicketOfferings(wallet, user));
        model.addAttribute("zones", zoneRepository.findAll());
        if (!businessRulesService.purchaseNewProducts(wallet)) {
            return "redirect:/store/cards";
        }
        if (model.get("parent") == null && model.get("cardnumber") == null) {
            return "redirect:/store/cards";
        }
        return "products/addProducts";
    }

    // Selected a product to add to a card
    @RequestMapping(value = "/products", method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.MOVED_PERMANENTLY)
    public String addProduct(HttpServletRequest request, HttpSession session, ModelMap model, Principal principal, RedirectAttributes attrs) throws InvalidOrderException {

        String parentstring = request.getParameter("parent");
        String walletId = request.getParameter("wallet");
        boolean productForNewCard = false;
        boolean productForExistingCard = false;

        if (parentstring != null && !parentstring.isEmpty()) {
            productForNewCard = true;
        } else if (walletId != null && !walletId.isEmpty()) {
            productForExistingCard = true;
        } else {
            return "redirect:/store/cards";
        }

        Byte parent = null;
        User user = usermanager.getUser(principal);
        Orders order = ordermanager.getCurrentOrCreate(user, session, OrdertypesType.SALES.getType());

        if (productForNewCard) {
            parent = new Byte(parentstring);
            attrs.addFlashAttribute("parent", parent);
        }

        if (productForExistingCard) {
            String cardNumber = walletManagementService.getPrintedId(walletId);
            Long id = Long.valueOf(walletId).longValue();
            Wallets wallet = walletsRepository.findByID(id);
            if (ordermanager.isCardBeingReplaced(user, wallet)) {
                return "redirect:/store/products";
            }
            Orderitems existingCard = ordermanager.getOrCreateOrderItemForExistingCard(order, wallet);
            if (ordermanager.retrieveOrderItem(existingCard.getId()) == null) {
                existingCard = ordermanager.updateOrderItemFarecode(existingCard, user);
                ordermanager.addOrderItem(order, existingCard, user, session);
            }
            parent = existingCard.getId().getOrderItemNumber();

            attrs.addFlashAttribute("cardnumber", cardNumber);
            attrs.addFlashAttribute("wallet", walletId);
        }

        String quantity = request.getParameter("quantity");
        short shortQuantity = ordermanager.validateQuantity(quantity);
        if (quantity.isEmpty() || quantity == null || shortQuantity <= 0) {
            if (productForNewCard) {
                return "redirect:/store/products?parent=" + parentstring;
            } else {
                return "redirect:/store/products?wallet=" + walletId;
            }
        }
        OrderitemsId parentId = null;
        short parentQuantity = 1;
        if (parent != null) {
            parentId = new OrderitemsId(order.getOrderId(), parent);
            parentQuantity = ordermanager.retrieveOrderItem(parentId).getQuantity();
        }
        Integer itemNumber = order.getOrderitemses().size();
        Offerings offer = offersRepository.findOne(new Byte(request.getParameter("offering")));
        Tickets ticket = offer.getTickets();
        BigDecimal price;
        BigDecimal amount;
        if (ticket.getDescription().equals("Variable Stored Value")) {
            shortQuantity = 1;
            price = new BigDecimal(request.getParameter("value"));
            amount = new BigDecimal(request.getParameter("value")).multiply(new BigDecimal(parentQuantity));
        } else {
            amount = ticket.getPrice().multiply(new BigDecimal(shortQuantity)).multiply(new BigDecimal(parentQuantity));
            price = ticket.getPrice();
        }
        Orderitems orderItem =
            new Orderitems(new OrderitemsId(order.getOrderId(), (itemNumber).byteValue()), OrderitemactionsType.NEW.getType(), OrderitemtypesType.TICKET.getType(), parent,
                parentQuantity, OrderItemStatusType.UNSUBMITTED.getStatus(), order, shortQuantity, price, amount);
        orderItem.setTickets(ticket);
        orderItem = ordermanager.updateOrderItemFarecode(orderItem, user);
        String success = ordermanager.checkRules(orderItem, order, user, session);
        if (success != null) {
            if (success.equals("itemError")) {
                attrs.addFlashAttribute("itemError", true);
            }
            else {
                attrs.addFlashAttribute("orderError", true);
            }

            if (productForNewCard) {
                return "redirect:/store/products?parent=" + parentstring;
            } else {
                return "redirect:/store/products?wallet=" + walletId;
            }
        }
        // The description must be retrieved for the confirmation page
        if (orderItem.getWallettypes() != null) {
            orderItem.getWallettypes().getDescription();
        }
        attrs.addFlashAttribute("item", orderItem);
        attrs.addFlashAttribute("order", order);
        attrs.addFlashAttribute("farecode", orderItem.getFarecodes());

        return "redirect:confirm";
    }

    // Selected a product to add to a card
    @RequestMapping(value = "/confirm", method = RequestMethod.GET)
    public String showConfirm(HttpSession session, ModelMap model) {
        if (!model.containsAttribute("item")) {
            return "redirect:/";
        }
        return "products/confirm";
    }

    // Show the zone map
    @RequestMapping(value = "/products/zones", method = RequestMethod.GET)
    public String showMap(@RequestParam(value = "parent", required = false) String parent, @RequestParam(value = "wallet", required = false) String walletId, ModelMap model,
        HttpSession session, RedirectAttributes attrs) {
        model.addAttribute("parent", parent);
        model.addAttribute("wallet", walletId);
        model.addAttribute("zones", zoneRepository.findAll());
        return "products/zoneMap";
    }

    // Show the shopping cart
    @RequestMapping(value = "/cart", method = RequestMethod.GET)
    public String getCart(@RequestParam(value = "parent", required = false) String parent, @RequestParam(value = "wallet", required = false) String walletId, Principal principal,
        HttpSession session, ModelMap model) {

        // instantiate appropriate credit service utils
        User user = usermanager.getUser(principal);

        Orders order = ordermanager.getCurrentOrCreate(user, session, OrdertypesType.SALES.getType());
        order.setOrderitemses(ordermanager.sortOrderItems(order)); // huh??

        if (ordermanager.orderRequiresShipping(order)) {
            model.addAttribute("Shipping", true);
        }
        model.addAttribute("wallettypeItem", OrderitemtypesType.WALLETTYPE.getType());
        model.addAttribute("walletItem", OrderitemtypesType.WALLET.getType());
        model.addAttribute("wallets", ordermanager.getWalletsForOrder(order));
        model.addAttribute("orderCount", ordermanager.orderItemsCount(order));
        model.addAttribute("user", user);
        model.addAttribute("order", order);
        model.addAttribute("replaceAction", OrderitemactionsType.REPLACE.getIndex());
        model.addAttribute("identifierType", IdentifiertypesType.PRINTED_ID.getIndex());
        if (parent != null && !parent.isEmpty()) {
            model.addAttribute("parent", parent);
        }

        if (walletId != null && !walletId.isEmpty()) {
            model.addAttribute("cardNumber", walletManagementService.getPrintedId(walletId));
            model.addAttribute("wallet", walletId);
        }

        model.addAttribute("creditServiceData", creditServiceService.getFormData(order, user, "/store/cart/success", null));
        model.addAttribute("postURL", tenantConfiguration.getCreditServicePostURL());
        model.addAttribute("rules", businessRulesService);

        return "cart/cart";
    }

    // Handle updates to the shopping cart
    @RequestMapping(value = "/cart", method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.MOVED_PERMANENTLY)
    public String updateCart(@Validated(value = { Shopping.class }) @ModelAttribute("order") Orders orders, BindingResult result, HttpServletRequest request,
        Principal principal, HttpSession session, ModelMap model, RedirectAttributes attr) {
        if (result.hasErrors() && (request.getParameter("update") != null || request.getParameter("ship") != null || request.getParameter("PaymentForm") != null)) {
            String parent = (String) request.getAttribute("parent");
            String walletId = (String) request.getAttribute("wallet");
            attr.addFlashAttribute("orderError", true);
            return "redirect:/store/cart";
        } else {
            User user = usermanager.getUser(principal);
            if (request.getParameter("update") != null) {
                orders = ordermanager.updateCart(orders, user, session);
            } else if (request.getParameter("ship") != null) {
                return "redirect:/store/cart/shipping";
            } else if (request.getParameter("PaymentForm") != null) {
                return "forward:/store/cart/shipping";
            }
            else {
                Orders currentorder = ordermanager.getCurrentOrder(user, session, OrdertypesType.SALES.getType());
                if (currentorder != null) {
                    if (request.getParameter("cancel") != null) {
                        ordermanager.removeAllOrderItems(currentorder, user, session);
                        return "redirect:/";
                    } else {
                        String toDelete = null;
                        Integer i = 0;
                        while (toDelete == null) {
                            if (request.getParameter("submit" + i.toString()) != null) {
                                toDelete = request.getParameter("submit" + i.toString());
                            }
                            i++;
                        }
                        Orderitems item = ordermanager.retrieveOrderItem(new OrderitemsId(currentorder.getOrderId(), new Byte(toDelete)));
                        ordermanager.removeOrderItem(currentorder, item, user, session);
                    }
                }
            }
        }
        return "redirect:/store/cart";
    }

    // Delete all items in the cart
    @RequestMapping(value = "/cart/delete", method = RequestMethod.POST)
    public String deleteAllItems(ModelMap model, Principal principal, HttpSession session) {
        User user = usermanager.getUser(principal);

        Orders order = ordermanager.getCurrentOrder(user, session, OrdertypesType.SALES.getType());
        if (order != null) {
            ordermanager.removeAllOrderItems(order, user, session);
        }
        model.addAttribute("rules", businessRulesService);

        return "redirect:/";
    }

    @RequestMapping(value = "/cart/success", method = RequestMethod.GET)
    public String updateCart(@ModelAttribute("order") Orders order, @ModelAttribute("shipping") Contact shipping, @ModelAttribute("billing") Contact billing, Principal principal,
        HttpSession session, ModelMap model, RedirectAttributes attrs) {

        if (order == null) {
            return "redirect:/store/cart";
        } else if (order.getOrderstatuses().getDescription().equals(OrderStatusType.HOLD.getStatus().getDescription())) {
            model.addAttribute("replaceOrder", true);
            model.addAttribute("cardnumber", walletManagementService.getPrintedId(order.getOrderitemses().get(0).getWallets().getWalletId().toString()));
        }

        User user = usermanager.getUser(principal);

        model.addAttribute("user", user);
        model.addAttribute("identifierType", IdentifiertypesType.PRINTED_ID.getIndex());
        return "cart/cartSuccess";
    }

    @RequestMapping(value = "/cart/cancel", method = RequestMethod.GET)
    public String cancel(Principal principal, HttpSession session, HttpServletRequest request) {
        return "cart/cartCancel";
    }

    @RequestMapping(value = "/cart/shipping", method = RequestMethod.GET)
    public String shipping(Principal principal, HttpSession session, HttpServletRequest request, ModelMap model) {

        SignupForm shipping_form = new SignupForm();
        User user = usermanager.getUser(principal);
        model.addAttribute("user", user);

        if (null != user) {
            shipping_form.setAddress(user.getAddress());
            shipping_form.setLastName(user.getLastName());
            shipping_form.setFirstName(user.getFirstName());
        }

        model.addAttribute("shippingForm", shipping_form);

        Orders order = ordermanager.getCurrentOrCreate(user, session, OrdertypesType.SALES.getType());
        order.setOrderitemses(ordermanager.sortOrderItems(order));
        model.addAttribute("order", order);
        model.addAttribute("orderCount", ordermanager.orderItemsCount(order));

        model.addAttribute("shippers", shippersRepository.findAll());
        Iterable<Shippers> shippers = shippersRepository.findAll();
        Shippers os = order.getShippers();
        for (Shippers shipper : shippers) {
            if (null != os && os.getShipperId() == shipper.getShipperId()) {
                model.addAttribute("selectedShipper", shipper);
            }
        }

        model.addAttribute("creditServiceData", creditServiceService.getFormData(order, user, "/store/cart/success", null));
        model.addAttribute("postURL", tenantConfiguration.getCreditServicePostURL());

        return "cart/cartShipping";
    }

    @RequestMapping(value = "/cart/shipping", method = RequestMethod.POST)
    public String shippingPost(Principal principal, HttpSession session, HttpServletRequest request, ModelMap model) {
        SignupForm shipping_form = new SignupForm();
        User user = usermanager.getUser(principal);
        model.addAttribute("user", user);
        String country = null;
        if (request.getParameter("address.country") != null) {
            country = request.getParameter("address.country").toString();
        }
        if ((null != user) || (request.getParameter("update") != null)) {
            Address a1 = new Address();
            a1.setLine1(request.getParameter("address.line1"));
            a1.setLine2(request.getParameter("address.line2"));
            a1.setCity(request.getParameter("address.city"));
            a1.setState(request.getParameter("address.state"));
            a1.setPostalCode(request.getParameter("address.postalCode"));
            a1.setCountry(country);
            shipping_form.setAddress(a1);
            shipping_form.setLastName(request.getParameter("lastName"));
            shipping_form.setFirstName(request.getParameter("firstName"));
        }

        model.addAttribute("shippingForm", shipping_form);

        Orders order = ordermanager.getCurrentOrCreate(user, session, OrdertypesType.SALES.getType());

        order.setOrderitemses(ordermanager.sortOrderItems(order));
        model.addAttribute("order", order);
        // @formatter:off
        String jsonString = new JSONObject()
            .put("Type", "Shipping Address")
            .put("firstName", request.getParameter("firstName"))
            .put("lastName", request.getParameter("lastName"))
            .put("Address",
                new JSONObject()
                    .put("line1",
                        request.getParameter("address.line1"))
                    .put("line2",
                        request.getParameter("address.line2"))
                    .put("city",
                        request.getParameter("address.city"))
                    .put("state",
                        request.getParameter("address.state"))
                    .put("postalCode",
                        request.getParameter("address.postalCode"))
                    .put("country", country)).toString();
        order.setOrderData(jsonString);
        // @formatter:on

        String shippingMethod = request.getParameter("method");
        Iterable<Shippers> shippers = shippersRepository.findAll();
        model.addAttribute("shippers", shippersRepository.findAll());
        for (Shippers shipper : shippers) {
            if (shipper.getDescription().equals(shippingMethod) && ordermanager.orderRequiresShipping(order)) {
                order.setShippers(shipper);
                model.addAttribute("selectedShipper", shipper);
            }
        }

        ordermanager.saveOrder(order);
        model.addAttribute("creditServiceData", creditServiceService.getFormData(order, user, "/store/cart/success", null));
        model.addAttribute("postURL", tenantConfiguration.getCreditServicePostURL());

        if (request.getParameter("update") != null) {
            return "cart/cartShipping";
        } else {
            model.addAllAttributes(request.getParameterMap());
            return "cart/cartShippingAutopost";
        }
    }

    @RequestMapping(value = "/cart/shippingautopost", method = RequestMethod.POST)
    public String shippingPostAuto(Principal principal, HttpSession session, HttpServletRequest request, ModelMap model) {
        User user = usermanager.getUser(principal);

        Orders order = ordermanager.getCurrentOrCreate(user, session, OrdertypesType.SALES.getType());
        order.setOrderitemses(ordermanager.sortOrderItems(order));

        model.addAttribute("creditServiceData", creditServiceService.getFormData(order, user, "/store/cart/success", null));
        model.addAttribute("postURL", tenantConfiguration.getCreditServicePostURL());
        model.addAllAttributes(request.getParameterMap());
        return "cart/cartShippingAutopost";
    }
}
