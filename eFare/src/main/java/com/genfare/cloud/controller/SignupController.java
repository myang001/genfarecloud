package com.genfare.cloud.controller;

import java.security.Principal;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.thymeleaf.context.WebContext;

import com.genfare.cloud.form.SignupForm;
import com.genfare.cloud.model.entity.repository.AuthenticationtypesRepository;
import com.genfare.cloud.model.entity.repository.LanguagesRepository;
import com.genfare.cloud.model.security.User;
import com.genfare.cloud.model.security.User.UserBuilder;
import com.genfare.cloud.model.service.EmailService;
import com.genfare.cloud.model.service.HashingService;
import com.genfare.cloud.model.service.UserManagementService;
import com.genfare.cloud.model.types.FarecodesType;
import com.genfare.cloud.model.types.GroupsBaseType;
import com.genfare.cloud.model.types.LanguagesType;

@Controller
@RequestMapping("/signup")
public class SignupController {

    private static final Logger LOGGER = LoggerFactory.getLogger(SignupController.class);

    @Autowired
    private UserManagementService usermanager;

    @Autowired
    private HashingService hasher;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private EmailService emailservice;

    @Autowired
    private LanguagesRepository languagesRepository;

    @Autowired
    private AuthenticationtypesRepository authenticationtypesRepository;

    @RequestMapping(method = RequestMethod.GET)
    public String getSignupForm(Principal principal, ModelMap model) {
        if (principal != null) {
            return "/user/main";
        }

        model.addAttribute("securityquestions", authenticationtypesRepository.findSecurityQuestions());
        model.addAttribute("signupForm", new SignupForm());
        return "signup/signup";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String registerUser(@Valid SignupForm signupForm, BindingResult result, ModelMap model, Locale locale, HttpServletRequest request,
        HttpServletResponse response) {

        if (result.hasErrors()) {
            LOGGER.debug("Errors Registering User");
            signupForm.setUsername(null);
            signupForm.setPassword1(null);
            signupForm.setPassword2(null);
            model.addAttribute("securityquestions", authenticationtypesRepository.findSecurityQuestions());
            return "signup/signup";
        }

        if (signupForm.getUsername() == null || signupForm.getUsername().isEmpty()) {
            signupForm.setUsername(signupForm.getEmail().getAddress());
        }

        UserBuilder builder = signupForm.getUserBuilder()
            .language(LanguagesType.ENGLISH);

        User user = builder.build();
        user.getFarecodes().add(FarecodesType.FULL.getType());
        user.getGroups().add(GroupsBaseType.RIDER.getType());

        user = usermanager.saveUser(user);
        usermanager.savePassword(user, signupForm.getPassword2());
        usermanager.saveSecurityQuestion(user, signupForm.getSecurityQuestion(), signupForm.getSecurityAnswer());
        emailservice.setContext(new WebContext(request, response, request.getServletContext()));
        emailservice.generateVerificationToken(user);
        model.addAttribute("email", user.getEmail().getAddress());
        return "email/thanksForRegistering";
    }
}
