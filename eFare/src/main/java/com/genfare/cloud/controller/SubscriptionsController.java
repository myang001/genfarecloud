package com.genfare.cloud.controller;

import java.security.Principal;
import java.util.Date;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.genfare.cloud.integration.producers.CreateOrderProducer;
import com.genfare.cloud.model.converters.DateToStringConverter;
import com.genfare.cloud.model.entity.Orders;
import com.genfare.cloud.model.entity.Peoplecards;
import com.genfare.cloud.model.entity.PeoplecardsId;
import com.genfare.cloud.model.entity.Subscriptions;
import com.genfare.cloud.model.entity.Tickets;
import com.genfare.cloud.model.entity.Walletcontents;
import com.genfare.cloud.model.entity.repository.PeoplecardsRepository;
import com.genfare.cloud.model.entity.repository.SubscriptionsRepository;
import com.genfare.cloud.model.entity.repository.WalletcontentsRepository;
import com.genfare.cloud.model.exception.InvalidOrderException;
import com.genfare.cloud.model.security.User;
import com.genfare.cloud.model.service.CreditCardManagementService;
import com.genfare.cloud.model.service.CreditServiceService;
import com.genfare.cloud.model.service.SubscriptionService;
import com.genfare.cloud.model.service.UserManagementService;
import com.genfare.cloud.model.service.WalletContentUtilityService;
import com.genfare.cloud.model.service.WalletManagementService;
import com.genfare.cloud.model.tenant.TenantConfiguration;

@Controller
@RequestMapping("/subscription")
public class SubscriptionsController {

    private static final Logger LOGGER = LoggerFactory.getLogger(SubscriptionsController.class);

    private DateToStringConverter dateToString = new DateToStringConverter();

    @Autowired
    private WalletcontentsRepository walletcontentsRepository;

    @Autowired
    private WalletManagementService walletManagementService;

    @Autowired
    private UserManagementService usermanager;

    @Autowired
    private PeoplecardsRepository peopleCardsRepository;

    @Autowired
    private SubscriptionsRepository subscriptionsRepository;

    @Autowired
    private CreditCardManagementService creditCardService;

    @Autowired
    private SubscriptionService subscriptionService;

    @Autowired
    private CreditServiceService creditServiceService;

    @Autowired
    private TenantConfiguration tenantConfig;

    @Autowired
    private CreateOrderProducer createOrderProducer;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private WalletContentUtilityService walletContentUtilityService;

    @RequestMapping(method = RequestMethod.GET)
    public String setup(@RequestParam(value = "walletcontents", required = true) String walletContentsId, Principal principal, ModelMap model) {
        User user = usermanager.getUser(principal);
        if (!walletManagementService.hasValidContentsId(user, walletContentsId))
            throw new IllegalArgumentException("invalid walletContentsId " + walletContentsId);

        Walletcontents product = walletcontentsRepository.findOne(new Long(walletContentsId));
        model.addAttribute("options", subscriptionService.getWhenOptions(product));
        model.addAttribute("expires", dateToString.convert(product.getWallets().getDateExpires()));
        model.addAttribute("product", product);
        model.addAttribute("cardNumber",
            walletManagementService.getPrintedId(String.valueOf(product.getWallets().getWalletId())));
        model.addAttribute("walletContentUtilityService", walletContentUtilityService);
        return "subscriptions/setupSubscription";
    }

    @RequestMapping(value = "/payment", method = RequestMethod.GET)
    public String payment(@ModelAttribute("userData") String userData, HttpServletRequest request, ModelMap model, Principal principal, HttpSession session) {

        Walletcontents product = null;
        String when = null;
        String expires = null;

        if (userData != null && !userData.isEmpty()) {
            String[] parts = userData.split(":");
            product = walletcontentsRepository.findOne(Long.valueOf(parts[0]));
            when = parts[1];
            expires = parts[2];
            model.addAttribute("newCard", true);
        } else {

            String walletContentsId = request.getParameter("walletcontents");
            if (walletContentsId == null) {
                return "redirect:/";
            }
            User user = usermanager.getUser(principal);
            if (!walletManagementService.hasValidContentsId(user, walletContentsId))
                throw new IllegalArgumentException("invalid walletContentsId " + walletContentsId);

            product = walletcontentsRepository.findOne(Long.valueOf(walletContentsId));
            // when = request.getParameter("when"); //TODO: Will be enabled for user selection
            when = walletContentUtilityService.getReplenishThreshold(product);
            expires = request.getParameter("expires");
            model.addAttribute("newCard", false);
        }

        if (product == null || when == null || expires == null) {
            return "redirect:/";
        }

        User user = usermanager.getUser(principal);
        Peoplecards card = creditCardService.getLastUsedCardByUser(user);

        if (card != null) {
            model.addAttribute("cardExpiration", creditCardService.getCardExpiration(card));
            model.addAttribute("cardType", card.getPaymenttypes().getDescription());
            model.addAttribute("imagePath", getImagePath(card.getPaymenttypes().getDescription()));
        }
        String builtUserData = product.getWalletContentsId().toString() + ":" + when + ":" + expires;

        model.addAttribute("postURL", tenantConfig.getCreditServicePostURL());
        model.addAttribute("creditServiceData",
            creditServiceService.getAuthOnlyFormData(user, "/subscription/payment", builtUserData));
        model.addAttribute("product", product);
        model.addAttribute("when", when);
        model.addAttribute("expires", expires);
        model.addAttribute("card", card);
        return "subscriptions/payment";
    }

    @RequestMapping(value = "/complete", method = RequestMethod.POST)
    public String getReceipt(Principal principal, ModelMap model, HttpServletRequest request) throws InvalidOrderException {

        String cardNumber = request.getParameter("cardNumber");
        String wc = request.getParameter("walletcontents");
        String expires = request.getParameter("expires");

        if (cardNumber == null || wc == null || expires == null) {
            return "redirect:/";
        }

        User user = usermanager.getUser(principal);
        Peoplecards card = peopleCardsRepository.findOne(new PeoplecardsId(user.getId(), new Byte(cardNumber)));
        Walletcontents product = walletcontentsRepository.findOne(Long.valueOf(wc));
        Tickets ticket = product.getTickets(); // TODO: Make user configurable?

        Orders order = subscriptionService.createSubscriptionOrder(user, card, product, expires,
            ticket.getReplenishPrice().toString(),
            ticket.getReplenishValue().toString(),
            ticket.getReplenishThreshold().toString());

        createOrderProducer.sendOrderNotification(tenantConfig.getSchemaName(), order);

        model.addAttribute("card", card);
        model.addAttribute("expires", expires);
        model.addAttribute("product", product);
        model.addAttribute("ticket", ticket);
        model.addAttribute("date", dateToString.convert(new Date()));
        model.addAttribute("cardExpiration", creditCardService.getCardExpiration(card));
        model.addAttribute("walletContentUtilityService", walletContentUtilityService);
        return "subscriptions/setupComplete";
    }

    @RequestMapping(value = "/details", method = RequestMethod.GET)
    public String getDetails(@RequestParam(value = "walletcontents", required = true) String walletContentsId, ModelMap model, Principal principal) {
        User user = usermanager.getUser(principal);

        Walletcontents product = walletcontentsRepository.findOne(new Long(walletContentsId));
        Subscriptions productSubscription =
            subscriptionsRepository.findByWalletContentsId(product.getWalletContentsId());
        Peoplecards card = productSubscription.getPeoplecards();

        String when = walletContentUtilityService.getReplenishThreshold(product);
        String expires = null;
        if (productSubscription.getDateReplenishExpires() != null) {
            expires = dateToString.convert(productSubscription.getDateReplenishExpires());
        }
        String userData = product.getWalletContentsId().toString() + ":" + when + ":" + expires;

        model.addAttribute("expires", expires);

        model.addAttribute("card", card);
        model.addAttribute("cardType", card.getPaymenttypes().getDescription());
        model.addAttribute("cardExpiration", creditCardService.getCardExpiration(card));
        model.addAttribute("imagePath", getImagePath(card.getPaymenttypes().getDescription()));

        model.addAttribute("creditServiceData",
            creditServiceService.getAuthOnlyFormData(user, "/subscription/payment", userData));
        model.addAttribute("postURL", tenantConfig.getCreditServicePostURL());
        model.addAttribute("newCard", false);

        model.addAttribute("subscriptionStatus", subscriptionService.getStatus(productSubscription));
        model.addAttribute("product", product);
        model.addAttribute("walletContentUtilityService", walletContentUtilityService);
        model.addAttribute("cardNumber",
            walletManagementService.getPrintedId(String.valueOf(product.getWallets().getWalletId())));
        return "subscriptions/details";
    }

    @RequestMapping(value = "/details", method = RequestMethod.POST)
    public String updateDetails(HttpServletRequest request, Principal principal, RedirectAttributes attributes) throws InvalidOrderException {

        String cardNumber = request.getParameter("cardNumber");
        String wc = request.getParameter("walletcontents");
        String expires = request.getParameter("expires");
        String status = request.getParameter("status");

        if (cardNumber == null || wc == null || expires == null || status == null) {
            return "redirect:/";
        }

        User user = usermanager.getUser(principal);
        Peoplecards card = peopleCardsRepository.findOne(new PeoplecardsId(user.getId(), new Byte(cardNumber)));
        Walletcontents product = walletcontentsRepository.findOne(Long.valueOf(wc));
        Subscriptions productSubscription =
            subscriptionsRepository.findByWalletContentsId(product.getWalletContentsId());

        Orders order = null;

        if (status.equals("On")) {
            order =
                subscriptionService.modifySubscriptionOrder(user, card, product, expires,
                    productSubscription.getReplenishPrice().toString(),
                    productSubscription.getReplenishValue().toString(),
                    productSubscription.getReplenishThreshold().toString());
        } else {
            order =
                subscriptionService.cancelSubscriptionOrder(user, card, product, expires,
                    productSubscription.getReplenishPrice().toString(),
                    productSubscription.getReplenishValue().toString(),
                    productSubscription.getReplenishThreshold().toString());
        }

        createOrderProducer.sendOrderNotification(tenantConfig.getSchemaName(), order);

        attributes.addFlashAttribute("message", "Subscription successfully updated");
        return "redirect:/";
    }

    private String getImagePath(String paymentType) {
        String imagePath = null;

        if (paymentType.matches("(.*)" + messageSource.getMessage("card.amex.title", null, Locale.US) + "(.*)")) {
            imagePath = messageSource.getMessage("card.amex.image_path", null, Locale.US);
        } else if (paymentType.matches("(.*)" + messageSource.getMessage("card.visa.title", null, Locale.US) + "(.*)")) {
            imagePath = messageSource.getMessage("card.visa.image_path", null, Locale.US);
        } else if (paymentType.matches("(.*)" + messageSource.getMessage("card.mastercard.title", null, Locale.US)
            + "(.*)")) {
            imagePath = messageSource.getMessage("card.mastercard.image_path", null, Locale.US);
        } else {
            imagePath = messageSource.getMessage("card.default.image_path", null, Locale.US);
        }

        return imagePath;
    }
}
