package com.genfare.cloud.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

import com.genfare.cloud.model.dto.UserDetails;
import com.genfare.cloud.model.entity.Orders;
import com.genfare.cloud.model.security.User;
import com.genfare.cloud.model.service.OrderManagementService;
import com.genfare.cloud.model.service.UserManagementService;
import com.genfare.cloud.model.types.OrdertypesType;

@ControllerAdvice
public class GlobalDefaultExceptionHandler {
    private static final Logger logger = LoggerFactory.getLogger(GlobalDefaultExceptionHandler.class);

    public static final String DEFAULT_ERROR_VIEW = "error/error";

    @Autowired
    OrderManagementService orderManagementService;

    @Autowired
    UserManagementService userManagementService;

    @ExceptionHandler(value = Exception.class)
    public ModelAndView defaultErrorHandler(HttpServletRequest req, Exception e)
        throws Exception {

        String id = "" + System.currentTimeMillis();
        logger.error("defaultErrorHandler ID=" + id + " Error=" + e + " URL=" + req.getRequestURL());
        logger.error("defaultErrorHandler ID=" + id, e);

        if (AnnotationUtils.findAnnotation(e.getClass(), ResponseStatus.class) != null)
            throw e;

        ModelAndView mav = new ModelAndView();
        mav.addObject("ExceptionID", "Exception ID: " + id);
        mav.addObject("ExceptionReason", "Exception Reason: " + e.getMessage());
        mav.setViewName(DEFAULT_ERROR_VIEW);
        postHandle(req, mav);
        return mav;
    }

    // com.genfare.cloud.model.interceptor.ModelHandlerInterceptor
    private void postHandle(HttpServletRequest request, ModelAndView mav) {
        try {
            User user = null;
            if (SecurityContextHolder.getContext().getAuthentication().getPrincipal() != "anonymousUser") {
                user =
                    userManagementService.getUser(((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername());
                if (user != null) {
                    String name = user.getFirstName() + " " + user.getLastName();
                    mav.getModelMap().addAttribute("name", name);
                }
            }
            HttpSession session = request.getSession();
            Orders order = orderManagementService.getCurrentOrder(user, session, OrdertypesType.SALES.getType());
            if (order != null && user != null) {
                String numOfItems = orderManagementService.orderItemsCount(order).toString();
                mav.getModelMap().addAttribute("numOfItems", numOfItems);
            }
        } catch (Exception e) {
            mav.addObject("ExceptionReason", "Exception Reason: " + e.getMessage());
        }
    }

    @ExceptionHandler(EmailException.class)
    public ModelAndView handleCustomException(EmailException e) {

        ModelAndView model = new ModelAndView();
        model.addObject("ExceptionID", "Exception ID: " + "undefined");
        model.addObject("ExceptionReason", "Exception Reason: " + e.getMessage());
        model.setViewName(DEFAULT_ERROR_VIEW);
        model.addObject("exception", e);

        return model;

    }
}
