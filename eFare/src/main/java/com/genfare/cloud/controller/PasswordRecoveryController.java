package com.genfare.cloud.controller;

import java.util.HashMap;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.hibernate.validator.constraints.Email;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.thymeleaf.context.WebContext;

import com.genfare.cloud.form.PasswordRecoveryForm;
import com.genfare.cloud.model.entity.repository.AuthenticationRepository;
import com.genfare.cloud.model.security.User;
import com.genfare.cloud.model.service.EmailService;
import com.genfare.cloud.model.service.UserManagementService;

@Controller
@RequestMapping("/recovery")
public class PasswordRecoveryController {

    private static final Logger log = LoggerFactory.getLogger(PasswordRecoveryController.class);

    @Autowired
    private UserManagementService usermanager;

    @Autowired
    private EmailService emailservice;

    @Autowired
    private AuthenticationRepository authenticationRepository;

    @RequestMapping(method = RequestMethod.GET)
    public String passwordRecovery(ModelMap model) {
        model.addAttribute("email", "");
        return "recovery/email";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String sendPasswordREcoveryEmail(@RequestParam(value = "email", required = true) @Valid @Email String email, Model model, HttpServletRequest request,
        HttpServletResponse response) {
        try {
            User user = usermanager.getUser(email);
            if (user == null) {
                throw new EntityNotFoundException();
            }
            log.debug("Emailing recovery to " + user.getFirstName() + " " + user.getLastName());
            emailservice.setContext(new WebContext(request, response, request.getServletContext()));
            emailservice.generatePasswordRecoveryVerificationToken(user);
            model.addAttribute("recovery", "password_recovery.send_email");
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", "password_recovery.error");
        }
        model.addAttribute("email", email);
        return "recovery/email";
    }

    @RequestMapping(value = "/reset", method = RequestMethod.GET)
    public String verify(@RequestParam(value = "token", required = false, defaultValue = "") String token, PasswordRecoveryForm passwordRecoveryForm) {
        if (token == null || token.isEmpty()) {
            return "redirect:/";
        }
        if (emailservice.verifyPasswordRecoveryToken(token)) {
            passwordRecoveryForm.setToken(token);
            return "recovery/changePassword";
        }
        return "redirect:/";
    }

    @RequestMapping(value = "/reset", method = RequestMethod.POST)
    public String resetPassword(@Valid PasswordRecoveryForm passwordRecoveryForm, BindingResult result, ModelMap model) {
        String token = passwordRecoveryForm.getToken();
        if (result.hasErrors()) {
            return "recovery/changePassword";
        }
        if (emailservice.verifyPasswordRecoveryToken(token)) {
            User user = usermanager.updatePasswordUsingRecoveryToken(token, passwordRecoveryForm.getPassword2());
            emailservice.sendEmail(user, "Password Recovery Notification", "passwordRecoveryNotification", new HashMap<String, Object>());
            model.addAttribute("success", "password_recovery.success");
            return "recovery/changePassword";
        }
        model.addAttribute("error2", "password_recovery.error");
        return "recovery/changePassword";
    }

}
