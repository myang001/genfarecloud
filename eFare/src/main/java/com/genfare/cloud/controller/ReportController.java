package com.genfare.cloud.controller;

import java.security.Principal;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.genfare.cloud.model.service.UserManagementService;
import com.genfare.cloud.reports.base.AbstractReport;
import com.genfare.cloud.reports.service.ReportRegistry;

/**
 * @author jyore
 */
@Controller
@RequestMapping(value = "/report")
public class ReportController {

    @PersistenceContext
    EntityManager em;

    @Autowired
    ReportRegistry reportRegistry;

    @Autowired
    UserManagementService userManager;

    @RequestMapping(value = "/{report}", method = { RequestMethod.GET, RequestMethod.POST })
    public String getReportParams(@PathVariable("report") String reportName, ModelMap model, HttpServletRequest request, Principal principal) {
        AbstractReport report = reportRegistry.getReport(reportName);

        if (report == null) {
            return "redirect:/";
        }

        model.addAllAttributes(report.query(em, userManager.getUserId(principal), request));

        return report.getView();
    }
}