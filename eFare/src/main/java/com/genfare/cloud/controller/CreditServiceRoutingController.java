package com.genfare.cloud.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.genfare.cloud.integration.producers.CreateOrderProducer;
import com.genfare.cloud.model.entity.Creditcardactivity;
import com.genfare.cloud.model.entity.Orders;
import com.genfare.cloud.model.entity.Payments;
import com.genfare.cloud.model.entity.PaymentsId;
import com.genfare.cloud.model.entity.Paymenttypes;
import com.genfare.cloud.model.entity.Wallets;
import com.genfare.cloud.model.entity.repository.CreditcardactivityRepository;
import com.genfare.cloud.model.entity.repository.PaymentsRepository;
import com.genfare.cloud.model.entity.repository.PaymenttypesRepository;
import com.genfare.cloud.model.entity.repository.WalletidentifiersRepository;
import com.genfare.cloud.model.exception.InvalidOrderException;
import com.genfare.cloud.model.security.User;
import com.genfare.cloud.model.service.CreditServiceService;
import com.genfare.cloud.model.service.EmailService;
import com.genfare.cloud.model.service.OrderManagementService;
import com.genfare.cloud.model.service.SubscriptionService;
import com.genfare.cloud.model.service.UserManagementService;
import com.genfare.cloud.model.tenant.TenantConfiguration;
import com.genfare.cloud.model.types.OrderItemStatusType;
import com.genfare.cloud.model.types.OrderPaymentStatusType;
import com.genfare.cloud.model.types.OrderitemactionsType;
import com.genfare.cloud.model.types.OrdertypesType;
import com.genfare.cloud.model.util.Contact;
import com.genfare.cloud.model.util.OrderPayment;

@Controller
@RequestMapping("/creditservice")
public class CreditServiceRoutingController {

    private static final Logger LOGGER = LoggerFactory.getLogger(CreditServiceRoutingController.class);

    @Autowired
    private CreditServiceService creditServiceService;

    @Autowired
    private TenantConfiguration tenantConfiguration;

    @Autowired
    private UserManagementService usermanager;

    @Autowired
    private OrderManagementService ordermanager;

    @Autowired
    private CreateOrderProducer createOrderProducer;

    @Autowired
    private SubscriptionService subscriptionService;

    @Autowired
    private PaymenttypesRepository paymentTypesRepo;

    @Autowired
    private PaymentsRepository paymentRepository;

    @Autowired
    private CreditcardactivityRepository creditRepository;

    @Autowired
    private EmailService emailservice;

    @Autowired
    private WalletidentifiersRepository walletidentifiersRepository;

    @RequestMapping(method = RequestMethod.GET)
    public String get() {
        return "redirect:/";
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.MOVED_PERMANENTLY)
    public String post(Principal principal, RedirectAttributes attrs, HttpServletRequest request, HttpSession session,
        ModelMap model) throws InvalidOrderException {
        String url = "/";
        User user = usermanager.getUser(principal);

        if (creditServiceService.validatePost(request, tenantConfiguration.getCreditServiceRelayResponseKey())) {

            if (creditServiceService.isAuthOnly(request)) {

                if (user == null) {
                    throw new UnsupportedOperationException("Cannot preauth guest accounts");
                }

            } else {

                // Get payment status
                OrderPayment order = creditServiceService.getOrderPayment(request);

                // Get contact info
                Contact c = creditServiceService.getContact(request);

                // Update order status
                Orders o = ordermanager.getOrder(order.getOrderId());
                if (o.getOrdertypes().getDescription().equals(OrdertypesType.SERVICE.getType().getDescription()) &&
                    o.getOrderitemses().get(0).getOrderitemactions().getDescription().equals(OrderitemactionsType.REPLACE.getType().getDescription())) {
                    o = ordermanager.updateOrderStatuses(o, OrderPaymentStatusType.PAID, OrderItemStatusType.HOLD);
                    Wallets w = o.getOrderitemses().get(0).getWallets();
                    o = ordermanager.processReplaceOrder(user, session, o);
                    List<Orders> cancelOrders = new ArrayList<Orders>();
                    cancelOrders = subscriptionService.cancelSubscriptionForWalletOrder(user, w);
                    for (Orders cancel : cancelOrders) {
                        LOGGER.debug("Subscription Cancelled for wallet " + w.getWalletId());
                        createOrderProducer.sendOrderNotification(tenantConfiguration.getSchemaName(), cancel);
                    }
                } else {
                    o = ordermanager.updateOrderStatuses(o, OrderPaymentStatusType.PAID, OrderItemStatusType.SUBMITTED);
                }

                // Clear the cart
                session.removeAttribute("orderId");
                LOGGER.debug("updateCart Order id: " + order.getOrderId());

                // Send Create Order Message
                String schemaName = tenantConfiguration.getSchemaName();
                createOrderProducer.sendOrderNotification(schemaName, o);
                LOGGER.debug("sent Order notification");

                // Record Credit Card Activity
                Creditcardactivity creditActivity =
                    new Creditcardactivity(order.getTransactionId(), order.getRc(), order.getRcText(), new Date(),
                        order.getMaskedCreditCardNumber());

                creditRepository.save(creditActivity);

                // Record Payment
                LOGGER.debug("Payment type is: " + order.getPaymentType());
                Paymenttypes paymentType = paymentTypesRepo.getPaymenttypeByDescription(order.getPaymentType());
                Byte pti = 5;
                if (null != paymentType) {
                    pti = paymentType.getPaymentTypeId();
                }
                PaymentsId paymentid = new PaymentsId(o.getOrderId(), pti);
                Payments payment =
                    new Payments(paymentid, paymentType, o, order.getAmount(),
                        order.getReferenceNumber(), new Date(), creditActivity);
                paymentRepository.save(payment);
                Contact ship = null;
                if (ordermanager.orderRequiresShipping(o)) {
                    ship = ordermanager.getOrderShipping(o);
                    attrs.addFlashAttribute("shipping", ship);
                }

                attrs.addFlashAttribute("billing", c);
                attrs.addFlashAttribute("order", o);

                // Send Receipt
                if (user != null) {
                    Map<String, Object> params = new HashMap<String, Object>();
                    params.put("order", o);
                    params.put("billing", c);
                    params.put("shipping", ship);
                    params.put("date", new Date());
                    params.put("walletidentifiersRepository", walletidentifiersRepository);
                    emailservice.sendEmail(user, "Your Order", "receipt", params);
                }
            }

            if (user != null) {
                creditServiceService.createPeopleCard(user, request);
            }

            String userData = "";
            try {
                url = "/";
                String context = request.getParameter(creditServiceService.getForwardToParam());
                if (null != context) {
                    url = URLDecoder.decode(request.getParameter(creditServiceService.getForwardToParam()), "UTF-8");
                }
                userData = URLDecoder.decode(request.getParameter(creditServiceService.getUserDataField()), "UTF-8");
            } catch (UnsupportedEncodingException e) {
                LOGGER.error("Invalid URL Encoding", e);
            }

            attrs.addFlashAttribute("userData", userData);
        }

        return "redirect:" + url;
    }
}
