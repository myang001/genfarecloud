package com.genfare.cloud.form;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

public class SearchCardForm {

    @NotBlank(message = "{searchCard.card_number.blank}")
    @Length(max = 50, message = "{searchCard.card_number.length}")
    private String cardNumber;

    @Length(max = 4, message = "{searchCard.cvv_number.length}")
    private String cvvNumber;

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getCvvNumber() {
        return cvvNumber;
    }

    public void setCvvNumber(String cvvNumber) {
        this.cvvNumber = cvvNumber;
    }
}