package com.genfare.cloud.form;

import java.util.Date;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import com.genfare.cloud.model.annotation.EmailAvailable;
import com.genfare.cloud.model.contact.Address;
import com.genfare.cloud.model.contact.Email;
import com.genfare.cloud.model.contact.Phone;
import com.genfare.cloud.model.security.ContactMethodEnum;
import com.genfare.cloud.model.security.User;
import com.genfare.cloud.model.security.User.UserBuilder;
import com.genfare.cloud.model.types.AddresstypesType;
import com.genfare.cloud.model.types.LinktypesType;

public class EditUserForm {

    @Valid
    @EmailAvailable(message = "{register.email.exists}")
    private Email email;

    @NotBlank(message = "{register.firstname.blank}")
    @Length(max = 30, message = "{register.firstName.length}")
    private String firstName;

    @NotBlank(message = "{register.lastName.blank}")
    @Length(max = 30, message = "{register.lastName.length}")
    private String lastName;

    @Valid
    private Address address;

    @Valid
    private Phone phone;

    @Past(message = "{register.birthdate.future}")
    private Date dateOfBirth;

    @NotNull
    private Boolean alert;

    @NotNull(message = "{register.primary_contact_method.blank}")
    private ContactMethodEnum contactMethod;

    public EditUserForm() {
    }

    public EditUserForm(User user) {
        this.email = user.getEmail();
        this.firstName = user.getFirstName();
        this.lastName = user.getLastName();
        this.address = user.getAddress();
        this.phone = user.getPhone();
        this.dateOfBirth = user.getDateOfBirth();
        this.alert = user.getAlert();
        this.contactMethod = user.getPrimaryContactMethod();
    }

    public User updateUser(User user) {

        email.setDescription(LinktypesType.PERSONAL_EMAIL.getType().getDescription());
        address.setDescription(AddresstypesType.SHIPPING.getType().getDescription());
        phone.setDescription(LinktypesType.HOME_PHONE.getType().getDescription());

        // @formatter:off
        UserBuilder builder = new User.UserBuilder(
            user,
            this.email.getAddress(),
            this.firstName,
            this.lastName,
            this.email,
            this.phone,
            this.address
            )
                .alert(this.alert)
                .dateOfBirth(this.dateOfBirth)
                .primaryContactMethod(this.contactMethod);
        // @formatter:on

        return builder.build();
    }

    public Email getEmail() {
        return email;
    }

    public void setEmail(Email email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Phone getPhone() {
        return phone;
    }

    public void setPhone(Phone phone) {
        this.phone = phone;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public Boolean getAlert() {
        return alert;
    }

    public void setAlert(Boolean alert) {
        this.alert = alert;
    }

    public ContactMethodEnum getContactMethod() {
        return contactMethod;
    }

    public void setContactMethod(ContactMethodEnum contactMethod) {
        this.contactMethod = contactMethod;
    }

}
