package com.genfare.cloud.form;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import com.genfare.cloud.annotation.FieldMatch;

//import org.hibernate.validator.constraints.Email;

@FieldMatch.List({ @FieldMatch(first = "password1", second = "password2", message = "{register.password.match}") })
public class PasswordRecoveryForm {

    @Pattern(regexp = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*-_+=])(?=\\S+$).{8,40}$",
        message = "{register.password1.invalid}")
    @Length(max = 256, message = "{register.password1.length}")
    private String password1;

    private String password2;

    @NotBlank
    @NotNull
    private String token;

    public String getPassword1() {
        return password1;
    }

    public void setPassword1(String password1) {
        this.password1 = password1;
    }

    public String getPassword2() {
        return password2;
    }

    public void setPassword2(String password2) {
        this.password2 = password2;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

}
