package com.genfare.cloud.form;

import org.hibernate.validator.constraints.NotBlank;

import com.genfare.cloud.annotation.DateRange;

@DateRange.List({ @DateRange(begdate = "begdate", enddate = "enddate", message = "{report.more.3months}") })
public class ReportForm {

    @NotBlank(message = "{report.begdate.notblank}")
    // @DateRange (message = "{report.more.3months}")
    private String begdate;

    @NotBlank(message = "{report.enddate.notblank}")
    private String enddate;

    public ReportForm() {
    }

    public void setBegdate(String begdate) {
        this.begdate = begdate;
    }

    public String getBegdate() {
        return begdate;
    }

    public void setEnddate(String enddate) {
        this.enddate = enddate;
    }

    public String getEnddate() {
        return enddate;
    }
}
