package com.genfare.cloud.form;

import java.util.Date;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import com.genfare.cloud.annotation.FieldMatch;
import com.genfare.cloud.annotation.UserLoginAvailable;
import com.genfare.cloud.model.annotation.EmailAvailable;
import com.genfare.cloud.model.contact.Address;
import com.genfare.cloud.model.contact.Email;
import com.genfare.cloud.model.contact.Phone;
import com.genfare.cloud.model.security.ContactMethodEnum;
import com.genfare.cloud.model.security.User.UserBuilder;
import com.genfare.cloud.model.types.AddresstypesType;
import com.genfare.cloud.model.types.LinktypesType;

//import org.hibernate.validator.constraints.Email;

@FieldMatch.List({ @FieldMatch(first = "password1", second = "password2", message = "{register.password.match}") })
public class SignupForm {

    @UserLoginAvailable(message = "{register.user.exists}")
    @Length(max = 256, message = "{register.username.length}")
    private String username;

    @Pattern(regexp = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*-_+=])(?=\\S+$).{8,40}$",
        message = "{register.password1.invalid}")
    @Length(max = 256, message = "{register.password1.length}")
    private String password1;

    private String password2;

    @Valid
    @EmailAvailable(message = "{register.email.exists}")
    private Email email;

    @Length(max = 10, message = "{register.prefix.length}")
    private String prefix;

    @Length(max = 10, message = "{register.suffix.length}")
    private String suffix;

    @NotBlank(message = "{register.firstName.blank}")
    @Length(max = 30, message = "{register.firstName.length}")
    private String firstName;

    @Length(max = 30, message = "{register.middleName.length}")
    private String middleName;

    @NotBlank(message = "{register.lastName.blank}")
    @Length(max = 30, message = "{register.lastName.length}")
    private String lastName;

    @Past(message = "{register.birthdate.future}")
    private Date dateOfBirth;

    @Valid
    private Phone phone;

    @Valid
    private Address address;

    @NotBlank(message = "{register.securityQuestion.blank}")
    @Length(max = 256, message = "{register.securityQuestion.length}")
    private String securityQuestion;

    @NotBlank(message = "{register.securityAnswer.blank}")
    @Length(max = 256, message = "{register.securityAnswer.length}")
    private String securityAnswer;

    @NotNull
    private Boolean alert;

    @NotNull(message = "{register.primary_contact_method.blank}")
    private ContactMethodEnum primaryContactMethod;

    public SignupForm() {
        this.address = new Address(AddresstypesType.SHIPPING.getType().getDescription());
        this.alert = false;
        this.dateOfBirth = null;
        this.email = new Email(LinktypesType.PERSONAL_EMAIL.getType().getDescription());
        this.firstName = "";
        this.lastName = "";
        this.middleName = "";
        this.phone = new Phone(LinktypesType.HOME_PHONE.getType().getDescription());
        this.prefix = "";
        this.primaryContactMethod = ContactMethodEnum.EMAIL;
        this.suffix = "";
        this.username = "";
    }

    public UserBuilder getUserBuilder() {

        Integer id = null;
        // @formatter:off
        return new UserBuilder(
            id,
            this.username,
            this.firstName,
            this.lastName,
            this.email,
            this.phone,
            this.address)
            .alert(this.alert)
            .dateOfBirth(this.dateOfBirth)
            .middleName(this.middleName)
            .prefix(this.prefix)
            .suffix(this.suffix);
        // @formatter:on
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword1() {
        return password1;
    }

    public void setPassword1(String password1) {
        this.password1 = password1;
    }

    public String getPassword2() {
        return password2;
    }

    public void setPassword2(String password2) {
        this.password2 = password2;
    }

    public Email getEmail() {
        return email;
    }

    public void setEmail(Email email) {
        this.email = email;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public Phone getPhone() {
        return phone;
    }

    public void setPhone(Phone phone) {
        this.phone = phone;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getSecurityQuestion() {
        return securityQuestion;
    }

    public void setSecurityQuestion(String securityQuestion) {
        this.securityQuestion = securityQuestion;
    }

    public String getSecurityAnswer() {
        return securityAnswer;
    }

    public void setSecurityAnswer(String securityAnswer) {
        this.securityAnswer = securityAnswer;
    }

    public Boolean getAlert() {
        return alert;
    }

    public void setAlert(Boolean alert) {
        this.alert = alert;
    }

    public ContactMethodEnum getPrimaryContactMethod() {
        return primaryContactMethod;
    }

    public void setPrimaryContactMethod(ContactMethodEnum primaryContactMethod) {
        this.primaryContactMethod = primaryContactMethod;
    }

}
