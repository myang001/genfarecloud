package com.genfare.cloud.form;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

public class CardRegistrationForm {

    @NotBlank(message = "{cardRegistration.card_number.blank}")
    @Length(max = 50, message = "{cardRegistration.card_number.length}")
    private String cardNumber;

    @NotBlank(message = "{cardRegistration.cvv_number.blank}")
    @Length(max = 4, message = "{cardRegistration.cvv_number.length}")
    private String cvvNumber;

    @NotBlank(message = "{cardRegistration.nickname.blank}")
    @Length(max = 18, message = "{cardRegistration.nickname.length}")
    private String nickname;

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getCvvNumber() {
        return cvvNumber;
    }

    public void setCvvNumber(String cvvNumber) {
        this.cvvNumber = cvvNumber;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

}
