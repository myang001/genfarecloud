package com.genfare.cloud.reports.service;

import java.lang.reflect.InvocationTargetException;

import javax.annotation.PostConstruct;

import org.apache.commons.collections.map.CaseInsensitiveMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.type.filter.AnnotationTypeFilter;
import org.springframework.stereotype.Service;

import com.genfare.cloud.reports.annotation.Report;
import com.genfare.cloud.reports.base.AbstractReport;
import com.genfare.cloud.reports.exception.DuplicateReportException;

@Service
public class ReportRegistryImpl
    implements ReportRegistry {

    private static final Logger LOGGER = LoggerFactory.getLogger(ReportRegistryImpl.class);

    private static final CaseInsensitiveMap REPORTS = new CaseInsensitiveMap();

    @PostConstruct
    private void autoregister()
        throws ClassNotFoundException, NoSuchFieldException, SecurityException, IllegalAccessException,
        IllegalArgumentException, InvocationTargetException, NoSuchMethodException, InstantiationException,
        DuplicateReportException {
        ClassPathScanningCandidateComponentProvider scanner = new ClassPathScanningCandidateComponentProvider(true);

        scanner.addIncludeFilter(new AnnotationTypeFilter(Report.class));

        // TODO: Dynamic, full class-path, or list based scan instead of specific directory
        for (BeanDefinition bd : scanner.findCandidateComponents("com.genfare.cloud")) {
            Class<?> clazz = Class.forName(bd.getBeanClassName());
            Report r = (Report) clazz.getAnnotation(Report.class);

            if (r == null || !AbstractReport.class.isAssignableFrom(clazz)) {
                continue;
            }

            register(r.name(), clazz);
        }
    }

    @Override
    public AbstractReport getReport(String name) {
        return (AbstractReport) REPORTS.get(name);
    }

    @Override
    public void register(String name, Class<?> clazz)
        throws InstantiationException, IllegalAccessException, DuplicateReportException {
        if (REPORTS.containsKey(name)) {
            throw new DuplicateReportException(
                String.format("There is already a report registered with name: %s", name));
        }
        REPORTS.put(name, clazz.newInstance());

        LOGGER.info(String.format("Registered Report '%s' from class '%s'", name, clazz.getName()));
    }
}
