package com.genfare.cloud.reports.service;

import com.genfare.cloud.reports.base.AbstractReport;
import com.genfare.cloud.reports.exception.DuplicateReportException;

public interface ReportRegistry {

    AbstractReport getReport(String name);

    void register(String name, Class<?> clazz)
        throws InstantiationException, IllegalAccessException, DuplicateReportException;
}
