package com.genfare.cloud.reports.base;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public abstract class AbstractThreeMonthHistoricalReport
    extends AbstractReport {

    protected static final DateTimeFormatter formatter = DateTimeFormat.forPattern("MM/dd/yyyy");

    protected Boolean threeMonthConstrain(HttpServletRequest request, String beginKey, String endKey,
        Map<String, Object> params) {
        DateTime begDate, endDate;

        String beg = request.getParameter(beginKey);
        String end = request.getParameter(endKey);

        if (end == null || end.isEmpty()) {
            endDate = new DateTime().withTime(23, 59, 59, 999);
        } else {
            endDate = formatter.parseDateTime(end).withTime(23, 59, 59, 999);
        }

        if (beg == null || beg.isEmpty()) {
            begDate = DateTime.now().minusMonths(3).withTimeAtStartOfDay();
        } else {
            begDate = formatter.parseDateTime(beg).withTimeAtStartOfDay();
        }

        params.put("_starttime", begDate.toString("yyyy-MM-dd HH:mm:ss"));
        params.put("_endtime", endDate.toString("yyyy-MM-dd HH:mm:ss"));
        params.put(beginKey, begDate.toString("MM/dd/yyyy"));
        params.put(endKey, endDate.toString("MM/dd/yyyy"));

        // MUST BE:
        // - no later than today
        // - no earlier than 3 months prior
        // - begDate before endDate
        if (endDate.isAfter(DateTime.now().plusDays(1)) || begDate.isBefore(DateTime.now().minusMonths(3).minusDays(1))
            || endDate.isBefore(begDate)) {
            return false;
        }
        return true;
    }
}
