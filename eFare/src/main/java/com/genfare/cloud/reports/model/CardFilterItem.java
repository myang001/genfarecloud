package com.genfare.cloud.reports.model;

public class CardFilterItem {
    private String identifier;

    private String nickName;

    public CardFilterItem(String identifier, String nickName) {
        this.identifier = identifier;
        this.nickName = nickName;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getDisplayName() {
        return (this.nickName != null ? this.nickName : this.identifier);
    }
}
