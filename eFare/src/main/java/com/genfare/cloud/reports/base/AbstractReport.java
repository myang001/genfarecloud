package com.genfare.cloud.reports.base;

import java.util.Map;

import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletRequest;

public abstract class AbstractReport {

    protected String view;

    public String getView() {
        return view;
    }

    public abstract Map<String, Object> query(EntityManager em, Integer id, HttpServletRequest request);

}
