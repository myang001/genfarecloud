package com.genfare.cloud.reports;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletRequest;

import com.genfare.cloud.model.entity.view.OrderHistoryReport;
import com.genfare.cloud.reports.annotation.Report;
import com.genfare.cloud.reports.base.AbstractThreeMonthHistoricalReport;

@Report(name = "orderhistory")
public class OrderHistory
    extends AbstractThreeMonthHistoricalReport {

    public OrderHistory() {
        this.view = "report/orderhistory";
    }

    @Override
    @SuppressWarnings("unchecked")
    public Map<String, Object> query(EntityManager em, Integer id, HttpServletRequest request) {
        Map<String, Object> results = new HashMap<String, Object>(2);
        Map<String, Object> params = new HashMap<String, Object>(2);

        if (threeMonthConstrain(request, "begDate", "endDate", params)) {
            String query =
                String.format("FROM OrderHistoryReport r WHERE r.personId = %s AND r.dateOrdered BETWEEN '%s' AND  '%s'",
                    id, params.get("_starttime"), params.get("_endtime"));
            results.put("success", true);
            results.put("results", buildHeirarchy(em.createQuery(query).getResultList()));
        } else {
            results.put("success", false);
            results.put("results", null);
        }

        results.put("params", params);
        return results;
    }

    private List<Order> buildHeirarchy(List<OrderHistoryReport> results) {
        Map<Integer, Order> orderMap = new LinkedHashMap<Integer, Order>();

        for (OrderHistoryReport r : results) {
            if (!orderMap.containsKey(r.getId().getOrderId())) {
                orderMap.put(r.getId().getOrderId(),
                    new Order(r.getId().getOrderId(), r.getDateOrdered(), r.getStatus(), r.getAmountOrder()));
            }

            Order o = orderMap.get(r.getId().getOrderId());
            Map<Integer, OrderItemParent> parents = o.getItems();

            if (r.getParentItemNumber() == null
                || r.getParentItemNumber().equals(r.getId().getOrderItemNumber().intValue())) {
                // Parent
                String description =
                    r.getNickName() == null ? (r.getWalletDescription() == null ? "New "
                        + (r.getOfferingDescription() == null ? r.getWalletTypeDescription()
                            : r.getOfferingDescription()) : r.getWalletDescription()) : r.getNickName();
                String subDescription = r.getCardNumber();

                Boolean hasCost = false;
                if (description.contains("New") || r.getStatus().equals("Hold")) {
                    hasCost = true;
                }

                parents.put(r.getId().getOrderItemNumber().intValue(), new OrderItemParent(description, subDescription, r.getQuantity(),
                    r.getUnitPrice(), r.getAmount(), hasCost));

            } else {
                // Child
                OrderItemParent p = parents.get(r.getParentItemNumber());
                String description =
                    (r.getOfferingDescription() != null ? r.getOfferingDescription() : r.getTicketDescription());
                p.addChild(new OrderItemChild(description, r.getQuantity(), r.getUnitPrice(), r.getAmount()));
            }
        }

        return new ArrayList<Order>(orderMap.values());
    }

    public class Order {
        Integer orderId;

        Date dateOrdered;

        String status;

        BigDecimal amountOrder;

        Map<Integer, OrderItemParent> items = new LinkedHashMap<Integer, OrderItemParent>();

        public Order(Integer orderId, Date dateOrdered, String status, BigDecimal amountOrder) {
            this.orderId = orderId;
            this.dateOrdered = dateOrdered;
            this.status = status;
            this.amountOrder = amountOrder;
        }

        public void addItem(Integer id, OrderItemParent parent) {
            items.put(id, parent);
        }

        public Integer getOrderId() {
            return orderId;
        }

        public Date getDateOrdered() {
            return dateOrdered;
        }

        public String getStatus() {
            return status;
        }

        public BigDecimal getAmountOrder() {
            return amountOrder;
        }

        public Map<Integer, OrderItemParent> getItems() {
            return items;
        }

    }

    public class OrderItemParent {
        String description;

        String subDescription;

        Integer quantity;

        BigDecimal unitPrice;

        BigDecimal amount;

        Boolean hasCost;

        List<OrderItemChild> children = new ArrayList<OrderItemChild>();

        public OrderItemParent(String description, String subDescription, Integer quantity, BigDecimal unitPrice,
            BigDecimal amount, Boolean hasCost) {
            this.description = description;
            this.subDescription = subDescription;
            this.quantity = quantity;
            this.unitPrice = unitPrice;
            this.amount = amount;
            this.hasCost = hasCost;
        }

        public void addChild(OrderItemChild child) {
            children.add(child);
        }

        public String getDescription() {
            return description;
        }

        public String getSubDescription() {
            return subDescription;
        }

        public Integer getQuantity() {
            return quantity;
        }

        public BigDecimal getUnitPrice() {
            return unitPrice;
        }

        public BigDecimal getAmount() {
            return amount;
        }

        public List<OrderItemChild> getChildren() {
            return children;
        }

        public Boolean getHasCost() {
            return hasCost;
        }

        public void setHasCost(Boolean hasCost) {
            this.hasCost = hasCost;
        }
    }

    public class OrderItemChild {
        String description;

        Integer quantity;

        BigDecimal unitPrice;

        BigDecimal amount;

        public OrderItemChild(String description, Integer quantity, BigDecimal unitPrice, BigDecimal amount) {
            this.description = description;
            this.quantity = quantity;
            this.unitPrice = unitPrice;
            this.amount = amount;
        }

        public String getDescription() {
            return description;
        }

        public Integer getQuantity() {
            return quantity;
        }

        public BigDecimal getUnitPrice() {
            return unitPrice;
        }

        public BigDecimal getAmount() {
            return amount;
        }
    }
}
