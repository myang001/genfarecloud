package com.genfare.cloud.reports.exception;

public class DuplicateReportException
    extends Exception {

    private static final long serialVersionUID = 1L;

    public DuplicateReportException(String message) {
        super(message);
    }
}
