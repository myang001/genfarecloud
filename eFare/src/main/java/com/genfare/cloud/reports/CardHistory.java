package com.genfare.cloud.reports;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletRequest;

import com.genfare.cloud.model.entity.view.CardHistoryReport;
import com.genfare.cloud.reports.annotation.Report;
import com.genfare.cloud.reports.base.AbstractThreeMonthHistoricalReport;
import com.genfare.cloud.reports.model.CardFilterItem;

@Report(name = "cardhistory")
public class CardHistory
    extends AbstractThreeMonthHistoricalReport {

    public CardHistory() {
        this.view = "report/cardhistory";
    }

    @SuppressWarnings("unchecked")
    @Override
    public Map<String, Object> query(EntityManager em, Integer id, HttpServletRequest request) {
        Map<String, Object> results = new HashMap<String, Object>(2);
        Map<String, Object> params = new HashMap<String, Object>(2);

        String activeCard = request.getParameter("cardFilter");

        if (threeMonthConstrain(request, "begDate", "endDate", params)) {
            String cardFilter;

            if (activeCard == null || activeCard.isEmpty() || activeCard.equals("All")) {
                cardFilter = activeCard = "";
            } else {
                cardFilter = String.format(" AND r.cardNumber = %s", activeCard);
            }

            String query =
                String.format("FROM CardHistoryReport r WHERE r.personId = %s%s AND r.id.dateTime BETWEEN '%s' AND  '%s'",
                    id, cardFilter, (String) params.get("_starttime"), (String) params.get("_endtime"));

            results.put("success", true);
            results.put("results", ((List<CardHistoryReport>) em.createQuery(query).getResultList()));
        } else {
            results.put("success", false);
            results.put("results", null);
        }

        params.put("cardFilter", getCards(em, id));
        params.put("activeCard", activeCard);
        results.put("params", params);
        return results;
    }

    @SuppressWarnings("unchecked")
    private List<CardFilterItem> getCards(EntityManager em, Integer id) {
        String query =
            String
                .format(
                    "select new com.genfare.cloud.reports.model.CardFilterItem(wi.identifier,w.nickname) from Walletidentifiers wi join wi.wallets w where w.people.personId = %s and wi.identifiertypes.identifierId <> 1",
                    id);
        return (List<CardFilterItem>) em.createQuery(query).getResultList();
    }
}
