package com.genfare.cloud.config;

import org.springframework.core.annotation.Order;
import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * @author martin
 */
@Order(1)
public class SecurityWebApplicationInitializer
    extends AbstractSecurityWebApplicationInitializer {

}
