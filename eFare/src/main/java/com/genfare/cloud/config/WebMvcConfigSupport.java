package com.genfare.cloud.config;

import org.springframework.context.annotation.Configuration;

import com.genfare.cloud.config.mvc.AbstractBaseWebMvcConfigurationSupport;

@Configuration
public class WebMvcConfigSupport extends AbstractBaseWebMvcConfigurationSupport {

}
