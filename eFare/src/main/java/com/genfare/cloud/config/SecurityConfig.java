package com.genfare.cloud.config;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.servlet.configuration.EnableWebMvcSecurity;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.web.util.matcher.RegexRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.social.security.SpringSocialConfigurer;

import com.genfare.cloud.config.security.AbstractBaseWebSecurityConfigurerAdapter;
import com.genfare.cloud.model.exception.EmailNotVerifiedException;
import com.genfare.cloud.model.service.LoginFailureHandler;

/**
 * @author martin
 */
@Configuration
@EnableWebMvcSecurity
public class SecurityConfig
    extends AbstractBaseWebSecurityConfigurerAdapter {

    /*
     * @Override public void configure(WebSecurity web) throws Exception { web // Spring Security ignores request to static
     * resources such as CSS or JS // files. .ignoring().antMatchers("/resources/**"); }
     */

    @Override
    // @formatter:off
        protected
        void configure(HttpSecurity http)
            throws Exception {
        super.configure(http);
        http.authorizeRequests().antMatchers("/admin/**")
            .hasRole("ADMIN")
            .antMatchers("/card/register", "/report/**", "/subscription/**")
            .hasAnyRole("USER", "RIDER")
            .antMatchers("/",
                "/faq",
                "/signup/**",
                "/verify/**",
                "/card/**",
                "/store/**",
                "/report/**",
                "/creditservice",
                "/recovery/**")
            .permitAll()
            .and().formLogin().defaultSuccessUrl("/")
            .and().logout()
            .deleteCookies("remember-me");

        // TODO: This is not a nice and secure key
        http.rememberMe().key("enteraniceandsecurekeytoencryptwith");
        http.apply(new SpringSocialConfigurer());

        http.csrf().requireCsrfProtectionMatcher(new RequestMatcher() {
            private final Pattern allowedMethods =
                Pattern.compile("^(GET|HEAD|TRACE|OPTIONS)$");

            private final RegexRequestMatcher apiMatcher = new RegexRequestMatcher("/creditservice", null);

            @Override
            public boolean matches(HttpServletRequest request) {
                // No CSRF due to allowedMethod
                if (allowedMethods.matcher(request.getMethod()).matches())
                    return false;

                // No CSRF due to credit service return call
                if (apiMatcher.matches(request))
                    return false;

                // CSRF for everything else that is not an API call or an allowedMethod
                return true;
            }
        });

        // -1 = unlimited logins
        http.sessionManagement().maximumSessions(-1).sessionRegistry(sessionRegistry());
    }

    @Bean
    public SessionRegistry sessionRegistry() {
        return new SessionRegistryImpl();
    }

    @Override
    @Bean
    public LoginFailureHandler authenticationFailureHandler() {
        LoginFailureHandler handler = new LoginFailureHandler();
        Map<String, String> map = new HashMap<String, String>();
        map.put(BadCredentialsException.class.getName(), "/login?error=Incorrect%20email%20or%20password");
        map.put(EmailNotVerifiedException.class.getName(), "/verify");
        map.put(DisabledException.class.getName(), "/login?error=Account%20disable");
        handler.setExceptionMappings(map);
        handler.setDefaultFailureUrl("/login?error");

        return handler;
    }
}
