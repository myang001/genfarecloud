package com.genfare.cloud.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.security.crypto.encrypt.Encryptors;
import org.springframework.social.UserIdSource;
import org.springframework.social.config.annotation.ConnectionFactoryConfigurer;
import org.springframework.social.config.annotation.EnableSocial;
import org.springframework.social.config.annotation.SocialConfigurer;
import org.springframework.social.connect.ConnectionFactoryLocator;
import org.springframework.social.connect.ConnectionRepository;
import org.springframework.social.connect.UsersConnectionRepository;
import org.springframework.social.connect.jdbc.JdbcUsersConnectionRepository;
import org.springframework.social.connect.web.ConnectController;
import org.springframework.social.security.AuthenticationNameUserIdSource;
import org.springframework.social.twitter.connect.TwitterConnectionFactory;

import com.genfare.cloud.model.tenant.DetectedDomain;
import com.genfare.cloud.repository.MultiTenantJdbcUsersConnectionRepository;

/**
 * @author martin
 */
@Configuration
@EnableSocial
public class SocialContext
    implements SocialConfigurer {

    @Autowired
    private DataSource dataSource;

    @Autowired
    private DetectedDomain detectedDomain;

    /**
     * Configures the connection factory for Twitter.
     * 
     * @param cfConfig
     * @param env
     */
    @Override
    public void addConnectionFactories(ConnectionFactoryConfigurer cfConfig, Environment env) {
        cfConfig.addConnectionFactory(new TwitterConnectionFactory(env.getProperty("twitter.consumerKey"),
            env.getProperty("twitter.consumerSecret")));

    }

    /**
     * The UserIdSource determines the account ID of the user. The application uses the email as the account ID.
     */
    @Override
    public UserIdSource getUserIdSource() {
        return new AuthenticationNameUserIdSource();
    }

    @Override
    public UsersConnectionRepository getUsersConnectionRepository(ConnectionFactoryLocator connectionFactoryLocator) {
        // JdbcUsersConnectionRepository jdbcUsersConnectionRepository = new JdbcUsersConnectionRepository(
        // dataSource,
        // connectionFactoryLocator,
        // /**
        // * The TextEncryptor object encrypts the authorization details of the connection. In
        // * our example, the authorization details are stored as plain text.
        // * DO NOT USE THIS IN PRODUCTION.
        // */
        // Encryptors.noOpText()
        // );
        // jdbcUsersConnectionRepository.setTablePrefix("albany.");
        JdbcUsersConnectionRepository jdbcUsersConnectionRepository =
            new MultiTenantJdbcUsersConnectionRepository(dataSource, connectionFactoryLocator, Encryptors.noOpText());
        return jdbcUsersConnectionRepository;
    }

    /**
     * This bean manages the connection flow between the account provider and the example application.
     */
    @Bean
    public ConnectController connectController(ConnectionFactoryLocator connectionFactoryLocator,
        ConnectionRepository connectionRepository) {
        return new ConnectController(connectionFactoryLocator, connectionRepository);
    }
}
