package com.genfare.cloud.config;

import org.springframework.core.annotation.Order;

import com.genfare.cloud.config.mvc.AbstractBaseWebAppInitializer;
import com.genfare.cloud.integration.config.IntegrationContext;

@Order(2)
public class WebAppInitializer
    extends AbstractBaseWebAppInitializer {

    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class<?>[] { ApplicationContext.class, WebMvcConfig.class, SecurityConfig.class,
            SocialContext.class, IntegrationContext.class };
    }

    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class<?>[] { WebMvcConfig.class };
    }

}
