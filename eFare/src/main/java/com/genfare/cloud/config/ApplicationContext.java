package com.genfare.cloud.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

/**
 * @author martin
 */
@Configuration
@ComponentScan(basePackages = { "com.genfare.cloud.service", "com.genfare.cloud.model",
    "com.genfare.cloud.integration", "com.genfare.cloud.reports" })
@PropertySource({ "classpath:config/sso.properties", "classpath:config/smtp.properties" })
public class ApplicationContext {

    @Bean
    public PropertySourcesPlaceholderConfigurer propertyPlaceHolderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }
}
