package com.genfare.cloud.config;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.env.Environment;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.thymeleaf.messageresolver.AbstractMessageResolver;
import org.thymeleaf.spring3.messageresolver.SpringMessageResolver;

import com.genfare.cloud.config.mvc.AbstractBaseWebMvcConfigurerAdapter;
import com.genfare.cloud.model.entity.repository.LoginsRepository;
import com.genfare.cloud.model.interceptor.ModelHandlerInterceptor;
import com.genfare.cloud.model.service.OrderManagementService;
import com.genfare.cloud.model.service.PropertiesService;
import com.genfare.cloud.resolvers.DatabaseMessageResolver;

@Configuration
@ComponentScan(basePackages = { "com.genfare.cloud.controller", "com.genfare.cloud.filter" })
@EnableWebMvc
@EnableTransactionManagement
public class WebMvcConfig
    extends AbstractBaseWebMvcConfigurerAdapter {

    @Autowired
    private Environment env;

    @Autowired
    OrderManagementService orderManagementService;

    @Autowired
    LoginsRepository loginsRepository;

    @Autowired
    private PropertiesService propertiesService;

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/login").setViewName("signin/login");
        registry.addViewController("/guest/cart").setViewName("order/cart");
        registry.addViewController("/user/cart").setViewName("order/cart");
        registry.addViewController("/guest").setViewName("home/main_guest");
        registry.addViewController("/user/cards").setViewName("user/cards");
        registry.setOrder(Ordered.HIGHEST_PRECEDENCE);
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        ModelHandlerInterceptor interceptor = new ModelHandlerInterceptor();
        interceptor.setOrderManagementService(orderManagementService);
        interceptor.setLoginsRepository(loginsRepository);
        registry.addInterceptor(interceptor);
    }

    @Override
    @Bean
    public Set<AbstractMessageResolver> messageResolvers() {
        Set<AbstractMessageResolver> msgResolvers = new HashSet<AbstractMessageResolver>();

        DatabaseMessageResolver db = new DatabaseMessageResolver();
        db.setService(propertiesService);
        db.setOrder(1);

        SpringMessageResolver spring = new SpringMessageResolver();
        spring.setMessageSource(messageSource());
        spring.setOrder(2);

        msgResolvers.add(db);
        msgResolvers.add(spring);
        return msgResolvers;
    }
}
