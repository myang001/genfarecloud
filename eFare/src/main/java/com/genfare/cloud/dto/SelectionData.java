package com.genfare.cloud.dto;

public class SelectionData {
    private final String value;

    private final String name;

    private final boolean selected;

    public SelectionData(String value, String name, boolean selected) {
        this.value = value;
        this.name = name;
        this.selected = selected;
    }

    public String getValue() {
        return value;
    }

    public String getName() {
        return name;
    }

    public boolean isSelected() {
        return selected;
    }
}
