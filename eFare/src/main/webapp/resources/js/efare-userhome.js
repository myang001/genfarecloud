$(function() {
	$('#result-table').dataTable({
		bFilter : false,
		sDom: '<"top"i>rt<"bottom"lp><"clear">',
		order: [[0,'desc']]
	});

	$('#myTabs > li').click(function() {
	    window.location.hash = $(this).children('a').attr('href');
	});
	
	$('a[href=' + window.location.hash + ']').click();
});