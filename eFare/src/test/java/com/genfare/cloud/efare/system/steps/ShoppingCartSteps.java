package com.genfare.cloud.efare.system.steps;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;

import com.genfare.cloud.efare.system.pagemodels.ShoppingCartPage;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

@WebAppConfiguration
@ContextConfiguration("classpath:/cucumber.xml")
public class ShoppingCartSteps {

    private static final Logger LOGGER = LoggerFactory.getLogger(ShoppingCartSteps.class);

    @Autowired
    ShoppingCartPage shoppingCartPage;

    @Given("^I have selected a card to add products to$")
    public void select_a_card_to_add_to_cart() {
        LOGGER.info("Entering: I have selected a card to add products to");
        shoppingCartPage.goToCardSelection().pageIsLoaded();
        shoppingCartPage.selectCard();
        shoppingCartPage.viewConfirmation(true);
        shoppingCartPage.goToAddProducts();
        shoppingCartPage.viewingProducts(true);
    }

    @When("^I add a product for the card to the cart$")
    public void add_product_to_cart() {
        LOGGER.info("Entering: I add a product for the card to the cart");
        shoppingCartPage.addProduct(0, "1");
        shoppingCartPage.viewConfirmation(true);
    }

    @Then("^I should see that the product was added to the cart$")
    public void product_added_to_cart() {
        LOGGER.info("Entering: I should see that the product was added to the cart");
        shoppingCartPage.viewCart();
        shoppingCartPage.productInCart(true);
    }

    @Given("^I have started an order and am on the cart page$")
    public void navigate_to_cart() {
        LOGGER.info("Entering: I have started an order and am on the cart page");
        shoppingCartPage.goToCardSelection().pageIsLoaded();
        shoppingCartPage.selectCard();
        shoppingCartPage.viewConfirmation(true);
        shoppingCartPage.goToAddProducts();
        shoppingCartPage.viewingProducts(true);
        shoppingCartPage.addProduct(0, "1");
        shoppingCartPage.viewConfirmation(true);
        shoppingCartPage.viewCart();
    }

    @When("^I try to remove a product from the cart$")
    public void remove_product_from_cart() {
        LOGGER.info("Entering: I try to remove a product from the cart");
        shoppingCartPage.removeProduct(1);
    }

    @Then("^I should see that the product was removed successfully$")
    public void product_removed_from_cart() {
        LOGGER.info("Entering: I should see that the product was removed successfully");
        shoppingCartPage.productRemoved(1, true);
    }

    @Given("^I have started an order and navigate to the cart page$")
    public void order_started_navigate_to_cart() {
        LOGGER.info("Entering: I have started an order and navigate to the cart page");
        shoppingCartPage.goToCardSelection().pageIsLoaded();
        shoppingCartPage.selectCard();
        shoppingCartPage.viewConfirmation(true);
        shoppingCartPage.goToAddProducts();
        shoppingCartPage.viewingProducts(true);
        shoppingCartPage.addProduct(0, "1");
        shoppingCartPage.viewConfirmation(true);
        shoppingCartPage.goToCartPage().pageIsLoaded();
    }

    @When("^I try to alter the quantity of a product$")
    public void alter_quantity_of_product() {
        LOGGER.info("Entering: I try to alter the quantity of a product");
        shoppingCartPage.changeQuantity(0, "2");
    }

    @Then("^I should see the order is updated$")
    public void order_is_updated() {
        LOGGER.info("Entering: I should see the order is updated");
        shoppingCartPage.orderUpdated(0, "2", true);
    }

}