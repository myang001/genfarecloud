package com.genfare.cloud.efare.system.steps;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import com.genfare.cloud.efare.system.pagemodels.CheckoutPage;
import com.genfare.cloud.efare.system.pagemodels.LoginPage;
import com.genfare.cloud.efare.system.pagemodels.RegisterCardPage;
import com.genfare.cloud.efare.system.pagemodels.ReplaceCardPage;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

@WebAppConfiguration
@ContextConfiguration("classpath:/cucumber.xml")
public class ReplaceCardSteps {
    private static final Logger LOGGER = LoggerFactory.getLogger(ReplaceCardSteps.class);

    @Autowired
    ReplaceCardPage replaceCardPage;

    @Autowired
    CheckoutPage checkoutPage;

    @Autowired
    LoginPage loginPage;

    @Autowired
    RegisterCardPage registerPage;

    @Given("^I am logged in and viewing the Replace Card Page$")
    public void logged_in_replacing_card() {
        LOGGER.info("Entering: I am logged in and on the replace card page");
        loginPage.goToPage().pageIsLoaded();
        loginPage.login("john.smith@spxefare.com", "Genfare123!");
        loginPage.isLoggedIn(true);
        replaceCardPage.goToPage().pageIsLoaded();
    }

    @When("^I choose a valid card name and reason$")
    public void select_card_and_reason() {
        LOGGER.info("Entering: I select a card and reason for replacement.");
        replaceCardPage.selectCard("My Period Card");
        replaceCardPage.selectReason("Lost/Stolen $5.00");
        replaceCardPage.clickContinue();
    }

    @Then("^I should checkout successfully$")
    public void checkout_successful() {
        LOGGER.info("Entering: I should receive a message saying the order was successful");
        checkoutPage.pageIsLoaded().onShippingPage();
        checkoutPage.enterShippingInformation("NO:1 Halas Way", "Apt 1", "Lake Forest", "IL", "60001", "USA");
        checkoutPage.pageIsLoaded().onCheckoutPage();
        checkoutPage.pageIsLoaded().enterInformation("4055011111111111", "123", "1115", "John Doe");
        checkoutPage.orderComplete();
    }
}