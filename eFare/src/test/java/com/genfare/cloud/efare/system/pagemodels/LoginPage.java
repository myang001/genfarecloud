package com.genfare.cloud.efare.system.pagemodels;

import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class LoginPage {

    private static final Logger LOGGER = LoggerFactory.getLogger(LoginPage.class);

    @Autowired
    WebDriver driver;

    @FindBy(id = "username")
    public WebElement usernameInput;

    @FindBy(id = "password")
    public WebElement passwordInput;

    @FindBy(id = "login")
    public WebElement submitButton;

    @FindBy(id = "nameUser")
    public List<WebElement> userWelcomeElements;

    public LoginPage goToPage() {
        driver.get("http://gfi-local.gfcp.io:8080/eFare/login");
        return this;
    }

    public LoginPage pageIsLoaded() {
        PageFactory.initElements(driver, this);
        return this;
    }

    public LoginPage login(String username, String password) {
        LOGGER.info("Logging in with username:" + username + " password:" + password);
        usernameInput.sendKeys(username);
        passwordInput.sendKeys(password);
        submitButton.click();
        return this;
    }

    public LoginPage isLoggedIn(boolean expected) {
        boolean actual;
        actual = userWelcomeElements.size() != 0;
        Assert.assertEquals(expected, actual);
        return this;
    }

}
