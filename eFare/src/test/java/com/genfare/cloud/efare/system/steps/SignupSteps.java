package com.genfare.cloud.efare.system.steps;

import java.io.IOException;
import java.math.BigInteger;
import java.security.SecureRandom;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;

import com.genfare.cloud.efare.system.pagemodels.EmailPage;
import com.genfare.cloud.efare.system.pagemodels.LoginPage;
import com.genfare.cloud.efare.system.pagemodels.SignupPage;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

@WebAppConfiguration
@ContextConfiguration("classpath:/cucumber.xml")
public class SignupSteps {

    private static final Logger LOGGER = LoggerFactory.getLogger(SignupSteps.class);

    String username;

    @Autowired
    SignupPage signupPage;

    @Autowired
    LoginPage loginPage;

    @Autowired
    EmailPage emailPage;

    @Given("^I navigate to the signup page$")
    public void given_I_navigate_to_the_signup_page() {
        LOGGER.info("Entering: I navigate to the signup page");
        signupPage.goToPage().pageIsLoaded();
    }

    @When("^I try to signup with valid form parameters$")
    public void when_I_try_to_signup_valid() {
        LOGGER.info("Entering: I try to signup with valid form parameters");
        SecureRandom random = new SecureRandom();
        username = new BigInteger(130, random).toString(32) + "@gmail.com";
        signupPage.enterUsername(username);
        signupPage.enterDefaults();
        signupPage.submit();
    }

    @Then("^I should see that I signed up successfully$")
    public void signed_up_successfully() {
        LOGGER.info("Entering: I should see that I signed up successfully");
        signupPage.formErrors(false);
        signupPage.isRegistered(true);
    }

    @Then("^I should see that I received an email to verify my email address$")
    public void verification_email_received()
        throws InterruptedException, IOException {
        LOGGER.info("Entering: I should see that I received an email to verify my email address");
        // emailPage.receiveEmail();
    }

    @When("^I verify my email address$")
    public void when_I_verify_my_email()
        throws IOException {
        LOGGER.info("Entering: I verify my email address");
        // emailPage.verifyUser();
    }

    @When("^navigate to the login page$")
    public void when_I_navigate_to_the_login() {
        LOGGER.info("Entering: Navigate to the login page");
        // loginPage.goToPage().pageIsLoaded();
    }

    @When("^enter valid credentials$")
    public void when_I_try_to_login_valid() {
        LOGGER.info("Entering: I try to login with valid credentials");
        // loginPage.login(username, "Password1!");
    }

    @Then("^I should see that I signed in successfully with my credentials$")
    public void then_I_login_successfully_with_my_credentials() {
        LOGGER.info("Entering: I should see that I logged in successfully with my credentials");
        // loginPage.isLoggedIn(true);
    }

    @When("^I try to signup with invalid form parameters$")
    public void when_I_try_to_signup_invalid() {
        LOGGER.info("Entering: I try to signup with invalid parameters");
        signupPage.enterUsername("joe@gmail.com");
        signupPage.enterPassword("Password", "Password1");
        signupPage.submit();
    }

    @Then("^I should see that there are form errors$")
    public void signed_up_errors() {
        LOGGER.info("Entering: I should see that there are form errors");
        signupPage.isRegistered(false);
        signupPage.formErrors(true);
    }

}
