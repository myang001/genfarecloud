package com.genfare.cloud.efare.system.pagemodels;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ReplaceCardPage {

    @Autowired
    WebDriver driver;

    @FindBy(id = "card")
    public WebElement cardNameList;

    @FindBy(id = "reason")
    public WebElement reasonList;

    @FindBy(id = "submit")
    public WebElement confirm;

    @FindBy(xpath = ("//button[contains(.,'Continue')]"))
    public WebElement continueButton;

    public ReplaceCardPage goToPage() {
        driver.get("http://gfi-local.gfcp.io:8080/eFare/card/replace");
        return this;
    }

    public ReplaceCardPage pageIsLoaded() {
        PageFactory.initElements(driver, this);
        return this;
    }

    public ReplaceCardPage selectCard(String reason) {
        Select selectCard = new Select(cardNameList);
        List<WebElement> options = selectCard.getOptions();
        for (WebElement option : options) {
            if (option.getText().equals(reason)) {
                option.click();
            }
        }
        return this;
    }

    public ReplaceCardPage selectReason(String reason) {
        Select selectReason = new Select(reasonList);
        List<WebElement> options = selectReason.getOptions();
        for (WebElement option : options) {
            if (option.getText().equals(reason)) {
                option.click();
            }
        }
        return this;
    }

    public ReplaceCardPage clickContinue() {
        continueButton.click();
        return this;
    }

    public ReplaceCardPage enterDefaults() {
        selectCard("My Period Card");
        selectReason("Lost/Stolen $5.00");
        return this;
    }
}
