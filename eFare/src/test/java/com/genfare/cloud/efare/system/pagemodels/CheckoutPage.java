package com.genfare.cloud.efare.system.pagemodels;

import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CheckoutPage {

    private static final Logger LOGGER = LoggerFactory.getLogger(CheckoutPage.class);

    @Autowired
    WebDriver driver;
    @FindBy(id = "receiptsubmitlink")
    public WebElement receiptForwardLink;

    @FindBy(name = "x_card_num")
    public WebElement creditNumberInput;

    @FindBy(id = "x_card_code")
    public WebElement cvvInput;

    @FindBy(id = "x_exp_date")
    public WebElement expDateInput;

    @FindBy(id = "exact_cardholder_name")
    public WebElement cardHolderInput;

    @FindBy(id = "address.line1")
    public WebElement addressLine1Input;

    @FindBy(id = "address.line2")
    public WebElement addressLine2Input;

    @FindBy(id = "address.city")
    public WebElement cityInput;

    @FindBy(id = "address.state")
    public WebElement stateInput;

    @FindBy(id = "address.postalCode")
    public WebElement postalInput;

    @FindBy(id = "address.country")
    public WebElement countryInput;

    @FindBy(className = "fieldErrorMessage")
    public List<WebElement> errors;

    @FindBy(id = "checkoutButton")
    public WebElement checkout;

    @FindBy(xpath = ("//button[contains(.,'Remove')]"))
    public List<WebElement> removeLinks;

    @FindBy(id = "goToPayment")
    public WebElement redirectToPayment;

    @FindBy(linkText = "View Cart")
    public WebElement viewCart;

    @FindBy(name = "commit")
    public WebElement submitPayment;

    public CheckoutPage pageIsLoaded() {
        PageFactory.initElements(driver, this);
        return this;
    }

    public CheckoutPage onShippingPage() {
        Assert.assertEquals(true, driver.getPageSource().contains("Shipping Information"));
        return this;
    }

    public CheckoutPage onCheckoutPage() {
        redirectToPayment.click();
        Assert.assertEquals(true, driver.getPageSource().contains("Review Your Order"));
        return this;
    }

    public CheckoutPage orderComplete() {
        Assert.assertEquals(true,
            driver.getPageSource().contains("Your payment has been processed"));
        return this;
    }

    public CheckoutPage checkForPrepopulated(String toLookFor) {
        Assert.assertEquals(true, driver.getPageSource().contains(toLookFor));
        return this;
    }

    public CheckoutPage enterInformation(String creditNumber, String cvvNumber, String exp, String holderName) {
        LOGGER.info("Entering information: Credit Card Number: " + creditNumber + ", Cvv Number: " + cvvNumber
            + ", Exp: " + exp + ", Card Holder: " + holderName);
        creditNumberInput.sendKeys(creditNumber);
        cvvInput.sendKeys(cvvNumber);
        expDateInput.sendKeys(exp);
        if (holderName != null) {
            cardHolderInput.sendKeys(holderName);
        } // allows to input null when don't want to set holder name
        submitPayment.click();
        return this;
    }

    public CheckoutPage enterShippingInformation(String addressLine1, String addressLine2, String city, String state,
        String postal, String country) {
        addressLine1Input.sendKeys(addressLine1);
        addressLine2Input.sendKeys(addressLine2);
        cityInput.sendKeys(city);
        stateInput.sendKeys(state);
        postalInput.sendKeys(postal);
        countryInput.sendKeys(country);
        checkout.click();
        return this;
    }

    public CheckoutPage checkForError(boolean expected) {
        boolean actual = (errors.size() != 0);
        Assert.assertEquals(actual, expected);
        return this;
    }

    public CheckoutPage goToCart(boolean productInCart) {
        viewCart.click();
        boolean actual = (removeLinks.size() != 0);
        Assert.assertEquals(actual, productInCart);// checks that there is something in cart
        return this;
    }

    public CheckoutPage clickHereDoesNotLoad() {
        receiptForwardLink.click();
        return this;
    }

    public CheckoutPage checkout() {
        checkout.click();
        return this;
    }

    public CheckoutPage driverIsOnCheckoutPage() {
        LOGGER.info("current url is: " + driver.getCurrentUrl());
        Assert.assertTrue(driver.getCurrentUrl().contains("collect_payment_data"));
        return this;
    }

}
