package com.genfare.cloud.efare.system.pagemodels;

import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CardSearchPage {

    private static final Logger LOGGER = LoggerFactory.getLogger(CardSearchPage.class);

    @Autowired
    WebDriver driver;

    @FindBy(id = "cardNumber")
    public WebElement cardNumberInput;

    @FindBy(id = "cvvNumber")
    public WebElement cvvInput;

    @FindBy(id = "search")
    public WebElement submitButton;

    @FindBy(id = "searchResults")
    public List<WebElement> searchResults;

    public CardSearchPage goToPage() {
        driver.get("http://gfi-local.gfcp.io:8080/eFare/card/search");
        return this;
    }

    public CardSearchPage pageIsLoaded() {
        PageFactory.initElements(driver, this);
        return this;
    }

    public CardSearchPage search(String cardNumber, String cvvNumber) {
        LOGGER.info("Searching with card number: " + cardNumber + " cvv number:" + cvvNumber);
        cardNumberInput.sendKeys(cardNumber);
        cvvInput.sendKeys(cvvNumber);
        submitButton.click();
        return this;
    }

    public CardSearchPage cardFound(boolean expected) {
        boolean actual;
        actual = searchResults.size() != 0;
        Assert.assertEquals(expected, actual);
        return this;
    }

}
