package com.genfare.cloud.efare.system.pagemodels;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SetupAutobuyPage {

    private static final Logger LOGGER = LoggerFactory.getLogger(SetupAutobuyPage.class);

    @Autowired
    WebDriver driver;

    @FindBy(linkText = "Cancel")
    public WebElement cancelButton;

    @FindBy(linkText = "Back")
    public WebElement backButton;

    @FindBy(xpath = ("//button[contains(.,'Continue')]"))
    public WebElement continueButton;

    @FindBy(id = "expires")
    public WebElement dateTextBox;

    public SetupAutobuyPage pageIsLoaded() {
        PageFactory.initElements(driver, this);
        return this;
    }

    public SetupAutobuyPage pressCancelButton() {
        cancelButton.click();
        return this;
    }

    public SetupAutobuyPage pressBackButton() {
        backButton.click();
        return this;
    }

    public SetupAutobuyPage pressContinueButton() {
        continueButton.click();
        return this;
    }

    public SetupAutobuyPage enterDate(String date) {
        LOGGER.info("Entering: User entering date: " + date);
        dateTextBox.sendKeys(date);
        return this;
    }

    public SetupAutobuyPage driverIsOnSetupAutobuyPage() {
        String url = driver.getCurrentUrl();
        Assert.assertTrue(url.contains("subscription"));
        return this;
    }
}
