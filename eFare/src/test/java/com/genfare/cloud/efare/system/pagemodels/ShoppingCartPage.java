package com.genfare.cloud.efare.system.pagemodels;

import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ShoppingCartPage {
    private static final Logger LOGGER = LoggerFactory.getLogger(ShoppingCartPage.class);

    @Autowired
    WebDriver driver;

    @FindBy(id = "x_card_num")
    public WebElement creditNumberInput;

    @FindBy(id = "x_card_code")
    public WebElement cvvInput;

    @FindBy(id = "x_exp_date")
    public WebElement expDateInput;

    @FindBy(id = "exact_cardholder_name")
    public WebElement cardHolderInput;

    @FindBy(className = "fieldErrorMessage")
    public List<WebElement> errors;

    @FindBy(id = "submit")
    public List<WebElement> submitButtons;

    @FindBy(linkText = "Checkout")
    public WebElement viewCart;

    @FindBy(xpath = ("//button[contains(.,'Update Total')]"))
    public WebElement updateTotal;

    @FindBy(xpath = ("//button[contains(.,'Delete')]"))
    public List<WebElement> removeLinks;

    @FindBy(id = "ticketFilter")
    public List<WebElement> ticketFilters;

    @FindBy(id = "quantity")
    public List<WebElement> quantityInputs;

    @FindBy(id = "itemAdded")
    public List<WebElement> itemAdded;

    @FindBy(id = "addProducts")
    public WebElement addProducts;

    public ShoppingCartPage goToCartPage() {
        driver.get("http://gfi-local.gfcp.io:8080/eFare/store/cart");
        return this;
    }

    public ShoppingCartPage goToCardSelection() {// required for generating field for products page
        driver.get("http://gfi-local.gfcp.io:8080/eFare/store/cards");
        return this;
    }

    public ShoppingCartPage goToAddProducts() {
        addProducts.click();
        return this;
    }

    public ShoppingCartPage selectCard() {// required for generating field for products page
        quantityInputs.get(0).sendKeys("1");
        submitButtons.get(0).click();
        return this;
    }

    public ShoppingCartPage viewingProducts(boolean expected) {// required for generating field for products page
        boolean actual;
        actual = ticketFilters.size() != 0;
        Assert.assertEquals(expected, quantityInputs != null);
        Assert.assertEquals(expected, actual);
        return this;
    }

    public ShoppingCartPage viewConfirmation(boolean expected) {
        boolean actual;
        actual = itemAdded.size() != 0;
        Assert.assertEquals(expected, actual);
        return this;
    }

    public ShoppingCartPage pageIsLoaded() {
        PageFactory.initElements(driver, this);
        LOGGER.info("Page is loaded");
        return this;
    }

    public ShoppingCartPage addProduct(int ticketNumber, String productQuantity) {
        quantityInputs.get(ticketNumber).sendKeys(productQuantity);
        submitButtons.get(ticketNumber).click();
        return this;
    }

    public ShoppingCartPage viewCart() {
        viewCart.click();
        return this;
    }

    public ShoppingCartPage productInCart(boolean expected) {
        boolean actual = (removeLinks.size() != 0);
        Assert.assertEquals(actual, expected);
        return this;
    }

    public ShoppingCartPage removeProduct(int productNumber) {
        removeLinks.get(productNumber).click();
        return this;
    }

    public ShoppingCartPage productRemoved(int size, boolean expected) {
        // needed to find Element here based on the productNumber
        boolean actual = (size == (removeLinks.size()));
        Assert.assertEquals(expected, actual);
        return this;
    }

    public ShoppingCartPage changeQuantity(Integer productNumber, String quantity) {
        // needed to find Element here based on the productNumber
        WebElement qChange = driver.findElement(By.id("orderitemses" + productNumber.toString() + ".quantity"));
        qChange.clear();
        qChange.sendKeys(quantity);
        updateTotal.click();
        return this;
    }

    public ShoppingCartPage orderUpdated(Integer productNumber, String newQuantity, boolean expected) {
        // needed to find Element here based on the productNumber
        WebElement productQuantity = driver.findElement(By.id("orderitemses" + productNumber.toString() + ".quantity"));
        boolean actual = productQuantity.getAttribute("value").equals(newQuantity);
        Assert.assertEquals(expected, actual);
        return this;
    }

    public ShoppingCartPage createDefaultOrder() {
        goToCardSelection().pageIsLoaded();
        selectCard();
        viewConfirmation(true);
        goToAddProducts();
        viewingProducts(true);
        addProduct(0, "1");
        viewConfirmation(true);
        viewCart();
        productInCart(true);
        return this;
    }

}