package com.genfare.cloud.efare.system.steps;

import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;

import com.genfare.cloud.efare.support.DatabaseSupport;

import cucumber.api.java.After;
import cucumber.api.java.Before;

@WebAppConfiguration
@ContextConfiguration("classpath:/cucumber.xml")
public class TestHooks {

    private static final Logger LOGGER = LoggerFactory.getLogger(TestHooks.class);

    // Remove replace card orders to be done before and after scenarios
    private final String deleteReplaceOrderItems = "DELETE from orderitems where status=13 and walletid=350 and orderitemactionid=2";
    private final String revertWalletStatus = "UPDATE wallets SET walletstatusId=2 WHERE walletid=350";
    private final String deleteHoldOrders = "DELETE from orders where personid=218 and status=13";
    // ensure no subscriptions set up for user
    private final String unsubscribe = "DELETE from subscriptions where personid=218";
    // delete the peopleCard for the user
    private final String peopleCard = "DELETE from peoplecards where personid=218";

    @Autowired
    WebDriver driver;

    @Before
    public void beforeScenario() {
        LOGGER.info("Clearing cookies");
        DatabaseSupport ds = new DatabaseSupport("jdbc:mysql://localhost:3306/test", "vagrant", "vagrant", "com.mysql.jdbc.Driver");

        // Remove any in progress sales orders or pending service orders for user
        String deleteOrderItems = "DELETE from orderitems WHERE orderid=(SELECT orderid from orders where personid=218 and ordertypeid=1 and status=1)";
        String deleteSubscriptionItems = "DELETE from orderitems WHERE orderid=(SELECT orderid from orders where personid=218 and ordertypeid=2 and status=8)";
        String deleteOrders = "DELETE from orders where personid=218 and ordertypeid=1 and status=1";
        String deleteSubscriptionOrders = "DELETE from orders where personid=218 and ordertypeid=2 and status=8";
        // ensure card to search for is unregistered
        String unregister = "UPDATE wallets SET personid=null WHERE walletid=351";
        // register a card for user to use
        String register = "UPDATE wallets SET personid=218, nickname='My Period Card' WHERE walletid=350";

        ds.execute(unsubscribe).execute(deleteReplaceOrderItems).execute(deleteHoldOrders).execute(revertWalletStatus).execute(deleteOrderItems).execute(deleteSubscriptionItems)
            .execute(deleteOrders)
            .execute(deleteSubscriptionOrders).execute(unregister).execute(register)
            .execute(peopleCard)
            .closeConnection();
        driver.get("http://gfi-local.gfcp.io:8080/eFare/logout");
        driver.manage().deleteAllCookies();
    }

    @After
    public void afterScenario() {
        DatabaseSupport ds = new DatabaseSupport("jdbc:mysql://localhost:3306/test", "vagrant", "vagrant", "com.mysql.jdbc.Driver");
        // Unregister card registered for tests
        String unregister = "UPDATE wallets SET personid=null WHERE walletid=350";
        // Unregister card registered during tests
        String unregister2 = "UPDATE wallets SET personid=null WHERE walletid=349";

        ds.execute(unsubscribe).execute(deleteReplaceOrderItems).execute(deleteHoldOrders).execute(revertWalletStatus).execute(unregister).execute(unregister2).execute(peopleCard)
            .closeConnection();
    }

}
