package com.genfare.cloud.efare.system.steps;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;

import com.genfare.cloud.efare.system.pagemodels.LoginPage;
import com.genfare.cloud.efare.system.pagemodels.RegisterCardPage;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

@WebAppConfiguration
@ContextConfiguration("classpath:/cucumber.xml")
public class RegisterCardSteps {
    private static final Logger LOGGER = LoggerFactory.getLogger(RegisterCardSteps.class);

    @Autowired
    RegisterCardPage registerCardPage;

    @Autowired
    LoginPage loginPage;

    @Given("^I am logged in and on the Card Registration page$")
    public void on_card_registration() {
        LOGGER.info("Entering: I am logged in and on the Card Registration page");
        loginPage.goToPage().pageIsLoaded();
        loginPage.login("john.smith@spxefare.com", "Genfare123!");
        loginPage.isLoggedIn(true);
        registerCardPage.goToPage().pageIsLoaded();
    }

    @When("^I enter a nickname and search a valid card number and CVV number$")
    public void valid_search() {
        LOGGER.info("Entering: I enter a nickname and search a valid card number and CVV number");
        registerCardPage.register("My Period Card", "1234560028", "123");
    }

    @When("^I enter a nickname and search an invalid card number or CVV number$")
    public void invalid_search() {
        LOGGER.info("Entering: I enter a nickname and search an invalid card number or CVV number");
        registerCardPage.register("Babe's Card", "1280085788", "707");
    }

    @Then("^I should see the card was registered successfully$")
    public void register_success() {
        LOGGER.info("Entering: I should see the card was registered successfully");
        registerCardPage.cardRegistered(true);
    }

    @Then("^I should see an error for invalid input$")
    public void register_unsuccessfull() {
        LOGGER.info("Entering: I should see an error for invalid input");
        registerCardPage.cardRegistered(false);
    }
}