package com.genfare.cloud.efare.system.pagemodels;

import java.util.List;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RegisterCardPage {

    private static final Logger LOGGER = LoggerFactory.getLogger(RegisterCardPage.class);

    @Autowired
    WebDriver driver;

    @FindBy(id = "nickname")
    public WebElement nicknameInput;

    @FindBy(id = "cardNumber")
    public WebElement cardNumberInput;

    @FindBy(id = "cvvNumber")
    public WebElement cvvInput;

    @FindBy(id = "search")
    public WebElement registerButton;

    @FindBy(id = "searchResults")
    public List<WebElement> searchResults;

    public RegisterCardPage goToPage() {
        driver.get("http://gfi-local.gfcp.io:8080/eFare/card/register");
        return this;
    }

    public RegisterCardPage pageIsLoaded() {
        PageFactory.initElements(driver, this);
        return this;
    }

    public RegisterCardPage register(String nickname, String cardNumber, String cvvNumber) {
        LOGGER.info("Registering with nickname: " + nickname + " card number: " + cardNumber + " and CVV number:"
            + cvvNumber);
        nicknameInput.sendKeys(nickname);
        cardNumberInput.sendKeys(cardNumber);
        cvvInput.sendKeys(cvvNumber);
        registerButton.click();
        return this;
    }

    public RegisterCardPage cardRegistered(boolean expected) {
        boolean actual;
        String url = driver.getCurrentUrl();
        actual = url.contains("name");
        Assert.assertEquals(expected, actual);
        return this;
    }

}