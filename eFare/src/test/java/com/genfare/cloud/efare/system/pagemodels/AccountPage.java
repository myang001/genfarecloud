package com.genfare.cloud.efare.system.pagemodels;

import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AccountPage {

    private static final Logger LOGGER = LoggerFactory.getLogger(AccountPage.class);

    @Autowired
    WebDriver driver;

    @FindBy(linkText = "My Profile")
    public WebElement profileTab;

    @FindBy(id = "phone")
    public WebElement phone;

    @FindBy(className = "fielderr")
    public List<WebElement> errors;

    @FindBy(className = "globalerr")
    public List<WebElement> gErrors;

    @FindBy(xpath = "//div[@id='cards']/div/a")
    public List<WebElement> cards;

    @FindBy(id = "submit")
    public WebElement submitButton;

    public AccountPage goToPage() {
        driver.get("http://gfi-local.gfcp.io:8080/eFare");
        return this;
    }

    public AccountPage pageIsLoaded() {
        PageFactory.initElements(driver, this);
        return this;
    }

    public AccountPage showProfile() {
        profileTab.click();
        return this;
    }

    public AccountPage submit() {
        submitButton.click();
        return this;
    }

    public AccountPage formErrors(boolean expected) {
        boolean actual;
        actual = errors.size() != 0 || gErrors.size() != 0;
        Assert.assertEquals(expected, actual);
        return this;
    }

    public AccountPage enterPhoneNumber(String number) {
        phone.clear();
        phone.sendKeys(number);
        return this;
    }

    public AccountPage clickCard(int pos) {
        cards.get(pos).click();
        return this;
    }

    public AccountPage isUpdated() {
        Assert.assertEquals(phone.getAttribute("value"), "(847) 593-8855");
        return this;
    }

    public AccountPage driverIsOnAccountPage() {
        Assert.assertEquals(true, driver.getPageSource().contains("My Profile"));
        return this;
    }

}
