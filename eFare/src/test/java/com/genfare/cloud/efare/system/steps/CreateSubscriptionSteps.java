/** 
 * This class is a steps file to interface with 
 * SetupAutobuy.feature file
 * 
 *  @author Mitch Cooley
 */
package com.genfare.cloud.efare.system.steps;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;

import com.genfare.cloud.efare.system.pagemodels.AccountPage;
import com.genfare.cloud.efare.system.pagemodels.CardDetailsPage;
import com.genfare.cloud.efare.system.pagemodels.CheckoutPage;
import com.genfare.cloud.efare.system.pagemodels.LoginPage;
import com.genfare.cloud.efare.system.pagemodels.RegisterCardPage;
import com.genfare.cloud.efare.system.pagemodels.SetupAutobuyPage;
import com.genfare.cloud.efare.system.pagemodels.SubscriptionCompletePage;
import com.genfare.cloud.efare.system.pagemodels.SubscriptionPaymentPage;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

@WebAppConfiguration
@ContextConfiguration("classpath:/cucumber.xml")
public class CreateSubscriptionSteps {
    private static final Logger LOGGER = LoggerFactory.getLogger(CreateSubscriptionSteps.class);

    @Autowired
    SetupAutobuyPage subscriptionPage;

    @Autowired
    LoginPage loginPage;

    @Autowired
    CardDetailsPage cardDetailsPage;

    @Autowired
    AccountPage accountPage;

    @Autowired
    SubscriptionPaymentPage paymentPage;

    @Autowired
    SubscriptionCompletePage completePage;

    @Autowired
    CheckoutPage creditCardPage;

    @Autowired
    RegisterCardPage registerPage;

    @Given("^I am logged in$")
    public void given_I_am_logged_in() {
        LOGGER.info("Entering: I am logged in to setup a subscription");
        loginPage.goToPage().pageIsLoaded();
        loginPage.login("john.smith@spxefare.com", "Genfare123!");
        loginPage.isLoggedIn(true);
    }

    @Given("^I am on Setup Autobuy page/prompt$")
    public void given_I_am_on_Setup_Autobuy_page() {
        LOGGER.info("Entering: I am on the Setup Autobuy page...");
        accountPage.goToPage().pageIsLoaded();
        accountPage.clickCard(0);
        cardDetailsPage.pageIsLoaded();
        cardDetailsPage.clickSetupLink();
        subscriptionPage.pageIsLoaded();
    }

    @When("^I enter date in format '(.+)'$")
    public void when_I_enter_date_in_format(String date) {
        LOGGER.info("Entering: I enter date in expire autobuy field" + "in format: " + date);
        subscriptionPage.enterDate(date);
    }

    @When("^I press the continue button$")
    public void when_I_press_the_continue_button() {
        LOGGER.info("Entering: I press the continue button...");
        subscriptionPage.driverIsOnSetupAutobuyPage();
        subscriptionPage.pressContinueButton();
    }

    @Then("^I should be redirected to the '(.+)' screen$")
    public void then_I_should_be_redirected_to_the_given_screen(String screen) {
        LOGGER.info("Entering: I should be redirected to the " + screen + " screen...");
        if (screen == "payment") {
            paymentPage.driverIsOnSubscriptionPaymentPage();
        } else {
            subscriptionPage.driverIsOnSetupAutobuyPage();
        }
    }

    @When("^I press the back button$")
    public void when_I_press_the_back_button() {
        LOGGER.info("Entering: I press the back button...");
        subscriptionPage.driverIsOnSetupAutobuyPage();
        subscriptionPage.pressBackButton();
    }

    @Given("^I am on Payment page/prompt$")
    public void given_I_am_on_Payment_page() {
        LOGGER.info("Entering: I am on the Payment page/prompt...");
        accountPage.goToPage().pageIsLoaded();
        accountPage.clickCard(0);
        cardDetailsPage.pageIsLoaded();
        cardDetailsPage.clickSetupLink();
        subscriptionPage.pageIsLoaded();
        subscriptionPage.driverIsOnSetupAutobuyPage();
        subscriptionPage.pressContinueButton();
        paymentPage.pageIsLoaded();
        paymentPage.driverIsOnSubscriptionPaymentPage();
    }

    @Then("^I press Setup a Card Now$")
    public void press_setup_card_now() {
        LOGGER.info("Entering: I press Setup a Card Now button");
        paymentPage.pressSetupCard();
    }

    @Given("^I am on Complete page/prompt$")
    public void given_I_am_on_Complete_page() {
        LOGGER.info("Entering: I am on the Complete page/prompt..");
        accountPage.goToPage().pageIsLoaded();
        accountPage.clickCard(0);
        cardDetailsPage.pageIsLoaded();
        cardDetailsPage.clickSetupLink();
        subscriptionPage.pageIsLoaded();
        subscriptionPage.driverIsOnSetupAutobuyPage();
        subscriptionPage.pressContinueButton();
        paymentPage.pageIsLoaded();
        paymentPage.driverIsOnSubscriptionPaymentPage();
        paymentPage.pressStartAutobuyButton();
        completePage.pageIsLoaded();
        completePage.driverIsOnSubscriptionCompletePage();
    }

    @When("^I press the cancel button$")
    public void when_I_press_the_cancel_button() {
        LOGGER.info("Entering: I press the cancel button...");
        subscriptionPage.driverIsOnSetupAutobuyPage();
        subscriptionPage.pressCancelButton();
    }

    @When("^I press the Start Autobuy button$")
    public void when_I_press_the_Start_Autobuy_button() {
        LOGGER.info("Entering: I press the Start Autobuy button...");
        creditCardPage.pageIsLoaded().clickHereDoesNotLoad();
        paymentPage.pageIsLoaded().pressStartAutobuyButton();
    }

    @Then("^I should be taken back to the Setup Autobuy page$")
    public void then_I_should_be_taken_back_to_the_Setup_Autobuy_page() {
        LOGGER.info("Entering: I should be taken back to the Setup Autobuy page...");
        subscriptionPage.driverIsOnSetupAutobuyPage();
    }

    @Then("^I should be taken back to the Card Details page$")
    public void then_I_should_be_taken_back_to_the_Card_Details_page() {
        LOGGER.info("Entering: I should be taken back to the Card Details page...");
        cardDetailsPage.driverIsOnCardDetailsPage();
    }

    @Then("^I should be taken to my account page$")
    public void then_I_should_be_taken_back_to_my_account_page() {
        LOGGER.info("Entering: I should be taken to my account page...");
        accountPage.driverIsOnAccountPage();
    }

    @Then("^I should be taken to Complete page$")
    public void then_I_should_be_taken_to_Complete_page() {
        LOGGER.info("Entering: I should be taken to Complete page...");
        completePage.driverIsOnSubscriptionCompletePage();
    }

    @Then("^I should be taken to the Credit Card info page$")
    public void then_I_should_be_taken_to_the_Credit_Card_info_page() {
        LOGGER.info("Entering: I should be taken to the Credit Card info page...");
        creditCardPage.pageIsLoaded().driverIsOnCheckoutPage();
    }

    @When("^I submit my card information$")
    public void submit_my_card_info() {
        LOGGER.info("Entering: I submit my card information");
        creditCardPage.enterInformation("4055011111111111", "123", "1115", "John Smith");
    }

}
