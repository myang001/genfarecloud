package com.genfare.cloud.efare.system.pagemodels;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SubscriptionPaymentPage {

    @Autowired
    WebDriver driver;

    @FindBy(linkText = "Cancel")
    public WebElement cancelButton;

    @FindBy(linkText = "Back")
    public WebElement backButton;

    @FindBy(xpath = ("//button[contains(.,'Start Autobuy')]"))
    public WebElement startAutobuyButton;

    @FindBy(xpath = ("//button[contains(.,'Use Another Card')]"))
    public WebElement useAnotherCardButton;

    @FindBy(xpath = ("//button[contains(.,'Setup a Card Now')]"))
    public WebElement setupCardButton;

    public SubscriptionPaymentPage pageIsLoaded() {
        PageFactory.initElements(driver, this);
        return this;
    }

    public SubscriptionPaymentPage pressCancelButton() {
        cancelButton.click();
        return this;
    }

    public SubscriptionPaymentPage pressBackButton() {
        backButton.click();
        return this;
    }

    public SubscriptionPaymentPage pressStartAutobuyButton() {
        startAutobuyButton.click();
        return this;
    }

    public SubscriptionPaymentPage pressUseAnotherCardButton() {
        useAnotherCardButton.submit();
        return this;
    }

    public SubscriptionPaymentPage driverIsOnSubscriptionPaymentPage() {
        String url = driver.getCurrentUrl();
        Assert.assertTrue(url.contains("payment"));
        return this;
    }

    public SubscriptionPaymentPage pressSetupCard() {
        setupCardButton.click();
        return this;
    }

}
