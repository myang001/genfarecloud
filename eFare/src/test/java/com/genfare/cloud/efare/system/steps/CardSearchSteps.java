package com.genfare.cloud.efare.system.steps;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;

import com.genfare.cloud.efare.system.pagemodels.CardSearchPage;
import com.genfare.cloud.efare.system.pagemodels.RegisterCardPage;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

@WebAppConfiguration
@ContextConfiguration("classpath:/cucumber.xml")
public class CardSearchSteps {
    private static final Logger LOGGER = LoggerFactory.getLogger(CardSearchSteps.class);

    @Autowired
    RegisterCardPage registerCardPage;

    @Autowired
    CardSearchPage searchPage;

    @Given("^I navigate to the search card page$")
    public void given_I_navigate_to_the_application() {
        LOGGER.info("Entering: I navigate to the search card page");
        searchPage.goToPage().pageIsLoaded();
    }

    @When("^I try to search for a valid card$")
    public void when_I_try_to_search_valid() {
        LOGGER.info("Entering: I try to search for a valid card");
        searchPage.search("1234560030", "123");
    }

    @When("^I try to search for an invalid card$")
    public void when_I_try_to_search_invalid() {
        LOGGER.info("Entering: I try to searchfor an invalid");
        searchPage.search("101", "1");
    }

    @Then("^I should see that a card is found$")
    public void then_I_searched_successfully() {
        LOGGER.info("Entering: I should see that a card is found");
        searchPage.cardFound(true);
    }

    @Then("^I should see that a card is not found$")
    public void then_I_searched_unsuccessfully() {
        LOGGER.info("Entering: I should see that a card is not found");
        searchPage.cardFound(false);
    }

}
