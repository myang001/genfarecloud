package com.genfare.cloud.efare.system.pagemodels;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SubscriptionCompletePage {

    @Autowired
    WebDriver driver;

    @FindBy(linkText = "Done")
    public WebElement doneButton;

    public SubscriptionCompletePage pageIsLoaded() {
        PageFactory.initElements(driver, this);
        return this;
    }

    public SubscriptionCompletePage clickDoneButton() {
        doneButton.click();
        return this;
    }

    public SubscriptionCompletePage driverIsOnSubscriptionCompletePage() {
        String url = driver.getCurrentUrl();
        Assert.assertTrue(url.contains("complete"));
        return this;
    }

}
