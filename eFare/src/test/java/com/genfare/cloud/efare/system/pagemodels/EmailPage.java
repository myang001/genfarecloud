package com.genfare.cloud.efare.system.pagemodels;

import java.io.IOException;
import java.net.URL;

import javax.mail.internet.MimeMessage;

import org.apache.commons.codec.binary.Base64;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gargoylesoftware.htmlunit.StringWebResponse;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HTMLParser;
import com.gargoylesoftware.htmlunit.html.HtmlAnchor;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.icegreen.greenmail.util.GreenMail;
import com.icegreen.greenmail.util.GreenMailUtil;

@Component
public class EmailPage {
    private static final Logger LOGGER = LoggerFactory.getLogger(EmailPage.class);

    @Autowired
    WebDriver driver;

    @Autowired
    GreenMail greenMail;

    String body;

    public EmailPage receiveEmail()
        throws InterruptedException, IOException {
        LOGGER.info("Receiving email");
        greenMail.waitForIncomingEmail(2000, 1);
        MimeMessage[] messages = greenMail.getReceivedMessages();
        String bodyReceived = GreenMailUtil.getBody(messages[0]);
        String[] parts = bodyReceived.split("base64");
        body = parts[1];
        byte[] decoded = Base64.decodeBase64(body);
        body = new String(decoded, "UTF-8");
        body = body.replaceAll("8081", "8080");
        return this;
    }

    public EmailPage verifyUser()
        throws IOException {
        WebClient client = new WebClient();
        URL url = new URL("http://www.example.com");
        StringWebResponse response = new StringWebResponse(body, url);
        HtmlPage page = HTMLParser.parseHtml(response, client.getCurrentWindow());
        HtmlAnchor anchor = (HtmlAnchor) page.getAnchors().get(0);
        String link = anchor.getHrefAttribute();
        driver.get(link);
        return this;
    }

}
