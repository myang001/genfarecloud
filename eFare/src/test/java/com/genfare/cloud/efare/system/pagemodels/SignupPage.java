package com.genfare.cloud.efare.system.pagemodels;

import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SignupPage {

    private static final Logger LOGGER = LoggerFactory.getLogger(SignupPage.class);

    String username;

    @Autowired
    WebDriver driver;

    @FindBy(id = "email.address")
    public WebElement usernameInput;

    @FindBy(id = "password1")
    public WebElement passwordInput1;

    @FindBy(id = "password2")
    public WebElement passwordInput2;

    @FindBy(id = "firstName")
    public WebElement firstName;

    @FindBy(id = "lastName")
    public WebElement lastName;

    @FindBy(id = "address.line1")
    public WebElement addrLine1;

    @FindBy(id = "address.line2")
    public WebElement addrLine2;

    @FindBy(id = "address.city")
    public WebElement addrCity;

    @FindBy(id = "address.postalCode")
    public WebElement addrPostal;

    @FindBy(id = "address.state")
    public WebElement addrState;

    @FindBy(id = "address.country")
    public WebElement addrCountry;

    @FindBy(id = "phone")
    public WebElement phone;

    @FindBy(id = "dateOfBirth")
    public WebElement dob;

    @FindBy(id = "securityQuestion")
    public WebElement securityQuestion;

    @FindBy(id = "securityAnswer")
    public WebElement securityAnswer;

    @FindBy(id = "submit")
    public WebElement submitButton;

    @FindBy(className = "fielderr")
    public List<WebElement> errors;

    @FindBy(className = "globalerr")
    public List<WebElement> gErrors;

    @FindBy(id = "registered")
    public List<WebElement> registered;

    @FindBy(id = "primaryContactMethod")
    public WebElement primaryContactMethod;

    @FindBy(id = "alert1")
    public WebElement alert1;

    public SignupPage goToPage() {
        driver.get("http://gfi-local.gfcp.io:8080/eFare/signup");
        return this;
    }

    public SignupPage pageIsLoaded() {
        PageFactory.initElements(driver, this);
        return this;
    }

    public SignupPage enterUsername(String username) {
        usernameInput.sendKeys(username);
        return this;
    }

    public SignupPage enterPassword(String password, String verifyPassword) {
        passwordInput1.sendKeys(password);
        passwordInput2.sendKeys(verifyPassword);
        return this;
    }

    public SignupPage enterName(String first, String last) {
        firstName.sendKeys(first);
        lastName.sendKeys(last);
        return this;
    }

    public SignupPage enterAddress(String line1, String line2, String city, String state, String postal, String country) {
        addrLine1.sendKeys(line1);
        addrLine2.sendKeys(line2);
        addrCity.sendKeys(city);
        addrState.sendKeys(state);
        addrPostal.sendKeys(postal);
        Select selectCountry = new Select(addrCountry);
        List<WebElement> options3 = selectCountry.getOptions();
        for (WebElement option : options3) {
            if (option.getText().equals("United States")) {
                option.click();
                break;
            }
        }
        return this;
    }

    public SignupPage enterPhone(String p) {
        phone.sendKeys(p);
        return this;
    }

    public SignupPage enterBirthDate(String date) {
        dob.sendKeys(date);
        return this;
    }

    public SignupPage enterAlert1(boolean clicked) {
        if (clicked) {
            alert1.click();
        }
        return this;
    }

    public SignupPage enterSecurityQuestion(String question, String answer) {
        Select selectQuestion = new Select(securityQuestion);
        List<WebElement> options3 = selectQuestion.getOptions();
        for (WebElement option : options3) {
            if (option.getText().equals(question)) {
                option.click();
                break;
            }
        }
        securityAnswer.sendKeys(answer);
        return this;
    }

    public SignupPage enterPrimaryContactMethod(String contactMethod) {
        Select selectQuestion = new Select(primaryContactMethod);
        List<WebElement> options3 = selectQuestion.getOptions();
        for (WebElement option : options3) {
            if (option.getText().equals(contactMethod)) {
                option.click();
                break;
            }
        }
        return this;
    }

    public SignupPage submit() {
        try {
            submitButton.click();
        } catch (Exception ex) {
            LOGGER.info("Error registering user");
        }
        return this;
    }

    public SignupPage enterDefaults() {
        enterPassword("Password1!", "Password1!");
        enterName("John", "Doe");
        enterAddress("100 Chicago Ave.", "apt 1", "Chicago", "Illinois", "60610", "United States");
        enterPhone("8475938855");
        enterBirthDate("01/01/1980");
        enterPrimaryContactMethod("Email");
        enterAlert1(true);
        enterSecurityQuestion("What is the name of your first pet?", "Fluffy");
        return this;
    }

    public SignupPage isRegistered(boolean expected) {
        boolean actual;
        actual = registered.size() != 0;
        Assert.assertEquals(expected, actual);
        return this;
    }

    public SignupPage formErrors(boolean expected) {
        boolean actual;
        actual = errors.size() != 0 || gErrors.size() != 0;
        Assert.assertEquals(expected, actual);
        return this;
    }
}
