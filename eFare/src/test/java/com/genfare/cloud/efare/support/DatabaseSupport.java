package com.genfare.cloud.efare.support;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DatabaseSupport {

    private static final Logger LOGGER = LoggerFactory.getLogger(DatabaseSupport.class);
    private String url;
    private String user;
    private String pass;
    private String dbClass;
    private Connection connection;

    public DatabaseSupport(String url, String user, String pass, String dbClass) {
        this.url = url;
        this.user = user;
        this.pass = pass;
        this.dbClass = dbClass;
        try {
            Class.forName(this.dbClass);
            this.connection = DriverManager.getConnection(this.url, this.user, this.pass);
        } catch (ClassNotFoundException | SQLException e) {
            LOGGER.error("Cannot establish connection");
            e.printStackTrace();
        }
    }

    public DatabaseSupport execute(String query) {
        try {
            Statement statement = connection.createStatement();
            statement.executeUpdate(query);
        } catch (SQLException e) {
            // Statements may or may not be able to execute based on what the test changed
            LOGGER.warn("Cannot execute query");
            e.printStackTrace();
        }
        return this;
    }

    public DatabaseSupport closeConnection() {
        try {
            this.connection.close();
        } catch (SQLException e) {
            LOGGER.error("Cannot close connection");
            e.printStackTrace();
        }
        return this;
    }

}
