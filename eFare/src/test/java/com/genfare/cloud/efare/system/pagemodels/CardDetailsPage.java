package com.genfare.cloud.efare.system.pagemodels;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CardDetailsPage {

    @Autowired
    WebDriver driver;

    @FindBy(linkText = "Start Autobuy")
    public List<WebElement> setupLinks;

    public CardDetailsPage goToPage() {
        return this;
    }

    public CardDetailsPage pageIsLoaded() {
        PageFactory.initElements(driver, this);
        return this;
    }

    public CardDetailsPage clickSetupLink() {
        setupLinks.get(0).click();
        return this;
    }

    public boolean driverIsOnCardDetailsPage() {
        String url = driver.getCurrentUrl();
        return url.contains("details");
    }

}
