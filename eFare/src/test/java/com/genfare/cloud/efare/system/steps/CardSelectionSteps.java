package com.genfare.cloud.efare.system.steps;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;

import com.genfare.cloud.efare.system.pagemodels.CardSearchPage;
import com.genfare.cloud.efare.system.pagemodels.CardSelectionPage;
import com.genfare.cloud.efare.system.pagemodels.RegisterCardPage;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

@WebAppConfiguration
@ContextConfiguration("classpath:/cucumber.xml")
public class CardSelectionSteps {
    private static final Logger LOGGER = LoggerFactory.getLogger(CardSelectionSteps.class);

    @Autowired
    CardSelectionPage cardSelectionPage;

    @Autowired
    CardSearchPage searchPage;

    @Autowired
    RegisterCardPage registerPage;

    @Given("^I navigate to the select card page$")
    public void navigate_to_card_selection() {
        LOGGER.info("Entering: I navigate to the card page");
        cardSelectionPage.goToPage().pageIsLoaded();
    }

    @When("^I click on card$")
    public void click_on_card() {
        LOGGER.info("Entering: I click on a card");
        cardSelectionPage.selectCard();
    }

    @Then("^I should see the confirmation page$")
    public void see_product_page() {
        LOGGER.info("Entering: I should see the confirmation page");
        cardSelectionPage.viewConfirmation(true);
    }

    @Given("^I have successfully searched a card$")
    public void search_a_card() {
        LOGGER.info("Entering: I have successfully searched a card");
        searchPage.goToPage().pageIsLoaded();
        searchPage.search("1234560030", "123");
    }

    @When("^I click add products$")
    public void click_add_products() {
        LOGGER.info("Entering: I click add product");
        cardSelectionPage.addProducts();
    }

    @Then("^I should see the products page for my card$")
    public void see_product_page_my_card() {
        LOGGER.info("Entering:I should see the products page for my card");
        cardSelectionPage.viewingProducts(true);
    }
}