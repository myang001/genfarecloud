package com.genfare.cloud.efare.system.steps;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;

import com.genfare.cloud.efare.system.pagemodels.AccountPage;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

@WebAppConfiguration
@ContextConfiguration("classpath:/cucumber.xml")
public class ProfileSteps {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProfileSteps.class);

    @Autowired
    AccountPage accountPage;

    @Given("^I navigate to my profile page$")
    public void I_navigate_to_my_profile_page()
        throws Throwable {
        LOGGER.info("Entering: I navigate to my profile page");
        accountPage.goToPage().pageIsLoaded().showProfile();
    }

    @When("^I try to edit profile with (invalid|valid) parameters$")
    public void I_try_to_edit_profile_with_invalid_parameters(String validity)
        throws Throwable {
        LOGGER.info("Entering: I try to edit profile with " + validity + " parameters");
        if (validity.equals("invalid")) {
            accountPage.enterPhoneNumber("123").submit();
        } else if (validity.equals("valid")) {
            accountPage.enterPhoneNumber("8475938855").submit();
        } else {
            // Unknown option
        }
    }

    @Then("^I (should not|should) see that there are edit profile form errors$")
    public void I_should_see_that_there_are_edit_profile_form_errors(String expectation)
        throws Throwable {
        LOGGER.info("Entering: I " + expectation + " should see that there are edit profile form errors");
        if (expectation.equals("should")) {
            accountPage.pageIsLoaded().formErrors(true);
        } else if (expectation.equals("should not")) {
            accountPage.pageIsLoaded().formErrors(false);
        } else {
            // Unknown option
        }
    }

    @Then("^user should be updated successfully$")
    public void user_should_be_updated_successfully() {
        LOGGER.info("Entering: User should be updated successfully");
        accountPage.isUpdated();
    }
}
