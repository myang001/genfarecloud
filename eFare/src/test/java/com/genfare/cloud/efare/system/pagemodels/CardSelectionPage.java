package com.genfare.cloud.efare.system.pagemodels;

import java.util.List;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CardSelectionPage {
    private static final Logger LOGGER = LoggerFactory.getLogger(CardSelectionPage.class);

    @Autowired
    WebDriver driver;

    @FindBy(id = "submit")
    public List<WebElement> submitButtons;

    @FindBy(id = "addProducts")
    public WebElement addProducts;

    @FindBy(id = "itemAdded")
    public List<WebElement> itemAdded;

    @FindBy(id = "ticketFilter")
    public List<WebElement> ticketFilters;

    @FindBy(id = "quantity")
    public List<WebElement> quantityInputs;

    public CardSelectionPage goToPage() {
        driver.get("http://gfi-local.gfcp.io:8080/eFare/store/cards");
        return this;
    }

    public CardSelectionPage pageIsLoaded() {
        PageFactory.initElements(driver, this);
        LOGGER.info("Card Selection Page is loaded");
        return this;
    }

    public CardSelectionPage selectCard() {
        LOGGER.info("Number of submit buttons: " + submitButtons.size());
        quantityInputs.get(0).sendKeys("1");
        submitButtons.get(0).click();
        return this;
    }

    public CardSelectionPage addProducts() {
        addProducts.click();
        return this;
    }

    public CardSelectionPage viewConfirmation(boolean expected) {
        boolean actual;
        actual = itemAdded.size() != 0;
        Assert.assertEquals(expected, actual);
        return this;
    }

    public CardSelectionPage viewingProducts(boolean expected) {// required for generating field for products page
        boolean actual;
        actual = ticketFilters.size() != 0;
        Assert.assertEquals(expected, quantityInputs != null);
        Assert.assertEquals(expected, actual);
        return this;
    }

}