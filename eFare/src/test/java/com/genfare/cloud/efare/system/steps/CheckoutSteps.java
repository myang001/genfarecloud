package com.genfare.cloud.efare.system.steps;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;

import com.genfare.cloud.efare.system.pagemodels.CheckoutPage;
import com.genfare.cloud.efare.system.pagemodels.LoginPage;
import com.genfare.cloud.efare.system.pagemodels.ShoppingCartPage;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

@WebAppConfiguration
@ContextConfiguration("classpath:/cucumber.xml")
public class CheckoutSteps {
    private static final Logger LOGGER = LoggerFactory.getLogger(CheckoutSteps.class);

    @Autowired
    CheckoutPage checkoutPage;

    @Autowired
    ShoppingCartPage shoppingCartPage;

    @Autowired
    LoginPage loginPage;

    // Scenario 1
    @Given("^I have a guest order started and am on the cart page$")
    public void guest_order_started() {
        LOGGER.info("Entering: I have a guest order started and am on the cart page");
        shoppingCartPage.createDefaultOrder();
    }

    @When("^I select Checkout$")
    public void select_checkout() {
        LOGGER.info("Entering: I select Checkout");
        checkoutPage.pageIsLoaded().checkout();
    }

    @Then("^I should see a page to enter my shipping information$")
    public void see_shipping_info() {
        LOGGER.info("Entering: I should see a page to enter my shipping information");
        checkoutPage.onShippingPage();
    }

    @When("^I enter my shipping information and select Checkout$")
    public void enter_shipping_info() {
        LOGGER.info("Entering: When I enter my shipping information and select Checkout");
        checkoutPage.enterShippingInformation("NO:1 Halas Way", "Apt 1", "Lake Forest", "IL", "60001", "USA");
    }

    @Then("^I should see a page to enter my payment information$")
    public void see_page_payment_info() {
        LOGGER.info("Entering: I should see a page to enter my payment information");
        checkoutPage.pageIsLoaded().onCheckoutPage();
    }

    @When("^I enter my payment information and click submit$")
    public void enter_my_payment_information() {
        LOGGER.info("Entering: I enter my payment information and click submit");
        checkoutPage.pageIsLoaded().enterInformation("4055011111111111", "123", "1115", "John Doe");
    }

    @Then("^I should see that I placed my order successfully$")
    public void placed_order_successfully() {
        LOGGER.info("Entering: I should see that I placed my order successfully");
        checkoutPage.orderComplete();
    }

    // Scenario 2 also uses scenario 1 steps
    @Given("^I am logged in and I have an order started and am on the cart page$")
    public void order_started() {
        LOGGER.info("Entering: I am logged in and I have an order started and am on the cart page");
        loginPage.goToPage().pageIsLoaded();
        loginPage.login("john.smith@spxefare.com", "Genfare123!");
        loginPage.isLoggedIn(true);
        shoppingCartPage.createDefaultOrder();
    }

    @Then("^I should see a semi-prepopulated page to enter my information$")
    public void prepopulated_page_enter_info() {
        LOGGER.info("Entering: I should see a semi-prepopulated page to enter my information");
        checkoutPage.pageIsLoaded().onCheckoutPage();
        checkoutPage.checkForPrepopulated("John Smith");
        checkoutPage.enterInformation("4055011111111111", "123", "1115", null);
    }

    // Scenario 3 also uses scenario 1 steps
    @When("^I enter bad information and click submit$")
    public void enter_bad_information() {
        LOGGER.info("Entering: I enter bad information and click submit");
        checkoutPage.pageIsLoaded().enterInformation("4055011111111000", "123", "1115", "John Doe");
    }

    @Then("^I should see an error on the page$")
    public void see_error_on_page() {
        LOGGER.info("Entering: I should see an error on the page");
        checkoutPage.checkForError(true);
    }

}
