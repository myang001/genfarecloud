package com.genfare.cloud.converters;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.genfare.cloud.model.converters.OrderTypeToOrderConverter;
import com.genfare.cloud.model.entity.Orderitems;
import com.genfare.cloud.model.entity.Orders;
import com.genfare.cloud.model.entity.Orderstatuses;

public class OrderTypeToOrderConverterTest {

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void test() {
        try {
            File file = new File("src/test/resources/test.order.xml");
            BufferedReader br = new BufferedReader(new FileReader(file));
            String xml = "";
            while (true) {
                String str = br.readLine();
                if (str == null)
                    break;
                xml += str + "\n";
            }
            System.out.println(xml);

            OrderTypeToOrderConverter c = new OrderTypeToOrderConverter();
            Orders order = c.convert(xml);

            int id = order.getOrderId();
            assertEquals(id, 12);

            Orderstatuses status = order.getOrderstatuses();
            assertEquals(status.getDescription(), "Complete");

            Date date = order.getDateOrdered();
            assertEquals(date.getTime(), 1404238064000L);

            String amount = "" + order.getAmountOrder().floatValue();
            assertEquals(amount, "4.0");

            List<Orderitems> list = order.getOrderitemses();
            assertEquals(list.size(), 1);

            Orderitems oi = list.get(0);

            int parentItemNumber = oi.getParentItemNumber();
            assertEquals(parentItemNumber, 0);

            int qty = oi.getQuantity();
            assertEquals(qty, 1);

            String price = "" + oi.getUnitPrice().floatValue();
            assertEquals(price, "4.0");

        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

}
