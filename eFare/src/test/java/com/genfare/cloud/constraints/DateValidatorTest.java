package com.genfare.cloud.constraints;

import static org.junit.Assert.assertEquals;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.BeforeClass;
import org.junit.Test;

public class DateValidatorTest {

    private static DateRangeValidator dr;

    private static DateValidatorForm form;

    protected static final DateTimeFormatter formatter = DateTimeFormat.forPattern("MM/dd/yyyy");

    public class DateValidatorForm {
        public String begDate;

        public String endDate;

        public String range;

        public DateValidatorForm(String begDate, String endDate, String range) {
            this.begDate = begDate;
            this.endDate = endDate;
            this.range = range;
        }

        public String getBegDate() {
            return begDate;
        }

        public String getEndDate() {
            return endDate;
        }

        public String getRange() {
            return range;
        }
    }

    @BeforeClass
    public static void setUp() {
        dr = new DateRangeValidator("begDate", "endDate", "range");
    }

    @Test
    public void testEndDateFuture() {
        form =
            new DateValidatorForm(new DateTime().withTime(23, 59, 59, 999).toString("MM/dd/yyyy"), DateTime.now().plusMonths(3).withTimeAtStartOfDay().toString("MM/dd/yyyy"), "3");
        boolean a = dr.isValid(form, null);
        assertEquals(a, false);
    }

    @Test
    public void testStartDateFuture() {
        form =
            new DateValidatorForm(DateTime.now().plusMonths(3).withTimeAtStartOfDay().toString("MM/dd/yyyy"), new DateTime().withTime(23, 59, 59, 999).toString("MM/dd/yyyy"), "3");
        boolean a = dr.isValid(form, null);
        assertEquals(a, false);
    }

    @Test
    public void testFourMonthRange() {
        form =
            new DateValidatorForm(DateTime.now().minusMonths(4).withTimeAtStartOfDay().toString("MM/dd/yyyy"), new DateTime().withTime(23, 59, 59, 999).toString("MM/dd/yyyy"), "3");
        boolean a = dr.isValid(form, null);
        assertEquals(a, false);
    }

    @Test
    public void testDateValid() {
        form =
            new DateValidatorForm(DateTime.now().minusMonths(3).withTimeAtStartOfDay().toString("MM/dd/yyyy"), new DateTime().withTime(23, 59, 59, 999).toString("MM/dd/yyyy"), "3");
        boolean a = dr.isValid(form, null);
        assertEquals(a, true);
    }

}
