package com.genfare.cloud.constraints;

import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;

public class FieldMatchValidatorTest {

    private static FieldMatchValidator fv;

    private static fieldMatchForm value;

    public class fieldMatchForm {
        public String first;

        public String second;

        public fieldMatchForm(String first, String second) {
            this.first = first;
            this.second = second;
        }

        public String getFirst() {
            return first;
        }

        public String getSecond() {
            return second;
        }
    }

    @BeforeClass
    public static void setUp() {
        fv = new FieldMatchValidator("first", "second");
    }

    @Test
    public void testDateValid() {
        value = new fieldMatchForm("abc", "abc");
        boolean a = fv.isValid(value, null);
        assertEquals(a, true);
    }

    @Test
    public void testDateNotValid() {
        value = new fieldMatchForm("abc", "123");
        boolean a = fv.isValid(value, null);
        assertEquals(a, false);
    }

}
