#encoding: utf-8
@checkout @all
Feature: Users can checkout after shopping
 
Scenario: Successful Guest Checkout
	Given I have a guest order started and am on the cart page
	When I select Checkout
	Then I should see a page to enter my shipping information
	When I enter my shipping information and select Checkout
	Then I should see a page to enter my payment information
	When I enter my payment information and click submit
	Then I should see that I placed my order successfully
	
Scenario: Unsuccessful Checkout
	Given I have a guest order started and am on the cart page
	When I select Checkout
	Then I should see a page to enter my shipping information
	When I enter my shipping information and select Checkout
	Then I should see a page to enter my payment information
	When I enter bad information and click submit
	Then I should see an error on the page
	
Scenario: Successful User Checkout
	Given I am logged in and I have an order started and am on the cart page
	When I select Checkout
	Then I should see a page to enter my shipping information
	When I enter my shipping information and select Checkout
	Then I should see a semi-prepopulated page to enter my information 
	Then I should see that I placed my order successfully
	