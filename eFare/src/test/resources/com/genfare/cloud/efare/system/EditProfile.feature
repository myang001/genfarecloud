
#encoding: utf-8
@editProfile @all
Feature: Users can edit profile information
 
Scenario: Edit profile success
    Given I am logged in to the application
    And I navigate to my profile page
    When I try to edit profile with valid parameters
    Then I should not see that there are edit profile form errors
    And user should be updated successfully
    
Scenario: Edit profile failure 
	Given I am logged in to the application
	And I navigate to my profile page
	When I try to edit profile with invalid parameters
	Then I should see that there are edit profile form errors
	