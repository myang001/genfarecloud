#encoding: utf-8
@cardSearch @all
Feature: Users and Guest can search their cards successfully
 
Scenario: Card Found
	Given I navigate to the search card page
	When I try to search for a valid card
	Then I should see that a card is found
	
Scenario: Card Not Found
	Given I navigate to the search card page
	When I try to search for an invalid card
	Then I should see that a card is not found
	
