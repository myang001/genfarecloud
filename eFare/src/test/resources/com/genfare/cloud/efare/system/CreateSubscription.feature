#encoding: utf-8
@subscriptions @all

Feature: Enable subscription/autobuy
		
	Background: I am logged in
		Given I am logged in
		
	Scenario Outline: On Setup Autobuy page, enter date that is in a certain format
		Given I am on Setup Autobuy page/prompt
		When I enter date in format '<fmt>'
		And I press the continue button
		Then I should be redirected to the '<outcome>' screen
			
		Examples:
			|       fmt      | outcome |
			|    10/02/15    | payment |
			|   06/09/2016   | payment |
			|October 14, 2015| failure |
			|    13/08/15    | failure |
			|    06/09/92    | failure |
	
	Scenario: On Setup Autobuy page, click Back button
		Given I am on Setup Autobuy page/prompt
		When I press the back button
		Then I should be taken back to the Card Details page
		
	Scenario: On Setup Autobuy page, click Cancel button
		Given I am on Setup Autobuy page/prompt
		When I press the cancel button
		Then I should be taken to my account page
		
	Scenario: On Payment page, click Back button
		Given I am on Payment page/prompt
		When I press the back button
		Then I should be taken back to the Setup Autobuy page
		
	Scenario: On Payment page, click Cancel button
		Given I am on Payment page/prompt
		When I press the cancel button
		Then I should be taken to my account page
		
	Scenario: On Payment page, click Start Autobuy button
		Given I am on Payment page/prompt
		When I press Setup a Card Now
		Then I should be taken to the Credit Card info page
		When I submit my card information
		And I press the Start Autobuy button
		Then I should be taken to Complete page
