#encoding: utf-8
@signup @all
Feature: Guests can signup successfully
 
Scenario: Signup Success
	Given I navigate to the signup page
	When I try to signup with valid form parameters
	Then I should see that I signed up successfully
	Then I should see that I received an email to verify my email address
	When I verify my email address 
		And navigate to the login page
		And enter valid credentials
	Then I should see that I signed in successfully with my credentials
	
Scenario: Signup Errors
	Given I navigate to the signup page
	When I try to signup with invalid form parameters
	Then I should see that there are form errors
	
