#encoding: utf-8
@replaceCard @all
Feature: Guests can replace card successfully
 
Scenario: Replacement Success
	Given I am logged in and viewing the Replace Card Page
	When I choose a valid card name and reason
	Then I should checkout successfully
	