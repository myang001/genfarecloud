#encoding: utf-8
@shoppingCart @all
Feature: Guests can use the shopping cart
 
Scenario: Add product to cart
	Given I have selected a card to add products to
	When I add a product for the card to the cart
	Then I should see that the product was added to the cart
	
Scenario: Remove product from cart
	Given I have started an order and am on the cart page
	When I try to remove a product from the cart
	Then I should see that the product was removed successfully
	
Scenario: Alter quantity of a product in a cart
	Given I have started an order and navigate to the cart page
	When I try to alter the quantity of a product
	Then I should see the order is updated
	
