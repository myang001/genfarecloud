#encoding: utf-8
@productSelect @all
Feature: Users and Guest can view products available for purchase
 
Scenario: View Products for New Card
	Given I navigate to the select card page
	When I click on card
	Then I should see the confirmation page
	
Scenario: View Products for Existing Card
	Given I have successfully searched a card
	When I click add products
	Then I should see the products page for my card

