#encoding: utf-8
@registerCard @all
Feature: Users can register their cards 

Scenario: Successful Card Registration
	Given I am logged in and on the Card Registration page 
	When I enter a nickname and search a valid card number and CVV number
	Then I should see the card was registered successfully
	
Scenario: Unsuccessful Card Registration
	Given I am logged in and on the Card Registration page 
	When I enter a nickname and search an invalid card number or CVV number
	Then I should see an error for invalid input

 