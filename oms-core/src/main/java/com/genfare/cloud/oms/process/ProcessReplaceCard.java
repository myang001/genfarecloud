package com.genfare.cloud.oms.process;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.xml.sax.SAXException;

import com.genfare.cloud.model.entity.Activitydetails;
import com.genfare.cloud.model.entity.Activitytypes;
import com.genfare.cloud.model.entity.Farecodes;
import com.genfare.cloud.model.entity.Orderitemactions;
import com.genfare.cloud.model.entity.Orderitems;
import com.genfare.cloud.model.entity.Pendingactivity;
import com.genfare.cloud.model.entity.Tickets;
import com.genfare.cloud.model.entity.Wallets;
import com.genfare.cloud.model.entity.repository.ActivitytypesRepository;
import com.genfare.cloud.model.entity.repository.OrderitemsRepository;
import com.genfare.cloud.model.entity.repository.PendingactivityRepository;
import com.genfare.cloud.model.types.ActivitytypesType;
import com.genfare.cloud.model.types.OrderItemStatusType;
import com.genfare.cloud.model.types.OrderitemactionsType;
import com.genfare.cloud.oms.util.MessageProcessingUtil;

@Component
public class ProcessReplaceCard extends Processor {

    @Autowired
    MessageProcessingUtil processingUtil;
    @Autowired
    OrderitemsRepository orderItemsRepo;
    @Autowired
    PendingactivityRepository pendingactivityRepo;
    @Autowired
    ActivitytypesRepository activitytypesRepo;

    private static final Logger LOGGER = LoggerFactory.getLogger(ProcessReplaceCard.class);

    @Override
    public List<Pendingactivity> transformMessage(String body) throws NumberFormatException, XPathExpressionException, ParserConfigurationException, SAXException, IOException {
        LOGGER.info("Setting the order items statuses to Fulfillment");
        Integer orderId = Integer.parseInt(processingUtil.evaluateXPath("//OrderManagementAPI/Order/@OrderID", body));
        List<Orderitems> items = orderItemsRepo.findByOrderId(orderId);
        List<Pendingactivity> pendingactivityList = new ArrayList<Pendingactivity>();
        for (Orderitems item : items) {
            Orderitemactions orderitemactions = item.getOrderitemactions();
            int replaceTypeIndex = OrderitemactionsType.REPLACE.getIndex();
            if (orderitemactions.getOrderItemActionId() != replaceTypeIndex)
                continue;
            item.setOrderstatuses(OrderItemStatusType.PENDING_ACTIVITY.getStatus());
            orderItemsRepo.save(item);

            Tickets tickets = item.getTickets();
            Farecodes farecodes = item.getFarecodes();
            BigDecimal valueActivity = null;
            Wallets wallets = item.getWallets();
            byte loadSequence = 0;
            Activitytypes activitytypes = ActivitytypesType.CARD_BADLIST.getType();
            byte slot = 0;
            Byte pendingStatus = 0;
            Date dateAdded = new Date();
            Date dateEffective = new Date();
            Date dateApplied = new Date();
            Date dateExpires = null;
            String reason = null;
            Set<Activitydetails> activitydetailses = null;

            Pendingactivity pendingactivity = new Pendingactivity(tickets, item, farecodes,
                valueActivity, wallets, loadSequence, activitytypes,
                slot, pendingStatus, dateAdded, dateEffective, dateApplied,
                dateExpires, reason, activitydetailses);
            pendingactivityRepo.save(pendingactivity);
            pendingactivityList.add(pendingactivity);
        }
        return pendingactivityList;
    }

}
