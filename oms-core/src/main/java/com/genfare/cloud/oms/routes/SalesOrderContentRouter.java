package com.genfare.cloud.oms.routes;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.builder.xml.Namespaces;
import org.springframework.stereotype.Component;

@Component
public class SalesOrderContentRouter extends RouteBuilder {

    @Override
    public void configure() throws Exception {
        Namespaces genfareCloudOM = new Namespaces("om", "http://genfare.com/cloud/model/om");

        from("direct:salesOrder")
            .choice()
            .when(genfareCloudOM
                .xpath("/om:OrderManagementAPI/om:Order/om:OrderDetails/om:OrderItem/om:Wallet"))
            .to("direct:ticketOrder")
            .otherwise().to("log:com.genfare.cloud.sales.unknown?level=INFO");
    }

}
