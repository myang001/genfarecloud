package com.genfare.cloud.oms.process;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.xml.sax.SAXException;

public abstract class Processor {

    public void process(Exchange exchange) throws NumberFormatException, XPathExpressionException, ParserConfigurationException, SAXException, IOException, InvalidKeyException,
        NoSuchAlgorithmException, URISyntaxException {
        Message message = exchange.getIn();
        String body = message.getBody(String.class);
        message.setBody(transformMessage(body));
        exchange.setIn(message);
    }

    public abstract Object transformMessage(String body) throws NumberFormatException, XPathExpressionException, ParserConfigurationException, SAXException, IOException,
        InvalidKeyException, NoSuchAlgorithmException, URISyntaxException;

}
