package com.genfare.cloud.oms.routes;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.genfare.cloud.oms.process.ProcessSubscription;

@Component
public class ModifySubscription extends RouteBuilder {

    @Autowired
    ProcessSubscription processSubscription;

    @Override
    public void configure() throws Exception {

        from("direct:modifysubscription")
            .multicast().to("direct:processModifySubscription", "direct:processOrderItem");

        from("direct:processModifySubscription")
            .bean(processSubscription, "process")
            .to("log:com.genfare.cloud.modifySubscription");
    }

}
