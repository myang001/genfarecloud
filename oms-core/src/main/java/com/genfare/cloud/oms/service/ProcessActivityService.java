package com.genfare.cloud.oms.service;

import com.genfare.cloud.model.entity.Orderitems;
import com.genfare.cloud.model.entity.Pendingactivity;

public interface ProcessActivityService {

    public Pendingactivity processWalletActivity(Orderitems item);

    public Pendingactivity processSubscriptionActivity(Long walletContentId);

}
