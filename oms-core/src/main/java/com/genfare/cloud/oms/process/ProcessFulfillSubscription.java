package com.genfare.cloud.oms.process;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.xml.sax.SAXException;

import com.genfare.cloud.model.entity.Pendingactivity;
import com.genfare.cloud.model.entity.Subscriptions;
import com.genfare.cloud.model.entity.repository.SubscriptionsRepository;
import com.genfare.cloud.oms.service.ProcessActivityService;
import com.genfare.cloud.oms.service.SubscriptionProcessingService;
import com.genfare.cloud.oms.util.MessageProcessingUtil;

@Component
public class ProcessFulfillSubscription extends Processor {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProcessFulfillSubscription.class);
    @Autowired
    ProcessActivityService activityService;
    @Autowired
    SubscriptionsRepository subscriptionsRepository;
    @Autowired
    MessageProcessingUtil processingUtil;
    @Autowired
    SubscriptionProcessingService subscriptionProcessor;

    @Override
    public Pendingactivity transformMessage(String body) throws NumberFormatException, XPathExpressionException, ParserConfigurationException, SAXException, IOException,
        InvalidKeyException, NoSuchAlgorithmException, URISyntaxException {
        LOGGER.info("Processing a Subscription Fulfillment");
        Subscriptions subscription = subscriptionsRepository.findOne(new Long(processingUtil.evaluateXPath("//SubscriptionAPI/Id", body)));
        subscriptionProcessor.fulfillSubscription(subscription, processingUtil.evaluateXPath("//SubscriptionAPI/@SchemaName", body));
        Long wc = subscription.getWalletcontents().getWalletContentsId();
        return activityService.processSubscriptionActivity(wc);
    }

}
