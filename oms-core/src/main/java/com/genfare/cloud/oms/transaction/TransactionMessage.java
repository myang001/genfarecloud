package com.genfare.cloud.oms.transaction;

public class TransactionMessage {

    private float amount;
    private String gateway_id;
    private String cc_expiry;
    private String transaction_type;
    private String cardholder_name;
    private String password;
    private String transarmor_token;
    private String credit_card_type;

    public TransactionMessage(String transaction_type, String gateway_id, String password, float amount, String cardholder_name,
        String cc_expiry, String transarmor_token, String credit_card_type) {
        this.transaction_type = transaction_type;
        this.gateway_id = gateway_id;
        this.password = password;
        this.amount = amount;
        this.cardholder_name = cardholder_name;
        this.cc_expiry = cc_expiry;
        this.transarmor_token = transarmor_token;
        this.credit_card_type = credit_card_type;
    }

    public String getCredit_card_type() {
        return credit_card_type;
    }

    public void setCredit_card_type(String credit_card_type) {
        this.credit_card_type = credit_card_type;
    }

    public String getTransarmor_token() {
        return transarmor_token;
    }

    public void setTransarmor_token(String transarmor_token) {
        this.transarmor_token = transarmor_token;
    }

    public String getCc_expiry() {
        return cc_expiry;
    }

    public void setCc_expiry(String cc_expiry) {
        this.cc_expiry = cc_expiry;
    }

    public String getCardholder_name() {
        return cardholder_name;
    }

    public void setCardholder_name(String cardholder_name) {
        this.cardholder_name = cardholder_name;
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getGateway_id() {
        return gateway_id;
    }

    public void setGateway_id(String gateway_id) {
        this.gateway_id = gateway_id;
    }

    public String getTransaction_type() {
        return transaction_type;
    }

    public void setTransaction_type(String transaction_type) {
        this.transaction_type = transaction_type;
    }

}
