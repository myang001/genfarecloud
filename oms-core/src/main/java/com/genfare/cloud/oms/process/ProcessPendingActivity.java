package com.genfare.cloud.oms.process;

import java.io.IOException;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.xml.sax.SAXException;
import com.genfare.cloud.model.entity.Orderitems;
import com.genfare.cloud.model.entity.repository.OrderitemsRepository;
import com.genfare.cloud.oms.service.ProcessActivityService;
import com.genfare.cloud.oms.util.MessageProcessingUtil;

@Component
public class ProcessPendingActivity extends Processor {

    @Autowired
    MessageProcessingUtil processingUtil;
    @Autowired
    OrderitemsRepository orderItemsRepo;
    @Autowired
    ProcessActivityService activityService;

    private static final Logger LOGGER = LoggerFactory.getLogger(ProcessPendingActivity.class);

    @Override
    public List<Orderitems> transformMessage(String body) throws NumberFormatException, XPathExpressionException, ParserConfigurationException, SAXException, IOException {
        LOGGER.info("Processing a pending activity");
        Integer orderId = Integer.parseInt(processingUtil.evaluateXPath("//OrderManagementAPI/Order/@OrderID", body));
        List<Orderitems> items = orderItemsRepo.findByOrderId(orderId);
        for (Orderitems i : items) {
            String test = processingUtil.evaluateXPath("//OrderManagementAPI/Order/OrderDetails/OrderItem[@OrderItemNumber=" + i.getId().getOrderItemNumber() + "]/Ticket", body);
            if ((test != null) && !test.isEmpty()) {
                activityService.processWalletActivity(i);
            }
            // else ignore as it is existing card
        }
        return items;
    }

}
