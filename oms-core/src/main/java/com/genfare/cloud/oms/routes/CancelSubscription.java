package com.genfare.cloud.oms.routes;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.genfare.cloud.oms.process.ProcessCancelSubscription;

@Component
public class CancelSubscription extends RouteBuilder {

    @Autowired
    ProcessCancelSubscription cancelSubscription;

    @Override
    public void configure() throws Exception {

        from("direct:cancelsubscription")
            .multicast().to("direct:processCancelSubscription", "direct:processOrderItem");

        from("direct:processCancelSubscription")
            .bean(cancelSubscription, "process")
            .to("log:com.genfare.cloud.cancelSubscription");
    }

}
