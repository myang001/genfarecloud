package com.genfare.cloud.oms.service;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Collections;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJacksonHttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.xml.sax.SAXException;

import com.genfare.cloud.model.converters.DateToStringConverter;
import com.genfare.cloud.model.converters.StringToDateConverter;
import com.genfare.cloud.model.entity.Peoplecards;
import com.genfare.cloud.model.entity.PeoplecardsId;
import com.genfare.cloud.model.entity.Subscriptions;
import com.genfare.cloud.model.entity.Subscriptionstatus;
import com.genfare.cloud.model.entity.repository.CreditcardactivityRepository;
import com.genfare.cloud.model.entity.repository.PeoplecardsRepository;
import com.genfare.cloud.model.entity.repository.SubscriptionsRepository;
import com.genfare.cloud.model.entity.repository.WalletcontentsRepository;
import com.genfare.cloud.model.security.User;
import com.genfare.cloud.model.service.HashingService;
import com.genfare.cloud.model.tenant.TenantConfiguration;
import com.genfare.cloud.model.tenant.TenantConfigurationRepository;
import com.genfare.cloud.model.types.SubscriptionstatusType;
import com.genfare.cloud.oms.transaction.TransactionMessage;
import com.genfare.cloud.oms.util.MessageProcessingUtil;

@Component
public class SubscriptionProcessingServiceImpl implements SubscriptionProcessingService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SubscriptionProcessingServiceImpl.class);

    @Autowired
    SubscriptionsRepository subscriptionsRepository;
    @Autowired
    PeoplecardsRepository cardRepository;
    @Autowired
    WalletcontentsRepository walletContentsRepository;
    @Autowired
    PeoplecardsRepository peoplecardsRepository;
    @Autowired
    CreditcardactivityRepository creditcardactivityRepository;
    @Autowired
    HashingService hasher;
    @Autowired
    TenantConfigurationRepository tenantConfig;
    @Autowired
    MessageProcessingUtil processingUtil;

    @Override
    public Subscriptions createOrModifySubscription(JSONObject obj, Integer personId) {
        Peoplecards card = cardRepository.findOne(new PeoplecardsId(personId, obj.getInt("peoplecardsId")));
        String userLogin = processingUtil.getUser(personId).getLogin();
        JSONObject replenishDetails = (JSONObject) (obj.get("replenishDetails"));
        Long walletContentId = obj.getLong("walletcontentsId");
        Subscriptions subscription = null;
        if (subscriptionsRepository.findByWalletContentsId(walletContentId) != null) {
            LOGGER.info("Modifying a subscription");
            subscription = subscriptionsRepository.findByWalletContentsId(walletContentId);
            Subscriptionstatus status = SubscriptionstatusType.ACTIVE.getStatus();
            subscription.setSubscriptionstatus(status);
        } else {
            LOGGER.info("Creating a new subscription");
            Subscriptionstatus status = SubscriptionstatusType.NEW.getStatus();
            subscription = new Subscriptions(walletContentsRepository.findOne(walletContentId), status, card, card.getPaymenttypes());
            subscription.setCreatedBy(userLogin);
            subscription.setReplenishPrice(new BigDecimal(replenishDetails.getString("price")));
            subscription.setReplenishValue(new BigDecimal(replenishDetails.getString("value")));
        }
        subscription.setReplenishThreshold(new BigDecimal(replenishDetails.getString("threshold")));
        subscription.setDateReplenishExpires(new StringToDateConverter().convert(replenishDetails.getString("expiration")));
        subscription.setTouchedBy(userLogin);

        subscriptionsRepository.save(subscription);
        return subscription;
    }

    @Override
    public String disableSubscription(Long walletContentId) throws JSONException, XPathExpressionException, ParserConfigurationException, SAXException, IOException {
        LOGGER.info("Canceling a Subscription");
        Subscriptions subscription = subscriptionsRepository.findByWalletContentsId(walletContentId);
        subscription.setSubscriptionstatus(SubscriptionstatusType.CANCELLED.getStatus());
        subscriptionsRepository.save(subscription);
        return "cancel Subscription()";
    }

    @Override
    public String fulfillSubscription(Subscriptions s, String schemaName) throws UnsupportedEncodingException, NoSuchAlgorithmException, InvalidKeyException, URISyntaxException {
        TenantConfiguration tenant = tenantConfig.getBySchemaName(schemaName);
        String uri = "/transaction/v12";
        User user = processingUtil.getUser(s.getPeople().getPersonId());
        Peoplecards pc = s.getPeoplecards();
        Integer personId = pc.getId().getPersonId();
        Integer cardNum = pc.getId().getCardNumber();
        DateToStringConverter converter = new DateToStringConverter();
        String fullExpires = converter.convert(peoplecardsRepository.getExpirationForPeopleCard(personId, cardNum));

        TransactionMessage message =
            new TransactionMessage("00", tenant.getCreditServiceGatewayId(), tenant.getCreditServiceAPIPassword(), s.getReplenishPrice().floatValue(), user.getFirstName() + " "
                + user.getLastName(), fullExpires.substring(0, 2) + fullExpires.substring(3, 5), peoplecardsRepository.getTokenForPeopleCard(personId, cardNum),
                peoplecardsRepository.getPaymentTypeForPeopleCard(personId, cardNum));
        JSONObject json = new JSONObject(message);
        String input = json.toString();
        LOGGER.info("Message body is: " + input);
        String content_sha1 = hasher.sha1Hash(input);
        String gge4_date = new DateTime(DateTimeZone.UTC).toString("yyyy-MM-ddHH:mm:ssZ");
        gge4_date = gge4_date.substring(0, 10) + "T" + gge4_date.substring(10, 18) + "Z";
        String hash_data = "POST\napplication/json\n" + content_sha1 + "\n" + gge4_date + "\n" + uri;
        String authorization = hasher.hmacSha1("lMNYbtyFX5E6one_y1v0pPtw4VdqDds3", hash_data);
        URI url = new URI(tenant.getCreditServiceAPIURL() + uri);

        RestTemplate template = new RestTemplate();
        template.getMessageConverters().add(new MappingJacksonHttpMessageConverter());
        template.getMessageConverters().add(new StringHttpMessageConverter());

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(new MediaType("application", "json"));
        headers.setAccept(Collections.singletonList(new MediaType("text", "xml")));
        headers.add("x-gge4-date", gge4_date);
        headers.add("x-gge4-content-sha1", content_sha1);
        headers.add("authorization", "GGE4_API 128552:" + authorization);
        HttpEntity<String> requestEntity = new HttpEntity<String>(input, headers);
        ResponseEntity<String> responseEntity = template.exchange(url, HttpMethod.POST, requestEntity, String.class);
        String result = responseEntity.getBody();
        LOGGER.info("Results are: " + result);
        return result;

    }
}
