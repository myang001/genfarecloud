package com.genfare.cloud.oms.routes;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.builder.xml.Namespaces;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.genfare.cloud.oms.process.InitialOrderProcessor;

@Component
public class OrderContentRouter extends RouteBuilder {

    @Autowired
    InitialOrderProcessor initialProcessor;

    @Override
    public void configure() throws Exception {
        Namespaces genfareCloudOM = new Namespaces("om",
            "http://genfare.com/cloud/model/om");

        from(
            "aws-sqs://"
                + System.getProperty("aws.Environment")
                + "-order_publish_core?amazonSQSClient=#sqsClient&region=us-east-1")
            .bean(initialProcessor, "process")
            .choice()
            .when(genfareCloudOM
                .xpath("/om:OrderManagementAPI/om:Order/om:OrderHeader/om:Ordertype/om:Description=\"Service Order\""))
            .to("direct:serviceOrder")
            .when(genfareCloudOM
                .xpath("/om:OrderManagementAPI/om:Order/om:OrderHeader/om:Ordertype/om:Description=\"Sales Order\""))
            .to("direct:salesOrder").otherwise()
            .to("log:com.genfare.cloud.order.unknown?level=ERROR");
    }

}
