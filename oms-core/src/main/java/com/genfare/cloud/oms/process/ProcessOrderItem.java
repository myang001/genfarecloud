package com.genfare.cloud.oms.process;

import java.io.IOException;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.xml.sax.SAXException;
import com.genfare.cloud.model.entity.Orderitems;
import com.genfare.cloud.model.entity.repository.OrderitemsRepository;
import com.genfare.cloud.model.types.OrderItemStatusType;
import com.genfare.cloud.oms.util.MessageProcessingUtil;

@Component
public class ProcessOrderItem extends Processor {

    @Autowired
    MessageProcessingUtil processingUtil;
    @Autowired
    OrderitemsRepository orderItemsRepo;

    private static final Logger LOGGER = LoggerFactory.getLogger(ProcessOrderItem.class);

    @Override
    public List<Orderitems> transformMessage(String body) throws NumberFormatException, XPathExpressionException, ParserConfigurationException, SAXException, IOException {
        LOGGER.info("Setting the order items statuses to Fulfillment");
        Integer orderId = Integer.parseInt(processingUtil.evaluateXPath("//OrderManagementAPI/Order/@OrderID", body));
        List<Orderitems> items = orderItemsRepo.findByOrderId(orderId);
        for (Orderitems item : items) {
            if (processingUtil.evaluateXPath("//OrderManagementAPI/Order/OrderHeader/Ordertype/Description", body).equals("Service Order")) {
                item.setOrderstatuses(OrderItemStatusType.COMPLETE.getStatus());
            }
            else {
                item.setOrderstatuses(OrderItemStatusType.FULFILLMENT.getStatus());
            }
            orderItemsRepo.save(item);
        }
        return items;
    }

}
