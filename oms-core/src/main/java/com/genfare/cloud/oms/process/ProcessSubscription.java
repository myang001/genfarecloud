package com.genfare.cloud.oms.process;

import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.xml.sax.SAXException;
import com.genfare.cloud.model.entity.Subscriptions;
import com.genfare.cloud.model.entity.repository.PeopleRepository;
import com.genfare.cloud.oms.service.SubscriptionProcessingService;
import com.genfare.cloud.oms.util.MessageProcessingUtil;

@Component
public class ProcessSubscription extends Processor {

    @Autowired
    MessageProcessingUtil processingUtil;
    @Autowired
    PeopleRepository peopleRepository;
    @Autowired
    SubscriptionProcessingService subscriptionService;

    private static final Logger LOGGER = LoggerFactory.getLogger(ProcessSubscription.class);

    @Override
    public Subscriptions transformMessage(String body) throws XPathExpressionException, ParserConfigurationException, SAXException, IOException {
        LOGGER.info("Creating or modifying a subscription");
        JSONObject obj = new JSONObject(processingUtil.evaluateXPath("//OrderManagementAPI/Order/OrderHeader/Data", body));
        Integer personId = peopleRepository.findPersonByOrder(Integer.parseInt(processingUtil.evaluateXPath("//OrderManagementAPI/Order/@OrderID", body))).getPersonId();
        return subscriptionService.createOrModifySubscription(obj, personId);
    }

}
