package com.genfare.cloud.oms.process;

import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.xml.sax.SAXException;
import com.genfare.cloud.oms.service.SubscriptionProcessingService;
import com.genfare.cloud.oms.util.MessageProcessingUtil;

@Component
public class ProcessCancelSubscription extends Processor {

    @Autowired
    SubscriptionProcessingService subscriptionService;
    @Autowired
    MessageProcessingUtil processingUtil;

    @Override
    public String transformMessage(String body) throws JSONException, XPathExpressionException, ParserConfigurationException, SAXException, IOException {
        JSONObject obj = new JSONObject(processingUtil.evaluateXPath("//OrderManagementAPI/Order/OrderHeader/Data", body));
        Long wc = obj.getLong("walletcontentsId");
        return subscriptionService.disableSubscription(wc);
    }

}
