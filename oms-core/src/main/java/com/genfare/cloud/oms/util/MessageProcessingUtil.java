package com.genfare.cloud.oms.util;

import java.io.IOException;
import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.genfare.cloud.model.converters.PeopleToUserConverter;
import com.genfare.cloud.model.entity.repository.PeopleRepository;
import com.genfare.cloud.model.security.User;

@Component
@Transactional
public class MessageProcessingUtil {

    @Autowired
    PeopleRepository peopleRepository;

    public String evaluateXPath(String xp, String messageBody) throws ParserConfigurationException, SAXException, IOException, XPathExpressionException {
        DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        XPath xpath = XPathFactory.newInstance().newXPath();
        Document document = builder.parse(new InputSource(new StringReader(messageBody)));
        return xpath.evaluate(xp, document);
    }

    public String extractXML(String original, String XML_BEGIN, String XML_END) {
        int startIdx = original.indexOf(XML_BEGIN);
        int endIdx = original.lastIndexOf(XML_END);

        if (startIdx == -1 || endIdx == -1) {
            return original;
        }

        return original.substring(startIdx, endIdx + XML_END.length());
    }

    public User getUser(Integer id) {
        PeopleToUserConverter people2user = new PeopleToUserConverter();
        User user = people2user.convert(peopleRepository.findOne(id));
        return user;
    }
}
