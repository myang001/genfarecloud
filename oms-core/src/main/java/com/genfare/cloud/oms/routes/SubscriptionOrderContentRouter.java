package com.genfare.cloud.oms.routes;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.builder.xml.Namespaces;
import org.springframework.stereotype.Component;

@Component
public class SubscriptionOrderContentRouter extends RouteBuilder {

    @Override
    public void configure() throws Exception {
        Namespaces genfareCloudOM = new Namespaces("om", "http://genfare.com/cloud/model/om");

        from("direct:subscription")
            .choice()
            .when(genfareCloudOM
                .xpath("/om:OrderManagementAPI/om:Order/om:OrderDetails/om:OrderItem/om:Action=\"New\""))
            .to("log:com.genfare.cloud.service.subscription.new?level=INFO")
            .to("direct:newsubscription")
            .when(genfareCloudOM
                .xpath("/om:OrderManagementAPI/om:Order/om:OrderDetails/om:OrderItem/om:Action=\"Change\""))
            .to("log:com.genfare.cloud.service.subscription.modify?level=INFO")
            .to("direct:modifysubscription")
            .when(genfareCloudOM
                .xpath("/om:OrderManagementAPI/om:Order/om:OrderDetails/om:OrderItem/om:Action=\"Cancel\""))
            .to("log:com.genfare.cloud.service.subscription.cancel?level=INFO")
            .to("direct:cancelsubscription")
            .otherwise().to("log:com.genfare.cloud.service.subscription.unknown?level=INFO");
    }

}
