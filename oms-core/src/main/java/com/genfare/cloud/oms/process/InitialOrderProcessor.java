package com.genfare.cloud.oms.process;

import java.io.UnsupportedEncodingException;
import org.apache.commons.codec.binary.Base64;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.genfare.cloud.oms.util.MessageProcessingUtil;

@Component
public class InitialOrderProcessor extends Processor {

    @Autowired
    MessageProcessingUtil processingUtil;

    @Override
    public String transformMessage(String body) throws UnsupportedEncodingException {
        body = body.replace("\r\n", "");
        JSONObject obj = new JSONObject(body);
        String xmlMessage = obj.getString("Message");
        byte[] decoded = Base64.decodeBase64(xmlMessage.getBytes());
        body = new String(decoded, "UTF-8");
        return processingUtil.extractXML(body, "<?xml", "/OrderManagementAPI>");
    }
}
