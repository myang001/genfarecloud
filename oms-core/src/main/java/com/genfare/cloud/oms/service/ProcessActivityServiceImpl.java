package com.genfare.cloud.oms.service;

import java.math.BigDecimal;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.genfare.cloud.model.entity.Activitytypes;
import com.genfare.cloud.model.entity.Orderitems;
import com.genfare.cloud.model.entity.OrderitemsId;
import com.genfare.cloud.model.entity.Pendingactivity;
import com.genfare.cloud.model.entity.Walletcontents;
import com.genfare.cloud.model.entity.Wallets;
import com.genfare.cloud.model.entity.repository.OrderitemsRepository;
import com.genfare.cloud.model.entity.repository.PendingactivityRepository;
import com.genfare.cloud.model.entity.repository.WalletcontentsRepository;
import com.genfare.cloud.model.entity.repository.WalletsRepository;
import com.genfare.cloud.model.types.ActivitytypesType;
import com.genfare.cloud.oms.util.MessageProcessingUtil;

@Component
public class ProcessActivityServiceImpl implements ProcessActivityService {

    @Autowired
    OrderitemsRepository orderItemsRepo;
    @Autowired
    WalletcontentsRepository walletContentsRepository;
    @Autowired
    WalletsRepository walletRepository;
    @Autowired
    PendingactivityRepository activityRepository;
    @Autowired
    MessageProcessingUtil processingUtil;

    private static final Logger LOGGER = LoggerFactory.getLogger(ProcessActivityServiceImpl.class);

    @Override
    public Pendingactivity processWalletActivity(Orderitems item) {
        LOGGER.info("Processing wallet activity");
        Wallets wallet = orderItemsRepo.findOne(new OrderitemsId(item.getId().getOrderId(), item.getParentItemNumber())).getWallets();
        Activitytypes type = ActivitytypesType.ADDPRODUCT.getType();
        for (Walletcontents content : walletContentsRepository.findWalletContentForWallet(wallet.getWalletId())) {
            if (content.getTickets() == item.getTickets()) {
                type = ActivitytypesType.ADDVALUE.getType();
            }
        }
        Pendingactivity activity =
            new Pendingactivity(orderItemsRepo.findValueForOrderItem(item.getId().getOrderId(), item.getId().getOrderItemNumber()), wallet, getLoadSequence(wallet), type);
        activity.setTickets(item.getTickets());
        activity.setOrderitems(item);
        activity.setDateAdded(new Date());
        String userLogin = processingUtil.getUser(walletRepository.findPersonForWallet(wallet.getWalletId()).getPersonId()).getLogin();
        activity.setCreatedBy(userLogin);
        activity.setTouchedBy(userLogin);
        activityRepository.save(activity);
        return activity;
    }

    @Override
    public Pendingactivity processSubscriptionActivity(Long walletContentId) {
        LOGGER.info("Processing subscription activity");
        Wallets wallet = walletContentsRepository.findWalletsForWalletcontent(walletContentId);
        Pendingactivity activity = new Pendingactivity(new BigDecimal(0.00), wallet, getLoadSequence(wallet), ActivitytypesType.ADDVALUE.getType());
        activity.setTickets(walletContentsRepository.findTicketForWalletcontent(walletContentId));
        String userLogin = processingUtil.getUser(walletRepository.findPersonForWallet(wallet.getWalletId()).getPersonId()).getLogin();
        activity.setCreatedBy(userLogin);
        activity.setTouchedBy(userLogin);
        activityRepository.save(activity);
        return activity;
    }

    private byte getLoadSequence(Wallets wallet) {
        byte loadSeq = 0;
        if (activityRepository.getPendingActivityCountForWallets(wallet.getWalletId()) != null) {
            loadSeq = activityRepository.getPendingActivityCountForWallets(wallet.getWalletId()).byteValue();
        }
        return loadSeq;
    }
}
