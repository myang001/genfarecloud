package com.genfare.cloud.oms.routes;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.builder.xml.Namespaces;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.genfare.cloud.oms.process.ProcessOrderItem;
import com.genfare.cloud.oms.process.ProcessPendingActivity;

@Component
public class TicketOrderContentRouter extends RouteBuilder {

    @Autowired
    ProcessOrderItem processItem;
    @Autowired
    ProcessPendingActivity processActivity;

    @Override
    public void configure() throws Exception {
        Namespaces genfareCloudOM = new Namespaces("om", "http://genfare.com/cloud/model/om");

        from("direct:ticketOrder")
            .choice()
            .when(genfareCloudOM
                .xpath("/om:OrderManagementAPI/om:Order/om:OrderDetails/om:OrderItem[om:Action=\"Change\" and om:Wallet]"))
            .multicast().to("direct:processWalletActivity", "direct:processOrderItem")
            .to("log:com.genfare.cloud.service.ticketOrder?level=INFO");

        from("direct:processWalletActivity")
            .bean(processActivity, "process");

        from("direct:processOrderItem")
            .bean(processItem, "process");
    }

}
