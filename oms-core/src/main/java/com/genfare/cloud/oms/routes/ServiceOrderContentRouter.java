package com.genfare.cloud.oms.routes;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.builder.xml.Namespaces;
import org.springframework.stereotype.Component;

@Component
public class ServiceOrderContentRouter extends RouteBuilder {

    @Override
    public void configure() throws Exception {
        Namespaces genfareCloudOM = new Namespaces("om", "http://genfare.com/cloud/model/om");

        from("direct:serviceOrder")
            .choice()
            .when(genfareCloudOM
                .xpath("/om:OrderManagementAPI/om:Order/om:OrderHeader//*[contains(., 'replenishDetails')]"))
            .to("direct:subscription")
            .when(genfareCloudOM
                .xpath("/om:OrderManagementAPI/om:Order/om:OrderDetails/om:OrderItem/om:Action=\"Replace\""))
            .to("log:com.genfare.cloud.service.subscription.replace?level=INFO")
            .to("direct:replacecard")
            .otherwise().to("log:com.genfare.cloud.service.unknown?level=INFO");
    }

}
