package com.genfare.cloud.oms.routes;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.genfare.cloud.oms.process.InitialSubscriptionProcessor;
import com.genfare.cloud.oms.process.ProcessFulfillSubscription;

@Component
public class SubscriptionFulfillmentRoute extends RouteBuilder {

    @Autowired
    InitialSubscriptionProcessor initialProcessor;

    @Autowired
    ProcessFulfillSubscription processSubscription;

    @Override
    public void configure() throws Exception {
        from(
            "aws-sqs://" + System.getProperty("aws.Environment") + "-subscription_publish_core?amazonSQSClient=#sqsClient&region=us-east-1")
            .bean(initialProcessor, "process")
            .bean(processSubscription, "process")
            .to("log:com.genfare.cloud.fulfillSubscription");
    }

}
