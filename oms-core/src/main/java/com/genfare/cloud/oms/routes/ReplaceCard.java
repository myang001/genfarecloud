package com.genfare.cloud.oms.routes;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.genfare.cloud.oms.process.ProcessReplaceCard;

@Component
public class ReplaceCard extends RouteBuilder {

    @Autowired
    ProcessReplaceCard replaceCard;

    @Override
    public void configure() throws Exception {

        from("direct:replacecard")
            .bean(replaceCard, "process")
            .to("log:com.genfare.cloud.replaceSubscription");
    }

}
