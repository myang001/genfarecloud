package com.genfare.cloud.oms.service;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.json.JSONException;
import org.json.JSONObject;
import org.xml.sax.SAXException;

import com.genfare.cloud.model.entity.Subscriptions;

public interface SubscriptionProcessingService {

    public Subscriptions createOrModifySubscription(JSONObject obj, Integer personId);

    public String disableSubscription(Long walletContentId) throws JSONException, XPathExpressionException, ParserConfigurationException, SAXException, IOException;

    public String fulfillSubscription(Subscriptions s, String schemaName) throws UnsupportedEncodingException, NoSuchAlgorithmException, InvalidKeyException, URISyntaxException;
}
