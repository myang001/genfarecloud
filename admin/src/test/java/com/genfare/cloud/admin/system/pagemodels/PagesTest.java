package com.genfare.cloud.admin.system.pagemodels;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PagesTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(PagesTest.class);

    private static final String rootPath = "http://localhost:8080/admin";

    protected String getRootPath() {
        LOGGER.debug("Returning selenium test root path: " + rootPath);
        return rootPath;
    }

}
