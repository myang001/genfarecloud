package com.genfare.cloud.admin.converters;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.genfare.cloud.model.contact.Phone;

public class PhoneToStringConverterTest {

    @Test
    public void validPhoneNumber() {
        String expected = "(847) 593-8855";
        Phone input = new Phone();
        input.setNumber("18475938855");

        PhoneToStringConverter converter = new PhoneToStringConverter();
        String result = converter.convert(input);

        assertEquals(expected, result);
    }

}
