package com.genfare.cloud.admin.converters;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.genfare.cloud.model.contact.Phone;

public class StringToPhoneConverterTest {

    @Test
    public void convertsValidPhoneNumber() {
        String input = "847-593-8855";
        String expected = "+18475938855";

        StringToPhoneConverter converter = new StringToPhoneConverter();
        Phone result = converter.convert(input);

        assertEquals(expected, result.getNumber());
    }

    @Test(expected = IllegalArgumentException.class)
    public void failsConvertingInvalidPhoneNumber() {
        String input = "8855";

        StringToPhoneConverter converter = new StringToPhoneConverter();
        converter.convert(input);
    }

}
