package com.genfare.cloud.admin.system.pagemodels;

import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class FindOrdersPage
    extends PagesTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(FindOrdersPage.class);

    @Autowired
    WebDriver driver;

    @FindBy(id = "orderNumber")
    public WebElement orderNumberInput;

    @FindBy(id = "orderStatus")
    public WebElement orderStatusSelector;

    @FindBy(id = "orderFrom")
    public WebElement from;

    @FindBy(id = "orderTo")
    public WebElement to;

    @FindBy(id = "search")
    public WebElement submitButton;

    @FindBy(id = "order-search-results")
    public List<WebElement> searchResults;

    @FindBy(className = "dataTables_empty")
    public List<WebElement> emptyResults;

    public FindOrdersPage goToPage() {
        driver.get(getRootPath() + "/fulfillment/orders");
        return this;
    }

    public FindOrdersPage pageIsLoaded() {
        PageFactory.initElements(driver, this);
        return this;
    }

    public FindOrdersPage search(Integer orderNumber/* , String orderStatus, String orderFrom, String orderTo */) {
        /* if(orderFrom == "MM/dd/yyyy" && orderTo == "MM/dd/yyyy" && orderStatus == ""){ */
        LOGGER.info("Searching for order number: " + orderNumber);
        orderNumberInput.sendKeys(Integer.toString(orderNumber));
        submitButton.click();
        /*
         * } else { return this; }
         */
        return this;
    }

    public FindOrdersPage orderFound(boolean expected) {
        boolean actual;
        if (expected == false) {
            actual = emptyResults.size() > 1;
        } else {
            actual = emptyResults.size() == 0;
        }

        LOGGER.info("Result set empty: " + !actual);

        Assert.assertEquals(expected, actual);
        return this;
    }

}
