package com.genfare.cloud.admin.system.steps;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;

import com.genfare.cloud.admin.system.pagemodels.FindOrdersPage;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

@WebAppConfiguration
@ContextConfiguration("classpath:/cucumber.xml")
public class FindOrdersSteps {
    private static final Logger LOGGER = LoggerFactory.getLogger(FindOrdersSteps.class);

    @Autowired
    FindOrdersPage searchPage;

    @Given("^I navigate to the find orders page$")
    public void given_I_navigate_to_the_application() {
        LOGGER.info("Entering: I navigate to the find order page");
        searchPage.goToPage().pageIsLoaded();
    }

    @When("^I try to search for an order$")
    public void when_I_try_to_search_valid() {
        LOGGER.info("Entering: I try to search for a valid order");
        searchPage.search(1);
    }

    @When("^I try to search for an invalid order$")
    public void when_I_try_to_search_invalid() {
        LOGGER.info("Entering: I try to searchfor an invalid");
        searchPage.search(999);
    }

    @Then("^I should see that an order is found$")
    public void then_I_searched_successfully() {
        LOGGER.info("Entering: I should see that a order is found");
        searchPage.orderFound(true);
    }

    @Then("^I should see that an order is not found$")
    public void then_I_searched_unsuccessfully() {
        LOGGER.info("Entering: I should see that a order is not found");
        searchPage.orderFound(false);
    }

}
