package com.genfare.cloud.admin.system.steps;

import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;

import cucumber.api.java.Before;

@WebAppConfiguration
@ContextConfiguration("classpath:/cucumber.xml")
public class TestHooks {
    private static final Logger LOGGER = LoggerFactory.getLogger(TestHooks.class);

    @Autowired
    WebDriver driver;

    @Before
    public void beforeScenario() {
        LOGGER.info("Clearing cookies");
        driver.manage().deleteAllCookies();
    }
}
