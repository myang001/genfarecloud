package com.genfare.cloud.admin.system;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import com.icegreen.greenmail.util.GreenMail;

@Component
public class TestResources {

    @Bean
    public WebDriver createDriver() {
        return new HtmlUnitDriver(false);
    }

    @Bean
    public GreenMail createGreenMail() {
        GreenMail greenMail = new GreenMail();
        greenMail.start();
        return greenMail;
    }

}