#encoding: utf-8
@findOrder @all
Feature: Admin Users can search for existing orders
 
Scenario: Order Found
	Given I navigate to the find orders page
	When I try to search for an order
	Then I should see that an order is found
	
Scenario: Order Not Found
	Given I navigate to the find orders page
	When I try to search for an invalid order
	Then I should see that an order is not found
