package com.genfare.cloud.admin.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.validation.Valid;

import org.joda.time.DateTime;
import org.joda.time.Minutes;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.genfare.cloud.admin.dto.OrderSearchDTO;
import com.genfare.cloud.admin.form.FindOrdersForm;
import com.genfare.cloud.model.dto.JSONWrapper;
import com.genfare.cloud.model.entity.Fulfillments;
import com.genfare.cloud.model.entity.Orderitems;
import com.genfare.cloud.model.entity.Orders;
import com.genfare.cloud.model.entity.Orderstatuses;
import com.genfare.cloud.model.entity.repository.OrdersRepository;
import com.genfare.cloud.model.service.OrderManagementService;
import com.genfare.cloud.model.tenant.TenantConfiguration;
import com.genfare.cloud.model.util.ToJsonHelper;

@Controller
public class FindOrdersController {

    private static final Logger LOGGER = LoggerFactory.getLogger(FindOrdersController.class);

    @Autowired
    private TenantConfiguration config;

    @Autowired
    private OrdersRepository ordersRepository;

    @PersistenceContext
    private EntityManager em;

    @Autowired
    private OrderManagementService orderManagementService;

    @RequestMapping(value = "/fulfillment/orders", method = RequestMethod.GET)
    public String findOrders(@ModelAttribute("findOrdersForm") FindOrdersForm form, Model model) {
        populateStatuses(model);
        return "fulfillment/find_orders";
    }

    private void populateStatuses(Model model) {
        List<Orderstatuses> fulStatuses = orderManagementService.getFulfillmentStatuses();
        model.addAttribute("statuses", fulStatuses);
    }

    @RequestMapping(value = "/fulfillment/orders", method = RequestMethod.POST)
    public String findOrders(@Valid @ModelAttribute("findOrdersForm") FindOrdersForm form, BindingResult result, Model model) {
        populateStatuses(model);
        if (result.hasErrors()) {
            return "fulfillment/find_orders";
        }
        try {
            // returns a list of orders
            List<Orders> ordersList =
                orderManagementService.getFulfillmentOrdersByFilter(form.getOrderNumber(), form.getFromDate(),
                    form.getToDate(), form.getStatus());

            validateOrderList(ordersList);
            List<OrderSearchDTO> orders = populateOrderDTOList(ordersList);
            LOGGER.info("Orders List Returned Elements: " + ordersList.size());
            model.addAttribute("ordersList", orders);
        } catch (EntityNotFoundException e) {
            LOGGER.info("No Result for List Orders");
            model.addAttribute("error", e.getMessage());
        }
        return "fulfillment/find_orders";
    }

    private List<OrderSearchDTO> populateOrderDTOList(List<Orders> ordersList) {
        List<OrderSearchDTO> orderList = new ArrayList<OrderSearchDTO>();
        for (Orders o : ordersList) {
            o = unlockOrderIfTimeout(o);
            OrderSearchDTO odto = new OrderSearchDTO(o);
            orderList.add(odto);
        }
        return orderList;
    }

    private Orders unlockOrderIfTimeout(Orders order) {
        Date lockedAt = order.getLockedAt();
        if (lockedAt != null) {
            DateTime tLocked = new DateTime(lockedAt);
            DateTime tUnlock = new DateTime(new Date());

            Minutes Interval = Minutes.minutesBetween(tLocked, tUnlock);
            Minutes minInterval = Minutes.minutes(config.getLockOrderTimeout());

            if (Interval.isGreaterThan(minInterval)) {
                order.setLockedAt(null);
                order.setLockedBy(null);
                // if status isn't explicitly re-set, it fails. why? I dunno.
                order.setOrderstatuses(order.getOrderstatuses());
                return ordersRepository.save(order);
            }

        }
        return order;
    }

    @ResponseBody
    @RequestMapping(value = "/fulfillment/orders/id", method = RequestMethod.POST)
    public JSONWrapper<List<OrderSearchDTO>> findOrdersById(@RequestParam("orderId") Integer orderId) {
        List<Integer> ids = new ArrayList<Integer>();
        ids.add(orderId);
        return searchOrders(ids);
    }

    @ResponseBody
    @RequestMapping(value = "/fulfillment/orders/shipping", method = RequestMethod.POST)
    public Object getShippingAddresses(@ModelAttribute(value = "orderIds") List<Integer> orderIds, BindingResult result, Model model) {
        JSONArray error = new JSONArray();
        JSONObject js = new JSONObject();
        if (orderIds == null) {
            error.put("errors.find.manage.enter.order.ids");
            js.put("errors", error);
            String errors = ToJsonHelper.cleanMarkup(js.toString());
            return new JSONWrapper<String>(errors, "error");
        }

        return searchOrders(orderIds);
    }

    private void validateOrderList(List<Orders> ordersList) {
        for (int i = 0; i < ordersList.size(); i++) {
            Orders o = ordersList.get(i);
            for (int j = 0; j < o.getOrderitemses().size(); j++) {
                Orderitems item = o.getOrderitemses().get(j);
                if (item.getFulfillments() == null) {
                    item.setFulfillments(new Fulfillments(item.getId()));
                    item.getFulfillments().setQuantityEncoded(new Integer(0).shortValue());
                    item.getFulfillments().setQuantityShipped(new Integer(0).shortValue());
                }
            }
        }
    }

    @SuppressWarnings("unchecked")
    public JSONWrapper<List<OrderSearchDTO>> searchOrders(List<Integer> orderIds) {
        StringBuilder query = new StringBuilder();
        query.append("SELECT DISTINCT NEW com.genfare.cloud.admin.dto.OrderSearchDTO(o) FROM Orders o ");
        query.append(" WHERE 1=1 ");
        if (orderIds != null && !orderIds.isEmpty()) {
            query.append(" AND o.orderId in (:orderIds)");
        }
        Query q = em.createQuery(query.toString());
        q.setParameter("orderIds", orderIds);

        List<OrderSearchDTO> results = (List<OrderSearchDTO>) q.getResultList();
        return new JSONWrapper<List<OrderSearchDTO>>(results, ((results.isEmpty()) ? "empty" : "success"));
    }
}