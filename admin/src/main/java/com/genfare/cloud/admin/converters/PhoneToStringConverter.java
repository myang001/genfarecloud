package com.genfare.cloud.admin.converters;

import org.springframework.core.convert.converter.Converter;

import com.genfare.cloud.model.contact.Phone;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.PhoneNumberUtil.PhoneNumberFormat;
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber;

public class PhoneToStringConverter
    implements Converter<Phone, String> {

    @Override
    public String convert(Phone input) {
        try {
            PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
            PhoneNumber phoneNumber = phoneUtil.parse(input.getNumber(), "US");
            return phoneUtil.format(phoneNumber, PhoneNumberFormat.NATIONAL);
        } catch (NumberParseException e) {
            throw new IllegalArgumentException("Not a valid phone number", e);
        }
    }

}
