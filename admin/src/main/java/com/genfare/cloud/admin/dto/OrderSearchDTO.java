package com.genfare.cloud.admin.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.joda.time.DateTime;
import org.joda.time.Minutes;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.genfare.cloud.admin.controller.ViewDetailsController;
import com.genfare.cloud.model.contact.Address;
import com.genfare.cloud.model.dto.OrderDataDTO;
import com.genfare.cloud.model.entity.Orderitems;
import com.genfare.cloud.model.entity.OrderitemsId;
import com.genfare.cloud.model.entity.Orders;
import com.genfare.cloud.model.util.DateHelper;
import com.genfare.cloud.model.util.OrderDataHelper;

public class OrderSearchDTO {

    private Integer ordId;

    private String user;

    private String name;

    private String addr1;

    private String addr2;

    private String city;

    private String state;

    private String country;

    private String zip;

    private String orderStatus;

    private String isEncoded;

    private String isShipped;

    private Integer numEncoded;

    private Integer numShipped;

    private String dateOrdered;

    private Integer quantity;

    private Boolean isLocked;

    private Boolean isLockedByCurrentUser;

    private Boolean isFulfillable;

    private String lockedAt;

    private String lockedBy;

    private List<OrderItemSearchDTO> orderItems = new ArrayList<OrderItemSearchDTO>();

    private static final Logger LOGGER = LoggerFactory.getLogger(ViewDetailsController.class);

    public OrderSearchDTO(Orders o) {
        isFulfillable = false;
        numEncoded = 0;
        numShipped = 0;
        ordId = o.getOrderId();
        dateOrdered = DateHelper.getDateAsString(o.getDateOrdered());

        try {
            OrderDataDTO orderData = OrderDataHelper.populateOrderData(o.getOrderData());
            Address shipping = orderData.getShipping();

            StringBuilder nameB = new StringBuilder();
            nameB.append(orderData.getFirstName());
            nameB.append(" ");
            nameB.append(orderData.getLastName());

            name = nameB.toString();
            addr1 = shipping.getLine1();
            addr2 = shipping.getLine2();
            city = shipping.getCity();
            country = shipping.getCountry();
            state = shipping.getState();
            zip = shipping.getPostalCode();

        } catch (JSONException e) {
            LOGGER.error("Could not parse order data, defaulting values");
            name = "";
            addr1 = "";
            addr2 = "";
            city = "";
            country = "";
            state = "";
            zip = "";
        } catch (Exception e) {
            LOGGER.error("Could not populate order data, defaulting values");
            name = "";
            addr1 = "";
            addr2 = "";
            city = "";
            country = "";
            state = "";
            zip = "";
        }

        if (name.trim().equals("")) {
            user = "Guest";
        } else {
            user = name;
        }

        isLocked = o.getLockedAt() != null;

        Minutes Interval = Minutes.minutesBetween(new DateTime(o.getLockedAt()), new DateTime(new Date()));
        lockedAt = Integer.toString(Interval.getMinutes());
        if (lockedAt.equalsIgnoreCase("0")) {
            lockedAt = "1";
        }
        lockedBy = o.getLockedBy();

        // set order status
        orderStatus = o.getOrderstatuses().getDescription();
        // keep track of parent order items
        Map<Byte, OrderItemSearchDTO> parentItems = new HashMap<Byte, OrderItemSearchDTO>();

        // TODO when orderitem can pull parent/child objects, this will need to be refactored
        // loop through all of the items to determine which are parents and which are children.
        boolean missedOne = false;
        List<OrderitemsId> missedIDs = new ArrayList<OrderitemsId>();
        for (Orderitems item : o.getOrderitemses()) {
            if (item.getParentItemNumber() != null && !item.getParentItemNumber().equals(item.getId().getOrderItemNumber())) {
                OrderItemSearchDTO parent = parentItems.get(item.getParentItemNumber());
                if (parent == null) {
                    missedOne = true;
                    missedIDs.add(item.getId());
                } else {
                    parent.addChild(new OrderItemSearchDTO(item));
                }
            } else {
                // this is a parent item
                parentItems.put(item.getId().getOrderItemNumber(), new OrderItemSearchDTO(item));
            }
        }

        // if for some reason there was a missed record adding a child to parent, do a second loop to resolve. this
        // should not happen and it should be removed once the above orderitem parent/child todo is resolved
        if (missedOne) {
            for (OrderitemsId id : missedIDs) {
                for (Orderitems item : o.getOrderitemses()) {
                    if (item.getId() == id) {
                        OrderItemSearchDTO parent = parentItems.get(item.getParentItemNumber());
                        parent.addChild(new OrderItemSearchDTO(item));
                    }
                }
            }
        }// if we missed any

        // loop through all items now that they've been properly inited and determine statuses
        quantity = 0;
        for (OrderItemSearchDTO itemDTO : parentItems.values()) {
            orderItems.add(itemDTO);
            numEncoded += itemDTO.getqEncoded();
            numShipped += itemDTO.getqShipped();
            if (itemDTO.getqEncoded() == itemDTO.getQuantity()) {
                isEncoded = "Yes";
            } else if (itemDTO.getqEncoded() > 0) {
                isEncoded = "Partial";
            } else {
                isEncoded = "No";
            }
            if (itemDTO.getqShipped() == itemDTO.getQuantity()) {
                isShipped = "Yes";
            } else if (itemDTO.getqShipped() > 0) {
                isShipped = "Partial";
            } else {
                isShipped = "No";
            }
            itemDTO.setSubtotal();
            quantity = quantity + itemDTO.getQuantity();
            if (!isFulfillable) {
                if (itemDTO.getChildren().size() == 0) {
                    if (itemDTO.isFulfillable()) {
                        isFulfillable = true;
                    }
                } else {
                    for (OrderItemSearchDTO child : itemDTO.getChildren()) {
                        if (!child.isAutoload()) {
                            isFulfillable = true;
                        }
                    }
                }
            }
        }

    }

    public Boolean getIsLockedByCurrentUser() {
        return isLockedByCurrentUser;
    }

    public void setIsLockedByCurrentUser(Boolean isLockedByCurrentUser) {
        this.isLockedByCurrentUser = isLockedByCurrentUser;
    }

    public String getIsEncoded() {
        return isEncoded;
    }

    public void setIsEncoded(String isEncoded) {
        this.isEncoded = isEncoded;
    }

    public String getIsShipped() {
        return isShipped;
    }

    public void setIsShipped(String isShipped) {
        this.isShipped = isShipped;
    }

    public String getDateOrdered() {
        return dateOrdered;
    }

    public void setDateOrdered(String dateOrdered) {
        this.dateOrdered = dateOrdered;
    }

    public Integer getOrdId() {
        return ordId;
    }

    public void setOrdId(Integer ordId) {
        this.ordId = ordId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddr1() {
        return addr1;
    }

    public void setAddr1(String addr1) {
        this.addr1 = addr1;
    }

    public String getAddr2() {
        return addr2;
    }

    public void setAddr2(String addr2) {
        this.addr2 = addr2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public List<OrderItemSearchDTO> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(List<OrderItemSearchDTO> orderItems) {
        this.orderItems = orderItems;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public Integer getNumEncoded() {
        return numEncoded;
    }

    public void setNumEncoded(Integer numEncoded) {
        this.numEncoded = numEncoded;
    }

    public Integer getNumShipped() {
        return numShipped;
    }

    public void setNumShipped(Integer numShipped) {
        this.numShipped = numShipped;
    }

    public Boolean getIsLocked() {
        return isLocked;
    }

    public void setIsLocked(Boolean isLocked) {
        this.isLocked = isLocked;
    }

    public Boolean getIsFulfillable() {
        return isFulfillable;
    }

    public void setIsFulfillable(Boolean isFulfillable) {
        this.isFulfillable = isFulfillable;
    }

    public String getLockedAt() {
        return lockedAt;
    }

    public void setLockedAt(String lockedAt) {
        this.lockedAt = lockedAt;
    }

    public String getLockedBy() {
        return lockedBy;
    }

    public void setLockedBy(String lockedBy) {
        this.lockedBy = lockedBy;
    }

}
