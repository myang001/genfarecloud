package com.genfare.cloud.admin.form;

import java.util.Date;

import javax.validation.constraints.AssertTrue;

public class ShipOrdersForm {

    private Integer orderNumber;

    private Byte orderItemNumber;

    private Date shipDate;

    private String tracking;

    private Byte shipperId;

    public Integer getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(Integer orderNumber) {
        this.orderNumber = orderNumber;
    }

    public Byte getOrderItemNumber() {
        return orderItemNumber;
    }

    public void setOrderItemNumber(Byte orderItemNumber) {
        this.orderItemNumber = orderItemNumber;
    }

    public Date getShipDate() {
        return shipDate;
    }

    public void setShipDate(Date toDate) {
        this.shipDate = toDate;
    }

    public String getTracking() {
        return tracking;
    }

    public void setTracking(String tracking) {
        this.tracking = tracking;
    }

    public Byte getShipperId() {
        return shipperId;
    }

    public void setShipperId(Byte shipperId) {
        this.shipperId = shipperId;
    }

    @AssertTrue(message = "{admin.find.orders.must.include.search.parm}")
    public boolean isValid() {
        return !(orderNumber == null && orderItemNumber == null && shipDate == null && shipperId == null);
    }

}