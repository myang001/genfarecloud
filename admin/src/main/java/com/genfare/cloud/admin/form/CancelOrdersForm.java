package com.genfare.cloud.admin.form;

import javax.validation.constraints.AssertTrue;

public class CancelOrdersForm {

    private Integer orderNumber;

    private Byte orderItemNumber;

    public Integer getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(Integer orderNumber) {
        this.orderNumber = orderNumber;
    }

    public Byte getOrderItemNumber() {
        return orderItemNumber;
    }

    public void setOrderItemNumber(Byte orderItemNumber) {
        this.orderItemNumber = orderItemNumber;
    }

    @AssertTrue(message = "{admin.find.orders.must.include.search.parm}")
    public boolean isValid() {
        return !(orderNumber == null);
    }

}