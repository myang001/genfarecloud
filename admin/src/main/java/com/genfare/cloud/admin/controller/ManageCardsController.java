package com.genfare.cloud.admin.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.genfare.cloud.admin.form.FindOrdersForm;
import com.genfare.cloud.model.service.OrderManagementService;

@Controller
public class ManageCardsController {

    private static final Logger LOGGER = LoggerFactory.getLogger(FindOrdersController.class);

    @Autowired
    private OrderManagementService orderManagementService;

    @RequestMapping(value = "id_cards/manage", method = RequestMethod.GET)
    public String manageIDCards(@ModelAttribute("findOrdersForm") FindOrdersForm form, Model model) {
        LOGGER.info("Entering Manage ID Cards");
        return "id_cards/manage";
    }

    @RequestMapping(value = "id_cards/manage/manageStepTwo", method = RequestMethod.GET)
    public String manageIDCardsStepTwo(@ModelAttribute("findOrdersForm") FindOrdersForm form, Model model) {
        LOGGER.info("Entering Manage ID Cards step two");
        return "id_cards/manage/manageStepTwo";
    }

    @RequestMapping(value = "id_cards/manage/manageStepThree", method = RequestMethod.GET)
    public String manageIDCardsStepThree(@ModelAttribute("findOrdersForm") FindOrdersForm form, Model model) {
        LOGGER.info("Entering Manage ID Cards step two");
        return "id_cards/manage/manageStepThree";
    }

}