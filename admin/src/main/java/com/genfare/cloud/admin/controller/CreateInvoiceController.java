package com.genfare.cloud.admin.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.genfare.cloud.admin.form.FindOrdersForm;
import com.genfare.cloud.model.service.OrderManagementService;

@Controller
public class CreateInvoiceController {

    private static final Logger LOGGER = LoggerFactory.getLogger(CreateInvoiceController.class);

    @Autowired
    private OrderManagementService orderManagementService;

    @RequestMapping(value = "/invoicing/create", method = RequestMethod.GET)
    public String findOrders(@Valid @ModelAttribute("findOrdersForm") FindOrdersForm findOrdersForm, BindingResult result, Model model) {
        /*
         * if(result.hasErrors()) { return "invoicing/unbilled_orders"; }
         */
        /*
         * Copied from UnbilledOrders try{ Orders orders = orderManagementService.getOrder(); LOGGER.debug("Orders ID: " + x);
         * model.addAttribute("orders", orders); }catch(EntityNotFoundException e){ LOGGER.info("No Result");
         * model.addAttribute("error", e.getMessage()); }
         */
        return "invoicing/create_invoice";
    }
}
