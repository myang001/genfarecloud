package com.genfare.cloud.admin.dto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.genfare.cloud.model.entity.FulfillmentItems;
import com.genfare.cloud.model.entity.Fulfillments;
import com.genfare.cloud.model.entity.Orderitems;
import com.genfare.cloud.model.entity.Pendingactivity;
import com.genfare.cloud.model.entity.Tickets;
import com.genfare.cloud.model.entity.Walletidentifiers;
import com.genfare.cloud.model.om.FarecodeType;
import com.genfare.cloud.model.types.ActivitytypesType;
import com.genfare.cloud.model.types.FarecodesType;
import com.genfare.cloud.model.types.IdentifiertypesType;
import com.genfare.cloud.model.types.OrderitemactionsType;
import com.genfare.cloud.model.types.OrderitemtypesType;
import com.genfare.cloud.model.types.TickettypesType;
import com.genfare.cloud.model.util.CardPIDHelper;

public class OrderItemSearchDTO {

    private Byte itemNumber;

    private String status;

    private Short quantity;

    private Short qShipped;

    private Short qEncoded;

    private Byte ticketType;

    private Integer ticketID;

    private Date ticketStart;

    private Date ticketEnd;

    private Byte typeExpiration;

    private BigDecimal value;

    private BigDecimal repValue;

    private BigDecimal repThreshold;

    private Byte storedFixedValue;

    private Byte storedRide;

    private Byte fixedPeriod;

    private boolean hasTicket;

    private String description;

    private BigDecimal unitPrice;

    private BigDecimal extPrice;

    private BigDecimal subTotal;

    private boolean isAutoload;

    private boolean isFulfillable;

    private boolean hasWalletType;

    private boolean hasWalletID;

    private Byte fareCodeID;

    private List<String> ranges = new ArrayList<String>();

    private Set<OrderItemSearchDTO> children = new HashSet<OrderItemSearchDTO>();

    public OrderItemSearchDTO(Orderitems o) {
        for (Pendingactivity a : o.getPendingactivities()) {
            Byte id = a.getActivitytypes().getActivityTypeId();
            if (id == ActivitytypesType.ADDPRODUCT.getIndex().byteValue()
                || id == ActivitytypesType.ADDVALUE.getIndex().byteValue()) {
                isAutoload = true;
                break;
            }
        }

        if (o.getWallets() != null) {
            hasWalletID = true;
        }

        // if has a wallettype && is new, then this is a fulfillable order
        if (o.getWallettypes() != null
            && o.getOrderitemactions().getOrderItemActionId().equals(OrderitemactionsType.NEW.getIndex().byteValue())) {
            hasWalletType = true;
            isFulfillable = true;
        }

        itemNumber = o.getId().getOrderItemNumber();
        status = o.getOrderstatuses().getDescription();

        if (o.getFarecodes() != null) {
            fareCodeID = o.getFarecodes().getFarecodeId();
        } else {
            fareCodeID = FarecodesType.FULL.getType().getFarecodeId();
        }

        if (o.getFulfillments() == null) {
            o.setFulfillments(new Fulfillments());
        }
        qShipped = o.getFulfillments().getQuantityShipped();
        qEncoded = o.getFulfillments().getQuantityEncoded();
        quantity = o.getQuantity();
        unitPrice = o.getUnitPrice();
        extPrice = o.getAmount();

        if (o.getTickets() != null) {
            Tickets t = o.getTickets();
            ticketType = t.getTickettypes().getTicketTypeId();
            ticketID = t.getTicketId();
            ticketStart = t.getDateEffective();
            ticketEnd = t.getDateExpires();
            if (ticketType == TickettypesType.FIXED_PERIOD_PASS.getIndex().byteValue()) {
                typeExpiration = t.getModifierId();
            } else {
                typeExpiration = null;
            }
            value = t.getValue();
            repValue = t.getReplenishValue();
            repThreshold = t.getReplenishThreshold();
            description = t.getDescription();
            hasTicket = true;
        } else {
            hasTicket = false;
            if (o.getOrderitemactions().getOrderItemActionId().equals(OrderitemactionsType.NEW.getIndex().byteValue())
                && o.getOrderitemtypes().getOrderItemTypeId().equals(OrderitemtypesType.WALLETTYPE.getIndex().byteValue())) {
                description = o.getWallettypes().getDescription();
            } else if (o.getOrderitemactions().getOrderItemActionId().equals(OrderitemactionsType.CHANGE.getIndex().byteValue())
                && o.getOrderitemtypes().getOrderItemTypeId().equals(OrderitemtypesType.WALLET.getIndex().byteValue())) {
                description = "Existing Wallet";
            }
        }
        if (!isAutoload) {
            // get ranges for this order
            if (qEncoded != null && qEncoded > 0) {
                int idLength = 0;
                List<Long> pids = new ArrayList<Long>();
                for (FulfillmentItems f : o.getFulfillments().getFulfillmentItems()) {
                    for (Walletidentifiers id : f.getWallet().getWalletidentifierses()) {
                        if (id.getIdentifiertypes().getIdentifierId().equals(IdentifiertypesType.PRINTED_ID.getIndex().byteValue())) {
                            idLength = id.getIdentifier().length();
                            pids.add(Long.valueOf(id.getIdentifier()));
                        }
                    }
                }

                if (pids.size() > 0) {
                    // sort PIDs and split into ranges
                    Collections.sort(pids);
                    Long firstPID = pids.get(0);
                    Long lastPID = pids.get(0);

                    for (int i = 0; i < pids.size(); i++) {
                        long cur = pids.get(i);
                        if (cur == firstPID) {
                            continue;
                        }
                        if (cur == lastPID + 1) {
                            lastPID++;
                            continue;
                        } else {
                            addRange(idLength, firstPID, lastPID);
                            lastPID = cur;
                            firstPID = cur;
                        }
                    }
                    addRange(idLength, firstPID, lastPID);
                }

            }// if any have been encoded
        }// if is not autoload

        storedFixedValue = TickettypesType.STORED_FIXED_VALUE.getIndex().byteValue();
        storedRide = TickettypesType.STORED_RIDE.getIndex().byteValue();
        fixedPeriod = TickettypesType.FIXED_PERIOD_PASS.getIndex().byteValue();

    }

    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

    public BigDecimal getExtPrice() {
        return extPrice;
    }

    public void setExtPrice(BigDecimal extPrice) {
        this.extPrice = extPrice;
    }

    public BigDecimal getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(BigDecimal extended) {
        this.subTotal = extended;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<String> getRanges() {
        return ranges;
    }

    public void setRanges(List<String> ranges) {
        this.ranges = ranges;
    }

    private void addRange(int idLength, Long firstPID, Long lastPID) {
        if (firstPID.equals(lastPID)) {
            ranges.add(CardPIDHelper.leftPadID(firstPID, idLength, '0') + "");
        } else {
            ranges.add(CardPIDHelper.leftPadID(firstPID, idLength, '0') + " - "
                + CardPIDHelper.leftPadID(lastPID, idLength, '0'));
        }
    }

    public Byte getItemNumber() {
        return itemNumber;
    }

    public void setItemNumber(Byte itemNumber) {
        this.itemNumber = itemNumber;
    }

    public String getStatus() {
        return status;
    }

    public Short getQuantity() {
        return quantity;
    }

    public void setQuantity(Short quantity) {
        this.quantity = quantity;
    }

    public Short getqShipped() {
        return qShipped;
    }

    public void setqShipped(Short qShipped) {
        this.qShipped = qShipped;
    }

    public Short getqEncoded() {
        return qEncoded;
    }

    public void setqEncoded(Short qEncoded) {
        this.qEncoded = qEncoded;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Byte getTicketType() {
        return ticketType;
    }

    public void setTicketType(Byte ticketType) {
        this.ticketType = ticketType;
    }

    public Integer getTicketID() {
        return ticketID;
    }

    public void setTicketID(Integer ticketID) {
        this.ticketID = ticketID;
    }

    public Date getTicketStart() {
        return ticketStart;
    }

    public void setTicketStart(Date ticketStart) {
        this.ticketStart = ticketStart;
    }

    public Date getTicketEnd() {
        return ticketEnd;
    }

    public void setTicketEnd(Date ticketEnd) {
        this.ticketEnd = ticketEnd;
    }

    public Byte getTypeExpiration() {
        return typeExpiration;
    }

    public void setTypeExpiration(Byte typeExpiration) {
        this.typeExpiration = typeExpiration;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public BigDecimal getRepValue() {
        return repValue;
    }

    public void setRepValue(BigDecimal repValue) {
        this.repValue = repValue;
    }

    public BigDecimal getRepThreshold() {
        return repThreshold;
    }

    public void setRepThreshold(BigDecimal repThreshold) {
        this.repThreshold = repThreshold;
    }

    public Byte getStoredFixedValue() {
        return storedFixedValue;
    }

    public void setStoredFixedValue(Byte storedFixedValue) {
        this.storedFixedValue = storedFixedValue;
    }

    public Byte getStoredRide() {
        return storedRide;
    }

    public void setStoredRide(Byte storedRide) {
        this.storedRide = storedRide;
    }

    public Byte getFixedPeriod() {
        return fixedPeriod;
    }

    public void setFixedPeriod(Byte fixedPeriod) {
        this.fixedPeriod = fixedPeriod;
    }

    public boolean isHasTicket() {
        return hasTicket;
    }

    public void setHasTicket(boolean hasTicket) {
        this.hasTicket = hasTicket;
    }

    public Set<OrderItemSearchDTO> getChildren() {
        return children;
    }

    public void setChildren(Set<OrderItemSearchDTO> children) {
        this.children = children;
    }

    public boolean isAutoload() {
        return isAutoload;
    }

    public void setAutoload(boolean isAutoload) {
        this.isAutoload = isAutoload;
    }

    public void addChild(OrderItemSearchDTO child) {
        this.children.add(child);
    }

    public void setSubtotal() {
        BigDecimal extended = extPrice;
        for (OrderItemSearchDTO child : this.children) {
            extended = extended.add(child.getExtPrice());
        }
        setSubTotal(extended);

    }

    public boolean isFulfillable() {
        return isFulfillable;
    }

    public void setFulfillable(boolean isFulfillable) {
        this.isFulfillable = isFulfillable;
    }

    public boolean isHasWalletType() {
        return hasWalletType;
    }

    public void setHasWalletType(boolean hasWalletType) {
        this.hasWalletType = hasWalletType;
    }

    public boolean isHasWalletID() {
        return hasWalletID;
    }

    public void setHasWalletID(boolean hasWalletID) {
        this.hasWalletID = hasWalletID;
    }

    public Byte getFareCodeID() {
        return fareCodeID;
    }

    public void setFareCodeID(Byte fareCodeID) {
        this.fareCodeID = fareCodeID;
    }

}
