package com.genfare.cloud.admin.form;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.AssertTrue;

public class FindOrdersForm {

    private Integer orderNumber;

    private List<Integer> orderNumbers;

    /** field used when searching 'back' to list of orders from details page **/
    private Integer searchOrderNumber;

    private Date fromDate;

    private Date toDate;

    private Byte status;

    public Integer getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(Integer orderNumber) {
        this.orderNumber = orderNumber;
    }

    public List<Integer> getOrderNumbers() {
        return orderNumbers;
    }

    public void setOrderNumbers(List<Integer> orderNumbers) {
        this.orderNumbers = orderNumbers;
    }

    public Integer getSearchOrderNumber() {
        return searchOrderNumber;
    }

    public void setSearchOrderNumber(Integer searchOrderNumber) {
        this.searchOrderNumber = searchOrderNumber;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    @AssertTrue(message = "{admin.find.orders.must.include.search.parm}")
    public boolean isValid() {
        return !(orderNumber == null && fromDate == null && toDate == null && status == null && orderNumbers == null);
    }

}