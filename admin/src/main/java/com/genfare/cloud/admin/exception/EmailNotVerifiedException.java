package com.genfare.cloud.admin.exception;

import org.springframework.security.authentication.AccountStatusException;

public class EmailNotVerifiedException
    extends AccountStatusException {

    private static final long serialVersionUID = 1335592107791647135L;

    public EmailNotVerifiedException(String msg) {
        super(msg);
    }

    public EmailNotVerifiedException(String msg, Throwable t) {
        super(msg, t);
    }

}
