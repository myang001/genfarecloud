package com.genfare.cloud.admin.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

/**
 * @author martin
 */
@Configuration
@ComponentScan(basePackages = { "com.genfare.cloud.admin.service", "com.genfare.cloud.model" })
@PropertySource({ "classpath:config/sso.properties", "classpath:config/smtp.properties" })
public class ApplicationContext {

    @Bean
    public PropertySourcesPlaceholderConfigurer propertyPlaceHolderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }

}