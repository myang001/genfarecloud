package com.genfare.cloud.admin.converters;

import org.apache.commons.lang.StringUtils;
import org.springframework.core.convert.converter.Converter;

import com.genfare.cloud.model.contact.Phone;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.PhoneNumberUtil.PhoneNumberFormat;
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber;

public class StringToPhoneConverter
    implements Converter<String, Phone> {

    @Override
    public Phone convert(String input) {
        if (StringUtils.isBlank(input)) {
            return null;
        }

        try {
            PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
            PhoneNumber phoneNumber = phoneUtil.parse(input, "US");

            if (!phoneUtil.isValidNumber(phoneNumber)) {
                throw new IllegalArgumentException("Not a valid phone number");
            }

            Phone result = new Phone();
            result.setNumber(phoneUtil.format(phoneNumber, PhoneNumberFormat.E164));
            return result;
        } catch (NumberParseException e) {
            throw new IllegalArgumentException("Not a valid phone number", e);
        }
    }

}
