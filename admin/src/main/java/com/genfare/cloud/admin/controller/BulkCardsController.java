package com.genfare.cloud.admin.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.genfare.cloud.admin.form.FindOrdersForm;
import com.genfare.cloud.model.service.OrderManagementService;

@Controller
public class BulkCardsController {

    private static final Logger LOGGER = LoggerFactory.getLogger(FindOrdersController.class);

    @Autowired
    private OrderManagementService orderManagementService;

    @RequestMapping(value = "id_cards/bulk", method = RequestMethod.GET)
    public String bulkStepOneGET(@ModelAttribute("findOrdersForm") FindOrdersForm form, Model model) {
        LOGGER.info("GET bulk id card management step 1");
        return "id_cards/bulk";
    }

    @RequestMapping(value = "id_cards/bulk", method = RequestMethod.POST)
    public String bulkStepOnePOST(@ModelAttribute("findOrdersForm") FindOrdersForm form, Model model) {
        LOGGER.info("POST bulk id card management step 1");
        /*
         * Once we have actual data to pass JSONObject js = new JSONObject(); js.put("order", order.toJson()); js.put("errors",
         * error); return ToJsonHelper.cleanMarkup(js.toString());
         */
        return "id_cards/bulk";
    }

    @RequestMapping(value = "id_cards/bulk/bulkStepTwo", method = RequestMethod.GET)
    public String bulkStepTwo(@ModelAttribute("findOrdersForm") FindOrdersForm form, Model model) {
        LOGGER.info("POST bulk id card management step 2");
        /*
         * Once we have actual data to pass JSONObject js = new JSONObject(); js.put("order", order.toJson()); js.put("errors",
         * error); return ToJsonHelper.cleanMarkup(js.toString());
         */
        return "id_cards/bulk/bulkStepTwo";
    }

    @RequestMapping(value = "id_cards/bulkStepThree", method = RequestMethod.GET)
    public String bulkStepThree(@ModelAttribute("findOrdersForm") FindOrdersForm form, Model model) {
        LOGGER.info("POST bulk id card management step 3");
        /*
         * Once we have actual data to pass JSONObject js = new JSONObject(); js.put("order", order.toJson()); js.put("errors",
         * error); return ToJsonHelper.cleanMarkup(js.toString());
         */
        return "id_cards/bulk/bulkStepThree";
    }

}