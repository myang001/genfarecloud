package com.genfare.cloud.admin.config;

import java.util.EnumSet;

import javax.servlet.DispatcherType;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import org.springframework.core.annotation.Order;
import org.springframework.orm.jpa.support.OpenEntityManagerInViewFilter;
import org.springframework.web.context.request.RequestContextListener;
import org.springframework.web.filter.DelegatingFilterProxy;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

/**
 * @author Bryan Madaras
 */
@Order(2)
public class WebAppInitializer
    extends AbstractAnnotationConfigDispatcherServletInitializer {

    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class<?>[] { ApplicationContext.class, WebMvcConfig.class, SecurityConfig.class };
    }

    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class<?>[] { WebMvcConfig.class };
    }

    @Override
    protected String[] getServletMappings() {
        return new String[] { "/*" };
    }

    @Override
    public void onStartup(ServletContext servletContext)
        throws ServletException {
        super.onStartup(servletContext);

        servletContext.addListener(RequestContextListener.class);

        EnumSet<DispatcherType> dispatcherTypes = EnumSet.of(DispatcherType.REQUEST, DispatcherType.FORWARD);

        javax.servlet.FilterRegistration.Dynamic auditRequest =
            servletContext.addFilter("domainRequestFilter", DelegatingFilterProxy.class);
        auditRequest.addMappingForUrlPatterns(dispatcherTypes, false, "/*");

        javax.servlet.FilterRegistration.Dynamic openEntityManagerInViewFilter =
            servletContext.addFilter("openEntityManagerInViewFilter", OpenEntityManagerInViewFilter.class);
        openEntityManagerInViewFilter.addMappingForUrlPatterns(dispatcherTypes, false, "/*");

        javax.servlet.FilterRegistration.Dynamic sessionAuditRequest =
            servletContext.addFilter("sessionFilter", DelegatingFilterProxy.class);
        sessionAuditRequest.addMappingForUrlPatterns(dispatcherTypes, false, "/*");

    }

}
