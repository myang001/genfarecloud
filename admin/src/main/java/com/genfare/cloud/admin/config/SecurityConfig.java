package com.genfare.cloud.admin.config;

import java.util.HashMap;
import java.util.Map;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.servlet.configuration.EnableWebMvcSecurity;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.social.security.SocialUserDetailsService;

import com.genfare.cloud.admin.exception.EmailNotVerifiedException;
import com.genfare.cloud.model.service.LoginFailureHandler;
import com.genfare.cloud.model.service.SimpleSocialUserDetailsService;
import com.genfare.cloud.model.service.UserDetailsServiceImpl;

/**
 * @author martin
 */
@Configuration
@EnableWebMvcSecurity
public class SecurityConfig
    extends WebSecurityConfigurerAdapter {

    @Override
    public void configure(WebSecurity web)
        throws Exception {
        web
            // Spring Security ignores request to static resources such as CSS or JS
            // files.
            .ignoring().antMatchers("/resources/**");
    }

    @Override
    protected void configure(HttpSecurity http)
        throws Exception {
        http.authorizeRequests().antMatchers("/", "/home", "/home/**", "/search/**", "/products/**", "/order/**",
            "/signup/**", "/signin/**", "/test/**", "/content/**", "/login/**",
            "/guest/**", "/fulfillment/**", "/invoicing/**", "/admin/id_cards/**").permitAll()
            // .antMatchers("/admin/**").hasRole("ADMIN")
            .anyRequest().authenticated();
        http.formLogin().loginPage("/login").failureUrl("/login?error").failureHandler(authenticationFailureHandler()).permitAll().and().logout().deleteCookies("JSESSIONID")
            .deleteCookies("remember-me").logoutUrl("/logout").logoutSuccessUrl("/home");
        http.rememberMe().key("enteraniceandsecurekeytoencryptwith");
        http.sessionManagement().maximumSessions(-1).sessionRegistry(sessionRegistry());
    }

    @Bean
    public SessionRegistry sessionRegistry() {
        return new SessionRegistryImpl();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth)
        throws Exception {
        auth.userDetailsService(userDetailsService()).passwordEncoder(passwordEncoder());
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder(10);
    }

    @Bean
    public SocialUserDetailsService socialUserDetailsService() {
        return new SimpleSocialUserDetailsService(userDetailsService());
    }

    @Override
    @Bean
    public UserDetailsService userDetailsService() {
        return new UserDetailsServiceImpl();
    }

    @Bean
    public LoginFailureHandler authenticationFailureHandler() {
        LoginFailureHandler handler = new LoginFailureHandler();
        Map<String, String> map = new HashMap<String, String>();
        map.put(BadCredentialsException.class.getName(), "/home?error=Incorrect%20email%20or%20password");
        map.put(EmailNotVerifiedException.class.getName(), "/home/not_verified");
        handler.setExceptionMappings(map);
        handler.setDefaultFailureUrl("/home?error");

        return handler;
    }
}
