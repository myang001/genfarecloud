package com.genfare.cloud.admin.controller;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.genfare.cloud.admin.form.MessageForm;
import com.genfare.cloud.model.tenant.TenantConfiguration;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {

    private static final Logger LOGGER = Logger.getLogger(HomeController.class);

    @Autowired
    private TenantConfiguration tenantConfiguration;

    /**
     * Simply selects the Layout testing view to render by returning its name.
     * 
     * @param model module for the form
     * @return view name
     */
    @RequestMapping(value = { "/", "/home" }, method = RequestMethod.GET)
    public String root(final Model model) {
        String rtrn = null;
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth instanceof AnonymousAuthenticationToken) {
            rtrn = "home/home";
        } else {
            rtrn = "home/main";
        }
        LOGGER.info("Hit the admin app");
        model.addAttribute("domain", tenantConfiguration.getTenantName());
        model.addAttribute("messageInfo", new MessageForm());
        return rtrn;
    }
}