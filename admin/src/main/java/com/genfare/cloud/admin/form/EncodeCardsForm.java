package com.genfare.cloud.admin.form;

import javax.validation.constraints.AssertTrue;

public class EncodeCardsForm {

    private Integer orderNumber;

    private Byte orderItemNumber;

    private Long lowPID;

    private Long highPID;

    private int pidLength;

    public Integer getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(Integer orderNumber) {
        this.orderNumber = orderNumber;
    }

    public Byte getOrderItemNumber() {
        return orderItemNumber;
    }

    public void setOrderItemNumber(Byte orderItemNumber) {
        this.orderItemNumber = orderItemNumber;
    }

    public Long getLowPID() {
        return lowPID;
    }

    public void setLowPID(Long lowPID) {
        this.lowPID = lowPID;
    }

    public Long getHighPID() {
        return highPID;
    }

    public void setHighPID(Long highPID) {
        this.highPID = highPID;
    }

    public int getPidLength() {
        return pidLength;
    }

    public void setPidLength(int pidLength) {
        this.pidLength = pidLength;
    }

    @AssertTrue(message = "{admin.find.orders.must.include.search.parm}")
    public boolean isValid() {
        return !(orderNumber == null && orderItemNumber == null && lowPID == null);
    }

}