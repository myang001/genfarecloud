package com.genfare.cloud.admin.form;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

public class EmailForm {

    @Email(message = "{register.email.invalid}")
    @NotEmpty(message = "{register.email.blank}")
    @Length(max = 256, message = "{register.email.address.length}")
    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
