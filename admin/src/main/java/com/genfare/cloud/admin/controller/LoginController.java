package com.genfare.cloud.admin.controller;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.genfare.cloud.admin.form.EmailForm;
import com.genfare.cloud.model.security.User;
import com.genfare.cloud.model.service.EmailService;
import com.genfare.cloud.model.service.UserManagementService;

@Controller
public class LoginController {

    private static final Logger log = LoggerFactory.getLogger(LoginController.class);

    @Autowired
    UserManagementService usermanager;

    @Autowired
    EmailService emailservice;

    @RequestMapping(value = "/login/not_verified", method = RequestMethod.GET)
    public String emailNotVerified() {
        log.debug("Current User: " + SecurityContextHolder.getContext().getAuthentication().getName());
        return "email/notVerified";
    }

    @RequestMapping(value = "/login/verify/resend", method = RequestMethod.GET)
    public String resendToken(ModelMap map) {
        map.addAttribute("emailForm", new EmailForm());
        return "email/resendVerification";
    }

    @RequestMapping(value = "/login/verify/resend", method = RequestMethod.POST)
    public String generateNewToken(@Valid EmailForm emailForm, BindingResult result) {

        if (result.hasErrors()) {
            // Just tell them we sent it, but do not actually hit our DB for no reason
            return "email/verificationResent";
        }

        try {
            User user = usermanager.getUser(emailForm.getEmail());
            log.debug("New Email Verification Token Set for User: " + user.getLogin());
        } catch (EntityNotFoundException e) {
            log.debug("Not Generating Token for Unknown User");
        }

        return "email/verificationResent";
    }

}
