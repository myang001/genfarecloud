package com.genfare.cloud.admin.controller;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.genfare.cloud.admin.form.FindOrdersForm;
import com.genfare.cloud.model.entity.Orders;
import com.genfare.cloud.model.service.OrderManagementService;

@Controller
public class UnbilledOrdersController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ViewDetailsController.class);

    @Autowired
    private OrderManagementService orderManagementService;

    @RequestMapping(value = "/invoicing/unbilled", method = RequestMethod.GET)
    public String findOrders(@Valid @ModelAttribute("findOrdersForm") FindOrdersForm findOrdersForm, BindingResult result, Model model) {
        int x = 1;
        /*
         * if(result.hasErrors()) { return "invoicing/unbilled_orders"; }
         */
        try {
            Orders orders = orderManagementService.getOrder(x);
            LOGGER.debug("Orders ID: " + x);
            model.addAttribute("orders", orders);

            if (orders == null) {
                model.addAttribute("nullSafeOrgName", null);
            } else if (orders.getOrganizations() == null) {
                model.addAttribute("nullSafeOrgName", null);
            } else {
                model.addAttribute("nullSafeOrgName", orders.getOrganizations().getName());
            }

        } catch (EntityNotFoundException e) {
            LOGGER.info("No Result");
            model.addAttribute("error", e.getMessage());
        }

        return "invoicing/unbilled_orders";
    }
}
