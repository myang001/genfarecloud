package com.genfare.cloud.admin.exception;

import org.springframework.security.authentication.AccountStatusException;

public class AdminProcessException
    extends AccountStatusException {

    private static final long serialVersionUID = 1335592107791647135L;

    public AdminProcessException(String msg) {
        super(msg);
    }

    public AdminProcessException(String msg, Throwable t) {
        super(msg, t);
    }

}
