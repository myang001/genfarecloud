package com.genfare.cloud.admin.config;

import java.util.HashSet;
import java.util.List;
import java.util.Properties;

import nz.net.ultraq.thymeleaf.LayoutDialect;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.core.Ordered;
import org.springframework.core.env.Environment;
import org.springframework.format.FormatterRegistry;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.validation.Validator;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.thymeleaf.extras.springsecurity3.dialect.SpringSecurityDialect;
import org.thymeleaf.spring3.SpringTemplateEngine;
import org.thymeleaf.spring3.view.ThymeleafViewResolver;
import org.thymeleaf.templatemode.StandardTemplateModeHandlers;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.thymeleaf.templateresolver.ServletContextTemplateResolver;
import org.thymeleaf.templateresolver.TemplateResolver;
import org.thymeleaf.templateresolver.UrlTemplateResolver;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.hibernate4.Hibernate4Module;
import com.genfare.cloud.admin.converters.DateToStringConverter;
import com.genfare.cloud.admin.converters.PhoneToStringConverter;
import com.genfare.cloud.admin.converters.StringToDateConverter;
import com.genfare.cloud.admin.converters.StringToPhoneConverter;

/**
 * @author Bryan Madaras
 */
@Configuration
@ComponentScan(basePackages = { "com.genfare.cloud.admin.controller", "com.genfare.cloud.admin.filter" })
@EnableWebMvc
@EnableTransactionManagement
public class WebMvcConfig
    extends WebMvcConfigurerAdapter {

    private static final int MESSAGE_SOURCE_CACHE_SECONDS = 15;

    private static final String VIEWS = "/resources/views/";

    private static final String LOCALE = "/resources/locale/messages";

    private static final String EMAILS = "mail/";

    // TODO: Determine tenant to replace 'test' in URL
    private static final String URL_VIEWS = "https://s3.amazonaws.com/genfare-tenant/test/resources/views/";

    private static final String URL_LOCALE = "https://s3.amazonaws.com/genfare-tenant/test/resources/locale/messages";

    private static final String RESOURCES_HANDLER = "/resources/";

    private static final String RESOURCES_LOCATION = RESOURCES_HANDLER + "**";

    @Autowired
    private Environment env;

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/home").setViewName("home/home");
        registry.addViewController("/login").setViewName("signin/login");
        registry.addViewController("/guest").setViewName("home/main_guest");
        // registry.addViewController("/fulfillment/orders").setViewName("fulfillment/find_orders");

        registry.setOrder(Ordered.HIGHEST_PRECEDENCE);
    }

    /**
     * Enable full support for JSR-303 Bean Validation via hibernate-validator
     * 
     * @return Validator
     */
    @Override
    public Validator getValidator() {
        LocalValidatorFactoryBean validator = new LocalValidatorFactoryBean();
        validator.setValidationMessageSource(messageSource());
        return validator;
    }

    @Override
    public void configureDefaultServletHandling(final DefaultServletHandlerConfigurer configurer) {
        configurer.enable();
    }

    @Override
    public void addResourceHandlers(final ResourceHandlerRegistry registry) {
        registry.addResourceHandler(RESOURCES_HANDLER).addResourceLocations(RESOURCES_LOCATION);
    }

    /**
     * Loads the local LOCALE packaged in the application, then the remote URL_LOCALE URL_LOCALE properties overwrite anything
     * previously set by LOCALE
     * 
     * @return MessageSource
     */
    @Bean(name = "messageSource")
    public MessageSource messageSource() {
        ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
        messageSource.setBasenames(LOCALE, URL_LOCALE);
        messageSource.setCacheSeconds(MESSAGE_SOURCE_CACHE_SECONDS);
        return messageSource;
    }

    /**
     * Support for HTTP Thymeleaf layout remote storage locations
     * 
     * @return TemplateResolver
     */
    @Bean
    public TemplateResolver urlTemplateResolver() {
        TemplateResolver templateResolver = new UrlTemplateResolver();
        templateResolver.setPrefix(URL_VIEWS);
        templateResolver.setSuffix(".html");
        templateResolver.setTemplateMode(StandardTemplateModeHandlers.HTML5.getTemplateModeName());
        templateResolver.setOrder(2); // TODO: switch order for urlTemplateResolver and webTemplateResolver when s3
                                      // integrated with maven build process
        templateResolver.setCacheable(true); // TODO: this needs to be fixed for non-development environments
        return templateResolver;
    }

    /**
     * Add in the Thymeleaf template resolver
     * 
     * @return TemplateResolver
     */
    @Bean
    public TemplateResolver webTemplateResolver() {
        TemplateResolver templateResolver = new ServletContextTemplateResolver();
        templateResolver.setPrefix(VIEWS);
        templateResolver.setSuffix(".html");
        templateResolver.setTemplateMode(StandardTemplateModeHandlers.HTML5.getTemplateModeName());
        templateResolver.setOrder(3);
        templateResolver.setCacheable(false); // TODO: this needs to be fixed for non-development environments
        return templateResolver;
    }

    /**
     * Supports email templates
     * 
     * @return
     */
    @Bean
    public TemplateResolver emailTemplateResolver() {
        TemplateResolver templateResolver = new ClassLoaderTemplateResolver();
        templateResolver.setPrefix(EMAILS);
        templateResolver.setSuffix(".html");
        templateResolver.setTemplateMode(StandardTemplateModeHandlers.HTML5.getTemplateModeName());
        templateResolver.setCharacterEncoding("UTF-8");
        templateResolver.setOrder(1);
        return templateResolver;
    }

    /**
     * Add in the Thymeleaf template engine
     * 
     * @return SpringTemplateEngine
     */
    @Bean
    public SpringTemplateEngine templateEngine() {
        SpringTemplateEngine templateEngine = new SpringTemplateEngine();
        HashSet<TemplateResolver> resolvers = new HashSet<TemplateResolver>();
        resolvers.add(emailTemplateResolver());
        resolvers.add(webTemplateResolver());
        // resolvers.add(urlTemplateResolver()); // Enable when we're ready to put content on S3
        templateEngine.setTemplateResolvers(resolvers);
        templateEngine.addDialect(new LayoutDialect());
        templateEngine.addDialect(new SpringSecurityDialect());
        return templateEngine;
    }

    /**
     * Add in the Thymeleaf view resolver
     * 
     * @return ThymeleafViewResolver
     */
    @Bean
    public ThymeleafViewResolver viewResolver() {
        ThymeleafViewResolver thymeleafViewResolver = new ThymeleafViewResolver();
        thymeleafViewResolver.setTemplateEngine(templateEngine());
        thymeleafViewResolver.setCharacterEncoding("UTF-8");
        return thymeleafViewResolver;
    }

    @Override
    public void addFormatters(FormatterRegistry registry) {
        super.addFormatters(registry);
        registry.addConverter(new PhoneToStringConverter());
        registry.addConverter(new StringToPhoneConverter());
        registry.addConverter(new DateToStringConverter());
        registry.addConverter(new StringToDateConverter());
    }

    @Bean
    public JavaMailSenderImpl mailSender() {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setHost(env.getProperty("smtp.host"));
        mailSender.setPort(Integer.parseInt(env.getProperty("smtp.port")));
        mailSender.setUsername(env.getProperty("smtp.smtp.username"));
        mailSender.setPassword(env.getProperty("smtp.smtp.password"));
        mailSender.setProtocol(env.getProperty("smtp.protocol"));

        Properties props = mailSender.getJavaMailProperties();
        props.put("mail.smtp.auth", env.getProperty("mail.smtp.auth"));
        props.put("mail.smtp.ssl.enable", env.getProperty("mail.smtp.ssl.enable"));
        props.put("mail.transport.protocol", env.getProperty("mail.transport.protocol"));
        props.put("mail.debug", env.getProperty("mail.debug"));

        return mailSender;
    }

    public MappingJackson2HttpMessageConverter jacksonMessageConverter() {
        MappingJackson2HttpMessageConverter messageConverter = new MappingJackson2HttpMessageConverter();
        ObjectMapper mapper = new ObjectMapper();
        // Registering Hibernate4Module to support lazy objects
        mapper.registerModule(new Hibernate4Module());
        messageConverter.setObjectMapper(mapper);
        return messageConverter;

    }

    @Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        // Here we add our custom-configured HttpMessageConverter
        converters.add(jacksonMessageConverter());
        super.configureMessageConverters(converters);
    }
}
