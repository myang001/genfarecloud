package com.genfare.cloud.admin.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.Minutes;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.genfare.cloud.admin.dto.OrderSearchDTO;
import com.genfare.cloud.admin.form.CancelOrdersForm;
import com.genfare.cloud.admin.form.EncodeCardsForm;
import com.genfare.cloud.admin.form.FindOrdersForm;
import com.genfare.cloud.admin.form.ShipOrdersForm;
import com.genfare.cloud.model.dto.JSONWrapper;
import com.genfare.cloud.model.dto.UserDetails;
import com.genfare.cloud.model.entity.Crossreferencefile;
import com.genfare.cloud.model.entity.FulfillmentItems;
import com.genfare.cloud.model.entity.Fulfillments;
import com.genfare.cloud.model.entity.Identifiertypes;
import com.genfare.cloud.model.entity.Orderitems;
import com.genfare.cloud.model.entity.OrderitemsId;
import com.genfare.cloud.model.entity.Orders;
import com.genfare.cloud.model.entity.Purposes;
import com.genfare.cloud.model.entity.Shipments;
import com.genfare.cloud.model.entity.ShipmentsId;
import com.genfare.cloud.model.entity.Shippers;
import com.genfare.cloud.model.entity.Walletcontents;
import com.genfare.cloud.model.entity.Walletidentifiers;
import com.genfare.cloud.model.entity.WalletidentifiersId;
import com.genfare.cloud.model.entity.Wallets;
import com.genfare.cloud.model.entity.repository.CrossreferencefileRepository;
import com.genfare.cloud.model.entity.repository.FulfillmentItemsRepository;
import com.genfare.cloud.model.entity.repository.FulfillmentsRepository;
import com.genfare.cloud.model.entity.repository.OrderitemsRepository;
import com.genfare.cloud.model.entity.repository.OrdersRepository;
import com.genfare.cloud.model.entity.repository.ShipmentsRepository;
import com.genfare.cloud.model.entity.repository.ShippersRepository;
import com.genfare.cloud.model.entity.repository.WalletcontentsRepository;
import com.genfare.cloud.model.entity.repository.WalletidentifiersRepository;
import com.genfare.cloud.model.entity.repository.WalletsRepository;
import com.genfare.cloud.model.security.User;
import com.genfare.cloud.model.service.EquipmentManagementService;
import com.genfare.cloud.model.service.OrderManagementService;
import com.genfare.cloud.model.service.UserManagementService;
import com.genfare.cloud.model.tenant.TenantConfiguration;
import com.genfare.cloud.model.types.IdentifiertypesType;
import com.genfare.cloud.model.types.OrderItemStatusType;
import com.genfare.cloud.model.types.OrderStatusType;
import com.genfare.cloud.model.types.PurposesType;
import com.genfare.cloud.model.types.WalletstatusType;
import com.genfare.cloud.model.types.WallettypesType;
import com.genfare.cloud.model.util.CardPIDHelper;
import com.genfare.cloud.model.util.DateHelper;
import com.genfare.cloud.model.util.ToJsonHelper;

@Controller
public class ViewDetailsController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ViewDetailsController.class);

    @PersistenceContext
    private EntityManager em;

    @Autowired
    private TenantConfiguration config;

    @Autowired
    private OrderManagementService orderManagementService;

    @Autowired
    private OrdersRepository ordersRepository;

    @Autowired
    private OrderitemsRepository orderitemsRepository;

    @Autowired
    private ShippersRepository shippersRepository;

    @Autowired
    private ShipmentsRepository shipmentsRepository;

    @Autowired
    private FulfillmentsRepository fulfillmentsRepository;

    @Autowired
    private WalletcontentsRepository walletContentsRepository;

    @Autowired
    private FulfillmentItemsRepository fulfillmentItemsRepository;

    @Autowired
    private CrossreferencefileRepository crossReferenceRepository;

    @Autowired
    private WalletsRepository walletsRepository;

    @Autowired
    private WalletidentifiersRepository walletidRepository;

    @Autowired
    private EquipmentManagementService equipemntService;

    @Autowired
    private UserManagementService userManagementService;

    private static final Byte SMART_CARD = WallettypesType.DESFIRE_SMART_CARD.getIndex().byteValue();

    private static final Byte PRINTED_ID_IDENT = IdentifiertypesType.PRINTED_ID.getIndex().byteValue();

    private static final Byte EID_IDENT = IdentifiertypesType.ELECTRONIC_ID.getIndex().byteValue();

    @RequestMapping(value = "/fulfillment/orders/details", method = RequestMethod.POST)
    public String viewDetails(@Valid @ModelAttribute("findOrdersForm") FindOrdersForm form, BindingResult result, Model model) {
        try {
            Orders orders = orderManagementService.getOrder(form.getOrderNumber());
            unlockOrderIfTimeout(orders);
            // check if orderitem has/needs a fulfillment object. create if necessary
            for (int i = 0; i < orders.getOrderitemses().size(); i++) {
                // if order item is null, then ship all
                Orderitems item = orders.getOrderitemses().get(i);
                if (item.getOrderstatuses() != OrderStatusType.COMPLETE.getStatus()
                    && item.getOrderstatuses() != OrderStatusType.CANCELLED.getStatus()) {
                    Fulfillments ful = item.getFulfillments();
                    if (ful == null || ful.getQuantity() != item.getQuantity()) {
                        LOGGER.info("Vew details creating new fulfillment for orderId: " + orders.getOrderId()
                            + " and item number + " + item.getId().getOrderItemNumber());
                        ful = new Fulfillments(item.getId());
                        ful.setQuantity(item.getQuantity());
                        ful.setQuantityEncoded(new Integer(0).shortValue());
                        ful.setQuantityShipped(new Integer(0).shortValue());
                        ful.setOrderstatuses(OrderStatusType.IN_PROGRESS.getStatus());
                        ful.setCreatedAt(new Date());
                        ful.setCreatedBy("fulfillment");
                        ful = fulfillmentsRepository.save(ful);
                        item.setFulfillments(ful);
                    }// if fulfillment doesn't exist yet
                }// if item is not compl/canceled
            }// loop through orders
            LOGGER.info("Vew details for orderId: " + form.getOrderNumber());
            model.addAttribute("shippers", shippersRepository.findAll());
            model.addAttribute("orders", orders);
            OrderSearchDTO order = new OrderSearchDTO(orders);
            order.setIsLockedByCurrentUser(order.getIsLocked()
                && getCurrentUserEmail().equalsIgnoreCase(order.getLockedBy()));
            model.addAttribute("order", order);
            model.addAttribute("searchOrderNumber", form.getSearchOrderNumber());
            model.addAttribute("fromDate", DateHelper.getDateAsString(form.getFromDate()));
            model.addAttribute("toDate", DateHelper.getDateAsString(form.getToDate()));
            model.addAttribute("status", form.getStatus());
            model.addAttribute("encodeOnFirst", config.isEncodeFirstUse());
        } catch (EntityNotFoundException e) {
            LOGGER.info("View details: No result");
            model.addAttribute("error", e.getMessage());
        }
        return "fulfillment/view_details";
    }

    /**
     * Ship Order Items. If orderItemNumber is null, ship all items in order. Else ship just the one.
     * 
     * @param form - contains orderNumber, orderItemNumber, shipDate, tracking, and shipperId
     * @param result
     * @param model
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/fulfillment/orders/details/shipitem", method = RequestMethod.POST)
    public Object shipItem(@Valid @ModelAttribute("shipOrdersForm") ShipOrdersForm form, BindingResult result, Model model) {
        Orders order = null;
        JSONArray error = new JSONArray();
        // TODO remove heavy logic from controller and put in model. keeping in controller for now for much faster
        // builds
        try {
            // if no actions were taken, don't update order object
            boolean updated = false;

            // get default order information from form
            Byte orderItemNumber = null;
            Integer orderId = form.getOrderNumber();
            Byte originalItemNumber = form.getOrderItemNumber();

            order = orderManagementService.getOrder(orderId);
            boolean locked = checkLockStatus(order);
            if (!locked) {
                error.put("errors.order.not.locked");
                String errors = putAndCleanError(error);
                return new JSONWrapper<String>(errors, "error");
            }
            if (order == null) {
                error.put("errors.order.not.found");
                String errors = putAndCleanError(error);
                return new JSONWrapper<String>(errors, "error");
            }
            updateLock(order);
            // loop through orderItems to ship
            // for (Orderitems item : order.getOrderitemses()) {
            for (int i = 0; i < order.getOrderitemses().size(); i++) {
                // if order item is null, then ship all
                Orderitems item = order.getOrderitemses().get(i);
                if (originalItemNumber == null) {
                    orderItemNumber = item.getId().getOrderItemNumber();
                } else { // else just ship this one, will break after the one is shipped
                    orderItemNumber = originalItemNumber;
                }
                OrderitemsId oid = new OrderitemsId(orderId, orderItemNumber);

                if (oid.equals(item.getId())) {
                    Integer nextShipId = order.getShipmentses().size();

                    Fulfillments fulfillment = item.getFulfillments();
                    Short qEncoded = fulfillment.getQuantityEncoded();
                    Short qShipped = fulfillment.getQuantityShipped();

                    if (qEncoded == null) {
                        qEncoded = 0;
                    }
                    if (qShipped == null) {
                        qShipped = 0;
                    }
                    // determine how many will be shipped
                    Integer qtyToShip = (qEncoded - qShipped);
                    qShipped = new Integer(qShipped + qtyToShip).shortValue();
                    fulfillment.setQuantityShipped(qShipped);
                    // create and save shipment record
                    Shipments shipment = new Shipments();
                    shipment.setId(new ShipmentsId(orderId, nextShipId.byteValue()));
                    shipment.setOrderitems(item);
                    shipment.setOrders(order);
                    shipment.setDateShipped(form.getShipDate());
                    shipment.setShippers(new Shippers(form.getShipperId()));
                    shipment.setShipperTrackingNumber(form.getTracking());
                    shipment.setQuantity(qtyToShip.shortValue());
                    shipment.setOrderItemNumber(orderItemNumber);

                    shipment = shipmentsRepository.save(shipment);
                    fulfillment = fulfillmentsRepository.save(fulfillment);

                    item.setFulfillments(fulfillment);
                    item.getShipmentses().add(shipment);
                    order.getShipmentses().add(shipment);
                    if (fulfillment.getQuantity() == fulfillment.getQuantityShipped()) {
                        orderManagementService.updateOrderItemStatus(item, OrderItemStatusType.COMPLETE);
                        updateChildStatuses(item, OrderItemStatusType.COMPLETE);
                    }
                    updated = true;
                }

                // if only shipping one item, break after it's found and processed
                if (originalItemNumber != null && updated) {
                    break;
                }
            }
            // }
            if (!updated) {
                error.put("errors.order.not.shipped");
            }

        } catch (Exception e) {
            error.put(e.getMessage());
        }
        JSONObject js = new JSONObject();
        js.put("errors", error);

        List<Integer> orderIds = new ArrayList<Integer>();
        orderIds.add(form.getOrderNumber());
        return searchOrders(orderIds, js);
    }

    /**
     * TODO Since we can't just grab child items (yet), need to do this loop to determine children and update their statuses
     * 
     * This method will loop through all orderitems, find the non-parent items, compare those item's parent id to the passed in
     * orderitemid. if it matches, then update the child-item to the specified status
     * 
     * @param item - the parent item that was updated
     * @param status - the status to update the children to
     */
    private void updateChildStatuses(Orderitems orderitem, OrderItemStatusType status) {
        for (Orderitems item : orderitem.getOrders().getOrderitemses()) {
            if (item.getParentItemNumber() != null
                && !item.getParentItemNumber().equals(item.getId().getOrderItemNumber())) {
                // this is a child item
                if (orderitem.getId().getOrderItemNumber().equals(item.getParentItemNumber())) {
                    orderManagementService.updateOrderItemStatus(item, status);
                }
            }
        }
    }

    private String putAndCleanError(JSONArray error) {
        JSONObject js = new JSONObject();
        js.put("errors", error);
        String errors = ToJsonHelper.cleanMarkup(js.toString());
        return errors;
    }

    @RequestMapping(value = "/fulfillment/orders/details", method = RequestMethod.GET)
    public String findOrders(@ModelAttribute("findOrdersForm") FindOrdersForm form, Model model) {
        return "redirect:/fulfillment/orders";
    }

    /**
     * Encode Order Items.
     * 
     * @param form - contains orderNumber, orderItemNumber, lowPID, highPID
     * @param result
     * @param model
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/fulfillment/orders/details/encodeCards", method = RequestMethod.POST)
    public Object encodeCards(@Valid @ModelAttribute("encodeCardsForm") EncodeCardsForm form, BindingResult result, Model model) {
        Orders order = null;
        JSONArray error = new JSONArray();
        // TODO remove heavy logic from controller and put in model. keeping in controller for now for much faster
        // builds
        try {
            // if no actions were taken, don't update order object
            boolean updated = false;

            // get default order information from form
            Integer orderId = form.getOrderNumber();
            Byte orderItemNumber = form.getOrderItemNumber();
            Integer length = form.getPidLength();
            order = orderManagementService.getOrder(orderId);
            boolean locked = checkLockStatus(order);
            if (!locked) {
                error.put("errors.order.not.locked");
                String errors = putAndCleanError(error);
                return new JSONWrapper<String>(errors, "error");
            }
            if (order == null) {
                error.put("errors.order.not.found");
                String errors = putAndCleanError(error);
                return new JSONWrapper<String>(errors, "error");
            }
            updateLock(order);
            // loop through orderItems to ship
            for (int i = 0; i < order.getOrderitemses().size(); i++) {
                Orderitems item = order.getOrderitemses().get(i);

                OrderitemsId oid = new OrderitemsId(orderId, orderItemNumber);
                if (oid.equals(item.getId())) {
                    //@formatter:off
                    /*
                     * Use PID 
                     * Check if card exists in inventory 
                     * check if card used in wallets table 
                     * put card in wallets table 
                     * put items in walletContents table 
                     * update fulfillments table 
                     * update fulfillmentItems table
                     */
                    //@formatter:on

                    Long lowPID = form.getLowPID();
                    Long highPID = form.getHighPID();
                    if (highPID == null) {
                        highPID = lowPID;
                    }

                    for (long pid = lowPID; pid <= highPID; pid++) {
                        Crossreferencefile ref = checkCardCanBeEncoded(pid, error, length);
                        if (null != ref) {
                            Fulfillments fulfillment = item.getFulfillments();
                            Short qEncoded = fulfillment.getQuantityEncoded();
                            if (qEncoded == null) {
                                qEncoded = 0;
                            }

                            if (qEncoded >= item.getQuantity()) {
                                error.put("errors.order.item.already.encoded[" + item + "][" + qEncoded + "]\"");
                                continue;
                            }

                            Wallets wallet = new Wallets();
                            wallet.setPurposes(new Purposes(PurposesType.NORMAL_USE.getIndex().byteValue()));
                            wallet.setWalletstatus(WalletstatusType.ACTIVE.getStatus());
                            wallet.setWallettypes(ref.getWallettypes());
                            wallet.setIsRegistered(false);
                            wallet.setIsThirdParty(false);
                            wallet.setWasReplaced(false);
                            wallet = walletsRepository.save(wallet);

                            WalletidentifiersId pidId = new WalletidentifiersId(wallet.getWalletId(), PRINTED_ID_IDENT);
                            WalletidentifiersId eidId = new WalletidentifiersId(wallet.getWalletId(), EID_IDENT);
                            Identifiertypes pidType = new Identifiertypes(PRINTED_ID_IDENT);
                            Identifiertypes eidType = new Identifiertypes(EID_IDENT);

                            String pidStr = CardPIDHelper.leftPadID(pid, length, '0');
                            Walletidentifiers pidFier = new Walletidentifiers(pidId, pidType, wallet, pidStr);
                            Walletidentifiers eidFier =
                                new Walletidentifiers(eidId, eidType, wallet, Long.toString(ref.getElectronicId()));

                            walletidRepository.save(pidFier);
                            walletidRepository.save(eidFier);

                            Set<Walletidentifiers> ids = new HashSet<Walletidentifiers>();
                            ids.add(eidFier);
                            ids.add(pidFier);
                            wallet.setWalletidentifierses(ids);
                            wallet = walletsRepository.save(wallet);

                            fulfillment.setQuantityEncoded(++qEncoded);
                            fulfillment = fulfillmentsRepository.save(fulfillment);

                            FulfillmentItems fitem = new FulfillmentItems();

                            fitem.setOrderID(orderId);
                            fitem.setOrderItemNumber(orderItemNumber);
                            fitem.setWallet(wallet);
                            fitem.setIsShipped(false);
                            fitem = fulfillmentItemsRepository.save(fitem);

                            fulfillment.getFulfillmentItems().add(fitem);
                            fulfillment = fulfillmentsRepository.save(fulfillment);

                            item.setFulfillments(fulfillment);

                            int slotCount = wallet.getWalletcontentses().size();
                            for (int c = 0; c < order.getOrderitemses().size(); c++) {
                                Orderitems items = order.getOrderitemses().get(c);
                                // find child items
                                if (items.getParentItemNumber() != item.getParentItemNumber()
                                    && items.getParentItemNumber() == item.getId().getOrderItemNumber()) {
                                    Walletcontents contents = new Walletcontents();
                                    contents.setTickets(items.getTickets());
                                    contents.setDateExpires(items.getTickets().getDateExpires());
                                    contents.setDateStart(items.getTickets().getDateEffective());
                                    contents.setSlot(new Integer(slotCount++).byteValue());
                                    contents.setWallets(wallet);
                                    contents.setIsBad(false);
                                    walletContentsRepository.save(contents);
                                }
                            }

                            if (order.getOrderstatuses().getStatus() == OrderStatusType.IN_PROGRESS.getStatus().getStatus()) {
                                order.setOrderstatuses(OrderStatusType.FULFILLMENT.getStatus());
                                ordersRepository.save(order);
                            }
                            updated = true;
                        }
                    }// loop through PIDs
                     // only need to encode cards for 1 item
                    break;
                }// if order found
            }// loop through order items
            if (!updated) {
                error.put("errors.no.card.encoded");
            }
        } catch (Exception e) {
            error.put(e.getMessage());
        }
        JSONObject js = new JSONObject();
        js.put("errors", error);

        List<Integer> orderIds = new ArrayList<Integer>();
        orderIds.add(form.getOrderNumber());
        return searchOrders(orderIds, js);
    }

    @ResponseBody
    @RequestMapping(value = "/fulfillment/orders/details/cancel", method = RequestMethod.POST)
    public Object cancelItems(@Valid @ModelAttribute("cancelOrdersForm") CancelOrdersForm form, BindingResult result, Model model) {
        // if no actions were taken, don't update order object
        boolean updated = false;
        Orders order = null;
        JSONArray error = new JSONArray();
        // TODO remove heavy logic from controller and put in model. keeping in controller for now for much faster
        // builds
        try {

            // get default order information from form
            Byte orderItemNumber = null;
            Integer orderId = form.getOrderNumber();
            Byte originalItemNumber = form.getOrderItemNumber();

            order = orderManagementService.getOrder(orderId);
            boolean locked = checkLockStatus(order);
            if (!locked) {
                error.put("errors.order.not.locked");
                String errors = putAndCleanError(error);
                return new JSONWrapper<String>(errors, "error");
            }
            if (order == null) {
                error.put("errors.order.not.found");
                String errors = putAndCleanError(error);
                return new JSONWrapper<String>(errors, "error");
            }
            updateLock(order);
            // loop through orderItems to cancel
            for (int i = 0; i < order.getOrderitemses().size(); i++) {
                Orderitems item = order.getOrderitemses().get(i);
                // if order item is null, then cancel all
                if (originalItemNumber == null) {
                    orderManagementService.updateOrderItemStatus(item, OrderItemStatusType.CANCELLED);
                    updated = true;
                } else { // else just cancel this one, will break after the one is shipped
                    orderItemNumber = originalItemNumber;
                    OrderitemsId oid = new OrderitemsId(orderId, orderItemNumber);
                    if (oid.equals(item.getId())) {
                        orderManagementService.updateOrderItemStatus(item, OrderItemStatusType.CANCELLED);
                        updated = true;
                        break;
                    }// found correct item to cancel
                }// cancel just one
            }// for

            if (!updated) {
                error.put("Order not canceled");
            }
        } catch (Exception e) {
            LOGGER.info("View details: No result");
            error.put("errors.cancel.generic.with.message[" + e.getMessage() + "]\"");
        }
        JSONObject js = new JSONObject();
        js.put("errors", error);

        List<Integer> orderIds = new ArrayList<Integer>();
        orderIds.add(form.getOrderNumber());
        return searchOrders(orderIds, js);
    }

    private boolean checkLockStatus(Orders order) {
        String email = getCurrentUserEmail();
        if (order.getLockedAt() != null && order.getLockedBy().equalsIgnoreCase(email)) {
            return true;
        }
        return false;
    }

    /**
     * Ensures a card exists in the CrossReferenceTable and does not exist in the Wallets table
     * 
     * @param pid
     * @param error
     * @return
     */
    private Crossreferencefile checkCardCanBeEncoded(long pid, JSONArray error, int initLength) {
        // TODO remove heavy logic from controller and put in model. keeping in controller for now for much faster
        // builds
        // check if record exists in reference file (should)
        String pidstr = CardPIDHelper.leftPadID(pid, initLength, '0');
        Crossreferencefile ref = crossReferenceRepository.findByPrintedId(pidstr);
        if (null != ref && null != ref.getWallettypes()
            && ref.getWallettypes().getWalletTypeId().byteValue() == SMART_CARD) {
            // check if record exists in wallet (shouldn't)
            Wallets wallet = walletsRepository.findByElectronicId(Long.toString(ref.getElectronicId()));
            if (null != wallet) {
                // wallet already used, can't re-encode
                error.put("errors.wallet.already.encoded[" + pidstr + "]\"");
                return null;
            } else {
                // wallet in ref table and not used, can encode
                return ref;
            }
        } else {
            // wallet not in reference table, can't encode.
            // Wallet with pid " + pid + " does not exist
            error.put("errors.wallet.not.found.with.pid[" + pidstr + "]\"");
            return null;
        }

    }

    /**
     * Check if Card PID is OK to encode
     * 
     * @param cardPID
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/fulfillment/orders/details/checkCardPID", method = RequestMethod.POST)
    public Object checkCardPID(@RequestParam("cardPID") String cardPID) {
        JSONArray error = new JSONArray();
        Crossreferencefile c = checkCardCanBeEncoded(Long.valueOf(cardPID), error, cardPID.length());
        JSONObject js = new JSONObject();
        js.put("errors", error);
        return new JSONWrapper<Boolean>(c != null, ToJsonHelper.cleanMarkup(js.toString()));
    }

    @SuppressWarnings("unchecked")
    public JSONWrapper<List<OrderSearchDTO>> searchOrders(List<Integer> orderIds, JSONObject jsReturn) {
        StringBuilder query = new StringBuilder();
        query.append("SELECT DISTINCT NEW com.genfare.cloud.admin.dto.OrderSearchDTO(o) FROM Orders o ");
        query.append(" WHERE 1=1 ");
        if (orderIds != null && !orderIds.isEmpty()) {
            query.append(" AND o.orderId in (:orderIds)");
        }
        Query q = em.createQuery(query.toString());
        q.setParameter("orderIds", orderIds);

        String status = "success";
        if (jsReturn.length() > 0) {
            JSONArray err = (JSONArray) jsReturn.get("errors");
            if (err.length() > 0) {
                status = "error";
            }
        }

        List<OrderSearchDTO> results = (List<OrderSearchDTO>) q.getResultList();
        for (OrderSearchDTO order : results) {
            order.setIsLockedByCurrentUser(order.getIsLocked()
                && getCurrentUserEmail().equalsIgnoreCase(order.getLockedBy()));
        }
        return new JSONWrapper<List<OrderSearchDTO>>(results, status, ToJsonHelper.cleanMarkup(jsReturn.toString()));
    }

    @ResponseBody
    @RequestMapping(value = "/fulfillment/orders/details/checkequipment", method = RequestMethod.POST)
    public Object checkEncoderEquipment(@RequestParam("serialNumber") String serialNumber) {
        serialNumber = StringUtils.trim(serialNumber);
        JSONArray error = new JSONArray();
        if (StringUtils.isEmpty(serialNumber)) {
            LOGGER.error("Serial number is empty");
            error.put("errors.order.not.found");
            JSONObject js = new JSONObject();
            js.put("errors", error);
            return js;
        }
        return equipemntService.getEquipmentBySerial(serialNumber).getEquipmentId();
    }

    @ResponseBody
    @RequestMapping(value = "/fulfillment/orders/lock", method = RequestMethod.POST)
    public Object lockOrder(@RequestParam("ordId") Integer orderId) {
        Orders order = null;
        JSONArray error = new JSONArray();

        try {
            order = orderManagementService.getOrder(orderId);
            OrderSearchDTO o = new OrderSearchDTO(order);
            if (o.getIsFulfillable()) {
                String email = getCurrentUserEmail();

                order.setLockedAt(new Date());
                order.setLockedBy(email);
                ordersRepository.save(order);
            } else {
                error.put("errors.cannot.lock.not.fulfillable");
            }

        } catch (EntityNotFoundException e) {
            LOGGER.error("Could not find order to lock");
            error.put("errors.order.not.found");
        }
        List<Integer> orderIds = new ArrayList<Integer>();
        orderIds.add(orderId);
        JSONObject js = new JSONObject();
        js.put("errors", error);
        return searchOrders(orderIds, js);
    }

    private String getCurrentUserEmail() {
        String email = "";
        User user = null;
        if (SecurityContextHolder.getContext().getAuthentication().getPrincipal() != "anonymousUser") {
            user =
                userManagementService.getUser(((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername());
            if (user != null) {
                email = user.getEmail().getAddress();
            }
        }
        return email;
    }

    @ResponseBody
    @RequestMapping(value = "/fulfillment/orders/unlock", method = RequestMethod.POST)
    public Object unlockOrder(@RequestParam("ordId") Integer orderId) {
        Orders order = null;
        JSONArray error = new JSONArray();

        try {
            String email = getCurrentUserEmail();
            order = orderManagementService.getOrder(orderId);

            if (email.equalsIgnoreCase(order.getLockedBy())) {
                order.setLockedAt(null);
                order.setLockedBy(null);
                ordersRepository.save(order);
            } else {
                error.put("errors.order.locked.by.user[" + order.getLockedBy() + "]\"");
            }

        } catch (EntityNotFoundException e) {
            LOGGER.error("Could not find order to unlock");
            error.put("errors.order.not.found");
        }
        List<Integer> orderIds = new ArrayList<Integer>();
        orderIds.add(orderId);
        JSONObject js = new JSONObject();
        js.put("errors", error);
        return searchOrders(orderIds, js);
    }

    /**
     * Check how much time has passed since the order has been locked. if greater than X minutes, unlock the order
     * 
     * @param orderId
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/fulfillment/orders/checkLock", method = RequestMethod.POST)
    public Object checkOrderLock(@RequestParam("ordId") Integer orderId) {
        Orders order = null;
        JSONArray error = new JSONArray();
        try {
            order = orderManagementService.getOrder(orderId);
            unlockOrderIfTimeout(order);
        } catch (EntityNotFoundException e) {
            LOGGER.error("Could not find order to unlock");
            error.put("errors.order.not.found");
        }
        List<Integer> orderIds = new ArrayList<Integer>();
        orderIds.add(orderId);
        JSONObject js = new JSONObject();
        js.put("errors", error);
        return searchOrders(orderIds, js);
    }

    private void unlockOrderIfTimeout(Orders order) {
        Date lockedAt = order.getLockedAt();
        if (lockedAt != null) {
            DateTime tLocked = new DateTime(lockedAt);
            DateTime tUnlock = new DateTime(new Date());

            Minutes Interval = Minutes.minutesBetween(tLocked, tUnlock);
            Minutes minInterval = Minutes.minutes(config.getLockOrderTimeout());

            if (Interval.isGreaterThan(minInterval)) {
                order.setLockedAt(null);
                order.setLockedBy(null);
                ordersRepository.save(order);
            }

        }
    }

    private void updateLock(Orders o) {
        o.setLockedAt(new Date());
        ordersRepository.save(o);
    }
}
