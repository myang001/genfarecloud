package com.genfare.cloud.admin.config;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * @author martin
 */
public class SecurityWebApplicationInitializer
    extends AbstractSecurityWebApplicationInitializer {

}
