/**
 * Find Orders custom javascript
 *
 * @author alimoussa
 */

function init(){
	$("#order-search-results").hide();

	/**
	 * Setup Date Picker
	 */
	$( "#from" ).datepicker({
		defaultDate: "-1w",
		changeMonth: true
	});
	$( "#to" ).datepicker({
		changeMonth: true
	});

	$("#order-search-results").dataTable({
		"lengthChange": false,
		"searching": false,
		"order": [[ 1, 'desc' ], [ 2, 'asc' ]]
	});

	$("#order-search-results").show();

}

function setupDataTables(){
	_log("Setting up data tables click events");
	$("tr").on("click", ".viewDetail", function(){
		var id = $(this).parents("tr").children(".detailOrderId").text();
		$(this).parents("form.details").children("input[name='orderNumber']").val(id);
		$(this).parents("form.details").submit();
	});
}

init();

$(document).ready(function() {

});