/**
 * Find Orders custom javascript
 *
 * @author alimoussa
 */

function init(){
	$("#order-search-results").hide();

	/**
	 * Setup Date Picker
	 */
	$( "#from" ).datepicker({
		defaultDate: "-1w",
		changeMonth: true
	});
	$( "#to" ).datepicker({
		changeMonth: true
	});

	$("#order-search-results").dataTable({
		"lengthChange": false,
		"searching": false,
		"order": [[ 1, 'desc' ], [ 2, 'asc' ]]
	});

	$("#order-search-results").show();
	
	$("#takePictureButton").click(function(){
		stepTwo();
	});

}

function setupDataTables(){
	_log("Setting up data tables click events");
	$("tr").on("click", ".viewDetail", function(){
		var id = $(this).parents("tr").children(".detailOrderId").text();
		$(this).parents("form.details").children("input[name='orderNumber']").val(id);
		$(this).parents("form.details").submit();
	});
}

init();

$(document).ready(function() {

});

function stepTwo() {
	/*
	$.ajax({url:"/admin/id_cards/bulk/bulkStepTwo",success:function(result){
		$("#bulkStepContainer").html(result);
	}});
	*/

	/*
	$.ajax({
		url: "/admin/id_cards/bulk/bulkStepTwo",
		type: "POST",
		data: { "_csrf": _csrfToken }
	}).done(function(data) {
		_log(data);
		if(data.errors.length>0) {
			var errors = "<ul>";
			$.each(data.errors, function(i,o) {
				errors += "<li>"+o+"</li>";
			});
			errors += "</ul>";
			$("#encodeModal .alert").html(errors).show();
		}
	}).fail(function(data) {
		_log("Failure loading bulk id cards step two.");
	});
	*/
}

function stepThree() {
	/*
	$.ajax({
		url: "/admin/id_cards/bulk/bulkStepThree",
		type: "POST",
		data: { "_csrf": _csrfToken }
	}).done(function(data) {
		_log(data);
		if(data.errors.length>0) {
			var errors = "<ul>";
			$.each(data.errors, function(i,o) {
				errors += "<li>"+o+"</li>";
			});
			errors += "</ul>";
			$("#encodeModal .alert").html(errors).show();
		}
	}).fail(function(data) {
		_log("Failure loading bulk id cards step three.");
	});
	*/
}