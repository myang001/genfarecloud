jQuery(function($) {
	$('.well-height').responsiveEqualHeightGrid();
	$('.panel-height').responsiveEqualHeightGrid();
	$('.alert-signin').bind('close', function () {
		$('.panel-height').responsiveEqualHeightGrid();
	});
});
//$('.well-height').equalize({reset: true});
//$('.panel-height').equalize({reset: true});

// set wells to identical heights
/*
boxes = $('.panel-signin, .panel-signup, .panel-guest');
maxHeight = Math.max.apply(
Math, boxes.map(function() {
    return $(this).height();
}).get());
boxes.height(maxHeight);
*/
//$('.alert-signin').bind('closed.bs.alert', function () {

//	maxHeight = Math.max.apply(
//			Math, boxes.map(function() {
//			    return $(this).height();
//			}).get());
//			boxes.height(maxHeight);	  
//	});

 
// accordion close on click
$(function () {
    var active = true;

    $('#collapse-init').click(function () {
        if (active) {
            active = false;
            $('.panel-collapse').collapse('show');
            $('.panel-title').attr('data-toggle', '');
            $(this).text('Enable accordion behavior');
        } else {
            active = true;
            $('.panel-collapse').collapse('hide');
            $('.panel-title').attr('data-toggle', 'collapse');
            $(this).text('Disable accordion behavior');
        }
    });
    
    $('#accordion').on('show.bs.collapse', function () {
        if (active) $('#accordion .in').collapse('hide');
    });

});

// form validator
$(function () { $("input,select,textarea").not("[type=submit]").jqBootstrapValidation(); });

// accordion chevrons
function toggleChevron(e) {
    $(e.target)
        .prev('.panel-heading')
        .find('i.indicator')
        .toggleClass('glyphicon-chevron-down glyphicon-chevron-up');
}

$('#accordion').on('hidden.bs.collapse', toggleChevron);
$('#accordion').on('shown.bs.collapse', toggleChevron);

// used for birthday selection
$(function() {
    for (i = new Date().getFullYear(); i > 1900; i--) {
        $('#years').append($('<option />').val(i).html(i));
    }
    for (i = 1; i < 13; i++) {
        $('#months').append($('<option />').val(i).html(i));
    }
    updateNumberOfDays();
    $('#months').change(function(){
        updateNumberOfDays();
    });
    $('#years, #months, #days').change(function(){    
        month=$('#months').val();
        year=$('#years').val();       
        day=$('#days').value;
        $('#dateOfBirth').val("" + $('#months').val() + "/" + $('#days').val() + "/" + $('#years').val());
    });
});

function updateNumberOfDays(){
    $('#days').html('');
    month=$('#months').val();
    year=$('#years').val();
    days=daysInMonth(month, year);

    for(i=1; i < days+1 ; i++){
            $('#days').append($('<option />').val(i).html(i));
    }
}

function daysInMonth(month, year) {
    return new Date(year, month, 0).getDate();
}


