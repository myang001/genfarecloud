/**
 * 
 */


function init(){
	
	/**
	 * Setup Date Picker
	 */
	$( "#from" ).datepicker({
		defaultDate: "-1w",
		changeMonth: true
	});
	$( "#to" ).datepicker({
		changeMonth: true
	});
	
	$("#order-search-results").dataTable({
		"lengthChange": false,
		"searching": false
	});
	
}


init();