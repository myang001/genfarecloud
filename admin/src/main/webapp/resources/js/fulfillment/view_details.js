/**
 * Order Details custom javascript
 * 
 * @author garyharper, alimoussa
 */

var _order = {};
var _orderId = null;
var _orderItemNumber = null;
var _encoderSet = false;
var _resetTable = "";
var _locked = false;
var _resetSingleEncoder = "";
var _resetBulkEncoder = "";
var _bulkPresent = false;
var _numEncoded = 0;
var _numRemaining = 0;

$(document).ready(function(){
	init();
});

function init() {

	_orderId = $("#order-id").text();

	$( "#shipDateModal" ).datepicker();
	if(!$(".encodeModalNavCol div").hasClass("selected")){
		$('#singleCardEncodeNav').addClass("selected");
		$("#singleCardEncodeText").show();
	};

	checkOrderLockState();
	setupOnClick();
}

function allowActions() {
	$(".lineItemDetails button, .allLineItemsButton").show();
}

function checkOrderLockState() {
	$.ajax({
		url: extract_url("/fulfillment/orders/checkLock"),
		type: "POST",
		data: { ordId: _orderId, "_csrf": _csrfToken }
	}).done(function(data) {
		_log(data);
		_order = data.value[0];
		updatePage(_order);
	}).fail(function(data) {
		_err("Failure retrieving order with id: "+_orderId);
		_err(data);
	});
}


function unlockOrder() {
	$.ajax({
		url: extract_url("/fulfillment/orders/unlock"),
		type: "POST",
		data: { ordId: _orderId, "_csrf": _csrfToken }
	}).done(function(data) {
		_log(data);
		_order = data.value[0];
		if(data.status != "success") {
			data.errors = JSON.parse(data.errors).errors;
			var errors = "<ul>";
			$.each(data.errors, function(i,o) {
				errors += "<li>"+_getMessage(o)+"</li>";
			});
			errors += "</ul>";
			clearAlerts();
			globalAlert(errors, -1);
		} else {
			updatePage(_order);
		}
	}).fail(function(data) {
		_err("Failure retrieving order with id: "+_orderId);
		_err(data);
	});
}

function lockOrder() {
	$.ajax({
		url: extract_url("/fulfillment/orders/lock"),
		type: "POST",
		data: { ordId: _orderId, "_csrf": _csrfToken }
	}).done(function(data) {
		_log(data);
		_order = data.value[0];
		if(data.status != "success") {
			data.errors = JSON.parse(data.errors).errors;
			var errors = "<ul>";
			$.each(data.errors, function(i,o) {
				errors += "<li>"+_getMessage(o)+"</li>";
			});
			errors += "</ul>";
			clearAlerts();
			globalAlert(errors, -1);
		} else {
			updatePage(_order);
		}

	}).fail(function(data) {
		_err("Failure retrieving order with id: "+_orderId);
		_err(data);
	});
}

function allowCurrentUser() {
	$.ajax({
		url: extract_url("/fulfillment/orders/canEdit"),
		type: "POST",
		data: { ordId: _orderId, "_csrf": _csrfToken }
	}).done(function(data) {
		_log(data);
		_order = data.value[0];
		if(data.status != "success") {
			data.errors = JSON.parse(data.errors).errors;
			var errors = "<ul>";
			$.each(data.errors, function(i,o) {
				errors += "<li>"+_getMessage(o)+"</li>";
			});
			errors += "</ul>";
			clearAlerts();
			globalAlert(errors, -1);
		} else {
			updatePage(_order);
		}

	}).fail(function(data) {
		_err("Failure retrieving order with id: "+_orderId);
		_err(data);
	});
}

function updatePage(order) {
	var isFulfillable = order.isFulfillable;
	_locked = order.isLocked;
	_lockedByCurrentUser = order.isLockedByCurrentUser;
	var lockedAt = order.lockedAt;
	var lockedBy = order.lockedBy;

	if(!isFulfillable) {
		$("#warn-lock").hide();
	}
	$("#orderEncodedStatus").text(_order.isEncoded);
	$("#orderShippedStatus").text(_order.isShipped);
	//set productsTable default
	if(_resetTable == "") {
		_resetTable = $("table.productsTable tbody").html();
	}
	var canShipAll = false;
	var canCancelAll = false;
	//updEnc_log("updating page with order object: ");
	//updEnc_log(order);
	//generate the product table based on order description
	$.each(order.orderItems, function(i,o) {
		//updEnc_log("Looping through orderitem: ");
		//updEnc_log(o);
		var prodTable = _resetTable;
		prodTable += "<tr>";
		prodTable += "<td class='description'>";
		prodTable += o.description;
		prodTable += "</td>";
		prodTable += "<td class='unitPrice'>";
		prodTable += formatMoney(o.unitPrice);
		prodTable += "</td>";
		prodTable += "<td class='extPrice'>";
		prodTable += formatMoney(o.extPrice);
		prodTable += "</td>";
		prodTable += "</tr>";
		$.each(o.children, function(ix, c) {
			prodTable += "<tr>";
			prodTable += "<td class='description'>";
			prodTable += c.description;
			prodTable += "</td>";
			prodTable += "<td class='unitPrice'>";
			prodTable += formatMoney(c.unitPrice);
			prodTable += "</td>";
			prodTable += "<td class='extPrice'>";
			prodTable += formatMoney(c.extPrice);
			prodTable += "</td>";
			prodTable += "</tr>";

			if(c.autoload) {
				$(".lineItemObject[name="+o.itemNumber+"]").parent().find(".lineItemActionButtons").hide();
				$(".lineItemObject[name="+o.itemNumber+"]").find("#isauto").show();
			}
		});
		$("table[name="+o.itemNumber+"] tbody").html(prodTable);
		$("span.subtotal[name="+o.itemNumber+"]").text(formatMoney(o.subTotal));
		//if the item has any encoded ranges, populate them here
		if(o.ranges && o.ranges.length > 0) {
			var ranges = ""; 
			$.each(o.ranges, function(ix, r) {
				ranges += r;
				ranges += "<br/>";
			});
			$("span.pids[name="+o.itemNumber+"]").html(ranges);
		}


		var lineItemDetails = $(".lineItemObject[name="+o.itemNumber+"]");
		var lineItem = lineItemDetails.parent();
		var btnRow = lineItem.find(".buttonRow");
		var shipButton = btnRow.find(".shipLine");
		var encodeButton = btnRow.find(".encodeLine");
		var cancelButton = btnRow.find(".cancelLine");
		//updEnc_log("qShipped: "+o.qShipped);
		//updEnc_log("qEncoded: "+o.qEncoded);
		if(!_locked || !isFulfillable || !_lockedByCurrentUser) {
			$(".lineItemDetails button, .allLineItemsButton").hide();
		} else if (isFulfillable && _lockedByCurrentUser) {
			if(isCompleteOrCanceled(_order.orderStatus)) {
				$(".cancelAllLineItemsButton, .shipAllLineItemsButton, .shipLine, .encodeLine, .cancelLine").addClass("disabled").hide();
				if(isCanceled(_order.orderStatus)){
					$(".shippingLabelsButton").addClass("disabled").hide();
				}
			} else if (isCompleteOrCanceled(o.status)) {
				$(btnRow).find("button").addClass("disabled").hide();
			} else {
				$(".lineItemDetails button, .allLineItemsButton").show();
				if (o.qShipped == o.qEncoded) {
					//updEnc_log("Can't Ship");
					shipButton.addClass("disabled");
				} else {
					//updEnc_log("Can Ship");
					shipButton.removeClass("disabled");
					canShipAll = true;
				}

				if (o.qEncoded == o.quantity) {
					//updEnc_log("Can't Encode");
					encodeButton.addClass("disabled");
				} else {
					//updEnc_log("Can Encode");
					encodeButton.removeClass("disabled");
				}

				if (o.qEncoded > 0) {
					//updEnc_log("Can't Cancel");
					cancelButton.addClass("disabled");
				} else {
					//updEnc_log("Can Cancel");
					canCancelAll = true;
				}
			}
		}

		if(o.qShipped == o.quantity) { //all fulfilled
			$(".lineItemObject[name="+o.itemNumber+"]").find(".glyphicon-remove-circle").addClass("hiddenFix");
			$(".lineItemObject[name="+o.itemNumber+"]").find(".glyphicon-ok-circle").removeClass("hiddenFix");
		} else if(o.qEncoded == o.quantity){//all encoded but not shipped
			$(".lineItemObject[name="+o.itemNumber+"]").find(".glyphicon-remove-circle").removeClass("hiddenFix");
			$(".lineItemObject[name="+o.itemNumber+"]").find(".glyphicon-ok-circle").addClass("hiddenFix");
			$(".lineItemObject[name="+o.itemNumber+"]").find(".lineLeftEncoded").find(".glyphicon-ok-circle").removeClass("hiddenFix");
			$(".lineItemObject[name="+o.itemNumber+"]").find(".lineLeftEncoded").find(".glyphicon-remove-circle").addClass("hiddenFix");
		} else { //not all encoded or shipped
			$(".lineItemObject[name="+o.itemNumber+"]").find(".glyphicon-remove-circle").removeClass("hiddenFix");
			$(".lineItemObject[name="+o.itemNumber+"]").find(".glyphicon-ok-circle").addClass("hiddenFix");
		}
	});//loop through each orderitem
	if(_locked) {
		if(_lockedByCurrentUser) {
			if(canCancelAll) {
				$(".cancelAllLineItemsButton").removeClass("disabled");
			} else {
				$(".cancelAllLineItemsButton").addClass("disabled");
			}
			if(canShipAll) {
				$(".shipAllLineItemsButton").removeClass("disabled");
			} else {
				$(".shipAllLineItemsButton").addClass("disabled");
			}
			
			$("#lockOrder").text(_getMessage("admin.order.details.unlock.order")).show();
		} else {
			$("#lockOrder").hide();			
		}
		$("#warn-info").text(_getMessage("admin.order.details.unlocked.message["+lockedAt+"]["+lockedBy+"]"));
	} else {
		$("#warn-info").text(_getMessage("admin.order.details.locked.message"));
		$("#lockOrder").text(_getMessage("admin.order.details.lock.order")).show();
	}
	
	if(isCompleteOrCanceled(order.orderStatus)) {
		//unlock the order & hide the lock info
		$("#warn-lock").hide();
		if(_locked) {
			unlockOrder();
		}
	}
}

function setupEncoder(item) {
	_log("Setting up encoder");
	var dfd = new $.Deferred();
	_log("Find encoder list");
	var encoderList = "";

	try {
		var encoderListResult = encoderApplet.getEncoderList();
		if(encoderListResult.getSuccess()) {
			encoderList = encoderListResult.getValue();
		}
	} catch (e) {
		encodeAlert(_getMessage("order.details.cannot.find.encoder"), -1);
		_err("getting encoder failed: " + e.message);
		dfd.reject("dfderror");
	}

	var encoders = $.parseJSON(encoderList);
	var encoder = "";
	for (var j=0; j < encoders.length; j++) {
		 if (encoders[j].indexOf("OMNIKEY CardMan 5x21-CL") == 0 ) { //bulk printer&encoder
			 encoder = encoders[j];
		 }
	}

	if(encoders.length == 0) {
		encodeAlert(_getMessage("order.details.cannot.find.encoder"), -1);
		_err("Error, encoder not found!");
		dfd.reject("dfderror");
	}

	if(dfd.state() == "rejected") {
		return dfd.promise();
	}

	_log("find serial number");
	var serialNumber = "";
	try {
		var serialNumberResult = encoderApplet.getSerialNumber(encoder);
		serialNumber = serialNumberResult.getValue();
	} catch (e) {
		encodeAlert(_getMessage("order.details.cannot.read.serial"), -1);
		_err("Error reading serial number: " + e.message);
		dfd.reject("dfderror");
	}

	if(!serialNumber || serialNumber.length === 0) {
		encodeAlert(_getMessage("order.details.cannot.read.serial"), -1);
		_err("Error, encoder serial number cannot be read");
		dfd.reject("dfderror");
	}
	if(dfd.state() == "rejected") {
		return dfd.promise();
	}
	_log("check encoder equipment in database");
	$.ajax({
		url: extract_url("/fulfillment/orders/details/checkequipment"),
		type: "POST",
		data: { serialNumber: serialNumber, "_csrf": _csrfToken }
	}).done(function(eqID) {
		_log(eqID);

		_log("set encoder in applet");
		var config = {
			"agency_id" : 1, 
			"priorities_and_configuration_type" : 1,
			"end_of_transit_day" : 0,
			"printed_id_file_version" : 1, 
			"card_properties_file_version" : 3,
			"product_list_file_version" : 2,
			"product_file_version" : 2,
			"journal_file_version" : 1,
			"equipment_id" : eqID
		};
		var encoderConfigJSON = JSON.stringify(config);
		try {
			var setEncoderResult = encoderApplet.setEncoder(encoder, encoderConfigJSON);
			_encoderSet = setEncoderResult.getSuccess();
			_log("SettingEncoder "+ _encoderSet);
		} catch (e) {
			_err("ErrorSetEncoderResult "+ e.message);
			dfd.reject("dfderror");
		}

		if(!_encoderSet) {
			encodeAlert(_getMessage("order.details.cannot.config.encoder"), -1);
			_err("ErrorNotEncoderSet: Encoder not found");
			dfd.reject("dfderror");
		}
		dfd.resolve(item);
	}).fail(function(data) {
		encodeAlert(_getMessage("order.details.cannot.read.serial"), -1);
		_err("Failure retrieving equipment for serial number: "+serialNumber);
		dfd.reject("dfderror");
	});
	_log("dock complete");
	return dfd.promise();

}//setup encoder

function dockCard() {
	var dfd = new $.Deferred();

	try {
		var dockResult = encoderApplet.dockCard();

		if(!dockResult.getSuccess()) {
			_err("Error docking card. Please check bulk encoder and try again", -1);
	    	dfd.reject("dfderror", _getMessage("order.details.cannot.read.bulk.pid"));
		}

		//ensure can read cardPID
		var cardPID = "";
		try {
			var cardPIDResult = encoderApplet.getCardPID();  
			if(!cardPIDResult.getSuccess()) {
				_err("Error getting PID: " + cardPIDResult.getMessage());
				dfd.reject("dfderror", _getMessage("order.details.cannot.read.bulk.pid"));
			}
			cardPID = cardPIDResult.getValue();
		} catch (e) {
			_err("Error getting PID: "+ e.message);
			encoderApplet.ejectCard();
			dfd.reject("dfderror", _getMessage("order.details.cannot.read.bulk.pid"));
		}

		if(!cardPID || cardPID.length === 0) {
	    	encoderApplet.ejectCard();
	    	dfd.reject("dfderror", _getMessage("order.details.cannot.read.bulk.pid"));
		} else {
			dfd.resolve();
		}
		_log("dock complete");
	} catch (e) {
		encodeAlert(e, -1);
		dfd.reject("dfderror", e);
	}
	return dfd.promise();
}

function encodeSingleCard(itemNum) {
	$.when(validateCardOnEncoder(itemNum)).then(function(rtn){
		confirmPID(rtn, itemNum).then(function(rtn){
			encodeProductsOnCard(itemNum, rtn).then(function(rtn){
				encodeCardsInDatabase(_orderId, itemNum, rtn, rtn)
				.fail(function(rtn, msg){
					encodeAlert(msg, -1);
					_err("Failed to encode cards in DB");
				});//encodeCardsInDatabase
			}).fail(function(rtn, msg){
				encodeAlert(msg, -1);
				_err("Failed to encode products on card");
			});//encodeProductsOnCard
		}).fail(function(rtn, msg){
			encodeAlert(msg, -1);
			_err("Failed to confirm PID");
		});//confirmPID
	}).fail(function(rtn, msg){
		encodeAlert(msg, -1);
		_err("Failed to validate card on encoder");
	});//validateCardOnEncoder
}

function encodeBulkCards(itemNum, qty) {
	_log("--------------------- encode multiple cards, "+qty+" left.");
	$.when(dockCard()).then(function(rtn) {
		validateCardOnEncoder(itemNum).then(function(rtn){
			confirmPID(rtn, itemNum).then(function(rtn){
				encodeProductsOnCard(itemNum, rtn).then(function(rtn){
					encodeCardsInDatabase(_orderId, itemNum, rtn, rtn).then(function(rtn){
						encoderApplet.ejectCard();
						//set a timeout for ejecting card before encoding next one
						setTimeout(function(){
							qty--;
							if(qty > 0) {
								encodeBulkCards(itemNum, qty);
							}
						}, 13000);
					}).fail(function(rtn, msg){
						bulkError(msg, $("#encodeBulkQty").val());
						_err("Failed to encode cards in DB");
						encoderApplet.ejectCard();
					});//encodeCardsInDatabase
				}).fail(function(rtn, msg){
					bulkError(msg, $("#encodeBulkQty").val());
					_err("Failed to encode products on card");
					encoderApplet.ejectCard();
				});//encodeProductsOnCard
			}).fail(function(rtn, msg){
				bulkError(msg, $("#encodeBulkQty").val());
				_err("Failed to confirm PID");
				encoderApplet.ejectCard();
			});//confirmPID
		}).fail(function(rtn, msg){
			bulkError(msg, $("#encodeBulkQty").val());
			_err("Failed to validate card on encoder");
			encoderApplet.ejectCard();
		});//validateCardOnEncoder
	}).fail(function(rtn, msg){
		bulkError(msg, $("#encodeBulkQty").val());
		_err("Failed to dock card");
		encoderApplet.ejectCard();
	});
}

/**
 * check card PID and ensure it can be used, then encode the card
 */
function validateCardOnEncoder(itemNum) {
	var dfd = new $.Deferred();
	_log("Encode single card");

	var cardPID = "";

	//get card PID
	try {
		var cardPIDResult = encoderApplet.getCardPID();
		if(!cardPIDResult.getSuccess()) {
			_err("Error getting PID: " + cardPIDResult.getMessage());
			if(_bulkPresent){
				dfd.reject("dfderror", _getMessage("order.details.cannot.read.bulk.pid"));
			} else {
				dfd.reject("dfderror", _getMessage("order.details.cannot.read.single.pid"));
			}
		} else {
			cardPID = cardPIDResult.getValue();
		}
	} catch (e) {
		_err("Error getting PID: "+ e.message);
		if(_bulkPresent){
			dfd.reject("dfderror", _getMessage("order.details.cannot.read.bulk.pid"));
		} else {
			dfd.reject("dfderror", _getMessage("order.details.cannot.read.single.pid"));
		}
	}

	if(!cardPID || cardPID.length === 0) {
		_log("ErrorPIDLength, Card Printed ID cannot be read");
		if(_bulkPresent){
			dfd.reject("dfderror", _getMessage("order.details.cannot.read.bulk.pid"));
		} else {
			dfd.reject("dfderror", _getMessage("order.details.cannot.read.single.pid"));
		}
	} else {
		_log(cardPID);
		dfd.resolve(cardPID);
	}

	return dfd.promise();

}

function getCurrentOrderToEncode() {
	var dfd = new $.Deferred(); // Create Deferred
	$.ajax({
		url: extract_url("/fulfillment/orders/id"),
		type: "POST",
		data: { orderId: _orderId, "_csrf": _csrfToken }
	}).done(function(data) {
		_log(data);
		_order = data.value[0];
		dfd.resolve();
	}).fail(function(data) {
		_err("Failure retrieving order with id: "+_ordId);
	});
	return dfd.promise();
}

function encodeCardsInDatabase(orderNum, orderItemNum, lowPID, highPID) {
	var dfd = new $.Deferred(); // Create Deferred
	$.ajax({
		url: extract_url("/fulfillment/orders/details/encodeCards"),
		type: "POST",
		data: { orderNumber: orderNum, orderItemNumber: orderItemNum, lowPID: lowPID, highPID: highPID, pidLength: lowPID.length, "_csrf": _csrfToken }
	}).done(function(data) {
		_log(data);
		$("input[name='lowPID'],input[name='highPID']").val('');
		if(data.status != "success") {
			data.errors = JSON.parse(data.errors).errors;
			var errors = "<ul>";
			$.each(data.errors, function(i,o) {
				errors += "<li>"+_getMessage(o)+"</li>";
			});
			errors += "</ul>";
			clearAlerts();
			dfd.reject("dfderror", errors);
		} else {
			_order = data.value[0];
			updateEncodeRecords(data.value[0], orderItemNum, lowPID, highPID);
			dfd.resolve(lowPID, highPID);
		}
	}).fail(function(data) {
		_log("Failure encoding order with id: "+orderNum+" and PID range "+lowPID+" to "+highPID+".");
	});
	_log("Finished encodeCardsInDatabase");
	return dfd.promise();
}

function shipItems(orderNum, orderItemNum, shipDate, tracking, shipperId) {
	$.ajax({
		url: extract_url("/fulfillment/orders/details/shipitem"),
		type: "POST",
		data: { orderNumber: orderNum, orderItemNumber: orderItemNum, shipDate: shipDate, tracking: tracking, shipperId: shipperId,  "_csrf": _csrfToken}
	}).done(function(data) {
		_log(data);
		if(data.status != "success") {
			data.errors = JSON.parse(data.errors).errors;
			var errors = "<ul>";
			$.each(data.errors, function(i,o) {
				errors += "<li>"+_getMessage(o)+"</li>";
			});
			errors += "</ul>";
			clearAlerts();
			shipAlert(errors, -1);
		}
		_order = data.value[0];
		updateShippedRecords(data.value[0], orderItemNum);
	}).fail(function(data) {
		_log("Failure shipping order with id: "+orderNum+". Response text is: "+data.responseText);
	});
}

function cancelItems(orderNum, orderItemNum) {
	$.ajax({
		url: extract_url("/fulfillment/orders/details/cancel"),
		type: "POST",
		data: { orderNumber: orderNum, orderItemNumber: orderItemNum, "_csrf": _csrfToken }
	}).done(function(data) {
		_log(data);
		if(data.status != "success") {
			data.errors = JSON.parse(data.errors).errors;
			var errors = "<ul>";
			$.each(data.errors, function(i,o) {
				errors += "<li>"+_getMessage(o)+"</li>";
			});
			errors += "</ul>";
			clearAlerts();
			cancelAlert(errors, -1);
		}
		_order = data.value[0];
		updateCanceledRecords(data.value[0], orderItemNum);
	}).fail(function(data) {
		_log("Failure canceling order with id: "+orderNum+" and orderitem "+orderItemNumber+". Response text is: \n"+data.responseText);
	});
}

function updateShippedRecords(order, item) {
	if(order && order.orderItems && order.orderItems.length > 0) {
		if(item == null) {
			$.each(order.orderItems, function(i,o) {
				var qShipped = o.qShipped;
				$(".lineItemObject[name='"+i+"'] .quantityShipped").text(qShipped);
			});
		} else {
			var allItems = getAllItems(order);
			var qShipped = allItems[item].qShipped;
			$(".lineItemObject[name='"+item+"'] .quantityShipped").text(qShipped);
		}

		$("#ship-modal-close").click();
		clearAlerts();
		globalAlert(_getMessage("messages.ship.success"), 1);

		updatePage(order);
	} else {
		var errors = "<ul><li>"+_getMessage("errors.retrieve.order.information")+"</li></ul>";
		clearAlerts();
		shipAlert(errors, -1);
	}
}

function updateEncodeRecords(order, item, lowPID, highPID) {
	var numLeft = $('#encodeModalRemaining').text();
	if(order && order.orderItems && order.orderItems.length > 0) {
		if(item == null) {//will this ever be null...?
			//updEnc_log("encode item null");
			$.each(order.orderItems, function(i,o) {
				var qEncoded = o.qEncoded;
				$(".lineItemObject[name='"+i+"'] .quantityEncoded").text(qEncoded);
			});
		} else {
			var allItems = getAllItems(order);
			var qEncoded = allItems[item].qEncoded;
			$(".lineItemObject[name='"+item+"'] .quantityEncoded").text(qEncoded);
			numLeft = allItems[item].quantity - qEncoded;
		}

		clearAlerts();
		var message = "";
		if(highPID && highPID != lowPID) {
			_numEncoded = (parseInt(highPID) - parseInt(lowPID))+1;
			message = _getMessage("messages.encode.success.low.high["+lowPID+"]["+highPID+"]");
		} else {
			message = _getMessage("messages.encode.success.pid["+lowPID+"]");
		}
		if(_bulkPresent) {
			_numEncoded++;
			_numRemaining = parseInt($("#encodeBulkQty").val())-_numEncoded;
			message = _getMessage("messages.encode.success.pid.bulk["+lowPID+"]["+_numEncoded+"]["+_numRemaining+"]");
		}
		encodeAlert(message, 1);
		updatePage(order);
		//set numToEncode
		$('#encodeModalRemaining').text(numLeft);
		if(numLeft == 0) {
			$("#encode-modal-close").click();
			clearAlerts();
			if(_bulkPresent) {
				if(highPID && highPID)
				globalAlert(_getMessage("order.details.bulk.encode.complete["+_numEncoded+"]["+item+"]"), 1);
			} else {
				globalAlert(_getMessage("order.details.all.encoded["+item+"]"), 1);
			}
		}
	} else {
		var errors = "<ul><li>"+_getMessage("errors.retrieve.order.information")+"</li></ul>";
		clearAlerts();
		encodeAlert(errors, -1);
	}
}

function updateCanceledRecords(order, item) {
	if(order && order.orderItems && order.orderItems.length > 0) {
		$("#cancel-modal-close").click();
		clearAlerts();
		globalAlert(_getMessage("messages.cancel.success"), 1);
		updatePage(order);
	} else {
		var errors = "<ul><li>"+_getMessage("errors.retrieve.order.information")+"</li></ul>";
		clearAlert();
		cancelAlert(errors, -1);
	}
}

function encodeProductsOnCard(itemNum, cardPID) {
	var dfd = new $.Deferred();
	//get order details & setup to encode
	var order = _order;
	var periodPassProduct = null; 
	var storedRideProduct = null;
	var storedValueProduct = null;
	var fareCode = 1;

	$.each(order.orderItems, function(i,o) {
		if(o.itemNumber == itemNum) {
			if(o.fareCodeID && parseInt(o.fareCodeID)){
				fareCode = o.fareCodeID;	
			}
			
			//parent item won't have any tickets to encode. parent item will be the wallet
			$.each(o.children, function(ix,c) {
				if(c.hasTicket) { //TODO this may need to change if we need to encode userprofiles on cards.
					var ticketType = c.ticketType;
				    var ticketID = c.ticketID;
				    var ticketStart = c.ticketStart;
				    var ticketEnd = c.ticketEnd;
				    var typeExpiration = c.typeExpiration;
				    var value = c.value;
				    var repValue = c.value;
				    var repThreshold = c.repThreshold;
				    if(ticketType == c.fixedPeriod) {
				    	periodPassProduct = {
								"product_type" : ticketType,
								"designator" : ticketID,
								"designator_details" : 0,
								"start_date" : ticketStart,
								"exp_date" : ticketEnd,
								"is_linked_to_user_profile" : false,
								"type_expiration" : typeExpiration,
								"add_time" : 0,
								"days" : value
						};
				    } else if (ticketType == c.storedRide) {
				    	storedRideProduct = {
				    			"product_type" : ticketType,
								"designator" : ticketID,
								"designator_details" : 0,
								"start_date" : ticketStart,
								"exp_date" : ticketEnd,
								"is_linked_to_user_profile" : false,
								"remaining_rides" : value,
								"recharge_rides" : repValue,
								"threshold" : repThreshold

						};
				    } else if (ticketType == c.storedFixedValue) {
				    	storedValueProduct = {
				    			"product_type" : ticketType,
								"designator" : ticketID,
								"designator_details" : 0,
								"start_date" : ticketStart,
								"exp_date" : ticketEnd,
								"is_linked_to_user_profile" : false,
								"remaining_value" : value,
								"replenish_amount" : repValue,
								"threshold" : repThreshold
						};
				    }//if tickettype
				}//if has a ticket to be encoded
			});

		}//if correct item num
	});

	var periodPassProductJSON = null;
	if(periodPassProduct != null) {
		periodPassProductJSON = JSON.stringify(periodPassProduct);
	}

	var storedRideProductJSON = null;
	if(storedRideProduct != null) {
		storedRideProductJSON = JSON.stringify(storedRideProduct);
	}

	var storedValueProductJSON = null;
	if(storedValueProduct != null) {
		storedValueProductJSON = JSON.stringify(storedValueProduct);
	}

	try {
		var encodeResult = encoderApplet.encodeCard(
			cardPID,
			fareCode, //userprofile
			0, //language
			0, //accessibility
			periodPassProductJSON, 
			storedRideProductJSON, 
			storedValueProductJSON
		);

		if(encodeResult.getSuccess()) {
			// update database
			_log("Success: Encoded on card");
			dfd.resolve(cardPID);
		} else {
			_err("Error encodeResult " + encodeResult.getMessage());
			dfd.reject("dfderror", encodeResult.getMessage());
		}

	} catch(e) {
		_err("Error encoding card "+ e.message);
		dfd.reject("dfderror");
	}
	_log("finished encodeProductsOnCard");
	return dfd.promise();
}

function confirmPID(cardPID){
	var dfd = new $.Deferred(); 
	$.ajax({
		url: extract_url("/fulfillment/orders/details/checkCardPID"),
		type: "POST",
		data: { cardPID: cardPID,  "_csrf": _csrfToken}
	}).done(function(data) {
		var errs = JSON.parse(data.status).errors;
		if(!data.value) {
			var errors = "<ul>";
			$.each(errs, function(i,o) {
				errors += "<li>"+_getMessage(o)+"</li>";
			});
			errors += "</ul>";
			dfd.reject("dfderror", errors);
		} else {
			dfd.resolve(cardPID);
		}
	}).fail(function(data) {
		_err("Failure checking if card PID is valid");
		dfd.reject("dfderror", _getMessage("order.details.cannot.encode.card"));
	});
	_log("finished confirmPID");
	return dfd.promise();
}


function isCompleteOrCanceled(status) {
	if (status.toUpperCase() == "CANCELED"
			|| status.toUpperCase() == "CANCELLED"
			|| status.toUpperCase() == "COMPLETE"
			|| status.toUpperCase() == "COMPLETED") {
		return true;
	}
	return false;
}

function isCanceled(status) {
	if (status.toUpperCase() == "CANCELED"
		|| status.toUpperCase() == "CANCELLED") {
		return true;
	}
	return false;
}

function globalAlert(msg, status) {
	$("#global-alert-bad-div").hide();
	$("#global-alert-good-div").hide();
	if(status == -1) {
		$("#global-alert-bad-div #info").html(msg);
		$("#global-alert-bad-div").show();
	} else if(status == 1) {
		$("#global-alert-good-div #info").html(msg);
		$("#global-alert-good-div").show();
	}
}

function encodeAlert(msg, status) {
	$("#encodeModal .alert-danger").hide();
	$("#encodeModal .alert-success").hide();
	if(status == -1) {
		$("#encodeModal .alert-danger #info").html(msg);
		$("#encodeModal .alert-danger").show();
	} else if(status == 1) {
		$("#encodeModal .alert-success #info").html(msg);
		$("#encodeModal .alert-success").show();
	}
}

function shipAlert(msg, status) {
	$("#shipModal .alert-danger").hide();
	$("#shipModal .alert-success").hide();
	if(status == -1) {
		$("#shipModal .alert-danger #info").html(msg);
		$("#shipModal .alert-danger").show();
	} else if(status == 1) {
		$("#shipModal .alert-success #info").html(msg);
		$("#shipModal .alert-success").show();
	}
}

function cancelAlert(msg, status) {
	$("#cancelModal .alert-danger").hide();
	$("#cancelModal .alert-success").hide();
	if(status == -1) {
		$("#cancelModal .alert-danger #info").html(msg);
		$("#cancelModal .alert-danger").show();
	} else if(status == 1) {
		$("#cancelModal .alert-success #info").html(msg);
		$("#cancelModal .alert-success").show();
	}
}

function bulkError(msg, qty) {
	var error = _getMessage("order.details.bulk.encode.error["+(_numEncoded+1)+"]["+qty+"]["+_numEncoded+"]["+_numRemaining+"]");
	error += "<br/><br/>";
	error += msg;
	encodeAlert(error, -1);
}

function clearAlerts() {
	$(".alert-danger, .alert-success").hide();
	$(".alert #info").text('');
}

function formatMoney(str) {
	str = str+"";
	var dot = str.indexOf(".");
	if(dot<0){
		str += ".00";
	} else if (dot == str.length-2){
		str += "0";
	}
	str = "$"+str;
	return str;
}

function setupOnClick() {
	//Event Handlers
	$("body").on("click", '.alert-div-close', function(){
		$(this).parent("#alert-div").hide();
		clearAlerts();
	});

	$("body").on("click", '#lockOrder', function() {
		if($(this).text() == _getMessage("admin.order.details.lock.order")){
			lockOrder();
		} else {
			unlockOrder();
		}
	});

	$("body").on("click", '#singleCardEncodeNav', function(){
		$("#singleCardEncodeText, #bulkCardEncodeText, #firstUseCardEncodeText").hide();
		$("#singleCardEncodeNav, #bulkCardEncodeNav, #firstUseCardEncodeNav").removeClass("selected");
		$('#singleCardEncodeNav').addClass("selected");
		$("#singleCardEncodeText").show();
	});
	$("body").on("click", '#bulkCardEncodeNav', function(){
		$("#singleCardEncodeText, #bulkCardEncodeText, #firstUseCardEncodeText").hide();
		$("#singleCardEncodeNav, #bulkCardEncodeNav, #firstUseCardEncodeNav").removeClass("selected");
		$('#bulkCardEncodeNav').addClass("selected");
		$("#bulkCardEncodeText").show();
	});
	$("body").on("click", '#firstUseCardEncodeNav', function(){
		$("#singleCardEncodeText, #bulkCardEncodeText, #firstUseCardEncodeText").hide();
		$("#singleCardEncodeNav, #bulkCardEncodeNav, #firstUseCardEncodeNav").removeClass("selected");
		$('#firstUseCardEncodeNav').addClass("selected");
		$("#firstUseCardEncodeText").show();
	});
	$("body").on("click", '.wholeOrderDivShip', function(){
		_log("Whole order Ship");
		_orderItemNumber = null;
		var orderitems = 0;
		$.each(_order.orderItems, function(i,o){
			if(!isCompleteOrCanceled(o.status)) {
				var shipped = o.qShipped;
				var encoded = o.qEncoded;
				orderitems += (encoded - shipped);
			}
		});
		$("#shipModalRowAllLines #shipModalQuantityToShip").html(orderitems);
		$("#shipModalRowSingleLine").hide();
		$("#shipModalRowAllLines").show();
		_log(orderitems);
	});
	$("body").on("click", '.wholeOrderDivCancel', function(){
		clearAlerts();
		_log("Whole order Cancel");
		_orderItemNumber = null;
		var orderitems = "<table class='table table-striped table-bordered table-condensed'><tr>";
		$.each(_order.orderItems, function(i,o){
			if(!isCompleteOrCanceled(o.status)) {
				var encoded = o.qEncoded;
				if(encoded == 0) {
					orderitems += "<td>"+o.itemNumber+"</td>";
				}
			}
		});
		orderitems += "</tr></table>";
		$("#cancelOrderItems").html(orderitems);
		_log(orderitems);
	});
	$("body").on("click", '.lineItemDivCancel', function(){
		clearAlerts();
		_log("Line item Cancel");
		_orderItemNumber = null;
		_orderItemNumber = $(this).parents(".lineItemButtons").attr("name");
		$("#cancelOrderItems").html("<table class='table table-striped table-bordered table-condensed'><tr><td>"+_orderItemNumber+"</td></tr></table>");
	});
	$("body").on("click", '#cancelBtnYes', function(){
		cancelItems(_orderId, _orderItemNumber);
	});
	 $("body").on("click", '.lineItemDivShip', function(){
		 clearAlerts();
		if($(this).hasClass("disabled")){
			return false;
		}
		_orderItemNumber = $(this).parents(".lineItemDetails").find(".lineItemObject").attr("name");

		var shipTotal, shipRemaining, canShip, encodedCards, shipDescription = "";
		var _self = this;
		$.each(_order.orderItems, function(i,o){ 
			if(o.itemNumber == _orderItemNumber){
				shipTotal = o.quantity;
				shipRemaining = shipTotal - o.qShipped;
				encodedCards = o.qEncoded;
				canShip = encodedCards - o.qShipped;
				if(shipRemaining == 0) {
					globalAlert(_getMessage("order.details.none.left.ship"), -1);
			  		return false;
			  	}
				if (encodedCards == 0) {
					_log("No encoded cards, can't ship");
					globalAlert(_getMessage("order.details.cannot.ship.unencoded"), -1);
					return false;
				} else if (encodedCards <= (shipTotal-shipRemaining)){
					_log("No encoded cards left to ship");
					globalAlert(_getMessage("order.details.all.encoded.already.shipped"), -1);
					return false;
				}
				shipDescription = $(_self).parents(".lineItemDetails").find(".lineItemStatus").text();
				shipDescription += "&nbsp;";
				shipDescription += $(_self).parents(".lineItemDetails").find(".lineItemNumber").text();
				$('#shipModalQuantityToShip').text(canShip);
				$('#shipModalProductDescription').html(shipDescription);
				$("#shipModalRowSingleLine").show();
				$("#shipModalRowAllLines").hide();
				$('input[name="orderNumber"]').val(_orderId);
				$('input[name="orderItemNumber"]').val(_orderItemNumber);
			}
		});
	});
	$("body").on("click", '.lineItemDivEncode', function() {
		clearAlerts();
		_orderItemNumber = null;
		_orderItemNumber = $(this).parents(".lineItemDetails").find(".lineItemObject").attr("name");

		var lineEncoded = $(this).parents(".lineItemDetails").find(".lineLeftEncodedRight");
		var encodeTotal = $(lineEncoded).find("span.quantityTotal").text();
		var encodeRemaining = (encodeTotal - $(lineEncoded).find("span.quantityFulfilled").text());
		var encodeDescription = $(this).parents(".lineItemDetails").find(".lineItemStatus").text();
		encodeDescription += "&nbsp;";
		encodeDescription += $(this).parents(".lineItemDetails").find(".lineItemNumber").text();

		_log("Encode total:"+encodeTotal+" remaining:"+encodeRemaining);
		if (encodeRemaining == 0) {
			_log("None left to encode.");
			return false;
		}


		if ($('#encoderAppletHolder').is(':empty')){
			var attributes = {
					id:'encoderApplet',
					width: 1,
					height: 1
				};
			var parameters = {
				jnlp_href: extract_url('/resources/encoder/encoder.jnlp')
			};
			docWriteWrapper($('#encoderAppletHolder'), function () {
				deployJava.runApplet(attributes, parameters, '1.7');
			});
		}
		_log("Find encoder list");
		var encoderList = "";

		try {
			var encoderListResult = encoderApplet.getEncoderList();
			if(encoderListResult.getSuccess()) {
				encoderList = encoderListResult.getValue();
			}
		} catch (e) {
			_err("getting encoder failed: " + e.message);
		}

		if(_resetSingleEncoder == "") {
			_resetSingleEncoder = $("#singleCardEncodeText").html(); 
		}
		if(_resetBulkEncoder == "") {
			_resetBulkEncoder = $("#bulkCardEncodeText").html(); 
		}

		var encoders = "";
		if(encoderList.length > 0) {
			encoders = $.parseJSON(encoderList);
		}

		//commented out because currently isBulkAvail still reads offline printers. 
		//need to find a solution to this in the future.
//		var isBulkPrinterAvailableResult = encoderApplet.isBulkPrinterAvailable();
//		_bulkPresent = isBulkPrinterAvailableResult.getSuccess();
		var singlePresent = false;

		if(encoders.length == 1 ) {
			singlePresent = true;
			_bulkPresent = false;
		} else if (encoders.length > 1) {
			singlePresent = false;
			_bulkPresent = true;
		} else if (encoders.length == 0) {
			singlePresent = false;
			_bulkPresent = false;
		}
		if(!singlePresent) {
			$("#singleCardEncodeText").html("<span class='bg-danger text-danger'>"+_getMessage("order.details.single.encoder.not.found")+"</span>");
			if(!_bulkPresent) {
				$("#firstUseCardEncodeNav").click();
			} else {
				$("#bulkCardEncodeNav").click();
			}
		} else {
			$("#singleCardEncodeText").html(_resetSingleEncoder); 
		}
		if(!_bulkPresent) {
			$("#bulkCardEncodeText").html("<span class='bg-danger text-danger'>"+_getMessage("order.details.bulk.encoder.not.found")+"</span>");
			if(!singlePresent) {
				$("#firstUseCardEncodeNav").click();
			}else{
				$("#singleCardEncodeNav").click();
			}
		} else {
			$("#bulkCardEncodeText").html(_resetBulkEncoder); 
		}

		$("#encodeBulkQty").val(encodeRemaining);
		$('#encodeModalOrdered').text(encodeTotal);
		$('#encodeModalRemaining').text(encodeRemaining);
		$('#encodeModalProduct').html(encodeDescription);
	});

	$("body").on("click", '#printLabels', function(){
		_log("print labels");
		$.ajax({
			url: extract_url("/fulfillment/orders/shipping"),
			type: "POST",
			data: { orderIds: _orderId,  "_csrf": _csrfToken}
		}).done(function(data) {
			_log(data);
			data = data.value;
			if(data.length > 0) {
				var usrAddr = "";
				$.each(data, function(i,o){
					usrAddr += "<div class='printaddr'><span class='addrOrderId'>Order ID: "+o.ordId+"</span><span>"+o.name+"<br>"+o.addr1+" "+o.addr2+"<br>"+o.city+", "+o.state+" "+o.zip+"</span></div>";
				});
				$("#printShippingLabelContent").html(usrAddr);
				$("#printLabelsAction").click();
			}
		}).fail(function(data) {
			_log("Failure getting ship records for ids: "+_printLabelsIDs+". Response text is: "+data.responseText);
		});
	});

	$("body").on("click", ".printButton", function() {
		$("#printTemplate").append($("#printShippingLabelContent").html());
		$(".orderDetailsPageRow").hide();
		$("#printTemplate").show(); //Show print box
		window.print();
		$("#printTemplate").hide();
		$("#printTemplate").html("");
		$(".orderDetailsPageRow").show();
	});

	$("body").on("click", "#encodeSingleCardButton button", function() {
		clearAlerts();
		var item = $(this).parents().find(".lineItemObject").attr('name');
		if(!_encoderSet) {
			$.when(setupEncoder(item))
			.then(function(data) {
				getCurrentOrderToEncode();
		    })
		    .then(function(data) {
		    	encodeSingleCard(item);
		    }).fail(function(data) {
		    	_err("Failed setting up encoder");
		    });
		} else {
			encodeSingleCard(item);
		}
	});

	$("body").on("click", "#encodeBulkCardButton", function() {
		var qty = $("#encodeBulkQty").val();
		qty = parseInt(qty) || -1;
		if(qty == -1) {
			encodeAlert(_getMessage("order.details.cannot.read.bulk.qty"), -1);
			return;
		}
		if(qty > parseInt($("#encodeModalRemaining").text())) {
			encodeAlert(_getMessage("order.details.bulk.qty.greater.encodable"), -1);
			return;
		}
		if(qty < 1) { 
			encodeAlert(_getMessage("order.details.bulk.qty.greater.zero"), -1);
			return;
		}
		clearAlerts();
		_numEncoded = 0;
		_numRemaining = qty;
		var item = $(this).parents().find(".lineItemObject").attr('name');
		if(!_encoderSet) {
			$.when(setupEncoder(item, false))
			.then(function(data) {
				getCurrentOrderToEncode();
		    })
		    .then(function(data) {
		    	encodeBulkCards(item, qty);
		    }).fail(function(data) {
		    	_err("Failed setting up encoder");
		    });
		} else {
			encodeBulkCards(item, qty);
		}


	});

	$("#shipModal").on("click", "#ship-modal-ship", function(){
		var shipDate = $("#shipDateModal").val();
		var tracking = $("input[name='tracking']").val();
		var shipper = "";
		if($("select[name='shipperId']").val()){
			shipper = $("select[name='shipperId']").val();
		} else {
			shipper = $("#textShipper").attr("name");
		}

		clearAlerts();
		if(!shipDate) {
			shipAlert(_getMessage("errors.must.insert.ship.date"), -1);
		} else {
			shipItems(_orderId, _orderItemNumber, shipDate, tracking, shipper);
		}
	});

	$("#encodeModal").on("click", "#encodeFirstUseCardButton", function(){
		var lowPID = $("input[name='lowPID']").val();
		var highPID = $("input[name='highPID']").val();
		var error = false;
		if(lowPID) {
			if(!highPID){
				highPID = lowPID;
			}
			if(!$.isNumeric(lowPID) || !$.isNumeric(highPID)) {
				error = true;
				clearAlerts();
				encodeAlert(_getMessage("errors.only.numeric"), -1);
			} else {
				if(parseInt(lowPID) > parseInt(highPID)) {
					error = true;
					clearAlerts();
					encodeAlert(_getMessage("errors.first.id.lower"), -1);
				} else {
					var cardsRemaining = $("#encodeModalRemaining").text();
					var amt = (parseInt(highPID) - parseInt(lowPID));
					if(parseInt(amt) > parseInt(cardsRemaining)){
						error = true;
						clearAlerts();
						encodeAlert(_getMessage("errors.encode.limit["+cardsRemaining+"]"), -1);
					}
				}
			}
		} else {
			error = true;
			clearAlerts();
			encodeAlert(_getMessage("errors.must.include.low.id"), -1);
		}
		if (!error) {
			clearAlerts();
			$.when(encodeCardsInDatabase(_orderId, _orderItemNumber, lowPID, highPID)).fail(function(rtn, msg){
				encodeAlert(msg, -1);
				_err("Failed to encode cards in DB");
			}).done(function(){
				_log("finished encoding");
			});
		}
	});

	$("#backButton").on("click", function(){
		var form = $("form#goBack");
		var orderNumber = $("#searchOrderNumber").text();
		var fromDate = $("#searchFromDate").text();
		var toDate = $("#searchToDate").text();
		var status = $("#searchStatus").text();
		$(form).children("input[name='orderNumber']").val(orderNumber);
		$(form).children("input[name='fromDate']").val(fromDate);
		$(form).children("input[name='toDate']").val(toDate);
		$(form).children("input[name='status']").val(status);
		$(form).submit();
	});
}

/*
 * Replace docwritewrapper so encoder applet doesn't reload whole page
 */
function docWriteWrapper(jq, func) {
	var oldwrite = document.write, content = '';
	document.write = function(text) {
		content += text;
	};
	func();
	document.write = oldwrite;
	jq.html(content);
}

/**
 * Populate a list of all current orderitems. This is required because with the
 * way we handle orderitems & children, when retrieving an order by item the
 * index of the item may be in a different location than in the index in the
 * order.orderItems list
 * 
 * @param order
 */
function getAllItems(order) {
	var allItems = {};
	var count = 0; 
	$.each(order.orderItems, function(i,o) {
	  allItems[count++] = o;
		$.each(o.children, function(j, c) {
		  allItems[count++] = c;
		});
	});
	return allItems;
}