/**
 * Find Orders custom javascript
 * 
 * @author alimoussa
 */
var _printLabelsIDs = new Array();
function init(){
	$("#order-search-results").hide();

	/**
	 * Setup Date Picker
	 */
	$( "#from" ).datepicker({
		defaultDate: "-1w",
		changeMonth: true
	});
	$( "#to" ).datepicker({
		changeMonth: true
	});

	$("#order-search-results").dataTable({
		"lengthChange" : false,
		"searching" : false,
		"order" : [ [ 1, 'desc' ], [ 2, 'asc' ] ],
		"aoColumnDefs" : [ {
			"bSortable" : false,
			"aTargets" : [ 0, 8 ]
		} ]
	});
	
	$("#order-search-results").on("click", ".lineItemCheckBox", function(){
		if($(".lineItemCheckBox:checked").size() > 0){
			$("#printLabels").removeClass("disabled");
		} else {
			$("#printLabels").addClass("disabled");
		}
		var ordId = $(this).parent().parent().find(".detailOrderId").text();
		if(this.checked){
			_printLabelsIDs.push(parseInt(ordId));
		} else {
			removeID(parseInt(ordId), _printLabelsIDs);
		}
	});
	
	$("#printLabels").click(function(){
		_log("print labels");
		$.ajax({
			url: extract_url("/fulfillment/orders/shipping"),
			type: "POST",
			data: { orderIds: _printLabelsIDs.toString(),  "_csrf": _csrfToken}
		}).done(function(data) {
			_log(data);
			data = data.value;
			if(data.length > 0){
				var usrAddr = "";
				$.each(data, function(i,o){
					usrAddr += "<div class='printaddr'><span class='addrOrderId'>Order ID: "+o.ordId+"</span><span>"+o.name+"<br>"+o.addr1+" "+o.addr2+"<br>"+o.city+", "+o.state+" "+o.zip+"</span></div>";	
				});
				$("#printShippingLabelContent").html(usrAddr);
				$("#printLabelsAction").click();
			}
		}).fail(function(data) {
			_log("Failure getting ship records for ids: "+_printLabelsIDs+". Response text is: "+data.responseText);
		});
	});
	
}

function setupDataTables(){
	_log("Setting up data tables click events");
	$("tr").on("click", ".viewDetail", function(){
		var id = $(this).parents("tr").children(".detailOrderId").text();
		var searchId = $("#findOrdersSearchRow input[name='orderNumber']").val();
		$(this).parents("form.details").children("input[name='orderNumber']").val(id);
		$(this).parents("form.details").children("input[name='searchOrderNumber']").val(searchId);
		$(this).parents("form.details").submit();
	});
	$("#order-search-results").show();
}

init();

function removeID(id, array){
	var index = array.indexOf(id);
	if (index > -1) {
	    array.splice(index, 1);
	}
}

$(document).ready(function() {
	setupDataTables();
});